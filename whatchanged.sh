#!/bin/bash

VERBOSE="false"
IDENT="*"

CODECDIR=$(find . -maxdepth 3 -name 'generated_prose' -type d)
STABLE=$(ls "$CODECDIR" | sed 's/\..*$//' | sort -g -u | tail -1)

usage() {
    echo -e "\x1b[1mwhatchanged\x1b[0m: script for finding what schemas were changed between two protocol versions\n\
		\rusage: whatchanged [-v] [-i <id>] OLDVERSION NEWVERSION\n\
		\r\n\
		\r\x1b[1mOptions\x1b[0m:\n\
		\r\t\x1b[33m-v\x1b[0m: Emits full output of the unified diff between the changed (prose) codecs\n\
		\r\t\x1b[33m-i\x1b[0m <ID>: only looks for changes for the specified identifier <ID> (must be verbatim)\
        \r\n\
        \r\x1b[1mSupported Versions\x1b[0m:"
    cat <(ls "$CODECDIR" | sed 's/\..*$//' | sort -g -u)
    exit 0
}

while getopts ":hvi:" opt; do
    case ${opt} in
    v)
        VERBOSE="true"
        ;;
    i)
        IDENT=$OPTARG
        ;;
    h | *)
        usage
        ;;
    esac
done

shift $(($OPTIND - 1))

OLD="${1:-$STABLE}"
NEW="${2:-alpha}"

main() {
    if grep -q "$OLD" <(ls | sed 's/\..*$//' | sort -g -u); then
        if grep -q "$NEW" <(ls | sed 's/\..*$//' | sort -g -u); then
            if [[ "$OLD" < "$NEW" ]]; then
                echo -e -n "[\x1b[1mwhatchanged\x1b[0m]: Comparing \x1b[31m$OLD\x1b[0m to \x1b[32m$NEW\x1b[0m"
            else
                echo -e "[\x1b[33m\x1b[1mwhatchanged\x1b[0m]: version order is flipped: $NEW < $OLD"
                exit 1
            fi
        else
            echo -e "[\x1b[33m\x1b[1mwhatchanged\x1b[0m]: Second version '$NEW' not found; use '-h' flag for list of supported versions"
            exit 1
        fi
    else
        echo -e "[\x1b[33m\x1b[1mwhatchanged\x1b[0m]: First version '$OLD' not found; use '-h' flag for list of supported versions"
        exit 1
    fi
    if [ "$VERBOSE" == "true" ]; then
        echo " (full output)"
    else
        echo " (summary only, use -v flag for full output)"
    fi
    if [[ "$IDENT" != "*" && ! -f "$NEW.$IDENT.prose" && ! -f "$OLD.$IDENT.prose" ]]; then
        echo -e "[\x1b[33m\x1b[1mwhatchanged\x1b[0m]: schema identifier \"$IDENT\" does not appear to exist in either version ('\x1b[31m$OLD\x1b[0m', '\x1b[32m$NEW\x1b[0m')..."
        exit 1
    fi
    for i in $NEW.$IDENT.prose; do
        ID="${i#*.}"
        if [ -f "$OLD.$ID" ]; then
            case $NEW in
            alpha)
                cmp -s <(sed -E "s/(Proto)?${OLD%-*}(.)${OLD#*-}/$NEW/g" "$OLD.$ID") "$i" || echo -e "[\x1b[1mwhatchanged\x1b[0m]:  \x1b[31m$OLD\x1b[0m.$ID -> \x1b[32m${i%%.*}\x1b[0m.${i#*.}"
                if [ "$VERBOSE" == "true" ]; then
                    diff -u -w <(sed -E "s/(Proto)?${OLD%-*}(.)${OLD#*-}/$NEW/g" "$OLD.$ID") "$i"
                fi
                ;;
            *)
                cmp -s <(sed -E "s/${OLD%-*}(.)${OLD#*-}/${NEW%-*}\\1${NEW#*-}/g" "$OLD.$ID") "$i" || echo -e "[\x1b[1mwhatchanged\x1b[0m]:  \x1b[31m$OLD\x1b[0m.$ID -> \x1b[32m${i%%.*}\x1b[0m.${i#*.}"
                if [ "$VERBOSE" == "true" ]; then
                    diff -u -w <(sed -E "s/${OLD%-*}(.)${OLD#*-}/${NEW%-*}\\1${NEW#*-}/g" "$OLD.$ID") "$i"
                fi
                ;;
            esac
        else
            echo -e "[\x1b[1mwhatchanged\x1b[0m]:  new schema \x1b[1m${ID%.*}\x1b[0m was added between \"\x1b[31m$OLD\x1b[0m\"..\"\x1b[32m$NEW\x1b[0m\""
        fi
    done
    for i in $OLD.$IDENT.prose; do
        if [ ! -f "$NEW.${i#*.}" ]; then
            ID=${i#*.}
            echo -e "[\x1b[1mwhatchanged\x1b[0m]:  schema \"\x1b[1m${ID%.*}\x1b[0m\" was removed between \"\x1b[31m$OLD\x1b[0m\"..\"\x1b[32m$NEW\x1b[0m\""
        fi
    done
}

cd "$CODECDIR" && main
