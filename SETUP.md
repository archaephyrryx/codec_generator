# Usage Instructions

There are two primary ways to make use of the `codec_generator` infrastructure: as a consumer, and as a developer.

## For Consumers

If there is no ongoing need to recompile existing codecs on-demand, or to customize the compiler implementation or input, a consumer can simply use the output of a `codec_generator` compilation run that is included in this repository and updated on a semi-regular basis.

The most direct way that a consumer can use this project is through the PROSE target format: either interrogating the contents of the generated PROSE codecs manually, or by using the packaged `whatchanged.sh` script.

### `whatchanged.sh`

The `whatchanged.sh` script is written in Bash, and should be usable without any further configuration steps, immediately after cloning this repository (or otherwise downloading the script itself, and the contents of the `generated_prose` directory).

This script reports on the imputed changes between any two protocol versions, defaulting 'latest non-alpha' ~ 'alpha' (based on the latest Octez version for which the `codec_generator` compiler has run).

When called without any arguments, it will produce a summary output of what identifiers were subject to change between the implicitly selected versions,
noting only the fact that at least one line of text differed substantively,
and supressing the actual diff output. Running with `-v` (verbose) will
unsupress the diff output, which can be quite long.

The `-i <IDENT>` flag can be used to specify a single codec-identifier to consider, and will either indicate that no changes were made, output a summary indicating that some changes were made, or, in combination with `-v`, output the substantive changeset.

For more up-to-date information on how to use the script, a `-h` (help) flag is also supported, which includes a list of the supported protocol versions that can be compared.

It is possible to control what versions are selected for comparison, by providing their literal prefix. Specifying only one such argument (which must follow any flags) will compare that version, if it is valid, against alpha; specifying two protocol version arguments (oldest first) will compare the first such argument against the second, treating any changes as updates from the first to the second. If the order is flipped, a warning message will be displayed rather than generate misleading output.

## Developer Setup

Several steps, many of which may be difficult to reproduce, troubleshoot, or otherwise make fully portable, are required to run the `codec_generator` locally. These are intended only as a rough guide as to what steps were required to boostrap the compilation process, and not as a set of steps that can be reproduced as-is and yield a working setup.

### Step 1: Setup a fresh clone of octez

```bash
cd
if [ -d tezos || -f tezos ]; then mkdir sandboxed && cd sandboxed; fi
git clone https://github.com/tezos/tezos
cd tezos
```

If `opam` is not already installed, it should be installed now.

### Step 2: Setup a fresh opam switch

```bash
opam switch create . 4.14.0
eval $(opam env)
```

(The version number should be updated based on the minimum required version by octez)

```bash
make build-deps
```

(This command may take a while, and may require some troubleshooting and rerunning).

```bash
opam pin data-encoding.dev git+https://gitlab.com/archaephyrryx/data-encoding#base-patch
eval $(opam env)
```

This step may cause issues if it is run too early, and unpinning may be required between octez version bumps, to avoid dependency issues.

### Step 3: Setup linking

```bash
cd .. && if [ ! -d codec_generator]; then git clone https://gitlab.com/archaephyrryx/codec_generator; fi
cd $OLDPWD
if [ ! -h src/bin_codec_generator]; then ln -s ../codec_generator src/bin_codec_generator; fi
```

(This allows dune to find all the octez-package dependencies within the `codec_generator` config, and compile them together)

### Step 4 (WILL NOT WORK!): Run the compiler

:::warning
This command **WILL NOT WORK** for general users at this time, due to a non-portable hack in the directory-finding logic of `codec_generator`, that assumes a specific absolute path to the project root. Therefore, it is only possible to run this following command on a custom patch of the `codec_generator` implementation, or in future when greater portability has been introduced to the compiler executable.
:::

```bash
dune build src/bin_codec_generator/src/compiler_main.exe && ./_build/default/src/bin_codec_generator/src/compiler_main.exe
```

Running this, if everything has been set up correctly, should generate PROSE, Rust, and certain TypeScript codec files to the respective destination directories in the codec_generator clone (again, this is not yet portable and will not work).
