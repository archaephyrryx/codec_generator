#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto015PtLimaPtGas,u8,proto015ptlimaptgas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto015ptlimaptgas_write<U: Target>(val: &Proto015PtLimaPtGas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptgas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtGas> {
    Proto015PtLimaPtGas::parse(p)
}
