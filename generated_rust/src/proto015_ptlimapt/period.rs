#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto015PtLimaPtPeriod = i64;
#[allow(dead_code)]
pub fn proto015ptlimaptperiod_write<U: Target>(val: &Proto015PtLimaPtPeriod, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtPeriod> {
    Ok(i64::parse(p)?)
}
