#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto015PtLimaPtVotingPeriodKind,u8,proto015ptlimaptvotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtVotingPeriod { pub index: i32, pub kind: Proto015PtLimaPtVotingPeriodKind, pub start_position: i32 }
#[allow(dead_code)]
pub fn proto015ptlimaptvotingperiod_write<U: Target>(val: &Proto015PtLimaPtVotingPeriod, buf: &mut U) -> usize {
    i32::write_to(&val.index, buf) + Proto015PtLimaPtVotingPeriodKind::write_to(&val.kind, buf) + i32::write_to(&val.start_position, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptvotingperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtVotingPeriod> {
    Ok(Proto015PtLimaPtVotingPeriod {index: i32::parse(p)?, kind: Proto015PtLimaPtVotingPeriodKind::parse(p)?, start_position: i32::parse(p)?})
}
