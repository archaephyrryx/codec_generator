#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{AutoBox,ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Padded,ParseResult,Parser,Sequence,Target,Z,cstyle,data,resolve_zero,u30};
data!(MichelineProto015PtLimaPtMichelsonV1Expression,u8,michelineproto015ptlimaptmichelsonv1expression,{
        0 => Int  { pub int: ::rust_runtime::Z },
        1 => String  { pub string: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        2 => Sequence (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineProto015PtLimaPtMichelsonV1Expression>>>),
        3 => Prim__no_args__no_annots  { pub prim: Proto015PtLimaPtMichelsonV1Primitives },
        4 => Prim__no_args__some_annots  { pub prim: Proto015PtLimaPtMichelsonV1Primitives, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        5 => Prim__1_arg__no_annots  { pub prim: Proto015PtLimaPtMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineProto015PtLimaPtMichelsonV1Expression> },
        6 => Prim__1_arg__some_annots  { pub prim: Proto015PtLimaPtMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineProto015PtLimaPtMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        7 => Prim__2_args__no_annots  { pub prim: Proto015PtLimaPtMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineProto015PtLimaPtMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineProto015PtLimaPtMichelsonV1Expression> },
        8 => Prim__2_args__some_annots  { pub prim: Proto015PtLimaPtMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineProto015PtLimaPtMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineProto015PtLimaPtMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        9 => Prim__generic  { pub prim: Proto015PtLimaPtMichelsonV1Primitives, pub args: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineProto015PtLimaPtMichelsonV1Expression>>>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        10 => Bytes  { pub bytes: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtApplyInternalResultsAlphaOperationResultDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtApplyInternalResultsAlphaOperationResultOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto015PtLimaPtApplyInternalResultsAlphaOperationResultRhs,u8,proto015ptlimaptapplyinternalresultsalphaoperationresultrhs,{
        1 => Transaction  { pub amount: ::rust_runtime::N, pub destination: Proto015PtLimaPtTransactionDestination, pub parameters: std::option::Option<Proto015PtLimaPtApplyInternalResultsAlphaOperationResultTransactionParameters> },
        2 => Origination  { pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto015PtLimaPtApplyInternalResultsAlphaOperationResultOriginationDelegate>, pub script: Proto015PtLimaPtScriptedContracts },
        3 => Delegation  { pub delegate: std::option::Option<Proto015PtLimaPtApplyInternalResultsAlphaOperationResultDelegationDelegate> },
        4 => Event  { pub r#type: MichelineProto015PtLimaPtMichelsonV1Expression, pub tag: std::option::Option<Proto015PtLimaPtEntrypoint>, pub payload: std::option::Option<::rust_runtime::AutoBox<MichelineProto015PtLimaPtMichelsonV1Expression>> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtApplyInternalResultsAlphaOperationResultTransactionParameters { pub entrypoint: Proto015PtLimaPtEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(Proto015PtLimaPtContractId,u8,proto015ptlimaptcontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto015PtLimaPtContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto015PtLimaPtEntrypoint,u8,proto015ptlimaptentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        5 => deposit ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
cstyle!(Proto015PtLimaPtMichelsonV1Primitives,u8,{
        parameter = 0,
        storage = 1,
        code = 2,
        False = 3,
        Elt = 4,
        Left = 5,
        None = 6,
        Pair = 7,
        Right = 8,
        Some = 9,
        True = 10,
        Unit = 11,
        PACK = 12,
        UNPACK = 13,
        BLAKE2B = 14,
        SHA256 = 15,
        SHA512 = 16,
        ABS = 17,
        ADD = 18,
        AMOUNT = 19,
        AND = 20,
        BALANCE = 21,
        CAR = 22,
        CDR = 23,
        CHECK_SIGNATURE = 24,
        COMPARE = 25,
        CONCAT = 26,
        CONS = 27,
        CREATE_ACCOUNT = 28,
        CREATE_CONTRACT = 29,
        IMPLICIT_ACCOUNT = 30,
        DIP = 31,
        DROP = 32,
        DUP = 33,
        EDIV = 34,
        EMPTY_MAP = 35,
        EMPTY_SET = 36,
        EQ = 37,
        EXEC = 38,
        FAILWITH = 39,
        GE = 40,
        GET = 41,
        GT = 42,
        HASH_KEY = 43,
        IF = 44,
        IF_CONS = 45,
        IF_LEFT = 46,
        IF_NONE = 47,
        INT = 48,
        LAMBDA = 49,
        LE = 50,
        LEFT = 51,
        LOOP = 52,
        LSL = 53,
        LSR = 54,
        LT = 55,
        MAP = 56,
        MEM = 57,
        MUL = 58,
        NEG = 59,
        NEQ = 60,
        NIL = 61,
        NONE = 62,
        NOT = 63,
        NOW = 64,
        OR = 65,
        PAIR = 66,
        PUSH = 67,
        RIGHT = 68,
        SIZE = 69,
        SOME = 70,
        SOURCE = 71,
        SENDER = 72,
        SELF = 73,
        STEPS_TO_QUOTA = 74,
        SUB = 75,
        SWAP = 76,
        TRANSFER_TOKENS = 77,
        SET_DELEGATE = 78,
        UNIT = 79,
        UPDATE = 80,
        XOR = 81,
        ITER = 82,
        LOOP_LEFT = 83,
        ADDRESS = 84,
        CONTRACT = 85,
        ISNAT = 86,
        CAST = 87,
        RENAME = 88,
        bool = 89,
        contract = 90,
        int = 91,
        key = 92,
        key_hash = 93,
        lambda = 94,
        list = 95,
        map = 96,
        big_map = 97,
        nat = 98,
        option = 99,
        or = 100,
        pair = 101,
        set = 102,
        signature = 103,
        string = 104,
        bytes = 105,
        mutez = 106,
        timestamp = 107,
        unit = 108,
        operation = 109,
        address = 110,
        SLICE = 111,
        DIG = 112,
        DUG = 113,
        EMPTY_BIG_MAP = 114,
        APPLY = 115,
        chain_id = 116,
        CHAIN_ID = 117,
        LEVEL = 118,
        SELF_ADDRESS = 119,
        never = 120,
        NEVER = 121,
        UNPAIR = 122,
        VOTING_POWER = 123,
        TOTAL_VOTING_POWER = 124,
        KECCAK = 125,
        SHA3 = 126,
        PAIRING_CHECK = 127,
        bls12_381_g1 = 128,
        bls12_381_g2 = 129,
        bls12_381_fr = 130,
        sapling_state = 131,
        sapling_transaction_deprecated = 132,
        SAPLING_EMPTY_STATE = 133,
        SAPLING_VERIFY_UPDATE = 134,
        ticket = 135,
        TICKET_DEPRECATED = 136,
        READ_TICKET = 137,
        SPLIT_TICKET = 138,
        JOIN_TICKETS = 139,
        GET_AND_UPDATE = 140,
        chest = 141,
        chest_key = 142,
        OPEN_CHEST = 143,
        VIEW = 144,
        view = 145,
        constant = 146,
        SUB_MUTEZ = 147,
        tx_rollup_l2_address = 148,
        MIN_BLOCK_TIME = 149,
        sapling_transaction = 150,
        EMIT = 151,
        Lambda_rec = 152,
        LAMBDA_REC = 153,
        TICKET = 154
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(Proto015PtLimaPtTransactionDestination,u8,proto015ptlimapttransactiondestination,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto015PtLimaPtTransactionDestinationOriginatedDenestPad,1>),
        2 => Tx_rollup (pub ::rust_runtime::Padded<Proto015PtLimaPtTxRollupId,1>),
        3 => Sc_rollup (pub ::rust_runtime::Padded<Proto015PtLimaPtTransactionDestinationScRollupDenestPad,1>),
        4 => Zk_rollup (pub ::rust_runtime::Padded<Proto015PtLimaPtTransactionDestinationZkRollupDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtTransactionDestinationOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtTransactionDestinationScRollupDenestPad { pub sc_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtTransactionDestinationZkRollupDenestPad { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtOperationInternal { pub source: Proto015PtLimaPtContractId, pub nonce: u16, pub proto015_ptlimapt_apply_internal_results_alpha_operation_result_rhs: Proto015PtLimaPtApplyInternalResultsAlphaOperationResultRhs }
#[allow(dead_code)]
pub fn proto015ptlimaptoperationinternal_write<U: Target>(val: &Proto015PtLimaPtOperationInternal, buf: &mut U) -> usize {
    Proto015PtLimaPtContractId::write_to(&val.source, buf) + u16::write_to(&val.nonce, buf) + Proto015PtLimaPtApplyInternalResultsAlphaOperationResultRhs::write_to(&val.proto015_ptlimapt_apply_internal_results_alpha_operation_result_rhs, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptoperationinternal_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtOperationInternal> {
    Ok(Proto015PtLimaPtOperationInternal {source: Proto015PtLimaPtContractId::parse(p)?, nonce: u16::parse(p)?, proto015_ptlimapt_apply_internal_results_alpha_operation_result_rhs: Proto015PtLimaPtApplyInternalResultsAlphaOperationResultRhs::parse(p)?})
}
