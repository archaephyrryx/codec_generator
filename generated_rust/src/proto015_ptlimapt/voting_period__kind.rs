#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto015PtLimaPtVotingPeriodKind,u8,proto015ptlimaptvotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[allow(dead_code)]
pub fn proto015ptlimaptvotingperiodkind_write<U: Target>(val: &Proto015PtLimaPtVotingPeriodKind, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptvotingperiodkind_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtVotingPeriodKind> {
    Proto015PtLimaPtVotingPeriodKind::parse(p)
}
