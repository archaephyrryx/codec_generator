#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto015PtLimaPtFitnessLockedRound,u8,proto015ptlimaptfitnesslockedround,{
        0 => None ,
        1 => Some (pub i32),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtFitness { pub level: i32, pub locked_round: Proto015PtLimaPtFitnessLockedRound, pub predecessor_round: i32, pub round: i32 }
#[allow(dead_code)]
pub fn proto015ptlimaptfitness_write<U: Target>(val: &Proto015PtLimaPtFitness, buf: &mut U) -> usize {
    i32::write_to(&val.level, buf) + Proto015PtLimaPtFitnessLockedRound::write_to(&val.locked_round, buf) + i32::write_to(&val.predecessor_round, buf) + i32::write_to(&val.round, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptfitness_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtFitness> {
    Ok(Proto015PtLimaPtFitness {level: i32::parse(p)?, locked_round: Proto015PtLimaPtFitnessLockedRound::parse(p)?, predecessor_round: i32::parse(p)?, round: i32::parse(p)?})
}
