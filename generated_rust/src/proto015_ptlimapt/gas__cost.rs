#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type Proto015PtLimaPtGasCost = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn proto015ptlimaptgascost_write<U: Target>(val: &Proto015PtLimaPtGasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptgascost_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtGasCost> {
    Ok(::rust_runtime::Z::parse(p)?)
}
