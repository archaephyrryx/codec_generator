#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto015PtLimaPtCycle = i32;
#[allow(dead_code)]
pub fn proto015ptlimaptcycle_write<U: Target>(val: &Proto015PtLimaPtCycle, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptcycle_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtCycle> {
    Ok(i32::parse(p)?)
}
