#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtBlockHeaderProtocolData { pub payload_hash: Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature }
#[allow(dead_code)]
pub fn proto015ptlimaptblockheaderprotocoldata_write<U: Target>(val: &Proto015PtLimaPtBlockHeaderProtocolData, buf: &mut U) -> usize {
    Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash::write_to(&val.payload_hash, buf) + i32::write_to(&val.payload_round, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + i8::write_to(&val.liquidity_baking_toggle_vote, buf) + Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptblockheaderprotocoldata_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtBlockHeaderProtocolData> {
    Ok(Proto015PtLimaPtBlockHeaderProtocolData {payload_hash: Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash::parse(p)?, payload_round: i32::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, liquidity_baking_toggle_vote: i8::parse(p)?, signature: Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature::parse(p)?})
}
