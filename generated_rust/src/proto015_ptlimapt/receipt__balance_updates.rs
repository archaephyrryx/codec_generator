#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(Proto015PtLimaPtBondId,u8,proto015ptlimaptbondid,{
        0 => Tx_rollup_bond_id  { pub tx_rollup: Proto015PtLimaPtTxRollupId },
        1 => Sc_rollup_bond_id  { pub sc_rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
    }
    );
data!(Proto015PtLimaPtContractId,u8,proto015ptlimaptcontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto015PtLimaPtContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto015PtLimaPtOperationMetadataAlphaBalance,u8,proto015ptlimaptoperationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto015PtLimaPtContractId, pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        2 => Block_fees  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        4 => Deposits  { pub delegate: Proto015PtLimaPtOperationMetadataAlphaBalanceDepositsDelegate, pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        5 => Nonce_revelation_rewards  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        6 => Double_signing_evidence_rewards  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        7 => Endorsing_rewards  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        8 => Baking_rewards  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        9 => Baking_bonuses  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        11 => Storage_fees  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        12 => Double_signing_punishments  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        13 => Lost_endorsing_rewards  { pub delegate: Proto015PtLimaPtOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate, pub participation: bool, pub revelation: bool, pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        14 => Liquidity_baking_subsidies  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        15 => Burned  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        16 => Commitments  { pub committer: Proto015PtLimaPtOperationMetadataAlphaBalanceCommitmentsCommitter, pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        17 => Bootstrap  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        18 => Invoice  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        19 => Initial_commitments  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        20 => Minted  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        21 => Frozen_bonds  { pub contract: Proto015PtLimaPtContractId, pub bond_id: Proto015PtLimaPtBondId, pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        22 => Tx_rollup_rejection_rewards  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        23 => Tx_rollup_rejection_punishments  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        24 => Sc_rollup_refutation_punishments  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
        25 => Sc_rollup_refutation_rewards  { pub change: i64, pub origin: Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtOperationMetadataAlphaBalanceCommitmentsCommitter { pub blinded_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtOperationMetadataAlphaBalanceDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto015PtLimaPtOperationMetadataAlphaUpdateOriginOrigin,u8,proto015ptlimaptoperationmetadataalphaupdateoriginorigin,{
        0 => Block_application ,
        1 => Protocol_migration ,
        2 => Subsidy ,
        3 => Simulation ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto015PtLimaPtReceiptBalanceUpdates = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto015PtLimaPtOperationMetadataAlphaBalance>>;
#[allow(dead_code)]
pub fn proto015ptlimaptreceiptbalanceupdates_write<U: Target>(val: &Proto015PtLimaPtReceiptBalanceUpdates, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto015PtLimaPtOperationMetadataAlphaBalance>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptreceiptbalanceupdates_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtReceiptBalanceUpdates> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto015PtLimaPtOperationMetadataAlphaBalance>>::parse(p)?)
}
