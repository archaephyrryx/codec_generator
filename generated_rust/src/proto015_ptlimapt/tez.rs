#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,N,ParseResult,Parser,Target,resolve_zero};
pub type Proto015PtLimaPtTez = ::rust_runtime::N;
#[allow(dead_code)]
pub fn proto015ptlimapttez_write<U: Target>(val: &Proto015PtLimaPtTez, buf: &mut U) -> usize {
    ::rust_runtime::N::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimapttez_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtTez> {
    Ok(::rust_runtime::N::parse(p)?)
}
