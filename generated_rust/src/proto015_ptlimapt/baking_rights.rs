#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct ConsensusKey { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Delegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtBakingRights { pub level: i32, pub delegate: Delegate, pub round: i32, pub estimated_time: std::option::Option<i64>, pub consensus_key: ConsensusKey }
#[allow(dead_code)]
pub fn proto015ptlimaptbakingrights_write<U: Target>(val: &Proto015PtLimaPtBakingRights, buf: &mut U) -> usize {
    i32::write_to(&val.level, buf) + Delegate::write_to(&val.delegate, buf) + i32::write_to(&val.round, buf) + std::option::Option::<i64>::write_to(&val.estimated_time, buf) + ConsensusKey::write_to(&val.consensus_key, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptbakingrights_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtBakingRights> {
    Ok(Proto015PtLimaPtBakingRights {level: i32::parse(p)?, delegate: Delegate::parse(p)?, round: i32::parse(p)?, estimated_time: std::option::Option::<i64>::parse(p)?, consensus_key: ConsensusKey::parse(p)?})
}
