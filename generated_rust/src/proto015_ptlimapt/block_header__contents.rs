#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto015PtLimaPtBlockHeaderContents { pub payload_hash: Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8 }
#[allow(dead_code)]
pub fn proto015ptlimaptblockheadercontents_write<U: Target>(val: &Proto015PtLimaPtBlockHeaderContents, buf: &mut U) -> usize {
    Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash::write_to(&val.payload_hash, buf) + i32::write_to(&val.payload_round, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + i8::write_to(&val.liquidity_baking_toggle_vote, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptblockheadercontents_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtBlockHeaderContents> {
    Ok(Proto015PtLimaPtBlockHeaderContents {payload_hash: Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash::parse(p)?, payload_round: i32::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, liquidity_baking_toggle_vote: i8::parse(p)?})
}
