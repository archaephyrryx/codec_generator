#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto015PtLimaPtRawLevel = i32;
#[allow(dead_code)]
pub fn proto015ptlimaptrawlevel_write<U: Target>(val: &Proto015PtLimaPtRawLevel, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptrawlevel_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtRawLevel> {
    Ok(i32::parse(p)?)
}
