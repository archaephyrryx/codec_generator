#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto015PtLimaPtVoteBallot = i8;
#[allow(dead_code)]
pub fn proto015ptlimaptvoteballot_write<U: Target>(val: &Proto015PtLimaPtVoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto015ptlimaptvoteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto015PtLimaPtVoteBallot> {
    Ok(i8::parse(p)?)
}
