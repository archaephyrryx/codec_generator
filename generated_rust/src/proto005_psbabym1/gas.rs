#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto005PsBabyM1Gas,u8,proto005psbabym1gas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto005psbabym1gas_write<U: Target>(val: &Proto005PsBabyM1Gas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1gas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1Gas> {
    Proto005PsBabyM1Gas::parse(p)
}
