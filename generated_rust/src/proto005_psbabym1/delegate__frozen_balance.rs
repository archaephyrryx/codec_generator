#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,N,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1DelegateFrozenBalance { pub deposit: ::rust_runtime::N, pub fees: ::rust_runtime::N, pub rewards: ::rust_runtime::N }
#[allow(dead_code)]
pub fn proto005psbabym1delegatefrozenbalance_write<U: Target>(val: &Proto005PsBabyM1DelegateFrozenBalance, buf: &mut U) -> usize {
    ::rust_runtime::N::write_to(&val.deposit, buf) + ::rust_runtime::N::write_to(&val.fees, buf) + ::rust_runtime::N::write_to(&val.rewards, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1delegatefrozenbalance_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1DelegateFrozenBalance> {
    Ok(Proto005PsBabyM1DelegateFrozenBalance {deposit: ::rust_runtime::N::parse(p)?, fees: ::rust_runtime::N::parse(p)?, rewards: ::rust_runtime::N::parse(p)?})
}
