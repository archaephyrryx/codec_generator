#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto005PsBabyM1Timestamp = i64;
#[allow(dead_code)]
pub fn proto005psbabym1timestamp_write<U: Target>(val: &Proto005PsBabyM1Timestamp, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1timestamp_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1Timestamp> {
    Ok(i64::parse(p)?)
}
