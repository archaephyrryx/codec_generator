#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,i31,resolve_zero};
pub type Proto005PsBabyM1ScriptLoc = ::rust_runtime::i31;
#[allow(dead_code)]
pub fn proto005psbabym1scriptloc_write<U: Target>(val: &Proto005PsBabyM1ScriptLoc, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1scriptloc_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1ScriptLoc> {
    Ok(::rust_runtime::i31::parse(p)?)
}
