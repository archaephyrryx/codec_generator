#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto005PsBabyM1VoteBallot = i8;
#[allow(dead_code)]
pub fn proto005psbabym1voteballot_write<U: Target>(val: &Proto005PsBabyM1VoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1voteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1VoteBallot> {
    Ok(i8::parse(p)?)
}
