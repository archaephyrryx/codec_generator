#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,N,ParseResult,Parser,Target,resolve_zero};
pub type Proto005PsBabyM1Tez = ::rust_runtime::N;
#[allow(dead_code)]
pub fn proto005psbabym1tez_write<U: Target>(val: &Proto005PsBabyM1Tez, buf: &mut U) -> usize {
    ::rust_runtime::N::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1tez_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1Tez> {
    Ok(::rust_runtime::N::parse(p)?)
}
