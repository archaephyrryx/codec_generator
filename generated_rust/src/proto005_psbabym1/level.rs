#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1Level { pub level: i32, pub level_position: i32, pub cycle: i32, pub cycle_position: i32, pub voting_period: i32, pub voting_period_position: i32, pub expected_commitment: bool }
#[allow(dead_code)]
pub fn proto005psbabym1level_write<U: Target>(val: &Proto005PsBabyM1Level, buf: &mut U) -> usize {
    i32::write_to(&val.level, buf) + i32::write_to(&val.level_position, buf) + i32::write_to(&val.cycle, buf) + i32::write_to(&val.cycle_position, buf) + i32::write_to(&val.voting_period, buf) + i32::write_to(&val.voting_period_position, buf) + bool::write_to(&val.expected_commitment, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1level_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1Level> {
    Ok(Proto005PsBabyM1Level {level: i32::parse(p)?, level_position: i32::parse(p)?, cycle: i32::parse(p)?, cycle_position: i32::parse(p)?, voting_period: i32::parse(p)?, voting_period_position: i32::parse(p)?, expected_commitment: bool::parse(p)?})
}
