#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto005PsBabyM1VotingPeriodKind,u8,proto005psbabym1votingperiodkind,{
        0 => Proposal ,
        1 => Testing_vote ,
        2 => Testing ,
        3 => Promotion_vote ,
    }
    );
#[allow(dead_code)]
pub fn proto005psbabym1votingperiodkind_write<U: Target>(val: &Proto005PsBabyM1VotingPeriodKind, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1votingperiodkind_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1VotingPeriodKind> {
    Proto005PsBabyM1VotingPeriodKind::parse(p)
}
