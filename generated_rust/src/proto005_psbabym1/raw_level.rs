#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto005PsBabyM1RawLevel = i32;
#[allow(dead_code)]
pub fn proto005psbabym1rawlevel_write<U: Target>(val: &Proto005PsBabyM1RawLevel, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1rawlevel_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1RawLevel> {
    Ok(i32::parse(p)?)
}
