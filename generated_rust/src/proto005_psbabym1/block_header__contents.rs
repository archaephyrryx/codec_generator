#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1BlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1BlockHeaderContents { pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto005PsBabyM1BlockHeaderAlphaUnsignedContentsSeedNonceHash> }
#[allow(dead_code)]
pub fn proto005psbabym1blockheadercontents_write<U: Target>(val: &Proto005PsBabyM1BlockHeaderContents, buf: &mut U) -> usize {
    u16::write_to(&val.priority, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto005PsBabyM1BlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1blockheadercontents_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1BlockHeaderContents> {
    Ok(Proto005PsBabyM1BlockHeaderContents {priority: u16::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto005PsBabyM1BlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?})
}
