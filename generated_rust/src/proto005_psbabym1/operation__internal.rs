#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Padded,ParseResult,Parser,Target,data,resolve_zero,u30};
data!(Proto005PsBabyM1ContractId,u8,proto005psbabym1contractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto005PsBabyM1ContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1ContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto005PsBabyM1Entrypoint,u8,proto005psbabym1entrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1OperationAlphaInternalOperationDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1OperationAlphaInternalOperationOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1OperationAlphaInternalOperationRevealPublicKey { pub signature_v0_public_key: PublicKey }
data!(Proto005PsBabyM1OperationAlphaInternalOperationRhs,u8,proto005psbabym1operationalphainternaloperationrhs,{
        0 => Reveal  { pub public_key: Proto005PsBabyM1OperationAlphaInternalOperationRevealPublicKey },
        1 => Transaction  { pub amount: ::rust_runtime::N, pub destination: Proto005PsBabyM1ContractId, pub parameters: std::option::Option<Proto005PsBabyM1OperationAlphaInternalOperationTransactionParameters> },
        2 => Origination  { pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto005PsBabyM1OperationAlphaInternalOperationOriginationDelegate>, pub script: Proto005PsBabyM1ScriptedContracts },
        3 => Delegation  { pub delegate: std::option::Option<Proto005PsBabyM1OperationAlphaInternalOperationDelegationDelegate> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1OperationAlphaInternalOperationTransactionParameters { pub entrypoint: Proto005PsBabyM1Entrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1ScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1OperationInternal { pub source: Proto005PsBabyM1ContractId, pub nonce: u16, pub proto005_psbabym1_operation_alpha_internal_operation_rhs: Proto005PsBabyM1OperationAlphaInternalOperationRhs }
#[allow(dead_code)]
pub fn proto005psbabym1operationinternal_write<U: Target>(val: &Proto005PsBabyM1OperationInternal, buf: &mut U) -> usize {
    Proto005PsBabyM1ContractId::write_to(&val.source, buf) + u16::write_to(&val.nonce, buf) + Proto005PsBabyM1OperationAlphaInternalOperationRhs::write_to(&val.proto005_psbabym1_operation_alpha_internal_operation_rhs, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1operationinternal_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1OperationInternal> {
    Ok(Proto005PsBabyM1OperationInternal {source: Proto005PsBabyM1ContractId::parse(p)?, nonce: u16::parse(p)?, proto005_psbabym1_operation_alpha_internal_operation_rhs: Proto005PsBabyM1OperationAlphaInternalOperationRhs::parse(p)?})
}
