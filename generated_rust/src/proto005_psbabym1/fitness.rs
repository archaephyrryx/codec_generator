#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Bytes,Decode,Dynamic,Encode,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
pub type Proto005PsBabyM1Fitness = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>;
#[allow(dead_code)]
pub fn proto005psbabym1fitness_write<U: Target>(val: &Proto005PsBabyM1Fitness, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1fitness_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1Fitness> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>>>::parse(p)?)
}
