#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,i31,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto005PsBabyM1ConstantsFixed { pub proof_of_work_nonce_size: u8, pub nonce_length: u8, pub max_revelations_per_block: u8, pub max_operation_data_length: ::rust_runtime::i31, pub max_proposals_per_delegate: u8 }
#[allow(dead_code)]
pub fn proto005psbabym1constantsfixed_write<U: Target>(val: &Proto005PsBabyM1ConstantsFixed, buf: &mut U) -> usize {
    u8::write_to(&val.proof_of_work_nonce_size, buf) + u8::write_to(&val.nonce_length, buf) + u8::write_to(&val.max_revelations_per_block, buf) + ::rust_runtime::i31::write_to(&val.max_operation_data_length, buf) + u8::write_to(&val.max_proposals_per_delegate, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto005psbabym1constantsfixed_parse<P: Parser>(p: &mut P) -> ParseResult<Proto005PsBabyM1ConstantsFixed> {
    Ok(Proto005PsBabyM1ConstantsFixed {proof_of_work_nonce_size: u8::parse(p)?, nonce_length: u8::parse(p)?, max_revelations_per_block: u8::parse(p)?, max_operation_data_length: ::rust_runtime::i31::parse(p)?, max_proposals_per_delegate: u8::parse(p)?})
}
