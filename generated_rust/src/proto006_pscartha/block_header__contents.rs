#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHABlockHeaderContents { pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash> }
#[allow(dead_code)]
pub fn proto006pscarthablockheadercontents_write<U: Target>(val: &Proto006PsCARTHABlockHeaderContents, buf: &mut U) -> usize {
    u16::write_to(&val.priority, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthablockheadercontents_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHABlockHeaderContents> {
    Ok(Proto006PsCARTHABlockHeaderContents {priority: u16::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?})
}
