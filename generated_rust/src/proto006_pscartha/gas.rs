#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto006PsCARTHAGas,u8,proto006pscarthagas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto006pscarthagas_write<U: Target>(val: &Proto006PsCARTHAGas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthagas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHAGas> {
    Proto006PsCARTHAGas::parse(p)
}
