#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAGasCost { pub allocations: ::rust_runtime::Z, pub steps: ::rust_runtime::Z, pub reads: ::rust_runtime::Z, pub writes: ::rust_runtime::Z, pub bytes_read: ::rust_runtime::Z, pub bytes_written: ::rust_runtime::Z }
#[allow(dead_code)]
pub fn proto006pscarthagascost_write<U: Target>(val: &Proto006PsCARTHAGasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(&val.allocations, buf) + ::rust_runtime::Z::write_to(&val.steps, buf) + ::rust_runtime::Z::write_to(&val.reads, buf) + ::rust_runtime::Z::write_to(&val.writes, buf) + ::rust_runtime::Z::write_to(&val.bytes_read, buf) + ::rust_runtime::Z::write_to(&val.bytes_written, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthagascost_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHAGasCost> {
    Ok(Proto006PsCARTHAGasCost {allocations: ::rust_runtime::Z::parse(p)?, steps: ::rust_runtime::Z::parse(p)?, reads: ::rust_runtime::Z::parse(p)?, writes: ::rust_runtime::Z::parse(p)?, bytes_read: ::rust_runtime::Z::parse(p)?, bytes_written: ::rust_runtime::Z::parse(p)?})
}
