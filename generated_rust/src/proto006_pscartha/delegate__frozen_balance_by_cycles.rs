#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,N,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHADelegateFrozenBalanceByCyclesDenestDynDenestSeq { pub cycle: i32, pub deposit: ::rust_runtime::N, pub fees: ::rust_runtime::N, pub rewards: ::rust_runtime::N }
pub type Proto006PsCARTHADelegateFrozenBalanceByCycles = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto006PsCARTHADelegateFrozenBalanceByCyclesDenestDynDenestSeq>>;
#[allow(dead_code)]
pub fn proto006pscarthadelegatefrozenbalancebycycles_write<U: Target>(val: &Proto006PsCARTHADelegateFrozenBalanceByCycles, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto006PsCARTHADelegateFrozenBalanceByCyclesDenestDynDenestSeq>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthadelegatefrozenbalancebycycles_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHADelegateFrozenBalanceByCycles> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto006PsCARTHADelegateFrozenBalanceByCyclesDenestDynDenestSeq>>::parse(p)?)
}
