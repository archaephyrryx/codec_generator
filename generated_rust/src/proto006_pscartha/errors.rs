#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,ParseResult,Parser,Target,resolve_zero,u30};
pub type Proto006PsCARTHAErrors = ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>;
#[allow(dead_code)]
pub fn proto006pscarthaerrors_write<U: Target>(val: &Proto006PsCARTHAErrors, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthaerrors_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHAErrors> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?)
}
