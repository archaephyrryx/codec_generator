#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto006PsCARTHAVotingPeriodKind,u8,proto006pscarthavotingperiodkind,{
        0 => Proposal ,
        1 => Testing_vote ,
        2 => Testing ,
        3 => Promotion_vote ,
    }
    );
#[allow(dead_code)]
pub fn proto006pscarthavotingperiodkind_write<U: Target>(val: &Proto006PsCARTHAVotingPeriodKind, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthavotingperiodkind_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHAVotingPeriodKind> {
    Proto006PsCARTHAVotingPeriodKind::parse(p)
}
