#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto006PsCARTHARawLevel = i32;
#[allow(dead_code)]
pub fn proto006pscartharawlevel_write<U: Target>(val: &Proto006PsCARTHARawLevel, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscartharawlevel_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHARawLevel> {
    Ok(i32::parse(p)?)
}
