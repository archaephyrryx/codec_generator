#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto006PsCARTHARoll = i32;
#[allow(dead_code)]
pub fn proto006pscartharoll_write<U: Target>(val: &Proto006PsCARTHARoll, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscartharoll_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHARoll> {
    Ok(i32::parse(p)?)
}
