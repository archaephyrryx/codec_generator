#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto006PsCARTHACycle = i32;
#[allow(dead_code)]
pub fn proto006pscarthacycle_write<U: Target>(val: &Proto006PsCARTHACycle, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthacycle_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHACycle> {
    Ok(i32::parse(p)?)
}
