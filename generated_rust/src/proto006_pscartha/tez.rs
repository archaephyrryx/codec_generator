#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,N,ParseResult,Parser,Target,resolve_zero};
pub type Proto006PsCARTHATez = ::rust_runtime::N;
#[allow(dead_code)]
pub fn proto006pscarthatez_write<U: Target>(val: &Proto006PsCARTHATez, buf: &mut U) -> usize {
    ::rust_runtime::N::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthatez_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHATez> {
    Ok(::rust_runtime::N::parse(p)?)
}
