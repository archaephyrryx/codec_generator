#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAVoteBallots { pub yay: i32, pub nay: i32, pub pass: i32 }
#[allow(dead_code)]
pub fn proto006pscarthavoteballots_write<U: Target>(val: &Proto006PsCARTHAVoteBallots, buf: &mut U) -> usize {
    i32::write_to(&val.yay, buf) + i32::write_to(&val.nay, buf) + i32::write_to(&val.pass, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthavoteballots_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHAVoteBallots> {
    Ok(Proto006PsCARTHAVoteBallots {yay: i32::parse(p)?, nay: i32::parse(p)?, pass: i32::parse(p)?})
}
