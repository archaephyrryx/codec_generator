#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Padded,ParseResult,Parser,Target,data,resolve_zero,u30};
data!(Proto006PsCARTHAContractId,u8,proto006pscarthacontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto006PsCARTHAContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto006PsCARTHAEntrypoint,u8,proto006pscarthaentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAOperationAlphaInternalOperationDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAOperationAlphaInternalOperationOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAOperationAlphaInternalOperationRevealPublicKey { pub signature_v0_public_key: PublicKey }
data!(Proto006PsCARTHAOperationAlphaInternalOperationRhs,u8,proto006pscarthaoperationalphainternaloperationrhs,{
        0 => Reveal  { pub public_key: Proto006PsCARTHAOperationAlphaInternalOperationRevealPublicKey },
        1 => Transaction  { pub amount: ::rust_runtime::N, pub destination: Proto006PsCARTHAContractId, pub parameters: std::option::Option<Proto006PsCARTHAOperationAlphaInternalOperationTransactionParameters> },
        2 => Origination  { pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto006PsCARTHAOperationAlphaInternalOperationOriginationDelegate>, pub script: Proto006PsCARTHAScriptedContracts },
        3 => Delegation  { pub delegate: std::option::Option<Proto006PsCARTHAOperationAlphaInternalOperationDelegationDelegate> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAOperationAlphaInternalOperationTransactionParameters { pub entrypoint: Proto006PsCARTHAEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAOperationInternal { pub source: Proto006PsCARTHAContractId, pub nonce: u16, pub proto006_pscartha_operation_alpha_internal_operation_rhs: Proto006PsCARTHAOperationAlphaInternalOperationRhs }
#[allow(dead_code)]
pub fn proto006pscarthaoperationinternal_write<U: Target>(val: &Proto006PsCARTHAOperationInternal, buf: &mut U) -> usize {
    Proto006PsCARTHAContractId::write_to(&val.source, buf) + u16::write_to(&val.nonce, buf) + Proto006PsCARTHAOperationAlphaInternalOperationRhs::write_to(&val.proto006_pscartha_operation_alpha_internal_operation_rhs, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthaoperationinternal_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHAOperationInternal> {
    Ok(Proto006PsCARTHAOperationInternal {source: Proto006PsCARTHAContractId::parse(p)?, nonce: u16::parse(p)?, proto006_pscartha_operation_alpha_internal_operation_rhs: Proto006PsCARTHAOperationAlphaInternalOperationRhs::parse(p)?})
}
