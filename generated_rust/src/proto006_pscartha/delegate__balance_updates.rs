#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(Proto006PsCARTHAContractId,u8,proto006pscarthacontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto006PsCARTHAContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto006PsCARTHAOperationMetadataAlphaBalance,u8,proto006pscarthaoperationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto006PsCARTHAContractId, pub change: i64 },
        1 => Rewards  { pub delegate: Proto006PsCARTHAOperationMetadataAlphaBalanceRewardsDelegate, pub cycle: i32, pub change: i64 },
        2 => Fees  { pub delegate: Proto006PsCARTHAOperationMetadataAlphaBalanceFeesDelegate, pub cycle: i32, pub change: i64 },
        3 => Deposits  { pub delegate: Proto006PsCARTHAOperationMetadataAlphaBalanceDepositsDelegate, pub cycle: i32, pub change: i64 },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAOperationMetadataAlphaBalanceDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAOperationMetadataAlphaBalanceFeesDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAOperationMetadataAlphaBalanceRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto006PsCARTHADelegateBalanceUpdates = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto006PsCARTHAOperationMetadataAlphaBalance>>;
#[allow(dead_code)]
pub fn proto006pscarthadelegatebalanceupdates_write<U: Target>(val: &Proto006PsCARTHADelegateBalanceUpdates, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto006PsCARTHAOperationMetadataAlphaBalance>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthadelegatebalanceupdates_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHADelegateBalanceUpdates> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto006PsCARTHAOperationMetadataAlphaBalance>>::parse(p)?)
}
