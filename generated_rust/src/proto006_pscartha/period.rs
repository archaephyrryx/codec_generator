#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto006PsCARTHAPeriod = i64;
#[allow(dead_code)]
pub fn proto006pscarthaperiod_write<U: Target>(val: &Proto006PsCARTHAPeriod, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthaperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHAPeriod> {
    Ok(i64::parse(p)?)
}
