#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto006PsCARTHATimestamp = i64;
#[allow(dead_code)]
pub fn proto006pscarthatimestamp_write<U: Target>(val: &Proto006PsCARTHATimestamp, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthatimestamp_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHATimestamp> {
    Ok(i64::parse(p)?)
}
