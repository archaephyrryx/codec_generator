#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto006PsCARTHAVotingPeriod = i32;
#[allow(dead_code)]
pub fn proto006pscarthavotingperiod_write<U: Target>(val: &Proto006PsCARTHAVotingPeriod, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthavotingperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHAVotingPeriod> {
    Ok(i32::parse(p)?)
}
