#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHAOperationRaw { pub branch: OperationShellHeaderBranch, pub data: ::rust_runtime::Bytes }
#[allow(dead_code)]
pub fn proto006pscarthaoperationraw_write<U: Target>(val: &Proto006PsCARTHAOperationRaw, buf: &mut U) -> usize {
    OperationShellHeaderBranch::write_to(&val.branch, buf) + ::rust_runtime::Bytes::write_to(&val.data, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthaoperationraw_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHAOperationRaw> {
    Ok(Proto006PsCARTHAOperationRaw {branch: OperationShellHeaderBranch::parse(p)?, data: ::rust_runtime::Bytes::parse(p)?})
}
