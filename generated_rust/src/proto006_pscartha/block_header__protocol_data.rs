#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHABlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto006PsCARTHABlockHeaderProtocolData { pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub signature: Proto006PsCARTHABlockHeaderAlphaSignedContentsSignature }
#[allow(dead_code)]
pub fn proto006pscarthablockheaderprotocoldata_write<U: Target>(val: &Proto006PsCARTHABlockHeaderProtocolData, buf: &mut U) -> usize {
    u16::write_to(&val.priority, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + Proto006PsCARTHABlockHeaderAlphaSignedContentsSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthablockheaderprotocoldata_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHABlockHeaderProtocolData> {
    Ok(Proto006PsCARTHABlockHeaderProtocolData {priority: u16::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, signature: Proto006PsCARTHABlockHeaderAlphaSignedContentsSignature::parse(p)?})
}
