#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto006PsCARTHAVoteBallot = i8;
#[allow(dead_code)]
pub fn proto006pscarthavoteballot_write<U: Target>(val: &Proto006PsCARTHAVoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto006pscarthavoteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto006PsCARTHAVoteBallot> {
    Ok(i8::parse(p)?)
}
