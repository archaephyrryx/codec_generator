#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto014PtKathmaCycle = i32;
#[allow(dead_code)]
pub fn proto014ptkathmacycle_write<U: Target>(val: &Proto014PtKathmaCycle, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmacycle_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaCycle> {
    Ok(i32::parse(p)?)
}
