#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,i31,resolve_zero};
pub type Proto014PtKathmaScriptLoc = ::rust_runtime::i31;
#[allow(dead_code)]
pub fn proto014ptkathmascriptloc_write<U: Target>(val: &Proto014PtKathmaScriptLoc, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmascriptloc_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaScriptLoc> {
    Ok(::rust_runtime::i31::parse(p)?)
}
