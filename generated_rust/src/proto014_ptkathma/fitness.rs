#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto014PtKathmaFitnessLockedRound,u8,proto014ptkathmafitnesslockedround,{
        0 => None ,
        1 => Some (pub i32),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaFitness { pub level: i32, pub locked_round: Proto014PtKathmaFitnessLockedRound, pub predecessor_round: i32, pub round: i32 }
#[allow(dead_code)]
pub fn proto014ptkathmafitness_write<U: Target>(val: &Proto014PtKathmaFitness, buf: &mut U) -> usize {
    i32::write_to(&val.level, buf) + Proto014PtKathmaFitnessLockedRound::write_to(&val.locked_round, buf) + i32::write_to(&val.predecessor_round, buf) + i32::write_to(&val.round, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmafitness_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaFitness> {
    Ok(Proto014PtKathmaFitness {level: i32::parse(p)?, locked_round: Proto014PtKathmaFitnessLockedRound::parse(p)?, predecessor_round: i32::parse(p)?, round: i32::parse(p)?})
}
