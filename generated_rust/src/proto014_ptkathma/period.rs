#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto014PtKathmaPeriod = i64;
#[allow(dead_code)]
pub fn proto014ptkathmaperiod_write<U: Target>(val: &Proto014PtKathmaPeriod, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmaperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaPeriod> {
    Ok(i64::parse(p)?)
}
