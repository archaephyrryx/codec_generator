#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{AutoBox,ByteString,Bytes,Decode,Dynamic,Encode,Estimable,FixSeq,N,Nullable,Padded,ParseResult,Parser,Sequence,Target,Z,cstyle,data,i31,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderMetadata { pub test_chain_status: TestChainStatus, pub max_operations_ttl: ::rust_runtime::i31, pub max_operation_data_length: ::rust_runtime::i31, pub max_block_header_length: ::rust_runtime::i31, pub max_operation_list_length: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<BlockHeaderMetadataMaxOperationListLengthDenestDynDenestDynDenestSeq>>>, pub proposer: Proto014PtKathmaBlockHeaderAlphaMetadataProposer, pub baker: Proto014PtKathmaBlockHeaderAlphaMetadataBaker, pub level_info: Proto014PtKathmaBlockHeaderAlphaMetadataLevelInfo, pub voting_period_info: Proto014PtKathmaBlockHeaderAlphaMetadataVotingPeriodInfo, pub nonce_hash: Proto014PtKathmaBlockHeaderAlphaMetadataNonceHash, pub deactivated: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaBlockHeaderAlphaMetadataDeactivatedDenestDynDenestSeq>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub liquidity_baking_toggle_ema: i32, pub implicit_operations_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResult>>, pub consumed_milligas: ::rust_runtime::N, pub dal_slot_availability: ::rust_runtime::Nullable<::rust_runtime::Z> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderMetadataMaxOperationListLengthDenestDynDenestDynDenestSeq { pub max_size: ::rust_runtime::i31, pub max_op: std::option::Option<::rust_runtime::i31> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct ChainId { pub chain_id: ::rust_runtime::ByteString<4> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Hash { pub block_hash: ::rust_runtime::ByteString<32> }
data!(InodeTree,u8,inodetree,{
        0 => Blinded_inode  { pub blinded_inode: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0BlindedInodeBlindedInode },
        1 => Inode_values  { pub inode_values: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeValuesInodeValuesDenestDynDenestSeq>> },
        2 => Inode_tree  { pub inode_tree: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTree },
        3 => Inode_extender  { pub inode_extender: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeExtenderInodeExtender },
        4 => None ,
    }
    );
data!(MichelineProto014PtKathmaMichelsonV1Expression,u8,michelineproto014ptkathmamichelsonv1expression,{
        0 => Int  { pub int: ::rust_runtime::Z },
        1 => String  { pub string: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        2 => Sequence (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>>>),
        3 => Prim__no_args__no_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives },
        4 => Prim__no_args__some_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        5 => Prim__1_arg__no_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression> },
        6 => Prim__1_arg__some_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        7 => Prim__2_args__no_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression> },
        8 => Prim__2_args__some_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        9 => Prim__generic  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub args: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>>>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        10 => Bytes  { pub bytes: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Operation { pub chain_id: OperationChainId, pub hash: OperationHash, pub operation_rhs: OperationRhs }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationChainId { pub chain_id: ::rust_runtime::ByteString<4> }
data!(OperationDenestDyn,u8,operationdenestdyn,{
        0 => Operation_with_too_large_metadata  { pub contents: ::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContents>, pub signature: Proto014PtKathmaOperationAlphaContentsAndSignatureSignature },
        1 => Operation_without_metadata  { pub contents: ::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContents>, pub signature: Proto014PtKathmaOperationAlphaContentsAndSignatureSignature },
        2 => Operation_with_metadata (pub Proto014PtKathmaOperationAlphaOperationWithMetadata),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationHash { pub operation_hash: ::rust_runtime::ByteString<32> }
pub type OperationIndex0 = ::rust_runtime::Dynamic<::rust_runtime::u30,OperationShellHeader>;
pub type OperationIndex1 = ::rust_runtime::Dynamic<::rust_runtime::u30,OperationDenestDyn>;
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationRhs(pub OperationIndex0,pub OperationIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeader { pub branch: OperationShellHeaderBranch }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaApplyInternalResultsAlphaOperationResult,u8,proto014ptkathmaapplyinternalresultsalphaoperationresult,{
        1 => transaction  { pub source: Proto014PtKathmaContractId, pub nonce: u16, pub amount: ::rust_runtime::N, pub destination: Proto014PtKathmaTransactionDestination, pub parameters: std::option::Option<Proto014PtKathmaApplyInternalResultsAlphaOperationResultTransactionParameters>, pub result: Proto014PtKathmaOperationAlphaInternalOperationResultTransaction },
        2 => origination  { pub source: Proto014PtKathmaContractId, pub nonce: u16, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto014PtKathmaApplyInternalResultsAlphaOperationResultOriginationDelegate>, pub script: Proto014PtKathmaScriptedContracts, pub result: Proto014PtKathmaOperationAlphaInternalOperationResultOrigination },
        3 => delegation  { pub source: Proto014PtKathmaContractId, pub nonce: u16, pub delegate: std::option::Option<Proto014PtKathmaApplyInternalResultsAlphaOperationResultDelegationDelegate>, pub result: Proto014PtKathmaOperationAlphaInternalOperationResultDelegation },
        4 => event  { pub source: Proto014PtKathmaContractId, pub nonce: u16, pub r#type: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub tag: std::option::Option<Proto014PtKathmaEntrypoint>, pub payload: std::option::Option<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>>, pub result: Proto014PtKathmaOperationAlphaInternalOperationResultEvent },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaApplyInternalResultsAlphaOperationResultDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaApplyInternalResultsAlphaOperationResultOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaApplyInternalResultsAlphaOperationResultTransactionParameters { pub entrypoint: Proto014PtKathmaEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaFullHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub payload_hash: Proto014PtKathmaBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto014PtKathmaBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: Proto014PtKathmaBlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaMetadataBaker { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaMetadataDeactivatedDenestDynDenestSeq { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaMetadataLevelInfo { pub level: i32, pub level_position: i32, pub cycle: i32, pub cycle_position: i32, pub expected_commitment: bool }
data!(Proto014PtKathmaBlockHeaderAlphaMetadataNonceHash,u8,proto014ptkathmablockheaderalphametadatanoncehash,{
        0 => None ,
        1 => Some  { pub cycle_nonce: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaMetadataProposer { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaMetadataVotingPeriodInfo { pub voting_period: Proto014PtKathmaBlockHeaderAlphaMetadataVotingPeriodInfoVotingPeriod, pub position: i32, pub remaining: i32 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaMetadataVotingPeriodInfoVotingPeriod { pub index: i32, pub kind: Proto014PtKathmaBlockHeaderAlphaMetadataVotingPeriodInfoVotingPeriodKind, pub start_position: i32 }
data!(Proto014PtKathmaBlockHeaderAlphaMetadataVotingPeriodInfoVotingPeriodKind,u8,proto014ptkathmablockheaderalphametadatavotingperiodinfovotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaBondId,u8,proto014ptkathmabondid,{
        0 => Tx_rollup_bond_id  { pub tx_rollup: Proto014PtKathmaTxRollupId },
        1 => Sc_rollup_bond_id  { pub sc_rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
    }
    );
data!(Proto014PtKathmaContractId,u8,proto014ptkathmacontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto014PtKathmaContractIdOriginatedDenestPad,1>),
    }
    );
data!(Proto014PtKathmaContractIdOriginated,u8,proto014ptkathmacontractidoriginated,{
        1 => Originated (pub ::rust_runtime::Padded<Proto014PtKathmaContractIdOriginatedOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaContractIdOriginatedOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto014PtKathmaEntrypoint,u8,proto014ptkathmaentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedEndorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto014PtKathmaInlinedEndorsementMempoolContents, pub signature: ::rust_runtime::Nullable<Proto014PtKathmaInlinedEndorsementSignature> }
data!(Proto014PtKathmaInlinedEndorsementMempoolContents,u8,proto014ptkathmainlinedendorsementmempoolcontents,{
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto014PtKathmaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedEndorsementSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedPreendorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto014PtKathmaInlinedPreendorsementContents, pub signature: ::rust_runtime::Nullable<Proto014PtKathmaInlinedPreendorsementSignature> }
data!(Proto014PtKathmaInlinedPreendorsementContents,u8,proto014ptkathmainlinedpreendorsementcontents,{
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto014PtKathmaInlinedPreendorsementContentsPreendorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedPreendorsementContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedPreendorsementSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
data!(Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq,u8,proto014ptkathmalazystoragediffdenestdyndenestseq,{
        0 => big_map  { pub id: ::rust_runtime::Z, pub diff: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff },
        1 => sapling_state  { pub id: ::rust_runtime::Z, pub diff: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff },
    }
    );
data!(Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff,u8,proto014ptkathmalazystoragediffdenestdyndenestseqbigmapdiff,{
        0 => update  { pub updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq>> },
        1 => remove ,
        2 => copy  { pub source: ::rust_runtime::Z, pub updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq>> },
        3 => alloc  { pub updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq>>, pub key_type: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub value_type: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq { pub key_hash: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash, pub key: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub value: std::option::Option<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq { pub key_hash: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash, pub key: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub value: std::option::Option<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq { pub key_hash: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash, pub key: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub value: std::option::Option<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash { pub script_expr: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff,u8,proto014ptkathmalazystoragediffdenestdyndenestseqsaplingstatediff,{
        0 => update  { pub updates: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates },
        1 => remove ,
        2 => copy  { pub source: ::rust_runtime::Z, pub updates: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates },
        3 => alloc  { pub updates: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates, pub memo_size: u16 },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates { pub commitments_and_ciphertexts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>>, pub nullifiers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq(pub ::rust_runtime::ByteString<32>,pub SaplingTransactionCiphertext);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates { pub commitments_and_ciphertexts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>>, pub nullifiers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq(pub ::rust_runtime::ByteString<32>,pub SaplingTransactionCiphertext);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates { pub commitments_and_ciphertexts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>>, pub nullifiers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq(pub ::rust_runtime::ByteString<32>,pub SaplingTransactionCiphertext);
cstyle!(Proto014PtKathmaMichelsonV1Primitives,u8,{
        parameter = 0,
        storage = 1,
        code = 2,
        False = 3,
        Elt = 4,
        Left = 5,
        None = 6,
        Pair = 7,
        Right = 8,
        Some = 9,
        True = 10,
        Unit = 11,
        PACK = 12,
        UNPACK = 13,
        BLAKE2B = 14,
        SHA256 = 15,
        SHA512 = 16,
        ABS = 17,
        ADD = 18,
        AMOUNT = 19,
        AND = 20,
        BALANCE = 21,
        CAR = 22,
        CDR = 23,
        CHECK_SIGNATURE = 24,
        COMPARE = 25,
        CONCAT = 26,
        CONS = 27,
        CREATE_ACCOUNT = 28,
        CREATE_CONTRACT = 29,
        IMPLICIT_ACCOUNT = 30,
        DIP = 31,
        DROP = 32,
        DUP = 33,
        EDIV = 34,
        EMPTY_MAP = 35,
        EMPTY_SET = 36,
        EQ = 37,
        EXEC = 38,
        FAILWITH = 39,
        GE = 40,
        GET = 41,
        GT = 42,
        HASH_KEY = 43,
        IF = 44,
        IF_CONS = 45,
        IF_LEFT = 46,
        IF_NONE = 47,
        INT = 48,
        LAMBDA = 49,
        LE = 50,
        LEFT = 51,
        LOOP = 52,
        LSL = 53,
        LSR = 54,
        LT = 55,
        MAP = 56,
        MEM = 57,
        MUL = 58,
        NEG = 59,
        NEQ = 60,
        NIL = 61,
        NONE = 62,
        NOT = 63,
        NOW = 64,
        OR = 65,
        PAIR = 66,
        PUSH = 67,
        RIGHT = 68,
        SIZE = 69,
        SOME = 70,
        SOURCE = 71,
        SENDER = 72,
        SELF = 73,
        STEPS_TO_QUOTA = 74,
        SUB = 75,
        SWAP = 76,
        TRANSFER_TOKENS = 77,
        SET_DELEGATE = 78,
        UNIT = 79,
        UPDATE = 80,
        XOR = 81,
        ITER = 82,
        LOOP_LEFT = 83,
        ADDRESS = 84,
        CONTRACT = 85,
        ISNAT = 86,
        CAST = 87,
        RENAME = 88,
        bool = 89,
        contract = 90,
        int = 91,
        key = 92,
        key_hash = 93,
        lambda = 94,
        list = 95,
        map = 96,
        big_map = 97,
        nat = 98,
        option = 99,
        or = 100,
        pair = 101,
        set = 102,
        signature = 103,
        string = 104,
        bytes = 105,
        mutez = 106,
        timestamp = 107,
        unit = 108,
        operation = 109,
        address = 110,
        SLICE = 111,
        DIG = 112,
        DUG = 113,
        EMPTY_BIG_MAP = 114,
        APPLY = 115,
        chain_id = 116,
        CHAIN_ID = 117,
        LEVEL = 118,
        SELF_ADDRESS = 119,
        never = 120,
        NEVER = 121,
        UNPAIR = 122,
        VOTING_POWER = 123,
        TOTAL_VOTING_POWER = 124,
        KECCAK = 125,
        SHA3 = 126,
        PAIRING_CHECK = 127,
        bls12_381_g1 = 128,
        bls12_381_g2 = 129,
        bls12_381_fr = 130,
        sapling_state = 131,
        sapling_transaction_deprecated = 132,
        SAPLING_EMPTY_STATE = 133,
        SAPLING_VERIFY_UPDATE = 134,
        ticket = 135,
        TICKET = 136,
        READ_TICKET = 137,
        SPLIT_TICKET = 138,
        JOIN_TICKETS = 139,
        GET_AND_UPDATE = 140,
        chest = 141,
        chest_key = 142,
        OPEN_CHEST = 143,
        VIEW = 144,
        view = 145,
        constant = 146,
        SUB_MUTEZ = 147,
        tx_rollup_l2_address = 148,
        MIN_BLOCK_TIME = 149,
        sapling_transaction = 150,
        EMIT = 151
    }
    );
data!(Proto014PtKathmaOperationAlphaContents,u8,proto014ptkathmaoperationalphacontents,{
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32> },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedEndorsement> },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaBlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaBlockHeaderAlphaFullHeader> },
        4 => Activate_account  { pub pkh: Proto014PtKathmaOperationAlphaContentsActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20> },
        5 => Proposals  { pub source: Proto014PtKathmaOperationAlphaContentsProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>> },
        6 => Ballot  { pub source: Proto014PtKathmaOperationAlphaContentsBallotSource, pub period: i32, pub proposal: Proto014PtKathmaOperationAlphaContentsBallotProposal, pub ballot: i8 },
        7 => Double_preendorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedPreendorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedPreendorsement> },
        8 => Vdf_revelation  { pub solution: Proto014PtKathmaOperationAlphaContentsVdfRevelationSolution },
        17 => Failing_noop  { pub arbitrary: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto014PtKathmaOperationAlphaContentsPreendorsementBlockPayloadHash },
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto014PtKathmaOperationAlphaContentsEndorsementBlockPayloadHash },
        22 => Dal_slot_availability  { pub endorser: Proto014PtKathmaOperationAlphaContentsDalSlotAvailabilityEndorser, pub endorsement: ::rust_runtime::Z },
        107 => Reveal  { pub source: Proto014PtKathmaOperationAlphaContentsRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: Proto014PtKathmaOperationAlphaContentsRevealPublicKey },
        108 => Transaction  { pub source: Proto014PtKathmaOperationAlphaContentsTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: Proto014PtKathmaContractId, pub parameters: std::option::Option<Proto014PtKathmaOperationAlphaContentsTransactionParameters> },
        109 => Origination  { pub source: Proto014PtKathmaOperationAlphaContentsOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto014PtKathmaOperationAlphaContentsOriginationDelegate>, pub script: Proto014PtKathmaScriptedContracts },
        110 => Delegation  { pub source: Proto014PtKathmaOperationAlphaContentsDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<Proto014PtKathmaOperationAlphaContentsDelegationDelegate> },
        111 => Register_global_constant  { pub source: Proto014PtKathmaOperationAlphaContentsRegisterGlobalConstantSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        112 => Set_deposits_limit  { pub source: Proto014PtKathmaOperationAlphaContentsSetDepositsLimitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub limit: std::option::Option<::rust_runtime::N> },
        113 => Increase_paid_storage  { pub source: Proto014PtKathmaOperationAlphaContentsIncreasePaidStorageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::Z, pub destination: Proto014PtKathmaContractIdOriginated },
        150 => Tx_rollup_origination  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N },
        151 => Tx_rollup_submit_batch  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupSubmitBatchSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub content: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub burn_limit: std::option::Option<::rust_runtime::N> },
        152 => Tx_rollup_commit  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupCommitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub commitment: Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitment },
        153 => Tx_rollup_return_bond  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupReturnBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId },
        154 => Tx_rollup_finalize_commitment  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupFinalizeCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId },
        155 => Tx_rollup_remove_commitment  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupRemoveCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId },
        156 => Tx_rollup_rejection  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub level: i32, pub message: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessage, pub message_position: ::rust_runtime::N, pub message_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq>>, pub message_result_hash: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageResultHash, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq>>, pub previous_message_result: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResult, pub previous_message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>>, pub proof: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProof },
        157 => Tx_rollup_dispatch_tickets  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub tx_rollup: Proto014PtKathmaTxRollupId, pub level: i32, pub context_hash: Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsContextHash, pub message_index: ::rust_runtime::i31, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>>, pub tickets_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>> },
        158 => Transfer_ticket  { pub source: Proto014PtKathmaOperationAlphaContentsTransferTicketSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub ticket_contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ticketer: Proto014PtKathmaContractId, pub ticket_amount: ::rust_runtime::N, pub destination: Proto014PtKathmaContractId, pub entrypoint: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        200 => Sc_rollup_originate  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupOriginateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub kind: Proto014PtKathmaOperationAlphaContentsScRollupOriginateKind, pub boot_sector: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub parameters_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        201 => Sc_rollup_add_messages  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupAddMessagesSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        202 => Sc_rollup_cement  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupCementSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub commitment: Proto014PtKathmaOperationAlphaContentsScRollupCementCommitment },
        203 => Sc_rollup_publish  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub commitment: Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitment },
        204 => Sc_rollup_refute  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupRefuteSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub opponent: Proto014PtKathmaOperationAlphaContentsScRollupRefuteOpponent, pub refutation: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutation, pub is_opening_move: bool },
        205 => Sc_rollup_timeout  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupTimeoutSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub stakers: Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakers },
        206 => Sc_rollup_execute_outbox_message  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupExecuteOutboxMessageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub cemented_commitment: Proto014PtKathmaOperationAlphaContentsScRollupExecuteOutboxMessageCementedCommitment, pub outbox_level: i32, pub message_index: ::rust_runtime::i31, pub inclusion_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        207 => Sc_rollup_recover_bond  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupRecoverBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaOperationAlphaContentsScRollupRecoverBondRollup },
        208 => Sc_rollup_dal_slot_subscribe  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupDalSlotSubscribeSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub slot_index: u8 },
        230 => Dal_publish_slot_header  { pub source: Proto014PtKathmaOperationAlphaContentsDalPublishSlotHeaderSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub slot: Proto014PtKathmaOperationAlphaContentsDalPublishSlotHeaderSlot },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsAndSignatureSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsBallotSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsDalPublishSlotHeaderSlot { pub level: i32, pub index: u8, pub header: ::rust_runtime::i31 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsDalPublishSlotHeaderSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsDalSlotAvailabilityEndorser { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsDelegationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsIncreasePaidStorageSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsProposalsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsRegisterGlobalConstantSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsRevealPublicKey { pub signature_v0_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsRevealSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupAddMessagesSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupCementCommitment { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupCementSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupDalSlotSubscribeSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupExecuteOutboxMessageCementedCommitment { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupExecuteOutboxMessageSource { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationAlphaContentsScRollupOriginateKind,u16,proto014ptkathmaoperationalphacontentsscrolluporiginatekind,{
        0 => Example_arith_smart_contract_rollup_kind ,
        1 => Wasm_2proto0proto0_smart_contract_rollup_kind ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupOriginateSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitment { pub compressed_state: Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitmentCompressedState, pub inbox_level: i32, pub predecessor: Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitmentPredecessor, pub number_of_messages: i32, pub number_of_ticks: i32 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitmentCompressedState { pub state_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitmentPredecessor { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupPublishSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRecoverBondRollup { pub sc_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRecoverBondSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteOpponent { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutation { pub choice: ::rust_runtime::N, pub step: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStep }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStep,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstep,{
        0 => Dissection (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepDissectionDenestDynDenestSeq>>),
        1 => Proof  { pub pvm_step: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStep, pub inbox: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInbox },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepDissectionDenestDynDenestSeq(pub Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepDissectionDenestDynDenestSeqIndex0,pub ::rust_runtime::N);
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepDissectionDenestDynDenestSeqIndex0,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepdissectiondenestdyndenestseqindex0,{
        0 => None ,
        1 => Some  { pub state_hash: ::rust_runtime::ByteString<32> },
    }
    );
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInbox,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofinbox,{
        0 => None ,
        1 => Some  { pub skips: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeq>>, pub level: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevel, pub inc: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeq>>, pub message_proof: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProof },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeq { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevel { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelCurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelCurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProof { pub version: i16, pub before: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBefore, pub after: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfter, pub state: ::rust_runtime::AutoBox<TreeEncoding> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfter,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofinboxsomemessageproofafter,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBefore,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofinboxsomemessageproofbefore,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeq(pub Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeq>>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0 { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0CurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0CurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeq { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStep,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmstep,{
        0 => Arithmetic_PVM_with_proof  { pub tree_proof: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProof, pub given: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofGiven, pub requested: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofRequested },
        1 => Wasm_2proto0proto0_PVM_with_proof  { pub tree_proof: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProof, pub given: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofGiven, pub requested: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofRequested },
    }
    );
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofGiven,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithproofgiven,{
        0 => None ,
        1 => Some  { pub inbox_level: i32, pub message_counter: ::rust_runtime::N, pub payload: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
    }
    );
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofRequested,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithproofrequested,{
        0 => No_input_required ,
        1 => Initial ,
        2 => First_after (pub i32,pub ::rust_runtime::N),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProof { pub version: i16, pub before: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBefore, pub after: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfter, pub state: TreeEncoding }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfter,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithprooftreeproofafter,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBefore,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithprooftreeproofbefore,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateBlindedNodeBlindedNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateBlindedValueBlindedValue { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateExtenderExtender { pub length: i64, pub segment: ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>, pub proof: ::rust_runtime::AutoBox<InodeTree> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInode { pub length: i64, pub proofs: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofs }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofs,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithprooftreeproofstateinodeinodeproofs,{
        0 => sparse_proof  { pub sparse_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeq>> },
        1 => dense_proof  { pub dense_proof: ::rust_runtime::FixSeq<::rust_runtime::AutoBox<InodeTree>,32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeq(pub u8,pub InodeTree);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0BlindedInodeBlindedInode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeExtenderInodeExtender { pub length: i64, pub segment: ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>, pub proof: ::rust_runtime::AutoBox<InodeTree> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTree { pub length: i64, pub proofs: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTreeProofs }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTreeProofs,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithprooftreeproofstateinodeinodeproofssparseproofsparseproofdenestdyndenestseqindex1index0inodetreeinodetreeproofs,{
        0 => sparse_proof  { pub sparse_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTreeProofsSparseProofSparseProofDenestDynDenestSeq>> },
        1 => dense_proof  { pub dense_proof: ::rust_runtime::FixSeq<::rust_runtime::AutoBox<InodeTree>,32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTreeProofsSparseProofSparseProofDenestDynDenestSeq(pub u8,pub ::rust_runtime::AutoBox<InodeTree>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeValuesInodeValuesDenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub ::rust_runtime::AutoBox<TreeEncoding>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateNodeNodeDenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub ::rust_runtime::AutoBox<TreeEncoding>);
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofGiven,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithproofgiven,{
        0 => None ,
        1 => Some  { pub inbox_level: i32, pub message_counter: ::rust_runtime::N, pub payload: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
    }
    );
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofRequested,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithproofrequested,{
        0 => No_input_required ,
        1 => Initial ,
        2 => First_after (pub i32,pub ::rust_runtime::N),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProof { pub version: i16, pub before: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBefore, pub after: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfter, pub state: ::rust_runtime::AutoBox<TreeEncoding> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfter,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithprooftreeproofafter,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBefore,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithprooftreeproofbefore,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupTimeoutSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakers { pub alice: Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakersAlice, pub bob: Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakersBob }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakersAlice { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakersBob { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsSetDepositsLimitSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTransactionParameters { pub entrypoint: Proto014PtKathmaEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTransactionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTransferTicketSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitment { pub level: i32, pub messages: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq>>, pub predecessor: Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentPredecessor, pub inbox_merkle_root: Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq { pub message_result_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentPredecessor,u8,proto014ptkathmaoperationalphacontentstxrollupcommitcommitmentpredecessor,{
        0 => None ,
        1 => Some  { pub commitment_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupCommitSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq { pub contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticketer: Proto014PtKathmaContractId, pub amount: Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount, pub claimer: Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,u8,proto014ptkathmaoperationalphacontentstxrollupdispatchticketsticketsinfodenestdyndenestseqamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupFinalizeCommitmentSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessage,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionmessage,{
        0 => Batch  { pub batch: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => Deposit  { pub deposit: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDeposit },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDeposit { pub sender: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender, pub destination: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination, pub ticket_hash: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash, pub amount: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionmessagedepositdepositamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination { pub tx_rollup_l2_address: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageResultHash { pub message_result_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResult { pub context_hash: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash, pub withdraw_list_hash: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash { pub withdraw_list_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProof,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproof,{
        0 => case_0 (pub i16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index1,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq>>),
        1 => case_1 (pub i16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index1,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq>>),
        2 => case_2 (pub i16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index1,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq>>),
        3 => case_3 (pub i16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index1,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq>>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRemoveCommitmentSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupReturnBondSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupSubmitBatchSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsVdfRevelationSolution(pub ::rust_runtime::ByteString<100>,pub ::rust_runtime::ByteString<100>);
data!(Proto014PtKathmaOperationAlphaInternalOperationResultDelegation,u8,proto014ptkathmaoperationalphainternaloperationresultdelegation,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaInternalOperationResultEvent,u8,proto014ptkathmaoperationalphainternaloperationresultevent,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaInternalOperationResultOrigination,u8,proto014ptkathmaoperationalphainternaloperationresultorigination,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub originated_contracts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaContractIdOriginated>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub paid_storage_size_diff: ::rust_runtime::Z, pub lazy_storage_diff: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>> },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub originated_contracts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaContractIdOriginated>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub paid_storage_size_diff: ::rust_runtime::Z, pub lazy_storage_diff: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>> },
    }
    );
data!(Proto014PtKathmaOperationAlphaInternalOperationResultTransaction,u8,proto014ptkathmaoperationalphainternaloperationresulttransaction,{
        0 => Applied  { pub proto014_ptkathma_operation_alpha_internal_operation_result_transaction_applied_rhs: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedRhs },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub proto014_ptkathma_operation_alpha_internal_operation_result_transaction_backtracked_rhs: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedRhs },
    }
    );
data!(Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedRhs,u8,proto014ptkathmaoperationalphainternaloperationresulttransactionappliedrhs,{
        0 => To_contract  { pub storage: std::option::Option<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub originated_contracts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaContractIdOriginated>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub paid_storage_size_diff: ::rust_runtime::Z, pub allocated_destination_contract: bool, pub lazy_storage_diff: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>> },
        1 => To_tx_rollup  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub ticket_hash: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToTxRollupTicketHash, pub paid_storage_size_diff: ::rust_runtime::N },
        2 => To_sc_rollup  { pub consumed_milligas: ::rust_runtime::N, pub inbox_after: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToScRollupInboxAfter },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToScRollupInboxAfter { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToScRollupInboxAfterCurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToScRollupInboxAfterCurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionAppliedToTxRollupTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedRhs,u8,proto014ptkathmaoperationalphainternaloperationresulttransactionbacktrackedrhs,{
        0 => To_contract  { pub storage: std::option::Option<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub originated_contracts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaContractIdOriginated>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub paid_storage_size_diff: ::rust_runtime::Z, pub allocated_destination_contract: bool, pub lazy_storage_diff: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>> },
        1 => To_tx_rollup  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub ticket_hash: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToTxRollupTicketHash, pub paid_storage_size_diff: ::rust_runtime::N },
        2 => To_sc_rollup  { pub consumed_milligas: ::rust_runtime::N, pub inbox_after: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToScRollupInboxAfter },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToScRollupInboxAfter { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToScRollupInboxAfterCurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToScRollupInboxAfterCurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaInternalOperationResultTransactionBacktrackedToTxRollupTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResult,u8,proto014ptkathmaoperationalphaoperationcontentsandresult,{
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultSeedNonceRevelationMetadata },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedEndorsement>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultDoubleEndorsementEvidenceMetadata },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaBlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaBlockHeaderAlphaFullHeader>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultDoubleBakingEvidenceMetadata },
        4 => Activate_account  { pub pkh: Proto014PtKathmaOperationAlphaOperationContentsAndResultActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultActivateAccountMetadata },
        5 => Proposals  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultProposalsProposalsDenestDynDenestSeq>> },
        6 => Ballot  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultBallotSource, pub period: i32, pub proposal: Proto014PtKathmaOperationAlphaOperationContentsAndResultBallotProposal, pub ballot: i8 },
        7 => Double_preendorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedPreendorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedPreendorsement>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultDoublePreendorsementEvidenceMetadata },
        8 => Vdf_revelation  { pub solution: Proto014PtKathmaOperationAlphaOperationContentsAndResultVdfRevelationSolution, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultVdfRevelationMetadata },
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto014PtKathmaOperationAlphaOperationContentsAndResultPreendorsementBlockPayloadHash, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultPreendorsementMetadata },
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto014PtKathmaOperationAlphaOperationContentsAndResultEndorsementBlockPayloadHash, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultEndorsementMetadata },
        22 => Dal_slot_availability  { pub endorser: Proto014PtKathmaOperationAlphaOperationContentsAndResultDalSlotAvailabilityEndorser, pub endorsement: ::rust_runtime::Z, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultDalSlotAvailabilityMetadata },
        107 => Reveal  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: Proto014PtKathmaOperationAlphaOperationContentsAndResultRevealPublicKey, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultRevealMetadata },
        108 => Transaction  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: Proto014PtKathmaContractId, pub parameters: std::option::Option<Proto014PtKathmaOperationAlphaOperationContentsAndResultTransactionParameters>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultTransactionMetadata },
        109 => Origination  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto014PtKathmaOperationAlphaOperationContentsAndResultOriginationDelegate>, pub script: Proto014PtKathmaScriptedContracts, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultOriginationMetadata },
        110 => Delegation  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<Proto014PtKathmaOperationAlphaOperationContentsAndResultDelegationDelegate>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultDelegationMetadata },
        111 => Register_global_constant  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultRegisterGlobalConstantSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultRegisterGlobalConstantMetadata },
        112 => Set_deposits_limit  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultSetDepositsLimitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub limit: std::option::Option<::rust_runtime::N>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultSetDepositsLimitMetadata },
        113 => Increase_paid_storage  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultIncreasePaidStorageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::Z, pub destination: Proto014PtKathmaContractIdOriginated, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultIncreasePaidStorageMetadata },
        150 => Tx_rollup_origination  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupOriginationMetadata },
        151 => Tx_rollup_submit_batch  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupSubmitBatchSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub content: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub burn_limit: std::option::Option<::rust_runtime::N>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupSubmitBatchMetadata },
        152 => Tx_rollup_commit  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub commitment: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitCommitment, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitMetadata },
        153 => Tx_rollup_return_bond  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupReturnBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupReturnBondMetadata },
        154 => Tx_rollup_finalize_commitment  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupFinalizeCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupFinalizeCommitmentMetadata },
        155 => Tx_rollup_remove_commitment  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRemoveCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRemoveCommitmentMetadata },
        156 => Tx_rollup_rejection  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub level: i32, pub message: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessage, pub message_position: ::rust_runtime::N, pub message_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessagePathDenestDynDenestSeq>>, pub message_result_hash: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageResultHash, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageResultPathDenestDynDenestSeq>>, pub previous_message_result: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionPreviousMessageResult, pub previous_message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>>, pub proof: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProof, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMetadata },
        157 => Tx_rollup_dispatch_tickets  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub tx_rollup: Proto014PtKathmaTxRollupId, pub level: i32, pub context_hash: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsContextHash, pub message_index: ::rust_runtime::i31, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>>, pub tickets_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsMetadata },
        158 => Transfer_ticket  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultTransferTicketSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub ticket_contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ticketer: Proto014PtKathmaContractId, pub ticket_amount: ::rust_runtime::N, pub destination: Proto014PtKathmaContractId, pub entrypoint: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultTransferTicketMetadata },
        200 => Sc_rollup_originate  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupOriginateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub kind: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupOriginateKind, pub boot_sector: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub parameters_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupOriginateMetadata },
        201 => Sc_rollup_add_messages  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupAddMessagesSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupAddMessagesMetadata },
        202 => Sc_rollup_cement  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupCementSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub commitment: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupCementCommitment, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupCementMetadata },
        203 => Sc_rollup_publish  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub commitment: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupPublishCommitment, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupPublishMetadata },
        204 => Sc_rollup_refute  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub opponent: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteOpponent, pub refutation: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutation, pub is_opening_move: bool, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteMetadata },
        205 => Sc_rollup_timeout  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupTimeoutSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub stakers: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupTimeoutStakers, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupTimeoutMetadata },
        206 => Sc_rollup_execute_outbox_message  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupExecuteOutboxMessageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub cemented_commitment: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupExecuteOutboxMessageCementedCommitment, pub outbox_level: i32, pub message_index: ::rust_runtime::i31, pub inclusion_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupExecuteOutboxMessageMetadata },
        207 => Sc_rollup_recover_bond  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRecoverBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRecoverBondRollup, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRecoverBondMetadata },
        208 => Sc_rollup_dal_slot_subscribe  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupDalSlotSubscribeSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub slot_index: u8, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupDalSlotSubscribeMetadata },
        230 => Dal_publish_slot_header  { pub source: Proto014PtKathmaOperationAlphaOperationContentsAndResultDalPublishSlotHeaderSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub slot: Proto014PtKathmaOperationAlphaOperationContentsAndResultDalPublishSlotHeaderSlot, pub metadata: Proto014PtKathmaOperationAlphaOperationContentsAndResultDalPublishSlotHeaderMetadata },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultActivateAccountMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultBallotSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDalPublishSlotHeaderMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultDalPublishSlotHeader, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDalPublishSlotHeaderSlot { pub level: i32, pub index: u8, pub header: ::rust_runtime::i31 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDalPublishSlotHeaderSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDalSlotAvailabilityEndorser { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDalSlotAvailabilityMetadata { pub delegate: Proto014PtKathmaOperationAlphaOperationContentsAndResultDalSlotAvailabilityMetadataDelegate }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDalSlotAvailabilityMetadataDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDelegationMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultDelegation, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDelegationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDoubleBakingEvidenceMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDoubleEndorsementEvidenceMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultDoublePreendorsementEvidenceMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultEndorsementMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub delegate: Proto014PtKathmaOperationAlphaOperationContentsAndResultEndorsementMetadataDelegate, pub endorsement_power: ::rust_runtime::i31 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultEndorsementMetadataDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultIncreasePaidStorageMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultIncreasePaidStorage, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultIncreasePaidStorageSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultOriginationMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultOrigination, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultPreendorsementMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub delegate: Proto014PtKathmaOperationAlphaOperationContentsAndResultPreendorsementMetadataDelegate, pub preendorsement_power: ::rust_runtime::i31 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultPreendorsementMetadataDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultProposalsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultRegisterGlobalConstantMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultRegisterGlobalConstant, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultRegisterGlobalConstantSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultRevealMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultReveal, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultRevealPublicKey { pub signature_v0_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultRevealSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupAddMessagesMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessages, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupAddMessagesSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupCementCommitment { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupCementMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultScRollupCement, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupCementSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupDalSlotSubscribeMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultScRollupDalSlotSubscribe, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupDalSlotSubscribeSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupExecuteOutboxMessageCementedCommitment { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupExecuteOutboxMessageMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultScRollupExecuteOutboxMessage, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupExecuteOutboxMessageSource { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupOriginateKind,u16,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluporiginatekind,{
        0 => Example_arith_smart_contract_rollup_kind ,
        1 => Wasm_2proto0proto0_smart_contract_rollup_kind ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupOriginateMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultScRollupOriginate, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupOriginateSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupPublishCommitment { pub compressed_state: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupPublishCommitmentCompressedState, pub inbox_level: i32, pub predecessor: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupPublishCommitmentPredecessor, pub number_of_messages: i32, pub number_of_ticks: i32 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupPublishCommitmentCompressedState { pub state_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupPublishCommitmentPredecessor { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupPublishMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultScRollupPublish, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupPublishSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRecoverBondMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultScRollupRecoverBond, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRecoverBondRollup { pub sc_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRecoverBondSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultScRollupRefute, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteOpponent { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutation { pub choice: ::rust_runtime::N, pub step: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStep }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStep,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstep,{
        0 => Dissection (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepDissectionDenestDynDenestSeq>>),
        1 => Proof  { pub pvm_step: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStep, pub inbox: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInbox },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepDissectionDenestDynDenestSeq(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepDissectionDenestDynDenestSeqIndex0,pub ::rust_runtime::N);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepDissectionDenestDynDenestSeqIndex0,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepdissectiondenestdyndenestseqindex0,{
        0 => None ,
        1 => Some  { pub state_hash: ::rust_runtime::ByteString<32> },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInbox,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofinbox,{
        0 => None ,
        1 => Some  { pub skips: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeq>>, pub level: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeLevel, pub inc: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeq>>, pub message_proof: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProof },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeq { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeLevel { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeLevelCurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeLevelCurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProof { pub version: i16, pub before: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofBefore, pub after: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofAfter, pub state: ::rust_runtime::AutoBox<TreeEncoding> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofAfter,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofinboxsomemessageproofafter,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofBefore,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofinboxsomemessageproofbefore,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeq(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeq>>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0 { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0CurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0CurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeq { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStep,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofpvmstep,{
        0 => Arithmetic_PVM_with_proof  { pub tree_proof: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProof, pub given: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofGiven, pub requested: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofRequested },
        1 => Wasm_2proto0proto0_PVM_with_proof  { pub tree_proof: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProof, pub given: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofGiven, pub requested: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofRequested },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofGiven,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithproofgiven,{
        0 => None ,
        1 => Some  { pub inbox_level: i32, pub message_counter: ::rust_runtime::N, pub payload: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofRequested,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithproofrequested,{
        0 => No_input_required ,
        1 => Initial ,
        2 => First_after (pub i32,pub ::rust_runtime::N),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProof { pub version: i16, pub before: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBefore, pub after: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfter, pub state: ::rust_runtime::AutoBox<TreeEncoding> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfter,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithprooftreeproofafter,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBefore,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithprooftreeproofbefore,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofGiven,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithproofgiven,{
        0 => None ,
        1 => Some  { pub inbox_level: i32, pub message_counter: ::rust_runtime::N, pub payload: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofRequested,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithproofrequested,{
        0 => No_input_required ,
        1 => Initial ,
        2 => First_after (pub i32,pub ::rust_runtime::N),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProof { pub version: i16, pub before: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBefore, pub after: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfter, pub state: ::rust_runtime::AutoBox<TreeEncoding> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfter,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithprooftreeproofafter,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBefore,u8,proto014ptkathmaoperationalphaoperationcontentsandresultscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithprooftreeproofbefore,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupRefuteSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupTimeoutMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultScRollupTimeout, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupTimeoutSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupTimeoutStakers { pub alice: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupTimeoutStakersAlice, pub bob: Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupTimeoutStakersBob }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupTimeoutStakersAlice { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultScRollupTimeoutStakersBob { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultSeedNonceRevelationMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultSetDepositsLimitMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultSetDepositsLimit, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultSetDepositsLimitSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTransactionMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultTransaction, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTransactionParameters { pub entrypoint: Proto014PtKathmaEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTransactionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTransferTicketMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultTransferTicket, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTransferTicketSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitCommitment { pub level: i32, pub messages: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitCommitmentMessagesDenestDynDenestSeq>>, pub predecessor: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitCommitmentPredecessor, pub inbox_merkle_root: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitCommitmentInboxMerkleRoot }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitCommitmentInboxMerkleRoot { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitCommitmentMessagesDenestDynDenestSeq { pub message_result_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitCommitmentPredecessor,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrollupcommitcommitmentpredecessor,{
        0 => None ,
        1 => Some  { pub commitment_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultTxRollupCommit, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupCommitSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultTxRollupDispatchTickets, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq { pub contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticketer: Proto014PtKathmaContractId, pub amount: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount, pub claimer: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrollupdispatchticketsticketsinfodenestdyndenestseqamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupFinalizeCommitmentMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultTxRollupFinalizeCommitment, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupFinalizeCommitmentSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupOriginationMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultTxRollupOrigination, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessage,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionmessage,{
        0 => Batch  { pub batch: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => Deposit  { pub deposit: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageDepositDeposit },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageDepositDeposit { pub sender: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageDepositDepositSender, pub destination: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageDepositDepositDestination, pub ticket_hash: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageDepositDepositTicketHash, pub amount: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageDepositDepositAmount }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageDepositDepositAmount,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionmessagedepositdepositamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageDepositDepositDestination { pub tx_rollup_l2_address: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageDepositDepositSender { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageDepositDepositTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessagePathDenestDynDenestSeq { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageResultHash { pub message_result_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultTxRollupRejection, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionPreviousMessageResult { pub context_hash: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionPreviousMessageResultContextHash, pub withdraw_list_hash: Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionPreviousMessageResultWithdrawListHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionPreviousMessageResultContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionPreviousMessageResultWithdrawListHash { pub withdraw_list_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProof,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproof,{
        0 => case_0 (pub i16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index1,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq>>),
        1 => case_1 (pub i16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index1,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq>>),
        2 => case_2 (pub i16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index1,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq>>),
        3 => case_3 (pub i16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index1,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq>>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2case1index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphaoperationcontentsandresulttxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRejectionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRemoveCommitmentMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultTxRollupRemoveCommitment, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupRemoveCommitmentSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupReturnBondMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultTxRollupReturnBond, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupReturnBondSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupSubmitBatchMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub operation_result: Proto014PtKathmaOperationAlphaOperationResultTxRollupSubmitBatch, pub internal_operation_results: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaApplyInternalResultsAlphaOperationResult>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultTxRollupSubmitBatchSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultVdfRevelationMetadata { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationContentsAndResultVdfRevelationSolution(pub ::rust_runtime::ByteString<100>,pub ::rust_runtime::ByteString<100>);
data!(Proto014PtKathmaOperationAlphaOperationResultDalPublishSlotHeader,u8,proto014ptkathmaoperationalphaoperationresultdalpublishslotheader,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultDelegation,u8,proto014ptkathmaoperationalphaoperationresultdelegation,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultIncreasePaidStorage,u8,proto014ptkathmaoperationalphaoperationresultincreasepaidstorage,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultOrigination,u8,proto014ptkathmaoperationalphaoperationresultorigination,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub originated_contracts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaContractIdOriginated>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub paid_storage_size_diff: ::rust_runtime::Z, pub lazy_storage_diff: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>> },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub originated_contracts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaContractIdOriginated>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub paid_storage_size_diff: ::rust_runtime::Z, pub lazy_storage_diff: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>> },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultRegisterGlobalConstant,u8,proto014ptkathmaoperationalphaoperationresultregisterglobalconstant,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub global_address: Proto014PtKathmaOperationAlphaOperationResultRegisterGlobalConstantAppliedGlobalAddress },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub global_address: Proto014PtKathmaOperationAlphaOperationResultRegisterGlobalConstantBacktrackedGlobalAddress },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultRegisterGlobalConstantAppliedGlobalAddress { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultRegisterGlobalConstantBacktrackedGlobalAddress { pub script_expr: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationResultReveal,u8,proto014ptkathmaoperationalphaoperationresultreveal,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessages,u8,proto014ptkathmaoperationalphaoperationresultscrollupaddmessages,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N, pub inbox_after: Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesAppliedInboxAfter },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N, pub inbox_after: Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesBacktrackedInboxAfter },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesAppliedInboxAfter { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesAppliedInboxAfterCurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesAppliedInboxAfterOldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesAppliedInboxAfterCurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesAppliedInboxAfterOldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesAppliedInboxAfterOldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesAppliedInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesAppliedInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesAppliedInboxAfterOldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesBacktrackedInboxAfter { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesBacktrackedInboxAfterCurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesBacktrackedInboxAfterOldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesBacktrackedInboxAfterCurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesBacktrackedInboxAfterOldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesBacktrackedInboxAfterOldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesBacktrackedInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesBacktrackedInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupAddMessagesBacktrackedInboxAfterOldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupCement,u8,proto014ptkathmaoperationalphaoperationresultscrollupcement,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupDalSlotSubscribe,u8,proto014ptkathmaoperationalphaoperationresultscrollupdalslotsubscribe,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N, pub slot_index: u8, pub level: i32 },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N, pub slot_index: u8, pub level: i32 },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupExecuteOutboxMessage,u8,proto014ptkathmaoperationalphaoperationresultscrollupexecuteoutboxmessage,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub paid_storage_size_diff: ::rust_runtime::Z },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub paid_storage_size_diff: ::rust_runtime::Z },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupOriginate,u8,proto014ptkathmaoperationalphaoperationresultscrolluporiginate,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub address: Proto014PtKathmaOperationAlphaOperationResultScRollupOriginateAppliedAddress, pub consumed_milligas: ::rust_runtime::N, pub size: ::rust_runtime::Z },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub address: Proto014PtKathmaOperationAlphaOperationResultScRollupOriginateBacktrackedAddress, pub consumed_milligas: ::rust_runtime::N, pub size: ::rust_runtime::Z },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupOriginateAppliedAddress { pub sc_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupOriginateBacktrackedAddress { pub sc_rollup_hash: ::rust_runtime::ByteString<20> }
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupPublish,u8,proto014ptkathmaoperationalphaoperationresultscrolluppublish,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N, pub staked_hash: Proto014PtKathmaOperationAlphaOperationResultScRollupPublishAppliedStakedHash, pub published_at_level: i32, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N, pub staked_hash: Proto014PtKathmaOperationAlphaOperationResultScRollupPublishBacktrackedStakedHash, pub published_at_level: i32, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupPublishAppliedStakedHash { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupPublishBacktrackedStakedHash { pub commitment_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupRecoverBond,u8,proto014ptkathmaoperationalphaoperationresultscrolluprecoverbond,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupRefute,u8,proto014ptkathmaoperationalphaoperationresultscrolluprefute,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N, pub status: Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteAppliedStatus, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N, pub status: Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteBacktrackedStatus, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteAppliedStatus,u8,proto014ptkathmaoperationalphaoperationresultscrolluprefuteappliedstatus,{
        0 => Ongoing ,
        1 => Ended (pub Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteAppliedStatusEndedIndex0,pub Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteAppliedStatusEndedIndex1),
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteAppliedStatusEndedIndex0,u8,proto014ptkathmaoperationalphaoperationresultscrolluprefuteappliedstatusendedindex0,{
        0 => Conflict_resolved ,
        1 => Invalid_move (pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>),
        2 => Timeout ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteAppliedStatusEndedIndex1 { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteBacktrackedStatus,u8,proto014ptkathmaoperationalphaoperationresultscrolluprefutebacktrackedstatus,{
        0 => Ongoing ,
        1 => Ended (pub Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteBacktrackedStatusEndedIndex0,pub Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteBacktrackedStatusEndedIndex1),
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteBacktrackedStatusEndedIndex0,u8,proto014ptkathmaoperationalphaoperationresultscrolluprefutebacktrackedstatusendedindex0,{
        0 => Conflict_resolved ,
        1 => Invalid_move (pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>),
        2 => Timeout ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupRefuteBacktrackedStatusEndedIndex1 { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupTimeout,u8,proto014ptkathmaoperationalphaoperationresultscrolluptimeout,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N, pub status: Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutAppliedStatus, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N, pub status: Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutBacktrackedStatus, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>> },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutAppliedStatus,u8,proto014ptkathmaoperationalphaoperationresultscrolluptimeoutappliedstatus,{
        0 => Ongoing ,
        1 => Ended (pub Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutAppliedStatusEndedIndex0,pub Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutAppliedStatusEndedIndex1),
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutAppliedStatusEndedIndex0,u8,proto014ptkathmaoperationalphaoperationresultscrolluptimeoutappliedstatusendedindex0,{
        0 => Conflict_resolved ,
        1 => Invalid_move (pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>),
        2 => Timeout ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutAppliedStatusEndedIndex1 { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutBacktrackedStatus,u8,proto014ptkathmaoperationalphaoperationresultscrolluptimeoutbacktrackedstatus,{
        0 => Ongoing ,
        1 => Ended (pub Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutBacktrackedStatusEndedIndex0,pub Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutBacktrackedStatusEndedIndex1),
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutBacktrackedStatusEndedIndex0,u8,proto014ptkathmaoperationalphaoperationresultscrolluptimeoutbacktrackedstatusendedindex0,{
        0 => Conflict_resolved ,
        1 => Invalid_move (pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>),
        2 => Timeout ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultScRollupTimeoutBacktrackedStatusEndedIndex1 { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationAlphaOperationResultSetDepositsLimit,u8,proto014ptkathmaoperationalphaoperationresultsetdepositslimit,{
        0 => Applied  { pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultTransaction,u8,proto014ptkathmaoperationalphaoperationresulttransaction,{
        0 => Applied  { pub proto014_ptkathma_operation_alpha_operation_result_transaction_applied_rhs: Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedRhs },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub proto014_ptkathma_operation_alpha_operation_result_transaction_backtracked_rhs: Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedRhs },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedRhs,u8,proto014ptkathmaoperationalphaoperationresulttransactionappliedrhs,{
        0 => To_contract  { pub storage: std::option::Option<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub originated_contracts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaContractIdOriginated>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub paid_storage_size_diff: ::rust_runtime::Z, pub allocated_destination_contract: bool, pub lazy_storage_diff: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>> },
        1 => To_tx_rollup  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub ticket_hash: Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToTxRollupTicketHash, pub paid_storage_size_diff: ::rust_runtime::N },
        2 => To_sc_rollup  { pub consumed_milligas: ::rust_runtime::N, pub inbox_after: Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToScRollupInboxAfter },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToScRollupInboxAfter { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToScRollupInboxAfterCurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToScRollupInboxAfterCurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToScRollupInboxAfterOldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionAppliedToTxRollupTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedRhs,u8,proto014ptkathmaoperationalphaoperationresulttransactionbacktrackedrhs,{
        0 => To_contract  { pub storage: std::option::Option<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub originated_contracts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaContractIdOriginated>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub paid_storage_size_diff: ::rust_runtime::Z, pub allocated_destination_contract: bool, pub lazy_storage_diff: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>> },
        1 => To_tx_rollup  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub ticket_hash: Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToTxRollupTicketHash, pub paid_storage_size_diff: ::rust_runtime::N },
        2 => To_sc_rollup  { pub consumed_milligas: ::rust_runtime::N, pub inbox_after: Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToScRollupInboxAfter },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToScRollupInboxAfter { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToScRollupInboxAfterCurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToScRollupInboxAfterCurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToScRollupInboxAfterOldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationResultTransactionBacktrackedToTxRollupTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaOperationResultTransferTicket,u8,proto014ptkathmaoperationalphaoperationresulttransferticket,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub paid_storage_size_diff: ::rust_runtime::Z },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub paid_storage_size_diff: ::rust_runtime::Z },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultTxRollupCommit,u8,proto014ptkathmaoperationalphaoperationresulttxrollupcommit,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultTxRollupDispatchTickets,u8,proto014ptkathmaoperationalphaoperationresulttxrollupdispatchtickets,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub paid_storage_size_diff: ::rust_runtime::Z },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub paid_storage_size_diff: ::rust_runtime::Z },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultTxRollupFinalizeCommitment,u8,proto014ptkathmaoperationalphaoperationresulttxrollupfinalizecommitment,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub level: i32 },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub level: i32 },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultTxRollupOrigination,u8,proto014ptkathmaoperationalphaoperationresulttxrolluporigination,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub originated_rollup: Proto014PtKathmaTxRollupId },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub originated_rollup: Proto014PtKathmaTxRollupId },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultTxRollupRejection,u8,proto014ptkathmaoperationalphaoperationresulttxrolluprejection,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultTxRollupRemoveCommitment,u8,proto014ptkathmaoperationalphaoperationresulttxrollupremovecommitment,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub level: i32 },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub level: i32 },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultTxRollupReturnBond,u8,proto014ptkathmaoperationalphaoperationresulttxrollupreturnbond,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationResultTxRollupSubmitBatch,u8,proto014ptkathmaoperationalphaoperationresulttxrollupsubmitbatch,{
        0 => Applied  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub paid_storage_size_diff: ::rust_runtime::N },
        1 => Failed  { pub errors: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        2 => Skipped ,
        3 => Backtracked  { pub errors: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub paid_storage_size_diff: ::rust_runtime::N },
    }
    );
data!(Proto014PtKathmaOperationAlphaOperationWithMetadata,u8,proto014ptkathmaoperationalphaoperationwithmetadata,{
        0 => Operation_with_metadata  { pub contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaOperationContentsAndResult>>, pub signature: std::option::Option<Proto014PtKathmaOperationAlphaOperationWithMetadataOperationWithMetadataSignature> },
        1 => Operation_without_metadata  { pub contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContents>>, pub signature: std::option::Option<Proto014PtKathmaOperationAlphaOperationWithMetadataOperationWithoutMetadataSignature> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationWithMetadataOperationWithMetadataSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaOperationWithMetadataOperationWithoutMetadataSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
data!(Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResult,u8,proto014ptkathmaoperationalphasuccessfulmanageroperationresult,{
        0 => reveal  { pub consumed_milligas: ::rust_runtime::N },
        1 => transaction  { pub proto014_ptkathma_operation_alpha_successful_manager_operation_result_transaction_rhs: Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionRhs },
        2 => origination  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub originated_contracts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaContractIdOriginated>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub paid_storage_size_diff: ::rust_runtime::Z, pub lazy_storage_diff: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>> },
        3 => delegation  { pub consumed_milligas: ::rust_runtime::N },
        5 => set_deposits_limit  { pub consumed_milligas: ::rust_runtime::N },
        9 => increase_paid_storage  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N },
        200 => sc_rollup_originate  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub address: Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultScRollupOriginateAddress, pub consumed_milligas: ::rust_runtime::N, pub size: ::rust_runtime::Z },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultScRollupOriginateAddress { pub sc_rollup_hash: ::rust_runtime::ByteString<20> }
data!(Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionRhs,u8,proto014ptkathmaoperationalphasuccessfulmanageroperationresulttransactionrhs,{
        0 => To_contract  { pub storage: std::option::Option<MichelineProto014PtKathmaMichelsonV1Expression>, pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub originated_contracts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaContractIdOriginated>>, pub consumed_milligas: ::rust_runtime::N, pub storage_size: ::rust_runtime::Z, pub paid_storage_size_diff: ::rust_runtime::Z, pub allocated_destination_contract: bool, pub lazy_storage_diff: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>> },
        1 => To_tx_rollup  { pub balance_updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>, pub consumed_milligas: ::rust_runtime::N, pub ticket_hash: Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToTxRollupTicketHash, pub paid_storage_size_diff: ::rust_runtime::N },
        2 => To_sc_rollup  { pub consumed_milligas: ::rust_runtime::N, pub inbox_after: Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToScRollupInboxAfter },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToScRollupInboxAfter { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToScRollupInboxAfterCurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToScRollupInboxAfterOldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToScRollupInboxAfterCurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToScRollupInboxAfterOldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToScRollupInboxAfterOldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToScRollupInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToScRollupInboxAfterOldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToScRollupInboxAfterOldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaSuccessfulManagerOperationResultTransactionToTxRollupTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationMetadataAlphaBalance,u8,proto014ptkathmaoperationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto014PtKathmaContractId, pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        2 => Block_fees  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        4 => Deposits  { pub delegate: Proto014PtKathmaOperationMetadataAlphaBalanceDepositsDelegate, pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        5 => Nonce_revelation_rewards  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        6 => Double_signing_evidence_rewards  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        7 => Endorsing_rewards  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        8 => Baking_rewards  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        9 => Baking_bonuses  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        11 => Storage_fees  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        12 => Double_signing_punishments  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        13 => Lost_endorsing_rewards  { pub delegate: Proto014PtKathmaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate, pub participation: bool, pub revelation: bool, pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        14 => Liquidity_baking_subsidies  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        15 => Burned  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        16 => Commitments  { pub committer: Proto014PtKathmaOperationMetadataAlphaBalanceCommitmentsCommitter, pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        17 => Bootstrap  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        18 => Invoice  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        19 => Initial_commitments  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        20 => Minted  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        21 => Frozen_bonds  { pub contract: Proto014PtKathmaContractId, pub bond_id: Proto014PtKathmaBondId, pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        22 => Tx_rollup_rejection_rewards  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        23 => Tx_rollup_rejection_punishments  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        24 => Sc_rollup_refutation_punishments  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationMetadataAlphaBalanceCommitmentsCommitter { pub blinded_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationMetadataAlphaBalanceDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin,u8,proto014ptkathmaoperationmetadataalphaupdateoriginorigin,{
        0 => Block_application ,
        1 => Protocol_migration ,
        2 => Subsidy ,
        3 => Simulation ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(Proto014PtKathmaTransactionDestination,u8,proto014ptkathmatransactiondestination,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto014PtKathmaTransactionDestinationOriginatedDenestPad,1>),
        2 => Tx_rollup (pub ::rust_runtime::Padded<Proto014PtKathmaTxRollupId,1>),
        3 => Sc_rollup (pub ::rust_runtime::Padded<Proto014PtKathmaTransactionDestinationScRollupDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaTransactionDestinationOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaTransactionDestinationScRollupDenestPad { pub sc_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct RawBlockHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub payload_hash: Proto014PtKathmaBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto014PtKathmaBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: Proto014PtKathmaBlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingTransactionCiphertext { pub cv: ::rust_runtime::ByteString<32>, pub epk: ::rust_runtime::ByteString<32>, pub payload_enc: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub nonce_enc: ::rust_runtime::ByteString<24>, pub payload_out: ::rust_runtime::ByteString<80>, pub nonce_out: ::rust_runtime::ByteString<24> }
data!(TestChainStatus,u8,testchainstatus,{
        0 => Not_running ,
        1 => Forking  { pub protocol: TestChainStatusForkingProtocol, pub expiration: i64 },
        2 => Running  { pub chain_id: TestChainStatusRunningChainId, pub genesis: TestChainStatusRunningGenesis, pub protocol: TestChainStatusRunningProtocol, pub expiration: i64 },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct TestChainStatusForkingProtocol { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct TestChainStatusRunningChainId { pub chain_id: ::rust_runtime::ByteString<4> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct TestChainStatusRunningGenesis { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct TestChainStatusRunningProtocol { pub protocol_hash: ::rust_runtime::ByteString<32> }
data!(TreeEncoding,u8,treeencoding,{
        0 => Value  { pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        1 => Blinded_value  { pub blinded_value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateBlindedValueBlindedValue },
        2 => Node  { pub node: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateNodeNodeDenestDynDenestSeq>> },
        3 => Blinded_node  { pub blinded_node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateBlindedNodeBlindedNode },
        4 => Inode  { pub inode: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInode },
        5 => Extender  { pub extender: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateExtenderExtender },
        6 => None ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockInfo { pub chain_id: ChainId, pub hash: Hash, pub header: ::rust_runtime::Dynamic<::rust_runtime::u30,RawBlockHeader>, pub metadata: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,BlockHeaderMetadata>>, pub operations: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Operation>>>>> }
#[allow(dead_code)]
pub fn proto014ptkathmablockinfo_write<U: Target>(val: &Proto014PtKathmaBlockInfo, buf: &mut U) -> usize {
    ChainId::write_to(&val.chain_id, buf) + Hash::write_to(&val.hash, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,RawBlockHeader>::write_to(&val.header, buf) + std::option::Option::<::rust_runtime::Dynamic::<::rust_runtime::u30,BlockHeaderMetadata>>::write_to(&val.metadata, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Operation>>>>>::write_to(&val.operations, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmablockinfo_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaBlockInfo> {
    Ok(Proto014PtKathmaBlockInfo {chain_id: ChainId::parse(p)?, hash: Hash::parse(p)?, header: ::rust_runtime::Dynamic::<::rust_runtime::u30,RawBlockHeader>::parse(p)?, metadata: std::option::Option::<::rust_runtime::Dynamic::<::rust_runtime::u30,BlockHeaderMetadata>>::parse(p)?, operations: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Operation>>>>>::parse(p)?})
}
