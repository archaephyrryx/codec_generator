#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto014PtKathmaNonce = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn proto014ptkathmanonce_write<U: Target>(val: &Proto014PtKathmaNonce, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmanonce_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaNonce> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
