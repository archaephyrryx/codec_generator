#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto014PtKathmaTimestamp = i64;
#[allow(dead_code)]
pub fn proto014ptkathmatimestamp_write<U: Target>(val: &Proto014PtKathmaTimestamp, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmatimestamp_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaTimestamp> {
    Ok(i64::parse(p)?)
}
