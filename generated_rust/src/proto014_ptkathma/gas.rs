#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto014PtKathmaGas,u8,proto014ptkathmagas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto014ptkathmagas_write<U: Target>(val: &Proto014PtKathmaGas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmagas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaGas> {
    Proto014PtKathmaGas::parse(p)
}
