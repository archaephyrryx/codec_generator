#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto014PtKathmaRawLevel = i32;
#[allow(dead_code)]
pub fn proto014ptkathmarawlevel_write<U: Target>(val: &Proto014PtKathmaRawLevel, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmarawlevel_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaRawLevel> {
    Ok(i32::parse(p)?)
}
