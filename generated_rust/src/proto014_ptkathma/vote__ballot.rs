#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto014PtKathmaVoteBallot = i8;
#[allow(dead_code)]
pub fn proto014ptkathmavoteballot_write<U: Target>(val: &Proto014PtKathmaVoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmavoteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaVoteBallot> {
    Ok(i8::parse(p)?)
}
