#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaVoteBallots { pub yay: i64, pub nay: i64, pub pass: i64 }
#[allow(dead_code)]
pub fn proto014ptkathmavoteballots_write<U: Target>(val: &Proto014PtKathmaVoteBallots, buf: &mut U) -> usize {
    i64::write_to(&val.yay, buf) + i64::write_to(&val.nay, buf) + i64::write_to(&val.pass, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmavoteballots_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaVoteBallots> {
    Ok(Proto014PtKathmaVoteBallots {yay: i64::parse(p)?, nay: i64::parse(p)?, pass: i64::parse(p)?})
}
