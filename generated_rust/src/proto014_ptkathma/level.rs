#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLevel { pub level: i32, pub level_position: i32, pub cycle: i32, pub cycle_position: i32, pub expected_commitment: bool }
#[allow(dead_code)]
pub fn proto014ptkathmalevel_write<U: Target>(val: &Proto014PtKathmaLevel, buf: &mut U) -> usize {
    i32::write_to(&val.level, buf) + i32::write_to(&val.level_position, buf) + i32::write_to(&val.cycle, buf) + i32::write_to(&val.cycle_position, buf) + bool::write_to(&val.expected_commitment, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmalevel_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaLevel> {
    Ok(Proto014PtKathmaLevel {level: i32::parse(p)?, level_position: i32::parse(p)?, cycle: i32::parse(p)?, cycle_position: i32::parse(p)?, expected_commitment: bool::parse(p)?})
}
