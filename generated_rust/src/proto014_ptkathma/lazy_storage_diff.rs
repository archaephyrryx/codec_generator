#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{AutoBox,ByteString,Bytes,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,Z,cstyle,data,resolve_zero,u30};
data!(MichelineProto014PtKathmaMichelsonV1Expression,u8,michelineproto014ptkathmamichelsonv1expression,{
        0 => Int  { pub int: ::rust_runtime::Z },
        1 => String  { pub string: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        2 => Sequence (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>>>),
        3 => Prim__no_args__no_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives },
        4 => Prim__no_args__some_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        5 => Prim__1_arg__no_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression> },
        6 => Prim__1_arg__some_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        7 => Prim__2_args__no_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression> },
        8 => Prim__2_args__some_annots  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        9 => Prim__generic  { pub prim: Proto014PtKathmaMichelsonV1Primitives, pub args: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>>>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        10 => Bytes  { pub bytes: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
data!(Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq,u8,proto014ptkathmalazystoragediffdenestdyndenestseq,{
        0 => big_map  { pub id: ::rust_runtime::Z, pub diff: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff },
        1 => sapling_state  { pub id: ::rust_runtime::Z, pub diff: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff },
    }
    );
data!(Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff,u8,proto014ptkathmalazystoragediffdenestdyndenestseqbigmapdiff,{
        0 => update  { pub updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq>> },
        1 => remove ,
        2 => copy  { pub source: ::rust_runtime::Z, pub updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq>> },
        3 => alloc  { pub updates: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq>>, pub key_type: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub value_type: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq { pub key_hash: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash, pub key: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub value: std::option::Option<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq { pub key_hash: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash, pub key: ::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>, pub value: std::option::Option<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq { pub key_hash: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash, pub key: MichelineProto014PtKathmaMichelsonV1Expression, pub value: std::option::Option<::rust_runtime::AutoBox<MichelineProto014PtKathmaMichelsonV1Expression>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash { pub script_expr: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff,u8,proto014ptkathmalazystoragediffdenestdyndenestseqsaplingstatediff,{
        0 => update  { pub updates: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates },
        1 => remove ,
        2 => copy  { pub source: ::rust_runtime::Z, pub updates: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates },
        3 => alloc  { pub updates: Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates, pub memo_size: u16 },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates { pub commitments_and_ciphertexts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>>, pub nullifiers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq(pub ::rust_runtime::ByteString<32>,pub SaplingTransactionCiphertext);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates { pub commitments_and_ciphertexts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>>, pub nullifiers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq(pub ::rust_runtime::ByteString<32>,pub SaplingTransactionCiphertext);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates { pub commitments_and_ciphertexts: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>>, pub nullifiers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq(pub ::rust_runtime::ByteString<32>,pub SaplingTransactionCiphertext);
cstyle!(Proto014PtKathmaMichelsonV1Primitives,u8,{
        parameter = 0,
        storage = 1,
        code = 2,
        False = 3,
        Elt = 4,
        Left = 5,
        None = 6,
        Pair = 7,
        Right = 8,
        Some = 9,
        True = 10,
        Unit = 11,
        PACK = 12,
        UNPACK = 13,
        BLAKE2B = 14,
        SHA256 = 15,
        SHA512 = 16,
        ABS = 17,
        ADD = 18,
        AMOUNT = 19,
        AND = 20,
        BALANCE = 21,
        CAR = 22,
        CDR = 23,
        CHECK_SIGNATURE = 24,
        COMPARE = 25,
        CONCAT = 26,
        CONS = 27,
        CREATE_ACCOUNT = 28,
        CREATE_CONTRACT = 29,
        IMPLICIT_ACCOUNT = 30,
        DIP = 31,
        DROP = 32,
        DUP = 33,
        EDIV = 34,
        EMPTY_MAP = 35,
        EMPTY_SET = 36,
        EQ = 37,
        EXEC = 38,
        FAILWITH = 39,
        GE = 40,
        GET = 41,
        GT = 42,
        HASH_KEY = 43,
        IF = 44,
        IF_CONS = 45,
        IF_LEFT = 46,
        IF_NONE = 47,
        INT = 48,
        LAMBDA = 49,
        LE = 50,
        LEFT = 51,
        LOOP = 52,
        LSL = 53,
        LSR = 54,
        LT = 55,
        MAP = 56,
        MEM = 57,
        MUL = 58,
        NEG = 59,
        NEQ = 60,
        NIL = 61,
        NONE = 62,
        NOT = 63,
        NOW = 64,
        OR = 65,
        PAIR = 66,
        PUSH = 67,
        RIGHT = 68,
        SIZE = 69,
        SOME = 70,
        SOURCE = 71,
        SENDER = 72,
        SELF = 73,
        STEPS_TO_QUOTA = 74,
        SUB = 75,
        SWAP = 76,
        TRANSFER_TOKENS = 77,
        SET_DELEGATE = 78,
        UNIT = 79,
        UPDATE = 80,
        XOR = 81,
        ITER = 82,
        LOOP_LEFT = 83,
        ADDRESS = 84,
        CONTRACT = 85,
        ISNAT = 86,
        CAST = 87,
        RENAME = 88,
        bool = 89,
        contract = 90,
        int = 91,
        key = 92,
        key_hash = 93,
        lambda = 94,
        list = 95,
        map = 96,
        big_map = 97,
        nat = 98,
        option = 99,
        or = 100,
        pair = 101,
        set = 102,
        signature = 103,
        string = 104,
        bytes = 105,
        mutez = 106,
        timestamp = 107,
        unit = 108,
        operation = 109,
        address = 110,
        SLICE = 111,
        DIG = 112,
        DUG = 113,
        EMPTY_BIG_MAP = 114,
        APPLY = 115,
        chain_id = 116,
        CHAIN_ID = 117,
        LEVEL = 118,
        SELF_ADDRESS = 119,
        never = 120,
        NEVER = 121,
        UNPAIR = 122,
        VOTING_POWER = 123,
        TOTAL_VOTING_POWER = 124,
        KECCAK = 125,
        SHA3 = 126,
        PAIRING_CHECK = 127,
        bls12_381_g1 = 128,
        bls12_381_g2 = 129,
        bls12_381_fr = 130,
        sapling_state = 131,
        sapling_transaction_deprecated = 132,
        SAPLING_EMPTY_STATE = 133,
        SAPLING_VERIFY_UPDATE = 134,
        ticket = 135,
        TICKET = 136,
        READ_TICKET = 137,
        SPLIT_TICKET = 138,
        JOIN_TICKETS = 139,
        GET_AND_UPDATE = 140,
        chest = 141,
        chest_key = 142,
        OPEN_CHEST = 143,
        VIEW = 144,
        view = 145,
        constant = 146,
        SUB_MUTEZ = 147,
        tx_rollup_l2_address = 148,
        MIN_BLOCK_TIME = 149,
        sapling_transaction = 150,
        EMIT = 151
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingTransactionCiphertext { pub cv: ::rust_runtime::ByteString<32>, pub epk: ::rust_runtime::ByteString<32>, pub payload_enc: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub nonce_enc: ::rust_runtime::ByteString<24>, pub payload_out: ::rust_runtime::ByteString<80>, pub nonce_out: ::rust_runtime::ByteString<24> }
pub type Proto014PtKathmaLazyStorageDiff = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>;
#[allow(dead_code)]
pub fn proto014ptkathmalazystoragediff_write<U: Target>(val: &Proto014PtKathmaLazyStorageDiff, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmalazystoragediff_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaLazyStorageDiff> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>>::parse(p)?)
}
