#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Bytes,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaScript { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[allow(dead_code)]
pub fn proto014ptkathmascript_write<U: Target>(val: &Proto014PtKathmaScript, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::write_to(&val.code, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::write_to(&val.storage, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmascript_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaScript> {
    Ok(Proto014PtKathmaScript {code: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::parse(p)?, storage: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::parse(p)?})
}
