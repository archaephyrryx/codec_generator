#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(Proto014PtKathmaBondId,u8,proto014ptkathmabondid,{
        0 => Tx_rollup_bond_id  { pub tx_rollup: Proto014PtKathmaTxRollupId },
        1 => Sc_rollup_bond_id  { pub sc_rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
    }
    );
data!(Proto014PtKathmaContractId,u8,proto014ptkathmacontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto014PtKathmaContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto014PtKathmaOperationMetadataAlphaBalance,u8,proto014ptkathmaoperationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto014PtKathmaContractId, pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        2 => Block_fees  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        4 => Deposits  { pub delegate: Proto014PtKathmaOperationMetadataAlphaBalanceDepositsDelegate, pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        5 => Nonce_revelation_rewards  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        6 => Double_signing_evidence_rewards  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        7 => Endorsing_rewards  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        8 => Baking_rewards  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        9 => Baking_bonuses  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        11 => Storage_fees  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        12 => Double_signing_punishments  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        13 => Lost_endorsing_rewards  { pub delegate: Proto014PtKathmaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate, pub participation: bool, pub revelation: bool, pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        14 => Liquidity_baking_subsidies  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        15 => Burned  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        16 => Commitments  { pub committer: Proto014PtKathmaOperationMetadataAlphaBalanceCommitmentsCommitter, pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        17 => Bootstrap  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        18 => Invoice  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        19 => Initial_commitments  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        20 => Minted  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        21 => Frozen_bonds  { pub contract: Proto014PtKathmaContractId, pub bond_id: Proto014PtKathmaBondId, pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        22 => Tx_rollup_rejection_rewards  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        23 => Tx_rollup_rejection_punishments  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
        24 => Sc_rollup_refutation_punishments  { pub change: i64, pub origin: Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationMetadataAlphaBalanceCommitmentsCommitter { pub blinded_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationMetadataAlphaBalanceDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin,u8,proto014ptkathmaoperationmetadataalphaupdateoriginorigin,{
        0 => Block_application ,
        1 => Protocol_migration ,
        2 => Subsidy ,
        3 => Simulation ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto014PtKathmaReceiptBalanceUpdates = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationMetadataAlphaBalance>>;
#[allow(dead_code)]
pub fn proto014ptkathmareceiptbalanceupdates_write<U: Target>(val: &Proto014PtKathmaReceiptBalanceUpdates, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto014PtKathmaOperationMetadataAlphaBalance>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmareceiptbalanceupdates_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaReceiptBalanceUpdates> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto014PtKathmaOperationMetadataAlphaBalance>>::parse(p)?)
}
