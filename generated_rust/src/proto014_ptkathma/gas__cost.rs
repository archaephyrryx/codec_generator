#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type Proto014PtKathmaGasCost = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn proto014ptkathmagascost_write<U: Target>(val: &Proto014PtKathmaGasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmagascost_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaGasCost> {
    Ok(::rust_runtime::Z::parse(p)?)
}
