#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{AutoBox,ByteString,Bytes,Decode,Dynamic,Encode,Estimable,FixSeq,N,Nullable,Padded,ParseResult,Parser,Sequence,Target,VPadded,Z,data,i31,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
data!(InodeTree,u8,inodetree,{
        0 => Blinded_inode  { pub blinded_inode: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0BlindedInodeBlindedInode },
        1 => Inode_values  { pub inode_values: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeValuesInodeValuesDenestDynDenestSeq>> },
        2 => Inode_tree  { pub inode_tree: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTree },
        3 => Inode_extender  { pub inode_extender: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeExtenderInodeExtender },
        4 => None ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaFullHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub payload_hash: Proto014PtKathmaBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto014PtKathmaBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: Proto014PtKathmaBlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaContractId,u8,proto014ptkathmacontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto014PtKathmaContractIdOriginatedDenestPad,1>),
    }
    );
data!(Proto014PtKathmaContractIdOriginated,u8,proto014ptkathmacontractidoriginated,{
        1 => Originated (pub ::rust_runtime::Padded<Proto014PtKathmaContractIdOriginatedOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaContractIdOriginatedOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto014PtKathmaEntrypoint,u8,proto014ptkathmaentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedEndorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto014PtKathmaInlinedEndorsementMempoolContents, pub signature: ::rust_runtime::Nullable<Proto014PtKathmaInlinedEndorsementSignature> }
data!(Proto014PtKathmaInlinedEndorsementMempoolContents,u8,proto014ptkathmainlinedendorsementmempoolcontents,{
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto014PtKathmaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedEndorsementSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedPreendorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto014PtKathmaInlinedPreendorsementContents, pub signature: ::rust_runtime::Nullable<Proto014PtKathmaInlinedPreendorsementSignature> }
data!(Proto014PtKathmaInlinedPreendorsementContents,u8,proto014ptkathmainlinedpreendorsementcontents,{
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto014PtKathmaInlinedPreendorsementContentsPreendorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedPreendorsementContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaInlinedPreendorsementSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
data!(Proto014PtKathmaOperationAlphaContents,u8,proto014ptkathmaoperationalphacontents,{
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32> },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedEndorsement> },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaBlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaBlockHeaderAlphaFullHeader> },
        4 => Activate_account  { pub pkh: Proto014PtKathmaOperationAlphaContentsActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20> },
        5 => Proposals  { pub source: Proto014PtKathmaOperationAlphaContentsProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>> },
        6 => Ballot  { pub source: Proto014PtKathmaOperationAlphaContentsBallotSource, pub period: i32, pub proposal: Proto014PtKathmaOperationAlphaContentsBallotProposal, pub ballot: i8 },
        7 => Double_preendorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedPreendorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto014PtKathmaInlinedPreendorsement> },
        8 => Vdf_revelation  { pub solution: Proto014PtKathmaOperationAlphaContentsVdfRevelationSolution },
        17 => Failing_noop  { pub arbitrary: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto014PtKathmaOperationAlphaContentsPreendorsementBlockPayloadHash },
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto014PtKathmaOperationAlphaContentsEndorsementBlockPayloadHash },
        22 => Dal_slot_availability  { pub endorser: Proto014PtKathmaOperationAlphaContentsDalSlotAvailabilityEndorser, pub endorsement: ::rust_runtime::Z },
        107 => Reveal  { pub source: Proto014PtKathmaOperationAlphaContentsRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: Proto014PtKathmaOperationAlphaContentsRevealPublicKey },
        108 => Transaction  { pub source: Proto014PtKathmaOperationAlphaContentsTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: Proto014PtKathmaContractId, pub parameters: std::option::Option<Proto014PtKathmaOperationAlphaContentsTransactionParameters> },
        109 => Origination  { pub source: Proto014PtKathmaOperationAlphaContentsOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto014PtKathmaOperationAlphaContentsOriginationDelegate>, pub script: Proto014PtKathmaScriptedContracts },
        110 => Delegation  { pub source: Proto014PtKathmaOperationAlphaContentsDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<Proto014PtKathmaOperationAlphaContentsDelegationDelegate> },
        111 => Register_global_constant  { pub source: Proto014PtKathmaOperationAlphaContentsRegisterGlobalConstantSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        112 => Set_deposits_limit  { pub source: Proto014PtKathmaOperationAlphaContentsSetDepositsLimitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub limit: std::option::Option<::rust_runtime::N> },
        113 => Increase_paid_storage  { pub source: Proto014PtKathmaOperationAlphaContentsIncreasePaidStorageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::Z, pub destination: Proto014PtKathmaContractIdOriginated },
        150 => Tx_rollup_origination  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N },
        151 => Tx_rollup_submit_batch  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupSubmitBatchSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub content: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub burn_limit: std::option::Option<::rust_runtime::N> },
        152 => Tx_rollup_commit  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupCommitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub commitment: Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitment },
        153 => Tx_rollup_return_bond  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupReturnBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId },
        154 => Tx_rollup_finalize_commitment  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupFinalizeCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId },
        155 => Tx_rollup_remove_commitment  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupRemoveCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId },
        156 => Tx_rollup_rejection  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaTxRollupId, pub level: i32, pub message: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessage, pub message_position: ::rust_runtime::N, pub message_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq>>, pub message_result_hash: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageResultHash, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq>>, pub previous_message_result: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResult, pub previous_message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>>, pub proof: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProof },
        157 => Tx_rollup_dispatch_tickets  { pub source: Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub tx_rollup: Proto014PtKathmaTxRollupId, pub level: i32, pub context_hash: Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsContextHash, pub message_index: ::rust_runtime::i31, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>>, pub tickets_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>> },
        158 => Transfer_ticket  { pub source: Proto014PtKathmaOperationAlphaContentsTransferTicketSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub ticket_contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ticketer: Proto014PtKathmaContractId, pub ticket_amount: ::rust_runtime::N, pub destination: Proto014PtKathmaContractId, pub entrypoint: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        200 => Sc_rollup_originate  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupOriginateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub kind: Proto014PtKathmaOperationAlphaContentsScRollupOriginateKind, pub boot_sector: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub parameters_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        201 => Sc_rollup_add_messages  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupAddMessagesSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        202 => Sc_rollup_cement  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupCementSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub commitment: Proto014PtKathmaOperationAlphaContentsScRollupCementCommitment },
        203 => Sc_rollup_publish  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub commitment: Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitment },
        204 => Sc_rollup_refute  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupRefuteSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub opponent: Proto014PtKathmaOperationAlphaContentsScRollupRefuteOpponent, pub refutation: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutation, pub is_opening_move: bool },
        205 => Sc_rollup_timeout  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupTimeoutSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub stakers: Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakers },
        206 => Sc_rollup_execute_outbox_message  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupExecuteOutboxMessageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub cemented_commitment: Proto014PtKathmaOperationAlphaContentsScRollupExecuteOutboxMessageCementedCommitment, pub outbox_level: i32, pub message_index: ::rust_runtime::i31, pub inclusion_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        207 => Sc_rollup_recover_bond  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupRecoverBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto014PtKathmaOperationAlphaContentsScRollupRecoverBondRollup },
        208 => Sc_rollup_dal_slot_subscribe  { pub source: Proto014PtKathmaOperationAlphaContentsScRollupDalSlotSubscribeSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub slot_index: u8 },
        230 => Dal_publish_slot_header  { pub source: Proto014PtKathmaOperationAlphaContentsDalPublishSlotHeaderSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub slot: Proto014PtKathmaOperationAlphaContentsDalPublishSlotHeaderSlot },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsAndSignatureSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsBallotSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsDalPublishSlotHeaderSlot { pub level: i32, pub index: u8, pub header: ::rust_runtime::i31 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsDalPublishSlotHeaderSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsDalSlotAvailabilityEndorser { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsDelegationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsIncreasePaidStorageSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsProposalsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsRegisterGlobalConstantSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsRevealPublicKey { pub signature_v0_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsRevealSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupAddMessagesSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupCementCommitment { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupCementSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupDalSlotSubscribeSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupExecuteOutboxMessageCementedCommitment { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupExecuteOutboxMessageSource { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationAlphaContentsScRollupOriginateKind,u16,proto014ptkathmaoperationalphacontentsscrolluporiginatekind,{
        0 => Example_arith_smart_contract_rollup_kind ,
        1 => Wasm_2proto0proto0_smart_contract_rollup_kind ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupOriginateSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitment { pub compressed_state: Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitmentCompressedState, pub inbox_level: i32, pub predecessor: Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitmentPredecessor, pub number_of_messages: i32, pub number_of_ticks: i32 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitmentCompressedState { pub state_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupPublishCommitmentPredecessor { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupPublishSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRecoverBondRollup { pub sc_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRecoverBondSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteOpponent { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutation { pub choice: ::rust_runtime::N, pub step: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStep }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStep,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstep,{
        0 => Dissection (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepDissectionDenestDynDenestSeq>>),
        1 => Proof  { pub pvm_step: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStep, pub inbox: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInbox },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepDissectionDenestDynDenestSeq(pub Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepDissectionDenestDynDenestSeqIndex0,pub ::rust_runtime::N);
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepDissectionDenestDynDenestSeqIndex0,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepdissectiondenestdyndenestseqindex0,{
        0 => None ,
        1 => Some  { pub state_hash: ::rust_runtime::ByteString<32> },
    }
    );
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInbox,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofinbox,{
        0 => None ,
        1 => Some  { pub skips: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeq>>, pub level: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevel, pub inc: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeq>>, pub message_proof: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProof },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeq { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeIncDenestDynDenestSeqContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevel { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelCurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelCurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeLevelOldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProof { pub version: i16, pub before: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBefore, pub after: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfter, pub state: ::rust_runtime::AutoBox<TreeEncoding> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfter,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofinboxsomemessageproofafter,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofAfterValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBefore,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofinboxsomemessageproofbefore,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeMessageProofBeforeValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeq(pub Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeq>>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0 { pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message_counter: ::rust_runtime::N, pub nb_available_messages: i64, pub nb_messages_in_commitment_period: i64, pub starting_level_of_current_commitment_period: i32, pub level: i32, pub current_messages_hash: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0CurrentMessagesHash, pub old_levels_messages: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessages }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0CurrentMessagesHash { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessages { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex0Index0OldLevelsMessagesContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeq { pub index: ::rust_runtime::i31, pub content: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqContent, pub back_pointers: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqBackPointersDenestDynDenestSeq>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqBackPointersDenestDynDenestSeq { pub inbox_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofInboxSomeSkipsDenestDynDenestSeqIndex1Index0DenestDynDenestSeqContent { pub inbox_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStep,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmstep,{
        0 => Arithmetic_PVM_with_proof  { pub tree_proof: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProof, pub given: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofGiven, pub requested: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofRequested },
        1 => Wasm_2proto0proto0_PVM_with_proof  { pub tree_proof: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProof, pub given: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofGiven, pub requested: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofRequested },
    }
    );
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofGiven,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithproofgiven,{
        0 => None ,
        1 => Some  { pub inbox_level: i32, pub message_counter: ::rust_runtime::N, pub payload: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
    }
    );
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofRequested,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithproofrequested,{
        0 => No_input_required ,
        1 => Initial ,
        2 => First_after (pub i32,pub ::rust_runtime::N),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProof { pub version: i16, pub before: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBefore, pub after: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfter, pub state: TreeEncoding }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfter,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithprooftreeproofafter,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofAfterValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBefore,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithprooftreeproofbefore,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofBeforeValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateBlindedNodeBlindedNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateBlindedValueBlindedValue { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateExtenderExtender { pub length: i64, pub segment: ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>, pub proof: ::rust_runtime::AutoBox<InodeTree> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInode { pub length: i64, pub proofs: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofs }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofs,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithprooftreeproofstateinodeinodeproofs,{
        0 => sparse_proof  { pub sparse_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeq>> },
        1 => dense_proof  { pub dense_proof: ::rust_runtime::FixSeq<::rust_runtime::AutoBox<InodeTree>,32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeq(pub u8,pub InodeTree);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0BlindedInodeBlindedInode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeExtenderInodeExtender { pub length: i64, pub segment: ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>, pub proof: ::rust_runtime::AutoBox<InodeTree> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTree { pub length: i64, pub proofs: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTreeProofs }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTreeProofs,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmsteparithmeticpvmwithprooftreeproofstateinodeinodeproofssparseproofsparseproofdenestdyndenestseqindex1index0inodetreeinodetreeproofs,{
        0 => sparse_proof  { pub sparse_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTreeProofsSparseProofSparseProofDenestDynDenestSeq>> },
        1 => dense_proof  { pub dense_proof: ::rust_runtime::FixSeq<::rust_runtime::AutoBox<InodeTree>,32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeTreeInodeTreeProofsSparseProofSparseProofDenestDynDenestSeq(pub u8,pub ::rust_runtime::AutoBox<InodeTree>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInodeProofsSparseProofSparseProofDenestDynDenestSeqIndex1Index0InodeValuesInodeValuesDenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub ::rust_runtime::AutoBox<TreeEncoding>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateNodeNodeDenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub ::rust_runtime::AutoBox<TreeEncoding>);
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofGiven,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithproofgiven,{
        0 => None ,
        1 => Some  { pub inbox_level: i32, pub message_counter: ::rust_runtime::N, pub payload: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
    }
    );
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofRequested,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithproofrequested,{
        0 => No_input_required ,
        1 => Initial ,
        2 => First_after (pub i32,pub ::rust_runtime::N),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProof { pub version: i16, pub before: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBefore, pub after: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfter, pub state: ::rust_runtime::AutoBox<TreeEncoding> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfter,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithprooftreeproofafter,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofAfterValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBefore,u8,proto014ptkathmaoperationalphacontentsscrolluprefuterefutationstepproofpvmstepwasm2proto0proto0pvmwithprooftreeproofbefore,{
        0 => Value  { pub value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeValueValue },
        1 => Node  { pub node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeNodeNode },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeNodeNode { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepWasm2Proto0Proto0PVMWithProofTreeProofBeforeValueValue { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupRefuteSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupTimeoutSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakers { pub alice: Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakersAlice, pub bob: Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakersBob }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakersAlice { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsScRollupTimeoutStakersBob { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsSetDepositsLimitSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTransactionParameters { pub entrypoint: Proto014PtKathmaEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTransactionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTransferTicketSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitment { pub level: i32, pub messages: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq>>, pub predecessor: Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentPredecessor, pub inbox_merkle_root: Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq { pub message_result_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupCommitCommitmentPredecessor,u8,proto014ptkathmaoperationalphacontentstxrollupcommitcommitmentpredecessor,{
        0 => None ,
        1 => Some  { pub commitment_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupCommitSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq { pub contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticketer: Proto014PtKathmaContractId, pub amount: Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount, pub claimer: Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,u8,proto014ptkathmaoperationalphacontentstxrollupdispatchticketsticketsinfodenestdyndenestseqamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupFinalizeCommitmentSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessage,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionmessage,{
        0 => Batch  { pub batch: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => Deposit  { pub deposit: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDeposit },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDeposit { pub sender: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender, pub destination: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination, pub ticket_hash: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash, pub amount: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionmessagedepositdepositamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination { pub tx_rollup_l2_address: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageResultHash { pub message_result_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResult { pub context_hash: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash, pub withdraw_list_hash: Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash { pub withdraw_list_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProof,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproof,{
        0 => case_0 (pub i16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index1,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq>>),
        1 => case_1 (pub i16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index1,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq>>),
        2 => case_2 (pub i16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index1,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq>>),
        3 => case_3 (pub i16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index1,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq>>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto014ptkathmaoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRejectionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupRemoveCommitmentSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupReturnBondSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsTxRollupSubmitBatchSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperationAlphaContentsVdfRevelationSolution(pub ::rust_runtime::ByteString<100>,pub ::rust_runtime::ByteString<100>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
data!(TreeEncoding,u8,treeencoding,{
        0 => Value  { pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        1 => Blinded_value  { pub blinded_value: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateBlindedValueBlindedValue },
        2 => Node  { pub node: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateNodeNodeDenestDynDenestSeq>> },
        3 => Blinded_node  { pub blinded_node: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateBlindedNodeBlindedNode },
        4 => Inode  { pub inode: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateInodeInode },
        5 => Extender  { pub extender: Proto014PtKathmaOperationAlphaContentsScRollupRefuteRefutationStepProofPvmStepArithmeticPVMWithProofTreeProofStateExtenderExtender },
        6 => None ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaOperation { pub branch: OperationShellHeaderBranch, pub contents: ::rust_runtime::VPadded<::rust_runtime::Sequence<Proto014PtKathmaOperationAlphaContents>,64>, pub signature: Proto014PtKathmaOperationAlphaContentsAndSignatureSignature }
#[allow(dead_code)]
pub fn proto014ptkathmaoperation_write<U: Target>(val: &Proto014PtKathmaOperation, buf: &mut U) -> usize {
    OperationShellHeaderBranch::write_to(&val.branch, buf) + ::rust_runtime::VPadded::<::rust_runtime::Sequence::<Proto014PtKathmaOperationAlphaContents>,64>::write_to(&val.contents, buf) + Proto014PtKathmaOperationAlphaContentsAndSignatureSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmaoperation_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaOperation> {
    Ok(Proto014PtKathmaOperation {branch: OperationShellHeaderBranch::parse(p)?, contents: ::rust_runtime::VPadded::<::rust_runtime::Sequence::<Proto014PtKathmaOperationAlphaContents>,64>::parse(p)?, signature: Proto014PtKathmaOperationAlphaContentsAndSignatureSignature::parse(p)?})
}
