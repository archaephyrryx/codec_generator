#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto014PtKathmaVotingPeriodKind,u8,proto014ptkathmavotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto014PtKathmaVotingPeriod { pub index: i32, pub kind: Proto014PtKathmaVotingPeriodKind, pub start_position: i32 }
#[allow(dead_code)]
pub fn proto014ptkathmavotingperiod_write<U: Target>(val: &Proto014PtKathmaVotingPeriod, buf: &mut U) -> usize {
    i32::write_to(&val.index, buf) + Proto014PtKathmaVotingPeriodKind::write_to(&val.kind, buf) + i32::write_to(&val.start_position, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto014ptkathmavotingperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto014PtKathmaVotingPeriod> {
    Ok(Proto014PtKathmaVotingPeriod {index: i32::parse(p)?, kind: Proto014PtKathmaVotingPeriodKind::parse(p)?, start_position: i32::parse(p)?})
}
