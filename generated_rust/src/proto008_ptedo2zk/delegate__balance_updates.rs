#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(Proto008PtEdo2ZkContractId,u8,proto008ptedo2zkcontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto008PtEdo2ZkContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto008PtEdo2ZkOperationMetadataAlphaBalance,u8,proto008ptedo2zkoperationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto008PtEdo2ZkContractId, pub change: i64 },
        1 => Rewards  { pub delegate: Proto008PtEdo2ZkOperationMetadataAlphaBalanceRewardsDelegate, pub cycle: i32, pub change: i64 },
        2 => Fees  { pub delegate: Proto008PtEdo2ZkOperationMetadataAlphaBalanceFeesDelegate, pub cycle: i32, pub change: i64 },
        3 => Deposits  { pub delegate: Proto008PtEdo2ZkOperationMetadataAlphaBalanceDepositsDelegate, pub cycle: i32, pub change: i64 },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationMetadataAlphaBalanceDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationMetadataAlphaBalanceFeesDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationMetadataAlphaBalanceRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto008PtEdo2ZkDelegateBalanceUpdates = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto008PtEdo2ZkOperationMetadataAlphaBalance>>;
#[allow(dead_code)]
pub fn proto008ptedo2zkdelegatebalanceupdates_write<U: Target>(val: &Proto008PtEdo2ZkDelegateBalanceUpdates, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto008PtEdo2ZkOperationMetadataAlphaBalance>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkdelegatebalanceupdates_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkDelegateBalanceUpdates> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto008PtEdo2ZkOperationMetadataAlphaBalance>>::parse(p)?)
}
