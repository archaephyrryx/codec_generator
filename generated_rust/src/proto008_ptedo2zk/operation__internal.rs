#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Padded,ParseResult,Parser,Target,data,resolve_zero,u30};
data!(Proto008PtEdo2ZkContractId,u8,proto008ptedo2zkcontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto008PtEdo2ZkContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto008PtEdo2ZkEntrypoint,u8,proto008ptedo2zkentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaInternalOperationDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaInternalOperationOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaInternalOperationRevealPublicKey { pub signature_v0_public_key: PublicKey }
data!(Proto008PtEdo2ZkOperationAlphaInternalOperationRhs,u8,proto008ptedo2zkoperationalphainternaloperationrhs,{
        0 => Reveal  { pub public_key: Proto008PtEdo2ZkOperationAlphaInternalOperationRevealPublicKey },
        1 => Transaction  { pub amount: ::rust_runtime::N, pub destination: Proto008PtEdo2ZkContractId, pub parameters: std::option::Option<Proto008PtEdo2ZkOperationAlphaInternalOperationTransactionParameters> },
        2 => Origination  { pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto008PtEdo2ZkOperationAlphaInternalOperationOriginationDelegate>, pub script: Proto008PtEdo2ZkScriptedContracts },
        3 => Delegation  { pub delegate: std::option::Option<Proto008PtEdo2ZkOperationAlphaInternalOperationDelegationDelegate> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaInternalOperationTransactionParameters { pub entrypoint: Proto008PtEdo2ZkEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationInternal { pub source: Proto008PtEdo2ZkContractId, pub nonce: u16, pub proto008_ptedo2zk_operation_alpha_internal_operation_rhs: Proto008PtEdo2ZkOperationAlphaInternalOperationRhs }
#[allow(dead_code)]
pub fn proto008ptedo2zkoperationinternal_write<U: Target>(val: &Proto008PtEdo2ZkOperationInternal, buf: &mut U) -> usize {
    Proto008PtEdo2ZkContractId::write_to(&val.source, buf) + u16::write_to(&val.nonce, buf) + Proto008PtEdo2ZkOperationAlphaInternalOperationRhs::write_to(&val.proto008_ptedo2zk_operation_alpha_internal_operation_rhs, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkoperationinternal_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkOperationInternal> {
    Ok(Proto008PtEdo2ZkOperationInternal {source: Proto008PtEdo2ZkContractId::parse(p)?, nonce: u16::parse(p)?, proto008_ptedo2zk_operation_alpha_internal_operation_rhs: Proto008PtEdo2ZkOperationAlphaInternalOperationRhs::parse(p)?})
}
