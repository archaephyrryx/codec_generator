#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkBlockHeaderContents { pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash> }
#[allow(dead_code)]
pub fn proto008ptedo2zkblockheadercontents_write<U: Target>(val: &Proto008PtEdo2ZkBlockHeaderContents, buf: &mut U) -> usize {
    u16::write_to(&val.priority, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkblockheadercontents_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkBlockHeaderContents> {
    Ok(Proto008PtEdo2ZkBlockHeaderContents {priority: u16::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?})
}
