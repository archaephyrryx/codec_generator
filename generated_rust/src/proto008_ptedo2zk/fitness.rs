#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Bytes,Decode,Dynamic,Encode,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
pub type Proto008PtEdo2ZkFitness = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>;
#[allow(dead_code)]
pub fn proto008ptedo2zkfitness_write<U: Target>(val: &Proto008PtEdo2ZkFitness, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkfitness_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkFitness> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>>>::parse(p)?)
}
