#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,N,ParseResult,Parser,Target,resolve_zero};
pub type Proto008PtEdo2ZkTez = ::rust_runtime::N;
#[allow(dead_code)]
pub fn proto008ptedo2zktez_write<U: Target>(val: &Proto008PtEdo2ZkTez, buf: &mut U) -> usize {
    ::rust_runtime::N::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zktez_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkTez> {
    Ok(::rust_runtime::N::parse(p)?)
}
