#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto008PtEdo2ZkVoteBallot = i8;
#[allow(dead_code)]
pub fn proto008ptedo2zkvoteballot_write<U: Target>(val: &Proto008PtEdo2ZkVoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkvoteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkVoteBallot> {
    Ok(i8::parse(p)?)
}
