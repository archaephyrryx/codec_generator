#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto008PtEdo2ZkNonce = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn proto008ptedo2zknonce_write<U: Target>(val: &Proto008PtEdo2ZkNonce, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zknonce_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkNonce> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
