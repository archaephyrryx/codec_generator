#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto008PtEdo2ZkGas,u8,proto008ptedo2zkgas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto008ptedo2zkgas_write<U: Target>(val: &Proto008PtEdo2ZkGas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkgas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkGas> {
    Proto008PtEdo2ZkGas::parse(p)
}
