#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Bytes,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkScript { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[allow(dead_code)]
pub fn proto008ptedo2zkscript_write<U: Target>(val: &Proto008PtEdo2ZkScript, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::write_to(&val.code, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::write_to(&val.storage, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkscript_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkScript> {
    Ok(Proto008PtEdo2ZkScript {code: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::parse(p)?, storage: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::parse(p)?})
}
