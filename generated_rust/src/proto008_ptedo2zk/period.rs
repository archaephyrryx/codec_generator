#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto008PtEdo2ZkPeriod = i64;
#[allow(dead_code)]
pub fn proto008ptedo2zkperiod_write<U: Target>(val: &Proto008PtEdo2ZkPeriod, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkPeriod> {
    Ok(i64::parse(p)?)
}
