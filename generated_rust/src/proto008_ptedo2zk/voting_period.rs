#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto008PtEdo2ZkVotingPeriodKind,u8,proto008ptedo2zkvotingperiodkind,{
        0 => Proposal ,
        1 => Testing_vote ,
        2 => Testing ,
        3 => Promotion_vote ,
        4 => Adoption ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkVotingPeriod { pub index: i32, pub kind: Proto008PtEdo2ZkVotingPeriodKind, pub start_position: i32 }
#[allow(dead_code)]
pub fn proto008ptedo2zkvotingperiod_write<U: Target>(val: &Proto008PtEdo2ZkVotingPeriod, buf: &mut U) -> usize {
    i32::write_to(&val.index, buf) + Proto008PtEdo2ZkVotingPeriodKind::write_to(&val.kind, buf) + i32::write_to(&val.start_position, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkvotingperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkVotingPeriod> {
    Ok(Proto008PtEdo2ZkVotingPeriod {index: i32::parse(p)?, kind: Proto008PtEdo2ZkVotingPeriodKind::parse(p)?, start_position: i32::parse(p)?})
}
