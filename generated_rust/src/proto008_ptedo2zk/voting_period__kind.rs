#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto008PtEdo2ZkVotingPeriodKind,u8,proto008ptedo2zkvotingperiodkind,{
        0 => Proposal ,
        1 => Testing_vote ,
        2 => Testing ,
        3 => Promotion_vote ,
        4 => Adoption ,
    }
    );
#[allow(dead_code)]
pub fn proto008ptedo2zkvotingperiodkind_write<U: Target>(val: &Proto008PtEdo2ZkVotingPeriodKind, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkvotingperiodkind_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkVotingPeriodKind> {
    Proto008PtEdo2ZkVotingPeriodKind::parse(p)
}
