#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,i31,resolve_zero};
pub type Proto008PtEdo2ZkScriptLoc = ::rust_runtime::i31;
#[allow(dead_code)]
pub fn proto008ptedo2zkscriptloc_write<U: Target>(val: &Proto008PtEdo2ZkScriptLoc, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkscriptloc_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkScriptLoc> {
    Ok(::rust_runtime::i31::parse(p)?)
}
