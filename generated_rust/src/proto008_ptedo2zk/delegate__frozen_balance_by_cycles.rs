#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,N,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkDelegateFrozenBalanceByCyclesDenestDynDenestSeq { pub cycle: i32, pub deposit: ::rust_runtime::N, pub fees: ::rust_runtime::N, pub rewards: ::rust_runtime::N }
pub type Proto008PtEdo2ZkDelegateFrozenBalanceByCycles = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto008PtEdo2ZkDelegateFrozenBalanceByCyclesDenestDynDenestSeq>>;
#[allow(dead_code)]
pub fn proto008ptedo2zkdelegatefrozenbalancebycycles_write<U: Target>(val: &Proto008PtEdo2ZkDelegateFrozenBalanceByCycles, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto008PtEdo2ZkDelegateFrozenBalanceByCyclesDenestDynDenestSeq>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkdelegatefrozenbalancebycycles_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkDelegateFrozenBalanceByCycles> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto008PtEdo2ZkDelegateFrozenBalanceByCyclesDenestDynDenestSeq>>::parse(p)?)
}
