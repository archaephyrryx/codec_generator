#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type Proto008PtEdo2ZkGasCost = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn proto008ptedo2zkgascost_write<U: Target>(val: &Proto008PtEdo2ZkGasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkgascost_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkGasCost> {
    Ok(::rust_runtime::Z::parse(p)?)
}
