#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkVoteListingsDenestDynDenestSeq { pub pkh: Proto008PtEdo2ZkVoteListingsDenestDynDenestSeqPkh, pub rolls: i32 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkVoteListingsDenestDynDenestSeqPkh { pub signature_v0_public_key_hash: PublicKeyHash }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto008PtEdo2ZkVoteListings = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto008PtEdo2ZkVoteListingsDenestDynDenestSeq>>;
#[allow(dead_code)]
pub fn proto008ptedo2zkvotelistings_write<U: Target>(val: &Proto008PtEdo2ZkVoteListings, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto008PtEdo2ZkVoteListingsDenestDynDenestSeq>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkvotelistings_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkVoteListings> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto008PtEdo2ZkVoteListingsDenestDynDenestSeq>>::parse(p)?)
}
