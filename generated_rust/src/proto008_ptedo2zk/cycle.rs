#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto008PtEdo2ZkCycle = i32;
#[allow(dead_code)]
pub fn proto008ptedo2zkcycle_write<U: Target>(val: &Proto008PtEdo2ZkCycle, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkcycle_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkCycle> {
    Ok(i32::parse(p)?)
}
