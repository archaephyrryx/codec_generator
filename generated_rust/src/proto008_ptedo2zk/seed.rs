#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto008PtEdo2ZkSeed = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn proto008ptedo2zkseed_write<U: Target>(val: &Proto008PtEdo2ZkSeed, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkseed_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkSeed> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
