#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Nullable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkBlockHeaderAlphaFullHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub signature: Proto008PtEdo2ZkBlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkBlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
data!(Proto008PtEdo2ZkContractId,u8,proto008ptedo2zkcontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto008PtEdo2ZkContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto008PtEdo2ZkEntrypoint,u8,proto008ptedo2zkentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkInlinedEndorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto008PtEdo2ZkInlinedEndorsementContents, pub signature: ::rust_runtime::Nullable<Proto008PtEdo2ZkInlinedEndorsementSignature> }
data!(Proto008PtEdo2ZkInlinedEndorsementContents,u8,proto008ptedo2zkinlinedendorsementcontents,{
        0 => Endorsement  { pub level: i32 },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkInlinedEndorsementSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsBallotSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsDelegationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsProposalsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsRevealPublicKey { pub signature_v0_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsRevealSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsTransactionParameters { pub entrypoint: Proto008PtEdo2ZkEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkOperationAlphaContentsTransactionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
data!(Proto008PtEdo2ZkOperationContents,u8,proto008ptedo2zkoperationcontents,{
        0 => Endorsement  { pub level: i32 },
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32> },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto008PtEdo2ZkInlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto008PtEdo2ZkInlinedEndorsement> },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto008PtEdo2ZkBlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto008PtEdo2ZkBlockHeaderAlphaFullHeader> },
        4 => Activate_account  { pub pkh: Proto008PtEdo2ZkOperationAlphaContentsActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20> },
        5 => Proposals  { pub source: Proto008PtEdo2ZkOperationAlphaContentsProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto008PtEdo2ZkOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>> },
        6 => Ballot  { pub source: Proto008PtEdo2ZkOperationAlphaContentsBallotSource, pub period: i32, pub proposal: Proto008PtEdo2ZkOperationAlphaContentsBallotProposal, pub ballot: i8 },
        107 => Reveal  { pub source: Proto008PtEdo2ZkOperationAlphaContentsRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: Proto008PtEdo2ZkOperationAlphaContentsRevealPublicKey },
        108 => Transaction  { pub source: Proto008PtEdo2ZkOperationAlphaContentsTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: Proto008PtEdo2ZkContractId, pub parameters: std::option::Option<Proto008PtEdo2ZkOperationAlphaContentsTransactionParameters> },
        109 => Origination  { pub source: Proto008PtEdo2ZkOperationAlphaContentsOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto008PtEdo2ZkOperationAlphaContentsOriginationDelegate>, pub script: Proto008PtEdo2ZkScriptedContracts },
        110 => Delegation  { pub source: Proto008PtEdo2ZkOperationAlphaContentsDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<Proto008PtEdo2ZkOperationAlphaContentsDelegationDelegate> },
    }
    );
#[allow(dead_code)]
pub fn proto008ptedo2zkoperationcontents_write<U: Target>(val: &Proto008PtEdo2ZkOperationContents, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkoperationcontents_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkOperationContents> {
    Proto008PtEdo2ZkOperationContents::parse(p)
}
