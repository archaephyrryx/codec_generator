#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkBlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto008PtEdo2ZkBlockHeaderProtocolData { pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub signature: Proto008PtEdo2ZkBlockHeaderAlphaSignedContentsSignature }
#[allow(dead_code)]
pub fn proto008ptedo2zkblockheaderprotocoldata_write<U: Target>(val: &Proto008PtEdo2ZkBlockHeaderProtocolData, buf: &mut U) -> usize {
    u16::write_to(&val.priority, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + Proto008PtEdo2ZkBlockHeaderAlphaSignedContentsSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto008ptedo2zkblockheaderprotocoldata_parse<P: Parser>(p: &mut P) -> ParseResult<Proto008PtEdo2ZkBlockHeaderProtocolData> {
    Ok(Proto008PtEdo2ZkBlockHeaderProtocolData {priority: u16::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, signature: Proto008PtEdo2ZkBlockHeaderAlphaSignedContentsSignature::parse(p)?})
}
