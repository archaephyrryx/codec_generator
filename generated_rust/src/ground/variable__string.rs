#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundVariableString = std::string::String;
#[allow(dead_code)]
pub fn groundvariablestring_write<U: Target>(val: &GroundVariableString, buf: &mut U) -> usize {
    std::string::String::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundvariablestring_parse<P: Parser>(p: &mut P) -> ParseResult<GroundVariableString> {
    Ok(std::string::String::parse(p)?)
}
