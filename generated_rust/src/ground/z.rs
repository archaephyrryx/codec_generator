#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type GroundZ = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn groundz_write<U: Target>(val: &GroundZ, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundz_parse<P: Parser>(p: &mut P) -> ParseResult<GroundZ> {
    Ok(::rust_runtime::Z::parse(p)?)
}
