#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundUint16 = u16;
#[allow(dead_code)]
pub fn grounduint16_write<U: Target>(val: &GroundUint16, buf: &mut U) -> usize {
    u16::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn grounduint16_parse<P: Parser>(p: &mut P) -> ParseResult<GroundUint16> {
    Ok(u16::parse(p)?)
}
