#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,N,ParseResult,Parser,Target,resolve_zero};
pub type GroundN = ::rust_runtime::N;
#[allow(dead_code)]
pub fn groundn_write<U: Target>(val: &GroundN, buf: &mut U) -> usize {
    ::rust_runtime::N::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundn_parse<P: Parser>(p: &mut P) -> ParseResult<GroundN> {
    Ok(::rust_runtime::N::parse(p)?)
}
