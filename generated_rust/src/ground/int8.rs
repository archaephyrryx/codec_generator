#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundInt8 = i8;
#[allow(dead_code)]
pub fn groundint8_write<U: Target>(val: &GroundInt8, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundint8_parse<P: Parser>(p: &mut P) -> ParseResult<GroundInt8> {
    Ok(i8::parse(p)?)
}
