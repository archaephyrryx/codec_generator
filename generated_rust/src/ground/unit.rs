#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundUnit = ();
#[allow(dead_code)]
pub fn groundunit_write<U: Target>(val: &GroundUnit, buf: &mut U) -> usize {
    <()>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundunit_parse<P: Parser>(p: &mut P) -> ParseResult<GroundUnit> {
    Ok(<()>::parse(p)?)
}
