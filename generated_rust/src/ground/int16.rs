#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundInt16 = i16;
#[allow(dead_code)]
pub fn groundint16_write<U: Target>(val: &GroundInt16, buf: &mut U) -> usize {
    i16::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundint16_parse<P: Parser>(p: &mut P) -> ParseResult<GroundInt16> {
    Ok(i16::parse(p)?)
}
