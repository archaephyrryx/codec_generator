#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundEmpty = ();
#[allow(dead_code)]
pub fn groundempty_write<U: Target>(val: &GroundEmpty, buf: &mut U) -> usize {
    <()>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundempty_parse<P: Parser>(p: &mut P) -> ParseResult<GroundEmpty> {
    Ok(<()>::parse(p)?)
}
