#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundNull = ();
#[allow(dead_code)]
pub fn groundnull_write<U: Target>(val: &GroundNull, buf: &mut U) -> usize {
    <()>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundnull_parse<P: Parser>(p: &mut P) -> ParseResult<GroundNull> {
    Ok(<()>::parse(p)?)
}
