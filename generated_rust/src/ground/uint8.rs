#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundUint8 = u8;
#[allow(dead_code)]
pub fn grounduint8_write<U: Target>(val: &GroundUint8, buf: &mut U) -> usize {
    u8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn grounduint8_parse<P: Parser>(p: &mut P) -> ParseResult<GroundUint8> {
    Ok(u8::parse(p)?)
}
