#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundInt64 = i64;
#[allow(dead_code)]
pub fn groundint64_write<U: Target>(val: &GroundInt64, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundint64_parse<P: Parser>(p: &mut P) -> ParseResult<GroundInt64> {
    Ok(i64::parse(p)?)
}
