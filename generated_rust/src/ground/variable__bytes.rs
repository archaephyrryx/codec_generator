#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Bytes,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundVariableBytes = ::rust_runtime::Bytes;
#[allow(dead_code)]
pub fn groundvariablebytes_write<U: Target>(val: &GroundVariableBytes, buf: &mut U) -> usize {
    ::rust_runtime::Bytes::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundvariablebytes_parse<P: Parser>(p: &mut P) -> ParseResult<GroundVariableBytes> {
    Ok(::rust_runtime::Bytes::parse(p)?)
}
