#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundInt32 = i32;
#[allow(dead_code)]
pub fn groundint32_write<U: Target>(val: &GroundInt32, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundint32_parse<P: Parser>(p: &mut P) -> ParseResult<GroundInt32> {
    Ok(i32::parse(p)?)
}
