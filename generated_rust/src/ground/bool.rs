#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundBool = bool;
#[allow(dead_code)]
pub fn groundbool_write<U: Target>(val: &GroundBool, buf: &mut U) -> usize {
    bool::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundbool_parse<P: Parser>(p: &mut P) -> ParseResult<GroundBool> {
    Ok(bool::parse(p)?)
}
