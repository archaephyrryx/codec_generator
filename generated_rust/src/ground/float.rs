#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type GroundFloat = f64;
#[allow(dead_code)]
pub fn groundfloat_write<U: Target>(val: &GroundFloat, buf: &mut U) -> usize {
    f64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn groundfloat_parse<P: Parser>(p: &mut P) -> ParseResult<GroundFloat> {
    Ok(f64::parse(p)?)
}
