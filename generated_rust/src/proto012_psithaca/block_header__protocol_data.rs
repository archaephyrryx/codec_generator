#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaBlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaBlockHeaderProtocolData { pub payload_hash: Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_escape_vote: bool, pub signature: Proto012PsithacaBlockHeaderAlphaSignedContentsSignature }
#[allow(dead_code)]
pub fn proto012psithacablockheaderprotocoldata_write<U: Target>(val: &Proto012PsithacaBlockHeaderProtocolData, buf: &mut U) -> usize {
    Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash::write_to(&val.payload_hash, buf) + i32::write_to(&val.payload_round, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + bool::write_to(&val.liquidity_baking_escape_vote, buf) + Proto012PsithacaBlockHeaderAlphaSignedContentsSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacablockheaderprotocoldata_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaBlockHeaderProtocolData> {
    Ok(Proto012PsithacaBlockHeaderProtocolData {payload_hash: Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash::parse(p)?, payload_round: i32::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, liquidity_baking_escape_vote: bool::parse(p)?, signature: Proto012PsithacaBlockHeaderAlphaSignedContentsSignature::parse(p)?})
}
