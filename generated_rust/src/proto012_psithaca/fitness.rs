#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto012PsithacaFitnessLockedRound,u8,proto012psithacafitnesslockedround,{
        0 => None ,
        1 => Some (pub i32),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaFitness { pub level: i32, pub locked_round: Proto012PsithacaFitnessLockedRound, pub predecessor_round: i32, pub round: i32 }
#[allow(dead_code)]
pub fn proto012psithacafitness_write<U: Target>(val: &Proto012PsithacaFitness, buf: &mut U) -> usize {
    i32::write_to(&val.level, buf) + Proto012PsithacaFitnessLockedRound::write_to(&val.locked_round, buf) + i32::write_to(&val.predecessor_round, buf) + i32::write_to(&val.round, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacafitness_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaFitness> {
    Ok(Proto012PsithacaFitness {level: i32::parse(p)?, locked_round: Proto012PsithacaFitnessLockedRound::parse(p)?, predecessor_round: i32::parse(p)?, round: i32::parse(p)?})
}
