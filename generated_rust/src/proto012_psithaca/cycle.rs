#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto012PsithacaCycle = i32;
#[allow(dead_code)]
pub fn proto012psithacacycle_write<U: Target>(val: &Proto012PsithacaCycle, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacacycle_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaCycle> {
    Ok(i32::parse(p)?)
}
