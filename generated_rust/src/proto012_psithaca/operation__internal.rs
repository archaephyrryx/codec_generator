#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Padded,ParseResult,Parser,Target,data,resolve_zero,u30};
data!(Proto012PsithacaContractId,u8,proto012psithacacontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto012PsithacaContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto012PsithacaEntrypoint,u8,proto012psithacaentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaInternalOperationDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaInternalOperationOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaInternalOperationRevealPublicKey { pub signature_v0_public_key: PublicKey }
data!(Proto012PsithacaOperationAlphaInternalOperationRhs,u8,proto012psithacaoperationalphainternaloperationrhs,{
        0 => Reveal  { pub public_key: Proto012PsithacaOperationAlphaInternalOperationRevealPublicKey },
        1 => Transaction  { pub amount: ::rust_runtime::N, pub destination: Proto012PsithacaContractId, pub parameters: std::option::Option<Proto012PsithacaOperationAlphaInternalOperationTransactionParameters> },
        2 => Origination  { pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto012PsithacaOperationAlphaInternalOperationOriginationDelegate>, pub script: Proto012PsithacaScriptedContracts },
        3 => Delegation  { pub delegate: std::option::Option<Proto012PsithacaOperationAlphaInternalOperationDelegationDelegate> },
        4 => Register_global_constant  { pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        5 => Set_deposits_limit  { pub limit: std::option::Option<::rust_runtime::N> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaInternalOperationTransactionParameters { pub entrypoint: Proto012PsithacaEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationInternal { pub source: Proto012PsithacaContractId, pub nonce: u16, pub proto012_psithaca_operation_alpha_internal_operation_rhs: Proto012PsithacaOperationAlphaInternalOperationRhs }
#[allow(dead_code)]
pub fn proto012psithacaoperationinternal_write<U: Target>(val: &Proto012PsithacaOperationInternal, buf: &mut U) -> usize {
    Proto012PsithacaContractId::write_to(&val.source, buf) + u16::write_to(&val.nonce, buf) + Proto012PsithacaOperationAlphaInternalOperationRhs::write_to(&val.proto012_psithaca_operation_alpha_internal_operation_rhs, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacaoperationinternal_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaOperationInternal> {
    Ok(Proto012PsithacaOperationInternal {source: Proto012PsithacaContractId::parse(p)?, nonce: u16::parse(p)?, proto012_psithaca_operation_alpha_internal_operation_rhs: Proto012PsithacaOperationAlphaInternalOperationRhs::parse(p)?})
}
