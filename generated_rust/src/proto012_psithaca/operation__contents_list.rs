#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Nullable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaBlockHeaderAlphaFullHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub payload_hash: Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_escape_vote: bool, pub signature: Proto012PsithacaBlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaBlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
data!(Proto012PsithacaContractId,u8,proto012psithacacontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto012PsithacaContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto012PsithacaEntrypoint,u8,proto012psithacaentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaInlinedEndorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto012PsithacaInlinedEndorsementMempoolContents, pub signature: ::rust_runtime::Nullable<Proto012PsithacaInlinedEndorsementSignature> }
data!(Proto012PsithacaInlinedEndorsementMempoolContents,u8,proto012psithacainlinedendorsementmempoolcontents,{
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto012PsithacaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaInlinedEndorsementSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaInlinedPreendorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto012PsithacaInlinedPreendorsementContents, pub signature: ::rust_runtime::Nullable<Proto012PsithacaInlinedPreendorsementSignature> }
data!(Proto012PsithacaInlinedPreendorsementContents,u8,proto012psithacainlinedpreendorsementcontents,{
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto012PsithacaInlinedPreendorsementContentsPreendorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaInlinedPreendorsementContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaInlinedPreendorsementSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
data!(Proto012PsithacaOperationAlphaContents,u8,proto012psithacaoperationalphacontents,{
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32> },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto012PsithacaInlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto012PsithacaInlinedEndorsement> },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto012PsithacaBlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto012PsithacaBlockHeaderAlphaFullHeader> },
        4 => Activate_account  { pub pkh: Proto012PsithacaOperationAlphaContentsActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20> },
        5 => Proposals  { pub source: Proto012PsithacaOperationAlphaContentsProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto012PsithacaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>> },
        6 => Ballot  { pub source: Proto012PsithacaOperationAlphaContentsBallotSource, pub period: i32, pub proposal: Proto012PsithacaOperationAlphaContentsBallotProposal, pub ballot: i8 },
        7 => Double_preendorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto012PsithacaInlinedPreendorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto012PsithacaInlinedPreendorsement> },
        17 => Failing_noop  { pub arbitrary: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto012PsithacaOperationAlphaContentsPreendorsementBlockPayloadHash },
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto012PsithacaOperationAlphaContentsEndorsementBlockPayloadHash },
        107 => Reveal  { pub source: Proto012PsithacaOperationAlphaContentsRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: Proto012PsithacaOperationAlphaContentsRevealPublicKey },
        108 => Transaction  { pub source: Proto012PsithacaOperationAlphaContentsTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: Proto012PsithacaContractId, pub parameters: std::option::Option<Proto012PsithacaOperationAlphaContentsTransactionParameters> },
        109 => Origination  { pub source: Proto012PsithacaOperationAlphaContentsOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto012PsithacaOperationAlphaContentsOriginationDelegate>, pub script: Proto012PsithacaScriptedContracts },
        110 => Delegation  { pub source: Proto012PsithacaOperationAlphaContentsDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<Proto012PsithacaOperationAlphaContentsDelegationDelegate> },
        111 => Register_global_constant  { pub source: Proto012PsithacaOperationAlphaContentsRegisterGlobalConstantSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        112 => Set_deposits_limit  { pub source: Proto012PsithacaOperationAlphaContentsSetDepositsLimitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub limit: std::option::Option<::rust_runtime::N> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsBallotSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsDelegationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsProposalsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsRegisterGlobalConstantSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsRevealPublicKey { pub signature_v0_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsRevealSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsSetDepositsLimitSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsTransactionParameters { pub entrypoint: Proto012PsithacaEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationAlphaContentsTransactionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto012PsithacaOperationContentsList = ::rust_runtime::Sequence<Proto012PsithacaOperationAlphaContents>;
#[allow(dead_code)]
pub fn proto012psithacaoperationcontentslist_write<U: Target>(val: &Proto012PsithacaOperationContentsList, buf: &mut U) -> usize {
    ::rust_runtime::Sequence::<Proto012PsithacaOperationAlphaContents>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacaoperationcontentslist_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaOperationContentsList> {
    Ok(::rust_runtime::Sequence::<Proto012PsithacaOperationAlphaContents>::parse(p)?)
}
