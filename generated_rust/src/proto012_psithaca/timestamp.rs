#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto012PsithacaTimestamp = i64;
#[allow(dead_code)]
pub fn proto012psithacatimestamp_write<U: Target>(val: &Proto012PsithacaTimestamp, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacatimestamp_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaTimestamp> {
    Ok(i64::parse(p)?)
}
