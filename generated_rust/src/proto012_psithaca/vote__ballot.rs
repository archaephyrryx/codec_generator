#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto012PsithacaVoteBallot = i8;
#[allow(dead_code)]
pub fn proto012psithacavoteballot_write<U: Target>(val: &Proto012PsithacaVoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacavoteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaVoteBallot> {
    Ok(i8::parse(p)?)
}
