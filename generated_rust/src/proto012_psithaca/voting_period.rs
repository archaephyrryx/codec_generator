#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto012PsithacaVotingPeriodKind,u8,proto012psithacavotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaVotingPeriod { pub index: i32, pub kind: Proto012PsithacaVotingPeriodKind, pub start_position: i32 }
#[allow(dead_code)]
pub fn proto012psithacavotingperiod_write<U: Target>(val: &Proto012PsithacaVotingPeriod, buf: &mut U) -> usize {
    i32::write_to(&val.index, buf) + Proto012PsithacaVotingPeriodKind::write_to(&val.kind, buf) + i32::write_to(&val.start_position, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacavotingperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaVotingPeriod> {
    Ok(Proto012PsithacaVotingPeriod {index: i32::parse(p)?, kind: Proto012PsithacaVotingPeriodKind::parse(p)?, start_position: i32::parse(p)?})
}
