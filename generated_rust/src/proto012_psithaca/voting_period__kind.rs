#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto012PsithacaVotingPeriodKind,u8,proto012psithacavotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[allow(dead_code)]
pub fn proto012psithacavotingperiodkind_write<U: Target>(val: &Proto012PsithacaVotingPeriodKind, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacavotingperiodkind_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaVotingPeriodKind> {
    Proto012PsithacaVotingPeriodKind::parse(p)
}
