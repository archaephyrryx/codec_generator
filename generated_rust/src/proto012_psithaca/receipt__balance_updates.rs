#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(Proto012PsithacaContractId,u8,proto012psithacacontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto012PsithacaContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto012PsithacaOperationMetadataAlphaBalance,u8,proto012psithacaoperationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto012PsithacaContractId, pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        1 => Legacy_rewards  { pub delegate: Proto012PsithacaOperationMetadataAlphaBalanceLegacyRewardsDelegate, pub cycle: i32, pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        2 => Block_fees  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        3 => Legacy_deposits  { pub delegate: Proto012PsithacaOperationMetadataAlphaBalanceLegacyDepositsDelegate, pub cycle: i32, pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        4 => Deposits  { pub delegate: Proto012PsithacaOperationMetadataAlphaBalanceDepositsDelegate, pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        5 => Nonce_revelation_rewards  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        6 => Double_signing_evidence_rewards  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        7 => Endorsing_rewards  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        8 => Baking_rewards  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        9 => Baking_bonuses  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        10 => Legacy_fees  { pub delegate: Proto012PsithacaOperationMetadataAlphaBalanceLegacyFeesDelegate, pub cycle: i32, pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        11 => Storage_fees  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        12 => Double_signing_punishments  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        13 => Lost_endorsing_rewards  { pub delegate: Proto012PsithacaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate, pub participation: bool, pub revelation: bool, pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        14 => Liquidity_baking_subsidies  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        15 => Burned  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        16 => Commitments  { pub committer: Proto012PsithacaOperationMetadataAlphaBalanceCommitmentsCommitter, pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        17 => Bootstrap  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        18 => Invoice  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        19 => Initial_commitments  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
        20 => Minted  { pub change: i64, pub origin: Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationMetadataAlphaBalanceCommitmentsCommitter { pub blinded_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationMetadataAlphaBalanceDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationMetadataAlphaBalanceLegacyDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationMetadataAlphaBalanceLegacyFeesDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationMetadataAlphaBalanceLegacyRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto012PsithacaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin,u8,proto012psithacaoperationmetadataalphaupdateoriginorigin,{
        0 => Block_application ,
        1 => Protocol_migration ,
        2 => Subsidy ,
        3 => Simulation ,
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto012PsithacaReceiptBalanceUpdates = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto012PsithacaOperationMetadataAlphaBalance>>;
#[allow(dead_code)]
pub fn proto012psithacareceiptbalanceupdates_write<U: Target>(val: &Proto012PsithacaReceiptBalanceUpdates, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto012PsithacaOperationMetadataAlphaBalance>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacareceiptbalanceupdates_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaReceiptBalanceUpdates> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto012PsithacaOperationMetadataAlphaBalance>>::parse(p)?)
}
