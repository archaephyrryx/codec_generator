#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto012PsithacaRawLevel = i32;
#[allow(dead_code)]
pub fn proto012psithacarawlevel_write<U: Target>(val: &Proto012PsithacaRawLevel, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacarawlevel_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaRawLevel> {
    Ok(i32::parse(p)?)
}
