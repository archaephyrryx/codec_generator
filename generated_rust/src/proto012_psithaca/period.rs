#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto012PsithacaPeriod = i64;
#[allow(dead_code)]
pub fn proto012psithacaperiod_write<U: Target>(val: &Proto012PsithacaPeriod, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacaperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaPeriod> {
    Ok(i64::parse(p)?)
}
