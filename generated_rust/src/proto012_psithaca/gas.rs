#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto012PsithacaGas,u8,proto012psithacagas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto012psithacagas_write<U: Target>(val: &Proto012PsithacaGas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacagas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaGas> {
    Proto012PsithacaGas::parse(p)
}
