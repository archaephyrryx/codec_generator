#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type Proto012PsithacaGasCost = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn proto012psithacagascost_write<U: Target>(val: &Proto012PsithacaGasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacagascost_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaGasCost> {
    Ok(::rust_runtime::Z::parse(p)?)
}
