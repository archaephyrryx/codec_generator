#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,i31,resolve_zero};
pub type Proto012PsithacaScriptLoc = ::rust_runtime::i31;
#[allow(dead_code)]
pub fn proto012psithacascriptloc_write<U: Target>(val: &Proto012PsithacaScriptLoc, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto012psithacascriptloc_parse<P: Parser>(p: &mut P) -> ParseResult<Proto012PsithacaScriptLoc> {
    Ok(::rust_runtime::i31::parse(p)?)
}
