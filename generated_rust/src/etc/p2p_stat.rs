#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,i31,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pStat { pub total_sent: i64, pub total_recv: i64, pub current_inflow: ::rust_runtime::i31, pub current_outflow: ::rust_runtime::i31 }
#[allow(dead_code)]
pub fn p2pstat_write<U: Target>(val: &P2pStat, buf: &mut U) -> usize {
    i64::write_to(&val.total_sent, buf) + i64::write_to(&val.total_recv, buf) + ::rust_runtime::i31::write_to(&val.current_inflow, buf) + ::rust_runtime::i31::write_to(&val.current_outflow, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn p2pstat_parse<P: Parser>(p: &mut P) -> ParseResult<P2pStat> {
    Ok(P2pStat {total_sent: i64::parse(p)?, total_recv: i64::parse(p)?, current_inflow: ::rust_runtime::i31::parse(p)?, current_outflow: ::rust_runtime::i31::parse(p)?})
}
