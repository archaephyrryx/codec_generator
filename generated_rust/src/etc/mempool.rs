#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct MempoolKnownValidDenestDynDenestSeq { pub operation_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct MempoolPendingDenestDynDenestDynDenestSeq { pub operation_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Mempool { pub known_valid: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<MempoolKnownValidDenestDynDenestSeq>>, pub pending: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<MempoolPendingDenestDynDenestDynDenestSeq>>> }
#[allow(dead_code)]
pub fn mempool_write<U: Target>(val: &Mempool, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<MempoolKnownValidDenestDynDenestSeq>>::write_to(&val.known_valid, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<MempoolPendingDenestDynDenestDynDenestSeq>>>::write_to(&val.pending, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn mempool_parse<P: Parser>(p: &mut P) -> ParseResult<Mempool> {
    Ok(Mempool {known_valid: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<MempoolKnownValidDenestDynDenestSeq>>::parse(p)?, pending: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<MempoolPendingDenestDynDenestDynDenestSeq>>>::parse(p)?})
}
