#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,cstyle,resolve_zero};
cstyle!(P2pPeerState,u8,{
        accepted = 0,
        running = 1,
        disconnected = 2
    }
    );
#[allow(dead_code)]
pub fn p2ppeerstate_write<U: Target>(val: &P2pPeerState, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn p2ppeerstate_parse<P: Parser>(p: &mut P) -> ParseResult<P2pPeerState> {
    P2pPeerState::parse(p)
}
