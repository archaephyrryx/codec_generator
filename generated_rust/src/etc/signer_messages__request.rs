#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,Nullable,ParseResult,Parser,Target,data,resolve_zero,u30};
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
        3 => Bls  { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesPublicKeyRequestPkh { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesRequestDeterministicNonceHashPkh { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesRequestDeterministicNonceHashSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesRequestDeterministicNoncePkh { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesRequestDeterministicNonceSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesRequestSignPkh { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesRequestSignSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesSupportsDeterministicNoncesRequestPkh { pub signature_public_key_hash: PublicKeyHash }
data!(SignerMessagesRequest,u8,signermessagesrequest,{
        0 => Sign  { pub pkh: SignerMessagesRequestSignPkh, pub data: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub signature: ::rust_runtime::Nullable<SignerMessagesRequestSignSignature> },
        1 => Public_key  { pub pkh: SignerMessagesPublicKeyRequestPkh },
        2 => Authorized_keys ,
        3 => Deterministic_nonce  { pub pkh: SignerMessagesRequestDeterministicNoncePkh, pub data: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub signature: ::rust_runtime::Nullable<SignerMessagesRequestDeterministicNonceSignature> },
        4 => Deterministic_nonce_hash  { pub pkh: SignerMessagesRequestDeterministicNonceHashPkh, pub data: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub signature: ::rust_runtime::Nullable<SignerMessagesRequestDeterministicNonceHashSignature> },
        5 => Supports_deterministic_nonces  { pub pkh: SignerMessagesSupportsDeterministicNoncesRequestPkh },
    }
    );
#[allow(dead_code)]
pub fn signermessagesrequest_write<U: Target>(val: &SignerMessagesRequest, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn signermessagesrequest_parse<P: Parser>(p: &mut P) -> ParseResult<SignerMessagesRequest> {
    SignerMessagesRequest::parse(p)
}
