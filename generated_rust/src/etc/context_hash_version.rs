#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type ContextHashVersion = u16;
#[allow(dead_code)]
pub fn contexthashversion_write<U: Target>(val: &ContextHashVersion, buf: &mut U) -> usize {
    u16::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn contexthashversion_parse<P: Parser>(p: &mut P) -> ParseResult<ContextHashVersion> {
    Ok(u16::parse(p)?)
}
