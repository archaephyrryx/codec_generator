#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointInfoExpectedPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointInfoLastDisconnection(pub P2pPointInfoLastDisconnectionIndex0,pub i64);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointInfoLastDisconnectionIndex0 { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointInfoLastEstablishedConnection(pub P2pPointInfoLastEstablishedConnectionIndex0,pub i64);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointInfoLastEstablishedConnectionIndex0 { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointInfoLastRejectedConnection(pub P2pPointInfoLastRejectedConnectionIndex0,pub i64);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointInfoLastRejectedConnectionIndex0 { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointInfoLastSeen(pub P2pPointInfoLastSeenIndex0,pub i64);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointInfoLastSeenIndex0 { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointInfoP2pPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
data!(P2pPointState,u8,p2ppointstate,{
        0 => Requested ,
        1 => Accepted  { pub p2p_peer_id: P2pPointStateAcceptedP2pPeerId },
        2 => Running  { pub p2p_peer_id: P2pPointStateRunningP2pPeerId },
        3 => Disconnected ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointStateAcceptedP2pPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointStateRunningP2pPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointInfo { pub trusted: bool, pub greylisted_until: std::option::Option<i64>, pub state: P2pPointState, pub p2p_peer_id: std::option::Option<P2pPointInfoP2pPeerId>, pub last_failed_connection: std::option::Option<i64>, pub last_rejected_connection: std::option::Option<P2pPointInfoLastRejectedConnection>, pub last_established_connection: std::option::Option<P2pPointInfoLastEstablishedConnection>, pub last_disconnection: std::option::Option<P2pPointInfoLastDisconnection>, pub last_seen: std::option::Option<P2pPointInfoLastSeen>, pub last_miss: std::option::Option<i64>, pub expected_peer_id: std::option::Option<P2pPointInfoExpectedPeerId> }
#[allow(dead_code)]
pub fn p2ppointinfo_write<U: Target>(val: &P2pPointInfo, buf: &mut U) -> usize {
    bool::write_to(&val.trusted, buf) + std::option::Option::<i64>::write_to(&val.greylisted_until, buf) + P2pPointState::write_to(&val.state, buf) + std::option::Option::<P2pPointInfoP2pPeerId>::write_to(&val.p2p_peer_id, buf) + std::option::Option::<i64>::write_to(&val.last_failed_connection, buf) + std::option::Option::<P2pPointInfoLastRejectedConnection>::write_to(&val.last_rejected_connection, buf) + std::option::Option::<P2pPointInfoLastEstablishedConnection>::write_to(&val.last_established_connection, buf) + std::option::Option::<P2pPointInfoLastDisconnection>::write_to(&val.last_disconnection, buf) + std::option::Option::<P2pPointInfoLastSeen>::write_to(&val.last_seen, buf) + std::option::Option::<i64>::write_to(&val.last_miss, buf) + std::option::Option::<P2pPointInfoExpectedPeerId>::write_to(&val.expected_peer_id, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn p2ppointinfo_parse<P: Parser>(p: &mut P) -> ParseResult<P2pPointInfo> {
    Ok(P2pPointInfo {trusted: bool::parse(p)?, greylisted_until: std::option::Option::<i64>::parse(p)?, state: P2pPointState::parse(p)?, p2p_peer_id: std::option::Option::<P2pPointInfoP2pPeerId>::parse(p)?, last_failed_connection: std::option::Option::<i64>::parse(p)?, last_rejected_connection: std::option::Option::<P2pPointInfoLastRejectedConnection>::parse(p)?, last_established_connection: std::option::Option::<P2pPointInfoLastEstablishedConnection>::parse(p)?, last_disconnection: std::option::Option::<P2pPointInfoLastDisconnection>::parse(p)?, last_seen: std::option::Option::<P2pPointInfoLastSeen>::parse(p)?, last_miss: std::option::Option::<i64>::parse(p)?, expected_peer_id: std::option::Option::<P2pPointInfoExpectedPeerId>::parse(p)?})
}
