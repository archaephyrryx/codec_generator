#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub protocol_data: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockLocatorHistoryDenestSeq { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockLocator { pub current_head: ::rust_runtime::Dynamic<::rust_runtime::u30,BlockHeader>, pub history: ::rust_runtime::Sequence<BlockLocatorHistoryDenestSeq> }
#[allow(dead_code)]
pub fn blocklocator_write<U: Target>(val: &BlockLocator, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,BlockHeader>::write_to(&val.current_head, buf) + ::rust_runtime::Sequence::<BlockLocatorHistoryDenestSeq>::write_to(&val.history, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn blocklocator_parse<P: Parser>(p: &mut P) -> ParseResult<BlockLocator> {
    Ok(BlockLocator {current_head: ::rust_runtime::Dynamic::<::rust_runtime::u30,BlockHeader>::parse(p)?, history: ::rust_runtime::Sequence::<BlockLocatorHistoryDenestSeq>::parse(p)?})
}
