#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(P2pPointPoolEventIndex1,u8,p2ppointpooleventindex1,{
        0 => Outgoing_request ,
        1 => Accepting_request  { pub p2p_peer_id: P2pPointPoolEventIndex1Index0AcceptingRequestP2pPeerId },
        2 => Rejecting_request  { pub p2p_peer_id: P2pPointPoolEventIndex1Index0RejectingRequestP2pPeerId },
        3 => Rejecting_rejected  { pub p2p_peer_id: std::option::Option<P2pPointPoolEventIndex1Index0RejectingRejectedP2pPeerId> },
        4 => Connection_established  { pub p2p_peer_id: P2pPointPoolEventIndex1Index0ConnectionEstablishedP2pPeerId },
        5 => Disconnection  { pub p2p_peer_id: P2pPointPoolEventIndex1Index0DisconnectionP2pPeerId },
        6 => External_disconnection  { pub p2p_peer_id: P2pPointPoolEventIndex1Index0ExternalDisconnectionP2pPeerId },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointPoolEventIndex1Index0AcceptingRequestP2pPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointPoolEventIndex1Index0ConnectionEstablishedP2pPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointPoolEventIndex1Index0DisconnectionP2pPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointPoolEventIndex1Index0ExternalDisconnectionP2pPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointPoolEventIndex1Index0RejectingRejectedP2pPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointPoolEventIndex1Index0RejectingRequestP2pPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointPoolEvent(pub i64,pub P2pPointPoolEventIndex1);
#[allow(dead_code)]
pub fn p2ppointpoolevent_write<U: Target>(val: &P2pPointPoolEvent, buf: &mut U) -> usize {
    i64::write_to(&val.0, buf) + P2pPointPoolEventIndex1::write_to(&val.1, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn p2ppointpoolevent_parse<P: Parser>(p: &mut P) -> ParseResult<P2pPointPoolEvent> {
    Ok(P2pPointPoolEvent(i64::parse(p)?, P2pPointPoolEventIndex1::parse(p)?))
}
