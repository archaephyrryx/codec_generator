#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionId { pub addr: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub port: std::option::Option<u16> }
#[allow(dead_code)]
pub fn p2pconnectionid_write<U: Target>(val: &P2pConnectionId, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(&val.addr, buf) + std::option::Option::<u16>::write_to(&val.port, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn p2pconnectionid_parse<P: Parser>(p: &mut P) -> ParseResult<P2pConnectionId> {
    Ok(P2pConnectionId {addr: ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?, port: std::option::Option::<u16>::parse(p)?})
}
