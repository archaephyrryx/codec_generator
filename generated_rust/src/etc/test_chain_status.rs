#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct TestChainStatusForkingProtocol { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct TestChainStatusRunningChainId { pub chain_id: ::rust_runtime::ByteString<4> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct TestChainStatusRunningGenesis { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct TestChainStatusRunningProtocol { pub protocol_hash: ::rust_runtime::ByteString<32> }
data!(TestChainStatus,u8,testchainstatus,{
        0 => Not_running ,
        1 => Forking  { pub protocol: TestChainStatusForkingProtocol, pub expiration: i64 },
        2 => Running  { pub chain_id: TestChainStatusRunningChainId, pub genesis: TestChainStatusRunningGenesis, pub protocol: TestChainStatusRunningProtocol, pub expiration: i64 },
    }
    );
#[allow(dead_code)]
pub fn testchainstatus_write<U: Target>(val: &TestChainStatus, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn testchainstatus_parse<P: Parser>(p: &mut P) -> ParseResult<TestChainStatus> {
    TestChainStatus::parse(p)
}
