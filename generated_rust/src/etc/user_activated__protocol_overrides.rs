#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct UserActivatedProtocolOverridesDenestDynDenestSeq { pub replaced_protocol: UserActivatedProtocolOverridesDenestDynDenestSeqReplacedProtocol, pub replacement_protocol: UserActivatedProtocolOverridesDenestDynDenestSeqReplacementProtocol }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct UserActivatedProtocolOverridesDenestDynDenestSeqReplacedProtocol { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct UserActivatedProtocolOverridesDenestDynDenestSeqReplacementProtocol { pub protocol_hash: ::rust_runtime::ByteString<32> }
pub type UserActivatedProtocolOverrides = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<UserActivatedProtocolOverridesDenestDynDenestSeq>>;
#[allow(dead_code)]
pub fn useractivatedprotocoloverrides_write<U: Target>(val: &UserActivatedProtocolOverrides, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<UserActivatedProtocolOverridesDenestDynDenestSeq>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn useractivatedprotocoloverrides_parse<P: Parser>(p: &mut P) -> ParseResult<UserActivatedProtocolOverrides> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<UserActivatedProtocolOverridesDenestDynDenestSeq>>::parse(p)?)
}
