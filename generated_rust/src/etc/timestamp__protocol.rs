#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type TimestampProtocol = i64;
#[allow(dead_code)]
pub fn timestampprotocol_write<U: Target>(val: &TimestampProtocol, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn timestampprotocol_parse<P: Parser>(p: &mut P) -> ParseResult<TimestampProtocol> {
    Ok(i64::parse(p)?)
}
