#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pIdentityPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pIdentity { pub peer_id: std::option::Option<P2pIdentityPeerId>, pub public_key: ::rust_runtime::ByteString<32>, pub secret_key: ::rust_runtime::ByteString<32>, pub proof_of_work_stamp: ::rust_runtime::ByteString<24> }
#[allow(dead_code)]
pub fn p2pidentity_write<U: Target>(val: &P2pIdentity, buf: &mut U) -> usize {
    std::option::Option::<P2pIdentityPeerId>::write_to(&val.peer_id, buf) + ::rust_runtime::ByteString::<32>::write_to(&val.public_key, buf) + ::rust_runtime::ByteString::<32>::write_to(&val.secret_key, buf) + ::rust_runtime::ByteString::<24>::write_to(&val.proof_of_work_stamp, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn p2pidentity_parse<P: Parser>(p: &mut P) -> ParseResult<P2pIdentity> {
    Ok(P2pIdentity {peer_id: std::option::Option::<P2pIdentityPeerId>::parse(p)?, public_key: ::rust_runtime::ByteString::<32>::parse(p)?, secret_key: ::rust_runtime::ByteString::<32>::parse(p)?, proof_of_work_stamp: ::rust_runtime::ByteString::<24>::parse(p)?})
}
