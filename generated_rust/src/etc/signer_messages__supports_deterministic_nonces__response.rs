#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesSupportsDeterministicNoncesResponse { pub bool: bool }
#[allow(dead_code)]
pub fn signermessagessupportsdeterministicnoncesresponse_write<U: Target>(val: &SignerMessagesSupportsDeterministicNoncesResponse, buf: &mut U) -> usize {
    bool::write_to(&val.bool, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn signermessagessupportsdeterministicnoncesresponse_parse<P: Parser>(p: &mut P) -> ParseResult<SignerMessagesSupportsDeterministicNoncesResponse> {
    Ok(SignerMessagesSupportsDeterministicNoncesResponse {bool: bool::parse(p)?})
}
