#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
        3 => Bls  { pub bls12_381_public_key: ::rust_runtime::ByteString<48> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesPublicKeyResponsePubkey { pub signature_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesPublicKeyResponse { pub pubkey: SignerMessagesPublicKeyResponsePubkey }
#[allow(dead_code)]
pub fn signermessagespublickeyresponse_write<U: Target>(val: &SignerMessagesPublicKeyResponse, buf: &mut U) -> usize {
    SignerMessagesPublicKeyResponsePubkey::write_to(&val.pubkey, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn signermessagespublickeyresponse_parse<P: Parser>(p: &mut P) -> ParseResult<SignerMessagesPublicKeyResponse> {
    Ok(SignerMessagesPublicKeyResponse {pubkey: SignerMessagesPublicKeyResponsePubkey::parse(p)?})
}
