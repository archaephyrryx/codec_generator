#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type P2pVersion = u16;
#[allow(dead_code)]
pub fn p2pversion_write<U: Target>(val: &P2pVersion, buf: &mut U) -> usize {
    u16::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn p2pversion_parse<P: Parser>(p: &mut P) -> ParseResult<P2pVersion> {
    Ok(u16::parse(p)?)
}
