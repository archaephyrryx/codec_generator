#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct ProtocolComponentsDenestDynDenestSeq { pub name: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub interface: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>, pub implementation: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Protocol { pub expected_env_version: u16, pub components: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<ProtocolComponentsDenestDynDenestSeq>> }
#[allow(dead_code)]
pub fn protocol_write<U: Target>(val: &Protocol, buf: &mut U) -> usize {
    u16::write_to(&val.expected_env_version, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<ProtocolComponentsDenestDynDenestSeq>>::write_to(&val.components, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn protocol_parse<P: Parser>(p: &mut P) -> ParseResult<Protocol> {
    Ok(Protocol {expected_env_version: u16::parse(p)?, components: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<ProtocolComponentsDenestDynDenestSeq>>::parse(p)?})
}
