#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct NetworkVersion { pub chain_name: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub distributed_db_version: u16, pub p2p_version: u16 }
#[allow(dead_code)]
pub fn networkversion_write<U: Target>(val: &NetworkVersion, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(&val.chain_name, buf) + u16::write_to(&val.distributed_db_version, buf) + u16::write_to(&val.p2p_version, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn networkversion_parse<P: Parser>(p: &mut P) -> ParseResult<NetworkVersion> {
    Ok(NetworkVersion {chain_name: ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?, distributed_db_version: u16::parse(p)?, p2p_version: u16::parse(p)?})
}
