#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type DistributedDbVersion = u16;
#[allow(dead_code)]
pub fn distributeddbversion_write<U: Target>(val: &DistributedDbVersion, buf: &mut U) -> usize {
    u16::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn distributeddbversion_parse<P: Parser>(p: &mut P) -> ParseResult<DistributedDbVersion> {
    Ok(u16::parse(p)?)
}
