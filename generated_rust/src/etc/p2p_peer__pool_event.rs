#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,cstyle,resolve_zero,u30};
cstyle!(P2pPeerPoolEventKind,u8,{
        incoming_request = 0,
        rejecting_request = 1,
        request_rejected = 2,
        connection_established = 3,
        disconnection = 4,
        external_disconnection = 5
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPeerPoolEvent { pub kind: P2pPeerPoolEventKind, pub timestamp: i64, pub addr: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub port: std::option::Option<u16> }
#[allow(dead_code)]
pub fn p2ppeerpoolevent_write<U: Target>(val: &P2pPeerPoolEvent, buf: &mut U) -> usize {
    P2pPeerPoolEventKind::write_to(&val.kind, buf) + i64::write_to(&val.timestamp, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(&val.addr, buf) + std::option::Option::<u16>::write_to(&val.port, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn p2ppeerpoolevent_parse<P: Parser>(p: &mut P) -> ParseResult<P2pPeerPoolEvent> {
    Ok(P2pPeerPoolEvent {kind: P2pPeerPoolEventKind::parse(p)?, timestamp: i64::parse(p)?, addr: ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?, port: std::option::Option::<u16>::parse(p)?})
}
