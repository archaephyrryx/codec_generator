#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct UserActivatedUpgradesDenestDynDenestSeq { pub level: i32, pub replacement_protocol: UserActivatedUpgradesDenestDynDenestSeqReplacementProtocol }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct UserActivatedUpgradesDenestDynDenestSeqReplacementProtocol { pub protocol_hash: ::rust_runtime::ByteString<32> }
pub type UserActivatedUpgrades = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<UserActivatedUpgradesDenestDynDenestSeq>>;
#[allow(dead_code)]
pub fn useractivatedupgrades_write<U: Target>(val: &UserActivatedUpgrades, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<UserActivatedUpgradesDenestDynDenestSeq>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn useractivatedupgrades_parse<P: Parser>(p: &mut P) -> ParseResult<UserActivatedUpgrades> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<UserActivatedUpgradesDenestDynDenestSeq>>::parse(p)?)
}
