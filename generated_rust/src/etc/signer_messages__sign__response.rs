#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Bytes,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesSignResponseSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesSignResponse { pub signature: SignerMessagesSignResponseSignature }
#[allow(dead_code)]
pub fn signermessagessignresponse_write<U: Target>(val: &SignerMessagesSignResponse, buf: &mut U) -> usize {
    SignerMessagesSignResponseSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn signermessagessignresponse_parse<P: Parser>(p: &mut P) -> ParseResult<SignerMessagesSignResponse> {
    Ok(SignerMessagesSignResponse {signature: SignerMessagesSignResponseSignature::parse(p)?})
}
