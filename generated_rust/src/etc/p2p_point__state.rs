#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointStateAcceptedP2pPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pPointStateRunningP2pPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
data!(P2pPointState,u8,p2ppointstate,{
        0 => Requested ,
        1 => Accepted  { pub p2p_peer_id: P2pPointStateAcceptedP2pPeerId },
        2 => Running  { pub p2p_peer_id: P2pPointStateRunningP2pPeerId },
        3 => Disconnected ,
    }
    );
#[allow(dead_code)]
pub fn p2ppointstate_write<U: Target>(val: &P2pPointState, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn p2ppointstate_parse<P: Parser>(p: &mut P) -> ParseResult<P2pPointState> {
    P2pPointState::parse(p)
}
