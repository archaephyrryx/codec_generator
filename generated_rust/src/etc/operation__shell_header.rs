#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeader { pub branch: OperationShellHeaderBranch }
#[allow(dead_code)]
pub fn operationshellheader_write<U: Target>(val: &OperationShellHeader, buf: &mut U) -> usize {
    OperationShellHeaderBranch::write_to(&val.branch, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn operationshellheader_parse<P: Parser>(p: &mut P) -> ParseResult<OperationShellHeader> {
    Ok(OperationShellHeader {branch: OperationShellHeaderBranch::parse(p)?})
}
