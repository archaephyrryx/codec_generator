#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Bytes,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SignerMessagesDeterministicNonceResponse { pub deterministic_nonce: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[allow(dead_code)]
pub fn signermessagesdeterministicnonceresponse_write<U: Target>(val: &SignerMessagesDeterministicNonceResponse, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::write_to(&val.deterministic_nonce, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn signermessagesdeterministicnonceresponse_parse<P: Parser>(p: &mut P) -> ParseResult<SignerMessagesDeterministicNonceResponse> {
    Ok(SignerMessagesDeterministicNonceResponse {deterministic_nonce: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::parse(p)?})
}
