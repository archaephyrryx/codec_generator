#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct ProtocolMetaHash { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct ProtocolMeta { pub hash: std::option::Option<ProtocolMetaHash>, pub expected_env_version: std::option::Option<u16>, pub modules: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> }
#[allow(dead_code)]
pub fn protocolmeta_write<U: Target>(val: &ProtocolMeta, buf: &mut U) -> usize {
    std::option::Option::<ProtocolMetaHash>::write_to(&val.hash, buf) + std::option::Option::<u16>::write_to(&val.expected_env_version, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>>>::write_to(&val.modules, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn protocolmeta_parse<P: Parser>(p: &mut P) -> ParseResult<ProtocolMeta> {
    Ok(ProtocolMeta {hash: std::option::Option::<ProtocolMetaHash>::parse(p)?, expected_env_version: std::option::Option::<u16>::parse(p)?, modules: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>>>::parse(p)?})
}
