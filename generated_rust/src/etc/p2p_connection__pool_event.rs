#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionId { pub addr: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub port: std::option::Option<u16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventAcceptingRequestPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventAdvertiseReceivedSource { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventAdvertiseSentSource { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventBootstrapReceivedSource { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventBootstrapSentSource { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventConnectionEstablishedPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventDisconnectionPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventExternalDisconnectionPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventNewPeerPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventRejectingRequestPeerId { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventRequestRejectedIdentity(pub P2pConnectionId,pub P2pConnectionPoolEventRequestRejectedIdentityIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventRequestRejectedIdentityIndex1 { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventSwapAckReceivedSource { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventSwapAckSentSource { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventSwapFailureSource { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventSwapRequestIgnoredSource { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventSwapRequestReceivedSource { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventSwapRequestSentSource { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct P2pConnectionPoolEventSwapSuccessSource { pub crypto_box_public_key_hash: ::rust_runtime::ByteString<16> }
data!(P2pConnectionPoolEvent,u8,p2pconnectionpoolevent,{
        0 => Too_few_connections ,
        1 => Too_many_connections ,
        2 => New_point  { pub point: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        3 => New_peer  { pub peer_id: P2pConnectionPoolEventNewPeerPeerId },
        4 => Incoming_connection  { pub point: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        5 => Outgoing_connection  { pub point: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        6 => Authentication_failed  { pub point: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        7 => Accepting_request  { pub point: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub id_point: P2pConnectionId, pub peer_id: P2pConnectionPoolEventAcceptingRequestPeerId },
        8 => Rejecting_request  { pub point: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub id_point: P2pConnectionId, pub peer_id: P2pConnectionPoolEventRejectingRequestPeerId },
        9 => Request_rejected  { pub point: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub identity: std::option::Option<P2pConnectionPoolEventRequestRejectedIdentity> },
        10 => Connection_established  { pub id_point: P2pConnectionId, pub peer_id: P2pConnectionPoolEventConnectionEstablishedPeerId },
        11 => Disconnection  { pub peer_id: P2pConnectionPoolEventDisconnectionPeerId },
        12 => External_disconnection  { pub peer_id: P2pConnectionPoolEventExternalDisconnectionPeerId },
        13 => Gc_points ,
        14 => Gc_peer_ids ,
        15 => Swap_request_received  { pub source: P2pConnectionPoolEventSwapRequestReceivedSource },
        16 => Swap_ack_received  { pub source: P2pConnectionPoolEventSwapAckReceivedSource },
        17 => Swap_request_sent  { pub source: P2pConnectionPoolEventSwapRequestSentSource },
        18 => Swap_ack_sent  { pub source: P2pConnectionPoolEventSwapAckSentSource },
        19 => Swap_request_ignored  { pub source: P2pConnectionPoolEventSwapRequestIgnoredSource },
        20 => Swap_success  { pub source: P2pConnectionPoolEventSwapSuccessSource },
        21 => Swap_failure  { pub source: P2pConnectionPoolEventSwapFailureSource },
        22 => Bootstrap_sent  { pub source: P2pConnectionPoolEventBootstrapSentSource },
        23 => Bootstrap_received  { pub source: P2pConnectionPoolEventBootstrapReceivedSource },
        24 => Advertise_sent  { pub source: P2pConnectionPoolEventAdvertiseSentSource },
        25 => Advertise_received  { pub source: P2pConnectionPoolEventAdvertiseReceivedSource },
    }
    );
#[allow(dead_code)]
pub fn p2pconnectionpoolevent_write<U: Target>(val: &P2pConnectionPoolEvent, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn p2pconnectionpoolevent_parse<P: Parser>(p: &mut P) -> ParseResult<P2pConnectionPoolEvent> {
    P2pConnectionPoolEvent::parse(p)
}
