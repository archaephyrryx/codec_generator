#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1BlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1BlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1BlockHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto007PsDELPH1BlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub signature: Proto007PsDELPH1BlockHeaderAlphaSignedContentsSignature }
#[allow(dead_code)]
pub fn proto007psdelph1blockheader_write<U: Target>(val: &Proto007PsDELPH1BlockHeader, buf: &mut U) -> usize {
    i32::write_to(&val.level, buf) + u8::write_to(&val.proto, buf) + BlockHeaderShellPredecessor::write_to(&val.predecessor, buf) + i64::write_to(&val.timestamp, buf) + u8::write_to(&val.validation_pass, buf) + BlockHeaderShellOperationsHash::write_to(&val.operations_hash, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>>>::write_to(&val.fitness, buf) + BlockHeaderShellContext::write_to(&val.context, buf) + u16::write_to(&val.priority, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto007PsDELPH1BlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + Proto007PsDELPH1BlockHeaderAlphaSignedContentsSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1blockheader_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1BlockHeader> {
    Ok(Proto007PsDELPH1BlockHeader {level: i32::parse(p)?, proto: u8::parse(p)?, predecessor: BlockHeaderShellPredecessor::parse(p)?, timestamp: i64::parse(p)?, validation_pass: u8::parse(p)?, operations_hash: BlockHeaderShellOperationsHash::parse(p)?, fitness: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>>>::parse(p)?, context: BlockHeaderShellContext::parse(p)?, priority: u16::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto007PsDELPH1BlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, signature: Proto007PsDELPH1BlockHeaderAlphaSignedContentsSignature::parse(p)?})
}
