#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type Proto007PsDELPH1GasCost = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn proto007psdelph1gascost_write<U: Target>(val: &Proto007PsDELPH1GasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1gascost_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1GasCost> {
    Ok(::rust_runtime::Z::parse(p)?)
}
