#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto007PsDELPH1Period = i64;
#[allow(dead_code)]
pub fn proto007psdelph1period_write<U: Target>(val: &Proto007PsDELPH1Period, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1period_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1Period> {
    Ok(i64::parse(p)?)
}
