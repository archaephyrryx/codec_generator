#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Padded,ParseResult,Parser,Target,data,resolve_zero,u30};
data!(Proto007PsDELPH1ContractId,u8,proto007psdelph1contractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto007PsDELPH1ContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1ContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto007PsDELPH1Entrypoint,u8,proto007psdelph1entrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1OperationAlphaInternalOperationDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1OperationAlphaInternalOperationOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1OperationAlphaInternalOperationRevealPublicKey { pub signature_v0_public_key: PublicKey }
data!(Proto007PsDELPH1OperationAlphaInternalOperationRhs,u8,proto007psdelph1operationalphainternaloperationrhs,{
        0 => Reveal  { pub public_key: Proto007PsDELPH1OperationAlphaInternalOperationRevealPublicKey },
        1 => Transaction  { pub amount: ::rust_runtime::N, pub destination: Proto007PsDELPH1ContractId, pub parameters: std::option::Option<Proto007PsDELPH1OperationAlphaInternalOperationTransactionParameters> },
        2 => Origination  { pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto007PsDELPH1OperationAlphaInternalOperationOriginationDelegate>, pub script: Proto007PsDELPH1ScriptedContracts },
        3 => Delegation  { pub delegate: std::option::Option<Proto007PsDELPH1OperationAlphaInternalOperationDelegationDelegate> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1OperationAlphaInternalOperationTransactionParameters { pub entrypoint: Proto007PsDELPH1Entrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1ScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1OperationInternal { pub source: Proto007PsDELPH1ContractId, pub nonce: u16, pub proto007_psdelph1_operation_alpha_internal_operation_rhs: Proto007PsDELPH1OperationAlphaInternalOperationRhs }
#[allow(dead_code)]
pub fn proto007psdelph1operationinternal_write<U: Target>(val: &Proto007PsDELPH1OperationInternal, buf: &mut U) -> usize {
    Proto007PsDELPH1ContractId::write_to(&val.source, buf) + u16::write_to(&val.nonce, buf) + Proto007PsDELPH1OperationAlphaInternalOperationRhs::write_to(&val.proto007_psdelph1_operation_alpha_internal_operation_rhs, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1operationinternal_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1OperationInternal> {
    Ok(Proto007PsDELPH1OperationInternal {source: Proto007PsDELPH1ContractId::parse(p)?, nonce: u16::parse(p)?, proto007_psdelph1_operation_alpha_internal_operation_rhs: Proto007PsDELPH1OperationAlphaInternalOperationRhs::parse(p)?})
}
