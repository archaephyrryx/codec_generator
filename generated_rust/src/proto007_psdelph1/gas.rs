#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto007PsDELPH1Gas,u8,proto007psdelph1gas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto007psdelph1gas_write<U: Target>(val: &Proto007PsDELPH1Gas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1gas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1Gas> {
    Proto007PsDELPH1Gas::parse(p)
}
