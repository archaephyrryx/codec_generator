#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(Proto007PsDELPH1ContractId,u8,proto007psdelph1contractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto007PsDELPH1ContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1ContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto007PsDELPH1OperationMetadataAlphaBalance,u8,proto007psdelph1operationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto007PsDELPH1ContractId, pub change: i64 },
        1 => Rewards  { pub delegate: Proto007PsDELPH1OperationMetadataAlphaBalanceRewardsDelegate, pub cycle: i32, pub change: i64 },
        2 => Fees  { pub delegate: Proto007PsDELPH1OperationMetadataAlphaBalanceFeesDelegate, pub cycle: i32, pub change: i64 },
        3 => Deposits  { pub delegate: Proto007PsDELPH1OperationMetadataAlphaBalanceDepositsDelegate, pub cycle: i32, pub change: i64 },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1OperationMetadataAlphaBalanceDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1OperationMetadataAlphaBalanceFeesDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1OperationMetadataAlphaBalanceRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto007PsDELPH1DelegateBalanceUpdates = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto007PsDELPH1OperationMetadataAlphaBalance>>;
#[allow(dead_code)]
pub fn proto007psdelph1delegatebalanceupdates_write<U: Target>(val: &Proto007PsDELPH1DelegateBalanceUpdates, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto007PsDELPH1OperationMetadataAlphaBalance>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1delegatebalanceupdates_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1DelegateBalanceUpdates> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto007PsDELPH1OperationMetadataAlphaBalance>>::parse(p)?)
}
