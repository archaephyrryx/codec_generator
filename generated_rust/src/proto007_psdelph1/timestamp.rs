#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto007PsDELPH1Timestamp = i64;
#[allow(dead_code)]
pub fn proto007psdelph1timestamp_write<U: Target>(val: &Proto007PsDELPH1Timestamp, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1timestamp_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1Timestamp> {
    Ok(i64::parse(p)?)
}
