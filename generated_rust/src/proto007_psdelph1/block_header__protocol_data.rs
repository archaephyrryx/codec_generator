#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1BlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1BlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1BlockHeaderProtocolData { pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto007PsDELPH1BlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub signature: Proto007PsDELPH1BlockHeaderAlphaSignedContentsSignature }
#[allow(dead_code)]
pub fn proto007psdelph1blockheaderprotocoldata_write<U: Target>(val: &Proto007PsDELPH1BlockHeaderProtocolData, buf: &mut U) -> usize {
    u16::write_to(&val.priority, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto007PsDELPH1BlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + Proto007PsDELPH1BlockHeaderAlphaSignedContentsSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1blockheaderprotocoldata_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1BlockHeaderProtocolData> {
    Ok(Proto007PsDELPH1BlockHeaderProtocolData {priority: u16::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto007PsDELPH1BlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, signature: Proto007PsDELPH1BlockHeaderAlphaSignedContentsSignature::parse(p)?})
}
