#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto007PsDELPH1VoteBallot = i8;
#[allow(dead_code)]
pub fn proto007psdelph1voteballot_write<U: Target>(val: &Proto007PsDELPH1VoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1voteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1VoteBallot> {
    Ok(i8::parse(p)?)
}
