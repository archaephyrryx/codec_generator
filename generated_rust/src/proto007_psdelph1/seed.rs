#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto007PsDELPH1Seed = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn proto007psdelph1seed_write<U: Target>(val: &Proto007PsDELPH1Seed, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1seed_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1Seed> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
