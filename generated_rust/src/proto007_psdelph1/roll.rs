#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto007PsDELPH1Roll = i32;
#[allow(dead_code)]
pub fn proto007psdelph1roll_write<U: Target>(val: &Proto007PsDELPH1Roll, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1roll_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1Roll> {
    Ok(i32::parse(p)?)
}
