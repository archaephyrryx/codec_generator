#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,i31,resolve_zero};
pub type Proto007PsDELPH1ScriptLoc = ::rust_runtime::i31;
#[allow(dead_code)]
pub fn proto007psdelph1scriptloc_write<U: Target>(val: &Proto007PsDELPH1ScriptLoc, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1scriptloc_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1ScriptLoc> {
    Ok(::rust_runtime::i31::parse(p)?)
}
