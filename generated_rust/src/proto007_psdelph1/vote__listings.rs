#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1VoteListingsDenestDynDenestSeq { pub pkh: Proto007PsDELPH1VoteListingsDenestDynDenestSeqPkh, pub rolls: i32 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto007PsDELPH1VoteListingsDenestDynDenestSeqPkh { pub signature_v0_public_key_hash: PublicKeyHash }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto007PsDELPH1VoteListings = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto007PsDELPH1VoteListingsDenestDynDenestSeq>>;
#[allow(dead_code)]
pub fn proto007psdelph1votelistings_write<U: Target>(val: &Proto007PsDELPH1VoteListings, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto007PsDELPH1VoteListingsDenestDynDenestSeq>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1votelistings_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1VoteListings> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto007PsDELPH1VoteListingsDenestDynDenestSeq>>::parse(p)?)
}
