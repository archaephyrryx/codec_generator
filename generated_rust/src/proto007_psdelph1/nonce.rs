#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto007PsDELPH1Nonce = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn proto007psdelph1nonce_write<U: Target>(val: &Proto007PsDELPH1Nonce, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1nonce_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1Nonce> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
