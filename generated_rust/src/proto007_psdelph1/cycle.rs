#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto007PsDELPH1Cycle = i32;
#[allow(dead_code)]
pub fn proto007psdelph1cycle_write<U: Target>(val: &Proto007PsDELPH1Cycle, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1cycle_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1Cycle> {
    Ok(i32::parse(p)?)
}
