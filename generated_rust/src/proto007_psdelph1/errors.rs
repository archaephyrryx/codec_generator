#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,ParseResult,Parser,Target,resolve_zero,u30};
pub type Proto007PsDELPH1Errors = ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>;
#[allow(dead_code)]
pub fn proto007psdelph1errors_write<U: Target>(val: &Proto007PsDELPH1Errors, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto007psdelph1errors_parse<P: Parser>(p: &mut P) -> ParseResult<Proto007PsDELPH1Errors> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?)
}
