#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,LimSeq,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingTransactionCiphertext { pub cv: ::rust_runtime::ByteString<32>, pub epk: ::rust_runtime::ByteString<32>, pub payload_enc: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub nonce_enc: ::rust_runtime::ByteString<24>, pub payload_out: ::rust_runtime::ByteString<80>, pub nonce_out: ::rust_runtime::ByteString<24> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingTransactionInput { pub cv: ::rust_runtime::ByteString<32>, pub nf: ::rust_runtime::ByteString<32>, pub rk: ::rust_runtime::ByteString<32>, pub proof_i: ::rust_runtime::ByteString<192>, pub signature: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingTransactionOutput { pub cm: ::rust_runtime::ByteString<32>, pub proof_o: ::rust_runtime::ByteString<192>, pub ciphertext: SaplingTransactionCiphertext }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingTransaction { pub inputs: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::LimSeq<SaplingTransactionInput,5208>>, pub outputs: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::LimSeq<SaplingTransactionOutput,2019>>, pub binding_sig: ::rust_runtime::ByteString<64>, pub balance: i64, pub root: ::rust_runtime::ByteString<32>, pub bound_data: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> }
#[allow(dead_code)]
pub fn saplingtransaction_write<U: Target>(val: &SaplingTransaction, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::LimSeq::<SaplingTransactionInput,5208>>::write_to(&val.inputs, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::LimSeq::<SaplingTransactionOutput,2019>>::write_to(&val.outputs, buf) + ::rust_runtime::ByteString::<64>::write_to(&val.binding_sig, buf) + i64::write_to(&val.balance, buf) + ::rust_runtime::ByteString::<32>::write_to(&val.root, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(&val.bound_data, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn saplingtransaction_parse<P: Parser>(p: &mut P) -> ParseResult<SaplingTransaction> {
    Ok(SaplingTransaction {inputs: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::LimSeq::<SaplingTransactionInput,5208>>::parse(p)?, outputs: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::LimSeq::<SaplingTransactionOutput,2019>>::parse(p)?, binding_sig: ::rust_runtime::ByteString::<64>::parse(p)?, balance: i64::parse(p)?, root: ::rust_runtime::ByteString::<32>::parse(p)?, bound_data: ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?})
}
