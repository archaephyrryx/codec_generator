#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type SaplingTransactionDiversifierIndex = i64;
#[allow(dead_code)]
pub fn saplingtransactiondiversifierindex_write<U: Target>(val: &SaplingTransactionDiversifierIndex, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn saplingtransactiondiversifierindex_parse<P: Parser>(p: &mut P) -> ParseResult<SaplingTransactionDiversifierIndex> {
    Ok(i64::parse(p)?)
}
