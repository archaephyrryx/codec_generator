#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingTransactionInput { pub cv: ::rust_runtime::ByteString<32>, pub nf: ::rust_runtime::ByteString<32>, pub rk: ::rust_runtime::ByteString<32>, pub proof_i: ::rust_runtime::ByteString<192>, pub signature: ::rust_runtime::ByteString<64> }
#[allow(dead_code)]
pub fn saplingtransactioninput_write<U: Target>(val: &SaplingTransactionInput, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(&val.cv, buf) + ::rust_runtime::ByteString::<32>::write_to(&val.nf, buf) + ::rust_runtime::ByteString::<32>::write_to(&val.rk, buf) + ::rust_runtime::ByteString::<192>::write_to(&val.proof_i, buf) + ::rust_runtime::ByteString::<64>::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn saplingtransactioninput_parse<P: Parser>(p: &mut P) -> ParseResult<SaplingTransactionInput> {
    Ok(SaplingTransactionInput {cv: ::rust_runtime::ByteString::<32>::parse(p)?, nf: ::rust_runtime::ByteString::<32>::parse(p)?, rk: ::rust_runtime::ByteString::<32>::parse(p)?, proof_i: ::rust_runtime::ByteString::<192>::parse(p)?, signature: ::rust_runtime::ByteString::<64>::parse(p)?})
}
