#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type SaplingTransactionCommitment = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn saplingtransactioncommitment_write<U: Target>(val: &SaplingTransactionCommitment, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn saplingtransactioncommitment_parse<P: Parser>(p: &mut P) -> ParseResult<SaplingTransactionCommitment> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
