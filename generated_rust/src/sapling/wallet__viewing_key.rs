#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingWalletFullViewingKey { pub ak: ::rust_runtime::ByteString<32>, pub nk: ::rust_runtime::ByteString<32>, pub ovk: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingWalletViewingKey { pub depth: ::rust_runtime::ByteString<1>, pub parent_fvk_tag: ::rust_runtime::ByteString<4>, pub child_index: ::rust_runtime::ByteString<4>, pub chain_code: ::rust_runtime::ByteString<32>, pub expsk: SaplingWalletFullViewingKey, pub dk: ::rust_runtime::ByteString<32> }
#[allow(dead_code)]
pub fn saplingwalletviewingkey_write<U: Target>(val: &SaplingWalletViewingKey, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<1>::write_to(&val.depth, buf) + ::rust_runtime::ByteString::<4>::write_to(&val.parent_fvk_tag, buf) + ::rust_runtime::ByteString::<4>::write_to(&val.child_index, buf) + ::rust_runtime::ByteString::<32>::write_to(&val.chain_code, buf) + SaplingWalletFullViewingKey::write_to(&val.expsk, buf) + ::rust_runtime::ByteString::<32>::write_to(&val.dk, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn saplingwalletviewingkey_parse<P: Parser>(p: &mut P) -> ParseResult<SaplingWalletViewingKey> {
    Ok(SaplingWalletViewingKey {depth: ::rust_runtime::ByteString::<1>::parse(p)?, parent_fvk_tag: ::rust_runtime::ByteString::<4>::parse(p)?, child_index: ::rust_runtime::ByteString::<4>::parse(p)?, chain_code: ::rust_runtime::ByteString::<32>::parse(p)?, expsk: SaplingWalletFullViewingKey::parse(p)?, dk: ::rust_runtime::ByteString::<32>::parse(p)?})
}
