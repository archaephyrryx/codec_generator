#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type SaplingTransactionBindingSig = ::rust_runtime::ByteString<64>;
#[allow(dead_code)]
pub fn saplingtransactionbindingsig_write<U: Target>(val: &SaplingTransactionBindingSig, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<64>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn saplingtransactionbindingsig_parse<P: Parser>(p: &mut P) -> ParseResult<SaplingTransactionBindingSig> {
    Ok(::rust_runtime::ByteString::<64>::parse(p)?)
}
