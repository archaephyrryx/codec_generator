#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingTransactionPlaintext { pub diversifier: ::rust_runtime::ByteString<11>, pub amount: i64, pub rcm: ::rust_runtime::ByteString<32>, pub memo: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[allow(dead_code)]
pub fn saplingtransactionplaintext_write<U: Target>(val: &SaplingTransactionPlaintext, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<11>::write_to(&val.diversifier, buf) + i64::write_to(&val.amount, buf) + ::rust_runtime::ByteString::<32>::write_to(&val.rcm, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::write_to(&val.memo, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn saplingtransactionplaintext_parse<P: Parser>(p: &mut P) -> ParseResult<SaplingTransactionPlaintext> {
    Ok(SaplingTransactionPlaintext {diversifier: ::rust_runtime::ByteString::<11>::parse(p)?, amount: i64::parse(p)?, rcm: ::rust_runtime::ByteString::<32>::parse(p)?, memo: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::parse(p)?})
}
