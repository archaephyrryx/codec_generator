#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingTransactionCiphertext { pub cv: ::rust_runtime::ByteString<32>, pub epk: ::rust_runtime::ByteString<32>, pub payload_enc: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub nonce_enc: ::rust_runtime::ByteString<24>, pub payload_out: ::rust_runtime::ByteString<80>, pub nonce_out: ::rust_runtime::ByteString<24> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingTransactionOutput { pub cm: ::rust_runtime::ByteString<32>, pub proof_o: ::rust_runtime::ByteString<192>, pub ciphertext: SaplingTransactionCiphertext }
#[allow(dead_code)]
pub fn saplingtransactionoutput_write<U: Target>(val: &SaplingTransactionOutput, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(&val.cm, buf) + ::rust_runtime::ByteString::<192>::write_to(&val.proof_o, buf) + SaplingTransactionCiphertext::write_to(&val.ciphertext, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn saplingtransactionoutput_parse<P: Parser>(p: &mut P) -> ParseResult<SaplingTransactionOutput> {
    Ok(SaplingTransactionOutput {cm: ::rust_runtime::ByteString::<32>::parse(p)?, proof_o: ::rust_runtime::ByteString::<192>::parse(p)?, ciphertext: SaplingTransactionCiphertext::parse(p)?})
}
