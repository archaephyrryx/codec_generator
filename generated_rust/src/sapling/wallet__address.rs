#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingWalletAddress { pub diversifier: ::rust_runtime::ByteString<11>, pub pkd: ::rust_runtime::ByteString<32> }
#[allow(dead_code)]
pub fn saplingwalletaddress_write<U: Target>(val: &SaplingWalletAddress, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<11>::write_to(&val.diversifier, buf) + ::rust_runtime::ByteString::<32>::write_to(&val.pkd, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn saplingwalletaddress_parse<P: Parser>(p: &mut P) -> ParseResult<SaplingWalletAddress> {
    Ok(SaplingWalletAddress {diversifier: ::rust_runtime::ByteString::<11>::parse(p)?, pkd: ::rust_runtime::ByteString::<32>::parse(p)?})
}
