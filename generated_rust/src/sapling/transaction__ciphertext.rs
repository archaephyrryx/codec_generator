#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct SaplingTransactionCiphertext { pub cv: ::rust_runtime::ByteString<32>, pub epk: ::rust_runtime::ByteString<32>, pub payload_enc: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub nonce_enc: ::rust_runtime::ByteString<24>, pub payload_out: ::rust_runtime::ByteString<80>, pub nonce_out: ::rust_runtime::ByteString<24> }
#[allow(dead_code)]
pub fn saplingtransactionciphertext_write<U: Target>(val: &SaplingTransactionCiphertext, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(&val.cv, buf) + ::rust_runtime::ByteString::<32>::write_to(&val.epk, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::write_to(&val.payload_enc, buf) + ::rust_runtime::ByteString::<24>::write_to(&val.nonce_enc, buf) + ::rust_runtime::ByteString::<80>::write_to(&val.payload_out, buf) + ::rust_runtime::ByteString::<24>::write_to(&val.nonce_out, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn saplingtransactionciphertext_parse<P: Parser>(p: &mut P) -> ParseResult<SaplingTransactionCiphertext> {
    Ok(SaplingTransactionCiphertext {cv: ::rust_runtime::ByteString::<32>::parse(p)?, epk: ::rust_runtime::ByteString::<32>::parse(p)?, payload_enc: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::parse(p)?, nonce_enc: ::rust_runtime::ByteString::<24>::parse(p)?, payload_out: ::rust_runtime::ByteString::<80>::parse(p)?, nonce_out: ::rust_runtime::ByteString::<24>::parse(p)?})
}
