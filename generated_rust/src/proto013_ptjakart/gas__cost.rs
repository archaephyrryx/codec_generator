#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type Proto013PtJakartGasCost = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn proto013ptjakartgascost_write<U: Target>(val: &Proto013PtJakartGasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartgascost_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartGasCost> {
    Ok(::rust_runtime::Z::parse(p)?)
}
