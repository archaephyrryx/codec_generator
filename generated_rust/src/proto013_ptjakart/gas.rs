#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto013PtJakartGas,u8,proto013ptjakartgas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto013ptjakartgas_write<U: Target>(val: &Proto013PtJakartGas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartgas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartGas> {
    Proto013PtJakartGas::parse(p)
}
