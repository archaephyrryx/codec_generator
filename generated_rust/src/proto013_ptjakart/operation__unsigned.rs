#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,FixSeq,N,Nullable,Padded,ParseResult,Parser,Sequence,Target,data,i31,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartBlockHeaderAlphaFullHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub payload_hash: Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: Proto013PtJakartBlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartBlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
data!(Proto013PtJakartContractId,u8,proto013ptjakartcontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto013PtJakartContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto013PtJakartEntrypoint,u8,proto013ptjakartentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartInlinedEndorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto013PtJakartInlinedEndorsementMempoolContents, pub signature: ::rust_runtime::Nullable<Proto013PtJakartInlinedEndorsementSignature> }
data!(Proto013PtJakartInlinedEndorsementMempoolContents,u8,proto013ptjakartinlinedendorsementmempoolcontents,{
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto013PtJakartInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartInlinedEndorsementSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartInlinedPreendorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto013PtJakartInlinedPreendorsementContents, pub signature: ::rust_runtime::Nullable<Proto013PtJakartInlinedPreendorsementSignature> }
data!(Proto013PtJakartInlinedPreendorsementContents,u8,proto013ptjakartinlinedpreendorsementcontents,{
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto013PtJakartInlinedPreendorsementContentsPreendorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartInlinedPreendorsementContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartInlinedPreendorsementSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
data!(Proto013PtJakartOperationAlphaContents,u8,proto013ptjakartoperationalphacontents,{
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32> },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto013PtJakartInlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto013PtJakartInlinedEndorsement> },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto013PtJakartBlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto013PtJakartBlockHeaderAlphaFullHeader> },
        4 => Activate_account  { pub pkh: Proto013PtJakartOperationAlphaContentsActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20> },
        5 => Proposals  { pub source: Proto013PtJakartOperationAlphaContentsProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>> },
        6 => Ballot  { pub source: Proto013PtJakartOperationAlphaContentsBallotSource, pub period: i32, pub proposal: Proto013PtJakartOperationAlphaContentsBallotProposal, pub ballot: i8 },
        7 => Double_preendorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto013PtJakartInlinedPreendorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto013PtJakartInlinedPreendorsement> },
        17 => Failing_noop  { pub arbitrary: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto013PtJakartOperationAlphaContentsPreendorsementBlockPayloadHash },
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto013PtJakartOperationAlphaContentsEndorsementBlockPayloadHash },
        107 => Reveal  { pub source: Proto013PtJakartOperationAlphaContentsRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: Proto013PtJakartOperationAlphaContentsRevealPublicKey },
        108 => Transaction  { pub source: Proto013PtJakartOperationAlphaContentsTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: Proto013PtJakartTransactionDestination, pub parameters: std::option::Option<Proto013PtJakartOperationAlphaContentsTransactionParameters> },
        109 => Origination  { pub source: Proto013PtJakartOperationAlphaContentsOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto013PtJakartOperationAlphaContentsOriginationDelegate>, pub script: Proto013PtJakartScriptedContracts },
        110 => Delegation  { pub source: Proto013PtJakartOperationAlphaContentsDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<Proto013PtJakartOperationAlphaContentsDelegationDelegate> },
        111 => Register_global_constant  { pub source: Proto013PtJakartOperationAlphaContentsRegisterGlobalConstantSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        112 => Set_deposits_limit  { pub source: Proto013PtJakartOperationAlphaContentsSetDepositsLimitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub limit: std::option::Option<::rust_runtime::N> },
        150 => Tx_rollup_origination  { pub source: Proto013PtJakartOperationAlphaContentsTxRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N },
        151 => Tx_rollup_submit_batch  { pub source: Proto013PtJakartOperationAlphaContentsTxRollupSubmitBatchSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto013PtJakartTxRollupId, pub content: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub burn_limit: std::option::Option<::rust_runtime::N> },
        152 => Tx_rollup_commit  { pub source: Proto013PtJakartOperationAlphaContentsTxRollupCommitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto013PtJakartTxRollupId, pub commitment: Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitment },
        153 => Tx_rollup_return_bond  { pub source: Proto013PtJakartOperationAlphaContentsTxRollupReturnBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto013PtJakartTxRollupId },
        154 => Tx_rollup_finalize_commitment  { pub source: Proto013PtJakartOperationAlphaContentsTxRollupFinalizeCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto013PtJakartTxRollupId },
        155 => Tx_rollup_remove_commitment  { pub source: Proto013PtJakartOperationAlphaContentsTxRollupRemoveCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto013PtJakartTxRollupId },
        156 => Tx_rollup_rejection  { pub source: Proto013PtJakartOperationAlphaContentsTxRollupRejectionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto013PtJakartTxRollupId, pub level: i32, pub message: Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage, pub message_position: ::rust_runtime::N, pub message_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq>>, pub message_result_hash: Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultHash, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq>>, pub previous_message_result: Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResult, pub previous_message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>>, pub proof: Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof },
        157 => Tx_rollup_dispatch_tickets  { pub source: Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub tx_rollup: Proto013PtJakartTxRollupId, pub level: i32, pub context_hash: Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsContextHash, pub message_index: ::rust_runtime::i31, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>>, pub tickets_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>> },
        158 => Transfer_ticket  { pub source: Proto013PtJakartOperationAlphaContentsTransferTicketSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub ticket_contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ticketer: Proto013PtJakartContractId, pub ticket_amount: ::rust_runtime::N, pub destination: Proto013PtJakartContractId, pub entrypoint: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        200 => Sc_rollup_originate  { pub source: Proto013PtJakartOperationAlphaContentsScRollupOriginateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub kind: Proto013PtJakartOperationAlphaContentsScRollupOriginateKind, pub boot_sector: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        201 => Sc_rollup_add_messages  { pub source: Proto013PtJakartOperationAlphaContentsScRollupAddMessagesSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub message: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        202 => Sc_rollup_cement  { pub source: Proto013PtJakartOperationAlphaContentsScRollupCementSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub commitment: Proto013PtJakartOperationAlphaContentsScRollupCementCommitment },
        203 => Sc_rollup_publish  { pub source: Proto013PtJakartOperationAlphaContentsScRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub commitment: Proto013PtJakartOperationAlphaContentsScRollupPublishCommitment },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsBallotSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsDelegationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsProposalsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsRegisterGlobalConstantSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsRevealPublicKey { pub signature_v0_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsRevealSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsScRollupAddMessagesSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsScRollupCementCommitment { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsScRollupCementSource { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto013PtJakartOperationAlphaContentsScRollupOriginateKind,u16,proto013ptjakartoperationalphacontentsscrolluporiginatekind,{
        0 => Example_arith_smart_contract_rollup_kind ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsScRollupOriginateSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsScRollupPublishCommitment { pub compressed_state: Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentCompressedState, pub inbox_level: i32, pub predecessor: Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentPredecessor, pub number_of_messages: i32, pub number_of_ticks: i32 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentCompressedState { pub state_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentPredecessor { pub commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsScRollupPublishSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsSetDepositsLimitSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTransactionParameters { pub entrypoint: Proto013PtJakartEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTransactionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTransferTicketSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitment { pub level: i32, pub messages: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq>>, pub predecessor: Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor, pub inbox_merkle_root: Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq { pub message_result_hash: ::rust_runtime::ByteString<32> }
data!(Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor,u8,proto013ptjakartoperationalphacontentstxrollupcommitcommitmentpredecessor,{
        0 => None ,
        1 => Some  { pub commitment_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupCommitSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq { pub contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticketer: Proto013PtJakartContractId, pub amount: Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount, pub claimer: Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer }
data!(Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,u8,proto013ptjakartoperationalphacontentstxrollupdispatchticketsticketsinfodenestdyndenestseqamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupFinalizeCommitmentSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage,u8,proto013ptjakartoperationalphacontentstxrolluprejectionmessage,{
        0 => Batch  { pub batch: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => Deposit  { pub deposit: Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDeposit },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDeposit { pub sender: Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender, pub destination: Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination, pub ticket_hash: Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash, pub amount: Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount }
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount,u8,proto013ptjakartoperationalphacontentstxrolluprejectionmessagedepositdepositamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination { pub tx_rollup_l2_address: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultHash { pub message_result_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResult { pub context_hash: Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash, pub withdraw_list_hash: Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash { pub withdraw_list_hash: ::rust_runtime::ByteString<32> }
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproof,{
        0 => case_0 (pub i16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index1,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq>>),
        1 => case_1 (pub i16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index1,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq>>),
        2 => case_2 (pub i16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index1,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq>>),
        3 => case_3 (pub i16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index1,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index2,pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq>>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseq,{
        0 => case_0 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1),
        1 => case_1 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1),
        2 => case_2 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1),
        3 => case_3 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1),
        4 => case_4 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1),
        5 => case_5 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1),
        6 => case_6 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1),
        7 => case_7 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1),
        8 => case_8 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1),
        9 => case_9 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1),
        10 => case_10 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1),
        11 => case_11 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1),
        12 => case_12 (pub u8,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1),
        13 => case_13 (pub u16,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1),
        14 => case_14 (pub i32,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1),
        15 => case_15 (pub i64,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1),
        128 => case_128 ,
        129 => case_129 (pub ::rust_runtime::FixSeq<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq,1>),
        130 => case_130 (pub ::rust_runtime::FixSeq<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq,2>),
        131 => case_131 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>>),
        192 => case_192 (pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>),
        193 => case_193 (pub ::rust_runtime::Dynamic<u16,::rust_runtime::Bytes>),
        195 => case_195 (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>),
        224 => case_224 (pub u8,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2),
        225 => case_225 (pub u16,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2),
        226 => case_226 (pub i32,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2),
        227 => case_227 (pub i64,pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq(pub ::rust_runtime::Dynamic<u8,::rust_runtime::Bytes>,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1);
data!(Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,u8,proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1,{
        0 => case_0  { pub context_hash: ::rust_runtime::ByteString<32> },
        1 => case_1  { pub context_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0,pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1(pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0,pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1(pub (),pub Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1(pub (),pub ());
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRejectionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupRemoveCommitmentSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupReturnBondSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationAlphaContentsTxRollupSubmitBatchSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(Proto013PtJakartTransactionDestination,u8,proto013ptjakarttransactiondestination,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto013PtJakartTransactionDestinationOriginatedDenestPad,1>),
        2 => Tx_rollup (pub ::rust_runtime::Padded<Proto013PtJakartTxRollupId,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartTransactionDestinationOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationUnsigned { pub branch: OperationShellHeaderBranch, pub contents: ::rust_runtime::Sequence<Proto013PtJakartOperationAlphaContents> }
#[allow(dead_code)]
pub fn proto013ptjakartoperationunsigned_write<U: Target>(val: &Proto013PtJakartOperationUnsigned, buf: &mut U) -> usize {
    OperationShellHeaderBranch::write_to(&val.branch, buf) + ::rust_runtime::Sequence::<Proto013PtJakartOperationAlphaContents>::write_to(&val.contents, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartoperationunsigned_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartOperationUnsigned> {
    Ok(Proto013PtJakartOperationUnsigned {branch: OperationShellHeaderBranch::parse(p)?, contents: ::rust_runtime::Sequence::<Proto013PtJakartOperationAlphaContents>::parse(p)?})
}
