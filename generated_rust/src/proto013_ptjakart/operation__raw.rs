#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationRaw { pub branch: OperationShellHeaderBranch, pub data: ::rust_runtime::Bytes }
#[allow(dead_code)]
pub fn proto013ptjakartoperationraw_write<U: Target>(val: &Proto013PtJakartOperationRaw, buf: &mut U) -> usize {
    OperationShellHeaderBranch::write_to(&val.branch, buf) + ::rust_runtime::Bytes::write_to(&val.data, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartoperationraw_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartOperationRaw> {
    Ok(Proto013PtJakartOperationRaw {branch: OperationShellHeaderBranch::parse(p)?, data: ::rust_runtime::Bytes::parse(p)?})
}
