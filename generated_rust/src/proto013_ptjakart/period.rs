#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto013PtJakartPeriod = i64;
#[allow(dead_code)]
pub fn proto013ptjakartperiod_write<U: Target>(val: &Proto013PtJakartPeriod, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartPeriod> {
    Ok(i64::parse(p)?)
}
