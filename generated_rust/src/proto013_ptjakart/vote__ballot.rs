#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto013PtJakartVoteBallot = i8;
#[allow(dead_code)]
pub fn proto013ptjakartvoteballot_write<U: Target>(val: &Proto013PtJakartVoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartvoteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartVoteBallot> {
    Ok(i8::parse(p)?)
}
