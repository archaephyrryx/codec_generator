#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto013PtJakartFitnessLockedRound,u8,proto013ptjakartfitnesslockedround,{
        0 => None ,
        1 => Some (pub i32),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartFitness { pub level: i32, pub locked_round: Proto013PtJakartFitnessLockedRound, pub predecessor_round: i32, pub round: i32 }
#[allow(dead_code)]
pub fn proto013ptjakartfitness_write<U: Target>(val: &Proto013PtJakartFitness, buf: &mut U) -> usize {
    i32::write_to(&val.level, buf) + Proto013PtJakartFitnessLockedRound::write_to(&val.locked_round, buf) + i32::write_to(&val.predecessor_round, buf) + i32::write_to(&val.round, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartfitness_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartFitness> {
    Ok(Proto013PtJakartFitness {level: i32::parse(p)?, locked_round: Proto013PtJakartFitnessLockedRound::parse(p)?, predecessor_round: i32::parse(p)?, round: i32::parse(p)?})
}
