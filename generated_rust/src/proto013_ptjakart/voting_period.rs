#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto013PtJakartVotingPeriodKind,u8,proto013ptjakartvotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartVotingPeriod { pub index: i32, pub kind: Proto013PtJakartVotingPeriodKind, pub start_position: i32 }
#[allow(dead_code)]
pub fn proto013ptjakartvotingperiod_write<U: Target>(val: &Proto013PtJakartVotingPeriod, buf: &mut U) -> usize {
    i32::write_to(&val.index, buf) + Proto013PtJakartVotingPeriodKind::write_to(&val.kind, buf) + i32::write_to(&val.start_position, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartvotingperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartVotingPeriod> {
    Ok(Proto013PtJakartVotingPeriod {index: i32::parse(p)?, kind: Proto013PtJakartVotingPeriodKind::parse(p)?, start_position: i32::parse(p)?})
}
