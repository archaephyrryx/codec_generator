#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto013PtJakartCycle = i32;
#[allow(dead_code)]
pub fn proto013ptjakartcycle_write<U: Target>(val: &Proto013PtJakartCycle, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartcycle_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartCycle> {
    Ok(i32::parse(p)?)
}
