#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(Proto013PtJakartBondId,u8,proto013ptjakartbondid,{
        0 => Tx_rollup_bond_id  { pub tx_rollup: Proto013PtJakartTxRollupId },
    }
    );
data!(Proto013PtJakartContractId,u8,proto013ptjakartcontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto013PtJakartContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto013PtJakartOperationMetadataAlphaBalance,u8,proto013ptjakartoperationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto013PtJakartContractId, pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        2 => Block_fees  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        4 => Deposits  { pub delegate: Proto013PtJakartOperationMetadataAlphaBalanceDepositsDelegate, pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        5 => Nonce_revelation_rewards  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        6 => Double_signing_evidence_rewards  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        7 => Endorsing_rewards  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        8 => Baking_rewards  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        9 => Baking_bonuses  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        11 => Storage_fees  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        12 => Double_signing_punishments  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        13 => Lost_endorsing_rewards  { pub delegate: Proto013PtJakartOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate, pub participation: bool, pub revelation: bool, pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        14 => Liquidity_baking_subsidies  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        15 => Burned  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        16 => Commitments  { pub committer: Proto013PtJakartOperationMetadataAlphaBalanceCommitmentsCommitter, pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        17 => Bootstrap  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        18 => Invoice  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        19 => Initial_commitments  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        20 => Minted  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        21 => Frozen_bonds  { pub contract: Proto013PtJakartContractId, pub bond_id: Proto013PtJakartBondId, pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        22 => Tx_rollup_rejection_rewards  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
        23 => Tx_rollup_rejection_punishments  { pub change: i64, pub origin: Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationMetadataAlphaBalanceCommitmentsCommitter { pub blinded_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationMetadataAlphaBalanceDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin,u8,proto013ptjakartoperationmetadataalphaupdateoriginorigin,{
        0 => Block_application ,
        1 => Protocol_migration ,
        2 => Subsidy ,
        3 => Simulation ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto013PtJakartReceiptBalanceUpdates = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto013PtJakartOperationMetadataAlphaBalance>>;
#[allow(dead_code)]
pub fn proto013ptjakartreceiptbalanceupdates_write<U: Target>(val: &Proto013PtJakartReceiptBalanceUpdates, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto013PtJakartOperationMetadataAlphaBalance>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartreceiptbalanceupdates_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartReceiptBalanceUpdates> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto013PtJakartOperationMetadataAlphaBalance>>::parse(p)?)
}
