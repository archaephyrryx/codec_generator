#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto013PtJakartVotingPeriodKind,u8,proto013ptjakartvotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[allow(dead_code)]
pub fn proto013ptjakartvotingperiodkind_write<U: Target>(val: &Proto013PtJakartVotingPeriodKind, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartvotingperiodkind_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartVotingPeriodKind> {
    Proto013PtJakartVotingPeriodKind::parse(p)
}
