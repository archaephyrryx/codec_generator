#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,i31,resolve_zero};
pub type Proto013PtJakartScriptLoc = ::rust_runtime::i31;
#[allow(dead_code)]
pub fn proto013ptjakartscriptloc_write<U: Target>(val: &Proto013PtJakartScriptLoc, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartscriptloc_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartScriptLoc> {
    Ok(::rust_runtime::i31::parse(p)?)
}
