#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto013PtJakartRawLevel = i32;
#[allow(dead_code)]
pub fn proto013ptjakartrawlevel_write<U: Target>(val: &Proto013PtJakartRawLevel, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartrawlevel_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartRawLevel> {
    Ok(i32::parse(p)?)
}
