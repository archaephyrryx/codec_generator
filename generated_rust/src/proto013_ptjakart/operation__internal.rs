#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Padded,ParseResult,Parser,Target,data,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartApplyResultsAlphaInternalOperationResultDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartApplyResultsAlphaInternalOperationResultOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs,u8,proto013ptjakartapplyresultsalphainternaloperationresultrhs,{
        1 => Transaction  { pub amount: ::rust_runtime::N, pub destination: Proto013PtJakartTransactionDestination, pub parameters: std::option::Option<Proto013PtJakartApplyResultsAlphaInternalOperationResultTransactionParameters> },
        2 => Origination  { pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto013PtJakartApplyResultsAlphaInternalOperationResultOriginationDelegate>, pub script: Proto013PtJakartScriptedContracts },
        3 => Delegation  { pub delegate: std::option::Option<Proto013PtJakartApplyResultsAlphaInternalOperationResultDelegationDelegate> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartApplyResultsAlphaInternalOperationResultTransactionParameters { pub entrypoint: Proto013PtJakartEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(Proto013PtJakartContractId,u8,proto013ptjakartcontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto013PtJakartContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto013PtJakartEntrypoint,u8,proto013ptjakartentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(Proto013PtJakartTransactionDestination,u8,proto013ptjakarttransactiondestination,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto013PtJakartTransactionDestinationOriginatedDenestPad,1>),
        2 => Tx_rollup (pub ::rust_runtime::Padded<Proto013PtJakartTxRollupId,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartTransactionDestinationOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartOperationInternal { pub source: Proto013PtJakartContractId, pub nonce: u16, pub proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs: Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs }
#[allow(dead_code)]
pub fn proto013ptjakartoperationinternal_write<U: Target>(val: &Proto013PtJakartOperationInternal, buf: &mut U) -> usize {
    Proto013PtJakartContractId::write_to(&val.source, buf) + u16::write_to(&val.nonce, buf) + Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs::write_to(&val.proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartoperationinternal_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartOperationInternal> {
    Ok(Proto013PtJakartOperationInternal {source: Proto013PtJakartContractId::parse(p)?, nonce: u16::parse(p)?, proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs: Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs::parse(p)?})
}
