#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartBlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto013PtJakartBlockHeaderProtocolData { pub payload_hash: Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: Proto013PtJakartBlockHeaderAlphaSignedContentsSignature }
#[allow(dead_code)]
pub fn proto013ptjakartblockheaderprotocoldata_write<U: Target>(val: &Proto013PtJakartBlockHeaderProtocolData, buf: &mut U) -> usize {
    Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash::write_to(&val.payload_hash, buf) + i32::write_to(&val.payload_round, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + i8::write_to(&val.liquidity_baking_toggle_vote, buf) + Proto013PtJakartBlockHeaderAlphaSignedContentsSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto013ptjakartblockheaderprotocoldata_parse<P: Parser>(p: &mut P) -> ParseResult<Proto013PtJakartBlockHeaderProtocolData> {
    Ok(Proto013PtJakartBlockHeaderProtocolData {payload_hash: Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash::parse(p)?, payload_round: i32::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, liquidity_baking_toggle_vote: i8::parse(p)?, signature: Proto013PtJakartBlockHeaderAlphaSignedContentsSignature::parse(p)?})
}
