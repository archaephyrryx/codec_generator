#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{AutoBox,ByteString,Bytes,Decode,Dynamic,Encode,Estimable,LimSeq,N,Nullable,Padded,ParseResult,Parser,Sequence,Target,Z,cstyle,data,i31,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
data!(MichelineProto016PtMumbaiMichelsonV1Expression,u8,michelineproto016ptmumbaimichelsonv1expression,{
        0 => Int  { pub int: ::rust_runtime::Z },
        1 => String  { pub string: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        2 => Sequence (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>>>),
        3 => Prim__no_args__no_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives },
        4 => Prim__no_args__some_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        5 => Prim__1_arg__no_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression> },
        6 => Prim__1_arg__some_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        7 => Prim__2_args__no_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression> },
        8 => Prim__2_args__some_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        9 => Prim__generic  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub args: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>>>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        10 => Bytes  { pub bytes: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderAlphaFullHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub payload_hash: Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
data!(Proto016PtMumbaiContractId,u8,proto016ptmumbaicontractid,{
        0 => Implicit  { pub signature_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto016PtMumbaiContractIdOriginatedDenestPad,1>),
    }
    );
data!(Proto016PtMumbaiContractIdOriginated,u8,proto016ptmumbaicontractidoriginated,{
        1 => Originated (pub ::rust_runtime::Padded<Proto016PtMumbaiContractIdOriginatedOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiContractIdOriginatedOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto016PtMumbaiEntrypoint,u8,proto016ptmumbaientrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        5 => deposit ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedEndorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto016PtMumbaiInlinedEndorsementMempoolContents, pub signature: ::rust_runtime::Nullable<Proto016PtMumbaiInlinedEndorsementSignature> }
data!(Proto016PtMumbaiInlinedEndorsementMempoolContents,u8,proto016ptmumbaiinlinedendorsementmempoolcontents,{
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto016PtMumbaiInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedEndorsementSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedPreendorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto016PtMumbaiInlinedPreendorsementContents, pub signature: ::rust_runtime::Nullable<Proto016PtMumbaiInlinedPreendorsementSignature> }
data!(Proto016PtMumbaiInlinedPreendorsementContents,u8,proto016ptmumbaiinlinedpreendorsementcontents,{
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto016PtMumbaiInlinedPreendorsementContentsPreendorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedPreendorsementContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedPreendorsementSignature { pub signature_v1: ::rust_runtime::Bytes }
cstyle!(Proto016PtMumbaiMichelsonV1Primitives,u8,{
        parameter = 0,
        storage = 1,
        code = 2,
        False = 3,
        Elt = 4,
        Left = 5,
        None = 6,
        Pair = 7,
        Right = 8,
        Some = 9,
        True = 10,
        Unit = 11,
        PACK = 12,
        UNPACK = 13,
        BLAKE2B = 14,
        SHA256 = 15,
        SHA512 = 16,
        ABS = 17,
        ADD = 18,
        AMOUNT = 19,
        AND = 20,
        BALANCE = 21,
        CAR = 22,
        CDR = 23,
        CHECK_SIGNATURE = 24,
        COMPARE = 25,
        CONCAT = 26,
        CONS = 27,
        CREATE_ACCOUNT = 28,
        CREATE_CONTRACT = 29,
        IMPLICIT_ACCOUNT = 30,
        DIP = 31,
        DROP = 32,
        DUP = 33,
        EDIV = 34,
        EMPTY_MAP = 35,
        EMPTY_SET = 36,
        EQ = 37,
        EXEC = 38,
        FAILWITH = 39,
        GE = 40,
        GET = 41,
        GT = 42,
        HASH_KEY = 43,
        IF = 44,
        IF_CONS = 45,
        IF_LEFT = 46,
        IF_NONE = 47,
        INT = 48,
        LAMBDA = 49,
        LE = 50,
        LEFT = 51,
        LOOP = 52,
        LSL = 53,
        LSR = 54,
        LT = 55,
        MAP = 56,
        MEM = 57,
        MUL = 58,
        NEG = 59,
        NEQ = 60,
        NIL = 61,
        NONE = 62,
        NOT = 63,
        NOW = 64,
        OR = 65,
        PAIR = 66,
        PUSH = 67,
        RIGHT = 68,
        SIZE = 69,
        SOME = 70,
        SOURCE = 71,
        SENDER = 72,
        SELF = 73,
        STEPS_TO_QUOTA = 74,
        SUB = 75,
        SWAP = 76,
        TRANSFER_TOKENS = 77,
        SET_DELEGATE = 78,
        UNIT = 79,
        UPDATE = 80,
        XOR = 81,
        ITER = 82,
        LOOP_LEFT = 83,
        ADDRESS = 84,
        CONTRACT = 85,
        ISNAT = 86,
        CAST = 87,
        RENAME = 88,
        bool = 89,
        contract = 90,
        int = 91,
        key = 92,
        key_hash = 93,
        lambda = 94,
        list = 95,
        map = 96,
        big_map = 97,
        nat = 98,
        option = 99,
        or = 100,
        pair = 101,
        set = 102,
        signature = 103,
        string = 104,
        bytes = 105,
        mutez = 106,
        timestamp = 107,
        unit = 108,
        operation = 109,
        address = 110,
        SLICE = 111,
        DIG = 112,
        DUG = 113,
        EMPTY_BIG_MAP = 114,
        APPLY = 115,
        chain_id = 116,
        CHAIN_ID = 117,
        LEVEL = 118,
        SELF_ADDRESS = 119,
        never = 120,
        NEVER = 121,
        UNPAIR = 122,
        VOTING_POWER = 123,
        TOTAL_VOTING_POWER = 124,
        KECCAK = 125,
        SHA3 = 126,
        PAIRING_CHECK = 127,
        bls12_381_g1 = 128,
        bls12_381_g2 = 129,
        bls12_381_fr = 130,
        sapling_state = 131,
        sapling_transaction_deprecated = 132,
        SAPLING_EMPTY_STATE = 133,
        SAPLING_VERIFY_UPDATE = 134,
        ticket = 135,
        TICKET_DEPRECATED = 136,
        READ_TICKET = 137,
        SPLIT_TICKET = 138,
        JOIN_TICKETS = 139,
        GET_AND_UPDATE = 140,
        chest = 141,
        chest_key = 142,
        OPEN_CHEST = 143,
        VIEW = 144,
        view = 145,
        constant = 146,
        SUB_MUTEZ = 147,
        tx_rollup_l2_address = 148,
        MIN_BLOCK_TIME = 149,
        sapling_transaction = 150,
        EMIT = 151,
        Lambda_rec = 152,
        LAMBDA_REC = 153,
        TICKET = 154,
        BYTES = 155,
        NAT = 156
    }
    );
data!(Proto016PtMumbaiOperationAlphaContents,u8,proto016ptmumbaioperationalphacontents,{
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32> },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiInlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiInlinedEndorsement> },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiBlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiBlockHeaderAlphaFullHeader> },
        4 => Activate_account  { pub pkh: Proto016PtMumbaiOperationAlphaContentsActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20> },
        5 => Proposals  { pub source: Proto016PtMumbaiOperationAlphaContentsProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::LimSeq<Proto016PtMumbaiOperationAlphaContentsProposalsProposalsDenestDynDenestSeq,20>> },
        6 => Ballot  { pub source: Proto016PtMumbaiOperationAlphaContentsBallotSource, pub period: i32, pub proposal: Proto016PtMumbaiOperationAlphaContentsBallotProposal, pub ballot: i8 },
        7 => Double_preendorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiInlinedPreendorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiInlinedPreendorsement> },
        8 => Vdf_revelation  { pub solution: Proto016PtMumbaiOperationAlphaContentsVdfRevelationSolution },
        9 => Drain_delegate  { pub consensus_key: Proto016PtMumbaiOperationAlphaContentsDrainDelegateConsensusKey, pub delegate: Proto016PtMumbaiOperationAlphaContentsDrainDelegateDelegate, pub destination: Proto016PtMumbaiOperationAlphaContentsDrainDelegateDestination },
        17 => Failing_noop  { pub arbitrary: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto016PtMumbaiOperationAlphaContentsPreendorsementBlockPayloadHash },
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto016PtMumbaiOperationAlphaContentsEndorsementBlockPayloadHash },
        22 => Dal_attestation  { pub attestor: Proto016PtMumbaiOperationAlphaContentsDalAttestationAttestor, pub attestation: ::rust_runtime::Z, pub level: i32 },
        107 => Reveal  { pub source: Proto016PtMumbaiOperationAlphaContentsRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: Proto016PtMumbaiOperationAlphaContentsRevealPublicKey },
        108 => Transaction  { pub source: Proto016PtMumbaiOperationAlphaContentsTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: Proto016PtMumbaiContractId, pub parameters: std::option::Option<Proto016PtMumbaiOperationAlphaContentsTransactionParameters> },
        109 => Origination  { pub source: Proto016PtMumbaiOperationAlphaContentsOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto016PtMumbaiOperationAlphaContentsOriginationDelegate>, pub script: Proto016PtMumbaiScriptedContracts },
        110 => Delegation  { pub source: Proto016PtMumbaiOperationAlphaContentsDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<Proto016PtMumbaiOperationAlphaContentsDelegationDelegate> },
        111 => Register_global_constant  { pub source: Proto016PtMumbaiOperationAlphaContentsRegisterGlobalConstantSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        112 => Set_deposits_limit  { pub source: Proto016PtMumbaiOperationAlphaContentsSetDepositsLimitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub limit: std::option::Option<::rust_runtime::N> },
        113 => Increase_paid_storage  { pub source: Proto016PtMumbaiOperationAlphaContentsIncreasePaidStorageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::Z, pub destination: Proto016PtMumbaiContractIdOriginated },
        114 => Update_consensus_key  { pub source: Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeySource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub pk: Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeyPk },
        150 => Tx_rollup_origination  { pub source: Proto016PtMumbaiOperationAlphaContentsTxRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N },
        151 => Tx_rollup_submit_batch  { pub source: Proto016PtMumbaiOperationAlphaContentsTxRollupSubmitBatchSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId, pub content: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub burn_limit: std::option::Option<::rust_runtime::N> },
        152 => Tx_rollup_commit  { pub source: Proto016PtMumbaiOperationAlphaContentsTxRollupCommitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId, pub commitment: Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitment },
        153 => Tx_rollup_return_bond  { pub source: Proto016PtMumbaiOperationAlphaContentsTxRollupReturnBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId },
        154 => Tx_rollup_finalize_commitment  { pub source: Proto016PtMumbaiOperationAlphaContentsTxRollupFinalizeCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId },
        155 => Tx_rollup_remove_commitment  { pub source: Proto016PtMumbaiOperationAlphaContentsTxRollupRemoveCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId },
        156 => Tx_rollup_rejection  { pub source: Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId, pub level: i32, pub message: Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage, pub message_position: ::rust_runtime::N, pub message_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq>>, pub message_result_hash: Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultHash, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq>>, pub previous_message_result: Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResult, pub previous_message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>>, pub proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        157 => Tx_rollup_dispatch_tickets  { pub source: Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub tx_rollup: Proto016PtMumbaiTxRollupId, pub level: i32, pub context_hash: Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsContextHash, pub message_index: ::rust_runtime::i31, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>>, pub tickets_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>> },
        158 => Transfer_ticket  { pub source: Proto016PtMumbaiOperationAlphaContentsTransferTicketSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub ticket_contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ticketer: Proto016PtMumbaiContractId, pub ticket_amount: ::rust_runtime::N, pub destination: Proto016PtMumbaiContractId, pub entrypoint: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        200 => Smart_rollup_originate  { pub source: Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub pvm_kind: Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginatePvmKind, pub kernel: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub origination_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub parameters_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        201 => Smart_rollup_add_messages  { pub source: Proto016PtMumbaiOperationAlphaContentsSmartRollupAddMessagesSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub message: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        202 => Smart_rollup_cement  { pub source: Proto016PtMumbaiOperationAlphaContentsSmartRollupCementSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiSmartRollupAddress, pub commitment: Proto016PtMumbaiOperationAlphaContentsSmartRollupCementCommitment },
        203 => Smart_rollup_publish  { pub source: Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiSmartRollupAddress, pub commitment: Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitment },
        204 => Smart_rollup_refute  { pub source: Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiSmartRollupAddress, pub opponent: Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteOpponent, pub refutation: Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation },
        205 => Smart_rollup_timeout  { pub source: Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiSmartRollupAddress, pub stakers: Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakers },
        206 => Smart_rollup_execute_outbox_message  { pub source: Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiSmartRollupAddress, pub cemented_commitment: Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment, pub output_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        207 => Smart_rollup_recover_bond  { pub source: Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondRollup, pub staker: Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondStaker },
        230 => Dal_publish_slot_header  { pub source: Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub slot_header: Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeader },
        250 => Zk_rollup_origination  { pub source: Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_parameters: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub circuits_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq>>, pub init_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub nb_ops: ::rust_runtime::i31 },
        251 => Zk_rollup_publish  { pub source: Proto016PtMumbaiOperationAlphaContentsZkRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub zk_rollup: Proto016PtMumbaiOperationAlphaContentsZkRollupPublishZkRollup, pub op: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq>> },
        252 => Zk_rollup_update  { pub source: Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub zk_rollup: Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateZkRollup, pub update: Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdate },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsBallotSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsDalAttestationAttestor { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeader { pub level: i32, pub index: u8, pub commitment: Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment, pub commitment_proof: ::rust_runtime::ByteString<48> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment { pub dal_commitment: ::rust_runtime::ByteString<48> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsDelegationDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsDelegationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsDrainDelegateConsensusKey { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsDrainDelegateDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsDrainDelegateDestination { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsIncreasePaidStorageSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOriginationDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOriginationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsProposalsSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsRegisterGlobalConstantSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsRevealPublicKey { pub signature_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsRevealSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSetDepositsLimitSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupAddMessagesSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupCementCommitment { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupCementSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageSource { pub signature_public_key_hash: PublicKeyHash }
cstyle!(Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginatePvmKind,u8,{
        arith = 0,
        wasm_2_0_0 = 1
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginateSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitment { pub compressed_state: Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentCompressedState, pub inbox_level: i32, pub predecessor: Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentPredecessor, pub number_of_ticks: i64 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentCompressedState { pub smart_rollup_state_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentPredecessor { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondRollup { pub smart_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondStaker { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteOpponent { pub signature_public_key_hash: PublicKeyHash }
data!(Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation,u8,proto016ptmumbaioperationalphacontentssmartrolluprefuterefutation,{
        0 => Start  { pub player_commitment_hash: Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash, pub opponent_commitment_hash: Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash },
        1 => Move  { pub choice: ::rust_runtime::N, pub step: Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep },
    }
    );
data!(Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep,u8,proto016ptmumbaioperationalphacontentssmartrolluprefuterefutationmovestep,{
        0 => Dissection (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq>>),
        1 => Proof  { pub pvm_step: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub input_proof: std::option::Option<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq { pub state: std::option::Option<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState>, pub tick: ::rust_runtime::N }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState { pub smart_rollup_state_hash: ::rust_runtime::ByteString<32> }
data!(Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof,u8,proto016ptmumbaioperationalphacontentssmartrolluprefuterefutationmovestepproofinputproof,{
        0 => inbox_proof  { pub level: i32, pub message_counter: ::rust_runtime::N, pub serialized_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => reveal_proof  { pub reveal_proof: Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof },
        2 => first_input ,
    }
    );
data!(Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof,u8,proto016ptmumbaioperationalphacontentssmartrolluprefuterefutationmovestepproofinputproofrevealproofrevealproof,{
        0 => raw_data_proof  { pub raw_data: ::rust_runtime::Dynamic<u16,std::string::String> },
        1 => metadata_proof ,
        2 => dal_page_proof  { pub dal_page_id: Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId, pub dal_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId { pub published_level: i32, pub slot_index: u8, pub page_index: i16 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakers { pub alice: Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersAlice, pub bob: Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersBob }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersAlice { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersBob { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTransactionParameters { pub entrypoint: Proto016PtMumbaiEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTransactionSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTransferTicketSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitment { pub level: i32, pub messages: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq>>, pub predecessor: Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor, pub inbox_merkle_root: Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq { pub message_result_hash: ::rust_runtime::ByteString<32> }
data!(Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor,u8,proto016ptmumbaioperationalphacontentstxrollupcommitcommitmentpredecessor,{
        0 => None ,
        1 => Some  { pub commitment_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupCommitSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq { pub contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticketer: Proto016PtMumbaiContractId, pub amount: Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount, pub claimer: Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer }
data!(Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,u8,proto016ptmumbaioperationalphacontentstxrollupdispatchticketsticketsinfodenestdyndenestseqamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupFinalizeCommitmentSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupOriginationSource { pub signature_public_key_hash: PublicKeyHash }
data!(Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage,u8,proto016ptmumbaioperationalphacontentstxrolluprejectionmessage,{
        0 => Batch  { pub batch: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => Deposit  { pub deposit: Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDeposit },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDeposit { pub sender: Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender, pub destination: Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination, pub ticket_hash: Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash, pub amount: Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount }
data!(Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount,u8,proto016ptmumbaioperationalphacontentstxrolluprejectionmessagedepositdepositamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultHash { pub message_result_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResult { pub context_hash: Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash, pub withdraw_list_hash: Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash { pub withdraw_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupRemoveCommitmentSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupReturnBondSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsTxRollupSubmitBatchSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeyPk { pub signature_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeySource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsVdfRevelationSolution(pub ::rust_runtime::ByteString<100>,pub ::rust_runtime::ByteString<100>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1);
data!(Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1,u8,proto016ptmumbaioperationalphacontentszkrolluporiginationcircuitsinfodenestdyndenestseqindex1,{
        0 => Public ,
        1 => Private ,
        2 => Fee ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq(pub Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0,pub Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0 { pub op_code: ::rust_runtime::i31, pub price: Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price, pub l1_dst: Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst, pub rollup_id: Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId, pub payload: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price { pub id: Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId, pub amount: ::rust_runtime::Z }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
data!(Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1,u8,proto016ptmumbaioperationalphacontentszkrolluppublishopdenestdyndenestseqindex1,{
        0 => None ,
        1 => Some  { pub contents: MichelineProto016PtMumbaiMichelsonV1Expression, pub ty: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>, pub ticketer: Proto016PtMumbaiContractId },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupPublishSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupPublishZkRollup { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdate { pub pending_pis: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq>>, pub private_pis: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq>>, pub fee_pi: Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdateFeePi, pub proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdateFeePi { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1 { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub fee: ::rust_runtime::ByteString<32>, pub exit_validity: bool }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1 { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub fee: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateZkRollup { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiSmartRollupAddress { pub smart_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
        3 => Bls  { pub bls12_381_public_key: ::rust_runtime::ByteString<48> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
        3 => Bls  { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto016PtMumbaiOperationContentsList = ::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContents>;
#[allow(dead_code)]
pub fn proto016ptmumbaioperationcontentslist_write<U: Target>(val: &Proto016PtMumbaiOperationContentsList, buf: &mut U) -> usize {
    ::rust_runtime::Sequence::<Proto016PtMumbaiOperationAlphaContents>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaioperationcontentslist_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiOperationContentsList> {
    Ok(::rust_runtime::Sequence::<Proto016PtMumbaiOperationAlphaContents>::parse(p)?)
}
