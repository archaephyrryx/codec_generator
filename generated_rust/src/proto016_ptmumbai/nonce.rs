#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto016PtMumbaiNonce = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn proto016ptmumbainonce_write<U: Target>(val: &Proto016PtMumbaiNonce, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbainonce_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiNonce> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
