#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,N,ParseResult,Parser,Target,i31,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiConstantsFixed { pub proof_of_work_nonce_size: u8, pub nonce_length: u8, pub max_anon_ops_per_block: u8, pub max_operation_data_length: ::rust_runtime::i31, pub max_proposals_per_delegate: u8, pub max_micheline_node_count: ::rust_runtime::i31, pub max_micheline_bytes_limit: ::rust_runtime::i31, pub max_allowed_global_constants_depth: ::rust_runtime::i31, pub cache_layout_size: u8, pub michelson_maximum_type_size: u16, pub smart_rollup_max_wrapped_proof_binary_size: ::rust_runtime::i31, pub smart_rollup_message_size_limit: ::rust_runtime::i31, pub smart_rollup_max_number_of_messages_per_level: ::rust_runtime::N }
#[allow(dead_code)]
pub fn proto016ptmumbaiconstantsfixed_write<U: Target>(val: &Proto016PtMumbaiConstantsFixed, buf: &mut U) -> usize {
    u8::write_to(&val.proof_of_work_nonce_size, buf) + u8::write_to(&val.nonce_length, buf) + u8::write_to(&val.max_anon_ops_per_block, buf) + ::rust_runtime::i31::write_to(&val.max_operation_data_length, buf) + u8::write_to(&val.max_proposals_per_delegate, buf) + ::rust_runtime::i31::write_to(&val.max_micheline_node_count, buf) + ::rust_runtime::i31::write_to(&val.max_micheline_bytes_limit, buf) + ::rust_runtime::i31::write_to(&val.max_allowed_global_constants_depth, buf) + u8::write_to(&val.cache_layout_size, buf) + u16::write_to(&val.michelson_maximum_type_size, buf) + ::rust_runtime::i31::write_to(&val.smart_rollup_max_wrapped_proof_binary_size, buf) + ::rust_runtime::i31::write_to(&val.smart_rollup_message_size_limit, buf) + ::rust_runtime::N::write_to(&val.smart_rollup_max_number_of_messages_per_level, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaiconstantsfixed_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiConstantsFixed> {
    Ok(Proto016PtMumbaiConstantsFixed {proof_of_work_nonce_size: u8::parse(p)?, nonce_length: u8::parse(p)?, max_anon_ops_per_block: u8::parse(p)?, max_operation_data_length: ::rust_runtime::i31::parse(p)?, max_proposals_per_delegate: u8::parse(p)?, max_micheline_node_count: ::rust_runtime::i31::parse(p)?, max_micheline_bytes_limit: ::rust_runtime::i31::parse(p)?, max_allowed_global_constants_depth: ::rust_runtime::i31::parse(p)?, cache_layout_size: u8::parse(p)?, michelson_maximum_type_size: u16::parse(p)?, smart_rollup_max_wrapped_proof_binary_size: ::rust_runtime::i31::parse(p)?, smart_rollup_message_size_limit: ::rust_runtime::i31::parse(p)?, smart_rollup_max_number_of_messages_per_level: ::rust_runtime::N::parse(p)?})
}
