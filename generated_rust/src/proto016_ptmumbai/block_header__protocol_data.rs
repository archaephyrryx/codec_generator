#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderProtocolData { pub payload_hash: Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature }
#[allow(dead_code)]
pub fn proto016ptmumbaiblockheaderprotocoldata_write<U: Target>(val: &Proto016PtMumbaiBlockHeaderProtocolData, buf: &mut U) -> usize {
    Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash::write_to(&val.payload_hash, buf) + i32::write_to(&val.payload_round, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + i8::write_to(&val.liquidity_baking_toggle_vote, buf) + Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaiblockheaderprotocoldata_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiBlockHeaderProtocolData> {
    Ok(Proto016PtMumbaiBlockHeaderProtocolData {payload_hash: Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash::parse(p)?, payload_round: i32::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, liquidity_baking_toggle_vote: i8::parse(p)?, signature: Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature::parse(p)?})
}
