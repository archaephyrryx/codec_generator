#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto016PtMumbaiFitnessLockedRound,u8,proto016ptmumbaifitnesslockedround,{
        0 => None ,
        1 => Some (pub i32),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiFitness { pub level: i32, pub locked_round: Proto016PtMumbaiFitnessLockedRound, pub predecessor_round: i32, pub round: i32 }
#[allow(dead_code)]
pub fn proto016ptmumbaifitness_write<U: Target>(val: &Proto016PtMumbaiFitness, buf: &mut U) -> usize {
    i32::write_to(&val.level, buf) + Proto016PtMumbaiFitnessLockedRound::write_to(&val.locked_round, buf) + i32::write_to(&val.predecessor_round, buf) + i32::write_to(&val.round, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaifitness_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiFitness> {
    Ok(Proto016PtMumbaiFitness {level: i32::parse(p)?, locked_round: Proto016PtMumbaiFitnessLockedRound::parse(p)?, predecessor_round: i32::parse(p)?, round: i32::parse(p)?})
}
