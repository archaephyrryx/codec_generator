#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto016PtMumbaiTimestamp = i64;
#[allow(dead_code)]
pub fn proto016ptmumbaitimestamp_write<U: Target>(val: &Proto016PtMumbaiTimestamp, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaitimestamp_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiTimestamp> {
    Ok(i64::parse(p)?)
}
