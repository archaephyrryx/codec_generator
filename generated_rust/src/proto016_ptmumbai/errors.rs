#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,ParseResult,Parser,Target,resolve_zero,u30};
pub type Proto016PtMumbaiErrors = ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>;
#[allow(dead_code)]
pub fn proto016ptmumbaierrors_write<U: Target>(val: &Proto016PtMumbaiErrors, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaierrors_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiErrors> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?)
}
