#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type Proto016PtMumbaiGasCost = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn proto016ptmumbaigascost_write<U: Target>(val: &Proto016PtMumbaiGasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaigascost_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiGasCost> {
    Ok(::rust_runtime::Z::parse(p)?)
}
