#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(Proto016PtMumbaiBondId,u8,proto016ptmumbaibondid,{
        0 => Tx_rollup_bond_id  { pub tx_rollup: Proto016PtMumbaiTxRollupId },
        1 => Smart_rollup_bond_id  { pub smart_rollup: Proto016PtMumbaiSmartRollupAddress },
    }
    );
data!(Proto016PtMumbaiContractId,u8,proto016ptmumbaicontractid,{
        0 => Implicit  { pub signature_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto016PtMumbaiContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto016PtMumbaiOperationMetadataAlphaBalance,u8,proto016ptmumbaioperationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto016PtMumbaiContractId, pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        2 => Block_fees  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        4 => Deposits  { pub delegate: Proto016PtMumbaiOperationMetadataAlphaBalanceDepositsDelegate, pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        5 => Nonce_revelation_rewards  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        6 => Double_signing_evidence_rewards  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        7 => Endorsing_rewards  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        8 => Baking_rewards  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        9 => Baking_bonuses  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        11 => Storage_fees  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        12 => Double_signing_punishments  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        13 => Lost_endorsing_rewards  { pub delegate: Proto016PtMumbaiOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate, pub participation: bool, pub revelation: bool, pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        14 => Liquidity_baking_subsidies  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        15 => Burned  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        16 => Commitments  { pub committer: Proto016PtMumbaiOperationMetadataAlphaBalanceCommitmentsCommitter, pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        17 => Bootstrap  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        18 => Invoice  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        19 => Initial_commitments  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        20 => Minted  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        21 => Frozen_bonds  { pub contract: Proto016PtMumbaiContractId, pub bond_id: Proto016PtMumbaiBondId, pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        22 => Tx_rollup_rejection_rewards  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        23 => Tx_rollup_rejection_punishments  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        24 => Smart_rollup_refutation_punishments  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
        25 => Smart_rollup_refutation_rewards  { pub change: i64, pub origin: Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationMetadataAlphaBalanceCommitmentsCommitter { pub blinded_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationMetadataAlphaBalanceDepositsDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate { pub signature_public_key_hash: PublicKeyHash }
data!(Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin,u8,proto016ptmumbaioperationmetadataalphaupdateoriginorigin,{
        0 => Block_application ,
        1 => Protocol_migration ,
        2 => Subsidy ,
        3 => Simulation ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiSmartRollupAddress { pub smart_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
        3 => Bls  { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto016PtMumbaiReceiptBalanceUpdates = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationMetadataAlphaBalance>>;
#[allow(dead_code)]
pub fn proto016ptmumbaireceiptbalanceupdates_write<U: Target>(val: &Proto016PtMumbaiReceiptBalanceUpdates, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto016PtMumbaiOperationMetadataAlphaBalance>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaireceiptbalanceupdates_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiReceiptBalanceUpdates> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto016PtMumbaiOperationMetadataAlphaBalance>>::parse(p)?)
}
