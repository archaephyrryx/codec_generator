#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,i31,resolve_zero};
pub type Proto016PtMumbaiScriptLoc = ::rust_runtime::i31;
#[allow(dead_code)]
pub fn proto016ptmumbaiscriptloc_write<U: Target>(val: &Proto016PtMumbaiScriptLoc, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaiscriptloc_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiScriptLoc> {
    Ok(::rust_runtime::i31::parse(p)?)
}
