#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto016PtMumbaiVoteBallot = i8;
#[allow(dead_code)]
pub fn proto016ptmumbaivoteballot_write<U: Target>(val: &Proto016PtMumbaiVoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaivoteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiVoteBallot> {
    Ok(i8::parse(p)?)
}
