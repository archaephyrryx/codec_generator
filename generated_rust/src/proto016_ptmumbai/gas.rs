#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto016PtMumbaiGas,u8,proto016ptmumbaigas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto016ptmumbaigas_write<U: Target>(val: &Proto016PtMumbaiGas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaigas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiGas> {
    Proto016PtMumbaiGas::parse(p)
}
