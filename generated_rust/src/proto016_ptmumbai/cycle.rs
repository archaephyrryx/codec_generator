#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto016PtMumbaiCycle = i32;
#[allow(dead_code)]
pub fn proto016ptmumbaicycle_write<U: Target>(val: &Proto016PtMumbaiCycle, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaicycle_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiCycle> {
    Ok(i32::parse(p)?)
}
