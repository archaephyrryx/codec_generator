#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto016PtMumbaiVotingPeriodKind,u8,proto016ptmumbaivotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiVotingPeriod { pub index: i32, pub kind: Proto016PtMumbaiVotingPeriodKind, pub start_position: i32 }
#[allow(dead_code)]
pub fn proto016ptmumbaivotingperiod_write<U: Target>(val: &Proto016PtMumbaiVotingPeriod, buf: &mut U) -> usize {
    i32::write_to(&val.index, buf) + Proto016PtMumbaiVotingPeriodKind::write_to(&val.kind, buf) + i32::write_to(&val.start_position, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaivotingperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiVotingPeriod> {
    Ok(Proto016PtMumbaiVotingPeriod {index: i32::parse(p)?, kind: Proto016PtMumbaiVotingPeriodKind::parse(p)?, start_position: i32::parse(p)?})
}
