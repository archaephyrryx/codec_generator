#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,Padded,ParseResult,Parser,Target,data,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
        3 => Bls  { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
data!(Proto016PtMumbaiContract,u8,proto016ptmumbaicontract,{
        0 => Implicit  { pub signature_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto016PtMumbaiContractIdOriginatedDenestPad,1>),
    }
    );
#[allow(dead_code)]
pub fn proto016ptmumbaicontract_write<U: Target>(val: &Proto016PtMumbaiContract, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaicontract_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiContract> {
    Proto016PtMumbaiContract::parse(p)
}
