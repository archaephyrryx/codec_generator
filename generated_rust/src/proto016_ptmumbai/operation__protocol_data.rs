#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{AutoBox,ByteString,Bytes,Decode,Dynamic,Encode,Estimable,LimSeq,N,Nullable,Padded,ParseResult,Parser,Sequence,Target,VPadded,Z,cstyle,data,i31,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
data!(BlsSignaturePrefix,u8,blssignatureprefix,{
        3 => Bls_prefix (pub ::rust_runtime::ByteString<32>),
    }
    );
data!(MichelineProto016PtMumbaiMichelsonV1Expression,u8,michelineproto016ptmumbaimichelsonv1expression,{
        0 => Int  { pub int: ::rust_runtime::Z },
        1 => String  { pub string: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        2 => Sequence (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>>>),
        3 => Prim__no_args__no_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives },
        4 => Prim__no_args__some_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        5 => Prim__1_arg__no_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression> },
        6 => Prim__1_arg__some_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        7 => Prim__2_args__no_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression> },
        8 => Prim__2_args__some_annots  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        9 => Prim__generic  { pub prim: Proto016PtMumbaiMichelsonV1Primitives, pub args: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>>>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        10 => Bytes  { pub bytes: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderAlphaFullHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub payload_hash: Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
data!(Proto016PtMumbaiContractId,u8,proto016ptmumbaicontractid,{
        0 => Implicit  { pub signature_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto016PtMumbaiContractIdOriginatedDenestPad,1>),
    }
    );
data!(Proto016PtMumbaiContractIdOriginated,u8,proto016ptmumbaicontractidoriginated,{
        1 => Originated (pub ::rust_runtime::Padded<Proto016PtMumbaiContractIdOriginatedOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiContractIdOriginatedOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto016PtMumbaiEntrypoint,u8,proto016ptmumbaientrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        5 => deposit ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedEndorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto016PtMumbaiInlinedEndorsementMempoolContents, pub signature: ::rust_runtime::Nullable<Proto016PtMumbaiInlinedEndorsementSignature> }
data!(Proto016PtMumbaiInlinedEndorsementMempoolContents,u8,proto016ptmumbaiinlinedendorsementmempoolcontents,{
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto016PtMumbaiInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedEndorsementSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedPreendorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto016PtMumbaiInlinedPreendorsementContents, pub signature: ::rust_runtime::Nullable<Proto016PtMumbaiInlinedPreendorsementSignature> }
data!(Proto016PtMumbaiInlinedPreendorsementContents,u8,proto016ptmumbaiinlinedpreendorsementcontents,{
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto016PtMumbaiInlinedPreendorsementContentsPreendorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedPreendorsementContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiInlinedPreendorsementSignature { pub signature_v1: ::rust_runtime::Bytes }
cstyle!(Proto016PtMumbaiMichelsonV1Primitives,u8,{
        parameter = 0,
        storage = 1,
        code = 2,
        False = 3,
        Elt = 4,
        Left = 5,
        None = 6,
        Pair = 7,
        Right = 8,
        Some = 9,
        True = 10,
        Unit = 11,
        PACK = 12,
        UNPACK = 13,
        BLAKE2B = 14,
        SHA256 = 15,
        SHA512 = 16,
        ABS = 17,
        ADD = 18,
        AMOUNT = 19,
        AND = 20,
        BALANCE = 21,
        CAR = 22,
        CDR = 23,
        CHECK_SIGNATURE = 24,
        COMPARE = 25,
        CONCAT = 26,
        CONS = 27,
        CREATE_ACCOUNT = 28,
        CREATE_CONTRACT = 29,
        IMPLICIT_ACCOUNT = 30,
        DIP = 31,
        DROP = 32,
        DUP = 33,
        EDIV = 34,
        EMPTY_MAP = 35,
        EMPTY_SET = 36,
        EQ = 37,
        EXEC = 38,
        FAILWITH = 39,
        GE = 40,
        GET = 41,
        GT = 42,
        HASH_KEY = 43,
        IF = 44,
        IF_CONS = 45,
        IF_LEFT = 46,
        IF_NONE = 47,
        INT = 48,
        LAMBDA = 49,
        LE = 50,
        LEFT = 51,
        LOOP = 52,
        LSL = 53,
        LSR = 54,
        LT = 55,
        MAP = 56,
        MEM = 57,
        MUL = 58,
        NEG = 59,
        NEQ = 60,
        NIL = 61,
        NONE = 62,
        NOT = 63,
        NOW = 64,
        OR = 65,
        PAIR = 66,
        PUSH = 67,
        RIGHT = 68,
        SIZE = 69,
        SOME = 70,
        SOURCE = 71,
        SENDER = 72,
        SELF = 73,
        STEPS_TO_QUOTA = 74,
        SUB = 75,
        SWAP = 76,
        TRANSFER_TOKENS = 77,
        SET_DELEGATE = 78,
        UNIT = 79,
        UPDATE = 80,
        XOR = 81,
        ITER = 82,
        LOOP_LEFT = 83,
        ADDRESS = 84,
        CONTRACT = 85,
        ISNAT = 86,
        CAST = 87,
        RENAME = 88,
        bool = 89,
        contract = 90,
        int = 91,
        key = 92,
        key_hash = 93,
        lambda = 94,
        list = 95,
        map = 96,
        big_map = 97,
        nat = 98,
        option = 99,
        or = 100,
        pair = 101,
        set = 102,
        signature = 103,
        string = 104,
        bytes = 105,
        mutez = 106,
        timestamp = 107,
        unit = 108,
        operation = 109,
        address = 110,
        SLICE = 111,
        DIG = 112,
        DUG = 113,
        EMPTY_BIG_MAP = 114,
        APPLY = 115,
        chain_id = 116,
        CHAIN_ID = 117,
        LEVEL = 118,
        SELF_ADDRESS = 119,
        never = 120,
        NEVER = 121,
        UNPAIR = 122,
        VOTING_POWER = 123,
        TOTAL_VOTING_POWER = 124,
        KECCAK = 125,
        SHA3 = 126,
        PAIRING_CHECK = 127,
        bls12_381_g1 = 128,
        bls12_381_g2 = 129,
        bls12_381_fr = 130,
        sapling_state = 131,
        sapling_transaction_deprecated = 132,
        SAPLING_EMPTY_STATE = 133,
        SAPLING_VERIFY_UPDATE = 134,
        ticket = 135,
        TICKET_DEPRECATED = 136,
        READ_TICKET = 137,
        SPLIT_TICKET = 138,
        JOIN_TICKETS = 139,
        GET_AND_UPDATE = 140,
        chest = 141,
        chest_key = 142,
        OPEN_CHEST = 143,
        VIEW = 144,
        view = 145,
        constant = 146,
        SUB_MUTEZ = 147,
        tx_rollup_l2_address = 148,
        MIN_BLOCK_TIME = 149,
        sapling_transaction = 150,
        EMIT = 151,
        Lambda_rec = 152,
        LAMBDA_REC = 153,
        TICKET = 154,
        BYTES = 155,
        NAT = 156
    }
    );
data!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefix,u8,proto016ptmumbaioperationalphacontentsorsignatureprefix,{
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32> },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiInlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiInlinedEndorsement> },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiBlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiBlockHeaderAlphaFullHeader> },
        4 => Activate_account  { pub pkh: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20> },
        5 => Proposals  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::LimSeq<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixProposalsProposalsDenestDynDenestSeq,20>> },
        6 => Ballot  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixBallotSource, pub period: i32, pub proposal: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixBallotProposal, pub ballot: i8 },
        7 => Double_preendorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiInlinedPreendorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto016PtMumbaiInlinedPreendorsement> },
        8 => Vdf_revelation  { pub solution: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixVdfRevelationSolution },
        9 => Drain_delegate  { pub consensus_key: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDrainDelegateConsensusKey, pub delegate: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDrainDelegateDelegate, pub destination: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDrainDelegateDestination },
        17 => Failing_noop  { pub arbitrary: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixPreendorsementBlockPayloadHash },
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixEndorsementBlockPayloadHash },
        22 => Dal_attestation  { pub attestor: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDalAttestationAttestor, pub attestation: ::rust_runtime::Z, pub level: i32 },
        107 => Reveal  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixRevealPublicKey },
        108 => Transaction  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: Proto016PtMumbaiContractId, pub parameters: std::option::Option<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTransactionParameters> },
        109 => Origination  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixOriginationDelegate>, pub script: Proto016PtMumbaiScriptedContracts },
        110 => Delegation  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDelegationDelegate> },
        111 => Register_global_constant  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixRegisterGlobalConstantSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        112 => Set_deposits_limit  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSetDepositsLimitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub limit: std::option::Option<::rust_runtime::N> },
        113 => Increase_paid_storage  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixIncreasePaidStorageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::Z, pub destination: Proto016PtMumbaiContractIdOriginated },
        114 => Update_consensus_key  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeySource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub pk: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeyPk },
        150 => Tx_rollup_origination  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N },
        151 => Tx_rollup_submit_batch  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupSubmitBatchSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId, pub content: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub burn_limit: std::option::Option<::rust_runtime::N> },
        152 => Tx_rollup_commit  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupCommitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId, pub commitment: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitment },
        153 => Tx_rollup_return_bond  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupReturnBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId },
        154 => Tx_rollup_finalize_commitment  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupFinalizeCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId },
        155 => Tx_rollup_remove_commitment  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRemoveCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId },
        156 => Tx_rollup_rejection  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiTxRollupId, pub level: i32, pub message: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage, pub message_position: ::rust_runtime::N, pub message_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessagePathDenestDynDenestSeq>>, pub message_result_hash: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultHash, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultPathDenestDynDenestSeq>>, pub previous_message_result: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResult, pub previous_message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>>, pub proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        157 => Tx_rollup_dispatch_tickets  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub tx_rollup: Proto016PtMumbaiTxRollupId, pub level: i32, pub context_hash: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsContextHash, pub message_index: ::rust_runtime::i31, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>>, pub tickets_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>> },
        158 => Transfer_ticket  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTransferTicketSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub ticket_contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ticketer: Proto016PtMumbaiContractId, pub ticket_amount: ::rust_runtime::N, pub destination: Proto016PtMumbaiContractId, pub entrypoint: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        200 => Smart_rollup_originate  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupOriginateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub pvm_kind: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupOriginatePvmKind, pub kernel: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub origination_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub parameters_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        201 => Smart_rollup_add_messages  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupAddMessagesSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub message: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        202 => Smart_rollup_cement  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupCementSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiSmartRollupAddress, pub commitment: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupCementCommitment },
        203 => Smart_rollup_publish  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiSmartRollupAddress, pub commitment: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitment },
        204 => Smart_rollup_refute  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiSmartRollupAddress, pub opponent: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteOpponent, pub refutation: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation },
        205 => Smart_rollup_timeout  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiSmartRollupAddress, pub stakers: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakers },
        206 => Smart_rollup_execute_outbox_message  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiSmartRollupAddress, pub cemented_commitment: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageCementedCommitment, pub output_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        207 => Smart_rollup_recover_bond  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondRollup, pub staker: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondStaker },
        230 => Dal_publish_slot_header  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub slot_header: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeader },
        250 => Zk_rollup_origination  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_parameters: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub circuits_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeq>>, pub init_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub nb_ops: ::rust_runtime::i31 },
        251 => Zk_rollup_publish  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub zk_rollup: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishZkRollup, pub op: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeq>> },
        252 => Zk_rollup_update  { pub source: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub zk_rollup: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateZkRollup, pub update: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdate },
        255 => Signature_prefix  { pub signature_prefix: BlsSignaturePrefix },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixBallotSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDalAttestationAttestor { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeader { pub level: i32, pub index: u8, pub commitment: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeaderCommitment, pub commitment_proof: ::rust_runtime::ByteString<48> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeaderCommitment { pub dal_commitment: ::rust_runtime::ByteString<48> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDelegationDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDelegationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDrainDelegateConsensusKey { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDrainDelegateDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixDrainDelegateDestination { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixIncreasePaidStorageSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixOriginationDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixOriginationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixProposalsSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixRegisterGlobalConstantSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixRevealPublicKey { pub signature_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixRevealSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSetDepositsLimitSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupAddMessagesSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupCementCommitment { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupCementSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageCementedCommitment { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageSource { pub signature_public_key_hash: PublicKeyHash }
cstyle!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupOriginatePvmKind,u8,{
        arith = 0,
        wasm_2_0_0 = 1
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupOriginateSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitment { pub compressed_state: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentCompressedState, pub inbox_level: i32, pub predecessor: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentPredecessor, pub number_of_ticks: i64 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentCompressedState { pub smart_rollup_state_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentPredecessor { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupPublishSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondRollup { pub smart_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondStaker { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteOpponent { pub signature_public_key_hash: PublicKeyHash }
data!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation,u8,proto016ptmumbaioperationalphacontentsorsignatureprefixsmartrolluprefuterefutation,{
        0 => Start  { pub player_commitment_hash: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartPlayerCommitmentHash, pub opponent_commitment_hash: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartOpponentCommitmentHash },
        1 => Move  { pub choice: ::rust_runtime::N, pub step: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep },
    }
    );
data!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep,u8,proto016ptmumbaioperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestep,{
        0 => Dissection (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq>>),
        1 => Proof  { pub pvm_step: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub input_proof: std::option::Option<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq { pub state: std::option::Option<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState>, pub tick: ::rust_runtime::N }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState { pub smart_rollup_state_hash: ::rust_runtime::ByteString<32> }
data!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof,u8,proto016ptmumbaioperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestepproofinputproof,{
        0 => inbox_proof  { pub level: i32, pub message_counter: ::rust_runtime::N, pub serialized_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => reveal_proof  { pub reveal_proof: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof },
        2 => first_input ,
    }
    );
data!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof,u8,proto016ptmumbaioperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestepproofinputproofrevealproofrevealproof,{
        0 => raw_data_proof  { pub raw_data: ::rust_runtime::Dynamic<u16,std::string::String> },
        1 => metadata_proof ,
        2 => dal_page_proof  { pub dal_page_id: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId, pub dal_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId { pub published_level: i32, pub slot_index: u8, pub page_index: i16 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartOpponentCommitmentHash { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartPlayerCommitmentHash { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakers { pub alice: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersAlice, pub bob: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersBob }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersAlice { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersBob { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTransactionParameters { pub entrypoint: Proto016PtMumbaiEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTransactionSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTransferTicketSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitment { pub level: i32, pub messages: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentMessagesDenestDynDenestSeq>>, pub predecessor: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor, pub inbox_merkle_root: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentInboxMerkleRoot }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentInboxMerkleRoot { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentMessagesDenestDynDenestSeq { pub message_result_hash: ::rust_runtime::ByteString<32> }
data!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor,u8,proto016ptmumbaioperationalphacontentsorsignatureprefixtxrollupcommitcommitmentpredecessor,{
        0 => None ,
        1 => Some  { pub commitment_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupCommitSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq { pub contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticketer: Proto016PtMumbaiContractId, pub amount: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount, pub claimer: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer }
data!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,u8,proto016ptmumbaioperationalphacontentsorsignatureprefixtxrollupdispatchticketsticketsinfodenestdyndenestseqamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupFinalizeCommitmentSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupOriginationSource { pub signature_public_key_hash: PublicKeyHash }
data!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage,u8,proto016ptmumbaioperationalphacontentsorsignatureprefixtxrolluprejectionmessage,{
        0 => Batch  { pub batch: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => Deposit  { pub deposit: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDeposit },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDeposit { pub sender: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositSender, pub destination: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositDestination, pub ticket_hash: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositTicketHash, pub amount: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount }
data!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount,u8,proto016ptmumbaioperationalphacontentsorsignatureprefixtxrolluprejectionmessagedepositdepositamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositDestination { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositSender { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessagePathDenestDynDenestSeq { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultHash { pub message_result_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResult { pub context_hash: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultContextHash, pub withdraw_list_hash: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultWithdrawListHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultWithdrawListHash { pub withdraw_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRejectionSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupRemoveCommitmentSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupReturnBondSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixTxRollupSubmitBatchSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeyPk { pub signature_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeySource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixVdfRevelationSolution(pub ::rust_runtime::ByteString<100>,pub ::rust_runtime::ByteString<100>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1);
data!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1,u8,proto016ptmumbaioperationalphacontentsorsignatureprefixzkrolluporiginationcircuitsinfodenestdyndenestseqindex1,{
        0 => Public ,
        1 => Private ,
        2 => Fee ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupOriginationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeq(pub Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0,pub Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0 { pub op_code: ::rust_runtime::i31, pub price: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price, pub l1_dst: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst, pub rollup_id: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId, pub payload: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price { pub id: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId, pub amount: ::rust_runtime::Z }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
data!(Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1,u8,proto016ptmumbaioperationalphacontentsorsignatureprefixzkrolluppublishopdenestdyndenestseqindex1,{
        0 => None ,
        1 => Some  { pub contents: MichelineProto016PtMumbaiMichelsonV1Expression, pub ty: ::rust_runtime::AutoBox<MichelineProto016PtMumbaiMichelsonV1Expression>, pub ticketer: Proto016PtMumbaiContractId },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupPublishZkRollup { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdate { pub pending_pis: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeq>>, pub private_pis: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq>>, pub fee_pi: Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdateFeePi, pub proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdateFeePi { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1 { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub fee: ::rust_runtime::ByteString<32>, pub exit_validity: bool }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1 { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub fee: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefixZkRollupUpdateZkRollup { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiSmartRollupAddress { pub smart_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
        3 => Bls  { pub bls12_381_public_key: ::rust_runtime::ByteString<48> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
        3 => Bls  { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto016PtMumbaiOperationProtocolData { pub contents_and_signature_prefix: ::rust_runtime::VPadded<::rust_runtime::Sequence<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefix>,64>, pub signature_suffix: ::rust_runtime::ByteString<64> }
#[allow(dead_code)]
pub fn proto016ptmumbaioperationprotocoldata_write<U: Target>(val: &Proto016PtMumbaiOperationProtocolData, buf: &mut U) -> usize {
    ::rust_runtime::VPadded::<::rust_runtime::Sequence::<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefix>,64>::write_to(&val.contents_and_signature_prefix, buf) + ::rust_runtime::ByteString::<64>::write_to(&val.signature_suffix, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaioperationprotocoldata_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiOperationProtocolData> {
    Ok(Proto016PtMumbaiOperationProtocolData {contents_and_signature_prefix: ::rust_runtime::VPadded::<::rust_runtime::Sequence::<Proto016PtMumbaiOperationAlphaContentsOrSignaturePrefix>,64>::parse(p)?, signature_suffix: ::rust_runtime::ByteString::<64>::parse(p)?})
}
