#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto016PtMumbaiPeriod = i64;
#[allow(dead_code)]
pub fn proto016ptmumbaiperiod_write<U: Target>(val: &Proto016PtMumbaiPeriod, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaiperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiPeriod> {
    Ok(i64::parse(p)?)
}
