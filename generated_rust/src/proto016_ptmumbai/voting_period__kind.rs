#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto016PtMumbaiVotingPeriodKind,u8,proto016ptmumbaivotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[allow(dead_code)]
pub fn proto016ptmumbaivotingperiodkind_write<U: Target>(val: &Proto016PtMumbaiVotingPeriodKind, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbaivotingperiodkind_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiVotingPeriodKind> {
    Proto016PtMumbaiVotingPeriodKind::parse(p)
}
