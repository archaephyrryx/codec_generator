#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto016PtMumbaiRawLevel = i32;
#[allow(dead_code)]
pub fn proto016ptmumbairawlevel_write<U: Target>(val: &Proto016PtMumbaiRawLevel, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto016ptmumbairawlevel_parse<P: Parser>(p: &mut P) -> ParseResult<Proto016PtMumbaiRawLevel> {
    Ok(i32::parse(p)?)
}
