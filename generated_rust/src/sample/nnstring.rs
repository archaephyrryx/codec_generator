#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Contents { contents: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct NNestedString { contents: Contents }
#[allow(dead_code)]
pub fn nnestedstring_write<U: Target>(val: &NNestedString, buf: &mut U) -> usize {
    Contents::write_to(&val.contents, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn nnestedstring_parse<P: Parser>(p: &mut P) -> ParseResult<NNestedString> {
    Ok(NNestedString {contents: Contents::parse(p)?})
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, NNestedString::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000003666f6f");
        roundtrip("00000003626172");
    }

}
