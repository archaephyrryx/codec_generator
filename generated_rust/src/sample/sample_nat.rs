#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{AutoBox,Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Nat,u8,nat,{
        0 => Z ,
        1 => S (::rust_runtime::AutoBox<Nat>),
    }
    );
#[allow(dead_code)]
pub fn nat_write<U: Target>(val: &Nat, buf: &mut U) -> usize {
    Nat::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn nat_parse<P: Parser>(p: &mut P) -> ParseResult<Nat> {
    Ok(Nat::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Nat::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00");
        roundtrip("0100");
        roundtrip("0101010100");
    }

}
