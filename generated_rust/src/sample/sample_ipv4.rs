#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct IPv4(u8,u8,u8,u8);
#[allow(dead_code)]
pub fn ipv4_write<U: Target>(val: &IPv4, buf: &mut U) -> usize {
    u8::write_to(&val.0, buf) + u8::write_to(&val.1, buf) + u8::write_to(&val.2, buf) + u8::write_to(&val.3, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn ipv4_parse<P: Parser>(p: &mut P) -> ParseResult<IPv4> {
    Ok(IPv4(u8::parse(p)?, u8::parse(p)?, u8::parse(p)?, u8::parse(p)?))
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, IPv4::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("7f000001");
        roundtrip("c0a80101");
        roundtrip("08080808");
    }

}
