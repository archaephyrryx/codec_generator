#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,Padded,ParseResult,Parser,Target,data,resolve_zero};
data!(TTOL,u8,ttol,{
        1 => OneLie (::rust_runtime::Padded<bool,1>),
        2 => TwoTruths (bool,bool),
    }
    );
#[allow(dead_code)]
pub fn ttol_write<U: Target>(val: &TTOL, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn ttol_parse<P: Parser>(p: &mut P) -> ParseResult<TTOL> {
    TTOL::parse(p)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, TTOL::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("02ffff");
        roundtrip("010000");
        roundtrip("01ff00");
        roundtrip("02ff00");
    }

}
