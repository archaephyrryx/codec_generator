#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,i31,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Quad4(::rust_runtime::i31,::rust_runtime::i31,::rust_runtime::i31,::rust_runtime::i31);
#[allow(dead_code)]
pub fn quad4_write<U: Target>(val: &Quad4, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(&val.0, buf) + ::rust_runtime::i31::write_to(&val.1, buf) + ::rust_runtime::i31::write_to(&val.2, buf) + ::rust_runtime::i31::write_to(&val.3, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn quad4_parse<P: Parser>(p: &mut P) -> ParseResult<Quad4> {
    Ok(Quad4(::rust_runtime::i31::parse(p)?, ::rust_runtime::i31::parse(p)?, ::rust_runtime::i31::parse(p)?, ::rust_runtime::i31::parse(p)?))
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Quad4::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000001000000020000000300000004");
        roundtrip("00000001000000030000000500000007");
        roundtrip("00000002000000030000000500000007");
        roundtrip("00000001000000020000000300000005");
    }

}
