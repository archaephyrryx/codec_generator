#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Unit = ();
#[allow(dead_code)]
pub fn unit_write<U: Target>(val: &Unit, buf: &mut U) -> usize {
    <()>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn unit_parse<P: Parser>(p: &mut P) -> ParseResult<Unit> {
    Ok(<()>::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Unit::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("");
    }

}
