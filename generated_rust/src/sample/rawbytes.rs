#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Bytes,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type RawBytes = ::rust_runtime::Bytes;
#[allow(dead_code)]
pub fn rawbytes_write<U: Target>(val: &RawBytes, buf: &mut U) -> usize {
    ::rust_runtime::Bytes::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn rawbytes_parse<P: Parser>(p: &mut P) -> ParseResult<RawBytes> {
    Ok(::rust_runtime::Bytes::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, RawBytes::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("666f6f626172");
        roundtrip("68656c6c6f20776f726c6421");
    }

}
