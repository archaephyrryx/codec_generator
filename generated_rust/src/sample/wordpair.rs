#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct WordPair(i8,i8);
#[allow(dead_code)]
pub fn wordpair_write<U: Target>(val: &WordPair, buf: &mut U) -> usize {
    i8::write_to(&val.0, buf) + i8::write_to(&val.1, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn wordpair_parse<P: Parser>(p: &mut P) -> ParseResult<WordPair> {
    Ok(WordPair(i8::parse(p)?, i8::parse(p)?))
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, WordPair::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("0102");
        roundtrip("0304");
    }

}
