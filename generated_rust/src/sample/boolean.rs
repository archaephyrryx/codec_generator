#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Boolean = bool;
#[allow(dead_code)]
pub fn boolean_write<U: Target>(val: &Boolean, buf: &mut U) -> usize {
    bool::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn boolean_parse<P: Parser>(p: &mut P) -> ParseResult<Boolean> {
    Ok(bool::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Boolean::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("ff");
        roundtrip("00");
    }

}
