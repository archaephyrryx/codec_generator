#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Digraph = ::rust_runtime::ByteString<2>;
#[allow(dead_code)]
pub fn digraph_write<U: Target>(val: &Digraph, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<2>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn digraph_parse<P: Parser>(p: &mut P) -> ParseResult<Digraph> {
    Ok(::rust_runtime::ByteString::<2>::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Digraph::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("c3a7");
        roundtrip("3432");
        roundtrip("0000");
    }

}
