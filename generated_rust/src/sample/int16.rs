#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Int16 = i16;
#[allow(dead_code)]
pub fn int16_write<U: Target>(val: &Int16, buf: &mut U) -> usize {
    i16::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn int16_parse<P: Parser>(p: &mut P) -> ParseResult<Int16> {
    Ok(i16::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Int16::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("0001");
        roundtrip("0002");
        roundtrip("0003");
        roundtrip("7fff");
        roundtrip("8000");
        roundtrip("ffff");
    }

}
