#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Int64 = i64;
#[allow(dead_code)]
pub fn int64_write<U: Target>(val: &Int64, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn int64_parse<P: Parser>(p: &mut P) -> ParseResult<Int64> {
    Ok(i64::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Int64::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("0000000000000001");
        roundtrip("7fffffffffffffff");
        roundtrip("8000000000000000");
        roundtrip("ffffffffffffffff");
    }

}
