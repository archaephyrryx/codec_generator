#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(SurveyAnswer,u8,surveyanswer,{
        0 => Answered (bool),
        1 => NoAnswer ,
    }
    );
#[allow(dead_code)]
pub fn surveyanswer_write<U: Target>(val: &SurveyAnswer, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn surveyanswer_parse<P: Parser>(p: &mut P) -> ParseResult<SurveyAnswer> {
    SurveyAnswer::parse(p)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, SurveyAnswer::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00ff");
        roundtrip("0000");
        roundtrip("01");
    }

}
