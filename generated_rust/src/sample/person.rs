#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Name { first: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, last: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Person { name: Name, fictional: bool }
#[allow(dead_code)]
pub fn person_write<U: Target>(val: &Person, buf: &mut U) -> usize {
    Name::write_to(&val.name, buf) + bool::write_to(&val.fictional, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn person_parse<P: Parser>(p: &mut P) -> ParseResult<Person> {
    Ok(Person {name: Name::parse(p)?, fictional: bool::parse(p)?})
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Person::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000008536865726c6f636b00000006486f6c6d6573ff");
        roundtrip("00000004416c616e00000006547572696e6700");
    }

}
