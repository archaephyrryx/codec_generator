#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero,u30};
pub type Uint30 = ::rust_runtime::u30;
#[allow(dead_code)]
pub fn uint30_write<U: Target>(val: &Uint30, buf: &mut U) -> usize {
    ::rust_runtime::u30::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn uint30_parse<P: Parser>(p: &mut P) -> ParseResult<Uint30> {
    Ok(::rust_runtime::u30::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Uint30::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000001");
        roundtrip("00000002");
        roundtrip("00000003");
        roundtrip("3fffffff");
    }

}
