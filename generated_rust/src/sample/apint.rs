#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type APInt = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn apint_write<U: Target>(val: &APInt, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn apint_parse<P: Parser>(p: &mut P) -> ParseResult<APInt> {
    Ok(::rust_runtime::Z::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, APInt::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00");
        roundtrip("01");
        roundtrip("89b811");
        roundtrip("60");
    }

}
