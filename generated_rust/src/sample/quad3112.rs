#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,i31,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Index2(::rust_runtime::i31,::rust_runtime::i31);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Quad3112(::rust_runtime::i31,::rust_runtime::i31,Index2);
#[allow(dead_code)]
pub fn quad3112_write<U: Target>(val: &Quad3112, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(&val.0, buf) + ::rust_runtime::i31::write_to(&val.1, buf) + Index2::write_to(&val.2, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn quad3112_parse<P: Parser>(p: &mut P) -> ParseResult<Quad3112> {
    Ok(Quad3112(::rust_runtime::i31::parse(p)?, ::rust_runtime::i31::parse(p)?, Index2::parse(p)?))
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Quad3112::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000001000000020000000300000004");
        roundtrip("00000001000000030000000500000007");
        roundtrip("00000002000000030000000500000007");
        roundtrip("00000001000000020000000300000005");
    }

}
