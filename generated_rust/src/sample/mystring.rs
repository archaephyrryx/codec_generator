#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,ParseResult,Parser,Target,resolve_zero,u30};
pub type MyString = ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>;
#[allow(dead_code)]
pub fn mystring_write<U: Target>(val: &MyString, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn mystring_parse<P: Parser>(p: &mut P) -> ParseResult<MyString> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, MyString::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000006666f6f626172");
        roundtrip("0000000c68656c6c6f20776f726c6421");
    }

}
