#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Uint8 = u8;
#[allow(dead_code)]
pub fn uint8_write<U: Target>(val: &Uint8, buf: &mut U) -> usize {
    u8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn uint8_parse<P: Parser>(p: &mut P) -> ParseResult<Uint8> {
    Ok(u8::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Uint8::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00");
        roundtrip("01");
        roundtrip("02");
        roundtrip("03");
        roundtrip("ff");
    }

}
