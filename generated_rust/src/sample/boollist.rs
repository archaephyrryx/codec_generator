#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
pub type BoolList = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<bool>>;
#[allow(dead_code)]
pub fn boollist_write<U: Target>(val: &BoolList, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<bool>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn boollist_parse<P: Parser>(p: &mut P) -> ParseResult<BoolList> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<bool>>::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, BoolList::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000000");
        roundtrip("00000002ff00");
    }

}
