#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct FirstLast { first: i8, last: i8 }
#[allow(dead_code)]
pub fn firstlast_write<U: Target>(val: &FirstLast, buf: &mut U) -> usize {
    i8::write_to(&val.first, buf) + i8::write_to(&val.last, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn firstlast_parse<P: Parser>(p: &mut P) -> ParseResult<FirstLast> {
    Ok(FirstLast {first: i8::parse(p)?, last: i8::parse(p)?})
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, FirstLast::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("0102");
        roundtrip("0304");
    }

}
