#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{CharString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Monogram = ::rust_runtime::CharString<1>;
#[allow(dead_code)]
pub fn monogram_write<U: Target>(val: &Monogram, buf: &mut U) -> usize {
    ::rust_runtime::CharString::<1>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn monogram_parse<P: Parser>(p: &mut P) -> ParseResult<Monogram> {
    Ok(::rust_runtime::CharString::<1>::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Monogram::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("61");
        roundtrip("0a");
    }

}
