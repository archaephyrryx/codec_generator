#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,RangedFloat,Target,resolve_zero};
pub type Iota = ::rust_runtime::RangedFloat<0xbff0000000000000,0x3ff0000000000000>;
#[allow(dead_code)]
pub fn iota_write<U: Target>(val: &Iota, buf: &mut U) -> usize {
    ::rust_runtime::RangedFloat::<0xbff0000000000000,0x3ff0000000000000>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn iota_parse<P: Parser>(p: &mut P) -> ParseResult<Iota> {
    Ok(::rust_runtime::RangedFloat::<0xbff0000000000000,0x3ff0000000000000>::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Iota::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("3fefffeb074a771d");
        roundtrip("bfefffeb074a771d");
        roundtrip("8000000000000000");
        roundtrip("0000000000000000");
    }

}
