#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,N,ParseResult,Parser,Target,resolve_zero};
pub type APNat = ::rust_runtime::N;
#[allow(dead_code)]
pub fn apnat_write<U: Target>(val: &APNat, buf: &mut U) -> usize {
    ::rust_runtime::N::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn apnat_parse<P: Parser>(p: &mut P) -> ParseResult<APNat> {
    Ok(::rust_runtime::N::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, APNat::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00");
        roundtrip("01");
        roundtrip("89dc08");
    }

}
