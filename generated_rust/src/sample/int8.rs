#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Int8 = i8;
#[allow(dead_code)]
pub fn int8_write<U: Target>(val: &Int8, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn int8_parse<P: Parser>(p: &mut P) -> ParseResult<Int8> {
    Ok(i8::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Int8::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00");
        roundtrip("01");
        roundtrip("02");
        roundtrip("03");
        roundtrip("7f");
        roundtrip("80");
        roundtrip("ff");
    }

}
