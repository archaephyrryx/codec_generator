#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Dimonic { contents: Monic }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Monic { contents: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Trimonic { contents: Dimonic }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct TetraMonic { contents: Trimonic }
#[allow(dead_code)]
pub fn tetramonic_write<U: Target>(val: &TetraMonic, buf: &mut U) -> usize {
    Trimonic::write_to(&val.contents, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn tetramonic_parse<P: Parser>(p: &mut P) -> ParseResult<TetraMonic> {
    Ok(TetraMonic {contents: Trimonic::parse(p)?})
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, TetraMonic::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000003666f6f");
        roundtrip("00000003626172");
    }

}
