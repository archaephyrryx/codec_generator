#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,i31,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Index1(::rust_runtime::i31,Index1Index1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Index1Index1(::rust_runtime::i31,::rust_runtime::i31);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct QuadLeft(::rust_runtime::i31,Index1);
#[allow(dead_code)]
pub fn quadleft_write<U: Target>(val: &QuadLeft, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(&val.0, buf) + Index1::write_to(&val.1, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn quadleft_parse<P: Parser>(p: &mut P) -> ParseResult<QuadLeft> {
    Ok(QuadLeft(::rust_runtime::i31::parse(p)?, Index1::parse(p)?))
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, QuadLeft::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000001000000020000000300000004");
        roundtrip("00000001000000030000000500000007");
        roundtrip("00000002000000030000000500000007");
        roundtrip("00000001000000020000000300000005");
    }

}
