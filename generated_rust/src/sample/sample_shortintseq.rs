#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,LimSeq,ParseResult,Parser,Target,i31,resolve_zero};
pub type ShortIntSeq = ::rust_runtime::LimSeq<::rust_runtime::i31,9>;
#[allow(dead_code)]
pub fn shortintseq_write<U: Target>(val: &ShortIntSeq, buf: &mut U) -> usize {
    ::rust_runtime::LimSeq::<::rust_runtime::i31,9>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn shortintseq_parse<P: Parser>(p: &mut P) -> ParseResult<ShortIntSeq> {
    Ok(::rust_runtime::LimSeq::<::rust_runtime::i31,9>::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, ShortIntSeq::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("");
        roundtrip("00000000");
        roundtrip("0000001100000013");
        roundtrip("0000000100000001000000020000000300000005");
    }

}
