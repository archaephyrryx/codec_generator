#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,i31,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Index0(::rust_runtime::i31,::rust_runtime::i31);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Index1(::rust_runtime::i31,::rust_runtime::i31);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Quad222(Index0,Index1);
#[allow(dead_code)]
pub fn quad222_write<U: Target>(val: &Quad222, buf: &mut U) -> usize {
    Index0::write_to(&val.0, buf) + Index1::write_to(&val.1, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn quad222_parse<P: Parser>(p: &mut P) -> ParseResult<Quad222> {
    Ok(Quad222(Index0::parse(p)?, Index1::parse(p)?))
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Quad222::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000001000000020000000300000004");
        roundtrip("00000001000000030000000500000007");
        roundtrip("00000002000000030000000500000007");
        roundtrip("00000001000000020000000300000005");
    }

}
