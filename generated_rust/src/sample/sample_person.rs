#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Person { first: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, middle: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>, last: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> }
#[allow(dead_code)]
pub fn person_write<U: Target>(val: &Person, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(&val.first, buf) + std::option::Option::<::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>>::write_to(&val.middle, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(&val.last, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn person_parse<P: Parser>(p: &mut P) -> ParseResult<Person> {
    Ok(Person {first: ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?, middle: std::option::Option::<::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>>::parse(p)?, last: ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?})
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Person::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("000000074861736b656c6cff0000000642726f6f6b73000000054375727279");
        roundtrip("00000006416c6f6e7a6f0000000006436875726368");
    }

}
