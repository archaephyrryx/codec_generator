#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Uint16 = u16;
#[allow(dead_code)]
pub fn uint16_write<U: Target>(val: &Uint16, buf: &mut U) -> usize {
    u16::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn uint16_parse<P: Parser>(p: &mut P) -> ParseResult<Uint16> {
    Ok(u16::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Uint16::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("0001");
        roundtrip("0002");
        roundtrip("0003");
        roundtrip("7fff");
        roundtrip("8000");
        roundtrip("ffff");
    }

}
