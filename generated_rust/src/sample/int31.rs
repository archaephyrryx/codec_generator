#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,i31,resolve_zero};
pub type Int31 = ::rust_runtime::i31;
#[allow(dead_code)]
pub fn int31_write<U: Target>(val: &Int31, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn int31_parse<P: Parser>(p: &mut P) -> ParseResult<Int31> {
    Ok(::rust_runtime::i31::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Int31::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000001");
        roundtrip("00000002");
        roundtrip("00000003");
        roundtrip("3fffffff");
        roundtrip("c0000000");
        roundtrip("ffffffff");
    }

}
