#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Int32 = i32;
#[allow(dead_code)]
pub fn int32_write<U: Target>(val: &Int32, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn int32_parse<P: Parser>(p: &mut P) -> ParseResult<Int32> {
    Ok(i32::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Int32::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000001");
        roundtrip("7fffffff");
        roundtrip("80000000");
        roundtrip("ffffffff");
    }

}
