#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct NestedString { contents: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> }
#[allow(dead_code)]
pub fn nestedstring_write<U: Target>(val: &NestedString, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(&val.contents, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn nestedstring_parse<P: Parser>(p: &mut P) -> ParseResult<NestedString> {
    Ok(NestedString {contents: ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?})
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, NestedString::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("00000003666f6f");
        roundtrip("00000003626172");
    }

}
