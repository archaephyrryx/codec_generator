#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(FilePath,u8,filepath,{
        0 => Absolute (::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>),
        1 => Relative (::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>>),
    }
    );
#[allow(dead_code)]
pub fn filepath_write<U: Target>(val: &FilePath, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn filepath_parse<P: Parser>(p: &mut P) -> ParseResult<FilePath> {
    FilePath::parse(p)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, FilePath::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("0000000016000000037573720000000362696e0000000462617368");
        roundtrip("010000001f0000000373726300000006636f6d6d6f6e0000000a7072696e7465722e6d6c");
    }

}
