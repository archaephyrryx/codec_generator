#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Double = f64;
#[allow(dead_code)]
pub fn double_write<U: Target>(val: &Double, buf: &mut U) -> usize {
    f64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn double_parse<P: Parser>(p: &mut P) -> ParseResult<Double> {
    Ok(f64::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, Double::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("3ff0000000000000");
        roundtrip("bff0000000000000");
        roundtrip("400921fb54442d18");
    }

}
