#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type RawString = std::string::String;
#[allow(dead_code)]
pub fn rawstring_write<U: Target>(val: &RawString, buf: &mut U) -> usize {
    std::string::String::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn rawstring_parse<P: Parser>(p: &mut P) -> ParseResult<RawString> {
    Ok(std::string::String::parse(p)?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rust_runtime::{hex,HexString};
    fn roundtrip(hex: &str) {
        assert_eq!(hex, RawString::decode(hex!(hex)).encode::<HexString>());
    }

    #[test]
    fn images_roundtrip() {
        roundtrip("666f6f626172");
        roundtrip("68656c6c6f20776f726c6421");
    }

}
