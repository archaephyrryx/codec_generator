#![allow(non_camel_case_types, non_snake_case)]
extern crate dissimilar;
extern crate rust_runtime;
extern crate std;

use rust_runtime::builder::strict;
use rust_runtime::{ hex, Builder, Decode, Encode, HexString };
use std::{ borrow::Cow, fmt::Display };

macro_rules! pub_if {
    ($($modname:ident, $feature_name:literal);+) => {
        $(
            #[cfg(feature = $feature_name)]
            pub mod $modname;
            #[cfg(not(feature = $feature_name))]
            mod $modname;
        )+
    };
}

pub_if! {
    alpha, "alpha";
    etc, "etc";
    ground, "ground";
    proto005_psbabym1, "babylon";
    proto006_pscartha, "carthage";
    proto007_psdelph1, "delphi";
    proto008_ptedo2zk, "edo";
    proto009_psfloren, "florence";
    proto010_ptgranad, "granada";
    proto011_pthangz2, "hangzhou";
    proto012_psithaca, "ithaca";
    proto013_ptjakart, "jakarta";
    proto014_ptkathma, "kathmandu";
    proto015_ptlimapt, "lima";
    proto016_ptmumbai, "mumbai";
    sample, "sample";
    sapling, "sapling"
}

pub struct CodecRun<'a, T: rust_runtime::Target = strict::StrictBuilder> {
    ident: &'a str,
    preimage: &'a str,
    image: String,
    postimage: T,
}

const TRUNC_LIMIT: usize = 24;
const OUTER_SLICE_LEN: usize = 4;

fn abbrev<'a>(s: &'a str) -> Cow<'a, str> {
    let l = s.len();
    if l >= TRUNC_LIMIT {
        Cow::Owned(
            format!(
                "{}[...{} chars...]{}",
                &s[0..OUTER_SLICE_LEN],
                l - 2 * OUTER_SLICE_LEN,
                &s[l - OUTER_SLICE_LEN..l]
            )
        )
    } else {
        Cow::Borrowed(s)
    }
}

impl<'a> CodecRun<'a, HexString> {
    pub fn into_images(self) -> (&'a str, HexString) {
        (self.preimage, self.postimage)
    }
}

impl<'a, T: Clone + rust_runtime::Target> CodecRun<'a, T> {
    pub fn rt_diff(&self) where T: Builder {
        let before: &str = self.preimage;
        let after: String = self.postimage.clone().into_hex();

        print!("\x1b[1mrt_diff\x1b[0m: ");
        for i in dissimilar::diff(before, &after) {
            match i {
                dissimilar::Chunk::Equal(seg) => {
                    print!("{}", abbrev(seg));
                }
                dissimilar::Chunk::Delete(seg) => {
                    print!("\x1b[91m{}\x1b[0m", seg);
                }
                dissimilar::Chunk::Insert(seg) => {
                    print!("\x1b[92m{}\x1b[0m", seg);
                }
            }
        }
        println!();
    }
}

impl<'a, T> std::fmt::Display
    for CodecRun<'a, T>
    where T: Clone + Builder, <T as Builder>::Final: Display
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Codec run `{}`:\n\t`{}` =>\n\t`{}` =>\n\t`{}` =>\n\t`{}`",
            self.ident,
            self.preimage,
            self.image,
            self.postimage.clone().finalize(),
            self.postimage.clone().into_hex()
        )
    }
}

#[derive(Debug)]
pub struct UnknownCodec<'a>(&'a str);

impl<'a> Display for UnknownCodec<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Unknown codec `{}`", self.0)
    }
}

macro_rules! invoke_run {
    ($codec:ty, $ident:ident, $preimage:ident) => {
        {
        let _image = <$codec>::decode_memo(hex!($preimage));
        let image = format!("{:?}", _image);
        let postimage = _image.encode::<T>();
        Ok(CodecRun {
            $ident,
            $preimage,
            image,
            postimage,
        })
        }
    };
}

macro_rules! codec_matcher {
    { $ident:ident, $preimage:ident; $(($id:pat, $codec:ty)),* $(,)? } => {
        match $ident {
            $( $id => { invoke_run!($codec, $ident, $preimage) }, )*
            _ => Err(UnknownCodec($ident)),
        }
    };
}

pub fn run_codec<'a, T: Clone + rust_runtime::Target>(
    ident: &'a str,
    preimage: &'a str
) -> Result<CodecRun<'a, T>, UnknownCodec<'a>> {
    codec_matcher! {
        ident, preimage;
        // generated
        ("alpha.block_header", alpha::block_header::AlphaBlockHeader),
        ("alpha.block_header.contents", alpha::block_header__contents::AlphaBlockHeaderContents),
        ("alpha.block_header.protocol_data", alpha::block_header__protocol_data::AlphaBlockHeaderProtocolData),
        ("alpha.block_header.raw", alpha::block_header__raw::AlphaBlockHeaderRaw),
        ("alpha.block_header.shell_header", alpha::block_header__shell_header::AlphaBlockHeaderShellHeader),
        ("alpha.block_header.unsigned", alpha::block_header__unsigned::AlphaBlockHeaderUnsigned),
        ("alpha.constants", alpha::constants::AlphaConstants),
        ("alpha.constants.fixed", alpha::constants__fixed::AlphaConstantsFixed),
        ("alpha.constants.parametric", alpha::constants__parametric::AlphaConstantsParametric),
        ("alpha.contract", alpha::contract::AlphaContract),
        ("alpha.cycle", alpha::cycle::AlphaCycle),
        ("alpha.errors", alpha::errors::AlphaErrors),
        ("alpha.fa1.2.token_transfer", alpha::fa1__2__token_transfer::AlphaFa12TokenTransfer),
        ("alpha.fitness", alpha::fitness::AlphaFitness),
        ("alpha.gas", alpha::gas::AlphaGas),
        ("alpha.gas.cost", alpha::gas__cost::AlphaGasCost),
        ("alpha.lazy_storage_diff", alpha::lazy_storage_diff::AlphaLazyStorageDiff),
        ("alpha.level", alpha::level::AlphaLevel),
        ("alpha.nonce", alpha::nonce::AlphaNonce),
        ("alpha.operation", alpha::operation::AlphaOperation),
        ("alpha.operation.contents", alpha::operation__contents::AlphaOperationContents),
        ("alpha.operation.contents_list", alpha::operation__contents_list::AlphaOperationContentsList),
        ("alpha.operation.internal", alpha::operation__internal::AlphaOperationInternal),
        ("alpha.operation.protocol_data", alpha::operation__protocol_data::AlphaOperationProtocolData),
        ("alpha.operation.raw", alpha::operation__raw::AlphaOperationRaw),
        ("alpha.operation.unsigned", alpha::operation__unsigned::AlphaOperationUnsigned),
        ("alpha.parameters", alpha::parameters::AlphaParameters),
        ("alpha.period", alpha::period::AlphaPeriod),
        ("alpha.raw_level", alpha::raw_level::AlphaRawLevel),
        ("alpha.receipt.balance_updates", alpha::receipt__balance_updates::AlphaReceiptBalanceUpdates),
        ("alpha.script", alpha::script::AlphaScript),
        ("alpha.script.expr", alpha::script__expr::AlphaScriptExpr),
        ("alpha.script.lazy_expr", alpha::script__lazy_expr::AlphaScriptLazyExpr),
        ("alpha.script.loc", alpha::script__loc::AlphaScriptLoc),
        ("alpha.script.prim", alpha::script__prim::AlphaScriptPrim),
        ("alpha.seed", alpha::seed::AlphaSeed),
        ("alpha.tez", alpha::tez::AlphaTez),
        ("alpha.timestamp", alpha::timestamp::AlphaTimestamp),
        ("alpha.vote.ballot", alpha::vote__ballot::AlphaVoteBallot),
        ("alpha.vote.ballots", alpha::vote__ballots::AlphaVoteBallots),
        ("alpha.vote.listings", alpha::vote__listings::AlphaVoteListings),
        ("alpha.voting_period", alpha::voting_period::AlphaVotingPeriod),
        ("alpha.voting_period.kind", alpha::voting_period__kind::AlphaVotingPeriodKind),
        ("babylon.block_header", proto005_psbabym1::block_header::Proto005PsBabyM1BlockHeader),
        ("babylon.block_header.contents", proto005_psbabym1::block_header__contents::Proto005PsBabyM1BlockHeaderContents),
        ("babylon.block_header.protocol_data", proto005_psbabym1::block_header__protocol_data::Proto005PsBabyM1BlockHeaderProtocolData),
        ("babylon.block_header.raw", proto005_psbabym1::block_header__raw::Proto005PsBabyM1BlockHeaderRaw),
        ("babylon.block_header.shell_header", proto005_psbabym1::block_header__shell_header::Proto005PsBabyM1BlockHeaderShellHeader),
        ("babylon.block_header.unsigned", proto005_psbabym1::block_header__unsigned::Proto005PsBabyM1BlockHeaderUnsigned),
        ("babylon.constants", proto005_psbabym1::constants::Proto005PsBabyM1Constants),
        ("babylon.constants.fixed", proto005_psbabym1::constants__fixed::Proto005PsBabyM1ConstantsFixed),
        ("babylon.constants.parametric", proto005_psbabym1::constants__parametric::Proto005PsBabyM1ConstantsParametric),
        ("babylon.contract", proto005_psbabym1::contract::Proto005PsBabyM1Contract),
        ("babylon.contract.big_map_diff", proto005_psbabym1::contract__big_map_diff::Proto005PsBabyM1ContractBigMapDiff),
        ("babylon.cycle", proto005_psbabym1::cycle::Proto005PsBabyM1Cycle),
        ("babylon.delegate.balance_updates", proto005_psbabym1::delegate__balance_updates::Proto005PsBabyM1DelegateBalanceUpdates),
        ("babylon.delegate.frozen_balance", proto005_psbabym1::delegate__frozen_balance::Proto005PsBabyM1DelegateFrozenBalance),
        ("babylon.delegate.frozen_balance_by_cycles", proto005_psbabym1::delegate__frozen_balance_by_cycles::Proto005PsBabyM1DelegateFrozenBalanceByCycles),
        ("babylon.errors", proto005_psbabym1::errors::Proto005PsBabyM1Errors),
        ("babylon.fitness", proto005_psbabym1::fitness::Proto005PsBabyM1Fitness),
        ("babylon.gas", proto005_psbabym1::gas::Proto005PsBabyM1Gas),
        ("babylon.gas.cost", proto005_psbabym1::gas__cost::Proto005PsBabyM1GasCost),
        ("babylon.level", proto005_psbabym1::level::Proto005PsBabyM1Level),
        ("babylon.nonce", proto005_psbabym1::nonce::Proto005PsBabyM1Nonce),
        ("babylon.operation", proto005_psbabym1::operation::Proto005PsBabyM1Operation),
        ("babylon.operation.contents", proto005_psbabym1::operation__contents::Proto005PsBabyM1OperationContents),
        ("babylon.operation.contents_list", proto005_psbabym1::operation__contents_list::Proto005PsBabyM1OperationContentsList),
        ("babylon.operation.internal", proto005_psbabym1::operation__internal::Proto005PsBabyM1OperationInternal),
        ("babylon.operation.protocol_data", proto005_psbabym1::operation__protocol_data::Proto005PsBabyM1OperationProtocolData),
        ("babylon.operation.raw", proto005_psbabym1::operation__raw::Proto005PsBabyM1OperationRaw),
        ("babylon.operation.unsigned", proto005_psbabym1::operation__unsigned::Proto005PsBabyM1OperationUnsigned),
        ("babylon.parameters", proto005_psbabym1::parameters::Proto005PsBabyM1Parameters),
        ("babylon.period", proto005_psbabym1::period::Proto005PsBabyM1Period),
        ("babylon.raw_level", proto005_psbabym1::raw_level::Proto005PsBabyM1RawLevel),
        ("babylon.roll", proto005_psbabym1::roll::Proto005PsBabyM1Roll),
        ("babylon.script", proto005_psbabym1::script::Proto005PsBabyM1Script),
        ("babylon.script.expr", proto005_psbabym1::script__expr::Proto005PsBabyM1ScriptExpr),
        ("babylon.script.lazy_expr", proto005_psbabym1::script__lazy_expr::Proto005PsBabyM1ScriptLazyExpr),
        ("babylon.script.loc", proto005_psbabym1::script__loc::Proto005PsBabyM1ScriptLoc),
        ("babylon.script.prim", proto005_psbabym1::script__prim::Proto005PsBabyM1ScriptPrim),
        ("babylon.seed", proto005_psbabym1::seed::Proto005PsBabyM1Seed),
        ("babylon.tez", proto005_psbabym1::tez::Proto005PsBabyM1Tez),
        ("babylon.timestamp", proto005_psbabym1::timestamp::Proto005PsBabyM1Timestamp),
        ("babylon.vote.ballot", proto005_psbabym1::vote__ballot::Proto005PsBabyM1VoteBallot),
        ("babylon.vote.ballots", proto005_psbabym1::vote__ballots::Proto005PsBabyM1VoteBallots),
        ("babylon.vote.listings", proto005_psbabym1::vote__listings::Proto005PsBabyM1VoteListings),
        ("babylon.voting_period", proto005_psbabym1::voting_period::Proto005PsBabyM1VotingPeriod),
        ("babylon.voting_period.kind", proto005_psbabym1::voting_period__kind::Proto005PsBabyM1VotingPeriodKind),
        ("block_header", etc::block_header::BlockHeader),
        ("block_header.shell", etc::block_header__shell::BlockHeaderShell),
        ("block_locator", etc::block_locator::BlockLocator),
        ("carthage.block_header", proto006_pscartha::block_header::Proto006PsCARTHABlockHeader),
        ("carthage.block_header.contents", proto006_pscartha::block_header__contents::Proto006PsCARTHABlockHeaderContents),
        ("carthage.block_header.protocol_data", proto006_pscartha::block_header__protocol_data::Proto006PsCARTHABlockHeaderProtocolData),
        ("carthage.block_header.raw", proto006_pscartha::block_header__raw::Proto006PsCARTHABlockHeaderRaw),
        ("carthage.block_header.shell_header", proto006_pscartha::block_header__shell_header::Proto006PsCARTHABlockHeaderShellHeader),
        ("carthage.block_header.unsigned", proto006_pscartha::block_header__unsigned::Proto006PsCARTHABlockHeaderUnsigned),
        ("carthage.constants", proto006_pscartha::constants::Proto006PsCARTHAConstants),
        ("carthage.constants.fixed", proto006_pscartha::constants__fixed::Proto006PsCARTHAConstantsFixed),
        ("carthage.constants.parametric", proto006_pscartha::constants__parametric::Proto006PsCARTHAConstantsParametric),
        ("carthage.contract", proto006_pscartha::contract::Proto006PsCARTHAContract),
        ("carthage.contract.big_map_diff", proto006_pscartha::contract__big_map_diff::Proto006PsCARTHAContractBigMapDiff),
        ("carthage.cycle", proto006_pscartha::cycle::Proto006PsCARTHACycle),
        ("carthage.delegate.balance_updates", proto006_pscartha::delegate__balance_updates::Proto006PsCARTHADelegateBalanceUpdates),
        ("carthage.delegate.frozen_balance", proto006_pscartha::delegate__frozen_balance::Proto006PsCARTHADelegateFrozenBalance),
        ("carthage.delegate.frozen_balance_by_cycles", proto006_pscartha::delegate__frozen_balance_by_cycles::Proto006PsCARTHADelegateFrozenBalanceByCycles),
        ("carthage.errors", proto006_pscartha::errors::Proto006PsCARTHAErrors),
        ("carthage.fitness", proto006_pscartha::fitness::Proto006PsCARTHAFitness),
        ("carthage.gas", proto006_pscartha::gas::Proto006PsCARTHAGas),
        ("carthage.gas.cost", proto006_pscartha::gas__cost::Proto006PsCARTHAGasCost),
        ("carthage.level", proto006_pscartha::level::Proto006PsCARTHALevel),
        ("carthage.nonce", proto006_pscartha::nonce::Proto006PsCARTHANonce),
        ("carthage.operation", proto006_pscartha::operation::Proto006PsCARTHAOperation),
        ("carthage.operation.contents", proto006_pscartha::operation__contents::Proto006PsCARTHAOperationContents),
        ("carthage.operation.contents_list", proto006_pscartha::operation__contents_list::Proto006PsCARTHAOperationContentsList),
        ("carthage.operation.internal", proto006_pscartha::operation__internal::Proto006PsCARTHAOperationInternal),
        ("carthage.operation.protocol_data", proto006_pscartha::operation__protocol_data::Proto006PsCARTHAOperationProtocolData),
        ("carthage.operation.raw", proto006_pscartha::operation__raw::Proto006PsCARTHAOperationRaw),
        ("carthage.operation.unsigned", proto006_pscartha::operation__unsigned::Proto006PsCARTHAOperationUnsigned),
        ("carthage.parameters", proto006_pscartha::parameters::Proto006PsCARTHAParameters),
        ("carthage.period", proto006_pscartha::period::Proto006PsCARTHAPeriod),
        ("carthage.raw_level", proto006_pscartha::raw_level::Proto006PsCARTHARawLevel),
        ("carthage.roll", proto006_pscartha::roll::Proto006PsCARTHARoll),
        ("carthage.script", proto006_pscartha::script::Proto006PsCARTHAScript),
        ("carthage.script.expr", proto006_pscartha::script__expr::Proto006PsCARTHAScriptExpr),
        ("carthage.script.lazy_expr", proto006_pscartha::script__lazy_expr::Proto006PsCARTHAScriptLazyExpr),
        ("carthage.script.loc", proto006_pscartha::script__loc::Proto006PsCARTHAScriptLoc),
        ("carthage.script.prim", proto006_pscartha::script__prim::Proto006PsCARTHAScriptPrim),
        ("carthage.seed", proto006_pscartha::seed::Proto006PsCARTHASeed),
        ("carthage.tez", proto006_pscartha::tez::Proto006PsCARTHATez),
        ("carthage.timestamp", proto006_pscartha::timestamp::Proto006PsCARTHATimestamp),
        ("carthage.vote.ballot", proto006_pscartha::vote__ballot::Proto006PsCARTHAVoteBallot),
        ("carthage.vote.ballots", proto006_pscartha::vote__ballots::Proto006PsCARTHAVoteBallots),
        ("carthage.vote.listings", proto006_pscartha::vote__listings::Proto006PsCARTHAVoteListings),
        ("carthage.voting_period", proto006_pscartha::voting_period::Proto006PsCARTHAVotingPeriod),
        ("carthage.voting_period.kind", proto006_pscartha::voting_period__kind::Proto006PsCARTHAVotingPeriodKind),
        ("context_hash_version", etc::context_hash_version::ContextHashVersion),
        ("delphi.block_header", proto007_psdelph1::block_header::Proto007PsDELPH1BlockHeader),
        ("delphi.block_header.contents", proto007_psdelph1::block_header__contents::Proto007PsDELPH1BlockHeaderContents),
        ("delphi.block_header.protocol_data", proto007_psdelph1::block_header__protocol_data::Proto007PsDELPH1BlockHeaderProtocolData),
        ("delphi.block_header.raw", proto007_psdelph1::block_header__raw::Proto007PsDELPH1BlockHeaderRaw),
        ("delphi.block_header.shell_header", proto007_psdelph1::block_header__shell_header::Proto007PsDELPH1BlockHeaderShellHeader),
        ("delphi.block_header.unsigned", proto007_psdelph1::block_header__unsigned::Proto007PsDELPH1BlockHeaderUnsigned),
        ("delphi.constants", proto007_psdelph1::constants::Proto007PsDELPH1Constants),
        ("delphi.constants.fixed", proto007_psdelph1::constants__fixed::Proto007PsDELPH1ConstantsFixed),
        ("delphi.constants.parametric", proto007_psdelph1::constants__parametric::Proto007PsDELPH1ConstantsParametric),
        ("delphi.contract", proto007_psdelph1::contract::Proto007PsDELPH1Contract),
        ("delphi.contract.big_map_diff", proto007_psdelph1::contract__big_map_diff::Proto007PsDELPH1ContractBigMapDiff),
        ("delphi.cycle", proto007_psdelph1::cycle::Proto007PsDELPH1Cycle),
        ("delphi.delegate.balance_updates", proto007_psdelph1::delegate__balance_updates::Proto007PsDELPH1DelegateBalanceUpdates),
        ("delphi.delegate.frozen_balance", proto007_psdelph1::delegate__frozen_balance::Proto007PsDELPH1DelegateFrozenBalance),
        ("delphi.delegate.frozen_balance_by_cycles", proto007_psdelph1::delegate__frozen_balance_by_cycles::Proto007PsDELPH1DelegateFrozenBalanceByCycles),
        ("delphi.errors", proto007_psdelph1::errors::Proto007PsDELPH1Errors),
        ("delphi.fitness", proto007_psdelph1::fitness::Proto007PsDELPH1Fitness),
        ("delphi.gas", proto007_psdelph1::gas::Proto007PsDELPH1Gas),
        ("delphi.gas.cost", proto007_psdelph1::gas__cost::Proto007PsDELPH1GasCost),
        ("delphi.level", proto007_psdelph1::level::Proto007PsDELPH1Level),
        ("delphi.nonce", proto007_psdelph1::nonce::Proto007PsDELPH1Nonce),
        ("delphi.operation", proto007_psdelph1::operation::Proto007PsDELPH1Operation),
        ("delphi.operation.contents", proto007_psdelph1::operation__contents::Proto007PsDELPH1OperationContents),
        ("delphi.operation.contents_list", proto007_psdelph1::operation__contents_list::Proto007PsDELPH1OperationContentsList),
        ("delphi.operation.internal", proto007_psdelph1::operation__internal::Proto007PsDELPH1OperationInternal),
        ("delphi.operation.protocol_data", proto007_psdelph1::operation__protocol_data::Proto007PsDELPH1OperationProtocolData),
        ("delphi.operation.raw", proto007_psdelph1::operation__raw::Proto007PsDELPH1OperationRaw),
        ("delphi.operation.unsigned", proto007_psdelph1::operation__unsigned::Proto007PsDELPH1OperationUnsigned),
        ("delphi.parameters", proto007_psdelph1::parameters::Proto007PsDELPH1Parameters),
        ("delphi.period", proto007_psdelph1::period::Proto007PsDELPH1Period),
        ("delphi.raw_level", proto007_psdelph1::raw_level::Proto007PsDELPH1RawLevel),
        ("delphi.roll", proto007_psdelph1::roll::Proto007PsDELPH1Roll),
        ("delphi.script", proto007_psdelph1::script::Proto007PsDELPH1Script),
        ("delphi.script.expr", proto007_psdelph1::script__expr::Proto007PsDELPH1ScriptExpr),
        ("delphi.script.lazy_expr", proto007_psdelph1::script__lazy_expr::Proto007PsDELPH1ScriptLazyExpr),
        ("delphi.script.loc", proto007_psdelph1::script__loc::Proto007PsDELPH1ScriptLoc),
        ("delphi.script.prim", proto007_psdelph1::script__prim::Proto007PsDELPH1ScriptPrim),
        ("delphi.seed", proto007_psdelph1::seed::Proto007PsDELPH1Seed),
        ("delphi.tez", proto007_psdelph1::tez::Proto007PsDELPH1Tez),
        ("delphi.timestamp", proto007_psdelph1::timestamp::Proto007PsDELPH1Timestamp),
        ("delphi.vote.ballot", proto007_psdelph1::vote__ballot::Proto007PsDELPH1VoteBallot),
        ("delphi.vote.ballots", proto007_psdelph1::vote__ballots::Proto007PsDELPH1VoteBallots),
        ("delphi.vote.listings", proto007_psdelph1::vote__listings::Proto007PsDELPH1VoteListings),
        ("delphi.voting_period", proto007_psdelph1::voting_period::Proto007PsDELPH1VotingPeriod),
        ("delphi.voting_period.kind", proto007_psdelph1::voting_period__kind::Proto007PsDELPH1VotingPeriodKind),
        ("distributed_db_version", etc::distributed_db_version::DistributedDbVersion),
        ("distributed_db_version.name", etc::distributed_db_version__name::DistributedDbVersionName),
        ("edo.block_header", proto008_ptedo2zk::block_header::Proto008PtEdo2ZkBlockHeader),
        ("edo.block_header.contents", proto008_ptedo2zk::block_header__contents::Proto008PtEdo2ZkBlockHeaderContents),
        ("edo.block_header.protocol_data", proto008_ptedo2zk::block_header__protocol_data::Proto008PtEdo2ZkBlockHeaderProtocolData),
        ("edo.block_header.raw", proto008_ptedo2zk::block_header__raw::Proto008PtEdo2ZkBlockHeaderRaw),
        ("edo.block_header.shell_header", proto008_ptedo2zk::block_header__shell_header::Proto008PtEdo2ZkBlockHeaderShellHeader),
        ("edo.block_header.unsigned", proto008_ptedo2zk::block_header__unsigned::Proto008PtEdo2ZkBlockHeaderUnsigned),
        ("edo.constants", proto008_ptedo2zk::constants::Proto008PtEdo2ZkConstants),
        ("edo.constants.fixed", proto008_ptedo2zk::constants__fixed::Proto008PtEdo2ZkConstantsFixed),
        ("edo.constants.parametric", proto008_ptedo2zk::constants__parametric::Proto008PtEdo2ZkConstantsParametric),
        ("edo.contract", proto008_ptedo2zk::contract::Proto008PtEdo2ZkContract),
        ("edo.contract.big_map_diff", proto008_ptedo2zk::contract__big_map_diff::Proto008PtEdo2ZkContractBigMapDiff),
        ("edo.cycle", proto008_ptedo2zk::cycle::Proto008PtEdo2ZkCycle),
        ("edo.delegate.balance_updates", proto008_ptedo2zk::delegate__balance_updates::Proto008PtEdo2ZkDelegateBalanceUpdates),
        ("edo.delegate.frozen_balance", proto008_ptedo2zk::delegate__frozen_balance::Proto008PtEdo2ZkDelegateFrozenBalance),
        ("edo.delegate.frozen_balance_by_cycles", proto008_ptedo2zk::delegate__frozen_balance_by_cycles::Proto008PtEdo2ZkDelegateFrozenBalanceByCycles),
        ("edo.errors", proto008_ptedo2zk::errors::Proto008PtEdo2ZkErrors),
        ("edo.fa1.2.token_transfer", proto008_ptedo2zk::fa1__2__token_transfer::Proto008PtEdo2ZkFa12TokenTransfer),
        ("edo.fitness", proto008_ptedo2zk::fitness::Proto008PtEdo2ZkFitness),
        ("edo.gas", proto008_ptedo2zk::gas::Proto008PtEdo2ZkGas),
        ("edo.gas.cost", proto008_ptedo2zk::gas__cost::Proto008PtEdo2ZkGasCost),
        ("edo.lazy_storage_diff", proto008_ptedo2zk::lazy_storage_diff::Proto008PtEdo2ZkLazyStorageDiff),
        ("edo.level", proto008_ptedo2zk::level::Proto008PtEdo2ZkLevel),
        ("edo.nonce", proto008_ptedo2zk::nonce::Proto008PtEdo2ZkNonce),
        ("edo.operation", proto008_ptedo2zk::operation::Proto008PtEdo2ZkOperation),
        ("edo.operation.contents", proto008_ptedo2zk::operation__contents::Proto008PtEdo2ZkOperationContents),
        ("edo.operation.contents_list", proto008_ptedo2zk::operation__contents_list::Proto008PtEdo2ZkOperationContentsList),
        ("edo.operation.internal", proto008_ptedo2zk::operation__internal::Proto008PtEdo2ZkOperationInternal),
        ("edo.operation.protocol_data", proto008_ptedo2zk::operation__protocol_data::Proto008PtEdo2ZkOperationProtocolData),
        ("edo.operation.raw", proto008_ptedo2zk::operation__raw::Proto008PtEdo2ZkOperationRaw),
        ("edo.operation.unsigned", proto008_ptedo2zk::operation__unsigned::Proto008PtEdo2ZkOperationUnsigned),
        ("edo.parameters", proto008_ptedo2zk::parameters::Proto008PtEdo2ZkParameters),
        ("edo.period", proto008_ptedo2zk::period::Proto008PtEdo2ZkPeriod),
        ("edo.raw_level", proto008_ptedo2zk::raw_level::Proto008PtEdo2ZkRawLevel),
        ("edo.roll", proto008_ptedo2zk::roll::Proto008PtEdo2ZkRoll),
        ("edo.script", proto008_ptedo2zk::script::Proto008PtEdo2ZkScript),
        ("edo.script.expr", proto008_ptedo2zk::script__expr::Proto008PtEdo2ZkScriptExpr),
        ("edo.script.lazy_expr", proto008_ptedo2zk::script__lazy_expr::Proto008PtEdo2ZkScriptLazyExpr),
        ("edo.script.loc", proto008_ptedo2zk::script__loc::Proto008PtEdo2ZkScriptLoc),
        ("edo.script.prim", proto008_ptedo2zk::script__prim::Proto008PtEdo2ZkScriptPrim),
        ("edo.seed", proto008_ptedo2zk::seed::Proto008PtEdo2ZkSeed),
        ("edo.tez", proto008_ptedo2zk::tez::Proto008PtEdo2ZkTez),
        ("edo.timestamp", proto008_ptedo2zk::timestamp::Proto008PtEdo2ZkTimestamp),
        ("edo.vote.ballot", proto008_ptedo2zk::vote__ballot::Proto008PtEdo2ZkVoteBallot),
        ("edo.vote.ballots", proto008_ptedo2zk::vote__ballots::Proto008PtEdo2ZkVoteBallots),
        ("edo.vote.listings", proto008_ptedo2zk::vote__listings::Proto008PtEdo2ZkVoteListings),
        ("edo.voting_period", proto008_ptedo2zk::voting_period::Proto008PtEdo2ZkVotingPeriod),
        ("edo.voting_period.kind", proto008_ptedo2zk::voting_period__kind::Proto008PtEdo2ZkVotingPeriodKind),
        ("florence.block_header", proto009_psfloren::block_header::Proto009PsFLorenBlockHeader),
        ("florence.block_header.contents", proto009_psfloren::block_header__contents::Proto009PsFLorenBlockHeaderContents),
        ("florence.block_header.protocol_data", proto009_psfloren::block_header__protocol_data::Proto009PsFLorenBlockHeaderProtocolData),
        ("florence.block_header.raw", proto009_psfloren::block_header__raw::Proto009PsFLorenBlockHeaderRaw),
        ("florence.block_header.shell_header", proto009_psfloren::block_header__shell_header::Proto009PsFLorenBlockHeaderShellHeader),
        ("florence.block_header.unsigned", proto009_psfloren::block_header__unsigned::Proto009PsFLorenBlockHeaderUnsigned),
        ("florence.constants", proto009_psfloren::constants::Proto009PsFLorenConstants),
        ("florence.constants.fixed", proto009_psfloren::constants__fixed::Proto009PsFLorenConstantsFixed),
        ("florence.constants.parametric", proto009_psfloren::constants__parametric::Proto009PsFLorenConstantsParametric),
        ("florence.contract", proto009_psfloren::contract::Proto009PsFLorenContract),
        ("florence.contract.big_map_diff", proto009_psfloren::contract__big_map_diff::Proto009PsFLorenContractBigMapDiff),
        ("florence.cycle", proto009_psfloren::cycle::Proto009PsFLorenCycle),
        ("florence.delegate.frozen_balance", proto009_psfloren::delegate__frozen_balance::Proto009PsFLorenDelegateFrozenBalance),
        ("florence.delegate.frozen_balance_by_cycles", proto009_psfloren::delegate__frozen_balance_by_cycles::Proto009PsFLorenDelegateFrozenBalanceByCycles),
        ("florence.errors", proto009_psfloren::errors::Proto009PsFLorenErrors),
        ("florence.fa1.2.token_transfer", proto009_psfloren::fa1__2__token_transfer::Proto009PsFLorenFa12TokenTransfer),
        ("florence.fitness", proto009_psfloren::fitness::Proto009PsFLorenFitness),
        ("florence.gas", proto009_psfloren::gas::Proto009PsFLorenGas),
        ("florence.gas.cost", proto009_psfloren::gas__cost::Proto009PsFLorenGasCost),
        ("florence.lazy_storage_diff", proto009_psfloren::lazy_storage_diff::Proto009PsFLorenLazyStorageDiff),
        ("florence.level", proto009_psfloren::level::Proto009PsFLorenLevel),
        ("florence.nonce", proto009_psfloren::nonce::Proto009PsFLorenNonce),
        ("florence.operation", proto009_psfloren::operation::Proto009PsFLorenOperation),
        ("florence.operation.contents", proto009_psfloren::operation__contents::Proto009PsFLorenOperationContents),
        ("florence.operation.contents_list", proto009_psfloren::operation__contents_list::Proto009PsFLorenOperationContentsList),
        ("florence.operation.internal", proto009_psfloren::operation__internal::Proto009PsFLorenOperationInternal),
        ("florence.operation.protocol_data", proto009_psfloren::operation__protocol_data::Proto009PsFLorenOperationProtocolData),
        ("florence.operation.raw", proto009_psfloren::operation__raw::Proto009PsFLorenOperationRaw),
        ("florence.operation.unsigned", proto009_psfloren::operation__unsigned::Proto009PsFLorenOperationUnsigned),
        ("florence.parameters", proto009_psfloren::parameters::Proto009PsFLorenParameters),
        ("florence.period", proto009_psfloren::period::Proto009PsFLorenPeriod),
        ("florence.raw_level", proto009_psfloren::raw_level::Proto009PsFLorenRawLevel),
        ("florence.receipt.balance_updates", proto009_psfloren::receipt__balance_updates::Proto009PsFLorenReceiptBalanceUpdates),
        ("florence.roll", proto009_psfloren::roll::Proto009PsFLorenRoll),
        ("florence.script", proto009_psfloren::script::Proto009PsFLorenScript),
        ("florence.script.expr", proto009_psfloren::script__expr::Proto009PsFLorenScriptExpr),
        ("florence.script.lazy_expr", proto009_psfloren::script__lazy_expr::Proto009PsFLorenScriptLazyExpr),
        ("florence.script.loc", proto009_psfloren::script__loc::Proto009PsFLorenScriptLoc),
        ("florence.script.prim", proto009_psfloren::script__prim::Proto009PsFLorenScriptPrim),
        ("florence.seed", proto009_psfloren::seed::Proto009PsFLorenSeed),
        ("florence.tez", proto009_psfloren::tez::Proto009PsFLorenTez),
        ("florence.timestamp", proto009_psfloren::timestamp::Proto009PsFLorenTimestamp),
        ("florence.vote.ballot", proto009_psfloren::vote__ballot::Proto009PsFLorenVoteBallot),
        ("florence.vote.ballots", proto009_psfloren::vote__ballots::Proto009PsFLorenVoteBallots),
        ("florence.vote.listings", proto009_psfloren::vote__listings::Proto009PsFLorenVoteListings),
        ("florence.voting_period", proto009_psfloren::voting_period::Proto009PsFLorenVotingPeriod),
        ("florence.voting_period.kind", proto009_psfloren::voting_period__kind::Proto009PsFLorenVotingPeriodKind),
        ("granada.block_header", proto010_ptgranad::block_header::Proto010PtGRANADBlockHeader),
        ("granada.block_header.contents", proto010_ptgranad::block_header__contents::Proto010PtGRANADBlockHeaderContents),
        ("granada.block_header.protocol_data", proto010_ptgranad::block_header__protocol_data::Proto010PtGRANADBlockHeaderProtocolData),
        ("granada.block_header.raw", proto010_ptgranad::block_header__raw::Proto010PtGRANADBlockHeaderRaw),
        ("granada.block_header.shell_header", proto010_ptgranad::block_header__shell_header::Proto010PtGRANADBlockHeaderShellHeader),
        ("granada.block_header.unsigned", proto010_ptgranad::block_header__unsigned::Proto010PtGRANADBlockHeaderUnsigned),
        ("granada.constants", proto010_ptgranad::constants::Proto010PtGRANADConstants),
        ("granada.constants.fixed", proto010_ptgranad::constants__fixed::Proto010PtGRANADConstantsFixed),
        ("granada.constants.parametric", proto010_ptgranad::constants__parametric::Proto010PtGRANADConstantsParametric),
        ("granada.contract", proto010_ptgranad::contract::Proto010PtGRANADContract),
        ("granada.contract.big_map_diff", proto010_ptgranad::contract__big_map_diff::Proto010PtGRANADContractBigMapDiff),
        ("granada.cycle", proto010_ptgranad::cycle::Proto010PtGRANADCycle),
        ("granada.delegate.frozen_balance", proto010_ptgranad::delegate__frozen_balance::Proto010PtGRANADDelegateFrozenBalance),
        ("granada.delegate.frozen_balance_by_cycles", proto010_ptgranad::delegate__frozen_balance_by_cycles::Proto010PtGRANADDelegateFrozenBalanceByCycles),
        ("granada.errors", proto010_ptgranad::errors::Proto010PtGRANADErrors),
        ("granada.fa1.2.token_transfer", proto010_ptgranad::fa1__2__token_transfer::Proto010PtGRANADFa12TokenTransfer),
        ("granada.fitness", proto010_ptgranad::fitness::Proto010PtGRANADFitness),
        ("granada.gas", proto010_ptgranad::gas::Proto010PtGRANADGas),
        ("granada.gas.cost", proto010_ptgranad::gas__cost::Proto010PtGRANADGasCost),
        ("granada.lazy_storage_diff", proto010_ptgranad::lazy_storage_diff::Proto010PtGRANADLazyStorageDiff),
        ("granada.level", proto010_ptgranad::level::Proto010PtGRANADLevel),
        ("granada.nonce", proto010_ptgranad::nonce::Proto010PtGRANADNonce),
        ("granada.operation", proto010_ptgranad::operation::Proto010PtGRANADOperation),
        ("granada.operation.contents", proto010_ptgranad::operation__contents::Proto010PtGRANADOperationContents),
        ("granada.operation.contents_list", proto010_ptgranad::operation__contents_list::Proto010PtGRANADOperationContentsList),
        ("granada.operation.internal", proto010_ptgranad::operation__internal::Proto010PtGRANADOperationInternal),
        ("granada.operation.protocol_data", proto010_ptgranad::operation__protocol_data::Proto010PtGRANADOperationProtocolData),
        ("granada.operation.raw", proto010_ptgranad::operation__raw::Proto010PtGRANADOperationRaw),
        ("granada.operation.unsigned", proto010_ptgranad::operation__unsigned::Proto010PtGRANADOperationUnsigned),
        ("granada.parameters", proto010_ptgranad::parameters::Proto010PtGRANADParameters),
        ("granada.period", proto010_ptgranad::period::Proto010PtGRANADPeriod),
        ("granada.raw_level", proto010_ptgranad::raw_level::Proto010PtGRANADRawLevel),
        ("granada.receipt.balance_updates", proto010_ptgranad::receipt__balance_updates::Proto010PtGRANADReceiptBalanceUpdates),
        ("granada.roll", proto010_ptgranad::roll::Proto010PtGRANADRoll),
        ("granada.script", proto010_ptgranad::script::Proto010PtGRANADScript),
        ("granada.script.expr", proto010_ptgranad::script__expr::Proto010PtGRANADScriptExpr),
        ("granada.script.lazy_expr", proto010_ptgranad::script__lazy_expr::Proto010PtGRANADScriptLazyExpr),
        ("granada.script.loc", proto010_ptgranad::script__loc::Proto010PtGRANADScriptLoc),
        ("granada.script.prim", proto010_ptgranad::script__prim::Proto010PtGRANADScriptPrim),
        ("granada.seed", proto010_ptgranad::seed::Proto010PtGRANADSeed),
        ("granada.tez", proto010_ptgranad::tez::Proto010PtGRANADTez),
        ("granada.timestamp", proto010_ptgranad::timestamp::Proto010PtGRANADTimestamp),
        ("granada.vote.ballot", proto010_ptgranad::vote__ballot::Proto010PtGRANADVoteBallot),
        ("granada.vote.ballots", proto010_ptgranad::vote__ballots::Proto010PtGRANADVoteBallots),
        ("granada.vote.listings", proto010_ptgranad::vote__listings::Proto010PtGRANADVoteListings),
        ("granada.voting_period", proto010_ptgranad::voting_period::Proto010PtGRANADVotingPeriod),
        ("granada.voting_period.kind", proto010_ptgranad::voting_period__kind::Proto010PtGRANADVotingPeriodKind),
        ("ground.N", ground::n::GroundN),
        ("ground.Z", ground::z::GroundZ),
        ("ground.bool", ground::bool::GroundBool),
        ("ground.bytes", ground::bytes::GroundBytes),
        ("ground.empty", ground::empty::GroundEmpty),
        ("ground.float", ground::float::GroundFloat),
        ("ground.int16", ground::int16::GroundInt16),
        ("ground.int31", ground::int31::GroundInt31),
        ("ground.int32", ground::int32::GroundInt32),
        ("ground.int64", ground::int64::GroundInt64),
        ("ground.int8", ground::int8::GroundInt8),
        ("ground.null", ground::null::GroundNull),
        ("ground.string", ground::string::GroundString),
        ("ground.uint16", ground::uint16::GroundUint16),
        ("ground.uint8", ground::uint8::GroundUint8),
        ("ground.unit", ground::unit::GroundUnit),
        ("ground.variable.bytes", ground::variable__bytes::GroundVariableBytes),
        ("ground.variable.string", ground::variable__string::GroundVariableString),
        ("hangzhou.block_header", proto011_pthangz2::block_header::Proto011PtHangz2BlockHeader),
        ("hangzhou.block_header.contents", proto011_pthangz2::block_header__contents::Proto011PtHangz2BlockHeaderContents),
        ("hangzhou.block_header.protocol_data", proto011_pthangz2::block_header__protocol_data::Proto011PtHangz2BlockHeaderProtocolData),
        ("hangzhou.block_header.raw", proto011_pthangz2::block_header__raw::Proto011PtHangz2BlockHeaderRaw),
        ("hangzhou.block_header.shell_header", proto011_pthangz2::block_header__shell_header::Proto011PtHangz2BlockHeaderShellHeader),
        ("hangzhou.block_header.unsigned", proto011_pthangz2::block_header__unsigned::Proto011PtHangz2BlockHeaderUnsigned),
        ("hangzhou.constants", proto011_pthangz2::constants::Proto011PtHangz2Constants),
        ("hangzhou.constants.fixed", proto011_pthangz2::constants__fixed::Proto011PtHangz2ConstantsFixed),
        ("hangzhou.constants.parametric", proto011_pthangz2::constants__parametric::Proto011PtHangz2ConstantsParametric),
        ("hangzhou.contract", proto011_pthangz2::contract::Proto011PtHangz2Contract),
        ("hangzhou.contract.big_map_diff", proto011_pthangz2::contract__big_map_diff::Proto011PtHangz2ContractBigMapDiff),
        ("hangzhou.cycle", proto011_pthangz2::cycle::Proto011PtHangz2Cycle),
        ("hangzhou.delegate.frozen_balance", proto011_pthangz2::delegate__frozen_balance::Proto011PtHangz2DelegateFrozenBalance),
        ("hangzhou.delegate.frozen_balance_by_cycles", proto011_pthangz2::delegate__frozen_balance_by_cycles::Proto011PtHangz2DelegateFrozenBalanceByCycles),
        ("hangzhou.errors", proto011_pthangz2::errors::Proto011PtHangz2Errors),
        ("hangzhou.fa1.2.token_transfer", proto011_pthangz2::fa1__2__token_transfer::Proto011PtHangz2Fa12TokenTransfer),
        ("hangzhou.fitness", proto011_pthangz2::fitness::Proto011PtHangz2Fitness),
        ("hangzhou.gas", proto011_pthangz2::gas::Proto011PtHangz2Gas),
        ("hangzhou.gas.cost", proto011_pthangz2::gas__cost::Proto011PtHangz2GasCost),
        ("hangzhou.lazy_storage_diff", proto011_pthangz2::lazy_storage_diff::Proto011PtHangz2LazyStorageDiff),
        ("hangzhou.level", proto011_pthangz2::level::Proto011PtHangz2Level),
        ("hangzhou.nonce", proto011_pthangz2::nonce::Proto011PtHangz2Nonce),
        ("hangzhou.operation", proto011_pthangz2::operation::Proto011PtHangz2Operation),
        ("hangzhou.operation.contents", proto011_pthangz2::operation__contents::Proto011PtHangz2OperationContents),
        ("hangzhou.operation.contents_list", proto011_pthangz2::operation__contents_list::Proto011PtHangz2OperationContentsList),
        ("hangzhou.operation.internal", proto011_pthangz2::operation__internal::Proto011PtHangz2OperationInternal),
        ("hangzhou.operation.protocol_data", proto011_pthangz2::operation__protocol_data::Proto011PtHangz2OperationProtocolData),
        ("hangzhou.operation.raw", proto011_pthangz2::operation__raw::Proto011PtHangz2OperationRaw),
        ("hangzhou.operation.unsigned", proto011_pthangz2::operation__unsigned::Proto011PtHangz2OperationUnsigned),
        ("hangzhou.parameters", proto011_pthangz2::parameters::Proto011PtHangz2Parameters),
        ("hangzhou.period", proto011_pthangz2::period::Proto011PtHangz2Period),
        ("hangzhou.raw_level", proto011_pthangz2::raw_level::Proto011PtHangz2RawLevel),
        ("hangzhou.receipt.balance_updates", proto011_pthangz2::receipt__balance_updates::Proto011PtHangz2ReceiptBalanceUpdates),
        ("hangzhou.roll", proto011_pthangz2::roll::Proto011PtHangz2Roll),
        ("hangzhou.script", proto011_pthangz2::script::Proto011PtHangz2Script),
        ("hangzhou.script.expr", proto011_pthangz2::script__expr::Proto011PtHangz2ScriptExpr),
        ("hangzhou.script.lazy_expr", proto011_pthangz2::script__lazy_expr::Proto011PtHangz2ScriptLazyExpr),
        ("hangzhou.script.loc", proto011_pthangz2::script__loc::Proto011PtHangz2ScriptLoc),
        ("hangzhou.script.prim", proto011_pthangz2::script__prim::Proto011PtHangz2ScriptPrim),
        ("hangzhou.seed", proto011_pthangz2::seed::Proto011PtHangz2Seed),
        ("hangzhou.tez", proto011_pthangz2::tez::Proto011PtHangz2Tez),
        ("hangzhou.timestamp", proto011_pthangz2::timestamp::Proto011PtHangz2Timestamp),
        ("hangzhou.vote.ballot", proto011_pthangz2::vote__ballot::Proto011PtHangz2VoteBallot),
        ("hangzhou.vote.ballots", proto011_pthangz2::vote__ballots::Proto011PtHangz2VoteBallots),
        ("hangzhou.vote.listings", proto011_pthangz2::vote__listings::Proto011PtHangz2VoteListings),
        ("hangzhou.voting_period", proto011_pthangz2::voting_period::Proto011PtHangz2VotingPeriod),
        ("hangzhou.voting_period.kind", proto011_pthangz2::voting_period__kind::Proto011PtHangz2VotingPeriodKind),
        ("ithaca.block_header", proto012_psithaca::block_header::Proto012PsithacaBlockHeader),
        ("ithaca.block_header.contents", proto012_psithaca::block_header__contents::Proto012PsithacaBlockHeaderContents),
        ("ithaca.block_header.protocol_data", proto012_psithaca::block_header__protocol_data::Proto012PsithacaBlockHeaderProtocolData),
        ("ithaca.block_header.raw", proto012_psithaca::block_header__raw::Proto012PsithacaBlockHeaderRaw),
        ("ithaca.block_header.shell_header", proto012_psithaca::block_header__shell_header::Proto012PsithacaBlockHeaderShellHeader),
        ("ithaca.block_header.unsigned", proto012_psithaca::block_header__unsigned::Proto012PsithacaBlockHeaderUnsigned),
        ("ithaca.constants", proto012_psithaca::constants::Proto012PsithacaConstants),
        ("ithaca.constants.fixed", proto012_psithaca::constants__fixed::Proto012PsithacaConstantsFixed),
        ("ithaca.constants.parametric", proto012_psithaca::constants__parametric::Proto012PsithacaConstantsParametric),
        ("ithaca.contract", proto012_psithaca::contract::Proto012PsithacaContract),
        ("ithaca.contract.big_map_diff", proto012_psithaca::contract__big_map_diff::Proto012PsithacaContractBigMapDiff),
        ("ithaca.cycle", proto012_psithaca::cycle::Proto012PsithacaCycle),
        ("ithaca.errors", proto012_psithaca::errors::Proto012PsithacaErrors),
        ("ithaca.fa1.2.token_transfer", proto012_psithaca::fa1__2__token_transfer::Proto012PsithacaFa12TokenTransfer),
        ("ithaca.fitness", proto012_psithaca::fitness::Proto012PsithacaFitness),
        ("ithaca.gas", proto012_psithaca::gas::Proto012PsithacaGas),
        ("ithaca.gas.cost", proto012_psithaca::gas__cost::Proto012PsithacaGasCost),
        ("ithaca.lazy_storage_diff", proto012_psithaca::lazy_storage_diff::Proto012PsithacaLazyStorageDiff),
        ("ithaca.level", proto012_psithaca::level::Proto012PsithacaLevel),
        ("ithaca.nonce", proto012_psithaca::nonce::Proto012PsithacaNonce),
        ("ithaca.operation", proto012_psithaca::operation::Proto012PsithacaOperation),
        ("ithaca.operation.contents", proto012_psithaca::operation__contents::Proto012PsithacaOperationContents),
        ("ithaca.operation.contents_list", proto012_psithaca::operation__contents_list::Proto012PsithacaOperationContentsList),
        ("ithaca.operation.internal", proto012_psithaca::operation__internal::Proto012PsithacaOperationInternal),
        ("ithaca.operation.protocol_data", proto012_psithaca::operation__protocol_data::Proto012PsithacaOperationProtocolData),
        ("ithaca.operation.raw", proto012_psithaca::operation__raw::Proto012PsithacaOperationRaw),
        ("ithaca.operation.unsigned", proto012_psithaca::operation__unsigned::Proto012PsithacaOperationUnsigned),
        ("ithaca.parameters", proto012_psithaca::parameters::Proto012PsithacaParameters),
        ("ithaca.period", proto012_psithaca::period::Proto012PsithacaPeriod),
        ("ithaca.raw_level", proto012_psithaca::raw_level::Proto012PsithacaRawLevel),
        ("ithaca.receipt.balance_updates", proto012_psithaca::receipt__balance_updates::Proto012PsithacaReceiptBalanceUpdates),
        ("ithaca.script", proto012_psithaca::script::Proto012PsithacaScript),
        ("ithaca.script.expr", proto012_psithaca::script__expr::Proto012PsithacaScriptExpr),
        ("ithaca.script.lazy_expr", proto012_psithaca::script__lazy_expr::Proto012PsithacaScriptLazyExpr),
        ("ithaca.script.loc", proto012_psithaca::script__loc::Proto012PsithacaScriptLoc),
        ("ithaca.script.prim", proto012_psithaca::script__prim::Proto012PsithacaScriptPrim),
        ("ithaca.seed", proto012_psithaca::seed::Proto012PsithacaSeed),
        ("ithaca.tez", proto012_psithaca::tez::Proto012PsithacaTez),
        ("ithaca.timestamp", proto012_psithaca::timestamp::Proto012PsithacaTimestamp),
        ("ithaca.vote.ballot", proto012_psithaca::vote__ballot::Proto012PsithacaVoteBallot),
        ("ithaca.vote.ballots", proto012_psithaca::vote__ballots::Proto012PsithacaVoteBallots),
        ("ithaca.vote.listings", proto012_psithaca::vote__listings::Proto012PsithacaVoteListings),
        ("ithaca.voting_period", proto012_psithaca::voting_period::Proto012PsithacaVotingPeriod),
        ("ithaca.voting_period.kind", proto012_psithaca::voting_period__kind::Proto012PsithacaVotingPeriodKind),
        ("jakarta.block_header", proto013_ptjakart::block_header::Proto013PtJakartBlockHeader),
        ("jakarta.block_header.contents", proto013_ptjakart::block_header__contents::Proto013PtJakartBlockHeaderContents),
        ("jakarta.block_header.protocol_data", proto013_ptjakart::block_header__protocol_data::Proto013PtJakartBlockHeaderProtocolData),
        ("jakarta.block_header.raw", proto013_ptjakart::block_header__raw::Proto013PtJakartBlockHeaderRaw),
        ("jakarta.block_header.shell_header", proto013_ptjakart::block_header__shell_header::Proto013PtJakartBlockHeaderShellHeader),
        ("jakarta.block_header.unsigned", proto013_ptjakart::block_header__unsigned::Proto013PtJakartBlockHeaderUnsigned),
        ("jakarta.constants", proto013_ptjakart::constants::Proto013PtJakartConstants),
        ("jakarta.constants.fixed", proto013_ptjakart::constants__fixed::Proto013PtJakartConstantsFixed),
        ("jakarta.constants.parametric", proto013_ptjakart::constants__parametric::Proto013PtJakartConstantsParametric),
        ("jakarta.contract", proto013_ptjakart::contract::Proto013PtJakartContract),
        ("jakarta.cycle", proto013_ptjakart::cycle::Proto013PtJakartCycle),
        ("jakarta.errors", proto013_ptjakart::errors::Proto013PtJakartErrors),
        ("jakarta.fa1.2.token_transfer", proto013_ptjakart::fa1__2__token_transfer::Proto013PtJakartFa12TokenTransfer),
        ("jakarta.fitness", proto013_ptjakart::fitness::Proto013PtJakartFitness),
        ("jakarta.gas", proto013_ptjakart::gas::Proto013PtJakartGas),
        ("jakarta.gas.cost", proto013_ptjakart::gas__cost::Proto013PtJakartGasCost),
        ("jakarta.lazy_storage_diff", proto013_ptjakart::lazy_storage_diff::Proto013PtJakartLazyStorageDiff),
        ("jakarta.level", proto013_ptjakart::level::Proto013PtJakartLevel),
        ("jakarta.nonce", proto013_ptjakart::nonce::Proto013PtJakartNonce),
        ("jakarta.operation", proto013_ptjakart::operation::Proto013PtJakartOperation),
        ("jakarta.operation.contents", proto013_ptjakart::operation__contents::Proto013PtJakartOperationContents),
        ("jakarta.operation.contents_list", proto013_ptjakart::operation__contents_list::Proto013PtJakartOperationContentsList),
        ("jakarta.operation.internal", proto013_ptjakart::operation__internal::Proto013PtJakartOperationInternal),
        ("jakarta.operation.protocol_data", proto013_ptjakart::operation__protocol_data::Proto013PtJakartOperationProtocolData),
        ("jakarta.operation.raw", proto013_ptjakart::operation__raw::Proto013PtJakartOperationRaw),
        ("jakarta.operation.unsigned", proto013_ptjakart::operation__unsigned::Proto013PtJakartOperationUnsigned),
        ("jakarta.parameters", proto013_ptjakart::parameters::Proto013PtJakartParameters),
        ("jakarta.period", proto013_ptjakart::period::Proto013PtJakartPeriod),
        ("jakarta.raw_level", proto013_ptjakart::raw_level::Proto013PtJakartRawLevel),
        ("jakarta.receipt.balance_updates", proto013_ptjakart::receipt__balance_updates::Proto013PtJakartReceiptBalanceUpdates),
        ("jakarta.script", proto013_ptjakart::script::Proto013PtJakartScript),
        ("jakarta.script.expr", proto013_ptjakart::script__expr::Proto013PtJakartScriptExpr),
        ("jakarta.script.lazy_expr", proto013_ptjakart::script__lazy_expr::Proto013PtJakartScriptLazyExpr),
        ("jakarta.script.loc", proto013_ptjakart::script__loc::Proto013PtJakartScriptLoc),
        ("jakarta.script.prim", proto013_ptjakart::script__prim::Proto013PtJakartScriptPrim),
        ("jakarta.seed", proto013_ptjakart::seed::Proto013PtJakartSeed),
        ("jakarta.tez", proto013_ptjakart::tez::Proto013PtJakartTez),
        ("jakarta.timestamp", proto013_ptjakart::timestamp::Proto013PtJakartTimestamp),
        ("jakarta.vote.ballot", proto013_ptjakart::vote__ballot::Proto013PtJakartVoteBallot),
        ("jakarta.vote.ballots", proto013_ptjakart::vote__ballots::Proto013PtJakartVoteBallots),
        ("jakarta.vote.listings", proto013_ptjakart::vote__listings::Proto013PtJakartVoteListings),
        ("jakarta.voting_period", proto013_ptjakart::voting_period::Proto013PtJakartVotingPeriod),
        ("jakarta.voting_period.kind", proto013_ptjakart::voting_period__kind::Proto013PtJakartVotingPeriodKind),
        ("kathmandu.block_header", proto014_ptkathma::block_header::Proto014PtKathmaBlockHeader),
        ("kathmandu.block_header.contents", proto014_ptkathma::block_header__contents::Proto014PtKathmaBlockHeaderContents),
        ("kathmandu.block_header.protocol_data", proto014_ptkathma::block_header__protocol_data::Proto014PtKathmaBlockHeaderProtocolData),
        ("kathmandu.block_header.raw", proto014_ptkathma::block_header__raw::Proto014PtKathmaBlockHeaderRaw),
        ("kathmandu.block_header.shell_header", proto014_ptkathma::block_header__shell_header::Proto014PtKathmaBlockHeaderShellHeader),
        ("kathmandu.block_header.unsigned", proto014_ptkathma::block_header__unsigned::Proto014PtKathmaBlockHeaderUnsigned),
        ("kathmandu.block_info", proto014_ptkathma::block_info::Proto014PtKathmaBlockInfo),
        ("kathmandu.constants", proto014_ptkathma::constants::Proto014PtKathmaConstants),
        ("kathmandu.constants.fixed", proto014_ptkathma::constants__fixed::Proto014PtKathmaConstantsFixed),
        ("kathmandu.constants.parametric", proto014_ptkathma::constants__parametric::Proto014PtKathmaConstantsParametric),
        ("kathmandu.contract", proto014_ptkathma::contract::Proto014PtKathmaContract),
        ("kathmandu.cycle", proto014_ptkathma::cycle::Proto014PtKathmaCycle),
        ("kathmandu.errors", proto014_ptkathma::errors::Proto014PtKathmaErrors),
        ("kathmandu.fa1.2.token_transfer", proto014_ptkathma::fa1__2__token_transfer::Proto014PtKathmaFa12TokenTransfer),
        ("kathmandu.fitness", proto014_ptkathma::fitness::Proto014PtKathmaFitness),
        ("kathmandu.gas", proto014_ptkathma::gas::Proto014PtKathmaGas),
        ("kathmandu.gas.cost", proto014_ptkathma::gas__cost::Proto014PtKathmaGasCost),
        ("kathmandu.lazy_storage_diff", proto014_ptkathma::lazy_storage_diff::Proto014PtKathmaLazyStorageDiff),
        ("kathmandu.level", proto014_ptkathma::level::Proto014PtKathmaLevel),
        ("kathmandu.nonce", proto014_ptkathma::nonce::Proto014PtKathmaNonce),
        ("kathmandu.operation", proto014_ptkathma::operation::Proto014PtKathmaOperation),
        ("kathmandu.operation.contents", proto014_ptkathma::operation__contents::Proto014PtKathmaOperationContents),
        ("kathmandu.operation.contents_list", proto014_ptkathma::operation__contents_list::Proto014PtKathmaOperationContentsList),
        ("kathmandu.operation.internal", proto014_ptkathma::operation__internal::Proto014PtKathmaOperationInternal),
        ("kathmandu.operation.protocol_data", proto014_ptkathma::operation__protocol_data::Proto014PtKathmaOperationProtocolData),
        ("kathmandu.operation.raw", proto014_ptkathma::operation__raw::Proto014PtKathmaOperationRaw),
        ("kathmandu.operation.unsigned", proto014_ptkathma::operation__unsigned::Proto014PtKathmaOperationUnsigned),
        ("kathmandu.parameters", proto014_ptkathma::parameters::Proto014PtKathmaParameters),
        ("kathmandu.period", proto014_ptkathma::period::Proto014PtKathmaPeriod),
        ("kathmandu.raw_level", proto014_ptkathma::raw_level::Proto014PtKathmaRawLevel),
        ("kathmandu.receipt.balance_updates", proto014_ptkathma::receipt__balance_updates::Proto014PtKathmaReceiptBalanceUpdates),
        ("kathmandu.script", proto014_ptkathma::script::Proto014PtKathmaScript),
        ("kathmandu.script.expr", proto014_ptkathma::script__expr::Proto014PtKathmaScriptExpr),
        ("kathmandu.script.lazy_expr", proto014_ptkathma::script__lazy_expr::Proto014PtKathmaScriptLazyExpr),
        ("kathmandu.script.loc", proto014_ptkathma::script__loc::Proto014PtKathmaScriptLoc),
        ("kathmandu.script.prim", proto014_ptkathma::script__prim::Proto014PtKathmaScriptPrim),
        ("kathmandu.seed", proto014_ptkathma::seed::Proto014PtKathmaSeed),
        ("kathmandu.tez", proto014_ptkathma::tez::Proto014PtKathmaTez),
        ("kathmandu.timestamp", proto014_ptkathma::timestamp::Proto014PtKathmaTimestamp),
        ("kathmandu.vote.ballot", proto014_ptkathma::vote__ballot::Proto014PtKathmaVoteBallot),
        ("kathmandu.vote.ballots", proto014_ptkathma::vote__ballots::Proto014PtKathmaVoteBallots),
        ("kathmandu.vote.listings", proto014_ptkathma::vote__listings::Proto014PtKathmaVoteListings),
        ("kathmandu.voting_period", proto014_ptkathma::voting_period::Proto014PtKathmaVotingPeriod),
        ("kathmandu.voting_period.kind", proto014_ptkathma::voting_period__kind::Proto014PtKathmaVotingPeriodKind),
        ("lima.baking_rights", proto015_ptlimapt::baking_rights::Proto015PtLimaPtBakingRights),
        ("lima.block_header", proto015_ptlimapt::block_header::Proto015PtLimaPtBlockHeader),
        ("lima.block_header.contents", proto015_ptlimapt::block_header__contents::Proto015PtLimaPtBlockHeaderContents),
        ("lima.block_header.protocol_data", proto015_ptlimapt::block_header__protocol_data::Proto015PtLimaPtBlockHeaderProtocolData),
        ("lima.block_header.raw", proto015_ptlimapt::block_header__raw::Proto015PtLimaPtBlockHeaderRaw),
        ("lima.block_header.shell_header", proto015_ptlimapt::block_header__shell_header::Proto015PtLimaPtBlockHeaderShellHeader),
        ("lima.block_header.unsigned", proto015_ptlimapt::block_header__unsigned::Proto015PtLimaPtBlockHeaderUnsigned),
        ("lima.block_info", proto015_ptlimapt::block_info::Proto015PtLimaPtBlockInfo),
        ("lima.constants", proto015_ptlimapt::constants::Proto015PtLimaPtConstants),
        ("lima.constants.fixed", proto015_ptlimapt::constants__fixed::Proto015PtLimaPtConstantsFixed),
        ("lima.constants.parametric", proto015_ptlimapt::constants__parametric::Proto015PtLimaPtConstantsParametric),
        ("lima.contract", proto015_ptlimapt::contract::Proto015PtLimaPtContract),
        ("lima.cycle", proto015_ptlimapt::cycle::Proto015PtLimaPtCycle),
        ("lima.errors", proto015_ptlimapt::errors::Proto015PtLimaPtErrors),
        ("lima.fa1.2.token_transfer", proto015_ptlimapt::fa1__2__token_transfer::Proto015PtLimaPtFa12TokenTransfer),
        ("lima.fitness", proto015_ptlimapt::fitness::Proto015PtLimaPtFitness),
        ("lima.gas", proto015_ptlimapt::gas::Proto015PtLimaPtGas),
        ("lima.gas.cost", proto015_ptlimapt::gas__cost::Proto015PtLimaPtGasCost),
        ("lima.lazy_storage_diff", proto015_ptlimapt::lazy_storage_diff::Proto015PtLimaPtLazyStorageDiff),
        ("lima.level", proto015_ptlimapt::level::Proto015PtLimaPtLevel),
        ("lima.nonce", proto015_ptlimapt::nonce::Proto015PtLimaPtNonce),
        ("lima.operation", proto015_ptlimapt::operation::Proto015PtLimaPtOperation),
        ("lima.operation.contents", proto015_ptlimapt::operation__contents::Proto015PtLimaPtOperationContents),
        ("lima.operation.contents_list", proto015_ptlimapt::operation__contents_list::Proto015PtLimaPtOperationContentsList),
        ("lima.operation.internal", proto015_ptlimapt::operation__internal::Proto015PtLimaPtOperationInternal),
        ("lima.operation.protocol_data", proto015_ptlimapt::operation__protocol_data::Proto015PtLimaPtOperationProtocolData),
        ("lima.operation.raw", proto015_ptlimapt::operation__raw::Proto015PtLimaPtOperationRaw),
        ("lima.operation.unsigned", proto015_ptlimapt::operation__unsigned::Proto015PtLimaPtOperationUnsigned),
        ("lima.parameters", proto015_ptlimapt::parameters::Proto015PtLimaPtParameters),
        ("lima.period", proto015_ptlimapt::period::Proto015PtLimaPtPeriod),
        ("lima.raw_level", proto015_ptlimapt::raw_level::Proto015PtLimaPtRawLevel),
        ("lima.receipt.balance_updates", proto015_ptlimapt::receipt__balance_updates::Proto015PtLimaPtReceiptBalanceUpdates),
        ("lima.script", proto015_ptlimapt::script::Proto015PtLimaPtScript),
        ("lima.script.expr", proto015_ptlimapt::script__expr::Proto015PtLimaPtScriptExpr),
        ("lima.script.lazy_expr", proto015_ptlimapt::script__lazy_expr::Proto015PtLimaPtScriptLazyExpr),
        ("lima.script.loc", proto015_ptlimapt::script__loc::Proto015PtLimaPtScriptLoc),
        ("lima.script.prim", proto015_ptlimapt::script__prim::Proto015PtLimaPtScriptPrim),
        ("lima.seed", proto015_ptlimapt::seed::Proto015PtLimaPtSeed),
        ("lima.tez", proto015_ptlimapt::tez::Proto015PtLimaPtTez),
        ("lima.timestamp", proto015_ptlimapt::timestamp::Proto015PtLimaPtTimestamp),
        ("lima.vote.ballot", proto015_ptlimapt::vote__ballot::Proto015PtLimaPtVoteBallot),
        ("lima.vote.ballots", proto015_ptlimapt::vote__ballots::Proto015PtLimaPtVoteBallots),
        ("lima.vote.listings", proto015_ptlimapt::vote__listings::Proto015PtLimaPtVoteListings),
        ("lima.voting_period", proto015_ptlimapt::voting_period::Proto015PtLimaPtVotingPeriod),
        ("lima.voting_period.kind", proto015_ptlimapt::voting_period__kind::Proto015PtLimaPtVotingPeriodKind),
        ("mempool", etc::mempool::Mempool),
        ("mumbai.block_header", proto016_ptmumbai::block_header::Proto016PtMumbaiBlockHeader),
        ("mumbai.block_header.contents", proto016_ptmumbai::block_header__contents::Proto016PtMumbaiBlockHeaderContents),
        ("mumbai.block_header.protocol_data", proto016_ptmumbai::block_header__protocol_data::Proto016PtMumbaiBlockHeaderProtocolData),
        ("mumbai.block_header.raw", proto016_ptmumbai::block_header__raw::Proto016PtMumbaiBlockHeaderRaw),
        ("mumbai.block_header.shell_header", proto016_ptmumbai::block_header__shell_header::Proto016PtMumbaiBlockHeaderShellHeader),
        ("mumbai.block_header.unsigned", proto016_ptmumbai::block_header__unsigned::Proto016PtMumbaiBlockHeaderUnsigned),
        ("mumbai.constants", proto016_ptmumbai::constants::Proto016PtMumbaiConstants),
        ("mumbai.constants.fixed", proto016_ptmumbai::constants__fixed::Proto016PtMumbaiConstantsFixed),
        ("mumbai.constants.parametric", proto016_ptmumbai::constants__parametric::Proto016PtMumbaiConstantsParametric),
        ("mumbai.contract", proto016_ptmumbai::contract::Proto016PtMumbaiContract),
        ("mumbai.cycle", proto016_ptmumbai::cycle::Proto016PtMumbaiCycle),
        ("mumbai.errors", proto016_ptmumbai::errors::Proto016PtMumbaiErrors),
        ("mumbai.fa1.2.token_transfer", proto016_ptmumbai::fa1__2__token_transfer::Proto016PtMumbaiFa12TokenTransfer),
        ("mumbai.fitness", proto016_ptmumbai::fitness::Proto016PtMumbaiFitness),
        ("mumbai.gas", proto016_ptmumbai::gas::Proto016PtMumbaiGas),
        ("mumbai.gas.cost", proto016_ptmumbai::gas__cost::Proto016PtMumbaiGasCost),
        ("mumbai.lazy_storage_diff", proto016_ptmumbai::lazy_storage_diff::Proto016PtMumbaiLazyStorageDiff),
        ("mumbai.level", proto016_ptmumbai::level::Proto016PtMumbaiLevel),
        ("mumbai.nonce", proto016_ptmumbai::nonce::Proto016PtMumbaiNonce),
        ("mumbai.operation", proto016_ptmumbai::operation::Proto016PtMumbaiOperation),
        ("mumbai.operation.contents", proto016_ptmumbai::operation__contents::Proto016PtMumbaiOperationContents),
        ("mumbai.operation.contents_list", proto016_ptmumbai::operation__contents_list::Proto016PtMumbaiOperationContentsList),
        ("mumbai.operation.internal", proto016_ptmumbai::operation__internal::Proto016PtMumbaiOperationInternal),
        ("mumbai.operation.protocol_data", proto016_ptmumbai::operation__protocol_data::Proto016PtMumbaiOperationProtocolData),
        ("mumbai.operation.raw", proto016_ptmumbai::operation__raw::Proto016PtMumbaiOperationRaw),
        ("mumbai.operation.unsigned", proto016_ptmumbai::operation__unsigned::Proto016PtMumbaiOperationUnsigned),
        ("mumbai.parameters", proto016_ptmumbai::parameters::Proto016PtMumbaiParameters),
        ("mumbai.period", proto016_ptmumbai::period::Proto016PtMumbaiPeriod),
        ("mumbai.raw_level", proto016_ptmumbai::raw_level::Proto016PtMumbaiRawLevel),
        ("mumbai.receipt.balance_updates", proto016_ptmumbai::receipt__balance_updates::Proto016PtMumbaiReceiptBalanceUpdates),
        ("mumbai.script", proto016_ptmumbai::script::Proto016PtMumbaiScript),
        ("mumbai.script.expr", proto016_ptmumbai::script__expr::Proto016PtMumbaiScriptExpr),
        ("mumbai.script.lazy_expr", proto016_ptmumbai::script__lazy_expr::Proto016PtMumbaiScriptLazyExpr),
        ("mumbai.script.loc", proto016_ptmumbai::script__loc::Proto016PtMumbaiScriptLoc),
        ("mumbai.script.prim", proto016_ptmumbai::script__prim::Proto016PtMumbaiScriptPrim),
        ("mumbai.seed", proto016_ptmumbai::seed::Proto016PtMumbaiSeed),
        ("mumbai.tez", proto016_ptmumbai::tez::Proto016PtMumbaiTez),
        ("mumbai.timestamp", proto016_ptmumbai::timestamp::Proto016PtMumbaiTimestamp),
        ("mumbai.vote.ballot", proto016_ptmumbai::vote__ballot::Proto016PtMumbaiVoteBallot),
        ("mumbai.vote.ballots", proto016_ptmumbai::vote__ballots::Proto016PtMumbaiVoteBallots),
        ("mumbai.vote.listings", proto016_ptmumbai::vote__listings::Proto016PtMumbaiVoteListings),
        ("mumbai.voting_period", proto016_ptmumbai::voting_period::Proto016PtMumbaiVotingPeriod),
        ("mumbai.voting_period.kind", proto016_ptmumbai::voting_period__kind::Proto016PtMumbaiVotingPeriodKind),
        ("network_version", etc::network_version::NetworkVersion),
        ("operation", etc::operation::Operation),
        ("operation.shell_header", etc::operation__shell_header::OperationShellHeader),
        ("p2p_address", etc::p2p_address::P2pAddress),
        ("p2p_connection.id", etc::p2p_connection__id::P2pConnectionId),
        ("p2p_connection.pool_event", etc::p2p_connection__pool_event::P2pConnectionPoolEvent),
        ("p2p_identity", etc::p2p_identity::P2pIdentity),
        ("p2p_peer.pool_event", etc::p2p_peer__pool_event::P2pPeerPoolEvent),
        ("p2p_peer.state", etc::p2p_peer__state::P2pPeerState),
        ("p2p_point.id", etc::p2p_point__id::P2pPointId),
        ("p2p_point.info", etc::p2p_point__info::P2pPointInfo),
        ("p2p_point.pool_event", etc::p2p_point__pool_event::P2pPointPoolEvent),
        ("p2p_point.state", etc::p2p_point__state::P2pPointState),
        ("p2p_stat", etc::p2p_stat::P2pStat),
        ("p2p_version", etc::p2p_version::P2pVersion),
        ("protocol", etc::protocol::Protocol),
        ("protocol.meta", etc::protocol__meta::ProtocolMeta),
        ("sapling.transaction", sapling::transaction::SaplingTransaction),
        ("sapling.transaction.binding_sig", sapling::transaction__binding_sig::SaplingTransactionBindingSig),
        ("sapling.transaction.ciphertext", sapling::transaction__ciphertext::SaplingTransactionCiphertext),
        ("sapling.transaction.commitment", sapling::transaction__commitment::SaplingTransactionCommitment),
        ("sapling.transaction.commitment_hash", sapling::transaction__commitment_hash::SaplingTransactionCommitmentHash),
        ("sapling.transaction.commitment_value", sapling::transaction__commitment_value::SaplingTransactionCommitmentValue),
        ("sapling.transaction.diversifier_index", sapling::transaction__diversifier_index::SaplingTransactionDiversifierIndex),
        ("sapling.transaction.input", sapling::transaction__input::SaplingTransactionInput),
        ("sapling.transaction.nullifier", sapling::transaction__nullifier::SaplingTransactionNullifier),
        ("sapling.transaction.output", sapling::transaction__output::SaplingTransactionOutput),
        ("sapling.transaction.plaintext", sapling::transaction__plaintext::SaplingTransactionPlaintext),
        ("sapling.transaction.rcm", sapling::transaction__rcm::SaplingTransactionRcm),
        ("sapling.wallet.address", sapling::wallet__address::SaplingWalletAddress),
        ("sapling.wallet.spending_key", sapling::wallet__spending_key::SaplingWalletSpendingKey),
        ("sapling.wallet.viewing_key", sapling::wallet__viewing_key::SaplingWalletViewingKey),
        ("signer_messages.deterministic_nonce.response", etc::signer_messages__deterministic_nonce__response::SignerMessagesDeterministicNonceResponse),
        ("signer_messages.deterministic_nonce_hash.response", etc::signer_messages__deterministic_nonce_hash__response::SignerMessagesDeterministicNonceHashResponse),
        ("signer_messages.public_key.response", etc::signer_messages__public_key__response::SignerMessagesPublicKeyResponse),
        ("signer_messages.request", etc::signer_messages__request::SignerMessagesRequest),
        ("signer_messages.sign.response", etc::signer_messages__sign__response::SignerMessagesSignResponse),
        ("signer_messages.supports_deterministic_nonces.response", etc::signer_messages__supports_deterministic_nonces__response::SignerMessagesSupportsDeterministicNoncesResponse),
        ("test_chain_status", etc::test_chain_status::TestChainStatus),
        ("timespan.system", etc::timespan__system::TimespanSystem),
        ("timestamp.protocol", etc::timestamp__protocol::TimestampProtocol),
        ("timestamp.system", etc::timestamp__system::TimestampSystem),
        ("user_activated.protocol_overrides", etc::user_activated__protocol_overrides::UserActivatedProtocolOverrides),
        ("user_activated.upgrades", etc::user_activated__upgrades::UserActivatedUpgrades),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use ::rust_runtime::hexstring::HexString;

    pub fn roundtrip(ident: &'static str, preimage: &'static str) {
        let run: CodecRun<'static, HexString> = run_codec(ident, preimage).unwrap();
        let (pre, post) = run.into_images();
        assert_eq!(pre, post);
    }

    #[test]
    pub fn regressions() {
        // cat ~/tezos/dev/tezos/tezt/_regressions/encoding/* | grep "decode" | sed -E "s/^.\/tezos-codec decode (.*) from ([0-9a-fA-F]+)$/roundtrip(\"\1\", \"\2\");/"`
        roundtrip(
            "alpha.block_header",
            "00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00242e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000ff043691f53c02ca1ac6f1a0c1586bf77973e04c2d9b618a8309e79651daf0d5580066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.block_header.raw",
            "00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00242e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c521d101895ca00000000ff043691f53c02ca1ac6f1a0c1586bf77973e04c2d9b618a8309e79651daf0d55866804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.block_header.unsigned",
            "00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00242e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000ff043691f53c02ca1ac6f1a0c1586bf77973e04c2d9b618a8309e79651daf0d55800"
        );
        roundtrip("alpha.contract.big_map_diff", "000000100300020000000203620200000002036c");
        roundtrip("alpha.contract.big_map_diff", "00000003020001");
        roundtrip("alpha.contract.big_map_diff", "000000020100");
        roundtrip(
            "alpha.contract.big_map_diff",
            "000000310000cffedbaf00cb581448a5683abdefe0d5cd4d4ba4923f1a489791810c3fec332502000000020001ff0200000002034f"
        );
        roundtrip("alpha.contract", "000002298c03ed7d454a101eb7022bc95f7e5f41ac78");
        roundtrip("alpha.contract", "0138560805b4c8d7b7fbbafad5c59dbfa3878ca70500");
        roundtrip("alpha.cycle", "7fffffff");
        roundtrip("alpha.cycle", "00000000");
        roundtrip("alpha.fitness", "0000000101000000010000000100000002");
        roundtrip("alpha.gas.cost", "a6fd9d9694c3d81f");
        roundtrip("alpha.gas.cost", "00");
        roundtrip("alpha.gas", "009613");
        roundtrip("alpha.gas", "01");
        roundtrip("alpha.gas", "0000");
        roundtrip("alpha.level", "000005337fffffff0000012c00000258ff");
        roundtrip(
            "alpha.nonce",
            "0000000000000000000000000000000000000000000000000000000000000000"
        );
        roundtrip(
            "alpha.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac7800000300"
        );
        roundtrip(
            "alpha.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac78000003ff0002298c03ed7d454a101eb7022bc95f7e5f41ac78"
        );
        roundtrip(
            "alpha.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac78000002af9105ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7800000020020000001b050003680501056303680502020000000a03160346053d036d03420000000e0200000009010000000474657374"
        );
        roundtrip(
            "alpha.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac78000000004798d2cc98473d7e250c898885718afd2e4efbcb1a1595ab9730761ed830de0f"
        );
        roundtrip(
            "alpha.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac780003019703000002298c03ed7d454a101eb7022bc95f7e5f41ac7800"
        );
        roundtrip(
            "alpha.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac7800000197030138560805b4c8d7b7fbbafad5c59dbfa3878ca70500ffff06616374696f6e000000070200000002034f"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a804c55cf02dbeecc978d9c84625dcae72bb77ea4fbd41f98b15efc63fa893d61d7d6eee4a2ce9427ac466804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8060002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c0066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d4010066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7866804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a803000000f100000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c000000f100000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8020000008b0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c0000008b0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86d0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401af9105ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7800000020020000001b050003680501056303680502020000000a03160346053d036d03420000000e020000000901000000047465737466804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8050002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf000000407663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86b0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401004798d2cc98473d7e250c898885718afd2e4efbcb1a1595ab9730761ed830de0f66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a80100000533000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d4019703000002298c03ed7d454a101eb7022bc95f7e5f41ac780066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d40197030138560805b4c8d7b7fbbafad5c59dbfa3878ca70500ffff06616374696f6e000000070200000002034f66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation.raw",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000053366804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a804c55cf02dbeecc978d9c84625dcae72bb77ea4fbd41f98b15efc63fa893d61d7d6eee4a2ce9427ac4"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8060002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c00"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d40100"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401ff0002298c03ed7d454a101eb7022bc95f7e5f41ac78"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a803000000f100000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c000000f100000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8020000008b0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c0000008b0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a815000000000533000000000000000000000000000000000000000000000000000000000000000000000000"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86d0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401af9105ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7800000020020000001b050003680501056303680502020000000a03160346053d036d03420000000e0200000009010000000474657374"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8050002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf000000407663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86b0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401004798d2cc98473d7e250c898885718afd2e4efbcb1a1595ab9730761ed830de0f"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a801000005330000000000000000000000000000000000000000000000000000000000000000"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d4019703000002298c03ed7d454a101eb7022bc95f7e5f41ac7800"
        );
        roundtrip(
            "alpha.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d40197030138560805b4c8d7b7fbbafad5c59dbfa3878ca70500ffff06616374696f6e000000070200000002034f"
        );
        roundtrip("alpha.period", "0000000000000ae5");
        roundtrip("alpha.period", "0000000000000000");
        roundtrip("alpha.raw_level", "7fffffff");
        roundtrip("alpha.raw_level", "00000000");
        roundtrip("alpha.seed", "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8");
        roundtrip("alpha.tez", "97f4be03");
        roundtrip("alpha.tez", "00");
        roundtrip("alpha.timestamp", "000000005e9dcbaf");
        roundtrip("alpha.vote.ballot", "01");
        roundtrip("alpha.vote.ballot", "02");
        roundtrip("alpha.vote.ballot", "00");
        roundtrip("alpha.vote.ballots", "7fffffff00000000000001c7");
        roundtrip(
            "alpha.vote.listings",
            "000000320002298c03ed7d454a101eb7022bc95f7e5f41ac7800004e9300e7670f32038107a59a2b9cfefae36ea21f5aa63c00002328"
        );
        roundtrip("alpha.voting_period.kind", "03");
        roundtrip("alpha.voting_period.kind", "00");
        roundtrip("alpha.voting_period.kind", "01");
        roundtrip("alpha.voting_period.kind", "02");
        roundtrip("alpha.voting_period", "00000000000011af72");
        roundtrip("alpha.voting_period", "000000000000000000");
        roundtrip(
            "hangzhou.block_header",
            "00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00242e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c521d101895ca00000000ff043691f53c02ca1ac6f1a0c1586bf77973e04c2d9b618a8309e79651daf0d5580066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.block_header.raw",
            "00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00242e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c521d101895ca00000000ff043691f53c02ca1ac6f1a0c1586bf77973e04c2d9b618a8309e79651daf0d55866804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.block_header.unsigned",
            "00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00242e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c521d101895ca00000000ff043691f53c02ca1ac6f1a0c1586bf77973e04c2d9b618a8309e79651daf0d55800"
        );
        roundtrip("hangzhou.contract.big_map_diff", "000000100300020000000203620200000002036c");
        roundtrip("hangzhou.contract.big_map_diff", "00000003020001");
        roundtrip("hangzhou.contract.big_map_diff", "000000020100");
        roundtrip(
            "hangzhou.contract.big_map_diff",
            "000000310000cffedbaf00cb581448a5683abdefe0d5cd4d4ba4923f1a489791810c3fec332502000000020001ff0200000002034f"
        );
        roundtrip("hangzhou.contract", "000002298c03ed7d454a101eb7022bc95f7e5f41ac78");
        roundtrip("hangzhou.contract", "0138560805b4c8d7b7fbbafad5c59dbfa3878ca70500");
        roundtrip("hangzhou.cycle", "7fffffff");
        roundtrip("hangzhou.cycle", "00000000");
        roundtrip(
            "hangzhou.delegate.frozen_balance_by_cycles",
            "000000140000000c00800180a01f0000000d80d00f800101"
        );
        roundtrip("hangzhou.delegate.frozen_balance", "0080d0dbc3f40281b701");
        roundtrip("hangzhou.fitness", "00000011000000010100000008000000000000000a");
        roundtrip("hangzhou.gas.cost", "a6fd9d9694c3d81f");
        roundtrip("hangzhou.gas.cost", "00");
        roundtrip("hangzhou.gas", "009613");
        roundtrip("hangzhou.gas", "01");
        roundtrip("hangzhou.gas", "0000");
        roundtrip("hangzhou.level", "000005337fffffff0000012c00000258ff");
        roundtrip(
            "hangzhou.nonce",
            "0000000000000000000000000000000000000000000000000000000000000000"
        );
        roundtrip(
            "hangzhou.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac7800000300"
        );
        roundtrip(
            "hangzhou.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac78000003ff0002298c03ed7d454a101eb7022bc95f7e5f41ac78"
        );
        roundtrip(
            "hangzhou.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac78000002af9105ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7800000020020000001b050003680501056303680502020000000a03160346053d036d03420000000e0200000009010000000474657374"
        );
        roundtrip(
            "hangzhou.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac78000000004798d2cc98473d7e250c898885718afd2e4efbcb1a1595ab9730761ed830de0f"
        );
        roundtrip(
            "hangzhou.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac780003019703000002298c03ed7d454a101eb7022bc95f7e5f41ac7800"
        );
        roundtrip(
            "hangzhou.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac7800000197030138560805b4c8d7b7fbbafad5c59dbfa3878ca70500ffff06616374696f6e000000070200000002034f"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a804c55cf02dbeecc978d9c84625dcae72bb77ea4fbd41f98b15efc63fa893d61d7d6eee4a2ce9427ac466804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8060002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c0066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d4010066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7866804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a803000000cf00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c0000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c000000cf00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c0000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a802000000650e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000053366804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c000000650e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000053366804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a80a000000650e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000053366804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000053366804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86d0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401af9105ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7800000020020000001b050003680501056303680502020000000a03160346053d036d03420000000e020000000901000000047465737466804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8050002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf000000407663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86b0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401004798d2cc98473d7e250c898885718afd2e4efbcb1a1595ab9730761ed830de0f66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a80100000533000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d4019703000002298c03ed7d454a101eb7022bc95f7e5f41ac780066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d40197030138560805b4c8d7b7fbbafad5c59dbfa3878ca70500ffff06616374696f6e000000070200000002034f66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation.raw",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000053366804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a804c55cf02dbeecc978d9c84625dcae72bb77ea4fbd41f98b15efc63fa893d61d7d6eee4a2ce9427ac4"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8060002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c00"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d40100"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401ff0002298c03ed7d454a101eb7022bc95f7e5f41ac78"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a803000000cf00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c0000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c000000cf00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c0000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a802000000650e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000053366804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c000000650e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000053366804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c0000"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a80a000000650e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000053366804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c0000"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a80000000533"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86d0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401af9105ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7800000020020000001b050003680501056303680502020000000a03160346053d036d03420000000e0200000009010000000474657374"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8050002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf000000407663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86b0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401004798d2cc98473d7e250c898885718afd2e4efbcb1a1595ab9730761ed830de0f"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a801000005330000000000000000000000000000000000000000000000000000000000000000"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d4019703000002298c03ed7d454a101eb7022bc95f7e5f41ac7800"
        );
        roundtrip(
            "hangzhou.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d40197030138560805b4c8d7b7fbbafad5c59dbfa3878ca70500ffff06616374696f6e000000070200000002034f"
        );
        roundtrip("hangzhou.period", "0000000000000ae5");
        roundtrip("hangzhou.period", "0000000000000000");
        roundtrip("hangzhou.raw_level", "7fffffff");
        roundtrip("hangzhou.raw_level", "00000000");
        roundtrip("hangzhou.roll", "7fffffff");
        roundtrip("hangzhou.roll", "00000000");
        roundtrip(
            "hangzhou.seed",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8"
        );
        roundtrip("hangzhou.tez", "97f4be03");
        roundtrip("hangzhou.tez", "00");
        roundtrip("hangzhou.timestamp", "000000005e9dcbaf");
        roundtrip("hangzhou.vote.ballot", "01");
        roundtrip("hangzhou.vote.ballot", "02");
        roundtrip("hangzhou.vote.ballot", "00");
        roundtrip("hangzhou.vote.ballots", "7fffffff00000000000001c7");
        roundtrip(
            "hangzhou.vote.listings",
            "000000320002298c03ed7d454a101eb7022bc95f7e5f41ac7800004e9300e7670f32038107a59a2b9cfefae36ea21f5aa63c00002328"
        );
        roundtrip("hangzhou.voting_period.kind", "03");
        roundtrip("hangzhou.voting_period.kind", "00");
        roundtrip("hangzhou.voting_period.kind", "01");
        roundtrip("hangzhou.voting_period.kind", "02");
        roundtrip("hangzhou.voting_period", "00000000000011af72");
        roundtrip("hangzhou.voting_period", "000000000000000000");
        roundtrip(
            "ithaca.block_header",
            "00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00242e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000ff043691f53c02ca1ac6f1a0c1586bf77973e04c2d9b618a8309e79651daf0d5580066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.block_header.raw",
            "00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00242e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c521d101895ca00000000ff043691f53c02ca1ac6f1a0c1586bf77973e04c2d9b618a8309e79651daf0d55866804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.block_header.unsigned",
            "00000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00242e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000ff043691f53c02ca1ac6f1a0c1586bf77973e04c2d9b618a8309e79651daf0d55800"
        );
        roundtrip("ithaca.contract.big_map_diff", "000000100300020000000203620200000002036c");
        roundtrip("ithaca.contract.big_map_diff", "00000003020001");
        roundtrip("ithaca.contract.big_map_diff", "000000020100");
        roundtrip(
            "ithaca.contract.big_map_diff",
            "000000310000cffedbaf00cb581448a5683abdefe0d5cd4d4ba4923f1a489791810c3fec332502000000020001ff0200000002034f"
        );
        roundtrip("ithaca.contract", "000002298c03ed7d454a101eb7022bc95f7e5f41ac78");
        roundtrip("ithaca.contract", "0138560805b4c8d7b7fbbafad5c59dbfa3878ca70500");
        roundtrip("ithaca.cycle", "7fffffff");
        roundtrip("ithaca.cycle", "00000000");
        roundtrip("ithaca.fitness", "0000000101000000010000000100000002");
        roundtrip("ithaca.gas.cost", "a6fd9d9694c3d81f");
        roundtrip("ithaca.gas.cost", "00");
        roundtrip("ithaca.gas", "009613");
        roundtrip("ithaca.gas", "01");
        roundtrip("ithaca.gas", "0000");
        roundtrip("ithaca.level", "000005337fffffff0000012c00000258ff");
        roundtrip(
            "ithaca.nonce",
            "0000000000000000000000000000000000000000000000000000000000000000"
        );
        roundtrip(
            "ithaca.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac7800000300"
        );
        roundtrip(
            "ithaca.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac78000003ff0002298c03ed7d454a101eb7022bc95f7e5f41ac78"
        );
        roundtrip(
            "ithaca.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac78000002af9105ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7800000020020000001b050003680501056303680502020000000a03160346053d036d03420000000e0200000009010000000474657374"
        );
        roundtrip(
            "ithaca.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac78000000004798d2cc98473d7e250c898885718afd2e4efbcb1a1595ab9730761ed830de0f"
        );
        roundtrip(
            "ithaca.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac780003019703000002298c03ed7d454a101eb7022bc95f7e5f41ac7800"
        );
        roundtrip(
            "ithaca.operation.internal",
            "000002298c03ed7d454a101eb7022bc95f7e5f41ac7800000197030138560805b4c8d7b7fbbafad5c59dbfa3878ca70500ffff06616374696f6e000000070200000002034f"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a804c55cf02dbeecc978d9c84625dcae72bb77ea4fbd41f98b15efc63fa893d61d7d6eee4a2ce9427ac466804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8060002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c0066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d4010066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7866804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a803000000f100000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c000000f100000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8020000008b0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c0000008b0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86d0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401af9105ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7800000020020000001b050003680501056303680502020000000a03160346053d036d03420000000e020000000901000000047465737466804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8050002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf000000407663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86b0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401004798d2cc98473d7e250c898885718afd2e4efbcb1a1595ab9730761ed830de0f66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a80100000533000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d4019703000002298c03ed7d454a101eb7022bc95f7e5f41ac780066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d40197030138560805b4c8d7b7fbbafad5c59dbfa3878ca70500ffff06616374696f6e000000070200000002034f66804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation.raw",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000053366804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a804c55cf02dbeecc978d9c84625dcae72bb77ea4fbd41f98b15efc63fa893d61d7d6eee4a2ce9427ac4"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8060002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c00"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d40100"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86e0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401ff0002298c03ed7d454a101eb7022bc95f7e5f41ac78"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a803000000f100000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c000000f100000533010e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8000000005e9dcbb00442e9bc4583d4f9fa6ba422733f45d3a44397141a953d2237bf8df62e5046eef700000011000000010100000008000000000000000a4c7319284b55068bb7c4e0b9f8585729db7fb27ab4ca9cff2038a1fc324f650c000000000000000000000000000000000000000000000000000000000000000000000000101895ca00000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8020000008b0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c0000008b0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a81500000000053300000000000000000000000000000000000000000000000000000000000000000000000066804fe735e06e97e26da8236b6341b91c625d5e82b3524ec0a88cc982365e70f8a5b9bc65df2ea6d21ee244cc3a96fb33031c394c78b1179ff1b8a44237740c"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a815000000000533000000000000000000000000000000000000000000000000000000000000000000000000"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86d0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401af9105ff0002298c03ed7d454a101eb7022bc95f7e5f41ac7800000020020000001b050003680501056303680502020000000a03160346053d036d03420000000e0200000009010000000474657374"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8050002298c03ed7d454a101eb7022bc95f7e5f41ac78000002cf000000407663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c7663cf120f3dc8189d5dc7d4d7a0483bcc53f3f18e700f5a2f5076aa8b9dc55c"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86b0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d401004798d2cc98473d7e250c898885718afd2e4efbcb1a1595ab9730761ed830de0f"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a801000005330000000000000000000000000000000000000000000000000000000000000000"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d4019703000002298c03ed7d454a101eb7022bc95f7e5f41ac7800"
        );
        roundtrip(
            "ithaca.operation.unsigned",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a86c0002298c03ed7d454a101eb7022bc95f7e5f41ac7821dc05edecc004adcacdb7d40197030138560805b4c8d7b7fbbafad5c59dbfa3878ca70500ffff06616374696f6e000000070200000002034f"
        );
        roundtrip("ithaca.period", "0000000000000ae5");
        roundtrip("ithaca.period", "0000000000000000");
        roundtrip("ithaca.raw_level", "7fffffff");
        roundtrip("ithaca.raw_level", "00000000");
        roundtrip(
            "ithaca.seed",
            "0e5751c026e543b2e8ab2eb06099daa1d1e5df47778f7787faab45cdf12fe3a8"
        );
        roundtrip("ithaca.tez", "97f4be03");
        roundtrip("ithaca.tez", "00");
        roundtrip("ithaca.timestamp", "000000005e9dcbaf");
        roundtrip("ithaca.vote.ballot", "01");
        roundtrip("ithaca.vote.ballot", "02");
        roundtrip("ithaca.vote.ballot", "00");
        roundtrip("ithaca.vote.ballots", "7fffffff00000000000001c7");
        roundtrip(
            "ithaca.vote.listings",
            "000000320002298c03ed7d454a101eb7022bc95f7e5f41ac7800004e9300e7670f32038107a59a2b9cfefae36ea21f5aa63c00002328"
        );
        roundtrip("ithaca.voting_period.kind", "03");
        roundtrip("ithaca.voting_period.kind", "00");
        roundtrip("ithaca.voting_period.kind", "01");
        roundtrip("ithaca.voting_period.kind", "02");
        roundtrip("ithaca.voting_period", "00000000000011af72");
        roundtrip("ithaca.voting_period", "000000000000000000");
    }
}