#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type Proto009PsFLorenGasCost = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn proto009psflorengascost_write<U: Target>(val: &Proto009PsFLorenGasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorengascost_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenGasCost> {
    Ok(::rust_runtime::Z::parse(p)?)
}
