#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,i31,resolve_zero};
pub type Proto009PsFLorenScriptLoc = ::rust_runtime::i31;
#[allow(dead_code)]
pub fn proto009psflorenscriptloc_write<U: Target>(val: &Proto009PsFLorenScriptLoc, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenscriptloc_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenScriptLoc> {
    Ok(::rust_runtime::i31::parse(p)?)
}
