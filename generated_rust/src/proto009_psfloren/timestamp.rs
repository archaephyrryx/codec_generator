#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto009PsFLorenTimestamp = i64;
#[allow(dead_code)]
pub fn proto009psflorentimestamp_write<U: Target>(val: &Proto009PsFLorenTimestamp, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorentimestamp_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenTimestamp> {
    Ok(i64::parse(p)?)
}
