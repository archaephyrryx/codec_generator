#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto009PsFLorenGas,u8,proto009psflorengas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto009psflorengas_write<U: Target>(val: &Proto009PsFLorenGas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorengas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenGas> {
    Proto009PsFLorenGas::parse(p)
}
