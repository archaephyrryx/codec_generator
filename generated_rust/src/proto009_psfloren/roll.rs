#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto009PsFLorenRoll = i32;
#[allow(dead_code)]
pub fn proto009psflorenroll_write<U: Target>(val: &Proto009PsFLorenRoll, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenroll_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenRoll> {
    Ok(i32::parse(p)?)
}
