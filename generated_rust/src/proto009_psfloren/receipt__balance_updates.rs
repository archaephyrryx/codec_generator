#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(Proto009PsFLorenContractId,u8,proto009psflorencontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto009PsFLorenContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto009PsFLorenOperationMetadataAlphaBalance,u8,proto009psflorenoperationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto009PsFLorenContractId, pub change: i64, pub origin: Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin },
        1 => Rewards  { pub delegate: Proto009PsFLorenOperationMetadataAlphaBalanceRewardsDelegate, pub cycle: i32, pub change: i64, pub origin: Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin },
        2 => Fees  { pub delegate: Proto009PsFLorenOperationMetadataAlphaBalanceFeesDelegate, pub cycle: i32, pub change: i64, pub origin: Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin },
        3 => Deposits  { pub delegate: Proto009PsFLorenOperationMetadataAlphaBalanceDepositsDelegate, pub cycle: i32, pub change: i64, pub origin: Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationMetadataAlphaBalanceDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationMetadataAlphaBalanceFeesDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationMetadataAlphaBalanceRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin,u8,proto009psflorenoperationmetadataalphaupdateoriginorigin,{
        0 => Block_application ,
        1 => Protocol_migration ,
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto009PsFLorenReceiptBalanceUpdates = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto009PsFLorenOperationMetadataAlphaBalance>>;
#[allow(dead_code)]
pub fn proto009psflorenreceiptbalanceupdates_write<U: Target>(val: &Proto009PsFLorenReceiptBalanceUpdates, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto009PsFLorenOperationMetadataAlphaBalance>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenreceiptbalanceupdates_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenReceiptBalanceUpdates> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto009PsFLorenOperationMetadataAlphaBalance>>::parse(p)?)
}
