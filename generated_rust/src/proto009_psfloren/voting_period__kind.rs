#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto009PsFLorenVotingPeriodKind,u8,proto009psflorenvotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[allow(dead_code)]
pub fn proto009psflorenvotingperiodkind_write<U: Target>(val: &Proto009PsFLorenVotingPeriodKind, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenvotingperiodkind_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenVotingPeriodKind> {
    Proto009PsFLorenVotingPeriodKind::parse(p)
}
