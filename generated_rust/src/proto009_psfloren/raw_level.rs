#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto009PsFLorenRawLevel = i32;
#[allow(dead_code)]
pub fn proto009psflorenrawlevel_write<U: Target>(val: &Proto009PsFLorenRawLevel, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenrawlevel_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenRawLevel> {
    Ok(i32::parse(p)?)
}
