#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,Padded,ParseResult,Parser,Target,data,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
data!(Proto009PsFLorenContract,u8,proto009psflorencontract,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto009PsFLorenContractIdOriginatedDenestPad,1>),
    }
    );
#[allow(dead_code)]
pub fn proto009psflorencontract_write<U: Target>(val: &Proto009PsFLorenContract, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorencontract_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenContract> {
    Proto009PsFLorenContract::parse(p)
}
