#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto009PsFLorenSeed = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn proto009psflorenseed_write<U: Target>(val: &Proto009PsFLorenSeed, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenseed_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenSeed> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
