#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenVoteBallots { pub yay: i32, pub nay: i32, pub pass: i32 }
#[allow(dead_code)]
pub fn proto009psflorenvoteballots_write<U: Target>(val: &Proto009PsFLorenVoteBallots, buf: &mut U) -> usize {
    i32::write_to(&val.yay, buf) + i32::write_to(&val.nay, buf) + i32::write_to(&val.pass, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenvoteballots_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenVoteBallots> {
    Ok(Proto009PsFLorenVoteBallots {yay: i32::parse(p)?, nay: i32::parse(p)?, pass: i32::parse(p)?})
}
