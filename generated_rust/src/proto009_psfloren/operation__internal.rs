#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Padded,ParseResult,Parser,Target,data,resolve_zero,u30};
data!(Proto009PsFLorenContractId,u8,proto009psflorencontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto009PsFLorenContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto009PsFLorenEntrypoint,u8,proto009psflorenentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaInternalOperationDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaInternalOperationOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaInternalOperationRevealPublicKey { pub signature_v0_public_key: PublicKey }
data!(Proto009PsFLorenOperationAlphaInternalOperationRhs,u8,proto009psflorenoperationalphainternaloperationrhs,{
        0 => Reveal  { pub public_key: Proto009PsFLorenOperationAlphaInternalOperationRevealPublicKey },
        1 => Transaction  { pub amount: ::rust_runtime::N, pub destination: Proto009PsFLorenContractId, pub parameters: std::option::Option<Proto009PsFLorenOperationAlphaInternalOperationTransactionParameters> },
        2 => Origination  { pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto009PsFLorenOperationAlphaInternalOperationOriginationDelegate>, pub script: Proto009PsFLorenScriptedContracts },
        3 => Delegation  { pub delegate: std::option::Option<Proto009PsFLorenOperationAlphaInternalOperationDelegationDelegate> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaInternalOperationTransactionParameters { pub entrypoint: Proto009PsFLorenEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationInternal { pub source: Proto009PsFLorenContractId, pub nonce: u16, pub proto009_psfloren_operation_alpha_internal_operation_rhs: Proto009PsFLorenOperationAlphaInternalOperationRhs }
#[allow(dead_code)]
pub fn proto009psflorenoperationinternal_write<U: Target>(val: &Proto009PsFLorenOperationInternal, buf: &mut U) -> usize {
    Proto009PsFLorenContractId::write_to(&val.source, buf) + u16::write_to(&val.nonce, buf) + Proto009PsFLorenOperationAlphaInternalOperationRhs::write_to(&val.proto009_psfloren_operation_alpha_internal_operation_rhs, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenoperationinternal_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenOperationInternal> {
    Ok(Proto009PsFLorenOperationInternal {source: Proto009PsFLorenContractId::parse(p)?, nonce: u16::parse(p)?, proto009_psfloren_operation_alpha_internal_operation_rhs: Proto009PsFLorenOperationAlphaInternalOperationRhs::parse(p)?})
}
