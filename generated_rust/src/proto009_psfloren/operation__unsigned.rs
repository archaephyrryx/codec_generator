#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Nullable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenBlockHeaderAlphaFullHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub signature: Proto009PsFLorenBlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenBlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
data!(Proto009PsFLorenContractId,u8,proto009psflorencontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto009PsFLorenContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto009PsFLorenEntrypoint,u8,proto009psflorenentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenInlinedEndorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto009PsFLorenInlinedEndorsementContents, pub signature: ::rust_runtime::Nullable<Proto009PsFLorenInlinedEndorsementSignature> }
data!(Proto009PsFLorenInlinedEndorsementContents,u8,proto009psfloreninlinedendorsementcontents,{
        0 => Endorsement  { pub level: i32 },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenInlinedEndorsementSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
data!(Proto009PsFLorenOperationAlphaContents,u8,proto009psflorenoperationalphacontents,{
        0 => Endorsement  { pub level: i32 },
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32> },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto009PsFLorenInlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto009PsFLorenInlinedEndorsement>, pub slot: u16 },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto009PsFLorenBlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto009PsFLorenBlockHeaderAlphaFullHeader> },
        4 => Activate_account  { pub pkh: Proto009PsFLorenOperationAlphaContentsActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20> },
        5 => Proposals  { pub source: Proto009PsFLorenOperationAlphaContentsProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto009PsFLorenOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>> },
        6 => Ballot  { pub source: Proto009PsFLorenOperationAlphaContentsBallotSource, pub period: i32, pub proposal: Proto009PsFLorenOperationAlphaContentsBallotProposal, pub ballot: i8 },
        10 => Endorsement_with_slot  { pub endorsement: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto009PsFLorenInlinedEndorsement>, pub slot: u16 },
        17 => Failing_noop  { pub arbitrary: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        107 => Reveal  { pub source: Proto009PsFLorenOperationAlphaContentsRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: Proto009PsFLorenOperationAlphaContentsRevealPublicKey },
        108 => Transaction  { pub source: Proto009PsFLorenOperationAlphaContentsTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: Proto009PsFLorenContractId, pub parameters: std::option::Option<Proto009PsFLorenOperationAlphaContentsTransactionParameters> },
        109 => Origination  { pub source: Proto009PsFLorenOperationAlphaContentsOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto009PsFLorenOperationAlphaContentsOriginationDelegate>, pub script: Proto009PsFLorenScriptedContracts },
        110 => Delegation  { pub source: Proto009PsFLorenOperationAlphaContentsDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<Proto009PsFLorenOperationAlphaContentsDelegationDelegate> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsBallotSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsDelegationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsProposalsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsRevealPublicKey { pub signature_v0_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsRevealSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsTransactionParameters { pub entrypoint: Proto009PsFLorenEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationAlphaContentsTransactionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenOperationUnsigned { pub branch: OperationShellHeaderBranch, pub contents: ::rust_runtime::Sequence<Proto009PsFLorenOperationAlphaContents> }
#[allow(dead_code)]
pub fn proto009psflorenoperationunsigned_write<U: Target>(val: &Proto009PsFLorenOperationUnsigned, buf: &mut U) -> usize {
    OperationShellHeaderBranch::write_to(&val.branch, buf) + ::rust_runtime::Sequence::<Proto009PsFLorenOperationAlphaContents>::write_to(&val.contents, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenoperationunsigned_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenOperationUnsigned> {
    Ok(Proto009PsFLorenOperationUnsigned {branch: OperationShellHeaderBranch::parse(p)?, contents: ::rust_runtime::Sequence::<Proto009PsFLorenOperationAlphaContents>::parse(p)?})
}
