#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenBlockHeaderContents { pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash> }
#[allow(dead_code)]
pub fn proto009psflorenblockheadercontents_write<U: Target>(val: &Proto009PsFLorenBlockHeaderContents, buf: &mut U) -> usize {
    u16::write_to(&val.priority, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenblockheadercontents_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenBlockHeaderContents> {
    Ok(Proto009PsFLorenBlockHeaderContents {priority: u16::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?})
}
