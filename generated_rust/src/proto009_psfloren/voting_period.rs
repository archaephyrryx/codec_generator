#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto009PsFLorenVotingPeriodKind,u8,proto009psflorenvotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenVotingPeriod { pub index: i32, pub kind: Proto009PsFLorenVotingPeriodKind, pub start_position: i32 }
#[allow(dead_code)]
pub fn proto009psflorenvotingperiod_write<U: Target>(val: &Proto009PsFLorenVotingPeriod, buf: &mut U) -> usize {
    i32::write_to(&val.index, buf) + Proto009PsFLorenVotingPeriodKind::write_to(&val.kind, buf) + i32::write_to(&val.start_position, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenvotingperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenVotingPeriod> {
    Ok(Proto009PsFLorenVotingPeriod {index: i32::parse(p)?, kind: Proto009PsFLorenVotingPeriodKind::parse(p)?, start_position: i32::parse(p)?})
}
