#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,N,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenDelegateFrozenBalanceByCyclesDenestDynDenestSeq { pub cycle: i32, pub deposit: ::rust_runtime::N, pub fees: ::rust_runtime::N, pub rewards: ::rust_runtime::N }
pub type Proto009PsFLorenDelegateFrozenBalanceByCycles = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto009PsFLorenDelegateFrozenBalanceByCyclesDenestDynDenestSeq>>;
#[allow(dead_code)]
pub fn proto009psflorendelegatefrozenbalancebycycles_write<U: Target>(val: &Proto009PsFLorenDelegateFrozenBalanceByCycles, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto009PsFLorenDelegateFrozenBalanceByCyclesDenestDynDenestSeq>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorendelegatefrozenbalancebycycles_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenDelegateFrozenBalanceByCycles> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto009PsFLorenDelegateFrozenBalanceByCyclesDenestDynDenestSeq>>::parse(p)?)
}
