#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto009PsFLorenVoteBallot = i8;
#[allow(dead_code)]
pub fn proto009psflorenvoteballot_write<U: Target>(val: &Proto009PsFLorenVoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenvoteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenVoteBallot> {
    Ok(i8::parse(p)?)
}
