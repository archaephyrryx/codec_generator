#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenBlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto009PsFLorenBlockHeaderProtocolData { pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub signature: Proto009PsFLorenBlockHeaderAlphaSignedContentsSignature }
#[allow(dead_code)]
pub fn proto009psflorenblockheaderprotocoldata_write<U: Target>(val: &Proto009PsFLorenBlockHeaderProtocolData, buf: &mut U) -> usize {
    u16::write_to(&val.priority, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + Proto009PsFLorenBlockHeaderAlphaSignedContentsSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto009psflorenblockheaderprotocoldata_parse<P: Parser>(p: &mut P) -> ParseResult<Proto009PsFLorenBlockHeaderProtocolData> {
    Ok(Proto009PsFLorenBlockHeaderProtocolData {priority: u16::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, signature: Proto009PsFLorenBlockHeaderAlphaSignedContentsSignature::parse(p)?})
}
