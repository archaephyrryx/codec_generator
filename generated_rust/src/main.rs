extern crate rust_runtime;

use std::io::prelude::*;

use rustgen;

type T = rust_runtime::MemoBuilder;

fn main() {
    let mut _args = std::env::args();
    _args.next();

    let ident = _args.next().unwrap();

    if let Some(input) = _args.next() {
        match rustgen::run_codec::<T>(&ident, &input) {
            Ok(run) => {
                println!("{}", run);
                run.rt_diff();
            }
            Err(e) => println!("{}", e),
        }
    } else {
        for line in std::io::stdin().lock().lines() {
            if let Ok(input) = line {
                match rustgen::run_codec::<T>(&ident, &input) {
                    Ok(run) => {
                        println!("{}", run);
                        run.rt_diff();
                    }
                    Err(e) => println!("{}", e),
                }
            } else {
                break;
            }
        }
    }
}
