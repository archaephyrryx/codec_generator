#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderAlphaSignedContentsSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderProtocolData { pub payload_hash: AlphaBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: AlphaBlockHeaderAlphaSignedContentsSignature }
#[allow(dead_code)]
pub fn alphablockheaderprotocoldata_write<U: Target>(val: &AlphaBlockHeaderProtocolData, buf: &mut U) -> usize {
    AlphaBlockHeaderAlphaUnsignedContentsPayloadHash::write_to(&val.payload_hash, buf) + i32::write_to(&val.payload_round, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + i8::write_to(&val.liquidity_baking_toggle_vote, buf) + AlphaBlockHeaderAlphaSignedContentsSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphablockheaderprotocoldata_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaBlockHeaderProtocolData> {
    Ok(AlphaBlockHeaderProtocolData {payload_hash: AlphaBlockHeaderAlphaUnsignedContentsPayloadHash::parse(p)?, payload_round: i32::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, liquidity_baking_toggle_vote: i8::parse(p)?, signature: AlphaBlockHeaderAlphaSignedContentsSignature::parse(p)?})
}
