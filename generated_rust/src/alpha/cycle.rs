#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type AlphaCycle = i32;
#[allow(dead_code)]
pub fn alphacycle_write<U: Target>(val: &AlphaCycle, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphacycle_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaCycle> {
    Ok(i32::parse(p)?)
}
