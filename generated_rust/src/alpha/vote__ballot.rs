#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type AlphaVoteBallot = i8;
#[allow(dead_code)]
pub fn alphavoteballot_write<U: Target>(val: &AlphaVoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphavoteballot_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaVoteBallot> {
    Ok(i8::parse(p)?)
}
