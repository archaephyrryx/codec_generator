#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(AlphaVotingPeriodKind,u8,alphavotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaVotingPeriod { pub index: i32, pub kind: AlphaVotingPeriodKind, pub start_position: i32 }
#[allow(dead_code)]
pub fn alphavotingperiod_write<U: Target>(val: &AlphaVotingPeriod, buf: &mut U) -> usize {
    i32::write_to(&val.index, buf) + AlphaVotingPeriodKind::write_to(&val.kind, buf) + i32::write_to(&val.start_position, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphavotingperiod_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaVotingPeriod> {
    Ok(AlphaVotingPeriod {index: i32::parse(p)?, kind: AlphaVotingPeriodKind::parse(p)?, start_position: i32::parse(p)?})
}
