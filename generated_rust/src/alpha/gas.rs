#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(AlphaGas,u8,alphagas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn alphagas_write<U: Target>(val: &AlphaGas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphagas_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaGas> {
    AlphaGas::parse(p)
}
