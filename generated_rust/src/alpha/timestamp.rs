#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type AlphaTimestamp = i64;
#[allow(dead_code)]
pub fn alphatimestamp_write<U: Target>(val: &AlphaTimestamp, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphatimestamp_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaTimestamp> {
    Ok(i64::parse(p)?)
}
