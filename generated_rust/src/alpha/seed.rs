#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type AlphaSeed = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn alphaseed_write<U: Target>(val: &AlphaSeed, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphaseed_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaSeed> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
