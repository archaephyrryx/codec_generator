#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,Padded,ParseResult,Parser,Target,data,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
        3 => Bls  { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
data!(AlphaContract,u8,alphacontract,{
        0 => Implicit  { pub signature_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<AlphaContractIdOriginatedDenestPad,1>),
    }
    );
#[allow(dead_code)]
pub fn alphacontract_write<U: Target>(val: &AlphaContract, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphacontract_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaContract> {
    AlphaContract::parse(p)
}
