#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type AlphaNonce = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn alphanonce_write<U: Target>(val: &AlphaNonce, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphanonce_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaNonce> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
