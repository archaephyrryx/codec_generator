#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(AlphaFitnessLockedRound,u8,alphafitnesslockedround,{
        0 => None ,
        1 => Some (pub i32),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaFitness { pub level: i32, pub locked_round: AlphaFitnessLockedRound, pub predecessor_round: i32, pub round: i32 }
#[allow(dead_code)]
pub fn alphafitness_write<U: Target>(val: &AlphaFitness, buf: &mut U) -> usize {
    i32::write_to(&val.level, buf) + AlphaFitnessLockedRound::write_to(&val.locked_round, buf) + i32::write_to(&val.predecessor_round, buf) + i32::write_to(&val.round, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphafitness_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaFitness> {
    Ok(AlphaFitness {level: i32::parse(p)?, locked_round: AlphaFitnessLockedRound::parse(p)?, predecessor_round: i32::parse(p)?, round: i32::parse(p)?})
}
