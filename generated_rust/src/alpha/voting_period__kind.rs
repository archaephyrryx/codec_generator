#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(AlphaVotingPeriodKind,u8,alphavotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[allow(dead_code)]
pub fn alphavotingperiodkind_write<U: Target>(val: &AlphaVotingPeriodKind, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphavotingperiodkind_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaVotingPeriodKind> {
    AlphaVotingPeriodKind::parse(p)
}
