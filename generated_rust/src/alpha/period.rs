#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type AlphaPeriod = i64;
#[allow(dead_code)]
pub fn alphaperiod_write<U: Target>(val: &AlphaPeriod, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphaperiod_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaPeriod> {
    Ok(i64::parse(p)?)
}
