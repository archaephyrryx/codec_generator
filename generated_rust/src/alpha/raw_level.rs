#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type AlphaRawLevel = i32;
#[allow(dead_code)]
pub fn alpharawlevel_write<U: Target>(val: &AlphaRawLevel, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alpharawlevel_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaRawLevel> {
    Ok(i32::parse(p)?)
}
