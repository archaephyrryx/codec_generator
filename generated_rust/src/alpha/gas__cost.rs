#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type AlphaGasCost = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn alphagascost_write<U: Target>(val: &AlphaGasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphagascost_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaGasCost> {
    Ok(::rust_runtime::Z::parse(p)?)
}
