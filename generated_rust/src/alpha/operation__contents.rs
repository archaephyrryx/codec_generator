#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{AutoBox,ByteString,Bytes,Decode,Dynamic,Encode,Estimable,LimSeq,N,Nullable,Padded,ParseResult,Parser,Sequence,Target,Z,cstyle,data,i31,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderAlphaFullHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub payload_hash: AlphaBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: AlphaBlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderAlphaSignedContentsSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
data!(AlphaContractId,u8,alphacontractid,{
        0 => Implicit  { pub signature_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<AlphaContractIdOriginatedDenestPad,1>),
    }
    );
data!(AlphaContractIdOriginated,u8,alphacontractidoriginated,{
        1 => Originated (pub ::rust_runtime::Padded<AlphaContractIdOriginatedOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaContractIdOriginatedOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(AlphaEntrypoint,u8,alphaentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        5 => deposit ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedEndorsement { pub branch: OperationShellHeaderBranch, pub operations: AlphaInlinedEndorsementMempoolContents, pub signature: ::rust_runtime::Nullable<AlphaInlinedEndorsementSignature> }
data!(AlphaInlinedEndorsementMempoolContents,u8,alphainlinedendorsementmempoolcontents,{
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedEndorsementSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedPreendorsement { pub branch: OperationShellHeaderBranch, pub operations: AlphaInlinedPreendorsementContents, pub signature: ::rust_runtime::Nullable<AlphaInlinedPreendorsementSignature> }
data!(AlphaInlinedPreendorsementContents,u8,alphainlinedpreendorsementcontents,{
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedPreendorsementSignature { pub signature_v1: ::rust_runtime::Bytes }
cstyle!(AlphaMichelsonV1Primitives,u8,{
        parameter = 0,
        storage = 1,
        code = 2,
        False = 3,
        Elt = 4,
        Left = 5,
        None = 6,
        Pair = 7,
        Right = 8,
        Some = 9,
        True = 10,
        Unit = 11,
        PACK = 12,
        UNPACK = 13,
        BLAKE2B = 14,
        SHA256 = 15,
        SHA512 = 16,
        ABS = 17,
        ADD = 18,
        AMOUNT = 19,
        AND = 20,
        BALANCE = 21,
        CAR = 22,
        CDR = 23,
        CHECK_SIGNATURE = 24,
        COMPARE = 25,
        CONCAT = 26,
        CONS = 27,
        CREATE_ACCOUNT = 28,
        CREATE_CONTRACT = 29,
        IMPLICIT_ACCOUNT = 30,
        DIP = 31,
        DROP = 32,
        DUP = 33,
        EDIV = 34,
        EMPTY_MAP = 35,
        EMPTY_SET = 36,
        EQ = 37,
        EXEC = 38,
        FAILWITH = 39,
        GE = 40,
        GET = 41,
        GT = 42,
        HASH_KEY = 43,
        IF = 44,
        IF_CONS = 45,
        IF_LEFT = 46,
        IF_NONE = 47,
        INT = 48,
        LAMBDA = 49,
        LE = 50,
        LEFT = 51,
        LOOP = 52,
        LSL = 53,
        LSR = 54,
        LT = 55,
        MAP = 56,
        MEM = 57,
        MUL = 58,
        NEG = 59,
        NEQ = 60,
        NIL = 61,
        NONE = 62,
        NOT = 63,
        NOW = 64,
        OR = 65,
        PAIR = 66,
        PUSH = 67,
        RIGHT = 68,
        SIZE = 69,
        SOME = 70,
        SOURCE = 71,
        SENDER = 72,
        SELF = 73,
        STEPS_TO_QUOTA = 74,
        SUB = 75,
        SWAP = 76,
        TRANSFER_TOKENS = 77,
        SET_DELEGATE = 78,
        UNIT = 79,
        UPDATE = 80,
        XOR = 81,
        ITER = 82,
        LOOP_LEFT = 83,
        ADDRESS = 84,
        CONTRACT = 85,
        ISNAT = 86,
        CAST = 87,
        RENAME = 88,
        bool = 89,
        contract = 90,
        int = 91,
        key = 92,
        key_hash = 93,
        lambda = 94,
        list = 95,
        map = 96,
        big_map = 97,
        nat = 98,
        option = 99,
        or = 100,
        pair = 101,
        set = 102,
        signature = 103,
        string = 104,
        bytes = 105,
        mutez = 106,
        timestamp = 107,
        unit = 108,
        operation = 109,
        address = 110,
        SLICE = 111,
        DIG = 112,
        DUG = 113,
        EMPTY_BIG_MAP = 114,
        APPLY = 115,
        chain_id = 116,
        CHAIN_ID = 117,
        LEVEL = 118,
        SELF_ADDRESS = 119,
        never = 120,
        NEVER = 121,
        UNPAIR = 122,
        VOTING_POWER = 123,
        TOTAL_VOTING_POWER = 124,
        KECCAK = 125,
        SHA3 = 126,
        PAIRING_CHECK = 127,
        bls12_381_g1 = 128,
        bls12_381_g2 = 129,
        bls12_381_fr = 130,
        sapling_state = 131,
        sapling_transaction_deprecated = 132,
        SAPLING_EMPTY_STATE = 133,
        SAPLING_VERIFY_UPDATE = 134,
        ticket = 135,
        TICKET_DEPRECATED = 136,
        READ_TICKET = 137,
        SPLIT_TICKET = 138,
        JOIN_TICKETS = 139,
        GET_AND_UPDATE = 140,
        chest = 141,
        chest_key = 142,
        OPEN_CHEST = 143,
        VIEW = 144,
        view = 145,
        constant = 146,
        SUB_MUTEZ = 147,
        tx_rollup_l2_address = 148,
        MIN_BLOCK_TIME = 149,
        sapling_transaction = 150,
        EMIT = 151,
        Lambda_rec = 152,
        LAMBDA_REC = 153,
        TICKET = 154,
        BYTES = 155,
        NAT = 156
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsBallotSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsDalAttestationAttestor { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeader { pub published_level: i32, pub slot_index: u8, pub commitment: AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment, pub commitment_proof: ::rust_runtime::ByteString<48> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment { pub dal_commitment: ::rust_runtime::ByteString<48> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsDalPublishSlotHeaderSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsDelegationDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsDelegationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsDrainDelegateConsensusKey { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsDrainDelegateDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsDrainDelegateDestination { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsIncreasePaidStorageSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOriginationDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOriginationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsProposalsSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsRegisterGlobalConstantSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsRevealPublicKey { pub signature_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsRevealSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSetDepositsLimitSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupAddMessagesSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupCementCommitment { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupCementSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageSource { pub signature_public_key_hash: PublicKeyHash }
cstyle!(AlphaOperationAlphaContentsSmartRollupOriginatePvmKind,u8,{
        arith = 0,
        wasm_2_0_0 = 1
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupOriginateSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupPublishCommitment { pub compressed_state: AlphaOperationAlphaContentsSmartRollupPublishCommitmentCompressedState, pub inbox_level: i32, pub predecessor: AlphaOperationAlphaContentsSmartRollupPublishCommitmentPredecessor, pub number_of_ticks: i64 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupPublishCommitmentCompressedState { pub smart_rollup_state_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupPublishCommitmentPredecessor { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupPublishSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupRecoverBondRollup { pub smart_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupRecoverBondSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupRecoverBondStaker { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupRefuteOpponent { pub signature_public_key_hash: PublicKeyHash }
data!(AlphaOperationAlphaContentsSmartRollupRefuteRefutation,u8,alphaoperationalphacontentssmartrolluprefuterefutation,{
        0 => Start  { pub player_commitment_hash: AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash, pub opponent_commitment_hash: AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash },
        1 => Move  { pub choice: ::rust_runtime::N, pub step: AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep },
    }
    );
data!(AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep,u8,alphaoperationalphacontentssmartrolluprefuterefutationmovestep,{
        0 => Dissection (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq>>),
        1 => Proof  { pub pvm_step: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub input_proof: std::option::Option<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq { pub state: std::option::Option<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState>, pub tick: ::rust_runtime::N }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState { pub smart_rollup_state_hash: ::rust_runtime::ByteString<32> }
data!(AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof,u8,alphaoperationalphacontentssmartrolluprefuterefutationmovestepproofinputproof,{
        0 => inbox_proof  { pub level: i32, pub message_counter: ::rust_runtime::N, pub serialized_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => reveal_proof  { pub reveal_proof: AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof },
        2 => first_input ,
    }
    );
data!(AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof,u8,alphaoperationalphacontentssmartrolluprefuterefutationmovestepproofinputproofrevealproofrevealproof,{
        0 => raw_data_proof  { pub raw_data: ::rust_runtime::Dynamic<u16,std::string::String> },
        1 => metadata_proof ,
        2 => dal_page_proof  { pub dal_page_id: AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId, pub dal_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId { pub published_level: i32, pub slot_index: u8, pub page_index: i16 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupRefuteSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupTimeoutSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupTimeoutStakers { pub alice: AlphaOperationAlphaContentsSmartRollupTimeoutStakersAlice, pub bob: AlphaOperationAlphaContentsSmartRollupTimeoutStakersBob }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupTimeoutStakersAlice { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsSmartRollupTimeoutStakersBob { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTransactionParameters { pub entrypoint: AlphaEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTransactionSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTransferTicketSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupCommitCommitment { pub level: i32, pub messages: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq>>, pub predecessor: AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor, pub inbox_merkle_root: AlphaOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq { pub message_result_hash: ::rust_runtime::ByteString<32> }
data!(AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor,u8,alphaoperationalphacontentstxrollupcommitcommitmentpredecessor,{
        0 => None ,
        1 => Some  { pub commitment_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupCommitSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupDispatchTicketsContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupDispatchTicketsSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq { pub contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticketer: AlphaContractId, pub amount: AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount, pub claimer: AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer }
data!(AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,u8,alphaoperationalphacontentstxrollupdispatchticketsticketsinfodenestdyndenestseqamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupFinalizeCommitmentSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupOriginationSource { pub signature_public_key_hash: PublicKeyHash }
data!(AlphaOperationAlphaContentsTxRollupRejectionMessage,u8,alphaoperationalphacontentstxrolluprejectionmessage,{
        0 => Batch  { pub batch: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => Deposit  { pub deposit: AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDeposit },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDeposit { pub sender: AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender, pub destination: AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination, pub ticket_hash: AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash, pub amount: AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount }
data!(AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount,u8,alphaoperationalphacontentstxrolluprejectionmessagedepositdepositamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionMessageResultHash { pub message_result_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResult { pub context_hash: AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash, pub withdraw_list_hash: AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash { pub withdraw_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRejectionSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupRemoveCommitmentSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupReturnBondSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsTxRollupSubmitBatchSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsUpdateConsensusKeyPk { pub signature_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsUpdateConsensusKeySource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsVdfRevelationSolution(pub ::rust_runtime::ByteString<100>,pub ::rust_runtime::ByteString<100>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1);
data!(AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1,u8,alphaoperationalphacontentszkrolluporiginationcircuitsinfodenestdyndenestseqindex1,{
        0 => Public ,
        1 => Private ,
        2 => Fee ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupOriginationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq(pub AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0,pub AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0 { pub op_code: ::rust_runtime::i31, pub price: AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price, pub l1_dst: AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst, pub rollup_id: AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId, pub payload: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price { pub id: AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId, pub amount: ::rust_runtime::Z }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
data!(AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1,u8,alphaoperationalphacontentszkrolluppublishopdenestdyndenestseqindex1,{
        0 => None ,
        1 => Some  { pub contents: MichelineAlphaMichelsonV1Expression, pub ty: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>, pub ticketer: AlphaContractId },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupPublishSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupPublishZkRollup { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupUpdateSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupUpdateUpdate { pub pending_pis: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq>>, pub private_pis: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq>>, pub fee_pi: AlphaOperationAlphaContentsZkRollupUpdateUpdateFeePi, pub proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupUpdateUpdateFeePi { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1 { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub fee: ::rust_runtime::ByteString<32>, pub exit_validity: bool }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1 { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub fee: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsZkRollupUpdateZkRollup { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaSmartRollupAddress { pub smart_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
data!(MichelineAlphaMichelsonV1Expression,u8,michelinealphamichelsonv1expression,{
        0 => Int  { pub int: ::rust_runtime::Z },
        1 => String  { pub string: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        2 => Sequence (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>>>),
        3 => Prim__no_args__no_annots  { pub prim: AlphaMichelsonV1Primitives },
        4 => Prim__no_args__some_annots  { pub prim: AlphaMichelsonV1Primitives, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        5 => Prim__1_arg__no_annots  { pub prim: AlphaMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression> },
        6 => Prim__1_arg__some_annots  { pub prim: AlphaMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        7 => Prim__2_args__no_annots  { pub prim: AlphaMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression> },
        8 => Prim__2_args__some_annots  { pub prim: AlphaMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        9 => Prim__generic  { pub prim: AlphaMichelsonV1Primitives, pub args: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>>>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        10 => Bytes  { pub bytes: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
        3 => Bls  { pub bls12_381_public_key: ::rust_runtime::ByteString<48> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
        3 => Bls  { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
data!(AlphaOperationContents,u8,alphaoperationcontents,{
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32> },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaInlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaInlinedEndorsement> },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaBlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaBlockHeaderAlphaFullHeader> },
        4 => Activate_account  { pub pkh: AlphaOperationAlphaContentsActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20> },
        5 => Proposals  { pub source: AlphaOperationAlphaContentsProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::LimSeq<AlphaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq,20>> },
        6 => Ballot  { pub source: AlphaOperationAlphaContentsBallotSource, pub period: i32, pub proposal: AlphaOperationAlphaContentsBallotProposal, pub ballot: i8 },
        7 => Double_preendorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaInlinedPreendorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaInlinedPreendorsement> },
        8 => Vdf_revelation  { pub solution: AlphaOperationAlphaContentsVdfRevelationSolution },
        9 => Drain_delegate  { pub consensus_key: AlphaOperationAlphaContentsDrainDelegateConsensusKey, pub delegate: AlphaOperationAlphaContentsDrainDelegateDelegate, pub destination: AlphaOperationAlphaContentsDrainDelegateDestination },
        17 => Failing_noop  { pub arbitrary: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: AlphaOperationAlphaContentsPreendorsementBlockPayloadHash },
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: AlphaOperationAlphaContentsEndorsementBlockPayloadHash },
        22 => Dal_attestation  { pub attestor: AlphaOperationAlphaContentsDalAttestationAttestor, pub attestation: ::rust_runtime::Z, pub level: i32 },
        107 => Reveal  { pub source: AlphaOperationAlphaContentsRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: AlphaOperationAlphaContentsRevealPublicKey },
        108 => Transaction  { pub source: AlphaOperationAlphaContentsTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: AlphaContractId, pub parameters: std::option::Option<AlphaOperationAlphaContentsTransactionParameters> },
        109 => Origination  { pub source: AlphaOperationAlphaContentsOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<AlphaOperationAlphaContentsOriginationDelegate>, pub script: AlphaScriptedContracts },
        110 => Delegation  { pub source: AlphaOperationAlphaContentsDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<AlphaOperationAlphaContentsDelegationDelegate> },
        111 => Register_global_constant  { pub source: AlphaOperationAlphaContentsRegisterGlobalConstantSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        112 => Set_deposits_limit  { pub source: AlphaOperationAlphaContentsSetDepositsLimitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub limit: std::option::Option<::rust_runtime::N> },
        113 => Increase_paid_storage  { pub source: AlphaOperationAlphaContentsIncreasePaidStorageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::Z, pub destination: AlphaContractIdOriginated },
        114 => Update_consensus_key  { pub source: AlphaOperationAlphaContentsUpdateConsensusKeySource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub pk: AlphaOperationAlphaContentsUpdateConsensusKeyPk },
        150 => Tx_rollup_origination  { pub source: AlphaOperationAlphaContentsTxRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N },
        151 => Tx_rollup_submit_batch  { pub source: AlphaOperationAlphaContentsTxRollupSubmitBatchSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId, pub content: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub burn_limit: std::option::Option<::rust_runtime::N> },
        152 => Tx_rollup_commit  { pub source: AlphaOperationAlphaContentsTxRollupCommitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId, pub commitment: AlphaOperationAlphaContentsTxRollupCommitCommitment },
        153 => Tx_rollup_return_bond  { pub source: AlphaOperationAlphaContentsTxRollupReturnBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId },
        154 => Tx_rollup_finalize_commitment  { pub source: AlphaOperationAlphaContentsTxRollupFinalizeCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId },
        155 => Tx_rollup_remove_commitment  { pub source: AlphaOperationAlphaContentsTxRollupRemoveCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId },
        156 => Tx_rollup_rejection  { pub source: AlphaOperationAlphaContentsTxRollupRejectionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId, pub level: i32, pub message: AlphaOperationAlphaContentsTxRollupRejectionMessage, pub message_position: ::rust_runtime::N, pub message_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq>>, pub message_result_hash: AlphaOperationAlphaContentsTxRollupRejectionMessageResultHash, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq>>, pub previous_message_result: AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResult, pub previous_message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>>, pub proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        157 => Tx_rollup_dispatch_tickets  { pub source: AlphaOperationAlphaContentsTxRollupDispatchTicketsSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub tx_rollup: AlphaTxRollupId, pub level: i32, pub context_hash: AlphaOperationAlphaContentsTxRollupDispatchTicketsContextHash, pub message_index: ::rust_runtime::i31, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>>, pub tickets_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>> },
        158 => Transfer_ticket  { pub source: AlphaOperationAlphaContentsTransferTicketSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub ticket_contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ticketer: AlphaContractId, pub ticket_amount: ::rust_runtime::N, pub destination: AlphaContractId, pub entrypoint: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        200 => Smart_rollup_originate  { pub source: AlphaOperationAlphaContentsSmartRollupOriginateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub pvm_kind: AlphaOperationAlphaContentsSmartRollupOriginatePvmKind, pub kernel: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub origination_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub parameters_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        201 => Smart_rollup_add_messages  { pub source: AlphaOperationAlphaContentsSmartRollupAddMessagesSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub message: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        202 => Smart_rollup_cement  { pub source: AlphaOperationAlphaContentsSmartRollupCementSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaSmartRollupAddress, pub commitment: AlphaOperationAlphaContentsSmartRollupCementCommitment },
        203 => Smart_rollup_publish  { pub source: AlphaOperationAlphaContentsSmartRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaSmartRollupAddress, pub commitment: AlphaOperationAlphaContentsSmartRollupPublishCommitment },
        204 => Smart_rollup_refute  { pub source: AlphaOperationAlphaContentsSmartRollupRefuteSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaSmartRollupAddress, pub opponent: AlphaOperationAlphaContentsSmartRollupRefuteOpponent, pub refutation: AlphaOperationAlphaContentsSmartRollupRefuteRefutation },
        205 => Smart_rollup_timeout  { pub source: AlphaOperationAlphaContentsSmartRollupTimeoutSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaSmartRollupAddress, pub stakers: AlphaOperationAlphaContentsSmartRollupTimeoutStakers },
        206 => Smart_rollup_execute_outbox_message  { pub source: AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaSmartRollupAddress, pub cemented_commitment: AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment, pub output_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        207 => Smart_rollup_recover_bond  { pub source: AlphaOperationAlphaContentsSmartRollupRecoverBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaOperationAlphaContentsSmartRollupRecoverBondRollup, pub staker: AlphaOperationAlphaContentsSmartRollupRecoverBondStaker },
        230 => Dal_publish_slot_header  { pub source: AlphaOperationAlphaContentsDalPublishSlotHeaderSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub slot_header: AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeader },
        250 => Zk_rollup_origination  { pub source: AlphaOperationAlphaContentsZkRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_parameters: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub circuits_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq>>, pub init_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub nb_ops: ::rust_runtime::i31 },
        251 => Zk_rollup_publish  { pub source: AlphaOperationAlphaContentsZkRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub zk_rollup: AlphaOperationAlphaContentsZkRollupPublishZkRollup, pub op: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq>> },
        252 => Zk_rollup_update  { pub source: AlphaOperationAlphaContentsZkRollupUpdateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub zk_rollup: AlphaOperationAlphaContentsZkRollupUpdateZkRollup, pub update: AlphaOperationAlphaContentsZkRollupUpdateUpdate },
    }
    );
#[allow(dead_code)]
pub fn alphaoperationcontents_write<U: Target>(val: &AlphaOperationContents, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphaoperationcontents_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaOperationContents> {
    AlphaOperationContents::parse(p)
}
