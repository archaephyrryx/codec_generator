#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{AutoBox,ByteString,Bytes,Decode,Dynamic,Encode,Estimable,LimSeq,N,Nullable,Padded,ParseResult,Parser,Sequence,Target,VPadded,Z,cstyle,data,i31,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderAlphaFullHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub payload_hash: AlphaBlockHeaderAlphaUnsignedContentsPayloadHash, pub payload_round: i32, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_toggle_vote: i8, pub signature: AlphaBlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderAlphaSignedContentsSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderAlphaUnsignedContentsPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
data!(AlphaContractId,u8,alphacontractid,{
        0 => Implicit  { pub signature_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<AlphaContractIdOriginatedDenestPad,1>),
    }
    );
data!(AlphaContractIdOriginated,u8,alphacontractidoriginated,{
        1 => Originated (pub ::rust_runtime::Padded<AlphaContractIdOriginatedOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaContractIdOriginatedOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(AlphaEntrypoint,u8,alphaentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        5 => deposit ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedEndorsement { pub branch: OperationShellHeaderBranch, pub operations: AlphaInlinedEndorsementMempoolContents, pub signature: ::rust_runtime::Nullable<AlphaInlinedEndorsementSignature> }
data!(AlphaInlinedEndorsementMempoolContents,u8,alphainlinedendorsementmempoolcontents,{
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedEndorsementSignature { pub signature_v1: ::rust_runtime::Bytes }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedPreendorsement { pub branch: OperationShellHeaderBranch, pub operations: AlphaInlinedPreendorsementContents, pub signature: ::rust_runtime::Nullable<AlphaInlinedPreendorsementSignature> }
data!(AlphaInlinedPreendorsementContents,u8,alphainlinedpreendorsementcontents,{
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaInlinedPreendorsementSignature { pub signature_v1: ::rust_runtime::Bytes }
cstyle!(AlphaMichelsonV1Primitives,u8,{
        parameter = 0,
        storage = 1,
        code = 2,
        False = 3,
        Elt = 4,
        Left = 5,
        None = 6,
        Pair = 7,
        Right = 8,
        Some = 9,
        True = 10,
        Unit = 11,
        PACK = 12,
        UNPACK = 13,
        BLAKE2B = 14,
        SHA256 = 15,
        SHA512 = 16,
        ABS = 17,
        ADD = 18,
        AMOUNT = 19,
        AND = 20,
        BALANCE = 21,
        CAR = 22,
        CDR = 23,
        CHECK_SIGNATURE = 24,
        COMPARE = 25,
        CONCAT = 26,
        CONS = 27,
        CREATE_ACCOUNT = 28,
        CREATE_CONTRACT = 29,
        IMPLICIT_ACCOUNT = 30,
        DIP = 31,
        DROP = 32,
        DUP = 33,
        EDIV = 34,
        EMPTY_MAP = 35,
        EMPTY_SET = 36,
        EQ = 37,
        EXEC = 38,
        FAILWITH = 39,
        GE = 40,
        GET = 41,
        GT = 42,
        HASH_KEY = 43,
        IF = 44,
        IF_CONS = 45,
        IF_LEFT = 46,
        IF_NONE = 47,
        INT = 48,
        LAMBDA = 49,
        LE = 50,
        LEFT = 51,
        LOOP = 52,
        LSL = 53,
        LSR = 54,
        LT = 55,
        MAP = 56,
        MEM = 57,
        MUL = 58,
        NEG = 59,
        NEQ = 60,
        NIL = 61,
        NONE = 62,
        NOT = 63,
        NOW = 64,
        OR = 65,
        PAIR = 66,
        PUSH = 67,
        RIGHT = 68,
        SIZE = 69,
        SOME = 70,
        SOURCE = 71,
        SENDER = 72,
        SELF = 73,
        STEPS_TO_QUOTA = 74,
        SUB = 75,
        SWAP = 76,
        TRANSFER_TOKENS = 77,
        SET_DELEGATE = 78,
        UNIT = 79,
        UPDATE = 80,
        XOR = 81,
        ITER = 82,
        LOOP_LEFT = 83,
        ADDRESS = 84,
        CONTRACT = 85,
        ISNAT = 86,
        CAST = 87,
        RENAME = 88,
        bool = 89,
        contract = 90,
        int = 91,
        key = 92,
        key_hash = 93,
        lambda = 94,
        list = 95,
        map = 96,
        big_map = 97,
        nat = 98,
        option = 99,
        or = 100,
        pair = 101,
        set = 102,
        signature = 103,
        string = 104,
        bytes = 105,
        mutez = 106,
        timestamp = 107,
        unit = 108,
        operation = 109,
        address = 110,
        SLICE = 111,
        DIG = 112,
        DUG = 113,
        EMPTY_BIG_MAP = 114,
        APPLY = 115,
        chain_id = 116,
        CHAIN_ID = 117,
        LEVEL = 118,
        SELF_ADDRESS = 119,
        never = 120,
        NEVER = 121,
        UNPAIR = 122,
        VOTING_POWER = 123,
        TOTAL_VOTING_POWER = 124,
        KECCAK = 125,
        SHA3 = 126,
        PAIRING_CHECK = 127,
        bls12_381_g1 = 128,
        bls12_381_g2 = 129,
        bls12_381_fr = 130,
        sapling_state = 131,
        sapling_transaction_deprecated = 132,
        SAPLING_EMPTY_STATE = 133,
        SAPLING_VERIFY_UPDATE = 134,
        ticket = 135,
        TICKET_DEPRECATED = 136,
        READ_TICKET = 137,
        SPLIT_TICKET = 138,
        JOIN_TICKETS = 139,
        GET_AND_UPDATE = 140,
        chest = 141,
        chest_key = 142,
        OPEN_CHEST = 143,
        VIEW = 144,
        view = 145,
        constant = 146,
        SUB_MUTEZ = 147,
        tx_rollup_l2_address = 148,
        MIN_BLOCK_TIME = 149,
        sapling_transaction = 150,
        EMIT = 151,
        Lambda_rec = 152,
        LAMBDA_REC = 153,
        TICKET = 154,
        BYTES = 155,
        NAT = 156
    }
    );
data!(AlphaOperationAlphaContentsOrSignaturePrefix,u8,alphaoperationalphacontentsorsignatureprefix,{
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32> },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaInlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaInlinedEndorsement> },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaBlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaBlockHeaderAlphaFullHeader> },
        4 => Activate_account  { pub pkh: AlphaOperationAlphaContentsOrSignaturePrefixActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20> },
        5 => Proposals  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::LimSeq<AlphaOperationAlphaContentsOrSignaturePrefixProposalsProposalsDenestDynDenestSeq,20>> },
        6 => Ballot  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixBallotSource, pub period: i32, pub proposal: AlphaOperationAlphaContentsOrSignaturePrefixBallotProposal, pub ballot: i8 },
        7 => Double_preendorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaInlinedPreendorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,AlphaInlinedPreendorsement> },
        8 => Vdf_revelation  { pub solution: AlphaOperationAlphaContentsOrSignaturePrefixVdfRevelationSolution },
        9 => Drain_delegate  { pub consensus_key: AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateConsensusKey, pub delegate: AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDelegate, pub destination: AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDestination },
        17 => Failing_noop  { pub arbitrary: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        20 => Preendorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: AlphaOperationAlphaContentsOrSignaturePrefixPreendorsementBlockPayloadHash },
        21 => Endorsement  { pub slot: u16, pub level: i32, pub round: i32, pub block_payload_hash: AlphaOperationAlphaContentsOrSignaturePrefixEndorsementBlockPayloadHash },
        22 => Dal_attestation  { pub attestor: AlphaOperationAlphaContentsOrSignaturePrefixDalAttestationAttestor, pub attestation: ::rust_runtime::Z, pub level: i32 },
        107 => Reveal  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: AlphaOperationAlphaContentsOrSignaturePrefixRevealPublicKey },
        108 => Transaction  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: AlphaContractId, pub parameters: std::option::Option<AlphaOperationAlphaContentsOrSignaturePrefixTransactionParameters> },
        109 => Origination  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<AlphaOperationAlphaContentsOrSignaturePrefixOriginationDelegate>, pub script: AlphaScriptedContracts },
        110 => Delegation  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<AlphaOperationAlphaContentsOrSignaturePrefixDelegationDelegate> },
        111 => Register_global_constant  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixRegisterGlobalConstantSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        112 => Set_deposits_limit  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixSetDepositsLimitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub limit: std::option::Option<::rust_runtime::N> },
        113 => Increase_paid_storage  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixIncreasePaidStorageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::Z, pub destination: AlphaContractIdOriginated },
        114 => Update_consensus_key  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeySource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub pk: AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeyPk },
        150 => Tx_rollup_origination  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N },
        151 => Tx_rollup_submit_batch  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupSubmitBatchSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId, pub content: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub burn_limit: std::option::Option<::rust_runtime::N> },
        152 => Tx_rollup_commit  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId, pub commitment: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitment },
        153 => Tx_rollup_return_bond  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupReturnBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId },
        154 => Tx_rollup_finalize_commitment  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupFinalizeCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId },
        155 => Tx_rollup_remove_commitment  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRemoveCommitmentSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId },
        156 => Tx_rollup_rejection  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaTxRollupId, pub level: i32, pub message: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage, pub message_position: ::rust_runtime::N, pub message_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessagePathDenestDynDenestSeq>>, pub message_result_hash: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultHash, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultPathDenestDynDenestSeq>>, pub previous_message_result: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResult, pub previous_message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>>, pub proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        157 => Tx_rollup_dispatch_tickets  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub tx_rollup: AlphaTxRollupId, pub level: i32, pub context_hash: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsContextHash, pub message_index: ::rust_runtime::i31, pub message_result_path: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>>, pub tickets_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>> },
        158 => Transfer_ticket  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixTransferTicketSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub ticket_contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticket_ticketer: AlphaContractId, pub ticket_amount: ::rust_runtime::N, pub destination: AlphaContractId, pub entrypoint: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        200 => Smart_rollup_originate  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub pvm_kind: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginatePvmKind, pub kernel: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub origination_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub parameters_ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
        201 => Smart_rollup_add_messages  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupAddMessagesSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub message: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>> },
        202 => Smart_rollup_cement  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaSmartRollupAddress, pub commitment: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementCommitment },
        203 => Smart_rollup_publish  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaSmartRollupAddress, pub commitment: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitment },
        204 => Smart_rollup_refute  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaSmartRollupAddress, pub opponent: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteOpponent, pub refutation: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation },
        205 => Smart_rollup_timeout  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaSmartRollupAddress, pub stakers: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakers },
        206 => Smart_rollup_execute_outbox_message  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaSmartRollupAddress, pub cemented_commitment: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageCementedCommitment, pub output_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        207 => Smart_rollup_recover_bond  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub rollup: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondRollup, pub staker: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondStaker },
        230 => Dal_publish_slot_header  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub slot_header: AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeader },
        250 => Zk_rollup_origination  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_parameters: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub circuits_info: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeq>>, pub init_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub nb_ops: ::rust_runtime::i31 },
        251 => Zk_rollup_publish  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub zk_rollup: AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishZkRollup, pub op: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeq>> },
        252 => Zk_rollup_update  { pub source: AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub zk_rollup: AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateZkRollup, pub update: AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdate },
        255 => Signature_prefix  { pub signature_prefix: BlsSignaturePrefix },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixBallotSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixDalAttestationAttestor { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeader { pub published_level: i32, pub slot_index: u8, pub commitment: AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeaderCommitment, pub commitment_proof: ::rust_runtime::ByteString<48> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeaderCommitment { pub dal_commitment: ::rust_runtime::ByteString<48> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixDelegationDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixDelegationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateConsensusKey { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDestination { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixEndorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixIncreasePaidStorageSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixOriginationDelegate { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixOriginationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixPreendorsementBlockPayloadHash { pub value_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixProposalsSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixRegisterGlobalConstantSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixRevealPublicKey { pub signature_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixRevealSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSetDepositsLimitSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupAddMessagesSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementCommitment { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageCementedCommitment { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageSource { pub signature_public_key_hash: PublicKeyHash }
cstyle!(AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginatePvmKind,u8,{
        arith = 0,
        wasm_2_0_0 = 1
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginateSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitment { pub compressed_state: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentCompressedState, pub inbox_level: i32, pub predecessor: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentPredecessor, pub number_of_ticks: i64 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentCompressedState { pub smart_rollup_state_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentPredecessor { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondRollup { pub smart_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondStaker { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteOpponent { pub signature_public_key_hash: PublicKeyHash }
data!(AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation,u8,alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutation,{
        0 => Start  { pub player_commitment_hash: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartPlayerCommitmentHash, pub opponent_commitment_hash: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartOpponentCommitmentHash },
        1 => Move  { pub choice: ::rust_runtime::N, pub step: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep },
    }
    );
data!(AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep,u8,alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestep,{
        0 => Dissection (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq>>),
        1 => Proof  { pub pvm_step: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub input_proof: std::option::Option<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq { pub state: std::option::Option<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState>, pub tick: ::rust_runtime::N }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState { pub smart_rollup_state_hash: ::rust_runtime::ByteString<32> }
data!(AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof,u8,alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestepproofinputproof,{
        0 => inbox_proof  { pub level: i32, pub message_counter: ::rust_runtime::N, pub serialized_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => reveal_proof  { pub reveal_proof: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof },
        2 => first_input ,
    }
    );
data!(AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof,u8,alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestepproofinputproofrevealproofrevealproof,{
        0 => raw_data_proof  { pub raw_data: ::rust_runtime::Dynamic<u16,std::string::String> },
        1 => metadata_proof ,
        2 => dal_page_proof  { pub dal_page_id: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId, pub dal_proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId { pub published_level: i32, pub slot_index: u8, pub page_index: i16 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartOpponentCommitmentHash { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartPlayerCommitmentHash { pub smart_rollup_commitment_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakers { pub alice: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersAlice, pub bob: AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersBob }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersAlice { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersBob { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTransactionParameters { pub entrypoint: AlphaEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTransactionSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTransferTicketSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitment { pub level: i32, pub messages: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentMessagesDenestDynDenestSeq>>, pub predecessor: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor, pub inbox_merkle_root: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentInboxMerkleRoot }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentInboxMerkleRoot { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentMessagesDenestDynDenestSeq { pub message_result_hash: ::rust_runtime::ByteString<32> }
data!(AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor,u8,alphaoperationalphacontentsorsignatureprefixtxrollupcommitcommitmentpredecessor,{
        0 => None ,
        1 => Some  { pub commitment_hash: ::rust_runtime::ByteString<32> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq { pub contents: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ty: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub ticketer: AlphaContractId, pub amount: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount, pub claimer: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer }
data!(AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,u8,alphaoperationalphacontentsorsignatureprefixtxrollupdispatchticketsticketsinfodenestdyndenestseqamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupFinalizeCommitmentSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupOriginationSource { pub signature_public_key_hash: PublicKeyHash }
data!(AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage,u8,alphaoperationalphacontentsorsignatureprefixtxrolluprejectionmessage,{
        0 => Batch  { pub batch: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        1 => Deposit  { pub deposit: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDeposit },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDeposit { pub sender: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositSender, pub destination: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositDestination, pub ticket_hash: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositTicketHash, pub amount: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount }
data!(AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount,u8,alphaoperationalphacontentsorsignatureprefixtxrolluprejectionmessagedepositdepositamount,{
        0 => case_0 (pub u8),
        1 => case_1 (pub u16),
        2 => case_2 (pub i32),
        3 => case_3 (pub i64),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositDestination { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositSender { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositTicketHash { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessagePathDenestDynDenestSeq { pub inbox_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultHash { pub message_result_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResult { pub context_hash: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultContextHash, pub withdraw_list_hash: AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultWithdrawListHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultContextHash { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq { pub message_result_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultWithdrawListHash { pub withdraw_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRemoveCommitmentSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupReturnBondSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixTxRollupSubmitBatchSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeyPk { pub signature_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeySource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixVdfRevelationSolution(pub ::rust_runtime::ByteString<100>,pub ::rust_runtime::ByteString<100>);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1);
data!(AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1,u8,alphaoperationalphacontentsorsignatureprefixzkrolluporiginationcircuitsinfodenestdyndenestseqindex1,{
        0 => Public ,
        1 => Private ,
        2 => Fee ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeq(pub AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0,pub AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0 { pub op_code: ::rust_runtime::i31, pub price: AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price, pub l1_dst: AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst, pub rollup_id: AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId, pub payload: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price { pub id: AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId, pub amount: ::rust_runtime::Z }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId { pub script_expr: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
data!(AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1,u8,alphaoperationalphacontentsorsignatureprefixzkrolluppublishopdenestdyndenestseqindex1,{
        0 => None ,
        1 => Some  { pub contents: MichelineAlphaMichelsonV1Expression, pub ty: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>, pub ticketer: AlphaContractId },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishZkRollup { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateSource { pub signature_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdate { pub pending_pis: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeq>>, pub private_pis: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq>>, pub fee_pi: AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdateFeePi, pub proof: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdateFeePi { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1 { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub fee: ::rust_runtime::ByteString<32>, pub exit_validity: bool }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq(pub ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>,pub AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1);
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1 { pub new_state: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::ByteString<32>>>, pub fee: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateZkRollup { pub zk_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaSmartRollupAddress { pub smart_rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaTxRollupId { pub rollup_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
data!(BlsSignaturePrefix,u8,blssignatureprefix,{
        3 => Bls_prefix (pub ::rust_runtime::ByteString<32>),
    }
    );
data!(MichelineAlphaMichelsonV1Expression,u8,michelinealphamichelsonv1expression,{
        0 => Int  { pub int: ::rust_runtime::Z },
        1 => String  { pub string: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        2 => Sequence (pub ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>>>),
        3 => Prim__no_args__no_annots  { pub prim: AlphaMichelsonV1Primitives },
        4 => Prim__no_args__some_annots  { pub prim: AlphaMichelsonV1Primitives, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        5 => Prim__1_arg__no_annots  { pub prim: AlphaMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression> },
        6 => Prim__1_arg__some_annots  { pub prim: AlphaMichelsonV1Primitives, pub arg: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        7 => Prim__2_args__no_annots  { pub prim: AlphaMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression> },
        8 => Prim__2_args__some_annots  { pub prim: AlphaMichelsonV1Primitives, pub arg1: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>, pub arg2: ::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        9 => Prim__generic  { pub prim: AlphaMichelsonV1Primitives, pub args: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::AutoBox<MichelineAlphaMichelsonV1Expression>>>, pub annots: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        10 => Bytes  { pub bytes: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
        3 => Bls  { pub bls12_381_public_key: ::rust_runtime::ByteString<48> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
        3 => Bls  { pub bls12_381_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct AlphaOperationProtocolData { pub contents_and_signature_prefix: ::rust_runtime::VPadded<::rust_runtime::Sequence<AlphaOperationAlphaContentsOrSignaturePrefix>,64>, pub signature_suffix: ::rust_runtime::ByteString<64> }
#[allow(dead_code)]
pub fn alphaoperationprotocoldata_write<U: Target>(val: &AlphaOperationProtocolData, buf: &mut U) -> usize {
    ::rust_runtime::VPadded::<::rust_runtime::Sequence::<AlphaOperationAlphaContentsOrSignaturePrefix>,64>::write_to(&val.contents_and_signature_prefix, buf) + ::rust_runtime::ByteString::<64>::write_to(&val.signature_suffix, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn alphaoperationprotocoldata_parse<P: Parser>(p: &mut P) -> ParseResult<AlphaOperationProtocolData> {
    Ok(AlphaOperationProtocolData {contents_and_signature_prefix: ::rust_runtime::VPadded::<::rust_runtime::Sequence::<AlphaOperationAlphaContentsOrSignaturePrefix>,64>::parse(p)?, signature_suffix: ::rust_runtime::ByteString::<64>::parse(p)?})
}
