#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADVoteListingsDenestDynDenestSeq { pub pkh: Proto010PtGRANADVoteListingsDenestDynDenestSeqPkh, pub rolls: i32 }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADVoteListingsDenestDynDenestSeqPkh { pub signature_v0_public_key_hash: PublicKeyHash }
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto010PtGRANADVoteListings = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto010PtGRANADVoteListingsDenestDynDenestSeq>>;
#[allow(dead_code)]
pub fn proto010ptgranadvotelistings_write<U: Target>(val: &Proto010PtGRANADVoteListings, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto010PtGRANADVoteListingsDenestDynDenestSeq>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadvotelistings_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADVoteListings> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto010PtGRANADVoteListingsDenestDynDenestSeq>>::parse(p)?)
}
