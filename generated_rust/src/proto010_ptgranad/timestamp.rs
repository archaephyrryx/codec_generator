#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto010PtGRANADTimestamp = i64;
#[allow(dead_code)]
pub fn proto010ptgranadtimestamp_write<U: Target>(val: &Proto010PtGRANADTimestamp, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadtimestamp_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADTimestamp> {
    Ok(i64::parse(p)?)
}
