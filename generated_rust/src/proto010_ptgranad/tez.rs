#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,N,ParseResult,Parser,Target,resolve_zero};
pub type Proto010PtGRANADTez = ::rust_runtime::N;
#[allow(dead_code)]
pub fn proto010ptgranadtez_write<U: Target>(val: &Proto010PtGRANADTez, buf: &mut U) -> usize {
    ::rust_runtime::N::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadtez_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADTez> {
    Ok(::rust_runtime::N::parse(p)?)
}
