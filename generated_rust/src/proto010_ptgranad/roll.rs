#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto010PtGRANADRoll = i32;
#[allow(dead_code)]
pub fn proto010ptgranadroll_write<U: Target>(val: &Proto010PtGRANADRoll, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadroll_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADRoll> {
    Ok(i32::parse(p)?)
}
