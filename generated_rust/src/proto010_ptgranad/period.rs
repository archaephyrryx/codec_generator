#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto010PtGRANADPeriod = i64;
#[allow(dead_code)]
pub fn proto010ptgranadperiod_write<U: Target>(val: &Proto010PtGRANADPeriod, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADPeriod> {
    Ok(i64::parse(p)?)
}
