#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto010PtGRANADVoteBallot = i8;
#[allow(dead_code)]
pub fn proto010ptgranadvoteballot_write<U: Target>(val: &Proto010PtGRANADVoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadvoteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADVoteBallot> {
    Ok(i8::parse(p)?)
}
