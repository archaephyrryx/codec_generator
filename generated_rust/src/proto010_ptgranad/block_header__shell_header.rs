#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADBlockHeaderShellHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext }
#[allow(dead_code)]
pub fn proto010ptgranadblockheadershellheader_write<U: Target>(val: &Proto010PtGRANADBlockHeaderShellHeader, buf: &mut U) -> usize {
    i32::write_to(&val.level, buf) + u8::write_to(&val.proto, buf) + BlockHeaderShellPredecessor::write_to(&val.predecessor, buf) + i64::write_to(&val.timestamp, buf) + u8::write_to(&val.validation_pass, buf) + BlockHeaderShellOperationsHash::write_to(&val.operations_hash, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>>>::write_to(&val.fitness, buf) + BlockHeaderShellContext::write_to(&val.context, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadblockheadershellheader_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADBlockHeaderShellHeader> {
    Ok(Proto010PtGRANADBlockHeaderShellHeader {level: i32::parse(p)?, proto: u8::parse(p)?, predecessor: BlockHeaderShellPredecessor::parse(p)?, timestamp: i64::parse(p)?, validation_pass: u8::parse(p)?, operations_hash: BlockHeaderShellOperationsHash::parse(p)?, fitness: ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>>>::parse(p)?, context: BlockHeaderShellContext::parse(p)?})
}
