#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto010PtGRANADGas,u8,proto010ptgranadgas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto010ptgranadgas_write<U: Target>(val: &Proto010PtGRANADGas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadgas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADGas> {
    Proto010PtGRANADGas::parse(p)
}
