#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto010PtGRANADSeed = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn proto010ptgranadseed_write<U: Target>(val: &Proto010PtGRANADSeed, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadseed_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADSeed> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
