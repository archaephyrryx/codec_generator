#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADBlockHeaderContents { pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_escape_vote: bool }
#[allow(dead_code)]
pub fn proto010ptgranadblockheadercontents_write<U: Target>(val: &Proto010PtGRANADBlockHeaderContents, buf: &mut U) -> usize {
    u16::write_to(&val.priority, buf) + ::rust_runtime::ByteString::<8>::write_to(&val.proof_of_work_nonce, buf) + std::option::Option::<Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash>::write_to(&val.seed_nonce_hash, buf) + bool::write_to(&val.liquidity_baking_escape_vote, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadblockheadercontents_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADBlockHeaderContents> {
    Ok(Proto010PtGRANADBlockHeaderContents {priority: u16::parse(p)?, proof_of_work_nonce: ::rust_runtime::ByteString::<8>::parse(p)?, seed_nonce_hash: std::option::Option::<Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash>::parse(p)?, liquidity_baking_escape_vote: bool::parse(p)?})
}
