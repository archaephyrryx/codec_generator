#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto010PtGRANADVotingPeriodKind,u8,proto010ptgranadvotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADVotingPeriod { pub index: i32, pub kind: Proto010PtGRANADVotingPeriodKind, pub start_position: i32 }
#[allow(dead_code)]
pub fn proto010ptgranadvotingperiod_write<U: Target>(val: &Proto010PtGRANADVotingPeriod, buf: &mut U) -> usize {
    i32::write_to(&val.index, buf) + Proto010PtGRANADVotingPeriodKind::write_to(&val.kind, buf) + i32::write_to(&val.start_position, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadvotingperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADVotingPeriod> {
    Ok(Proto010PtGRANADVotingPeriod {index: i32::parse(p)?, kind: Proto010PtGRANADVotingPeriodKind::parse(p)?, start_position: i32::parse(p)?})
}
