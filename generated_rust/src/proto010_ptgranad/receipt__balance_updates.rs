#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(Proto010PtGRANADContractId,u8,proto010ptgranadcontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto010PtGRANADContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto010PtGRANADOperationMetadataAlphaBalance,u8,proto010ptgranadoperationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto010PtGRANADContractId, pub change: i64, pub origin: Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin },
        1 => Rewards  { pub delegate: Proto010PtGRANADOperationMetadataAlphaBalanceRewardsDelegate, pub cycle: i32, pub change: i64, pub origin: Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin },
        2 => Fees  { pub delegate: Proto010PtGRANADOperationMetadataAlphaBalanceFeesDelegate, pub cycle: i32, pub change: i64, pub origin: Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin },
        3 => Deposits  { pub delegate: Proto010PtGRANADOperationMetadataAlphaBalanceDepositsDelegate, pub cycle: i32, pub change: i64, pub origin: Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADOperationMetadataAlphaBalanceDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADOperationMetadataAlphaBalanceFeesDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADOperationMetadataAlphaBalanceRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin,u8,proto010ptgranadoperationmetadataalphaupdateoriginorigin,{
        0 => Block_application ,
        1 => Protocol_migration ,
        2 => Subsidy ,
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto010PtGRANADReceiptBalanceUpdates = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto010PtGRANADOperationMetadataAlphaBalance>>;
#[allow(dead_code)]
pub fn proto010ptgranadreceiptbalanceupdates_write<U: Target>(val: &Proto010PtGRANADReceiptBalanceUpdates, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto010PtGRANADOperationMetadataAlphaBalance>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadreceiptbalanceupdates_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADReceiptBalanceUpdates> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto010PtGRANADOperationMetadataAlphaBalance>>::parse(p)?)
}
