#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto010PtGRANADCycle = i32;
#[allow(dead_code)]
pub fn proto010ptgranadcycle_write<U: Target>(val: &Proto010PtGRANADCycle, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadcycle_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADCycle> {
    Ok(i32::parse(p)?)
}
