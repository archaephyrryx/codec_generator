#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,N,ParseResult,Parser,Target,Z,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADFa12TokenTransfer { pub token_contract: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub destination: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>, pub amount: ::rust_runtime::Z, pub tez_amount: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>, pub fee: std::option::Option<::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String>>, pub gas_limit: std::option::Option<::rust_runtime::N>, pub storage_limit: std::option::Option<::rust_runtime::Z> }
#[allow(dead_code)]
pub fn proto010ptgranadfa12tokentransfer_write<U: Target>(val: &Proto010PtGRANADFa12TokenTransfer, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(&val.token_contract, buf) + ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::write_to(&val.destination, buf) + ::rust_runtime::Z::write_to(&val.amount, buf) + std::option::Option::<::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>>::write_to(&val.tez_amount, buf) + std::option::Option::<::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>>::write_to(&val.fee, buf) + std::option::Option::<::rust_runtime::N>::write_to(&val.gas_limit, buf) + std::option::Option::<::rust_runtime::Z>::write_to(&val.storage_limit, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadfa12tokentransfer_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADFa12TokenTransfer> {
    Ok(Proto010PtGRANADFa12TokenTransfer {token_contract: ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?, destination: ::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>::parse(p)?, amount: ::rust_runtime::Z::parse(p)?, tez_amount: std::option::Option::<::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>>::parse(p)?, fee: std::option::Option::<::rust_runtime::Dynamic::<::rust_runtime::u30,std::string::String>>::parse(p)?, gas_limit: std::option::Option::<::rust_runtime::N>::parse(p)?, storage_limit: std::option::Option::<::rust_runtime::Z>::parse(p)?})
}
