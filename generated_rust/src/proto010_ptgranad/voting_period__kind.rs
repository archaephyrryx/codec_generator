#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto010PtGRANADVotingPeriodKind,u8,proto010ptgranadvotingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[allow(dead_code)]
pub fn proto010ptgranadvotingperiodkind_write<U: Target>(val: &Proto010PtGRANADVotingPeriodKind, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadvotingperiodkind_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADVotingPeriodKind> {
    Proto010PtGRANADVotingPeriodKind::parse(p)
}
