#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,N,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADDelegateFrozenBalanceByCyclesDenestDynDenestSeq { pub cycle: i32, pub deposits: ::rust_runtime::N, pub fees: ::rust_runtime::N, pub rewards: ::rust_runtime::N }
pub type Proto010PtGRANADDelegateFrozenBalanceByCycles = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto010PtGRANADDelegateFrozenBalanceByCyclesDenestDynDenestSeq>>;
#[allow(dead_code)]
pub fn proto010ptgranaddelegatefrozenbalancebycycles_write<U: Target>(val: &Proto010PtGRANADDelegateFrozenBalanceByCycles, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto010PtGRANADDelegateFrozenBalanceByCyclesDenestDynDenestSeq>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranaddelegatefrozenbalancebycycles_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADDelegateFrozenBalanceByCycles> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto010PtGRANADDelegateFrozenBalanceByCyclesDenestDynDenestSeq>>::parse(p)?)
}
