#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,N,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADDelegateFrozenBalance { pub deposits: ::rust_runtime::N, pub fees: ::rust_runtime::N, pub rewards: ::rust_runtime::N }
#[allow(dead_code)]
pub fn proto010ptgranaddelegatefrozenbalance_write<U: Target>(val: &Proto010PtGRANADDelegateFrozenBalance, buf: &mut U) -> usize {
    ::rust_runtime::N::write_to(&val.deposits, buf) + ::rust_runtime::N::write_to(&val.fees, buf) + ::rust_runtime::N::write_to(&val.rewards, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranaddelegatefrozenbalance_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADDelegateFrozenBalance> {
    Ok(Proto010PtGRANADDelegateFrozenBalance {deposits: ::rust_runtime::N::parse(p)?, fees: ::rust_runtime::N::parse(p)?, rewards: ::rust_runtime::N::parse(p)?})
}
