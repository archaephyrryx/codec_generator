#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type Proto010PtGRANADGasCost = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn proto010ptgranadgascost_write<U: Target>(val: &Proto010PtGRANADGasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadgascost_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADGasCost> {
    Ok(::rust_runtime::Z::parse(p)?)
}
