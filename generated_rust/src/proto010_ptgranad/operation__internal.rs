#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Padded,ParseResult,Parser,Target,data,resolve_zero,u30};
data!(Proto010PtGRANADContractId,u8,proto010ptgranadcontractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto010PtGRANADContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto010PtGRANADEntrypoint,u8,proto010ptgranadentrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADOperationAlphaInternalOperationDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADOperationAlphaInternalOperationOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADOperationAlphaInternalOperationRevealPublicKey { pub signature_v0_public_key: PublicKey }
data!(Proto010PtGRANADOperationAlphaInternalOperationRhs,u8,proto010ptgranadoperationalphainternaloperationrhs,{
        0 => Reveal  { pub public_key: Proto010PtGRANADOperationAlphaInternalOperationRevealPublicKey },
        1 => Transaction  { pub amount: ::rust_runtime::N, pub destination: Proto010PtGRANADContractId, pub parameters: std::option::Option<Proto010PtGRANADOperationAlphaInternalOperationTransactionParameters> },
        2 => Origination  { pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto010PtGRANADOperationAlphaInternalOperationOriginationDelegate>, pub script: Proto010PtGRANADScriptedContracts },
        3 => Delegation  { pub delegate: std::option::Option<Proto010PtGRANADOperationAlphaInternalOperationDelegationDelegate> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADOperationAlphaInternalOperationTransactionParameters { pub entrypoint: Proto010PtGRANADEntrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto010PtGRANADOperationInternal { pub source: Proto010PtGRANADContractId, pub nonce: u16, pub proto010_ptgranad_operation_alpha_internal_operation_rhs: Proto010PtGRANADOperationAlphaInternalOperationRhs }
#[allow(dead_code)]
pub fn proto010ptgranadoperationinternal_write<U: Target>(val: &Proto010PtGRANADOperationInternal, buf: &mut U) -> usize {
    Proto010PtGRANADContractId::write_to(&val.source, buf) + u16::write_to(&val.nonce, buf) + Proto010PtGRANADOperationAlphaInternalOperationRhs::write_to(&val.proto010_ptgranad_operation_alpha_internal_operation_rhs, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto010ptgranadoperationinternal_parse<P: Parser>(p: &mut P) -> ParseResult<Proto010PtGRANADOperationInternal> {
    Ok(Proto010PtGRANADOperationInternal {source: Proto010PtGRANADContractId::parse(p)?, nonce: u16::parse(p)?, proto010_ptgranad_operation_alpha_internal_operation_rhs: Proto010PtGRANADOperationAlphaInternalOperationRhs::parse(p)?})
}
