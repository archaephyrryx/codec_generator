#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Dynamic,Encode,Estimable,N,ParseResult,Parser,Sequence,Target,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2DelegateFrozenBalanceByCyclesDenestDynDenestSeq { pub cycle: i32, pub deposits: ::rust_runtime::N, pub fees: ::rust_runtime::N, pub rewards: ::rust_runtime::N }
pub type Proto011PtHangz2DelegateFrozenBalanceByCycles = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto011PtHangz2DelegateFrozenBalanceByCyclesDenestDynDenestSeq>>;
#[allow(dead_code)]
pub fn proto011pthangz2delegatefrozenbalancebycycles_write<U: Target>(val: &Proto011PtHangz2DelegateFrozenBalanceByCycles, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto011PtHangz2DelegateFrozenBalanceByCyclesDenestDynDenestSeq>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2delegatefrozenbalancebycycles_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2DelegateFrozenBalanceByCycles> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto011PtHangz2DelegateFrozenBalanceByCyclesDenestDynDenestSeq>>::parse(p)?)
}
