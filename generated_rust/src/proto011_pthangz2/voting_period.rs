#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto011PtHangz2VotingPeriodKind,u8,proto011pthangz2votingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2VotingPeriod { pub index: i32, pub kind: Proto011PtHangz2VotingPeriodKind, pub start_position: i32 }
#[allow(dead_code)]
pub fn proto011pthangz2votingperiod_write<U: Target>(val: &Proto011PtHangz2VotingPeriod, buf: &mut U) -> usize {
    i32::write_to(&val.index, buf) + Proto011PtHangz2VotingPeriodKind::write_to(&val.kind, buf) + i32::write_to(&val.start_position, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2votingperiod_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2VotingPeriod> {
    Ok(Proto011PtHangz2VotingPeriod {index: i32::parse(p)?, kind: Proto011PtHangz2VotingPeriodKind::parse(p)?, start_position: i32::parse(p)?})
}
