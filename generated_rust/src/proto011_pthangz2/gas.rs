#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,Z,data,resolve_zero};
data!(Proto011PtHangz2Gas,u8,proto011pthangz2gas,{
        0 => Limited (pub ::rust_runtime::Z),
        1 => Unaccounted ,
    }
    );
#[allow(dead_code)]
pub fn proto011pthangz2gas_write<U: Target>(val: &Proto011PtHangz2Gas, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2gas_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2Gas> {
    Proto011PtHangz2Gas::parse(p)
}
