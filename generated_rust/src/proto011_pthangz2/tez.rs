#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,N,ParseResult,Parser,Target,resolve_zero};
pub type Proto011PtHangz2Tez = ::rust_runtime::N;
#[allow(dead_code)]
pub fn proto011pthangz2tez_write<U: Target>(val: &Proto011PtHangz2Tez, buf: &mut U) -> usize {
    ::rust_runtime::N::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2tez_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2Tez> {
    Ok(::rust_runtime::N::parse(p)?)
}
