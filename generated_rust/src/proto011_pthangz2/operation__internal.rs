#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Padded,ParseResult,Parser,Target,data,resolve_zero,u30};
data!(Proto011PtHangz2ContractId,u8,proto011pthangz2contractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto011PtHangz2ContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2ContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto011PtHangz2Entrypoint,u8,proto011pthangz2entrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaInternalOperationDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaInternalOperationOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaInternalOperationRevealPublicKey { pub signature_v0_public_key: PublicKey }
data!(Proto011PtHangz2OperationAlphaInternalOperationRhs,u8,proto011pthangz2operationalphainternaloperationrhs,{
        0 => Reveal  { pub public_key: Proto011PtHangz2OperationAlphaInternalOperationRevealPublicKey },
        1 => Transaction  { pub amount: ::rust_runtime::N, pub destination: Proto011PtHangz2ContractId, pub parameters: std::option::Option<Proto011PtHangz2OperationAlphaInternalOperationTransactionParameters> },
        2 => Origination  { pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto011PtHangz2OperationAlphaInternalOperationOriginationDelegate>, pub script: Proto011PtHangz2ScriptedContracts },
        3 => Delegation  { pub delegate: std::option::Option<Proto011PtHangz2OperationAlphaInternalOperationDelegationDelegate> },
        4 => Register_global_constant  { pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaInternalOperationTransactionParameters { pub entrypoint: Proto011PtHangz2Entrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2ScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationInternal { pub source: Proto011PtHangz2ContractId, pub nonce: u16, pub proto011_pthangz2_operation_alpha_internal_operation_rhs: Proto011PtHangz2OperationAlphaInternalOperationRhs }
#[allow(dead_code)]
pub fn proto011pthangz2operationinternal_write<U: Target>(val: &Proto011PtHangz2OperationInternal, buf: &mut U) -> usize {
    Proto011PtHangz2ContractId::write_to(&val.source, buf) + u16::write_to(&val.nonce, buf) + Proto011PtHangz2OperationAlphaInternalOperationRhs::write_to(&val.proto011_pthangz2_operation_alpha_internal_operation_rhs, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2operationinternal_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2OperationInternal> {
    Ok(Proto011PtHangz2OperationInternal {source: Proto011PtHangz2ContractId::parse(p)?, nonce: u16::parse(p)?, proto011_pthangz2_operation_alpha_internal_operation_rhs: Proto011PtHangz2OperationAlphaInternalOperationRhs::parse(p)?})
}
