#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,i31,resolve_zero};
pub type Proto011PtHangz2ScriptLoc = ::rust_runtime::i31;
#[allow(dead_code)]
pub fn proto011pthangz2scriptloc_write<U: Target>(val: &Proto011PtHangz2ScriptLoc, buf: &mut U) -> usize {
    ::rust_runtime::i31::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2scriptloc_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2ScriptLoc> {
    Ok(::rust_runtime::i31::parse(p)?)
}
