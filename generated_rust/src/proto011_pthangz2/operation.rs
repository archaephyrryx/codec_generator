#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Bytes,Decode,Dynamic,Encode,Estimable,N,Nullable,Padded,ParseResult,Parser,Sequence,Target,VPadded,data,resolve_zero,u30};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellContext { pub context_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellOperationsHash { pub operation_list_list_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct BlockHeaderShellPredecessor { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct OperationShellHeaderBranch { pub block_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2BlockHeaderAlphaFullHeader { pub level: i32, pub proto: u8, pub predecessor: BlockHeaderShellPredecessor, pub timestamp: i64, pub validation_pass: u8, pub operations_hash: BlockHeaderShellOperationsHash, pub fitness: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>>>, pub context: BlockHeaderShellContext, pub priority: u16, pub proof_of_work_nonce: ::rust_runtime::ByteString<8>, pub seed_nonce_hash: std::option::Option<Proto011PtHangz2BlockHeaderAlphaUnsignedContentsSeedNonceHash>, pub liquidity_baking_escape_vote: bool, pub signature: Proto011PtHangz2BlockHeaderAlphaSignedContentsSignature }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2BlockHeaderAlphaSignedContentsSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2BlockHeaderAlphaUnsignedContentsSeedNonceHash { pub cycle_nonce: ::rust_runtime::ByteString<32> }
data!(Proto011PtHangz2ContractId,u8,proto011pthangz2contractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto011PtHangz2ContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2ContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto011PtHangz2Entrypoint,u8,proto011pthangz2entrypoint,{
        0 => default ,
        1 => root ,
        2 => r#do ,
        3 => set_delegate ,
        4 => remove_delegate ,
        255 => named (pub ::rust_runtime::Dynamic<u8,std::string::String>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2InlinedEndorsement { pub branch: OperationShellHeaderBranch, pub operations: Proto011PtHangz2InlinedEndorsementContents, pub signature: ::rust_runtime::Nullable<Proto011PtHangz2InlinedEndorsementSignature> }
data!(Proto011PtHangz2InlinedEndorsementContents,u8,proto011pthangz2inlinedendorsementcontents,{
        0 => Endorsement  { pub level: i32 },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2InlinedEndorsementSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
data!(Proto011PtHangz2OperationAlphaContents,u8,proto011pthangz2operationalphacontents,{
        0 => Endorsement  { pub level: i32 },
        1 => Seed_nonce_revelation  { pub level: i32, pub nonce: ::rust_runtime::ByteString<32> },
        2 => Double_endorsement_evidence  { pub op1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto011PtHangz2InlinedEndorsement>, pub op2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto011PtHangz2InlinedEndorsement>, pub slot: u16 },
        3 => Double_baking_evidence  { pub bh1: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto011PtHangz2BlockHeaderAlphaFullHeader>, pub bh2: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto011PtHangz2BlockHeaderAlphaFullHeader> },
        4 => Activate_account  { pub pkh: Proto011PtHangz2OperationAlphaContentsActivateAccountPkh, pub secret: ::rust_runtime::ByteString<20> },
        5 => Proposals  { pub source: Proto011PtHangz2OperationAlphaContentsProposalsSource, pub period: i32, pub proposals: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto011PtHangz2OperationAlphaContentsProposalsProposalsDenestDynDenestSeq>> },
        6 => Ballot  { pub source: Proto011PtHangz2OperationAlphaContentsBallotSource, pub period: i32, pub proposal: Proto011PtHangz2OperationAlphaContentsBallotProposal, pub ballot: i8 },
        10 => Endorsement_with_slot  { pub endorsement: ::rust_runtime::Dynamic<::rust_runtime::u30,Proto011PtHangz2InlinedEndorsement>, pub slot: u16 },
        17 => Failing_noop  { pub arbitrary: ::rust_runtime::Dynamic<::rust_runtime::u30,std::string::String> },
        107 => Reveal  { pub source: Proto011PtHangz2OperationAlphaContentsRevealSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub public_key: Proto011PtHangz2OperationAlphaContentsRevealPublicKey },
        108 => Transaction  { pub source: Proto011PtHangz2OperationAlphaContentsTransactionSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub amount: ::rust_runtime::N, pub destination: Proto011PtHangz2ContractId, pub parameters: std::option::Option<Proto011PtHangz2OperationAlphaContentsTransactionParameters> },
        109 => Origination  { pub source: Proto011PtHangz2OperationAlphaContentsOriginationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub balance: ::rust_runtime::N, pub delegate: std::option::Option<Proto011PtHangz2OperationAlphaContentsOriginationDelegate>, pub script: Proto011PtHangz2ScriptedContracts },
        110 => Delegation  { pub source: Proto011PtHangz2OperationAlphaContentsDelegationSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub delegate: std::option::Option<Proto011PtHangz2OperationAlphaContentsDelegationDelegate> },
        111 => Register_global_constant  { pub source: Proto011PtHangz2OperationAlphaContentsRegisterGlobalConstantSource, pub fee: ::rust_runtime::N, pub counter: ::rust_runtime::N, pub gas_limit: ::rust_runtime::N, pub storage_limit: ::rust_runtime::N, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsActivateAccountPkh { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsAndSignatureSignature { pub signature_v0: ::rust_runtime::ByteString<64> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsBallotProposal { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsBallotSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsDelegationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsDelegationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsOriginationDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsOriginationSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsProposalsProposalsDenestDynDenestSeq { pub protocol_hash: ::rust_runtime::ByteString<32> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsProposalsSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsRegisterGlobalConstantSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsRevealPublicKey { pub signature_v0_public_key: PublicKey }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsRevealSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsTransactionParameters { pub entrypoint: Proto011PtHangz2Entrypoint, pub value: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationAlphaContentsTransactionSource { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2ScriptedContracts { pub code: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>, pub storage: ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes> }
data!(PublicKey,u8,publickey,{
        0 => Ed25519  { pub ed25519_public_key: ::rust_runtime::ByteString<32> },
        1 => Secp256k1  { pub secp256k1_public_key: ::rust_runtime::ByteString<33> },
        2 => P256  { pub p256_public_key: ::rust_runtime::ByteString<33> },
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2Operation { pub branch: OperationShellHeaderBranch, pub contents: ::rust_runtime::VPadded<::rust_runtime::Sequence<Proto011PtHangz2OperationAlphaContents>,64>, pub signature: Proto011PtHangz2OperationAlphaContentsAndSignatureSignature }
#[allow(dead_code)]
pub fn proto011pthangz2operation_write<U: Target>(val: &Proto011PtHangz2Operation, buf: &mut U) -> usize {
    OperationShellHeaderBranch::write_to(&val.branch, buf) + ::rust_runtime::VPadded::<::rust_runtime::Sequence::<Proto011PtHangz2OperationAlphaContents>,64>::write_to(&val.contents, buf) + Proto011PtHangz2OperationAlphaContentsAndSignatureSignature::write_to(&val.signature, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2operation_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2Operation> {
    Ok(Proto011PtHangz2Operation {branch: OperationShellHeaderBranch::parse(p)?, contents: ::rust_runtime::VPadded::<::rust_runtime::Sequence::<Proto011PtHangz2OperationAlphaContents>,64>::parse(p)?, signature: Proto011PtHangz2OperationAlphaContentsAndSignatureSignature::parse(p)?})
}
