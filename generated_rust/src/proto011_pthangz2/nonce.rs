#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto011PtHangz2Nonce = ::rust_runtime::ByteString<32>;
#[allow(dead_code)]
pub fn proto011pthangz2nonce_write<U: Target>(val: &Proto011PtHangz2Nonce, buf: &mut U) -> usize {
    ::rust_runtime::ByteString::<32>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2nonce_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2Nonce> {
    Ok(::rust_runtime::ByteString::<32>::parse(p)?)
}
