#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,Z,resolve_zero};
pub type Proto011PtHangz2GasCost = ::rust_runtime::Z;
#[allow(dead_code)]
pub fn proto011pthangz2gascost_write<U: Target>(val: &Proto011PtHangz2GasCost, buf: &mut U) -> usize {
    ::rust_runtime::Z::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2gascost_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2GasCost> {
    Ok(::rust_runtime::Z::parse(p)?)
}
