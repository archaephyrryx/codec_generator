#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,data,resolve_zero};
data!(Proto011PtHangz2VotingPeriodKind,u8,proto011pthangz2votingperiodkind,{
        0 => Proposal ,
        1 => exploration ,
        2 => Cooldown ,
        3 => Promotion ,
        4 => Adoption ,
    }
    );
#[allow(dead_code)]
pub fn proto011pthangz2votingperiodkind_write<U: Target>(val: &Proto011PtHangz2VotingPeriodKind, buf: &mut U) -> usize {
    val.write_to(buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2votingperiodkind_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2VotingPeriodKind> {
    Proto011PtHangz2VotingPeriodKind::parse(p)
}
