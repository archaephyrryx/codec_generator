#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Bytes,Decode,Dynamic,Encode,ParseResult,Parser,Target,resolve_zero,u30};
pub type Proto011PtHangz2ScriptLazyExpr = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Bytes>;
#[allow(dead_code)]
pub fn proto011pthangz2scriptlazyexpr_write<U: Target>(val: &Proto011PtHangz2ScriptLazyExpr, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2scriptlazyexpr_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2ScriptLazyExpr> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Bytes>::parse(p)?)
}
