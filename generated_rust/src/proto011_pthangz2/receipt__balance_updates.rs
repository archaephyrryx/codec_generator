#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{ByteString,Decode,Dynamic,Encode,Estimable,Padded,ParseResult,Parser,Sequence,Target,data,resolve_zero,u30};
data!(Proto011PtHangz2ContractId,u8,proto011pthangz2contractid,{
        0 => Implicit  { pub signature_v0_public_key_hash: PublicKeyHash },
        1 => Originated (pub ::rust_runtime::Padded<Proto011PtHangz2ContractIdOriginatedDenestPad,1>),
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2ContractIdOriginatedDenestPad { pub contract_hash: ::rust_runtime::ByteString<20> }
data!(Proto011PtHangz2OperationMetadataAlphaBalance,u8,proto011pthangz2operationmetadataalphabalance,{
        0 => Contract  { pub contract: Proto011PtHangz2ContractId, pub change: i64, pub origin: Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin },
        1 => Rewards  { pub delegate: Proto011PtHangz2OperationMetadataAlphaBalanceRewardsDelegate, pub cycle: i32, pub change: i64, pub origin: Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin },
        2 => Fees  { pub delegate: Proto011PtHangz2OperationMetadataAlphaBalanceFeesDelegate, pub cycle: i32, pub change: i64, pub origin: Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin },
        3 => Deposits  { pub delegate: Proto011PtHangz2OperationMetadataAlphaBalanceDepositsDelegate, pub cycle: i32, pub change: i64, pub origin: Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin },
    }
    );
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationMetadataAlphaBalanceDepositsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationMetadataAlphaBalanceFeesDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2OperationMetadataAlphaBalanceRewardsDelegate { pub signature_v0_public_key_hash: PublicKeyHash }
data!(Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin,u8,proto011pthangz2operationmetadataalphaupdateoriginorigin,{
        0 => Block_application ,
        1 => Protocol_migration ,
        2 => Subsidy ,
    }
    );
data!(PublicKeyHash,u8,publickeyhash,{
        0 => Ed25519  { pub ed25519_public_key_hash: ::rust_runtime::ByteString<20> },
        1 => Secp256k1  { pub secp256k1_public_key_hash: ::rust_runtime::ByteString<20> },
        2 => P256  { pub p256_public_key_hash: ::rust_runtime::ByteString<20> },
    }
    );
pub type Proto011PtHangz2ReceiptBalanceUpdates = ::rust_runtime::Dynamic<::rust_runtime::u30,::rust_runtime::Sequence<Proto011PtHangz2OperationMetadataAlphaBalance>>;
#[allow(dead_code)]
pub fn proto011pthangz2receiptbalanceupdates_write<U: Target>(val: &Proto011PtHangz2ReceiptBalanceUpdates, buf: &mut U) -> usize {
    ::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto011PtHangz2OperationMetadataAlphaBalance>>::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2receiptbalanceupdates_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2ReceiptBalanceUpdates> {
    Ok(::rust_runtime::Dynamic::<::rust_runtime::u30,::rust_runtime::Sequence::<Proto011PtHangz2OperationMetadataAlphaBalance>>::parse(p)?)
}
