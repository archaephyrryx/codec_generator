#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto011PtHangz2VoteBallot = i8;
#[allow(dead_code)]
pub fn proto011pthangz2voteballot_write<U: Target>(val: &Proto011PtHangz2VoteBallot, buf: &mut U) -> usize {
    i8::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2voteballot_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2VoteBallot> {
    Ok(i8::parse(p)?)
}
