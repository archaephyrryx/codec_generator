#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,Estimable,ParseResult,Parser,Target,resolve_zero};
#[derive(Debug, Estimable, Clone, PartialEq, PartialOrd, Hash, Encode, Decode)]
pub struct Proto011PtHangz2VoteBallots { pub yay: i32, pub nay: i32, pub pass: i32 }
#[allow(dead_code)]
pub fn proto011pthangz2voteballots_write<U: Target>(val: &Proto011PtHangz2VoteBallots, buf: &mut U) -> usize {
    i32::write_to(&val.yay, buf) + i32::write_to(&val.nay, buf) + i32::write_to(&val.pass, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2voteballots_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2VoteBallots> {
    Ok(Proto011PtHangz2VoteBallots {yay: i32::parse(p)?, nay: i32::parse(p)?, pass: i32::parse(p)?})
}
