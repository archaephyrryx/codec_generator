#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto011PtHangz2Roll = i32;
#[allow(dead_code)]
pub fn proto011pthangz2roll_write<U: Target>(val: &Proto011PtHangz2Roll, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2roll_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2Roll> {
    Ok(i32::parse(p)?)
}
