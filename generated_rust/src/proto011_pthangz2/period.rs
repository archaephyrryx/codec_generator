#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto011PtHangz2Period = i64;
#[allow(dead_code)]
pub fn proto011pthangz2period_write<U: Target>(val: &Proto011PtHangz2Period, buf: &mut U) -> usize {
    i64::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2period_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2Period> {
    Ok(i64::parse(p)?)
}
