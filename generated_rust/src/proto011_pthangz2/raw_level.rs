#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{Decode,Encode,ParseResult,Parser,Target,resolve_zero};
pub type Proto011PtHangz2RawLevel = i32;
#[allow(dead_code)]
pub fn proto011pthangz2rawlevel_write<U: Target>(val: &Proto011PtHangz2RawLevel, buf: &mut U) -> usize {
    i32::write_to(val, buf) + resolve_zero!(buf)
}

#[allow(dead_code)]
pub fn proto011pthangz2rawlevel_parse<P: Parser>(p: &mut P) -> ParseResult<Proto011PtHangz2RawLevel> {
    Ok(i32::parse(p)?)
}
