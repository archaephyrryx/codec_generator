// Invoke using `deno run --allow-net --allow-read main.ts --id=<ID> --hex=<HEXSTRING>`
import { init, WASI } from "https://deno.land/x/wasm@v1.0.2/wasi.ts";
import { parse } from "https://deno.land/std@0.164.0/flags/mod.ts"
import { serve } from "https://deno.land/std@0.164.0/http/server.ts";

await init();

const moduleBytes = Deno.readFileSync('../target/wasm32-wasi/debug/gen.wasm');
const module = await WebAssembly.compile(moduleBytes, {});

const flags = parse(Deno.args, { string: ["id", "hex"] });

const port = 8080;

async function runWasi(id: string, hex: string) {
  if (id === '?' || hex === '?') {
    return `Malformed request; try again.`;
  }

  let wasi = new WASI({
    env: {},
    args: ["", id, hex],
  });

  await wasi.instantiate(module, {});

  // Run the start function
  wasi.start();
  const ret = wasi.getStdoutString();
  return ret;
}

async function handler(request: Request): Promise<Response> {
  const id = request.headers.get("id") ?? '?';
  const hex = request.headers.get("hex") ?? '?';
  const body = await runWasi(id, hex);
  return new Response(body, { status: 200 });
};

console.log(`Deno+WASI webserver running and listening: http://localhost:${port}/`)
await serve(handler, { hostname: "localhost", port });