#!/bin/bash

cd generated_rust/src/
for i in *; do
    if [[ -d $i && ! -f "$i.rs" ]]; then
        ls $i | sed -E 's/^(.*)\.rs/pub mod \\1;/' >>"$i.rs"
    fi
done
