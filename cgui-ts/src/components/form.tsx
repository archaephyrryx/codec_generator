import React, { useState } from 'react';
import styles from './form.module.css'

type ResponseLeft = { tag: 'left', left: string[] }
type ResponseRight = { tag: 'right', right: [] | boolean | number | bigint | string | readonly number[] | Buffer }

type Response = ResponseLeft | ResponseRight

type ExpectedResponse = { codec: string, hex: string, decoding: string }

function isExpectedResponse(response: unknown): response is ExpectedResponse {
    if (response === null) {
        return false;
    }
    if (typeof response === 'object') {
        return true;
    } else {
        return false;
    }
}

function* displayItems(items: string[]) {
    for (const item of items) {
        yield (<><li>{item}</li></>)
    }
}

function displayResponse(response: undefined | any) {
    if (isExpectedResponse(response)) {
        const decoding: Response = JSON.parse(response.decoding);
        if (decoding.tag === 'right') {
            return (<><pre>Decode@{response.codec}({response.hex}) := {`${decoding.right}`}</pre></>);
        }
    }
    return (<>Please Submit a Valid Query</>);
}

function displayHelp(response: undefined | any) {
    if (isExpectedResponse(response)) {
        const decoding: Response = JSON.parse(response.decoding);
        if (decoding.tag === 'left') {
            return (<><p>Valid Codecs:<br/><ul className={styles.ul}>{[...displayItems(decoding.left)]}</ul></p></>);
        }
    }
    return (<><p className={styles.hint}>Hint: Click the <b>?</b> button to list available codecs.</p></>);
}


export function Form() {
    const [codec, setCodec] = useState('');
    const [hex, setHex] = useState('');
    const [response, setResponse] = useState();

    async function handleHelp(event: React.MouseEvent<HTMLButtonElement>) {
        event.preventDefault();

        const help = await fetch('/.netlify/functions/submit', {
            method: 'POST',
            body: JSON.stringify({ codec: "HELP", hex: "" }),
        }).then((res) => res.json());

        setResponse(help);
    }

    async function handleSubmit(event: React.SyntheticEvent<HTMLFormElement>) {
        event.preventDefault();

        if (codec === '' && hex === '') {
            return;
        }

        const res = await fetch('/.netlify/functions/submit', {
            method: 'POST',
            body: JSON.stringify({ codec, hex }),
        }).then((res) => res.json());

        setResponse(res);
        setCodec('');
        setHex('');
    }

    return (
        <>
        <form onSubmit={handleSubmit} className={styles.form}>
            <label htmlFor="codec" className={styles.label}>Codec</label><button className={styles.button} onClick={handleHelp}>?</button>
            <input name="codec" id="codec" className={styles.input} type="text" onChange={(e) => setCodec(e.target.value)}
            value={codec}/>

            <label htmlFor="hex" className={styles.label}>Hex Input</label>
            <input name="hex" id="hex" className={styles.input} type="text" onChange={(e) => setHex(e.target.value)}
            value={hex}/>

            <button className={styles.button}>Submit</button>
        </form>
        <div className={styles.container}>
            {displayResponse(response)}
        </div>
        <div className={styles.container}>
            {displayHelp(response)}
        </div>

        </>
    );
}