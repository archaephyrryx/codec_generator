# cgui-ts

`cgui-ts` is a sample frontend application for the `codec-generator` project, specifically interfacing with TypeScript and Rust-WASM
codecs.

The application itself is written using TypeScript + React, using Vite and Netlify for live updates and state management.

## Implicit Dependencies

In order to run this application in the intended fashion, `netlify-cli` is required.

For the optional Rust-WASM query feature, `deno` is also required; it can be installed
using any of the suggested commands at the [deno installation page][deno-install]

[deno-install]: https://deno.land/manual@v1.28.0/getting_started/installation

### Using `npm`

Though by no means an absolute requirement, `npm` is the recommended tool for bootstrapping application setup.

Before first use, the following commands should be run with `cgui-ts` as the current working directory.

```sh
npm i
npm i -g netlify-cli@latest
```

## Running the Application

Assuming that `netlify-cli` has been installed as detailed above, this
application can be launched by running `ntl dev` in this directory.

### With Rust-WASM

Assuming that `rustup` is installed, the following commands can be run to configure and launch the Deno WASI server:

Unix (macOS + Linux):

```sh
rustup update
rustup target add wasm32-wasi
cd ../generated_rust
cargo build --target wasm32-wasi
cd ../wasm_runtime
deno run --allow-net --allow-read main.ts &
```

Once the Deno WASM server is running, `cgui-ts` should be able to make Rust codec queries.

Note that limited support around Rust queries is provided.

## Using the Frontend

On application launch, a browser window pointing to a localhost port (usually `8000`) should open.

The main, and only page of the application, consists of an HTML form with two output boxes below.

### TypeScript Codec Samples

In the current implementation, a limited set of `ts_runtime`-native codec types are supported for
querying. This list can be viewed by pressing the `?` button near the `Codec` entry field of the form. All such queries must be entered verbatim into the text-input for Codec, and a
hex-string to be decoded as that codec type should be entered into the `Hex` box below.

Pressing submit will commit the query, and if it is well-formed and valid, the resulting decoding will be rendered in the first container, with each text-input automatically cleared.

If the query is for a non-existent codec, the help-list of
valid codecs will be displayed in the second container instead.

Due to how application state is managed, pressing the `?` button will cause any output stored in the first container
to be cleared as a side-effect, but will not change the unsubmitted input in the query text fields.

### Rust-WASM Queries

Rust-WASM queries can only be made if the Deno-WASM app is running. If it is, any Codec identifier that begins with `rust#` will be treated as a query to this service, and the remainder of the identifier after this prefix will be passed
to the Deno-WASM server using an HTTP POST request.

If the query is malformed or invalid, the results of making a rust query may be unpredictable, and may crash the application; this is not expected to occur for TypeScript-native queries, however.

A comprehensive list of valid codecs for the Rust-WASM server is not yet available, but may be provided in future versions.
