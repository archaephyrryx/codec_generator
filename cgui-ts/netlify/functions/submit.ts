import { Handler } from '@netlify/functions';
import { request } from 'http'
import { MVar } from 'mvar';

import { dynamic_reader } from '../../../ts_runtime/composite/dynamic';
import width from '../../../ts_runtime/core/width.type';
import { double_reader } from '../../../ts_runtime/float/double';
import {
    int16_reader,
    int31_reader,
    int32_reader,
    int64_reader,
    int8_reader,
    uint16_reader,
    uint30_reader,
    uint8_reader
} from '../../../ts_runtime/integer/integer';
import { boolean_reader } from '../../../ts_runtime/primitive/bool';
import { Bytes, bytes_reader } from '../../../ts_runtime/primitive/bytes';
import { fixedbytes_reader } from '../../../ts_runtime/primitive/bytes.fixed';
import { U8String, u8string_reader } from '../../../ts_runtime/primitive/string';
import { fixedstring_reader } from '../../../ts_runtime/primitive/string.fixed';
import { unit_reader } from '../../../ts_runtime/primitive/unit';
import { z_reader } from '../../../ts_runtime/zarith/integer';
import { n_reader } from '../../../ts_runtime/zarith/natural';

enum Codecs {
    Unit = 'Unit',
    Bool = 'Bool',
    Uint8 = 'Uint8',
    Uint16 = 'Uint16',
    Uint30 = 'Uint30',
    Int8 = 'Int8',
    Int16 = 'Int16',
    Int31 = 'Int31',
    Int32 = 'Int32',
    Int64 = 'Int64',
    Double = 'Double',
    N = 'N',
    Z = 'Z',
    FixedString32 = 'FixedString32',
    FixedBytes32 = 'FixedBytes32',
    U8String = 'U8String',
    Dyn30U8String = 'Dyn30U8String',
    Bytes = 'Bytes',
    Dyn30Bytes = 'Dyn30Bytes',
}

function isCodec(maybeCodec: string): maybeCodec is Codecs {
    return Object.values(Codecs).includes(Codecs[maybeCodec]);
}

type ResponseLeft = { tag: 'left', left: string[] }
type ResponseRight = { tag: 'right', right: [] | boolean | number | bigint | string | readonly number[] | Buffer }

type Response = ResponseLeft | ResponseRight

function decode(codec: string, hex: string): Response {
    if (isCodec(codec)) {
        switch (codec) {
            case Codecs.Unit: return { tag: 'right', right: unit_reader(hex) };
            case Codecs.Bool: return { tag: 'right', right: boolean_reader(hex) };
            case Codecs.Uint8: return { tag: 'right', right: uint8_reader(hex) };
            case Codecs.Uint16: return { tag: 'right', right: uint16_reader(hex) };
            case Codecs.Uint30: return { tag: 'right', right: uint30_reader(hex) };
            case Codecs.Int8: return { tag: 'right', right: int8_reader(hex) };
            case Codecs.Int16: return { tag: 'right', right: int16_reader(hex) };
            case Codecs.Int31: return { tag: 'right', right: int31_reader(hex) };
            case Codecs.Int32: return { tag: 'right', right: int32_reader(hex) };
            case Codecs.Int64: return { tag: 'right', right: int64_reader(hex) };
            case Codecs.Double: return { tag: 'right', right: double_reader(hex) };
            case Codecs.N: return { tag: 'right', right: n_reader(hex) };
            case Codecs.Z: return { tag: 'right', right: z_reader(hex) };
            case Codecs.FixedBytes32: return { tag: 'right', right: fixedbytes_reader({ len: 32 })(hex) };
            case Codecs.Bytes: return { tag: 'right', right: bytes_reader(hex) };
            case Codecs.FixedString32: return { tag: 'right', right: fixedstring_reader({ len: 32 })(hex) };
            case Codecs.U8String: return { tag: 'right', right: u8string_reader(hex) };
            case Codecs.Dyn30U8String: return { tag: 'right', right: dynamic_reader(U8String.decode, width.Uint30)(hex).value };
            case Codecs.Dyn30Bytes: return { tag: 'right', right: dynamic_reader(Bytes.decode, width.Uint30)(hex).value };
        }
    } else {
        return { tag: 'left', left: Object.entries(Codecs).map(([x, _]) => x) };
    }
}

export const handler: Handler = async (event) => {
    const { codec, hex }: { codec: string, hex: string } = JSON.parse(event.body);

    if (codec.startsWith('rust#')) {
        const cell: MVar<string> = MVar.newEmpty();

        const id = codec.substring(5);
        const req = request(
            {
                host: 'localhost',
                port: '8080',
                path: '/',
                method: 'POST',
                headers: {
                    'content-type': 'application/json',
                    'id': id,
                    'hex': hex,
                }
            }, response => {
                const chunks = new Array();
                response.on('data', (chunk: Uint8Array) => {
                    chunks.push(chunk);
                });
                response.on('end', () => {
                    const result = Buffer.concat(chunks).toString();
                    console.log(result);
                    cell.put(result);
                });
            });
        req.end();
        const decoding = await cell.read();
        return {
            statusCode: 200,
            body: JSON.stringify({
                codec,
                hex,
                decoding: JSON.stringify({ tag: 'right', 'right': decoding }),
            }),
        }
    }

    return {
        statusCode: 200,
        body: JSON.stringify({
            codec,
            hex,
            decoding: JSON.stringify(decode(codec, hex)),
        }),
    }
}

