//! Auto-generated codec module for custom schema `Tree`
#![allow(unused_imports)]
extern crate rust_runtime;
use rust_runtime::{AutoBox,Decode,Encode,Estimable,ParseResult,Parser,Target,data};
data!(Forest,u8,forest,{
        0 => Empty ,
        1 => Children  { tree: ::rust_runtime::AutoBox<Tree>, rest: ::rust_runtime::AutoBox<Forest> },
    }
    );
data!(Tree,u8,tree,{
        0 => Leaf ,
        1 => Node  { first: ::rust_runtime::AutoBox<Tree>, forest: Forest },
    }
    );
pub fn tree_write<U: Target>(val: &Tree, buf: &mut U) -> usize {
    Tree::write_to(val, buf)
}

pub fn tree_parse<P: Parser>(p: &mut P) -> ParseResult<Tree> {
    Ok(Tree::parse(p)?)
}
fn main() {
    println!("{:?}", rust_runtime::Builder::into_hex(Tree::encode::<rust_runtime::StrictBuilder>(&Tree::Leaf(tree::Leaf))));
}