SRC_DIR := src
SRCS := $(shell find $(SRC_DIR) -name '*.ml')
SUBLIBS := $(shell ls -F $(SRC_DIR) | grep '/')
SUBCHECKS := $(foreach libdir,$(SUBLIBS),@src/$(libdir)check)

rust-tgts: $(find generated_rust/src -name '*.rs')
rust-sublibs: $(find generated_rust/src -type d)
rust-rts: $(find rust_runtime/src -name '*.rs')
ts-rts: $(find ts_runtime -name '*.ts')
test-ml: "test/rust_main.ml"

interactive: $(compiler-ml)
	@dune utop --root=.

rustgen-autoindex: $(rust-sublibs)
	@. scripts/rustgen_autoindex.sh

generate: $(compiler-ml) $(test-ml)
	@dune build @runtest --root=.

build: $(rust-tgts)
	@cargo build --release

build-deps:
	@opam update
	@opam install dune pprint zarith
	@opam pin data-encoding.dev https://gitlab.com/archaephyrryx/data-encoding#base-patch
	@opam upgrade

build-dev-deps:
	@opam install utop merlin ocaml-lsp-server ocamlformat-rpc

install: $(compiler-ml)
	@dune install codec-generator

PHONY: %/check
%/check:
	@dune build $@

selfcheck: $(SUBCHECKS) @test/check

rustcheck: $(rust-tgts) $(rust-rts)
	@cargo check

tscheck: $(ts-rts)
	@npx tsc --noEmit

format: $(compiler-ml)
	@dune build @fmt --auto-promote --root=. | true

fmt: format
rustgen: generate
check: selfcheck rustcheck
