# Watermarks for Support

This is a portable milestone-tracker intended to aid in both
self-tracking of ongoing support for a target language, and
providing information to potential users as to the ongoing
status of support for a target language.

The categories given below are not a direct linear flow of progress,
but measure interleaved tasks along several fronts. Therefore, some
may be completed out-of-order or in different order between languages.

## General Bootstrapping

The following is a list of items, statements, and goals that should
proceed in mostly linear order, though some things may be prioritized
or handled in variable order for specific languages.

* [x]  Language has been chosen (:= **Rust**)
* [x]  Relevant language features have been studied
* [x]  Gen/AST directory has been created
* [x]  Runtime library directory has been created
* [x]  Overall style of the runtime library has been considered through practice
* [x]  Gen/AST support has been extended to at least one schema case
* [x]  Fundamental features (parser logic, Encoder/Decoder model) has been implemented in runtime
* [x]  Support in `compiler_lib` for targeting the language
* [x]  Target output directory has been created
* [x]  Simple-enough schema yields at least a plausible skeleton of a codec
* [x]  At least one codec compiles
* [x]  At least one codec behaves correctly (moving target based on [testing](#testing))
* [x]  Support includes all combinatorial cases, at least theoretically
* [x]  Codecs can be compiled from Octez schemas to the prospective target
* [x]  Generated Octez codecs are free of unanticipated compiler errors (moving target based on version updates)
* [x]  Tooling implemented for running language-native code using codecs and runtime
* [ ]  All known bugs have been fixed (moving target based on bug identification)

## Extended Tooling

* [ ]  Integrate a testing pipeline into the project CI
  * [x] CI runner for sanity checking of OCaml pipeline, including new target
  * [ ] CI runner for compiling and testing per-language runtime and generated codecs

## Runtime Support

* [x]  Implement support for a single, simple case (:= `UInt8` ~ `u8`)
* [x]  Extend support to the full set of trivial cases
  * [x]  All integral types supported by `data-encoding`
  * [x]  Booleans
  * [x]  Double-precision floating point numbers (full and range-restricted)
  * [x]  Fixed-length charstrings
  * [x]  Fixed-length bytestrings
* [x] Extend support to the full set of 'simple' cases
  * [x] Nullary enums
  * [x] Dynamic-size charstrings
  * [x] Dynamic-size bytestrings
  * [x] Fixed- or dynamic-size list/array
  * [x] Arbitrary precision integrals/naturals (Zarith `N` and `Z`)
* [x] Extend support to non-simple cases
  * [x] Tuples with arity `>= 2`
    * [x] Tuples whose elements are all simple
    * [x] Tuples containing non-simple positional arguments
  * [x] Record types
    * [x] Records whose field types are all simple
    * [x] Records whose field types are not all simple
  * [x] Union types
* [x] Extend support to all cases
  * [x] `Mu` (self-recursive types)
* [ ] Total support for under-implemented cases
  * [ ] `Check_size` handling
* [x] Roundtrip checking for sample encodings
* [x] Test against live Tezos chain samples
* [ ] Add PBT for as many codecs as possible

## Generator Logic

* [x] Add support for Base schema types (as defined in `Common.Simplified`):
  * [x] `Unit`
  * [x] `Bool`
  * [x] `Num`
    * [x] `Int`
      * [x] `NativeInt`
        * [x] `Uint8`, `Int8`
        * [x] `Uint16`, `Int16`
      * [x] `Uint30`,`Int31`
        * [x] `Int32`
        * [x] `Int64`
      * [x] `RangedInt`
    * [x] `Float`
      * [x] `Double`
      * [x] `RangedDouble`
  * [x] `Fixed`
    * [x] `String`
    * [x] `Bytes`
  * [x] `Var`
    * [x] `VString`
    * [x] `VBytes`
  * [x] `Zarith` types
    * [x] `ZarithZ`
    * [x] `ZarithN`
  * [x] `NamedRef`
    * [x] `Abstract`
    * [x] `Concrete`
* [x] Add support for Composite schema types
  * [x] `Opt`
    * [x] With one-byte tag
    * [x] Without one-byte tag
  * [x] `Seq`
    * [x] `No_limit`
    * [x] `At_most`
    * [x] `Exactly`
  * [x] `Padded`
  * [x] Dynamic types
    * [x] `Dyn`
    * [x] `VPadded`
* [x] Add support for Product schema types
  * [x] Tuples
    * [x] 2-tuple, 3-tuple, etc.
    * [x] N-tuple
  * [x] Records
    * [x] 1-field, 2-field, etc.
    * [x] N-field
* [x] Add support for Sum schema types
  * [x] `CStyle`
  * [x] `Data`

## Testing

* [x] Unit tests have been implemented for critical runtime code
* [x] Sample codecs have been written to serve as practical test cases for the runtime framework
* [x] Sample codecs have been generated to serve as practical test cases for the compiler framework
* [x] Support has been added for either emitting separate test modules, or injecting test cases into generated modules
* [x] Live Octez test cases have been run against compiled codecs
* [ ] Property-based testing framework has been introduced

## Known Bugs

The following is a list of known bugs or issues related specifically to the
implemented support for the target language in question. It will start out
empty, but will gradually be added to as new bugs are found, and will hopefully
be resolved as they are patched.

* [x] FixSeq should terminate after N iterations even if output remains (commit hash:
[f534e222](https://gitlab.com/archaephyrryx/codec_generator/-/commit/f534e2226e4c539cd713dd2b615f674d63eb6c6c)

* [x] Positive-minimum RangedInt should have consistent notion of transparent-value or transparent-encoding (commit hash: [3f788f2c](https://gitlab.com/archaephyrryx/codec_generator/-/commit/3f788f2c2de6d1c2f3e5e3a6df7f49cca8fed999))
