# Watermarks for Support

This is a portable milestone-tracker intended to aid in both
self-tracking of ongoing support for a target language, and
providing information to potential users as to the ongoing
status of support for a target language.

The categories given below are not a direct linear flow of progress,
but measure interleaved tasks along several fronts. Therefore, some
may be completed out-of-order or in different order between languages.

## General Bootstrapping

The following is a list of items, statements, and goals that should
proceed in mostly linear order, though some things may be prioritized
or handled in variable order for specific languages.

* [x]  Language has been chosen (:= **Typescript**)
* [x]  Relevant language features have been studied
* [x]  Gen/AST directory has been created
* [x]  Runtime library directory has been created
* [x]  Overall style of the runtime library has been considered through practice
* [x]  Gen/AST support has been extended to at least one schema case
* [x]  Fundamental features (parser logic, Encoder/Decoder model) has been implemented in runtime
* [x]  Support in `compiler_lib` for targeting the language
* [x]  Target output directory has been created
* [x]  Simple-enough schema yields at least a plausible skeleton of a codec
* [ ]  At least one codec compiles
* [ ]  At least one codec behaves correctly (moving target based on [testing](#testing))
* [ ]  Support includes all combinatorial cases, at least theoretically
* [ ]  Codecs can be compiled from Octez schemas to the prospective target
* [ ]  Generated Octez codecs are free of unanticipated compiler errors (moving target based on version updates)
* [ ]  Tooling implemented for running language-native code using codecs and runtime
* [ ]  All known bugs have been fixed (moving target based on bug identification)

## Extended Tooling

* [ ]  Integrate a testing pipeline into the project CI
  * [x] CI runner for sanity checking of OCaml pipeline, including new target
  * [ ] CI runner for compiling and testing per-language runtime and generated codecs

## Runtime Support

* [x] Implement fundamental model
  * [x] Encoding model
    * [x] Low-level serialization
    * [x] Type-associated structural encoder model
  * [x] Decoding model
    * [x] Low-level parsing
    * [x] Type-associated structural decoder model
* [x] Add core support for Base schema types
  * [x] `Unit`
  * [x] `Bool`
  * [x] `Num`
    * [x] `Int`
      * [x] `NativeInt`
        * [x] `Uint8`, `Int8`
        * [x] `Uint16`, `Int16`
      * [x] `Uint30`,`Int31`
        * [x] `Int32`
        * [x] `Int64`
      * [x] `RangedInt`
    * [x] `Float`
      * [x] `Double`
      * [x] `RangedDouble`
  * [x] `Fixed`
    * [x] `String`
    * [x] `Bytes`
  * [x] `Var`
    * [x] `VString`
    * [x] `VBytes`
  * [x] `Zarith` types
    * [x] `ZarithZ`
    * [x] `ZarithN`
* [x] Add core support for Composite schema types
  * [x] `Opt`
    * [x] With one-byte tag
    * [x] Without one-byte tag
  * [x] `Seq`
    * [x] `No_limit`
    * [x] `At_most`
    * [x] `Exactly`
  * [x] `Padded`
  * [x] Dynamic types
    * [x] `Dyn`
    * [x] `VPadded`
* [x] Add support for Product schema types
  * [x] Tuples
    * [x] Tuples over at least 2 'simple' types
    * [x] Tuples over N 'simple' types
    * [x] Tuples over at least 2 'complex' types
    * [x] N-tuple over any type
  * [x] Records
    * [x] Records over at least 1 'simple' field-type
    * [x] Records over N 'simple' field-types
    * [x] Records over at least 1 'complex' field-type
    * [x] N-field Records over any field-type
* [x] Add support for Sum schema types
  * [x] `CStyle`
  * [x] `Data`

## Generator Logic

* [x] Add support for Base schema types (as defined in `Common.Simplified`):
  * [x] `Unit`
  * [x] `Bool`
  * [x] `Num`
    * [x] `Int`
      * [x] `NativeInt`
        * [x] `Uint8`, `Int8`
        * [x] `Uint16`, `Int16`
      * [x] `Uint30`,`Int31`
        * [x] `Int32`
        * [x] `Int64`
      * [x] `RangedInt`
    * [x] `Float`
      * [x] `Double`
      * [x] `RangedDouble`
  * [x] `Fixed`
    * [x] `String`
    * [x] `Bytes`
  * [x] `Var`
    * [x] `VString`
    * [x] `VBytes`
  * [x] `Zarith` types
    * [x] `ZarithZ`
    * [x] `ZarithN`
  * [ ] `NamedRef`
    * [x] `Abstract`
    * [ ] `Concrete`
* [x] Add support for Composite schema types
  * [x] `Opt`
    * [x] With one-byte tag
    * [x] Without one-byte tag
  * [x] `Seq`
    * [x] `No_limit`
    * [x] `At_most`
    * [x] `Exactly`
  * [x] `Padded`
  * [x] Dynamic types
    * [x] `Dyn`
    * [x] `VPadded`
* [ ] Add support for Product schema types
  * [ ] Tuples
    * [ ] 2-tuple, 3-tuple, etc.
    * [ ] N-tuple
  * [ ] Records
    * [ ] 1-field, 2-field, etc.
    * [ ] N-field
* [ ] Add support for Sum schema types
  * [ ] `CStyle`
  * [ ] `Data`

## Testing

* [x] Unit tests have been implemented for critical runtime code
* [ ] Sample codecs have been written to serve as practical test cases for the runtime framework
* [ ] Sample codecs have been generated to serve as practical test cases for the compiler framework
* [ ] Support has been added for either emitting separate test modules, or injecting test cases into generated modules
* [ ] Live Octez test cases have been run against compiled codecs
* [ ] Property-based testing framework has been introduced

## Known Bugs

The following is a list of known bugs or issues related specifically to the
implemented support for the target language in question. It will start out
empty, but will gradually be added to as new bugs are found, and will hopefully
be resolved as they are patched.
