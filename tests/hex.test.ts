import { HexString } from "../ts_runtime/util/hex";
import * as fc from 'fast-check';

describe('fromString(Raw) sanity', () => {
    test('rejects invalid strings', () => {
        expect(() => HexString.fromString("not actually hex")).toThrow();
        expect(() => HexString.fromString("12345")).toThrow();
        expect(() => HexString.fromString("a")).toThrow();
        expect(() => HexString.fromString("deadbees")).toThrow();
    });

    test('accepts valid strings', () => {
        fc.assert(fc.property(fc.hexaString().filter(x => (x.length % 2) == 0),
            (str) => { HexString.fromString(str).byteLength === (str.length / 2) }));
    });

    test('roundtrip equality', () => {
        fc.assert(fc.property(fc.hexaString().filter(x => (x.length % 2) == 0),
            (str) => { HexString.fromString(str).toHex() === str.toLowerCase() }
        ))
    });
})