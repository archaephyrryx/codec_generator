import { Bytes } from "../ts_runtime/primitive/bytes";
import * as fc from 'fast-check';
import { liftDecoder } from '../ts_runtime/decode';

describe('Bytes', () => {
    test('roundtrip', () => {
        const constraints: fc.IntArrayConstraints = { minLength: 0, maxLength: 32 };
        fc.assert(fc.property(fc.uint8Array(constraints), arr => {
            const bytes = Bytes.fromArray(arr);
            liftDecoder(Bytes.decode)(bytes.encode()).equals(bytes);
        }));
    });
});
