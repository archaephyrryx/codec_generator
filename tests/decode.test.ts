import { toParser, Input } from '../ts_runtime/decode'
import { Parser } from '../ts_runtime/parse'
import { ByteParser } from '../ts_runtime/parse/byteparser'

describe('toParser sanity', () => {
    const inpStr: Input = '00ffdeadbeef'
    const inpArr: Input = [0x00, 0xff, 0xde, 0xad, 0xbe, 0xef]
    const inpU8A: Input = new Uint8Array(inpArr)
    const inpPrs: Input = ByteParser.fromBuffer(inpU8A)

    test('toParser@Parser identity', () => {
        expect(toParser(inpPrs)).toEqual(inpPrs)
    })

    test('toParser@!Parser equivalence', () => {
        expect(toParser(inpStr)).toStrictEqual(inpPrs)
        expect(toParser(inpArr)).toStrictEqual(inpPrs)
        expect(toParser(inpU8A)).toStrictEqual(inpPrs)
    })
})