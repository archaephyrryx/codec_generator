import { fromBytes, toBytes } from '../ts_runtime/core/ops'
import * as intKinds from '../ts_runtime/integer/kinds'
import * as fc from 'fast-check'
import { PrimInt64, PrimInt8, PrimUint16, PrimUint30, PrimUint8 } from '../ts_runtime/integer/types'
import { forceNum } from '../ts_runtime/core/safe'

const arr_eq = <T>(operand: readonly T[], template: readonly T[]): boolean => {
    return (operand.length === template.length && operand.every((val, ix) => val === template[ix]))
}

describe('toBytes/fromBytes@uint8', () => {
    const bounds: fc.IntegerConstraints = { ...PrimUint8.bounds }
    const kind = intKinds.Uint8Kind
    test('correctness', () => {

        fc.assert(fc.property(fc.integer(bounds),
            i => {
                const bytes = toBytes({ ...kind, value: i })
                const j: number = forceNum(fromBytes(kind, new Uint8Array(bytes)))
                return arr_eq(bytes, [i]) && i === j
            }
        ))
    })
})


describe('toBytes/fromBytes@int8', () => {
    const bounds: fc.IntegerConstraints = { ...PrimInt8.bounds }
    const kind = intKinds.Int8Kind

    test('correctness', () => {
        fc.assert(fc.property(fc.integer(bounds),
            i => {
                const bytes = toBytes({ ...kind, value: i })
                const j: number = fromBytes(kind, new Uint8Array(bytes))
                return i === j
            }
        ))
    })
})

describe('toBytes/fromBytes@uint16', () => {
    const constraints: fc.ArrayConstraints = { minLength: 2, maxLength: 2 }
    const bounds: fc.IntegerConstraints = { ...PrimUint16.bounds }
    const kind = intKinds.Uint16Kind

    test('identity (arbitrary array)', () => {
        fc.assert(fc.property(fc.array(fc.integer({ min: 0, max: 255 }), constraints),
            arr => {
                const val = (arr[0] << 8) | arr[1]
                const bytes = toBytes({ ...kind, value: val })
                return arr_eq(bytes, arr) && fromBytes(kind, new Uint8Array(bytes)) === val
            }
        ))
    })

    test('identity (arbitrary number)', () => {
        fc.assert(fc.property(fc.integer(bounds),
            value => {
                const bytes = toBytes({ ...kind, value })
                return fromBytes(kind, new Uint8Array(bytes)) === value
            }
        ))
    })
})

describe('toBytes/fromBytes@uint30', () => {
    const constraints: fc.ArrayConstraints = { minLength: 4, maxLength: 4 }
    const kind = intKinds.Uint30Kind
    const bounds: fc.IntegerConstraints = { min: PrimUint30.bounds.min, max: PrimUint30.bounds.max }

    test('identity (arbitrary array)', () => {
        fc.assert(fc.property(fc.array(fc.integer({ min: 0, max: 255 }), constraints).filter(arr => arr[0] <= 0x3f),
            arr => {
                const val = (arr[0] << 24) | (arr[1] << 16) | (arr[2] << 8) | arr[3]
                const bytes = toBytes({ ...kind, value: val })
                return arr_eq(bytes, arr) && fromBytes(kind, new Uint8Array(bytes)) === val
            }
        ))
    })

    test('identity (arbitrary number)', () => {
        fc.assert(fc.property(fc.integer(bounds),
            val => {
                const bytes = toBytes({ ...kind, value: val })
                return fromBytes(kind, new Uint8Array(bytes)) === val
            }
        ))
    })
})


describe('toBytes/fromBytes@int64', () => {
    // const constraints: fc.ArrayConstraints = { minLength: 8, maxLength: 8 }
    const kind = intKinds.Int64Kind
    const bounds: fc.BigIntConstraints = { min: PrimInt64.bounds.min, max: PrimInt64.bounds.max }

    test('identity (arbitrary number)', () => {
        fc.assert(fc.property(fc.bigInt(bounds),
            val => {
                const bytes = toBytes({ ...kind, value: val })
                return fromBytes(kind, new Uint8Array(bytes)) === val
            }
        ))
    })
})