import { Dynamic } from "../ts_runtime/composite/dynamic";
import width from "../ts_runtime/core/width.type";
import { liftDecoder } from "../ts_runtime/decode";
import { UNIT, Unit } from "../ts_runtime/primitive/unit";
// Test file for dynamic

describe('Dynamic prefix', () => {
    test('unit payload', () => {
        expect(liftDecoder(Dynamic.decode(Unit.decode, width.Uint8))("00").value).toEqual(UNIT);
    })
})