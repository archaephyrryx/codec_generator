import { FixedString, fixedstring_reader } from '../ts_runtime/primitive/string.fixed';
import * as fc from 'fast-check';
import { strByteLen } from '../ts_runtime/util/strops';

describe('fixedstring test', () => {
    test('roundtrip', () => {
        const strbounds: fc.StringSharedConstraints = { minLength: 1, maxLength: 32 };
        fc.assert(fc.property(
            fc.unicodeString(strbounds),
            str => {
                const fixStr = FixedString.promote(str);
                return fixedstring_reader({ len: strByteLen(str) })(fixStr.encode()) === str
            }
        ))
    });
})