import { Parser, TypedParser } from '../ts_runtime/parse'
import { ByteParser } from '../ts_runtime/parse/byteparser'
import { HexString } from '../ts_runtime/util/hex'
import { bool_type } from '../ts_runtime/primitive'
import * as integer from '../ts_runtime/integer/integer'
import { unwrap, extract, Ok, mapOk } from '../ts_runtime/core/result'

describe('parse', () => {
    test('enforces parity', () => {
        expect(() => ByteParser.fromStringHex('abcde')).toThrow()
    })

    test('enforces alphabet', () => {
        expect(() => ByteParser.fromStringHex('nonsense')).toThrow()
    })

    test('case-insensitive', () => {
        const lower = ByteParser.fromStringHex('abcdef0a1b2c')
        const upper = ByteParser.fromStringHex('ABCDEF0A1B2C')
        const mixed = ByteParser.fromStringHex('AbCdEf0A1b2C');

        [1, 2, 3].forEach(width => {
            const x = lower.consume(width)
            const y = upper.consume(width)
            const z = mixed.consume(width)

            expect(x).toStrictEqual(y)
            expect(y).toStrictEqual(z)
        })
    })
})

describe('consume', () => {
    test('disallows backtracking', () => {
        const p: ByteParser = ByteParser.fromStringHex('00000000')
        expect(() => p.consume(-1)).toThrow()
    })

    test('prevents overrun', () => {
        const p: ByteParser = ByteParser.fromStringHex('00000000')
        expect(() => p.consume(8)).toThrow()
    })
})

describe('integral get methods', () => {
    test('takeUint8', () => {
        const p = ByteParser.fromStringHex('010280ff')
        expect(p.viewLen).toBe(4)
        expect(unwrap(p.takePrimUint8()).value).toBe(1)
        expect(unwrap(p.takePrimUint8()).value).toBe(2)
        expect(unwrap(p.takePrimUint8()).value).toBe(2 ** 7)
        expect(unwrap(p.takePrimUint8()).value).toBe((2 ** 8) - 1)
    })

    test('get_multi_int8', () => {
        var p = ByteParser.fromStringHex('010280ff');
        expect(p.viewLen).toBe(4);
        expect(unwrap(p.takePrimInt8()).value).toBe(1);
        expect(p.takeInt8()).toStrictEqual(Ok(2));
        expect(p.takeInt8()).toStrictEqual(Ok(-(2 ** 7)));
        expect(p.takeInt8()).toStrictEqual(Ok(-1));
    });

    test('get_multi_uint16', () => {
        var p = ByteParser.fromStringHex("000100028000ffff");
        expect(p.viewLen).toBe(8);
        expect(p.takeUint16()).toStrictEqual(Ok(1));
        expect(p.takeUint16()).toStrictEqual(Ok(2));
        expect(p.takeUint16()).toStrictEqual(Ok(2 ** 15));
        expect(p.takeUint16()).toStrictEqual(Ok((2 ** 16) - 1));
    });

    test('get_multi_int16', () => {
        var p = ByteParser.fromStringHex("000100028000ffff");
        expect(p.viewLen).toBe(8);
        expect(p.takeInt16()).toStrictEqual(Ok(1));
        expect(p.takeInt16()).toStrictEqual(Ok(2));
        expect(p.takeInt16()).toStrictEqual(Ok(-(2 ** 15)));
        expect(p.takeInt16()).toStrictEqual(Ok(-1));
    });

    test('get_multi_uint30', () => {
        var p = ByteParser.fromStringHex("00000001" + "00000002" + "3fffffff" + "40000000");
        expect(p.viewLen).toBe(16);
        expect(p.takeUint30()).toStrictEqual(Ok(1));
        expect(p.takeUint30()).toStrictEqual(Ok(2));
        expect(p.takeUint30()).toStrictEqual(Ok(2 ** 30 - 1));
        expect(() => p.takeUint30()).toThrow();
    });

    test('get_multi_int31', () => {
        var p = ByteParser.fromStringHex("00000001" + "00000002" + "c0000000" + "ffffffff");
        expect(p.viewLen).toBe(16);
        expect(p.takeInt31()).toStrictEqual(Ok(1));
        expect(p.takeInt31()).toStrictEqual(Ok(2));
        expect(p.takeInt31()).toStrictEqual(Ok(-(2 ** 30)));
        expect(p.takeInt31()).toStrictEqual(Ok(-1));
    });


    test('get_multi_int32', () => {
        var p = ByteParser.fromStringHex("00000001" + "00000002" + "80000000" + "ffffffff");
        expect(p.viewLen).toBe(16);
        expect(p.takeInt32()).toStrictEqual(Ok(1));
        expect(p.takeInt32()).toStrictEqual(Ok(2));
        expect(p.takeInt32()).toStrictEqual(Ok(-(2 ** 31)));
        expect(p.takeInt32()).toStrictEqual(Ok(-1));
    });


    test('get_multi_int64', () => {
        var p = ByteParser.fromStringHex("0000000000000001" + "0000000000000002" + "8000000000000000" + "ffffffffffffffff");
        expect(p.viewLen).toBe(32);
        expect(p.takeInt64()).toStrictEqual(Ok(1n));
        expect(p.takeInt64()).toStrictEqual(Ok(2n));
        expect(p.takeInt64()).toStrictEqual(Ok(-(2n ** 63n)));
        expect(p.takeInt64()).toStrictEqual(Ok(-1n));
    });
})

test('takeBoolean', () => {
    const p = ByteParser.fromStringHex('00ffabcd')
    expect(p.takeBoolean()).toStrictEqual(Ok(false))
    expect(p.takeBoolean()).toStrictEqual(Ok(true))
    expect(() => unwrap(p.takeBoolean())).toThrow()
    expect(() => unwrap(p.takeBoolean())).toThrow()
})

// test('skip_padding', () => {
//     var p = ByteParser.fromString("00000001000000");
//     expect(p.takeInt32()).toBe(1);
//     expect(() => p.skip_padding(4)).toThrow();
//     expect(() => p.skip_padding(1)).not.toThrow();
//     expect(() => p.skip_padding(1)).not.toThrow();
//     expect(() => p.skip_padding(1)).not.toThrow();
//     expect(() => p.skip_padding(1)).toThrow();
// })

describe('takeSelfTerminating', () => {
    test('constant-true predicate consumes one byte', () => {
        const p = ByteParser.fromStringHex('00000000')
        expect(p.takeSelfTerminating((_) => true)).toEqual(Ok(new Uint8Array([0x00])))
        expect(p.remainder).toBe(3)
    })

    test('constant-false predicate causes attempted overrun', () => {
        const p = ByteParser.fromStringHex('00000000')
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        expect(() => unwrap(p.takeSelfTerminating((_) => false))).toThrow()
        p.setFit(2)
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        expect(() => unwrap(p.takeSelfTerminating((_) => false))).toThrow()
    })

    test('Zarith predicate', () => {
        const p = ByteParser.fromStringHex('808f810c00')
        expect(unwrap(p.takeSelfTerminating((byte) => !(byte & 0x80)))).toEqual(new Uint8Array([0x80, 0x8f, 0x81, 0x0c]))
        expect(unwrap(p.takeSelfTerminating((byte) => !(byte & 0x80)))).toEqual(new Uint8Array([0x00]))
    })
})

describe('length', () => {
    test('over entire buffer', () => {
        let i: number
        for (i = 1; i <= 4; i++) {
            const p = ByteParser.fromStringHex('01020304')
            p.consume(i)
            expect(p.remainder).toBe(4 - i)
        }
    })

    test('inside context window', () => {
        let i: number
        for (i = 1; i <= 8; i++) {
            for (let j = i; j <= 8; j++) {
                const p = ByteParser.fromStringHex('0102030401020304')
                p.setFit(j)
                p.consume(i)
                expect(p.remainder).toBe(j - i)
            }
        }
    })

    test('inside nested context windows', () => {
        let i: number
        const p = ByteParser.fromStringHex('0102030401020304')
        for (i = 8; i >= 0; i--) {
            p.setFit(i)
            expect(p.viewLen).toBe(i)
        }
        for (i = 0; i < 8; i++) {
            const avail = (i == 0) ? 0 : 1
            expect(p.testTarget()).toEqual(i == 0)
            expect(p.remainder).toEqual(avail)
            expect(p.consume(avail).length).toBe(avail)
            expect(p.testTarget()).toEqual(true)
            expect(() => p.enforceTarget()).not.toThrow()
        }
        expect(p.testTarget()).toEqual(false)
        expect(p.remainder).toEqual(1)
        expect(() => p.consume(1)).not.toThrow()
        expect(p.testTarget()).toEqual(true)
        expect(() => p.enforceTarget()).not.toThrow()
        expect(p.remainder).toEqual(0)
        expect(p.testTarget()).toEqual(false)
        expect(() => p.enforceTarget()).toThrow()
    })
})


test('persistent state after closure', () => {
    const f = (p: TypedParser): integer.Uint8 => {
        return extract(mapOk(p.takePrimUint8(), integer.Uint8.promote))!
    }

    const p = ByteParser.fromStringHex('01020304')
    expect(extract(p.takePrimUint8())!.value).toBe(1)
    expect(f(p).value).toBe(2)
    expect(f(p).value).toBe(3)
    expect(extract(p.takePrimUint8())!.value).toBe(4)
})

describe('context window behavior', () => {
    test('cannot consume past target-offset', () => {
        const p = ByteParser.fromStringHex('00000001')
        p.setFit(3)
        expect(() => p.consume(4)).toThrow()
        // expect(() => p.takeInt32()).toThrow();
    })

    test('setFit cannot set illegal context-window', () => {
        const p = ByteParser.fromStringHex('00000001')

        expect(() => p.setFit(-1)).toThrow() // width must be non-negative
        expect(() => p.setFit(100)).toThrow() // width cannot exceed buffer bounds
        p.setFit(3)
        expect(() => p.setFit(4)).toThrow() // new window cannot be wider than old window
        p.consume(2)
        expect(() => p.setFit(2)).toThrow() // narrower inner window cannot overrun end of wider old window
    })

    test('testTarget + enforceTarget correctness', () => {
        const p = ByteParser.fromStringHex('00000001' + '00000002')

        /* test should fail and enforce should fail before we have opened any windows */
        expect(p.testTarget()).toEqual(false)
        expect(() => p.enforceTarget()).toThrow()

        p.setFit(8)
        p.setFit(4)

        /* we can test and enforce a goal of 0 without consuming */
        p.setFit(0)
        expect(p.testTarget()).toEqual(true)
        expect(() => p.enforceTarget()).not.toThrow()

        for (let i = 0; i < 4; i++) {
            /* test should be negative and enforce should fail while we are still consuming */
            expect(p.testTarget()).toEqual(false)
            expect(() => p.enforceTarget()).toThrow()
            p.consume(1)
        }
        /* test should be positive and enforce should succeed once we have exhausted the inner window */
        expect(p.testTarget()).toEqual(true)
        expect(() => p.enforceTarget()).not.toThrow()

        for (let i = 0; i < 4; i++) {
            /* test should be negative and enforce should fail while we are still consuming */
            expect(p.testTarget()).toEqual(false)
            expect(() => p.enforceTarget()).toThrow()
            p.consume(1)
        }
        /* test should be positive and enforce should succeed once we have exhausted the outer window */
        expect(p.testTarget()).toEqual(true)
        expect(() => p.enforceTarget()).not.toThrow()

        /* test should fail and enforce should fail once all windows have been closed */
        expect(p.testTarget()).toEqual(false)
        expect(() => p.enforceTarget()).toThrow()
    })

    // test('tighten_fit cannot set illegal context-window', () => {
    //     var p = ByteParser.fromString("00000001");
    //     const new_fit = 3;
    //     expect(() => p.tighten_fit(8)).toThrow();
    //     p.setFit(new_fit);
    //     expect(() => p.tighten_fit(4)).toThrow();
    //     for (var i = new_fit - 1; i >= 0; i--) {
    //         expect(() => { p.tighten_fit(1); }).not.toThrow();
    //     }
    //     expect(() => { p.tighten_fit(1); }).toThrow();
    // });

    // test('tighten_fit cannot underrun current offset', () => {
    //     var p = ByteParser.fromString("00000001");
    //     p.tighten_fit(1);
    //     p.consume(2);
    //     expect(() => p.tighten_fit(2)).toThrow();
    // });
})
