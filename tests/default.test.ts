import { Default } from '../ts_runtime/composite/default';
import { liftDecoder } from '../ts_runtime/decode';
import { Uint8 } from '../ts_runtime/integer/integer';

describe('DefaultValue', () => {
    test('detects default values', () => {
        type TheDefault = Default<Uint8, '2a'>;
        const mkIt = (value: number): TheDefault => new Default(new Uint8(value), '2a');
        const zero = mkIt(0);
        const answer = mkIt(42);

        expect(zero.encode()).toStrictEqual([0xff, 0x00]);
        expect(answer.encode()).toStrictEqual([0x00]);
        let decoder = liftDecoder(Default.decode(Uint8.decode, '2a'));
        expect(decoder([0x00]).value).toStrictEqual(new Uint8(42));
        expect(decoder([0xff, 0x00]).value).toStrictEqual(new Uint8(0));
        expect(decoder([0xff, 0x2a]).encode()).toStrictEqual([0x00]);
    });
});