import { RangedInt, rangedint_reader, rangedint_encoder } from '../ts_runtime/integer/integer.ranged'
import * as kinds from '../ts_runtime/integer/kinds'
import * as fc from 'fast-check'
import { PrimInt8, PrimUint8 } from '../ts_runtime/integer/types'
import { NumericBounds } from '../ts_runtime/bounds'
import * as uint8_type from '../ts_runtime/integer'
import * as int8_type from '../ts_runtime/integer'

const arr_eq = <T>(operand: readonly T[], template: readonly T[]): boolean => {
    return (operand.length === template.length && operand.every((val, ix) => val === template[ix]))
}


describe('RangedInt constructor', () => {
    test('rejects inverted but legal bounds', () => {
        expect(() => new RangedInt(3, kinds.Uint8Kind, { min: 4, max: 2 } as const)).toThrow()
    })

    test('accepts range-transposed legal bounds', () => {
        expect(new RangedInt(1024, kinds.Uint8Kind, { min: 1024, max: 1200 } as const).value).toBe(1024)
    })

    test('rejects bounds too wide for kind', () => {
        expect(() => new RangedInt(0, kinds.Uint8Kind, { min: 1024, max: 2048 } as const).value).toThrow()
    })

    test('rejects positive minimum for signed kind', () => {
        expect(() => new RangedInt(2, kinds.Int8Kind, { min: 1, max: 3 } as const)).toThrow()
    })
})


describe('RangedInt codec', () => {
    test('Uint8 works in any translation', () => {
        const normal: NumericBounds = PrimUint8.bounds
        const shifted = (shift: number): NumericBounds => ({ min: normal.min + shift, max: normal.max + shift })

        fc.assert(fc.property(fc.tuple(fc.integer({ min: 0, max: 0x3fff_ff00 }), fc.integer(normal)), ([i, x]) => {
            const bounds = shifted(i)

            rangedint_reader({ intKind: kinds.Uint8Kind, bounds: bounds })([x]).value == i + uint8_type.uint8_reader([x])
        }))
    })

    test('Int8 is equivalent to itself in any restriction', () => {
        const normal: NumericBounds = PrimInt8.bounds
        const kind = PrimInt8.kind

        fc.assert(fc.property(
            fc.integer({ ...normal, max: -1 })
                .chain(i => fc.tuple(fc.constant(i), fc.integer({ ...normal, min: i })))
                .chain(([min, max]) => fc.tuple(fc.constant({ min: min, max: max }), fc.integer({ min: min, max: max })))
            , ([bounds, x]) => {
                return arr_eq(rangedint_encoder(kind, bounds)(x), int8_type.int8_encoder(x))
            }
        ))
    })
})