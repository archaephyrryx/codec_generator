import { U8String } from '../ts_runtime/primitive/string'
import * as fc from 'fast-check'
import { liftDecoder } from '../ts_runtime/decode'

describe('U8String', () => {
    test('roundtrip', () => {
        const constraints = { maxLength: 32 }
        fc.assert(fc.property(fc.unicodeString(constraints), str => {
            const u8str = U8String.fromString(str);
            liftDecoder(U8String.decode)(u8str.encode()).equals(u8str)
        }))
    })
})