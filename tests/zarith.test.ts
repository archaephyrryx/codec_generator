import { z_type, n_type } from "../ts_runtime/zarith"
import * as fc from 'fast-check';

describe('z_type.integer tests', () => {
    const cases: Array<[bigint, number[]]> = [
        [0n, [0x00]],
        [1n, [0x01]],
        [-1n, [0x41]],
        [64n, [0x80, 0x01]],
        [-64n, [0xc0, 0x01]],
    ];
    const encoder = z_type.z_encoder;
    const decoder = z_type.z_reader;
    const min_val = -0xffff_ffff_ffff_ffffn
    const max_val = 0xffff_ffff_ffff_ffffn
    const bigintbounds = { min: min_val, max: max_val };

    test('encoder', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toStrictEqual(image)
        );
    });

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('rejects empty array decode', () => {
        expect(() => decoder([])).toThrow();
    })

    test('rejects extraneous zero-continue decode', () => {
        expect(() => decoder([0x80, 0x80, 0x00])).toThrow();
    })

    test('rejects non-terminating array decode', () => {
        expect(() => decoder([0x80, 0x80, 0x80, 0x80])).toThrow();
    })

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.bigInt(bigintbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});

describe('n_type.natural tests', () => {
    const cases: Array<[bigint, number[]]> = [
        [0n, [0x00]],
        [1n, [0x01]],
        [0x41n, [0x41]],
        [128n, [0x80, 0x01]],
        [192n, [0xc0, 0x01]],
    ];
    const encoder = n_type.n_encoder;
    const decoder = n_type.n_reader;
    const min_val = 0x0n;
    const max_val = 0xffff_ffff_ffff_ffffn;
    const bigintbounds = { min: min_val, max: max_val };

    test('encode', () => {
        cases.forEach(([preimage, image]) =>
            expect(encoder(preimage)).toStrictEqual(image)
        );
    });

    test('encode out of range', () => {
        expect(() => encoder(min_val - 1n)).toThrow();
    });

    test('rejects empty array decode', () => {
        expect(() => decoder([])).toThrow();
    })

    test('rejects non-terminating array decode', () => {
        expect(() => decoder([0x80, 0x80, 0x80, 0x80])).toThrow();
    })

    test('rejects extraneous zero-continue decode', () => {
        expect(() => decoder([0x80, 0x80, 0x00])).toThrow();
    })

    test('decode', () => {
        cases.forEach(([image, preimage]) =>
            expect(decoder(preimage)).toBe(image)
        );
    });

    test('roundtrip', () => {
        fc.assert(fc.property(
            fc.bigInt(bigintbounds),
            i => { decoder(encoder(i)) === i; }
        ));
    });
});