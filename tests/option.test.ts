import { isNone, None, Some, isSome } from '../ts_runtime/composite/opt/option.type'
import * as fc from 'fast-check'

describe('option core functionality', () => {
    test('none is none', () => {
        expect(isNone(None))
    })

    test('some is always some', () => {
        fc.assert(fc.property(fc.anything(), x => {
            const someX = Some(x)
            return isSome(someX) && !isNone(someX)
        }))
    })
})