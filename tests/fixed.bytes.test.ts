import { FixedBytes, fixedbytes_reader } from "../ts_runtime/primitive/bytes.fixed";
import * as fc from 'fast-check';

const arr_eq = <T>(operand: readonly T[], template: readonly T[]): boolean => {
    return (operand.length === template.length && operand.every((val, ix) => val === template[ix]));
}

describe('fixedbytes test', () => {
    test('roundtrip', () => {
        const arrbounds: fc.ArrayConstraints = { minLength: 1, maxLength: 32 };
        const numbounds: fc.IntegerConstraints = { min: 0, max: 255 };
        fc.assert(fc.property(
            fc.array(fc.integer(numbounds), arrbounds),
            arr => {
                return arr_eq(fixedbytes_reader({ len: arr.length })(FixedBytes.promote(arr).encode()), arr);
            }
        ))
    });
})