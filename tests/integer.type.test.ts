import { NumericBounds } from '../ts_runtime/bounds'
import { IntKind } from '../ts_runtime/integer/kinds'
import * as types from '../ts_runtime/integer/types'

describe('integer-type sanity', () => {
    const cases: [string, IntKind, NumericBounds<number | bigint>][] =
        [
            ['Int8', types.PrimInt8.kind, types.PrimInt8.bounds],
            ['Int16', types.PrimInt16.kind, types.PrimInt16.bounds],
            ['Int31', types.PrimInt31.kind, types.PrimInt31.bounds],
            ['Int32', types.PrimInt32.kind, types.PrimInt32.bounds],
            ['Int64', types.PrimInt64.kind, types.PrimInt64.bounds],
            ['Uint8', types.PrimUint8.kind, types.PrimUint8.bounds],
            ['Uint16', types.PrimUint16.kind, types.PrimUint16.bounds],
            ['Uint30', types.PrimUint30.kind, types.PrimUint30.bounds]]


    test.each(cases)('%sKind', (name, kind, bounds) => {
        expect(types.getIntegerBounds(kind)).toBe(bounds)
        expect(types.getIntegerName(kind)).toBe(name)
    })
})
