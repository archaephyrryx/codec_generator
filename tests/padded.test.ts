import { Padded } from '../ts_runtime/composite/padded'
import * as fc from 'fast-check'
import { Int64 } from '../ts_runtime/integer'
import { liftDecoder } from '../ts_runtime/decode'

describe('Padded', () => {
    test('roundtrip (int64)', () => {
        fc.assert(fc.property(
            fc.nat(128)
                .chain(padding =>
                    fc.bigIntN(64)
                        .map(value => new Padded(new Int64(value), padding))
                ),
            p64 => {
                liftDecoder(Padded.decode(Int64.decode, p64.padding))(p64.encode()) === p64
            }
        ))
    })
})