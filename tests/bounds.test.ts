import { clampWith, NumericBounds } from '../ts_runtime/bounds'
// import * as fc from 'fast-check'

describe('clampWith', () => {
    test('inverted bounds throws', () => {
        const inverted: NumericBounds<number> = { min: 5, max: 3 };
        expect(() => clampWith(4, inverted)).toThrow();
    })

    test('normal bounds correctness', () => {
        const normal: NumericBounds<number> = { min: 3, max: 5 };

        const inRange: number = 4;
        const over: number = 6;
        const under: number = 2;
        const floating: number = 4.5;

        expect(clampWith(inRange, normal)).toBe(inRange);
        expect(clampWith(inRange, normal, { strict: false })).toBe(inRange);
        expect(clampWith(inRange, normal, { strict: true })).toBe(inRange);

        expect(clampWith(over, normal)).toBe(normal.max);
        expect(clampWith(over, normal, { strict: false })).toBe(normal.max);
        expect(() => clampWith(over, normal, { strict: true })).toThrow();

        expect(clampWith(under, normal)).toBe(normal.min);
        expect(clampWith(under, normal, { strict: false })).toBe(normal.min);
        expect(() => clampWith(under, normal, { strict: true })).toThrow();

        expect(clampWith(floating, normal)).toBe(Math.trunc(floating));
        expect(clampWith(floating, normal, { strict: false })).toBe(Math.trunc(floating));
        expect(() => clampWith(floating, normal, { strict: true })).toThrow();

        expect(clampWith(floating, normal, { domain: 'float' })).toBe(floating);
        expect(clampWith(floating, normal, { domain: 'float', strict: false })).toBe(floating);
        expect(clampWith(floating, normal, { domain: 'float', strict: true })).toBe(floating);
    });
});

