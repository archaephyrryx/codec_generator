import * as fc from 'fast-check'
import { NumericBounds } from '../ts_runtime/bounds'
import { Decoder, liftDecoder } from '../ts_runtime/decode'
import { Encoder } from '../ts_runtime/encode'
import { Uint8, Int8, Uint16, Int16, Uint30, Int31, Int32, Int64 } from '../ts_runtime/integer'
import * as integer from '../ts_runtime/integer'
import { Int64Kind, IntKindNumber } from '../ts_runtime/integer/kinds'
import { getIntegerName } from '../ts_runtime/integer/types'

type TestCase =
    { disc: 'number', bounds: NumericBounds<number>, kind: IntKindNumber, dec: Decoder<number>, enc: Encoder<number> }
    | { disc: 'bigint', bounds: NumericBounds<bigint>, kind: Int64Kind, dec: Decoder<bigint>, enc: Encoder<bigint> }


describe('integer correctness', () => {
    const table: Array<TestCase> =
        [
            { disc: 'number', kind: Uint8.kind, bounds: Uint8.bounds, enc: integer.uint8_encoder, dec: integer.uint8_decoder },
            { disc: 'number', kind: Int8.kind, bounds: Int8.bounds, enc: integer.int8_encoder, dec: integer.int8_decoder },
            { disc: 'number', kind: Uint16.kind, bounds: Uint16.bounds, enc: integer.uint16_encoder, dec: integer.uint16_decoder },
            { disc: 'number', kind: Int16.kind, bounds: Int16.bounds, enc: integer.int16_encoder, dec: integer.int16_decoder },
            { disc: 'number', kind: Uint30.kind, bounds: Uint30.bounds, enc: integer.uint30_encoder, dec: integer.uint30_decoder },
            { disc: 'number', kind: Int31.kind, bounds: Int31.bounds, enc: integer.int31_encoder, dec: integer.int31_decoder },
            { disc: 'number', kind: Int32.kind, bounds: Int32.bounds, enc: integer.int32_encoder, dec: integer.int32_decoder },
            { disc: 'bigint', kind: Int64.kind, bounds: Int64.bounds, enc: integer.int64_encoder, dec: integer.int64_decoder },
        ]

    test.each(table.map(({ kind, ...arg }) => [getIntegerName(kind), arg]))('%s roundtrip', (_name, testcase) => {
        const { disc, bounds, enc, dec } = testcase;
        if (disc === 'number') {
            fc.assert(fc.property(fc.integer(bounds),
                i => {
                    liftDecoder(dec)(enc(i)) === i;
                }
            ))
        } else {
            fc.assert(fc.property(fc.bigInt(bounds),
                i => {
                    liftDecoder(dec)(enc(i)) === i;
                }
            ))
        }
    })
})

