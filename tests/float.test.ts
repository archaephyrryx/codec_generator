import * as fc from 'fast-check'
import { liftDecoder } from '../ts_runtime/decode'
import { Double } from '../ts_runtime/float/double'

describe('Double correctness', () => {
    test('unsafe values rejected by constructor', () => {
        expect(() => new Double(Number.NaN)).toThrow()
        expect(() => new Double(Number.NEGATIVE_INFINITY)).toThrow()
        expect(() => new Double(Number.POSITIVE_INFINITY)).toThrow()
    })

    test('safe values roundtrip', () => {
        fc.assert(fc.property(fc.double(), x => {
            liftDecoder(Double.decode)(new Double(x).encode()).value === x;
        }))
    })
})