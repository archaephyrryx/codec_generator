import { Codec } from '../ts_runtime/codec';
import { Dynamic } from '../ts_runtime/composite/dynamic';
import { Sequence } from '../ts_runtime/composite/seq/sequence';
import { variant_decoder, variant_encoder, VariantDecoder } from '../ts_runtime/constructed/adt';
import { Box } from '../ts_runtime/core/box';
import width from '../ts_runtime/core/width.type';
import { OutputBytes } from '../ts_runtime/encode';
import { Parser } from '../ts_runtime/parse';
import { U8String } from '../ts_runtime/primitive/string';
import { Sink, Target } from '../ts_runtime/target';
import { Z } from '../ts_runtime/zarith/integer';

export enum MichelTag {
    int = 0,
    string = 1,
    sequence = 2,
}

type IntValue = {
    kind: MichelTag.int;
    value: Z;
};

type StringValue = {
    kind: MichelTag.string;
    value: Dynamic<U8String, width.Uint30>;
};

type SequenceValue = {
    kind: MichelTag.sequence,
    value: Sequence<MichelsonValue>
}

export type michelson_value =
    | IntValue
    | StringValue
    | SequenceValue

export function mkDecoder(): VariantDecoder<MichelTag, michelson_value> {
    function f(disc: MichelTag) {
        switch (disc) {
            case MichelTag.int: return Z.decode;
            case MichelTag.string: return Dynamic.decode(U8String.decode, width.Uint30);
            case MichelTag.sequence: return Sequence.decode(MichelsonValue.decode);
        }
    }
    f.isValid = (tagval: number): tagval is MichelTag => Object.values(MichelTag).includes(tagval)
    return f;
}

export class MichelsonValue extends Box<michelson_value> implements Codec {
    static decode(p: Parser): MichelsonValue {
        const raw = variant_decoder(width.Uint8)(mkDecoder())(p);
        return new MichelsonValue(raw);
    }

    encode(): OutputBytes {
        return variant_encoder<MichelTag, michelson_value>(width.Uint8)(this.value);
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }

    get encodeLength(): number {
        return this.writeTarget(new Sink());
    }
}