import { N, n_decoder, n_encoder } from '../ts_runtime/zarith/natural';
import { Box, onValue, unboxed, Unboxed } from '../ts_runtime/core/box';
import { Decoder } from '../ts_runtime/decode';
import { Encoder } from '../ts_runtime/encode';

export class Mutez extends N { }

export type mutez = Unboxed<Mutez>;

export const mutez_decoder: Decoder<mutez> = n_decoder;
export const mutez_encoder: Encoder<mutez> = n_encoder;