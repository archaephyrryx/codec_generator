import { Codec, ConstantLength } from "../ts_runtime/codec";
import { Box } from "../ts_runtime/core/box";
import { OutputBytes } from "../ts_runtime/encode";
import { Target } from "../ts_runtime/target";
import { enum_decoder, enum_encoder } from '../ts_runtime/constructed/enum';
import width from "../ts_runtime/core/width.type";
import { Decoder } from "../ts_runtime/decode";

export enum ballotvote {
    yay = 0,
    nay = 1,
    abstain = 2,
};

export class BallotVote extends Box<ballotvote> implements Codec, ConstantLength<1> {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<ballotvote>(this.value);
    }

    static readonly decode: Decoder<BallotVote> = (p) => {
        const raw = enum_decoder(width.Uint8)((x): x is ballotvote => (Object.values(ballotvote).includes(x)))(p);
        return new BallotVote(raw);
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }

    __constantLength__: void;

    get encodeLength(): 1 {
        return 1;
    }
}