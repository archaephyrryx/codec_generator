import { Dynamic } from '../ts_runtime/composite/dynamic';
import width from '../ts_runtime/core/width.type';
import { U8String } from '../ts_runtime/primitive/string';
import { Option } from '../ts_runtime/composite/opt/option';
import { Box, unboxed } from '../ts_runtime/core/box';
import { Codec } from '../ts_runtime/codec';
import { Encode, Encoder, OutputBytes } from '../ts_runtime/encode';
import { Decoder } from '../ts_runtime/decode';
import { identify, record_decoder } from '../ts_runtime/constructed/record';
import { Target } from '../ts_runtime/target';

type UniString = Dynamic<U8String, width.Uint30>;

const unistring_decoder = Dynamic.decode(U8String.decode, width.Uint30);
const unistring_encoder = Dynamic.encoder;

export type person = { first: UniString, middle: Option<UniString>, last: UniString };

const personOrder: Array<keyof person> = ['first', 'middle', 'last'];

export class Person extends Box<person> implements Codec {
    static readonly order = personOrder;

    get order() {
        return Person.order;
    }

    encode(): OutputBytes {
        return this.order.flatMap(key => this.value[key].encode(), this);
    }

    static encoder(person: Person): OutputBytes {
        return person.encode();
    }

    static readonly decode: Decoder<Person> = (p) => {
        const value: person = identify<person>(
            record_decoder<person>(
                {
                    first: unistring_decoder,
                    middle: Option.decode(unistring_decoder),
                    last: unistring_decoder,
                }, { order: this.order }
            )(p)
        );
        return new Person(value);
    }

    writeTarget(tgt: Target): number {
        return this.order.reduce((acc, key) => acc + tgt.writeArray(this.value[key].encode()), 0);
    }

    get encodeLength(): number {
        return this.order.reduce((acc, key) => acc + this.value[key].encodeLength, 0);
    }
}

export const person_encoder: Encoder<person> = (val) => new Person(val).encode();
export const person_decoder: Decoder<person> = unboxed(Person.decode);
