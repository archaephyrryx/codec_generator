open Data_encoding

let rec simplify : type a. a Encoding.t -> a Encoding.t =
 fun x ->
  match x.encoding with
  | Describe {encoding; _}
  | Check_size {encoding; _}
  | Splitted {encoding; _}
  | Dynamic_size {encoding; _}
  | Tup encoding
  | Padded (encoding, _) ->
      encoding
  | Delayed thunk -> simplify (thunk ())
  | Mu _ -> x
  | Union _ -> x
  | Conv _ -> x
  | String_enum _ -> x
  | Objs _ -> x
  | Obj _ -> x
  | List ({elts; _} as l) ->
      {x with encoding = List {l with elts = simplify elts}}
  | Array ({elts; _} as arr) ->
      {x with encoding = Array {arr with elts = simplify elts}}
  | Tups _ -> x
  | String _ -> x
  | _ -> x

let rec lr_balance : type a. a Encoding.t -> (int, int * int) Either.t =
 fun x ->
  match (simplify x).encoding with
  | Tup _ -> Either.Left 1
  | Tups {left; right; _} ->
      let weight = function
        | Either.Left n -> n
        | Either.Right (l, r) -> l + r
      in
      Either.Right (weight (lr_balance left), weight (lr_balance right))
  | Conv {encoding; _} -> lr_balance encoding
  | _ -> invalid_arg "Non-tuple argument in lr_balance"

let rec raw_equiv : type a b. a Encoding.t -> b Encoding.t -> a -> b -> bool =
 fun encoding_a encoding_b val_a val_b ->
  let a = simplify encoding_a and b = simplify encoding_b in
  match (a.encoding, b.encoding) with
  | Conv {encoding; proj; _}, _ -> raw_equiv encoding b (proj val_a) val_b
  | _, Conv {encoding; proj; _} -> raw_equiv a encoding val_a (proj val_b)
  | Mu {fix = fix_a; _}, _ -> raw_equiv (fix_a a) b val_a val_b
  | _, Mu {fix = fix_b; _} -> raw_equiv a (fix_b b) val_a val_b
  | (Null | Empty | Ignore | Constant _), (Null | Empty | Ignore | Constant _)
    ->
      true
  | (Null | Empty | Ignore | Constant _), _
  | _, (Null | Empty | Ignore | Constant _) ->
      false
  | Bool, Bool -> val_a = val_b
  | Bool, _ | _, Bool -> false
  | Uint8, Uint8 -> val_a = val_b
  | Uint8, _ | _, Uint8 -> false
  | Int8, Int8 -> val_a = val_b
  | Int8, _ | _, Int8 -> false
  | Uint16, Uint16 -> val_a = val_b
  | Uint16, _ | _, Uint16 -> false
  | Int16, Int16 -> val_a = val_b
  | Int16, _ | _, Int16 -> false
  | Int31, Int31 -> val_a = val_b
  | Int31, _ | _, Int31 -> false
  | Int32, Int32 -> val_a = val_b
  | Int32, _ | _, Int32 -> false
  | Int64, Int64 -> val_a = val_b
  | Int64, _ | _, Int64 -> false
  | RangedInt _, RangedInt _ -> val_a = val_b
  | RangedInt _, _ | _, RangedInt _ -> false
  | Float, Float -> val_a = val_b
  | Float, _ | _, Float -> false
  | RangedFloat _, RangedFloat _ -> val_a = val_b
  | RangedFloat _, _ | _, RangedFloat _ -> false
  | N, N -> val_a = val_b
  | N, _ | _, N -> false
  | Z, Z -> val_a = val_b
  | Z, _ | _, Z -> false
  | Bytes (`Fixed n, _), Bytes (`Fixed m, _) -> n == m && val_a = val_b
  | Bytes (`Variable, _), Bytes (`Variable, _) -> val_a = val_b
  | Bytes _, _ | _, Bytes _ -> false
  | String (`Fixed n, _), String (`Fixed m, _) -> n == m && val_a = val_b
  | String (`Variable, _), String (`Variable, _) -> val_a = val_b
  | String _, _ | _, String _ -> false
  | Array {elts = elts_a; _}, Array {elts = elts_b; _} ->
      Array.for_all2 (raw_equiv elts_a elts_b) val_a val_b
  | Array _, _ | _, Array _ -> false
  | List {elts = elts_a; _}, List {elts = elts_b; _} ->
      List.for_all2 (raw_equiv elts_a elts_b) val_a val_b
  | List _, _ | _, List _ -> false
  | Tup enc_a, _ -> raw_equiv enc_a b val_a val_b
  | _, Tup enc_b -> raw_equiv a enc_b val_a val_b
  | ( Tups {left = left_a; right = right_a; _},
      Tups {left = left_b; right = right_b; _} ) ->
      (* FIXME: handle skewed nesting of otherwise linearly identical tuples *)
      let xa, ya = val_a and xb, yb = val_b in
      raw_equiv left_a left_b xa xb && raw_equiv right_a right_b ya yb
  | Obj (Req {encoding = enc_a; _}), Obj (Req {encoding = enc_b; _}) ->
      raw_equiv enc_a enc_b val_a val_b
  | Obj (Dft {encoding = enc_a; _}), Obj (Req {encoding = enc_b; _}) ->
      raw_equiv enc_a enc_b val_a val_b
  | Obj (Req {encoding = enc_a; _}), Obj (Dft {encoding = enc_b; _}) ->
      raw_equiv enc_a enc_b val_a val_b
  | Obj (Dft {encoding = enc_a; _}), Obj (Dft {encoding = enc_b; _}) ->
      raw_equiv enc_a enc_b val_a val_b
  | Obj (Opt {encoding = enc_a; _}), Obj (Opt {encoding = enc_b; _}) -> (
      match (val_a, val_b) with
      | None, None -> true
      | Some x, Some y -> raw_equiv enc_a enc_b x y
      | _ -> false)
  | Obj _, _ | _, Obj _ -> false
  | ( Objs {left = left_a; right = right_a; _},
      Objs {left = left_b; right = right_b; _} ) ->
      (* FIXME: handle skewed nesting of otherwise linearly identical tuples *)
      let xa, ya = val_a and xb, yb = val_b in
      raw_equiv left_a left_b xa xb && raw_equiv right_a right_b ya yb
  | Objs _, _ | _, Objs _ -> false
  | String_enum (tbl_a, _), String_enum (tbl_b, _) ->
      Hashtbl.find tbl_a val_a = Hashtbl.find tbl_b val_b
  | String_enum _, _ | _, String_enum _ -> false
  | Tups _, _ | _, Tups _ -> false
  | Delayed _, _ | _, Delayed _ -> assert false
  | Describe _, _ | _, Describe _ -> assert false
  | Splitted _, _ | _, Splitted _ -> assert false
  | Check_size _, _ | _, Check_size _ -> assert false
  | Dynamic_size _, _ | _, Dynamic_size _ -> assert false
  | Padded _, _ | _, Padded _ -> assert false
  | Union {match_case = match_a; _}, Union {match_case = match_b; _} -> (
      match (match_a val_a, match_b val_b) with
      | Matched (tag_a, inner_a, a), Matched (tag_b, inner_b, b) ->
          tag_a == tag_b && raw_equiv inner_a inner_b a b)
