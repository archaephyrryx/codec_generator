module S = struct
  include Hashtbl.Make (struct
    include String

    let hash = Hashtbl.hash
  end)
end

type t = (string * bool) S.t

let table_size = 128

let global : t = S.create table_size

let register : item:string -> ?is_default:bool -> loc:string -> unit -> unit =
 fun ~item ?(is_default = false) ~loc () ->
  S.add global item ("ts_runtime/" ^ loc, is_default)

let register_default : item:string -> loc:string -> unit =
 fun ~item ~loc -> register ~item ~is_default:true ~loc ()

let register = register ~is_default:false ()

let lookup : string -> (string * bool) option = S.find_opt global
