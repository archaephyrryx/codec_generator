open Ast
module Simplified = Common.Simplified

(** [dir]: Marker type for direction of transcoding *)
type dir = Encode | Decode

(** [mk_fname]: Produce an identifier for the associated transcoding function of a given type-name *)
let mk_fname = function
  | Encode -> fun x -> Identifier.AssociatedMethod (x, "encoder")
  | Decode -> fun x -> Identifier.AssociatedMethod (x, "decoder")

(** [sem]: Second Element Map

    Helper function for applying functions selectively to the second element of a tuple while preserving the first element.

    [sem f (x, y)] is defined to be [(x, f y)] over any two-element tuple type.
    *)
let sem : 'a 'b. ('b -> 'c) -> 'a * 'b -> 'a * 'c = fun f (c, x) -> (c, f x)

(** [TypeDef]: Module for Type-Definitions within the Typescript AST model *)
module TypeDef = struct
  (** [t]: Expanded shape of a Simplified codec, within the Typescript AST model*)
  type t =
    | Monic of Type.Atom.t  (** Undecorated simple term of any atomic type *)
    | Elems of Type.Atom.t list
        (** Tuple over some number of atomic elements *)
    | Fields of (string * Type.Atom.t) list
        (** Record over some number of named, atomically typed fields *)
    | CStyle of Simplified.enum_size * (string * int) list
        (** C-Style enum with a specified size (width) *)
    | Variants of Simplified.tag_size * (int * (string * t)) list
        (** Discriminated union with a specified tag-size (width) *)

  type u = Expanded of t | Raw of Common.Simplified.t

  module ClassDef = struct
    type nonrec t = {class_name : string; unboxed_name : string; tdef : t}
  end
end

module Ctxt = struct
  module State = struct
    (** String-valued Map over module import path to import set *)
    module ISet = struct
      module S = struct
        include Set.Make (String)

        type nonrec t = string option * t

        let singleton' x = (Some x, empty)

        let empty = (None, empty)

        let singleton x = (None, singleton x)

        let add value : t -> t = fun (c, s) -> (c, add value s)

        let add' value : t -> t = function
          | None, s -> (Some value, s)
          | (Some value', _) as x ->
              if value <> value' then
                failwith "Ctxt.State.Iset.S.add': conflicting default imports"
              else x

        let union (c, s) (c', s') =
          let c'' =
            match (c, c') with
            | None, _ -> c'
            | _, None -> c
            | Some x, Some y ->
                if x <> y then
                  failwith
                    "Ctxt.State.ISet.S.union: conflicting default imports"
                else c
          in
          (c'', union s s')

        let elements (c, s) = (c, elements s)
      end

      module M = Map.Make (String)

      type t = S.t M.t

      let empty = M.empty

      let add_value : key:string -> value:string -> t -> t =
       fun ~key ~value ->
        let f = function
          | None -> Some (S.singleton value)
          | Some s -> Some (S.add value s)
        in
        M.update key f

      let add_default : key:string -> value:string -> t -> t =
       fun ~key ~value ->
        let f = function
          | None -> Some (S.singleton' value)
          | Some s -> Some (S.add' value s)
        in
        M.update key f

      let add : string -> t -> t =
       fun value m ->
        let key, is_default =
          Symtab.lookup value |> Option.value ~default:("MISSING", false)
        in
        if is_default then add_default ~key ~value m
        else add_value ~key ~value m

      let index : t -> (string * (string option * string list)) list =
       fun m -> M.bindings m |> List.map (sem S.elements)

      let union : S.t M.t -> S.t M.t -> S.t M.t =
        M.union (fun _ x y -> Some (S.union x y))

      let elements : t -> string option * string list =
       fun m -> M.fold (fun _ -> S.union) m S.empty |> S.elements
    end

    (** String-keyed Type Dictionary for subordinate defintion tracking *)
    module TDict = struct
      include Map.Make (String)

      (** Merges two type-dictionaries, taking the associated definition of the
        second (right-biased) when keys overlap.

        Takes an optional parameter [?neq], a predicate for inequality over the entry-type,
        defaulting to [Stdlib.(<>)].

        If both dictionaries contain a shared key, and the associated entries are judged unequal,
        an error message will be printed, but otherwise the right-biased preference will
        still be enforced (without changing the value that is returned).
      *)
      let union : ?neq:('a -> 'a -> bool) -> 'a t -> 'a t -> 'a t =
        let f neq id x y =
          if neq x y then
            Format.eprintf
              "@[Typescript.Generator.Ctxt.State.TDict.union:@ Conflicting \
               values for key '%s'@,\
               Defaulting to right-biased override...]@."
              id ;
          Some y
        in
        fun ?(neq = Stdlib.( <> )) -> union (f neq)
    end

    type imports = ISet.t

    type subdefs = TypeDef.u TDict.t

    type classes = TypeDef.ClassDef.t TDict.t

    type t = {imports : imports; defs : subdefs; classes : classes}

    let empty =
      {imports = ISet.empty; defs = TDict.empty; classes = TDict.empty}

    let add_import : string -> t -> t =
     fun s ({imports; _} as st) -> {st with imports = ISet.add s imports}

    let add_definition : id:string -> entry:TypeDef.u -> t -> t =
     fun ~id ~entry ({defs; _} as st) ->
      {st with defs = TDict.add id entry defs}

    let add_class : key:string -> entry:TypeDef.ClassDef.t -> t -> t =
     fun ~key ~entry ({classes; _} as st) ->
      {st with classes = TDict.add key entry classes}

    let union : t -> t -> t =
     fun st st' ->
      {
        imports = ISet.union st.imports st'.imports;
        defs = TDict.union st.defs st'.defs;
        classes = TDict.union st.classes st'.classes;
      }

    let has_def : string -> t -> bool =
     fun s x ->
      TDict.mem s x.defs
      || TDict.mem Identifier.(RootType s |> to_string) x.defs

    let has_class : string -> t -> bool = fun s x -> TDict.mem s x.classes

    let defs : t -> (string * TypeDef.u) list = fun x -> TDict.bindings x.defs

    let classes : t -> (string * TypeDef.ClassDef.t) list =
     fun x -> TDict.bindings x.classes

    let imports : t -> (string * (string option * string list)) list =
     fun x -> ISet.index x.imports
  end

  (** Monad that associates an AST node with the contextual state: all non-builtin items that must be imported
in the codec module preamble, and all sub-codecs that must be generated in scope *)
  type 'a t = 'a * State.t

  let return : 'a. 'a -> 'a t = fun x -> (x, State.empty)

  let pure = return

  let add : 'a. string -> 'a t -> 'a t = fun s -> sem (State.add_import s)

  let add_def : id:string -> entry:TypeDef.u -> 'a t -> 'a t =
   fun ~id ~entry -> sem (State.add_definition ~id ~entry)

  let add_class : key:string -> entry:TypeDef.ClassDef.t -> 'a t -> 'a t =
   fun ~key ~entry -> sem (State.add_class ~key ~entry)

  let bind : 'a t -> ('a -> 'b t) -> 'b t =
   fun (x, st) f ->
    let y, st' = f x in
    (y, State.union st st')

  let ( >>= ) = bind

  let ( >> ) x a = bind x (fun _ -> a)

  let obind : 'a t option -> ('a -> 'b t option) -> 'b t option = function
    | None -> fun _ -> None
    | Some (x, st) -> (
        fun f ->
          match f x with
          | None -> None
          | Some (y, st') -> Some (y, State.union st st'))

  let ( >>? ) = obind

  let fmap : ('a -> 'b) -> 'a t -> 'b t = fun f (x, st) -> (f x, st)

  let omap : ('a -> 'b) -> 'a t option -> 'b t option =
   fun f -> Option.map (fmap f)

  let forget : 'a. 'a t -> 'a = fun (x, _) -> x

  let void : 'a. 'a t -> unit t = fun (_, st) -> ((), st)

  let take : 'a. 'a t -> 'a * unit t = fun (x, st) -> (x, ((), st))

  let ovoid : 'a. 'a t option -> unit t = function
    | None -> pure ()
    | Some (_, st) -> ((), st)

  let ( >$< ) x f = fmap f x

  let ( <$> ) f x = fmap f x

  let ( >?< ) x f = omap f x

  let ( <?> ) f x = omap f x

  let fmerge : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t =
   fun f (a, st_a) (b, st_b) -> (f a b, State.union st_a st_b)

  let seq : 'a t list -> 'a list t =
   fun x -> List.fold_right (fmerge List.cons) x (pure [])

  let sseq : ('a * 'b t) list -> ('a * 'b) list t =
   fun xs ->
    List.fold_right
      (fun (a, x) acc -> fmerge List.cons (x >$< fun y -> (a, y)) acc)
      xs
      (pure [])

  module Let_syntax = struct
    let ( let+ ) x f = fmap f x

    let ( and+ ) : 'a t -> 'b t -> ('a * 'b) t =
     fun (x, st) (y, st') -> ((x, y), State.union st st')

    let ( let* ) = bind

    let ( and* ) : 'a t -> 'b t -> ('a * 'b) t =
     fun (x, st) (y, st') -> ((x, y), State.union st st')

    let ( let*? ) = obind
  end

  include Let_syntax
end

module Traversal = struct
  module SS = Set.Make (String)

  type v = (string * TypeDef.u) list * (string * TypeDef.ClassDef.t) list

  type u = {defs : SS.t; classes : SS.t}

  let debug : u -> v -> unit =
   fun {defs; classes} (defs', classes') ->
    let open Common.Printer in
    let _covered_defs, uncovered_defs =
      List.partition (fun (id, _) -> SS.mem id defs) defs'
    in
    let _covered_classes, uncovered_classes =
      List.partition (fun (id, _) -> SS.mem id classes) classes'
    in
    let oput =
      str "Definitions: "
      |>> wrap
            ((* chr '\n' (* *)|>> str "Seen: " |>> wrap (chr '\n' |>> seq
                ~sep:(str ", ") str (SS.elements defs)) |>> chr '\n' |>> str
                "Covered: " |>> wrap (chr '\n' |>> seq ~sep:(str ", ") str
                (covered_defs |> List.map fst)) |>> *)
             chr '\n'
            |>> str "Uncovered: "
            |>> wrap
                  (chr '\n'
                  |>> seq ~sep:(str ", ") str (uncovered_defs |> List.map fst))
            )
      |>> chr '\n' |>> str "Classes: "
      |>> wrap
            ((* chr '\n' |>> str "Seen: " |>> wrap (chr '\n' |>> seq ~sep:(str
                ", ") str (SS.elements classes)) |>> chr '\n' |>> str "Covered:
                " |>> wrap (chr '\n' |>> seq ~sep:(str ", ") str
                (covered_classes |> List.map fst) ) |>> *)
             chr '\n'
            |>> str "Uncovered: "
            |>> wrap
                  (chr '\n'
                  |>> seq ~sep:(str ", ") str (uncovered_classes |> List.map fst)
                  ))
    in
    oput $ Doc stderr

  let preclude : u -> v -> v =
   fun {defs; classes} (xs, ys) ->
    let f ss (id, _) = not (SS.mem id ss) in
    (List.filter (f defs) xs, List.filter (f classes) ys)

  let undiscovered : u -> v -> bool =
   fun {defs; classes} (xs, ys) ->
    let f ss (id, _) = not (SS.mem id ss) in
    List.exists (f defs) xs || List.exists (f classes) ys

  let rec walk :
      acc:'b list Ctxt.t ->
      visited:SS.t ->
      f:(string * 'a -> 'b Ctxt.t) ->
      (string * 'a) list ->
      SS.t * 'b list Ctxt.t =
   fun ~acc ~visited ~f -> function
    | [] -> (visited, acc)
    | ((id, _) as h) :: t ->
        if SS.mem id visited then walk ~acc ~visited ~f t
        else
          walk
            ~acc:(Ctxt.fmerge List.cons (f h) acc)
            ~visited:(SS.add id visited)
            ~f
            t

  let scan :
      acc:ts_decl list Ctxt.t ->
      visited:u ->
      f:(string * TypeDef.u -> ts_decl Ctxt.t) ->
      g:(string * TypeDef.ClassDef.t -> ts_decl Ctxt.t) ->
      v ->
      u * ts_decl list Ctxt.t =
   fun ~acc ~visited ~f ~g (ds, cs) ->
    let defs, acc' = walk ~acc ~visited:visited.defs ~f ds in
    let classes, acc'' = walk ~acc:acc' ~visited:visited.classes ~f:g cs in
    ({defs; classes}, acc'')

  let rec traverse' :
      acc:ts_decl list Ctxt.t ->
      recursion_limit:int ->
      visited:u ->
      f:(string * TypeDef.u -> ts_decl Ctxt.t) ->
      g:(string * TypeDef.ClassDef.t -> ts_decl Ctxt.t) ->
      v ->
      ts_decl list Ctxt.t =
   fun ~acc ~recursion_limit ~visited ~f ~g -> function
    | [], [] ->
        let remaining =
          preclude
            visited
            (Ctxt.State.defs (snd acc), Ctxt.State.classes (snd acc))
        in
        if undiscovered visited remaining then
          if recursion_limit > 0 then
            traverse'
              ~acc
              ~recursion_limit:(recursion_limit - 1)
              ~visited
              ~f
              ~g
              (preclude visited remaining)
          else (
            debug visited remaining ;
            assert false)
        else acc
    | items ->
        let visited', ((_, st) as acc') =
          scan ~acc:(Ctxt.pure []) ~visited ~f ~g items
        in
        traverse'
          ~acc:(Ctxt.fmerge List.append acc' acc)
          ~recursion_limit
          ~visited:visited'
          ~f
          ~g
          (preclude visited' (Ctxt.State.defs st, Ctxt.State.classes st))

  let traverse ~f ~g items =
    traverse'
      ~acc:(Ctxt.pure [])
      ~recursion_limit:16
      ~visited:{defs = SS.empty; classes = SS.empty}
      ~f
      ~g
      items
end

module TypeConv = struct
  include TypeDef

  let rec of_base :
      ?force_codec:bool -> Simplified.base_t -> Type.Atom.t Ctxt.t option =
    let pure_atom = function
      | Type.Atom x -> Ctxt.pure x
      | _ -> invalid_arg "pure_atom"
    in
    fun ?(force_codec = false) -> function
      | NamedRef (name, Abstract) ->
          let name' =
            Identifier.(
              to_string
              @@ if force_codec then Class (RawName name) else RootType name)
          in
          Some (pure_atom @@ Type.ldef name')
      | NamedRef (name, Concrete def) ->
          let open Ctxt in
          let*? tdef = of_simplified def in
          let name' =
            Identifier.(
              to_string
              @@ if force_codec then Class (RawName name) else RootType name)
          in
          Some
            Ctxt.(
              (if force_codec then
               add_class
                 ~key:name'
                 ~entry:
                   {
                     class_name = name';
                     unboxed_name = Identifier.to_string (RootType name);
                     tdef;
                   }
              else Fun.id)
              @@ add_def ~id:name ~entry:(Raw def) (pure_atom @@ Type.ldef name'))
      | Bool -> Some (Ctxt.add "Bool" @@ pure_atom Type.bool)
      | Unit -> Some (Ctxt.add "Unit" @@ pure_atom Type.unit)
      | Num (Int int_t) -> (
          match int_t with
          | NativeInt nat ->
              Some
                (Ctxt.add (Type.Prim.Int.to_string nat)
                @@ Ctxt.pure Type.(Atom.Prim (Int nat)))
          | RangedInt {repr; minimum; maximum} ->
              let kind : Type.Prim.Int.Kind.t =
                match repr with
                | `Uint8 -> Uint8Kind
                | `Int8 -> Int8Kind
                | `Uint16 -> Uint16Kind
                | `Int16 -> Int16Kind
                | `Uint30 -> Uint30Kind
                | `Int31 -> Int31Kind
              in
              let raw =
                Ctxt.add (Type.Prim.Int.Kind.to_string kind)
                @@ pure_atom (Type._RangedInt kind minimum maximum)
              in
              Some (Ctxt.add "RangedInt" @@ raw))
      | Num (Float Double) -> Some (Ctxt.add "Double" @@ pure_atom Type.double)
      | Num (Float (RangedDouble {minimum; maximum})) ->
          Some
            (Ctxt.add "RangedDouble" @@ pure_atom
            @@ Type._RangedDouble minimum maximum)
      | ZarithN -> Some (Ctxt.add "N" @@ pure_atom Type._N)
      | ZarithZ -> Some (Ctxt.add "Z" @@ pure_atom Type._Z)
      | Fixed (Bytes n) ->
          Some (Ctxt.add "FixedBytes" @@ pure_atom @@ Type._FixedBytes n)
      | Fixed (String n) ->
          Some (Ctxt.add "FixedString" @@ pure_atom @@ Type._FixedString n)
      | Var VBytes -> Some (Ctxt.add "Bytes" @@ pure_atom Type._Bytes)
      | Var VString -> Some (Ctxt.add "U8String" @@ pure_atom Type._U8String)

  and repad : ?virt:bool -> int -> Simplified.t -> Type.Atom.t Ctxt.t option =
    let pure_atom = function
      | Type.Atom x -> Ctxt.pure x
      | _ -> invalid_arg "pure_atom"
    in
    fun ?(virt = false) n ->
      let f x =
        if virt then Some Ctxt.(add "VPadded" @@ pure_atom (Type._VPadded x n))
        else Some Ctxt.(add "Padded" @@ pure_atom (Type._Padded x n))
      in
      fun x -> Ctxt.(of_simple ~force_codec:true x >>? f)

  and of_comp : Simplified.comp_t -> Type.Atom.t Ctxt.t option =
    (* FIXME[epic=scaffolding] - Fill in unimplemented cases *)
    let pure_atom = function
      | Type.Atom x -> Ctxt.pure x
      | _ -> invalid_arg "pure_atom"
    in
    function
    | Seq {elt; len_enc; lim} ->
        let f x =
          match (lim, len_enc) with
          | No_limit, None ->
              Some Ctxt.(add "Sequence" @@ pure_atom (Type._Sequence x))
          | At_most n, lenc ->
              Some
                Ctxt.(
                  add "SequenceBounded"
                  @@ pure_atom (Type._SequenceBounded ?lenc x n))
          | Exactly n, None ->
              Some
                Ctxt.(
                  add "SequenceFixed" @@ pure_atom (Type._SequenceFixed x n))
          | (No_limit | Exactly _), Some _ -> assert false
        in
        Ctxt.(of_simple ~force_codec:true elt >>? f)
    | Opt {has_tag; enc} ->
        let f x =
          if has_tag then Some Ctxt.(add "Option" @@ pure_atom (Type._Option x))
          else Some Ctxt.(add "Nullable" @@ pure_atom (Type._Nullable x))
        in
        Ctxt.(of_simple ~force_codec:true enc >>? f)
    | Padded (n, x) -> repad n x
    | VPadded (n, x) -> repad ~virt:true n x
    | Dyn (lp, x) ->
        Ctxt.(
          of_simple ~force_codec:true x >>? fun x ->
          Some (add "Dynamic" @@ add "width" @@ pure_atom (Type._Dynamic x lp)))

  and of_prod : Simplified.prod_t -> t Ctxt.t option = function
    | Record {fields; _} ->
        let cata fname acc x =
          match (acc, x) with
          | Fields xs, Monic field_type ->
              let field_name = Identifier.(FieldName fname |> to_string) in
              Fields (xs @ [(field_name, field_type)])
          | Fields _xs, Elems _ -> invalid_arg "Cannot fuse elems into fields"
          | Fields _xs, Fields _ -> invalid_arg "Cannot fuse fields into fields"
          | _, _ -> invalid_arg "incoherent state reached"
        in
        List.fold_left
          (fun acc (fname, x) ->
            if fname = "kind" && x = Common.Simplified.Base Unit then acc
            else
              Ctxt.(
                let*? acc = acc in
                let*? x = of_simplified ~force_codec:true x in
                Some (return (cata fname acc x))))
          (Some (Ctxt.pure (Fields [])))
          fields
    | Tuple {elems; _} ->
        let cata acc x =
          match (acc, x) with
          | Elems xs, Monic flat -> Elems (xs @ [flat])
          | _ -> invalid_arg "cata"
        in
        List.fold_left
          (fun acc x ->
            Ctxt.(
              let*? acc = acc in
              let*? x = of_simplified ~force_codec:true x in
              Some (return @@ cata acc x)))
          (Some (Ctxt.return @@ Elems []))
          elems

  and of_sum : Simplified.sum_t -> t Ctxt.t option = function
    | CStyle {size; enums; _} ->
        let sz' = size in
        let elems' =
          List.map
            (fun (cname, tag) ->
              (Identifier.(ConstructorName cname |> to_string), tag))
            enums
          |> List.sort (fun (_, tag0) (_, tag1) -> Int.compare tag0 tag1)
        in
        Some Ctxt.(add "width" @@ pure @@ CStyle (sz', elems'))
    | Data {size; vars; _} ->
        let lp' = size in
        let cata vtag vname acc x =
          match acc with
          | Variants (lp, xs) ->
              let new_var =
                (vtag, (Identifier.(ConstructorName vname |> to_string), x))
              in
              Variants (lp, new_var :: xs)
          | _ -> failwith "of_sum"
        in
        List.fold_left
          (fun acc (vtag, (vname, x)) ->
            Ctxt.(
              let*? acc = acc in
              let*? x = of_simplified ~force_codec:true x in
              Some (return @@ (cata vtag vname) acc x)))
          (Some Ctxt.(add "width" @@ pure (Variants (lp', []))))
          (List.sort (fun (tag0, _) (tag1, _) -> Int.compare tag1 tag0) vars)

  and of_simple : ?force_codec:bool -> Simplified.t -> Type.Atom.t Ctxt.t option
      =
   fun ?(force_codec = false) -> function
    | Base t -> of_base ~force_codec t
    | Comp t -> of_comp t
    | _ -> None

  and of_simplified : ?force_codec:bool -> Simplified.t -> t Ctxt.t option =
   fun ?(force_codec = false) -> function
    | (Base _ | Comp _) as simple ->
        Ctxt.(of_simple ~force_codec simple >?< fun x -> Monic x)
    | Prod t -> of_prod t
    | Sum t -> of_sum t
end

module Snippets = struct
  module Encode = struct
    let add_depends : 'a. 'a Ctxt.t -> 'a Ctxt.t =
     fun x -> Ctxt.add "OutputBytes" x

    let declare :
        fname:string ->
        [`Const | `Func] ->
        ty:Type.t ->
        ?param_name:string ->
        ts_expr list ->
        ts_decl =
     fun ~fname decl_kind ~ty ?param_name calls ->
      let body =
        match calls with
        | [] -> [Stmt.return (Expr.ArrayOf [])]
        | [x] -> [Stmt.return x]
        | _ :: _ :: _ as xs -> [Stmt.Return (Expr.concat_bytes xs)]
      in
      let sg =
        match param_name with
        | Some param_name ->
            Decl.Fun.Sig.([param_name #: ty] @: Type._OutputBytes)
        | None -> Decl.Fun.Sig.([] @: Type._OutputBytes)
      in
      match decl_kind with
      | `Const -> Decl._constLambda fname Decl.Fun.Arrow.(sg => body)
      | `Func -> Decl._function fname sg body
  end

  module Decode = struct
    let add_depends : 'a. 'a Ctxt.t -> 'a Ctxt.t = fun x -> Ctxt.add "Parser" x

    let declare :
        type a.
        fname:string ->
        [`Const | `Func] ->
        ty:Type.t ->
        ?param_name:string ->
        of_atom:(a -> Ast.Expr.t) ->
        a ->
        ts_decl =
     fun ~fname decl_kind ~ty ?(param_name = "p") ~of_atom atom ->
      let body = [Stmt.return @@ of_atom atom] in
      let sg = Decl.Fun.Sig.([param_name #: Type._Parser] @: ty) in
      match decl_kind with
      | `Const -> Decl._constLambda fname Decl.Fun.Arrow.(sg => body)
      | `Func -> Decl._function fname sg body
  end
end

let call_transcoder :
    ?apply:bool ->
    dir:dir ->
    ?tname:string ->
    def:TypeDef.t ->
    Expr.t ->
    Expr.t Ctxt.t =
 fun ?(apply = true) ~dir ?tname ~def operands ->
  let call_method ~apply : Type.Atom.t * Expr.t -> Expr.t =
    match dir with
    | Encode ->
        fun (_ty, x) ->
          let meth = Expr.(x $. "encode") in
          if apply then Expr.(meth.$([])) else meth
    | Decode ->
        fun (ty, x) ->
          let rec f : Type.Atom.t -> Expr.t = function
            | Comp typ ->
                let cstr = Type.Comp.class_string typ in
                let args =
                  match typ with
                  | Dynamic (t, lp) ->
                      let went =
                        Expr.loc_ent (Type.Runtime.Width.to_string lp)
                      in
                      [f t; went]
                  | Option t | Nullable t | Sequence t -> [f t]
                  | Padded (t, n) | VPadded (t, n) | SequenceFixed (t, n) ->
                      [f t; Expr.(Literal (LitNumber n))]
                  | SequenceBounded (t, n, lenc) ->
                      [
                        f t;
                        Expr.(Literal (LitNumber n));
                        Expr.(
                          match lenc with
                          | Some x ->
                              (loc_ent "Some").$([
                                                   loc_ent
                                                     (Type.Runtime.Width
                                                      .to_string
                                                        x);
                                                 ])
                          | None -> loc_ent "None");
                      ]
                in
                Expr.((loc_ent cstr $. "decode").$(args))
            | LDef x ->
                Expr.(
                  loc_ent Identifier.(Class (RawName x) |> to_string)
                  $. "decode")
            | Prim (FixedBytes n | FixedString n) as typ ->
                let ext = Expr.(RecordOf [("len", Literal (LitNumber n))]) in
                Expr.(
                  (loc_ent (Type.Atom.to_string ~static:"decode" typ)).$([ext]))
            | typ -> Expr.(loc_ent (Type.Atom.to_string ~static:"decode" typ))
          in
          let meth = f ty in
          if apply then Expr.(meth.$([x])) else meth
  in
  let cond meth args = if apply then Expr.(meth.$(args)) else meth in
  match (def, operands) with
  | Monic x, y -> Ctxt.pure @@ call_method ~apply (x, y)
  | Elems elts, y -> (
      match dir with
      | Encode ->
          let meth = Expr.(rt_ent Tuple__tuple_encoder) in
          Ctxt.(add "tuple_encoder" @@ pure @@ cond meth [y])
      | Decode ->
          Ctxt.(
            let* param =
              match tname with
              | None -> pure (Type.Tuple elts)
              | Some x ->
                  if String.starts_with ~prefix:"CGRIDClass" x then
                    add "Unboxed" @@ pure Type.(_Unboxed @@ ldef x)
                  else pure (Type.ldef x)
            in
            let decs =
              List.map (fun eltt -> call_method ~apply:false (eltt, y)) elts
            in
            add "tuple_decoder" @@ pure
            @@ cond Expr.((rt_ent Tuple__tuple_decoder).>([param]).$(decs)) [y])
      )
  | Fields flds, y -> (
      match dir with
      | Encode ->
          let hint =
            Expr.RecordOf
              [
                ( "order",
                  Expr.ArrayOf
                    (List.map
                       (fun x -> Expr.(Literal (LitString (fst x))))
                       flds) );
              ]
          in
          Ctxt.(
            add "record_encoder" @@ pure
            @@ cond Expr.((rt_ent Record__record_encoder).$([hint])) [y])
      | Decode ->
          let open Ctxt in
          let* param =
            match tname with
            | None -> return (Type.Record flds)
            | Some x ->
                if String.starts_with ~prefix:"CGRIDClass" x then
                  add "Unboxed" @@ return @@ Type.(_Unboxed @@ ldef x)
                else return @@ Type.ldef x
          in
          let decs =
            Expr.RecordOf
              (List.map
                 (fun (fname, ftype) ->
                   (fname, call_method ~apply:false (ftype, y)))
                 flds)
          in
          let hint =
            Expr.RecordOf
              [
                ( "order",
                  Expr.ArrayOf
                    (List.map
                       (fun x -> Expr.(Literal (LitString (fst x))))
                       flds) );
              ]
          in
          Ctxt.(
            add "record_decoder" @@ pure
            @@ cond
                 Expr.(
                   (rt_ent Record__record_decoder).>([param]).$([decs; hint]))
                 [y]))
  | CStyle (sz, _), y -> (
      let width = Expr.loc_ent (Type.Runtime.Width.to_string (sz :> Type.Runtime.Width.t)) in
      match dir with
      | Encode ->
          let open Ctxt in
          let* param =
            match tname with
            | None -> invalid_arg "call_transcoder"
            | Some x ->
                if String.starts_with ~prefix:"CGRIDClass" x then
                  add "Unboxed" @@ pure Type.(_Unboxed @@ ldef x)
                else pure (Type.ldef x)
          in
          add "enum_encoder" @@ pure
          @@ cond Expr.((rt_ent Enum__enum_encoder).$([width]).>([param])) [y]
      | Decode ->
          let qualt, obje =
            match tname with
            | None -> invalid_arg "call_transcoder"
            | Some x -> (Type.ldef x, Expr.Entity.loc x)
          in
          let predicate = Expr.EnumPredicate (qualt, obje) in
          Ctxt.(
            add "enum_decoder" @@ pure
            @@ cond
                 Expr.((rt_ent Enum__enum_decoder).$([width]).$([predicate]))
                 [y]))
  | Variants (sz, _), y -> (
      let width =
        Expr.loc_ent (Type.Runtime.Width.to_string (sz :> Type.Runtime.Width.t))
      in
      match dir with
      | Encode ->
          let open Ctxt in
          let* param =
            match tname with
            | None -> invalid_arg "call_transcoder"
            | Some x ->
                if String.starts_with ~prefix:"CGRIDClass" x then
                  add "Unboxed" @@ pure Type.(_Unboxed @@ ldef x)
                else pure (Type.ldef x)
          in
          add "variant_encoder" @@ add "KindOf" @@ pure
          @@ cond
               Expr.(
                 (rt_ent Adt__variant_encoder).>([Type._KindOf param; param]).$([
                                                                                width;
                                                                                ]))
               [y]
      | Decode ->
          let rawname =
            match tname with
            | None -> invalid_arg "call_transcoder"
            | Some x -> x
          in
          let decoder_thunk =
            Expr.loc_ent
              Identifier.(AssociatedMethod (rawname, "mkDecoder") |> to_string)
          in
          Ctxt.(
            add "variant_decoder" @@ pure
            @@ cond
                 Expr.(
                   (rt_ent Adt__variant_decoder).$([width]).$([
                                                                decoder_thunk.$([]);
                                                              ]))
                 [y]))

module Gen = struct
  let _mk_encoder :
      ?is_boxed:bool ->
      fname:string ->
      rawname:string ->
      Identifier.t ->
      TypeDef.t ->
      ts_decl Ctxt.t =
   fun ?(is_boxed = false) ~fname ~rawname type_ident ->
    let dir = Encode in
    let tname = Identifier.(to_string @@ RawName rawname) in
    let this_name = Identifier.to_string type_ident in
    let param_name, operand =
      if is_boxed then (None, Expr.(ent This $. "value"))
      else (Some "value", Expr.loc_ent "value")
    in
    Ctxt.(
      fun def ->
        let of_atom = call_transcoder ~dir ~tname ~def in
        Snippets.Encode.(
          declare ~fname `Const ?param_name ~ty:(Atom (LDef this_name))
          <$> add_depends (seq [of_atom operand])))

  let mk_encoder :
      ?preserve_name:bool ->
      ?is_boxed:bool ->
      string ->
      TypeDef.t ->
      ts_decl Ctxt.t =
   fun ?(preserve_name = false) ?is_boxed rawname ->
    let fname = mk_fname Encode rawname |> Identifier.to_string in
    let type_ident = Identifier.(RootType rawname) in
    let rawname =
      if preserve_name then rawname
      else Identifier.(RootType rawname |> to_string)
    in
    _mk_encoder ?is_boxed ~fname ~rawname type_ident

  let _mk_decoder :
      ?is_boxed:bool ->
      fname:string ->
      rawname:string ->
      Identifier.t ->
      TypeDef.t ->
      (ts_blpl * ts_decl, ts_decl) Either.t Ctxt.t =
   fun ?(is_boxed = false) ~fname ~rawname type_ident ->
    let dir = Decode in
    let tname = Identifier.(to_string @@ RawName rawname) in
    let this_name = Identifier.(to_string @@ type_ident) in
    let param_name = "p" in
    let of_atom x =
      (* FIXME[epic=scaffolding] - implement proper AST node for 'new this'
         constructions *)
      if is_boxed then Expr.((loc_ent "new this").$([x])) else x
    in
    Ctxt.(
      function
      | (Monic _ | Elems _ | Fields _ | CStyle _) as def ->
          let* elt =
            call_transcoder ~dir ~tname ~def (Expr.loc_ent param_name)
          in
          Snippets.Decode.(
            add_depends @@ return
            @@ Either.Right
                 (declare
                    ~fname
                    `Const
                    ~param_name
                    ~ty:(Atom (LDef this_name))
                    ~of_atom
                    elt))
      | Variants (_, vars) as def ->
          let hybrid, ctxt' =
            let kind = Identifier.(TagEnum rawname |> to_string) in
            let rtype =
              Type._VariantDecoder (Type.ldef kind) (Type.ldef tname)
            in
            let cases, ctxt' =
              let ccases =
                Ctxt.seq
                  (List.map
                     (fun (_, (c, _)) ->
                       let classname =
                         Identifier.(Class (SubType (rawname, c)) |> to_string)
                       in
                       Ctxt.(
                         call_transcoder
                           ~dir:Decode
                           ~def:(TypeDef.Monic (LDef classname))
                           ~apply:false
                           Expr.FORBIDDEN
                         >$< fun x' -> (c, x')))
                     vars)
              in
              Ctxt.take (Ctxt.add "VariantDecoder" ccases)
            in
            ( Ast.Boilerplate._HybridDecoder
                ~outer:
                  Identifier.(
                    AssociatedMethod (tname, "mkDecoder") |> to_string)
                ~kind
                ~rtype
                cases,
              ctxt' )
          in
          Ctxt.(
            let* () = void ctxt' in
            let* elt =
              call_transcoder ~dir ~tname ~def (Expr.loc_ent param_name)
            in
            Snippets.Decode.add_depends @@ return
            @@ Either.Left
                 ( hybrid,
                   Snippets.Decode.(
                     declare
                       ~fname
                       `Const
                       ~param_name
                       ~ty:(Atom (LDef this_name))
                       ~of_atom
                       elt) )))

  let mk_decoder :
      ?preserve_name:bool ->
      ?is_boxed:bool ->
      string ->
      TypeDef.t ->
      (ts_blpl * ts_decl, ts_decl) Either.t Ctxt.t =
   fun ?(preserve_name = false) ?is_boxed rawname ->
    let fname = mk_fname Decode rawname |> Identifier.to_string in
    let type_ident = Identifier.(RootType rawname) in
    let rawname =
      if preserve_name then rawname
      else Identifier.(RootType rawname |> to_string)
    in
    _mk_decoder ?is_boxed ~fname ~rawname type_ident

  let mk_meter : TypeDef.t -> Decl.Class.Item.t Ctxt.t =
   fun rep ->
    let open Ctxt in
    let of_size = function
      | `Uint8 -> Expr.Literal (LitNumber 1)
      | `Uint16 -> Expr.Literal (LitNumber 2)
      | `Uint30 -> Expr.Literal (LitNumber 4)
    in
    let base = Expr.(ent Entity.This $. "value") in
    let join f = Expr.(f base $. "encodeLength") in
    let* body =
      let ret =
        match rep with
        | Monic _ -> join (fun x -> x)
        | Elems elts ->
            Expr.(SumTerms (List.mapi (fun i _ -> join (fun x -> x.![i])) elts))
        | Fields flds ->
            Expr.(
              SumTerms
                (List.map
                   (fun (fldname, _) -> join (fun x -> x $. fldname))
                   flds))
        | CStyle (sz, _) -> of_size sz
        | Variants (sz, _) ->
            Expr.(SumTerms [of_size sz; join (fun x -> x $. "value")])
      in
      return (Stmt.Return ret)
    in
    return
      (Decl.Class.Item.Method
         ( Instance,
           ( "get encodeLength",
             Decl.Fun.Arrow.(Decl.Fun.Sig.([] @: Type.verb "number") => [body])
           ) ))

  (* FIXME - implement write target logic *)
  let mk_write_target : TypeDef.t -> Decl.Class.Item.t Ctxt.t =
   fun rep ->
    let open Ctxt in
    let of_size (sz : Simplified.lenpref) expr =
      add "Width" @@ pure
      @@ Expr.(
           (loc_ent "Width" $. "from").$([
                                           loc_ent
                                             (Type.Runtime.Width.to_string sz);
                                           expr;
                                         ]))
    in
    let base = Expr.(ent Entity.This $. "value") in
    let join f =
      let* x = f base in
      return Expr.((x $. "writeTarget").$([loc_ent "tgt"]))
    in
    let* body =
      let* ret =
        match rep with
        | Monic _ -> join pure
        | Elems elts ->
            let* terms =
              seq
                (List.mapi
                   (fun i _ -> join (fun x -> return Expr.(x.![i])))
                   elts)
            in
            return Expr.(SumTerms terms)
        | Fields flds ->
            let* terms =
              seq
                (List.map
                   (fun (fldname, _) ->
                     join Expr.(fun x -> return @@ (x $. fldname)))
                   flds)
            in
            return @@ Expr.(SumTerms terms)
        | CStyle (sz, _) -> join (fun x -> of_size (sz :> Common.Simplified.lenpref) x)
        | Variants (sz, _) ->
            let* term0 =
              join (fun x ->
                  (of_size (sz :> Simplified.lenpref)) Expr.(x $. "kind"))
            in
            let* terms = join (fun x -> return Expr.(x $. "value")) in
            return Expr.(SumTerms [term0; terms])
      in
      return (Stmt.Return ret)
    in
    add "Target"
    @@ return
         (Decl.Class.Item.Method
            ( Instance,
              ( "writeTarget",
                Decl.Fun.Arrow.(
                  Decl.Fun.Sig.(["tgt" #: Type._Target] @: Type.verb "number")
                  => [body]) ) ))

  let _mk_class :
      class_name:string -> unboxed_name:string -> TypeDef.t -> ts_decl Ctxt.t =
   fun ~class_name ~unboxed_name rep ->
    Ctxt.(
      let* encode_method =
        _mk_encoder
          ~is_boxed:true
          ~fname:"encode"
          ~rawname:unboxed_name
          Identifier.(Class (RawName class_name))
          rep
        >$< function
        | DConst (ConstArrow (_, _, x)) | DFunction (_, x) ->
            Decl.Class.Item.Method (Instance, ("encode", x))
        | _ -> invalid_arg "mk_class.encode_method"
      in
      let* extra, decode_method =
        let* edec =
          _mk_decoder
            ~is_boxed:true
            ~fname:"decode"
            ~rawname:unboxed_name
            Identifier.(Class (RawName class_name))
            rep
        in
        match edec with
        | Either.Right (DConst (ConstArrow (_, _, x)) | DFunction (_, x)) ->
            return (None, Decl.Class.Item.Method (Static, ("decode", x)))
        | Either.Left (blpl, (DConst (ConstArrow (_, _, x)) | DFunction (_, x)))
          ->
            return (Some blpl, Decl.Class.Item.Method (Static, ("decode", x)))
        | _ -> invalid_arg "mk_class.decode_method"
      in
      let* encodeLength_method = mk_meter rep in
      let* writeTarget_method = mk_write_target rep in
      add "Box" @@ add "Codec" @@ return
      @@ (fun x ->
           match extra with
           | None -> x
           | Some blpl -> Decl.DMulti [DBoilerplate blpl; x])
      @@ Decl._class
           class_name
           ~extends:(Printf.sprintf "Box<%s>" unboxed_name)
           ~implements:["Codec"]
           [
             encode_method;
             decode_method;
             encodeLength_method;
             writeTarget_method;
           ])

  let mk_class : rawname:string -> TypeDef.t -> ts_decl Ctxt.t =
   fun ~rawname ->
    let class_name = Identifier.(Class (RootType rawname) |> to_string)
    and unboxed_name = Identifier.(RootType rawname |> to_string) in
    _mk_class ~class_name ~unboxed_name

  let mk_definition :
      ?preserve_name:bool -> string -> TypeDef.t -> ts_decl Ctxt.t =
   fun ?(preserve_name = false) name ->
    let typename =
      Identifier.(
        (if preserve_name then RawName name else RootType name) |> to_string)
    in
    let mk_rhs : TypeDef.t -> Type.t = function
      | Monic x -> Type.Atom x
      | Elems els -> Type.Tuple els
      | Fields flds -> Type.Record flds
      | _ -> invalid_arg "mk_rhs"
    in
    let mk_codec : ?id:string -> ?constr:string -> TypeDef.t -> Type.t Ctxt.t =
      let open Ctxt in
      fun ?id ?constr tdef ->
        match tdef with
        | Monic (LDef _ as x) -> return (Type.Atom x)
        | Monic _ | Elems _ | Fields _ ->
            let subtype =
              match (constr, id) with
              | Some constr, None -> Identifier.SubType (name, constr)
              | None, Some id -> Identifier.RootType id
              | Some _, Some _ | None, None -> invalid_arg "mk_codec"
            in
            let unboxed_name = Identifier.(subtype |> to_string)
            and class_name = Identifier.(Class subtype |> to_string) in
            let entry : TypeDef.ClassDef.t = {class_name; unboxed_name; tdef} in
            add_def ~id:unboxed_name ~entry:(Expanded tdef)
            @@ add_class ~key:class_name ~entry
            @@ return (Type.ldef class_name)
        | _ -> invalid_arg "mk_codec"
    in
    Ctxt.(
      fun tdef ->
        match tdef with
        | (Monic _ | Elems _ | Fields _) as rep ->
            return @@ Ast.Decl._type typename (mk_rhs rep)
        | CStyle (sz, elems) -> return @@ Ast.Decl._enum ~size:sz typename elems
        | Variants (sz, vars) ->
            let tag_enum_name = Identifier.(TagEnum typename |> to_string) in
            let tag_enum =
              let tag_enum_body = List.map (fun (i, (c, _)) -> (c, i)) vars in
              Ast.Decl._enum
                ~size:(sz :> Common.Simplified.enum_size)
                tag_enum_name
                tag_enum_body
            in
            let typemap_name = Identifier.(SubtypeMap typename |> to_string) in
            let* typemap =
              let f : int * (string * TypeDef.t) -> (string * Type.t) Ctxt.t =
               fun (_, (c, x)) ->
                let* x' = mk_codec ~constr:c x in
                return (c, x')
              in
              Decl._typeMap typemap_name <$> seq (List.map f vars)
            in
            return
            @@ Ast.Decl.DMulti
                 [
                   tag_enum;
                   typemap;
                   Ast.Decl._typeUnion typename
                   @@ List.map
                        (fun (_, (c, _)) ->
                          Type.Record
                            [
                              ("kind", Type.inEnum tag_enum_name c);
                              ("value", Type.inMap typemap_name c);
                            ])
                        vars;
                 ])

  let mk_payload : string -> TypeDef.t -> ts_decl list Ctxt.t =
   fun name rep ->
    let open Ctxt in
    let* definition = mk_definition name rep in
    let* cdef = mk_class ~rawname:name rep in
    let* encoder = mk_encoder name rep in
    let* decoder =
      let* extdecoder = mk_decoder name rep in
      match extdecoder with
      | Left (_ext, decoder) -> return decoder
      | Right decoder -> return decoder
    in
    return [definition; cdef; encoder; decoder]

  let traverse : _ Ctxt.t -> ts_decl list Ctxt.t =
   fun (_, st) ->
    let items = Ctxt.State.(defs st, classes st) in
    let open Ctxt in
    let f (id, def) =
      match def with
      | TypeDef.Raw def -> (
          match TypeConv.of_simplified def with
          | None -> assert false
          | Some def -> def >>= mk_definition id)
      | TypeDef.Expanded def -> mk_definition ~preserve_name:true id def
    and g (_, TypeDef.ClassDef.{class_name; unboxed_name; tdef}) =
      let* classdec = _mk_class ~class_name ~unboxed_name tdef in
      return
        (Decl.Commented
           (Header
              ( ( Printf.sprintf
                    "Class %s generated for %s"
                    class_name
                    unboxed_name,
                  `Line ),
                classdec )))
    in
    Traversal.traverse ~f ~g items

  (* FIXME[epic=bug] - figure out how to recurse into classes while defining
     classes *)
  let generate : string -> Simplified.t -> ts_module =
   fun schema_name rep ->
    let payload =
      Ctxt.(
        TypeConv.of_simplified ~force_codec:true rep
        |> Option.get >>= mk_payload schema_name)
    in
    let extra = traverse payload in
    let decls, st' = Ctxt.fmerge List.append extra payload in
    let preamble =
      Ctxt.State.imports st'
      |> List.map (fun (from, (default, named)) ->
             Ast.Preamble.ImportSelected {default; named; from})
    in
    {preamble; decls}
end
