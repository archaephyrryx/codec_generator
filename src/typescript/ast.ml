module Common = Common

let addstr = Common.Printer.str

let addchr = Common.Printer.chr

let pp_block ?(sep = ";") ?(wsp = '\n') ~inner xs =
  let open Common.Printer in
  let _sep = sep in
  let sep = addstr sep |>> addchr wsp in
  addstr "{"
  |>> wrap (addchr wsp |>> seq ~sep inner xs)
  |>> addchr wsp |>> addstr "}"

let pp_group ?(delim = ("(", ")")) ?(sep = ",") ?(wsp = ' ') ~inner xs =
  let open Common.Printer in
  let _sep = sep in
  let sep = addstr sep |>> addchr wsp in
  wrap (addstr (fst delim) |>> seq ~sep inner xs |>> addstr (snd delim))

module Preamble = struct
  type directive =
    | ImportSelected of {
        default : string option;
        named : string list;
        from : string;
      }
    | ImportGlob of {qualified : string; from : string}

  type t = directive list

  let pp : ?relpath:string -> t -> _ Common.Printer.fragment =
   fun ?relpath ->
    let open Common.Printer in
    let pp_directive : directive -> _ Common.Printer.fragment = function
      | ImportSelected {default; named; from} ->
          addstr "import "
          |>> opt
                (fun x ->
                  addstr x |>> iff (List.length named > 0) (addstr ", "))
                default
          |>> iff
                (List.length named > 0)
                (pp_block ~sep:"," ~wsp:' ' ~inner:addstr named)
          |>> addstr " from '" |>> opt addstr relpath |>> addstr from
          |>> addstr "';"
      | ImportGlob {qualified; from} ->
          addstr "import * as " |>> addstr qualified |>> addstr " from '"
          |>> opt addstr relpath |>> addstr from |>> addstr "';"
    in
    fun directives -> seq ~sep:(addchr '\n') pp_directive directives
end

module Comment = struct
  type style = [`Block | `Line]

  type u = string * style

  type _ t =
    | Standalone : u -> unit t
    | Header : u * 'a -> 'a t
    | Footer : 'a * u -> 'a t
    | Bounding : u * 'a * u -> 'a t

  let pp_comment : u -> _ Common.Printer.fragment =
   fun (comment, style) ->
    match style with
    | `Line -> Common.Printer.(addstr "// " |>> addstr comment |>> addchr '\n')
    | `Block ->
        Common.Printer.(addstr "/* " |>> addstr comment |>> addstr " */")

  let pp :
      type a.
      inner:(a -> _ Common.Printer.fragment) -> a t -> _ Common.Printer.fragment
      =
   fun ~inner -> function
    | Standalone c -> pp_comment c
    | Header (c, x) -> Common.Printer.(pp_comment c |>> inner x)
    | Footer (x, c) -> Common.Printer.(inner x |>> pp_comment c)
    | Bounding (c, x, c') ->
        Common.Printer.(pp_comment c |>> inner x |>> pp_comment c')
end

module Type = struct
  module Runtime = struct
    module Width = struct
      type t = Common.Simplified.lenpref

      let () =
        Symtab.register_default ~item:"width" ~loc:"core/width.type" ;
        Symtab.register ~item:"Width" ~loc:"core/width" ;
        ()

      let to_string : t -> string = function
        | `Uint8 -> "width.Uint8"
        | `Uint16 -> "width.Uint16"
        | `Uint30 -> "width.Uint30"
        | `N -> "width.N"
    end

    type t = Parser | Width of Width.t | OutputBytes | Target

    let () =
      Symtab.register ~item:"Parser" ~loc:"parse" ;
      Symtab.register ~item:"OutputBytes" ~loc:"encode" ;
      Symtab.register ~item:"Target" ~loc:"target" ;
      ()

    let to_string = function
      | Parser -> "Parser"
      | Width w -> Width.to_string w
      | OutputBytes -> "OutputBytes"
      | Target -> "Target"
  end

  module Prim = struct
    module Int = struct
      module Kind = struct
        type t =
          | Uint8Kind
          | Int8Kind
          | Uint16Kind
          | Int16Kind
          | Uint30Kind
          | Int31Kind

        let () =
          let loc = "integer/kinds" in
          List.iter
            (fun item -> Symtab.register ~item ~loc)
            [
              "Uint8Kind";
              "Int8Kind";
              "Uint16Kind";
              "Int16Kind";
              "Uint30Kind";
              "Int31Kind";
            ]

        let to_string = function
          | Uint8Kind -> "Uint8Kind"
          | Int8Kind -> "Int8Kind"
          | Uint16Kind -> "Uint16Kind"
          | Int16Kind -> "Int16Kind"
          | Uint30Kind -> "Uint30Kind"
          | Int31Kind -> "Int31Kind"
      end

      type t = Common.Simplified.native_int =
        | Uint8
        | Uint16
        | Uint30
        | Int8
        | Int16
        | Int31
        | Int32
        | Int64

      let () =
        let loc = "integer/integer" in
        List.iter
          (fun item -> Symtab.register ~item ~loc)
          [
            "Uint8";
            "Int8";
            "Uint16";
            "Int16";
            "Uint30";
            "Int31";
            "Int32";
            "Int64";
          ]

      let to_string = function
        | Uint8 -> "Uint8"
        | Int8 -> "Int8"
        | Uint16 -> "Uint16"
        | Int16 -> "Int16"
        | Uint30 -> "Uint30"
        | Int31 -> "Int31"
        | Int32 -> "Int32"
        | Int64 -> "Int64"
    end

    type t =
      | Bool
      | Unit
      | Int of Int.t
      | RangedInt of Int.Kind.t * int * int
      | Double
      | RangedDouble of float * float
      | ZarithN
      | ZarithZ
      | FixedBytes of int
      | FixedString of int
      | Bytes
      | U8String

    let () =
      Symtab.register ~item:"Bool" ~loc:"primitive/bool" ;
      Symtab.register ~item:"Unit" ~loc:"primitive/unit" ;
      Symtab.register ~item:"RangedInt" ~loc:"integer/integer.ranged" ;
      Symtab.register ~item:"Double" ~loc:"float/double" ;
      Symtab.register ~item:"RangedDouble" ~loc:"float/ranged" ;
      Symtab.register ~item:"N" ~loc:"zarith/natural" ;
      Symtab.register ~item:"Z" ~loc:"zarith/integer" ;
      Symtab.register ~item:"FixedBytes" ~loc:"primitive/bytes.fixed" ;
      Symtab.register ~item:"FixedString" ~loc:"primitive/string.fixed" ;
      Symtab.register ~item:"Bytes" ~loc:"primitive/bytes" ;
      Symtab.register ~item:"U8String" ~loc:"primitive/string" ;
      ()

    let to_string : ?static:string -> t -> string =
     fun ?static prim ->
      let base, params =
        match prim with
        | Bool -> ("Bool", None)
        | Unit -> ("Unit", None)
        | Int i -> (Int.to_string i, None)
        | RangedInt (k, min, max) ->
            ( "RangedInt",
              Some (Printf.sprintf "<%s,%d,%d>" (Int.Kind.to_string k) min max)
            )
        | Double -> ("Double", None)
        | RangedDouble (min, max) ->
            ("RangedDouble", Some (Printf.sprintf "<%f,%f>" min max))
        | ZarithN -> ("N", None)
        | ZarithZ -> ("Z", None)
        | FixedBytes n -> ("FixedBytes", Some (Printf.sprintf "<%d>" n))
        | FixedString n -> ("FixedString", Some (Printf.sprintf "<%d>" n))
        | Bytes -> ("Bytes", None)
        | U8String -> ("U8String", None)
      in
      base
      ^ Option.(
          (value ~default:"" @@ map (fun x -> "." ^ x) static)
          ^ value ~default:"" params)
  end

  module Comp = struct
    type 'a t =
      | Padded of 'a * int
      | VPadded of 'a * int
      | Sequence of 'a
      | SequenceBounded of 'a * int * Runtime.Width.t option
      | SequenceFixed of 'a * int
      | Option of 'a
      | Nullable of 'a
      | Dynamic of 'a * Runtime.Width.t

    let () =
      Symtab.register ~item:"Padded" ~loc:"composite/padded" ;
      Symtab.register ~item:"VPadded" ~loc:"composite/vpadded" ;
      Symtab.register ~item:"Sequence" ~loc:"composite/seq/sequence" ;
      Symtab.register
        ~item:"SequenceBounded"
        ~loc:"composite/seq/sequence.bounded" ;
      Symtab.register ~item:"SequenceFixed" ~loc:"composite/seq/sequence.fixed" ;
      Symtab.register ~item:"Option" ~loc:"composite/opt/option" ;
      Symtab.register ~item:"Nullable" ~loc:"composite/opt/nullable" ;
      Symtab.register ~item:"Dynamic" ~loc:"composite/dynamic" ;
      ()

    let class_string : 'a t -> string = function
      | Padded _ -> "Padded"
      | VPadded _ -> "VPadded"
      | Sequence _ -> "Sequence"
      | SequenceBounded _ -> "SequenceBounded"
      | SequenceFixed _ -> "SequenceFixed"
      | Option _ -> "Option"
      | Nullable _ -> "Nullable"
      | Dynamic _ -> "Dynamic"

    let to_string : ?static:string -> inner:('a -> string) -> 'a t -> string =
     fun ?static ~inner t ->
      let base = class_string t
      and params =
        match t with
        | SequenceBounded (x, n, Some lenc) ->
            Printf.sprintf "<%s, %d, %s>" (inner x) n (Runtime.Width.to_string lenc)
        | Padded (x, n)
        | SequenceBounded (x, n, None)
        | SequenceFixed (x, n)
        | VPadded (x, n) ->
            Printf.sprintf "<%s,%d>" (inner x) n
        | Sequence x | Nullable x | Option x -> Printf.sprintf "<%s>" (inner x)
        | Dynamic (x, lp) ->
            Printf.sprintf "<%s,%s>" (inner x) (Runtime.Width.to_string lp)
      in
      base
      ^ Option.(value ~default:"" @@ map (fun x -> "." ^ x) static)
      ^ params
  end

  module Atom = struct
    type t = Prim of Prim.t | Comp of t Comp.t | LDef of string

    let rec to_string : ?static:string -> t -> string =
     fun ?static -> function
      | Prim p -> Prim.to_string ?static p
      | Comp c -> Comp.to_string ?static ~inner:(to_string ?static) c
      | LDef lit -> lit
  end

  module RuntimeComp = struct
    type t = ValueOf | KindOf | VariantDecoder | Unboxed

    let () =
      Symtab.register ~item:"ValueOf" ~loc:"constructed/adt" ;
      Symtab.register ~item:"KindOf" ~loc:"constructed/adt" ;
      Symtab.register ~item:"VariantDecoder" ~loc:"constructed/adt" ;
      Symtab.register ~item:"Unboxed" ~loc:"core/box" ;
      ()

    let to_string = function
      | ValueOf -> "ValueOf"
      | KindOf -> "KindOf"
      | VariantDecoder -> "VariantDecoder"
      | Unboxed -> "Unboxed"
  end

  type t =
    | Atom of Atom.t
    | Runtime of Runtime.t
    | Tuple of Atom.t list
    | Record of (string * Atom.t) list
    | VerbatimString of string
    | NumericLiteral of int
    | VerbatimUnion of string list
    | RuntimeComp of RuntimeComp.t * t list

  module SmartConstructors = struct
    let bool = Atom (Prim Bool)

    let unit = Atom (Prim Unit)

    let uint8 = Atom (Prim (Int Uint8))

    let int8 = Atom (Prim (Int Int8))

    let uint16 = Atom (Prim (Int Uint16))

    let int16 = Atom (Prim (Int Int16))

    let uint30 = Atom (Prim (Int Uint30))

    let int31 = Atom (Prim (Int Int31))

    let int32 = Atom (Prim (Int Int32))

    let int64 = Atom (Prim (Int Int64))

    let double = Atom (Prim Double)

    let _Tuple els =
      let f = function Atom x -> x | _ -> assert false in
      Tuple (List.map f els)

    let _Record flds =
      let f = function fname, Atom x -> (fname, x) | _ -> assert false in
      Record (List.map f flds)

    let _Parser = Runtime Parser

    let _Target = Runtime Target

    let _OutputBytes = Runtime OutputBytes

    let _RangedInt backer min max = Atom (Prim (RangedInt (backer, min, max)))

    let _RangedDouble min max = Atom (Prim (RangedDouble (min, max)))

    let _N = Atom (Prim ZarithN)

    let _Z = Atom (Prim ZarithZ)

    let _FixedBytes n = Atom (Prim (FixedBytes n))

    let _FixedString n = Atom (Prim (FixedString n))

    let _Bytes = Atom (Prim Bytes)

    let _U8String = Atom (Prim U8String)

    let _Padded elt n = Atom (Comp (Padded (elt, n)))

    let _VPadded elt n = Atom (Comp (VPadded (elt, n)))

    let _Sequence elt = Atom (Comp (Sequence elt))

    let _SequenceBounded ?lenc elt n  = Atom (Comp (SequenceBounded (elt, n, lenc)))

    let _SequenceFixed elt n = Atom (Comp (SequenceFixed (elt, n)))

    let _Option elt = Atom (Comp (Option elt))

    let _Nullable elt = Atom (Comp (Nullable elt))

    let _Dynamic elt lp = Atom (Comp (Dynamic (elt, lp)))

    let _ValueOf elt = RuntimeComp (ValueOf, [elt])

    let _KindOf elt = RuntimeComp (KindOf, [elt])

    let _Unboxed elt = RuntimeComp (Unboxed, [elt])

    let _VariantDecoder x y = RuntimeComp (VariantDecoder, [x; y])

    let ldef x = Atom (LDef x)

    let verb x = VerbatimString x

    let lit n = NumericLiteral n

    let inEnum x y = Atom.(LDef (Printf.sprintf "%s.%s" x y))

    let inMap x y = Atom.(LDef (Printf.sprintf "%s['%s']" x y))
  end

  include SmartConstructors

  let peel :
      t ->
      ( string,
        'a Common.Printer.fragment -> 'a Common.Printer.fragment )
      Either.t
      * t list option = function
    | Atom at -> (Left (Atom.to_string at), None)
    | Runtime rt -> (Left (Runtime.to_string rt), None)
    | RuntimeComp (wrapper, elts) ->
        ( Right
            Common.Printer.(
              fun elts' ->
                addstr (RuntimeComp.to_string wrapper)
                |>> pp_group ~delim:("<", ">") ~inner:Fun.id [elts']),
          Some elts )
    | VerbatimString lit -> (Left lit, None)
    | NumericLiteral lit -> (Left (string_of_int lit), None)
    | VerbatimUnion verbs ->
        ( Right (fun _ -> Common.Printer.seq ~sep:(addstr " | ") addstr verbs),
          None )
    | Tuple xs ->
        ( Right
            (fun _ ->
              pp_group
                ~delim:("[", "]")
                ~sep:","
                ~inner:(fun x -> addstr @@ Atom.to_string x)
                xs),
          None )
    | Record xs ->
        ( Right
            (fun _ ->
              pp_block
                ~sep:","
                ~wsp:' '
                ~inner:(fun (s, x) ->
                  Common.Printer.(
                    addstr s |>> addstr ": " |>> addstr (Atom.to_string x)))
                xs),
          None )

  let pp : t -> _ Common.Printer.fragment =
    let open Common.Printer in
    let rec pp' x =
      match peel x with
      | Left shard, remainder ->
          addstr shard |>> opt (seq ~sep:(addchr ',') pp') remainder
      | Right inj, remainder -> inj (opt (seq ~sep:(addchr ',') pp') remainder)
    in
    pp'

  let pp_sig : t -> _ Common.Printer.fragment =
   fun x -> Common.Printer.(addstr ": " |>> pp x)
end

module Expr = struct
  module Entity = struct
    module RTEnt = struct
      type t =
        (* | Box__OnValue *)
        | Tuple__tuple_encoder
        | Tuple__tuple_decoder
        | Record__record_encoder
        | Record__record_decoder
        | Enum__enum_encoder
        | Enum__enum_decoder
        | Adt__variant_encoder
        | Adt__variant_decoder

      let () =
        (* Symtab.register ~item:"onValue" ~loc:"core/box" ; *)
        Symtab.register ~item:"tuple_encoder" ~loc:"constructed/tuple" ;
        Symtab.register ~item:"tuple_decoder" ~loc:"constructed/tuple" ;
        Symtab.register ~item:"record_encoder" ~loc:"constructed/record" ;
        Symtab.register ~item:"record_decoder" ~loc:"constructed/record" ;
        Symtab.register ~item:"enum_encoder" ~loc:"constructed/enum" ;
        Symtab.register ~item:"enum_decoder" ~loc:"constructed/enum" ;
        Symtab.register ~item:"variant_encoder" ~loc:"constructed/adt" ;
        Symtab.register ~item:"variant_decoder" ~loc:"constructed/adt" ;
        ()

      let to_string : t -> string = function
        (* | Box__OnValue -> "onValue" *)
        | Tuple__tuple_encoder -> "tuple_encoder"
        | Tuple__tuple_decoder -> "tuple_decoder"
        | Record__record_encoder -> "record_encoder"
        | Record__record_decoder -> "record_decoder"
        | Enum__enum_encoder -> "enum_encoder"
        | Enum__enum_decoder -> "enum_decoder"
        | Adt__variant_encoder -> "variant_encoder"
        | Adt__variant_decoder -> "variant_decoder"
    end

    type t =
      | Local of string
      | Scoped of string * string
      | Runtime of RTEnt.t
      | This

    let loc s = Local s

    let ( #. ) pref ent = Scoped (pref, ent)

    let pp : t -> _ Common.Printer.fragment =
      let open Common.Printer in
      function
      | Local name -> addstr name
      | Scoped (outer, s) -> addstr outer |>> addchr '.' |>> addstr s
      | Runtime rt -> addstr (RTEnt.to_string rt)
      | This -> addstr "this"
  end

  include Entity

  module Literal = struct
    type t = LitString of string | LitNumber of int

    let to_string = function
      | LitString s -> Printf.sprintf "'%s'" s
      | LitNumber n -> string_of_int n
  end

  type t =
    | Entity of Entity.t
    | Attribute of t * string
    | IndexAttribute of t * Literal.t
    | Literal of Literal.t
    | Spread of t
    | ParenExpr of t
    | RecordOf of (string * t) list
    | WithGenerics of t * Type.t list
    | FunCall of t * t list
    | ArrayOf of t list
    | SumTerms of t list
    | EnumPredicate of Type.t * Entity.t
    | FORBIDDEN

  let ( $. ) : t -> string -> t = fun x attr -> Attribute (x, attr)

  let ( .@[] ) : t -> string -> t =
   fun x attrix -> IndexAttribute (x, LitString attrix)

  let ( .![] ) : t -> int -> t =
   fun x attrix -> IndexAttribute (x, LitNumber attrix)

  let ( .>() ) f gs = WithGenerics (f, gs)

  let ( .$() ) f xs = FunCall (f, xs)

  let ent x = Entity x

  let loc_ent x = ent (loc x)

  let rt_ent x = ent (Runtime x)

  let concat_bytes xs = List.map (fun x -> Spread x) xs |> fun ys -> ArrayOf ys

  let rec pp : t -> _ Common.Printer.fragment =
    let open Common.Printer in
    function
    | FORBIDDEN -> invalid_arg "FORBIDDEN expression"
    | Entity ent -> Entity.pp ent
    | ParenExpr x -> pp_group ~inner:pp [x]
    | Literal lit -> addstr (Literal.to_string lit)
    | Attribute (x, attr) -> pp x |>> addchr '.' |>> addstr attr
    | IndexAttribute (x, attr) ->
        pp x |>> addstr "[" |>> addstr (Literal.to_string attr) |>> addstr "]"
    | Spread x -> addstr "..." |>> pp x
    | FunCall (f, args) -> pp f |>> pp_group ~inner:pp args
    | ArrayOf elems -> pp_group ~delim:("[", "]") ~inner:pp elems
    | RecordOf flds ->
        pp_group
          ~delim:("{", "}")
          ~inner:(fun (fname, ftype) ->
            addstr fname |>> addstr ": " |>> pp ftype)
          flds
    | SumTerms [] -> addstr "0"
    | SumTerms terms -> pp_group ~sep:" + " ~inner:pp terms
    | WithGenerics (f, gs) ->
        pp f |>> pp_group ~delim:("<", ">") ~inner:Type.pp ~sep:"," ~wsp:' ' gs
    | EnumPredicate (typ, obj) ->
        let arg = loc_ent "x" in
        pp_group ~inner:pp [arg] |>> addstr ": " |>> pp arg |>> addstr " is "
        |>> Type.pp typ |>> addstr " => "
        |>> pp
              (ParenExpr
                 ((ent "Object" #. "values").$([ent obj]) $. "includes").$([arg]))
end

module Stmt = struct
  module Binding = struct
    type 'a u = string * Type.t option * 'a

    type t = Expr.t u

    let pp' :
        ('a -> _ Common.Printer.fragment) -> 'a u -> _ Common.Printer.fragment =
     fun inner (name, vsig, rhs) ->
      let open Common.Printer in
      addstr name |>> opt Type.pp_sig vsig |>> addstr " = " |>> inner rhs

    let pp : t -> _ Common.Printer.fragment = pp' Expr.pp
  end

  type t = LocalAssign of [`Let | `Const] * Binding.t | Return of Expr.t

  let return x = Return x

  let pp : t -> _ Common.Printer.fragment =
    let open Common.Printer in
    function
    | LocalAssign (kind, b) ->
        (match kind with `Let -> addstr "let " | `Const -> addstr "const ")
        |>> Binding.pp b |>> addchr ';'
    | Return ret -> addstr "return " |>> Expr.pp ret |>> addchr ';'
end

module Boilerplate = struct
  type t =
    | HybridDecoder of {
        outer : string;
        rtype : Type.t;
        kind : string;
        f : string;
        f_arg : string;
        v_arg : string;
        cases : (string * Expr.t) list;
      }

  let _HybridDecoder ?(outer = "__mkDecoder") ~rtype ~kind ?(f = "f")
      ?(f_arg = "disc") ?(v_arg = "tagval") cases =
    HybridDecoder {outer; rtype; kind; f; f_arg; cases; v_arg}

  let pp : t -> _ Common.Printer.fragment =
    let open Common.Printer in
    function
    | HybridDecoder {outer; rtype; kind; f; f_arg; cases; v_arg} ->
        let brace x =
          addchr '{' |>> wrap (addchr '\n' |>> x) |>> addchr '\n' |>> addchr '}'
        in
        let switch_stmt =
          addstr (Printf.sprintf "switch (%s) " f_arg)
          |>> brace
                (seq
                   ~sep:(addchr ';' |>> addchr '\n')
                   (fun (lhs, rhs) ->
                     addstr "case " |>> addstr kind |>> addchr '.'
                     |>> addstr lhs |>> addstr ": return " |>> Expr.pp rhs)
                   cases
                |>> addchr ';')
        in
        let f_decl =
          addstr (Printf.sprintf "function %s(%s: %s) " f f_arg kind)
          |>> brace switch_stmt
        in
        let f_validator =
          addstr
            (Printf.sprintf
               "%s.isValid = (%s: number): %s is %s => \
                Object.values(%s).includes(%s);"
               f
               v_arg
               v_arg
               kind
               kind
               v_arg)
        in
        let f_return = addstr (Printf.sprintf "return %s;" f) in
        addstr (Printf.sprintf "function %s(): " outer)
        |>> Type.pp rtype |>> addstr " "
        |>> brace
              (f_decl |>> addchr '\n' |>> f_validator |>> addchr '\n'
             |>> f_return)
end

module Decl = struct
  module Fun = struct
    module Sig = struct
      type param = string * Type.t

      let ( #: ) : string -> Type.t -> param = fun pname ptype -> (pname, ptype)

      type paramlist = param list

      type t = paramlist * Type.t option

      let ( @: ) : paramlist -> Type.t -> t = fun pars typ -> (pars, Some typ)

      let void = ()

      let ( @! ) : paramlist -> unit -> t = fun pars () -> (pars, None)

      let pp_paramlist =
        pp_group ~inner:(fun (pname, ptype) ->
            Common.Printer.(addstr pname |>> Type.pp_sig ptype))

      let pp : t -> _ Common.Printer.fragment =
       fun (pars, rhs_sig) ->
        Common.Printer.(pp_paramlist pars |>> opt Type.pp_sig rhs_sig)
    end

    module Arrow = struct
      type t = Sig.t * Stmt.t list

      let ( => ) lhs rhs = (lhs, rhs)

      let pp : ?arrow:bool -> t -> _ Common.Printer.fragment =
       fun ?(arrow = true) (lhs, rhs) ->
        Common.Printer.(
          Sig.pp lhs
          |>> (if arrow then addstr " => " else addchr ' ')
          |>> pp_block ~inner:Stmt.pp rhs)
    end

    type t = string * Arrow.t

    let pp : ?keyword:bool -> t -> _ Common.Printer.fragment =
     fun ?(keyword = true) (fname, lambda) ->
      let open Common.Printer in
      iff keyword (addstr "function ")
      |>> addstr fname
      |>> Arrow.pp ~arrow:false lambda
  end

  module Class = struct
    module Item = struct
      type item_mod = Static | Instance

      type t = Method of item_mod * Fun.t

      let pp_mod = function
        | Static -> addstr "static "
        | Instance -> Common.Printer.ign

      let pp : t -> _ Common.Printer.fragment = function
        | Method (imod, f) ->
            Common.Printer.(pp_mod imod |>> Fun.pp ~keyword:false f)
    end

    type t = {
      class_name : string;
      superclass : string option;
      interfaces : string list option;
      body : Item.t list;
    }

    let () =
      Symtab.register ~item:"Codec" ~loc:"codec" ;
      Symtab.register ~item:"Encode" ~loc:"encode" ;
      Symtab.register ~item:"OutputBytes" ~loc:"encode" ;
      Symtab.register ~item:"Box" ~loc:"core/box" ;
      ()

    let pp : t -> _ Common.Printer.fragment =
     fun {class_name; superclass; interfaces; body} ->
      let open Common.Printer in
      addstr "class " |>> addstr class_name
      |>> opt (fun sup -> addstr " extends " |>> addstr sup) superclass
      |>> opt
            (fun ifs ->
              addstr " implements "
              |>> seq ~sep:(addstr ", ") addstr ifs
              |>> addchr ' ')
            interfaces
      |>> pp_block ~inner:Item.pp body
  end

  module TypeAlias = struct
    type t = string * Type.t

    let pp : t -> _ Common.Printer.fragment =
      let open Common.Printer in
      fun (name, rhs) ->
        addstr "type " |>> addstr name |>> addstr " = " |>> Type.pp rhs
        |>> addstr ";"
  end

  module Const = struct
    type t =
      | Const of Stmt.Binding.t
      | ConstArrow of Fun.Arrow.t Stmt.Binding.u

    let pp : t -> _ Common.Printer.fragment = function
      | Const c -> Common.Printer.(addstr "const " |>> Stmt.Binding.pp c)
      | ConstArrow ca ->
          Common.Printer.(addstr "const " |>> Stmt.Binding.pp' Fun.Arrow.pp ca)
  end

  module Enum = struct
    type width = Common.Simplified.enum_size

    type t = [`Const] option * string * width * (string * int) list

    let pp : t -> _ Common.Printer.fragment =
     fun (is_const, name, _width, elems) ->
      let open Common.Printer in
      opt (fun `Const -> addstr "const ") is_const
      |>> addstr "enum " |>> addstr name
      |>> pp_block
            ~sep:","
            ~inner:(fun (name, disc) ->
              addstr name |>> addstr " = " |>> addstr (string_of_int disc))
            elems
  end

  module TypeMap = struct
    type t = string * (string * Type.t) list

    let pp : t -> _ Common.Printer.fragment =
      let open Common.Printer in
      fun (name, elts) ->
        addstr "interface " |>> addstr name |>> addstr " "
        |>> pp_block
              ~sep:","
              ~wsp:'\n'
              ~inner:(fun (c, rhs) -> addstr c |>> Type.pp_sig rhs)
              elts
  end

  module TypeUnion = struct
    type t = string * Type.t list

    let pp : t -> _ Common.Printer.fragment =
      let open Common.Printer in
      fun (name, vars) ->
        addstr "type " |>> addstr name |>> addstr " = "
        |>> seq ~sep:(addstr " | ") Type.pp vars
        |>> addchr ';'
  end

  type t =
    | DFunction of Fun.t
    | DClass of Class.t
    | DTypeAlias of TypeAlias.t
    | DTypeUnion of TypeUnion.t
    | DTypeMap of TypeMap.t
    | DConst of Const.t
    | DEnum of Enum.t
    | DBoilerplate of Boilerplate.t
    | DMulti of t list
    | Commented of t Comment.t

  let _function fname fsig stmts = DFunction (fname, (fsig, stmts))

  let _class class_name ?extends ?implements body =
    DClass {class_name; superclass = extends; interfaces = implements; body}

  let _type alias_name typ = DTypeAlias (alias_name, typ)

  let _typeMap map_name typs = DTypeMap (map_name, typs)

  let _typeUnion union_name vars = DTypeUnion (union_name, vars)

  let _const const_name ?typ rhs = DConst (Const (const_name, typ, rhs))

  let _constLambda const_name ?typ rhs =
    DConst (ConstArrow (const_name, typ, rhs))

  let _enum ~size enum_name elems = DEnum (None, enum_name, size, elems)

  let rec pp : t -> _ Common.Printer.fragment =
    let open Common.Printer in
    function
    | DFunction f -> addstr "export " |>> Fun.pp f
    | DClass c -> addstr "export " |>> Class.pp c
    | DTypeAlias alias -> addstr "export " |>> TypeAlias.pp alias
    | DTypeMap map -> addstr "export " |>> TypeMap.pp map
    | DConst c -> addstr "export " |>> Const.pp c
    | DEnum e -> addstr "export " |>> Enum.pp e
    | DTypeUnion u -> addstr "export " |>> TypeUnion.pp u
    | DBoilerplate bpl -> addstr "export " |>> Boilerplate.pp bpl
    | DMulti decls -> Common.Printer.seq ~sep:(addchr '\n') pp decls
    | Commented ct -> Comment.pp ~inner:pp ct
end

type ts_type = Type.t

type ts_expr = Expr.t

type ts_stmt = Stmt.t

type ts_decl = Decl.t

type ts_blpl = Boilerplate.t

type ts_module = {preamble : Preamble.t; decls : ts_decl list}

let pp_module : ?relpath:string -> ts_module -> _ Common.Printer.fragment =
 fun ?relpath {preamble; decls} ->
  let open Common.Printer in
  Preamble.pp ?relpath preamble
  |>> addchr '\n'
  |>> seq ~sep:(addchr '\n') Decl.pp decls
  |>> addchr '\n'
