type rootname = string

type extname = string

type t =
  | RawName of string
  | RootType of extname
  | FieldName of extname
  | ConstructorName of extname
  | AssociatedMethod of extname * extname
  | SubType of extname * extname
  | Class of t
  | TagEnum of extname
  | SubtypeMap of extname

val to_string : t -> string
