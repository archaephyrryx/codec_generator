module Keywords = struct
  let rectify : string -> bool * string =
   fun s ->
    match s with
    | "break" | "case" | "catch" | "class" | "const" | "continue" | "debugger"
    | "default" | "delete" | "do" | "else" | "enum" | "export" | "extends"
    | "false" | "finally" | "for" | "function" | "if" | "import" | "in"
    | "instanceof" | "new" | "null" | "return" | "super" | "switch" | "this"
    | "throw" | "true" | "try" | "typeof" | "var" | "void" | "while" | "with" ->
        (true, "_" ^ s) (* Reserved words *)
    | "as" | "implements" | "interface" | "let" | "package" | "private"
    | "protected" | "public" | "static" | "yield" ->
        (true, "_" ^ s) (* Strict Mode Reserved words *)
    | "any" | "boolean" | "constructor" | "declare" | "get" | "module"
    | "require" | "number" | "set" | "string" | "symbol" | "type" | "from"
    | "of" ->
        (true, "_" ^ s)
    | _ -> (false, s)
end

let sanitize_name ~f ~sep raw_name =
  (* FIXME[epic=eternity] - logic unsound for any NXX numeric protocol where N >
     0 *)
  let is_numeric s = String.(sub s 0 1 = "0") in
  let sanitize = function s when is_numeric s -> f "proto" ^ f s | s -> f s in
  let undash = String.map (function ' ' | '-' -> '_' | c -> c) in
  String.split_on_char '.' raw_name
  |> List.map (fun x -> x |> undash |> sanitize)
  |> String.concat sep |> Keywords.rectify
  |> fun (_, y) -> y

let normalize_field_id fname =
  sanitize_name ~f:String.lowercase_ascii ~sep:"_" fname

let normalize_class_id cname =
  sanitize_name ~f:String.capitalize_ascii ~sep:"" cname

type id_case = Pascal | Snake

let normalize_type_id : ?idcase:id_case -> string -> string =
 fun ?(idcase = Pascal) ->
  let f, sep =
    match idcase with
    | Pascal ->
        ( (fun y ->
            String.split_on_char '_' y
            |> List.map String.capitalize_ascii
            |> String.concat ""),
          "" )
    | Snake -> (String.lowercase_ascii, "_")
  in
  sanitize_name ~f ~sep

let normalize_constructor : string -> string = sanitize_name ~f:Fun.id ~sep:""

type rootname = string

type extname = string

type t =
  | RawName of string
  | RootType of rootname
  | FieldName of rootname
  | ConstructorName of rootname
  | AssociatedMethod of rootname * extname
  | SubType of rootname * extname
  | Class of t
  | TagEnum of rootname
  | SubtypeMap of rootname

let ensure_prefix :
    prefix:string ->
    ?observer:('a -> string) ->
    f:('a -> string) ->
    ?sanitize:(string -> string) ->
    'a ->
    string =
 fun ~prefix ?observer ~f ?sanitize x ->
  let observed = match observer with None -> f x | Some g -> g x in
  if String.starts_with ~prefix observed then observed
  else f x |> Option.value ~default:Fun.id sanitize |> fun y -> prefix ^ y

let rec to_string : ?raw:bool -> t -> string =
 fun ?(raw = false) -> function
  | RawName x -> x
  | RootType tname -> if raw then tname else normalize_type_id tname
  | FieldName fname -> if raw then fname else normalize_field_id fname
  | ConstructorName cname -> if raw then cname else normalize_constructor cname
  | AssociatedMethod (tname, mname) ->
      assert (not raw) ;
      Printf.sprintf "%s_%s" (normalize_type_id ~idcase:Snake tname) mname
  | SubType (x, sy) ->
      Printf.sprintf
        "%s__%s"
        (if raw then x else normalize_type_id x)
        (if raw then sy else normalize_constructor sy)
  | Class x ->
      ensure_prefix
        ~prefix:"CGRIDClass__"
        ~observer:(to_string ~raw)
        ~f:to_string
        ~sanitize:normalize_class_id
        x
  | TagEnum x -> Printf.sprintf "CGRIDTag__%s" (normalize_class_id x)
  | SubtypeMap x -> Printf.sprintf "CGRIDMap__%s" (normalize_class_id x)

let to_string = to_string ~raw:false
