open Data_encoding
open Virtual
open Common

module type Codec = sig
  type t

  val schema_name : string

  val encoding : t Encoding.t
end

module Pipeline : sig
  val run : ?ts:bool -> (module Codec) -> unit
end = struct
  let run ?(ts = false) (module M : Codec) =
    Printf.fprintf stderr "[codec_generator]: After simplification...\n" ;
    let simple = Simplified.of_encoding ~debug:() M.encoding in
    let rust =
      let open Rust.Generator in
      Gen.generate
        ~force_derive:Gen.Conf.Impl.SuppressDerive
        M.schema_name
        simple
    in
    Printf.fprintf
      stderr
      "[codec_generator]: Generating for target language `Prose`...\n" ;
    Printer.(Prose.Generator.produce simple $ Doc stderr) ;
    prerr_newline () ;
    Printf.fprintf
      stderr
      "[codec_generator]: Generating for target language `Rust`...\n" ;
    Printf.fprintf
      stderr
      "//! Auto-generated codec module for custom schema `%s`\n"
      M.schema_name ;
    Printer.(Rust.Ast.pp_module rust $ Doc stderr) ;
    if ts then (
      let typescript =
        let open Typescript.Generator in
        Gen.generate M.schema_name simple
      in
      prerr_newline () ;
      Printf.fprintf
        stderr
        "[codec_generator]: Generating for target language `TypeScript`...\n" ;
      Printf.fprintf
        stderr
        "// Auto-generated codec module for custom schema `%s`\n"
        M.schema_name ;
      Printer.(Typescript.Ast.pp_module typescript $ Doc stderr)) ;
    ()
end
