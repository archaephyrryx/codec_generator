open Data_encoding
open Rust.Generator
open Rust.Ast

let simplify_prefix prefix =
  match String.lowercase_ascii prefix with
  | "alpha" -> "alpha"
  | "005-psbabym1" -> "babylon"
  | "006-pscartha" -> "carthage"
  | "007-psdelph1" -> "delphi"
  | "008-ptedo2zk" -> "edo"
  | "009-psfloren" -> "florence"
  | "010-ptgranad" -> "granada"
  | "011-pthangz2" -> "hangzhou"
  | "012-psithaca" -> "ithaca"
  | "013-ptjakart" -> "jakarta"
  | "014-ptkathma" -> "kathmandu"
  | "015-ptlimapt" -> "lima"
  | "016-ptmumbai" -> "mumbai"
  (* NOTE[epic=ongoing] - add new protocols as they are supported *)
  | _ -> prefix

let remap_orig orig_id =
  String.split_on_char '.' orig_id
  |> (function h :: t -> simplify_prefix h :: t | [] -> [])
  |> String.concat "."

module IdMap = struct
  module H = Hashtbl.Make (struct
    include String

    let hash = Hashtbl.hash
  end)

  type t = string H.t

  let create : int -> t = H.create

  let add ~orig ~prefix ~modname ~typename tab =
    H.add
      tab
      (remap_orig orig)
      (Printf.sprintf "%s::%s::%s" prefix modname typename)

  let write : t -> out_channel -> unit =
   fun htab chan -> H.iter (Printf.fprintf chan "(\"%s\", %s),\n") htab
end

let sanitize_name ~f raw_name =
  let sanitize = function
    | s when String.(sub s 0 1 = "0") -> f "proto" ^ f s
    | s -> s |> f
  in
  let undash = String.map (function '-' -> '_' | c -> c) in
  String.split_on_char '.' raw_name
  |> List.map (fun x -> x |> undash |> sanitize)
  |> String.concat "__"

let get_protoname ~f raw_name =
  let sanitize = function
    | s when String.(sub s 0 1 = "0") -> f "proto" ^ f s
    | ("alpha" | "ground" | "sapling") as s -> f s
    | _ -> "etc"
  in
  let undash = String.map (function '-' -> '_' | c -> c) in
  String.split_on_char '.' raw_name
  |> List.map (fun x -> x |> undash |> sanitize)
  |> List.hd

let get_modulename ~f raw_name =
  let prefix = get_protoname ~f raw_name ^ "__" in
  let sanitized_name = sanitize_name ~f raw_name in
  if String.starts_with ~prefix sanitized_name then
    String.sub
      sanitized_name
      (String.length prefix)
      (String.length sanitized_name - String.length prefix)
  else sanitized_name

let to_images enc =
  let to_image i = Hex.(show @@ of_bytes @@ Binary.to_bytes_exn enc i) in
  List.map to_image

let reverse_prune : tgt:string -> string -> string =
  let rec drop_until x ys =
    match ys with
    | [] -> []
    | y :: _ when x = y -> ys
    | _ :: yt -> drop_until x yt
  in
  fun ~tgt path ->
    String.split_on_char '/' path
    |> List.rev |> drop_until tgt |> List.rev |> String.concat "/"

type input =
  | Input : {
      schema : 'a Encoding.t;
      orig_id : string;
      type_name : string;
      proto_name : string;
      module_name : string;
      preimages : 'a list option;
    }
      -> input

type tl_conf = {relpath : string; file_ext : string}

type rust_conf = {relpath : string; file_ext : string; datafile : string}

type prose_conf = {relpath : string; file_ext : string; max_length : int option}

type msyn_conf = {relpath : string; filename : string; id : string}

type target_conf =
  [ `RustConf of rust_conf
  | `ProseConf of prose_conf
  | `TsConf of tl_conf
  | `MSynConf of msyn_conf ]

type env = {debug : unit option; verbosity : Reporting.level; root : string}

type conf = target_conf list

let get_env : ?config:Config.t -> unit -> env =
 fun ?(config = Config.default_config) () ->
  let root =
    match config.root_override with
    | None -> Sys.getcwd () |> reverse_prune ~tgt:"codec_generator"
    | Some dir -> if Sys.is_directory dir then dir else ""
  in
  (* FIXME: hack for tezos-link build *)
  let root =
    if root <> "" then root
    else
      let home = Sys.getenv "HOME" in
      home ^ "/tezos/dev/codec_generator"
  in
  {debug = config.debug; verbosity = config.verbosity; root}

let configure : ?config:Config.t -> unit -> conf =
 fun ?(config = Config.default_config) () ->
  List.concat_map
    (let open Config in
    function
    | ForLang (Rust (Some {relpath; datafile})) ->
        [`RustConf {relpath; file_ext = "rs"; datafile}]
    | ForLang (Typescript (Some {relpath})) ->
        [`TsConf {relpath; file_ext = "ts"}]
    | ForLang (Prose (Some {relpath; passes})) ->
        List.map
          (fun ProseConfig.{ident_length_limit; file_ext} ->
            `ProseConf {relpath; max_length = ident_length_limit; file_ext})
          passes
    | ForLang (MichelSyn (Some {relpath; cherrypick})) ->
        List.map
          (fun MichelSynConfig.{id; filename} ->
            `MSynConf {relpath; id; filename})
          cherrypick
    | ForLang (Rust None | Typescript None | Prose None | MichelSyn None) -> [])
    config.targets

let to_typename : string -> string = fun x -> x

let to_modulename : string -> string = get_modulename ~f:String.lowercase_ascii

let to_protoname : string -> string = get_protoname ~f:String.lowercase_ascii

let of_registration :
    ?prefix:string ->
    Data_encoding.Registration.id * Data_encoding.Registration.introspectable ->
    input =
 fun ?(prefix = "") (orig_id, Any schema) ->
  let type_name = to_typename (prefix ^ orig_id)
  and module_name = to_modulename (prefix ^ orig_id)
  and proto_name = to_protoname (prefix ^ orig_id)
  and preimages = None in
  Input {schema; orig_id; type_name; proto_name; module_name; preimages}

let of_encoding_raw :
    type a. ?prefix:string -> string * a Data_encoding.Encoding.t -> input =
 fun ?prefix (id, schema) -> of_registration ?prefix (id, Any schema)

let run_target ~env ~logger (tgt : target_conf) orig_id type_name proto_name
    ?assoc module_name ?images simple =
  match tgt with
  | `RustConf {relpath; file_ext; _} -> (
      logger orig_id ;
      try
        let oput_rust =
          simple
          |> Gen.generate ~tyconf:(None, PublishAll) ?images type_name
          |> pp_module
        in
        let specdir = Printf.sprintf "%s/%s/%s" env.root relpath proto_name in
        if Sys.file_exists specdir then
          if Sys.is_directory specdir then ()
          else failwith ("Cannot overwrite non-directory file: " ^ specdir)
        else Sys.mkdir specdir 0o755 ;
        let filepath = Printf.sprintf "%s/%s.%s" specdir module_name file_ext in
        let dest = open_out filepath in
        Common.Printer.(oput_rust $ Doc dest) ;
        close_out dest ;
        Option.iter
          (IdMap.add
             ~orig:orig_id
             ~prefix:proto_name
             ~modname:module_name
             ~typename:(Gen.mk_tname type_name))
          assoc ;
        ()
      with
      | Invalid_argument err ->
          Reporting.err_dump_schema simple ;
          prerr_endline err
      | Assert_failure _ as err ->
          Reporting.err_dump_schema simple ;
          raise err)
  | `ProseConf {relpath; file_ext; max_length} ->
      logger orig_id ;
      let module_name' = type_name in
      let oput_prose = simple |> Virtual.Prose.Generator.produce ?max_length in
      (* NOTE - reinstating original unsanitized name for prose generation *)
      let filepath =
        Printf.sprintf "%s/%s/%s.%s" env.root relpath module_name' file_ext
      in
      let dest = open_out filepath in
      Common.Printer.(oput_prose $ Doc dest) ;
      close_out dest ;
      ()
  | `TsConf {relpath; file_ext} -> (
      logger orig_id ;
      let module_name' = type_name in
      try
        let oput_ts =
          simple
          |> Typescript.Generator.Gen.generate type_name
          |> Typescript.Ast.pp_module ~relpath:"../../"
        in
        let specdir = Printf.sprintf "%s/%s/%s" env.root relpath proto_name in
        if Sys.file_exists specdir then
          if Sys.is_directory specdir then ()
          else failwith ("Cannot overwrite non-directory file: " ^ specdir)
        else Sys.mkdir specdir 0o755 ;
        let filepath =
          Printf.sprintf "%s/%s.%s" specdir module_name' file_ext
        in
        let dest = open_out filepath in
        Common.Printer.(oput_ts $ Doc dest) ;
        close_out dest ;
        ()
      with
      | Invalid_argument err ->
          (* Reporting.err_dump_schema simple ; *)
          prerr_endline err
      | Assert_failure _ ->
          (* Reporting.err_dump_schema simple ; *)
          prerr_endline "assertion failure; skipping typescript output"
      | _ ->
          (* Reporting.err_dump_schema simple ; *)
          prerr_endline "unspecified exception; skipping typescript output")
  | `MSynConf {relpath; filename; id} ->
      if orig_id = id then (
        logger orig_id ;
        let oput_msyn = simple |> Virtual.Michelsyn.Generator.transform in
        (* NOTE - reinstating original unsanitized name for prose generation *)
        let filepath = Printf.sprintf "%s/%s/%s" env.root relpath filename in
        let dest = open_out filepath in
        Common.Printer.(oput_msyn $ Doc dest) ;
        close_out dest ;
        ())

let run_pipeline : env:env -> conf:conf -> input list -> unit =
 fun ~env ~conf inputs ->
  let assoc = IdMap.create (List.length inputs) in
  let logger =
    Reporting.init ~level:env.verbosity ~count:(List.length inputs)
  in
  let single_run : input -> unit =
   fun (Input {schema; orig_id; type_name; proto_name; module_name; preimages}) ->
    logger orig_id ;
    let images = Option.map (to_images schema) preimages in
    logger orig_id ;
    try
      let simple = Simplified.of_encoding ?debug:env.debug schema in
      List.iter
        (fun tgt ->
          run_target
            ~env
            ~logger
            tgt
            orig_id
            type_name
            proto_name
            ?images
            ~assoc
            module_name
            simple)
        conf
    with Invalid_argument err ->
      Format.eprintf "codec_generator: Pipeline failed for input schema: {@[" ;
      Data_encoding__Binary_schema.pp
        Format.err_formatter
        (Data_encoding__Binary_description.describe schema) ;
      Format.pp_close_box Format.err_formatter () ;
      Format.pp_print_char Format.err_formatter '}' ;
      Format.pp_print_newline Format.err_formatter () ;
      prerr_endline err
  in
  List.iter single_run inputs ;
  let filepath =
    List.find_map
      (function
        | `RustConf {datafile; relpath; _} -> Some (relpath ^ "/" ^ datafile)
        | _ -> None)
      conf
  in
  match filepath with
  | None -> ()
  | Some fp ->
      let oc = open_out (env.root ^ "/" ^ fp) in
      IdMap.write assoc oc ;
      close_out oc ;
      ()
