type level = Silent | Dots of int | Numeric | Report | PerStage | Full

type inp_id = (int, string) Either.t

type message =
  | Startup
  | Init of inp_id
  | Simplifying of inp_id
  | Generating of string * inp_id
  | RunOver
  | Shutdown

let err_dump_schema schema =
  let dump = Common__Simplified.Verbose.(to_string @@ pp schema) in
  Format.eprintf
    "codec_generator: @[<hov 2> Pipeline failed for the following input:@,\
     {@[%s@]}@]@."
    dump

let err_no_input i =
  if i = 0 then
    Format.eprintf
      "codec_generator: Pipeline run began, but no inputs were found!@." ;
  fun unexpected ->
    Format.eprintf
      "codec_generator: Attempted to log `%s`, but no inputs are expected!@."
      unexpected ;
    ()

let startup =
  Format.eprintf
    "@[<hov 2>[codec_generator]: Preparing to generate codecs for %d schema \
     inputs...]@."

let init_dots total _width =
  startup total ;
  let peridot =
    match _width with n when n >= 1 -> total / _width | _nonpositive -> 1
  in
  Format.eprintf
    "@[<hov 2>[codec_generator]: Reporting mode set to `Dots`@,\
     (each dot represents %d of %d total inputs)@]@."
    peridot
    total ;
  let ctr = ref 0 and inp = ref "" in
  let step str =
    let n = !ctr in
    let str' = !inp in
    if str <> str' then inp := str ;
    ctr := n + 1 ;
    if (n + 1) / peridot > n / peridot then Stdlib.prerr_char '.' else ()
  in
  step

let init_num total =
  startup total ;
  Format.eprintf
    "@[<hov 2>[codec_generator]: Reporting mode set to `Numeric`@]@." ;
  let ctr = ref 0 and inp = ref "" in
  let step str =
    let n = !ctr in
    let str' = !inp in
    if str <> str' then (
      inp := str ;
      ctr := n + 1 ;
      Format.eprintf "@[<hov 2>Processing input #%d (of %d)@]@." (n + 1) total) ;
    ()
  in
  step

let init_report total =
  startup total ;
  Format.eprintf
    "@[<hov 2>[codec_generator]: Reporting mode set to `Report`@]@." ;
  let inp = ref "" in
  let step str =
    let str' = !inp in
    if str <> str' then
      (inp := str ;
       Format.eprintf "@[<hov 2>Processing input `%s`@]@.")
        str ;
    ()
  in
  step

let init_perstage total =
  startup total ;
  Format.eprintf "@[<hov 2>[codec_generator]: Reporting mode set to `Stage`@]@." ;
  let ctr = ref ("", 0) in
  let step str =
    match !ctr with
    | inp, _ when inp <> str ->
        ctr := (str, 0) ;
        if inp <> "" then Format.eprintf "...Finished!@]@." else () ;
        if str <> "" then
          Format.eprintf
            "@[<hov 2>[codec_generator]: Starting pipeline run on input \
             `%s`...@,"
            str
        else ()
    | _, n -> (
        ctr := (str, n + 1) ;
        match n with
        | 0 -> Format.eprintf "...Simplifying...@,"
        | _ when n > 0 -> Format.eprintf "...Generating (target #%d)...@," n
        | _ -> assert false)
  in
  step

let init_full total =
  startup total ;
  Format.eprintf "@[<hov 2>[codec_generator]: Reporting mode set to `Full`@]@." ;
  let ctr = ref ("", 0, 0) in
  let step str =
    match !ctr with
    | inp, n, _ when inp <> str ->
        ctr := (str, n + 1, 0) ;
        if inp <> "" then Format.eprintf "...Finished!@]@." else () ;
        if str <> "" then
          Format.eprintf
            "@[<hov 2>[codec_generator]: Starting pipeline run on input `%s` \
             (#%d of %d)...@,"
            str
            (n + 1)
            total
        else ()
    | _, n, m -> (
        ctr := (str, n, m + 1) ;
        match m with
        | 0 -> Format.eprintf "...Simplifying...@,"
        | _ when m > 0 -> Format.eprintf "...Generating (target #%d)...@," m
        | _ -> assert false)
  in
  step

let init : level:level -> count:int -> string -> unit =
 fun ~level ~count ->
  if count = 0 then err_no_input count
  else
    match level with
    | Silent -> ignore
    | Dots _n -> init_dots count _n
    | Numeric -> init_num count
    | Report -> init_report count
    | PerStage -> init_perstage count
    | Full -> init_full count

let log_start : (string -> unit) -> string -> unit =
 fun logger key -> logger key
