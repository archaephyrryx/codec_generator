module RustConfig = struct
  type t = {relpath : string; datafile : string}

  let default_datafile = "assocs.rstub"
end

module ProseConfig = struct
  type params = {ident_length_limit : int option; file_ext : string}

  type t = {relpath : string; passes : params list}

  let default_passes : params list =
    [
      {ident_length_limit = None; file_ext = "prose"};
      {ident_length_limit = Some 80; file_ext = "prose~"};
    ]
end

module MichelSynConfig = struct
  type params = {id : string; filename : string}

  type t = {relpath : string; cherrypick : params list}

  let default_cherrypick : params list =
    [{id = "015-PtLimaPt.script.prim"; filename = "michelson.tmLanguage.json"}]
end

type rust

type typescript

type prose

type michelsyn

type simple_conf = {relpath : string}

type rust_conf = RustConfig.t

type prose_conf = ProseConfig.t

type msyn_conf = MichelSynConfig.t

type _ tgt_conf =
  | Rust : rust_conf option -> rust tgt_conf
  | Typescript : simple_conf option -> typescript tgt_conf
  | Prose : prose_conf option -> prose tgt_conf
  | MichelSyn : msyn_conf option -> michelsyn tgt_conf

type lang_conf = ForLang : 'a tgt_conf -> lang_conf

type lang_confs = lang_conf list

type t = {
  debug : unit option;
  verbosity : Reporting.level;
  root_override : string option;
  targets : lang_confs;
}

let default_targets : lang_confs =
  [
    ForLang
      (Rust
         (Some
            {
              relpath = "generated_rust/src";
              datafile = RustConfig.default_datafile;
            }));
    ForLang (Typescript (Some {relpath = "generated_typescript"}));
    ForLang
      (Prose
         (Some
            {relpath = "generated_prose"; passes = ProseConfig.default_passes}));
    ForLang
      (MichelSyn
         (Some
            {
              relpath = "michelsyn";
              cherrypick = MichelSynConfig.default_cherrypick;
            }));
  ]

let default_config : t =
  {
    debug = None;
    verbosity = Reporting.Numeric;
    root_override = None;
    targets = default_targets;
  }
