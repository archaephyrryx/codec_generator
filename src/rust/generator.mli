module Simplified = Common.Simplified

(** [Ctxt]: Monad that associates each AST node with the set of runtime imports
    and subordinate definitions mandated somewhere in its local subtree, which
    is accumulated monoidally and propogated to the root node by induction. *)
module Ctxt : sig
  module State : sig
    (** [S]: import-set representing imports from rust_runtime for the generated
        codec.

        Includes three anonymous extra flags for derive-macro importing from the
        associated [<trait_name>_derive] crates. *)
    module S : sig
      type t = Set.Make(String).t * (bool * bool * bool)

      val empty : t

      val add : string -> t -> t

      val union : t -> t -> t

      val elements : t -> string list * (bool * bool * bool)
    end

    (* [T]: definition-map for subordinate types which must be explicitly
       defined, based on named-indirection in the simplified schema.

       The default implementation of [union] is overloaded with a manual
       implementation that prints warnings to stderr on distinct-valued (via
       structural equality test) key collisions, but otherwise is right-biased
       and does not *)
    module T : sig
      include module type of Map.Make (String)

      (** [union]: Version of the standard [Map]-functor [union] that is
          specialized for cases in which there is no appropriate strategy for
          handling overlapping keys.

          [union x y] is right-biased, in that the return value will preserve
          the associated values in [y] for keys found in [x] as well.

          More specifically, if a given key [s] is associated with value [vx] in
          [x] and [vy] in [y], [vy] is always used over [vx]. If [vx <> vy ==>
          true], a warning message will be output to stderr, reporting the value
          of [s] for debugging purposes, as well as calling a possibly-noop function
          on each value in sequence. *)
      val union : ?trace:('a -> unit) -> ?neq:('a -> 'a -> bool) -> 'a t -> 'a t -> 'a t
    end

    (** [ents]: set of imports that are contextually required by a subtree or
        node *)
    type ents = S.t

    (** [defs]: identifier-map of subordinate type definitions for de-nesting *)
    type defs = Simplified.t T.t

    type t
  end

  (** ['a t]: Context monad parametrized over nodes of type ['a] *)
  type 'a t = 'a * State.t

  (** [return]: wrap any value in the null context (empty set of imports, empty
      map of definitions) *)
  val return : 'a -> 'a t

  (** [pure]: same as [[return]] *)
  val pure : 'a -> 'a t

  (** [add s x]: returns [x] with [s] added to its import-set

      If [s] is already included in the import-set, acts as identity on [x].

      Although they are handled completely transparently, [add]ing the strings
      ["deriveEncode"], ["deriveDecode"], ["deriveEstimable"] will yield a
      preamble that includes both the original trait import from [rust_runtime],
      and the derive macro for that trait from the [derive_XXX] crate in
      question, when the AST preamble is being constructed and printed. *)
  val add : string -> 'a t -> 'a t

  (** [add_def ~id ~entry x]: returns [x] with the association [id -> entry]
      added to its definition-map

      [add_def] will override prior associations with the same identifier in
      [x], as it is based on [[T.add]].

      As an alternative, consider the construct [fmerge (fun a () -> a) x
      (add_def ~id ~entry (pure ()))]

      This pattern will add the novel key-value entry via the right-biased
      [T.union]. *)
  val add_def : id:string -> entry:Simplified.t -> 'a t -> 'a t

  (** [bind]: standard monad bind over ['a t]

      In particular, this is implemented so that the following monad laws hold:
      * [bind (pure x) f === f x] * [bind x pure === x] * [bind (bind a f) g ===
      bind a (fun x -> bind (f x) g)]

      Furthermore, the following should hold as well, as implemented: * [bind x
      f === fmerge (fun _ a -> a) x @@ f (forget x)] *)
  val bind : 'a t -> ('a -> 'b t) -> 'b t

  (** [(>>=)]: infix operator version of [bind] *)
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t

  (** [fmap]: functor map over ['a t]

      [fmap] is implemented such that the context ([ctxt] and [defs] elements)
      are invariant under its application, which means that it commutes freely
      with [add] and [add_def] calls, for which the contextualized value (['a]
      element) is invariant under application.

      It is associative, but not inherently commutative, with other [fmap]
      calls.

      It upholds the following properties: * [fmap (fun y -> y) x === x] * [fmap
      (fun y -> g (f y)) x === fmap g (fmap f x) ]

      Furthermore: * [forget (fmap f x) === f (forget x)] * [fmap f x === bind
      (fun y -> pure (f y)) x] * [(f (g x) == g (f x)) ==> fmap f (fmap g x) ===
      fmap g (fmap f x)] *)
  val fmap : ('a -> 'b) -> 'a t -> 'b t

  (** [forget]: extract a value from its context forgetfully

      For [x : 'a t], [forget x] will return the unique value [x' : 'a] such
      that [x' |> f_1 |> f_2 |> ... |> f_n === x], where each [f_i] is a partial
      application of [add] or [add_def].

      Since ['a t] is currently implemented as a 3-tuple containing a value of
      type ['a] and two context values, this amounts to the specific
      implementation [forget (x, _, _) = x].

      The following properties hold: * [forget (pure x) === x] * [forget (fmap f
      x) === f (forget x)] * [forget (add a x) === forget x] * [forget (add_def
      ~id ~entry x) === forget x] * [x === fmerge (fun y () -> y) (pure (forget
      x)) (fmap ignore x)] *)
  val forget : 'a t -> 'a

  (** [(>$<)]: reverse-argument-order version of [(<$>)]

      [x >$< f := fmap f x]

      This is especially useful as an alternative to [fmap] or [(<$>)] in cases
      where the argument [x] is much more concise than [f], and when parentheses
      or equivalent would be required around [f] in order to ensure the proper
      parsing precedence for [f <$> x] or [fmap f x].

      e.g. [x >$< incredibly_long_or_multi_argument_function_expression] [x >$<
      if external_condition then this_function else that_function] [x >$< fun a
      -> ...] [x >$< match n with | ...] [x >$< function | ...] *)
  val ( >$< ) : 'a t -> ('a -> 'b) -> 'b t

  (** [(<$>)]: infix-operator form of [fmap]

      [f <$> x := fmap f x].

      See also [(>$<)], which may be more appropriate in certain cases, but is
      semantically equivalent modulo argument order. *)
  val ( <$> ) : ('a -> 'b) -> 'a t -> 'b t

  (** [fmerge]: Functional merge

      [fmerge f] merges two context-wrapped values, by applying the two-argument
      function [f] to their payload values to determine the payload of the
      result, and performing simultaneous but independent right-biased unions on
      the associated context elements.

      Concretely, [fmerge] is defined and implemented such that: * [forget
      (fmerge f x y) === f (forget x) (forget y)] * [fmerge f (pure x) (pure y)
      === pure (f x y)] * [fmerge (fun a _ -> a) x (pure y) === x] * [fmerge
      (fun _ b -> b) (pure x) y === y] * [fmerge f (add s x) y === add s (fmerge
      f x y) === fmerge f x (add s y)] * [fmerge f x (add_def ~id ~entry y) ===
      fmerge (fun a () -> a) (fmerge f x y) (add_def ~id ~entry (pure ()))]

      Note that the final property only allows us to decompose the definitions
      of the right argument, as it is implemented using the right-biased
      [[T.union]], which additionally reports conflicting (non-equal) entries
      for identical keys in its left and right operands.

      Therefore, [fmerge (fun a _ -> a) x x === x]. *)
  val fmerge : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t

  (** [seq]: sequencing operator on the context monad

      [seq xs] performs a left-associative fold over a list of element type ['a
      t], aggregating the individual contexts of each element into a cumulative
      context over the list of bare payloads.

      The following properties hold: * [seq [] === pure []] * [seq (x @ y) =
      fmerge List.append (seq x) (seq y)] * [forget (seq xs) === List.map forget
      xs] *)
  val seq : 'a t list -> 'a list t

  (** [sseq]: Second-element seq

      Specialized variant of [seq] that upholds the same general properties but
      applies to lists of tuples in which only the second element bears context.

      Otherwise, it can be rewritten in terms of [[seq]]: [sseq pqs === seq
      (List.map (fun (p, q) -> q >$< fun q' -> (p,q')) pqs)] *)
  val sseq : ('a * 'b t) list -> ('a * 'b) list t
end

(** [transcoder_dir]: marker type used to indicate what direction a generalized
    transcoding operation is being perfromed in *)
type transcoder_dir = Encode | Decode

(** [method_kind]: indicates whether type-associated-functions or trait-methods
    should be fully quantified with explicit receivers ([Static]), or written as
    `.method_name(...)` calls on the implicit receiver value *)
type method_kind = Static | Dynamic

(** [TDef]: intermediate AST used to represent various styles of type-definition *)
module TDef : sig
  (** [t]: Encapsulation of the definition of a type, in terms of its level of
      complexity *)
  type t =
    | Monic of Ast.rust_type
        (** [Monic]: type-def with only a single (anonymous) term *)
    | Elems of Ast.rust_type list
        (** [Elems]: type-def with two or more anonymous terms *)
    | Fields of Ast.Decl.Type_def.Struct.field list
        (** [Fields]: type-def with one or more named terms *)
    | CStyle of Ast.Type.Prim.Int.repr * (string * int) list
        (** [CStyle]: type-enumeration with no payloads, only named constants *)
    | Variants of Ast.Type.Prim.Int.disc * (int * (string * t)) list
        (** [Variants]: type-enumeration with at least one payload, which can
            even be null *)

  (** [of_simplified]: convert an intermediate (simplified) type into a type-def
      value, along with the import- and definition-context accumulated through
      structural recursion *)
  val of_simplified : Simplified.t -> t Ctxt.t option
end

(** [gen_transcoder_call]: produce an AST value encapsulating a type-specific
    transcoder call

    [gen_transcoder_call ?kind ~dir enc args] returns a context-associated
    Rust-AST expression node, which determines the fully qualified method call
    (when [kind == Static]) in the appropriate transcoder direction, taking
    [args] to be the list of arguments, where if a receiver value is expected at
    all, it is implicitly assumed to be the first argument. *)
val gen_transcoder_call :
  ?kind:method_kind ->
  dir:transcoder_dir ->
  Simplified.t ->
  Ast.rust_val list ->
  Ast.rust_val Ctxt.t

(** [Gen]: module for actual generator logic *)
module Gen : sig
  (** [Conf]: Module for encapsulating the various configurable options of the
      generator *)
  module Conf : sig
    (** [TyDef]: configurations related to the definition of types *)
    module TyDef : sig
      (** [u]: Enumerated type controlling the style of type-definition for
          aliasable types. *)
      type u =
        | ForceAlias
            (** [ForceAlias]: Whenever aliasing is possible, use type-aliases
                instead of default behavior. *)
        | SuppressAlias
            (** [SuppressAlias]: Explicitly suppress all type-alias definitions
                that would normally be generated. *)

      (** [v]: Enumerated type controlling the field visibility of non-root struct definitions *)
      type v =
        | PublishRoot
            (** [PublishRoot]: For root type only, make all struct fields public, and otherwise
                leave their visibility unchanged
                *)
        | PublishAll
            (** [PublishAll]: For all types, make all struct fields public *)

      (** [t]: Cartesian product of aliasing and visibility configuration values *)
      type t = u option * v
    end

    (** [Impl]: configurations related to the implementation of traits *)
    module Impl : sig
      (** [t]: Enumerated type for forcing a specific style of
          trait-implementation for [Encode] and [Decode] traits: derive-macro,
          or explicit [impl] blocks. *)
      type t =
        | ForceDerive
            (** [ForceDerive]: Causes a particular trait to always be derived if
                possible. *)
        | SuppressDerive
            (** [SuppressDerive]: Causes a particular trait to always be
                explicitly implemented. *)

    end

    (** [tydef]: Type-definition behavior override *)
    type tydef = TyDef.t

    (** [impl]: Trait-implementation behavior ovveride *)
    type impl = Impl.t
  end

  (** [mk_tname]: Produces the finalized type-name of a given raw identifier *)
  val mk_tname : string -> string

  (** [generate]: Generate the entire rust codec module for a given schema

      Takes optional arguments [?force_alias] and [?force_derive] that override
      the unspecified policy on when to use type aliases, and whether to derive
      or explicitly implement the [Encode] and [Decode] traits.

      Also takes an optional argument [?images], a list of hexadecimal strings
      to use as test cases for roundtrip (i.e. decoding and re-encoding)
      invariance checking.

      Requires a name to use for the codec root type, which can also determine
      the names given to anonymous auxilliary types. *)
  val generate :
    ?tyconf:Conf.tydef ->
    ?force_derive:Conf.impl ->
    ?images:string list ->
    string ->
    Simplified.t ->
    Ast.rust_module
end
