module Common = Common

let addstr = Common.Printer.str

let addchr = Common.Printer.chr

module Preamble = struct
  type entity_group = string list

  type mod_path = string list

  type macro_use = bool

  type t = {
    crates : string list;
    uses : (mod_path * entity_group * macro_use) list;
  }

  let of_rt_imports : string list * (bool * bool * bool) -> t =
   fun (extra, (_enc, _dec, _est)) ->
    {
      crates = ["rust_runtime"];
      uses =
        [
          (["rust_runtime"], extra, false)
          (* @ (if _enc then [(["encode_derive"], ["Encode"], false)] else []) @
             (if _dec then [(["decode_derive"], ["Decode"], false)] else []) @
             if _est then [(["estimable_derive"], ["Estimable"], false)] else
             [] *);
        ];
    }

  let default_preamble = of_rt_imports ([], (false, false, false))

  let pp {crates; uses} =
    let open Common.Printer in
    let pp_crate crate_name =
      addstr "extern crate " |>> addstr crate_name |>> addstr ";"
    and pp_use (path, grp, mac) =
      (if mac then addstr "#[macro_use]" |>> addchr '\n' else ign)
      |>> addstr "use "
      |>> seq ~sep:(addstr "::") addstr path
      |>> (match grp with
          | [] -> ign
          | [x] -> addstr "::" |>> addstr x
          | xs ->
              addstr "::{" |>> seq ~sep:(addchr ',') addstr xs |>> addstr "}")
      |>> addchr ';'
    in
    addstr "#![allow(unused_imports)]"
    |>> addchr '\n'
    |>> seq ~sep:(addchr '\n') pp_crate crates
    |>> addchr '\n'
    |>> seq ~sep:(addchr '\n') pp_use uses
end

(** Sequentially prints and concatenates a list of values and surrounds the
    result in curly-braces *)
let pp_block ?sep ~inner ?prelude ?(trailing_sep = false) block =
  let open Common.Printer in
  let _sep = opt addstr sep in
  let sep = _sep |>> addchr '\n' in
  addchr '{'
  |>> wrap
        (addchr '\n'
        |>> (match prelude with None -> ign | Some x -> x |>> addchr '\n')
        |>> seq ~sep inner block
        |>> if trailing_sep then _sep else ign)
  |>> addchr '\n' |>> addchr '}' |>> addchr '\n'

module Type = struct
  type str_kind = Slice | Struct | Hex

  module Prim = struct
    module Int = struct
      type lenpref = [`U8 | `U16 | `U30]

      type compat = [`U8 | `I8 | `U16 | `I16]

      type reserved = [`U32 | `I32]

      type range_backed = [compat | reserved]

      type custom = [`U30 | `I31]

      type codec = [compat | custom | `I32 | `I64]

      type disc = [`U8 | `U16]

      type repr = [disc | `U32]

      type t =
        | Int of codec
        | RangedInt of (range_backed * int * int)
        | Reserved of reserved

      let _to_string : [< repr] -> string = function
        | `U8 -> "u8"
        | `U16 -> "u16"
        | `U32 -> "u32"

      let rec to_string ~sep ?(qualified = true) = function
        | Int `U8 -> "u8"
        | Int `I8 -> "i8"
        | Int `U16 -> "u16"
        | Int `I16 -> "i16"
        | Int `U30 -> if qualified then "::rust_runtime::u30" else "u30"
        | Int `I31 -> if qualified then "::rust_runtime::i31" else "i31"
        | Int `I32 -> "i32"
        | Int `I64 -> "i64"
        | Reserved `U32 -> "u32"
        | Reserved `I32 -> "i32"
        | RangedInt ((#compat as rb), min, max) ->
            Printf.sprintf
              "%sRangedInt%s<%s,%d,%d>"
              (if qualified then "::rust_runtime::" else "")
              sep
              (to_string ~sep:"" (Int rb))
              min
              max
        | RangedInt ((#reserved as rb), min, max) ->
            Printf.sprintf
              "%sRangedInt%s<%s,%d,%d>"
              (if qualified then "::rust_runtime::" else "")
              sep
              (to_string ~sep:"" (Reserved rb))
              min
              max
    end

    module Float = struct
      type t = F64 | RangedFloat of (float * float)

      let to_string ~sep ?(qualified = true) = function
        | F64 -> "f64"
        | RangedFloat (min, max) ->
            let min64 = Int64.bits_of_float min
            and max64 = Int64.bits_of_float max in
            Printf.sprintf
              "%sRangedFloat%s<%#Lx,%#Lx>"
              (if qualified then "::rust_runtime::" else "")
              sep
              min64
              max64
    end

    type t =
      | Integral of Int.t
      | Floating of Float.t
      | Boolean
      | Unit
      | ByteString of int
      | CharString of int
      | VarString
      | VarBytes
      | N
      | Z

    let to_string ?(expand = false) ?(qualified = true) =
      let sep = if expand then "::" else "" in
      let qp = if qualified then "::rust_runtime::" else "" in
      function
      | Integral i -> Int.to_string ~sep i
      | Floating f -> Float.to_string ~sep f
      | Unit -> if expand then "<()>" else "()"
      | Boolean -> "bool"
      | VarString -> if qualified then "std::string::String" else "String"
      | VarBytes -> if qualified then "::rust_runtime::Bytes" else "Bytes"
      | Z -> if qualified then "::rust_runtime::Z" else "Z"
      | N -> if qualified then "::rust_runtime::N" else "N"
      | ByteString n -> Printf.sprintf "%sByteString%s<%d>" qp sep n
      | CharString n -> Printf.sprintf "%sCharString%s<%d>" qp sep n
  end

  module Comp = struct
    type 'a t =
      | Sequence of 'a
      | LimSeq of 'a * int * [`N | Prim.Int.lenpref] option
      | FixSeq of 'a * int
      | Padded of 'a * int
      | VPadded of 'a * int
      | Option of bool * 'a
      | Dynamic of [`N | Prim.Int.lenpref] * 'a
      | AutoBox of 'a

    let to_string ~inner ?(expand = false) ?(qualified = true) =
      let sep = if expand then "::" else "" in
      let pref = if qualified then "::rust_runtime::" else "" in
      function
      | Sequence t -> Printf.sprintf "%sSequence%s<%s>" pref sep (inner t)
      | LimSeq (t, n, Some `N) ->
          Printf.sprintf "%sLimSeq%s<%s,%d,N>" pref sep (inner t) n
      | LimSeq (t, n, Some (#Prim.Int.lenpref as x)) ->
          Printf.sprintf
            "%sLimSeq%s<%s,%d,%s>"
            pref
            sep
            (inner t)
            n
            Prim.Int.(to_string ~sep:"" (Int (x :> codec)))
      | LimSeq (t, n, None) ->
          Printf.sprintf "%sLimSeq%s<%s,%d>" pref sep (inner t) n
      | FixSeq (t, n) -> Printf.sprintf "%sFixSeq%s<%s,%d>" pref sep (inner t) n
      | Padded (t, n) -> Printf.sprintf "%sPadded%s<%s,%d>" pref sep (inner t) n
      | VPadded (t, n) ->
          Printf.sprintf "%sVPadded%s<%s,%d>" pref sep (inner t) n
      | Option (true, t) ->
          Printf.sprintf
            "%sOption%s<%s>"
            (if qualified then "std::option::" else "")
            sep
            (inner t)
      | Option (false, t) ->
          Printf.sprintf "%sNullable%s<%s>" pref sep (inner t)
      | AutoBox t -> Printf.sprintf "%sAutoBox%s<%s>" pref sep (inner t)
      | Dynamic (`N, y) -> Printf.sprintf "%sDynamic%s<N,%s>" pref sep (inner y)
      | Dynamic ((#Prim.Int.lenpref as x), y) ->
          Printf.sprintf
            "%sDynamic%s<%s,%s>"
            pref
            sep
            Prim.Int.(to_string ~sep:"" (Int (x :> codec)))
            (inner y)
  end

  module Atom = struct
    type t = Prim of Prim.t | Comp of t Comp.t | LDef of string

    let rec to_string ?(expand = false) ?(qualified = true) = function
      | Prim p -> Prim.to_string ~expand ~qualified p
      | Comp c ->
          Comp.to_string
            ~expand
            ~qualified
            ~inner:(to_string ~expand ~qualified)
            c
      | LDef tname -> tname
  end

  type t =
    | Borrow of t
    | Atom of Atom.t
    | Tuple of Atom.t list
    | String of str_kind
    | Usize
    | Verbatim of string
    | Generic_param of string
    | Impl of string
    | StrictBuilder
    | Self

  let ( !& ) x = Borrow x

  let _str = String Slice

  let _String = String Struct

  let _HexString = String Hex

  let verb x = Verbatim x

  let impl x = Impl x

  let self = Self

  let _Sequence = function
    | Atom t -> Atom (Comp (Sequence t))
    | _ -> assert false

  let _AutoBox = function
    | Atom t -> Atom (Comp (AutoBox t))
    | _ -> assert false

  let _Option = function
    | Atom t -> Atom (Comp (Option (true, t)))
    | _ -> invalid_arg "_Option requires (Atom _)"

  let _Nullable = function
    | Atom t -> Atom (Comp (Option (false, t)))
    | _ -> invalid_arg "_Nullable requires (Atom _)"

  let _Dynamic (x, y) =
    match x with
    | Atom (Prim (Integral (Int (#Prim.Int.lenpref as x)))) -> (
        match y with
        | Atom y -> Atom (Comp (Dynamic (x, y)))
        | _ -> assert false)
    | _ -> assert false

  let _Tuple =
    let unprim = function
      | Atom x -> x
      | _ -> invalid_arg "Can only unprim a Prim"
    in
    function
    | [] -> Atom (Prim Unit) | [x] -> x | xs -> Tuple (List.map unprim xs)

  let usize = Usize

  let u8 = Atom (Prim (Integral (Int `U8)))

  let i8 = Atom (Prim (Integral (Int `I8)))

  let u16 = Atom (Prim (Integral (Int `U16)))

  let i16 = Atom (Prim (Integral (Int `I16)))

  let u30 = Atom (Prim (Integral (Int `U30)))

  let u32 = Atom (Prim (Integral (Reserved `U32)))

  let i31 = Atom (Prim (Integral (Int `I31)))

  let i32 = Atom (Prim (Integral (Int `I32)))

  let _i32 = Atom (Prim (Integral (Reserved `I32)))

  let i64 = Atom (Prim (Integral (Int `I64)))

  let f64 = Atom (Prim (Floating F64))

  let bool = Atom (Prim Boolean)

  let unit = Atom (Prim Unit)

  let bytestring n = Atom (Prim (ByteString n))

  let charstring n = Atom (Prim (CharString n))

  let _N = Atom (Prim N)

  let _Z = Atom (Prim Z)

  let peel :
      t ->
      ( string,
        'a Common.Printer.fragment -> 'a Common.Printer.fragment )
      Either.t
      * t option = function
    | Borrow t -> (Left "&", Some t)
    | String Hex -> (Left "HexString", None)
    | String Struct -> (Left "String", None)
    | String Slice -> (Left "&str", None)
    | Usize -> (Left "usize", None)
    | Atom at -> (Left (Atom.to_string at), None)
    | Tuple xs ->
        ( Right
            Common.Printer.(
              fun _ ->
                addchr '('
                |>> seq
                      ~sep:(addchr ',')
                      (fun x -> addstr (Atom.to_string x))
                      xs
                |>> addchr ')'),
          None )
    | Verbatim lit -> (Left lit, None)
    | Impl trait -> (Left ("impl " ^ trait), None)
    | Generic_param t -> (Left t, None)
    | StrictBuilder -> (Left "StrictBuilder", None)
    | Self -> (Left "Self", None)

  let pp x =
    let open Common.Printer in
    let rec pp' x =
      match peel x with
      | Left shard, remainder -> addstr shard |>> opt pp' remainder
      | Right inj, remainder -> inj (opt pp' remainder)
    in
    pp' x
end

type vis = Pub

let pp_vis : vis -> _ Common.Printer.fragment = fun Pub -> addstr "pub "

type mut = Mutable | Immutable

module Expr = struct
  module Entity = struct
    type t = Local of string | Scoped of string list * string | Self_value

    let loc s = Local s

    let ( #:: ) pref ent = Scoped ([pref], ent)

    let ( @:: ) pref = function
      | Self_value -> invalid_arg "Cannot pre-qualify 'self' entity"
      | Local name -> pref #:: name
      | Scoped (path, name) -> Scoped (pref :: path, name)

    let pp =
      let open Common.Printer in
      function
      | Local s -> addstr s
      | Scoped (nodes, s) ->
          seq ~sep:(addstr "::") addstr nodes |>> addstr "::" |>> addstr s
      | Self_value -> addstr "self"
  end

  include Entity

  type t =
    | Entity of Entity.t
    | String_lit of string
    | Attribute of t * string
    | Function_call of t * t list
    | Tuple_expr of t list
    | Struct_expr of string * (string * t option) list
    | Paren of t
    | Borrow of t
    | BorrowMut of t
    | Question of t
    | Summation of t list

  let self = Entity Self_value

  let ent x = Entity x

  let loc_ent x = Entity (Local x)

  let ( $. ) : t -> string -> t = fun x att -> Attribute (x, att)

  let ( .$() ) : t -> t list -> t = fun f args -> Function_call (f, args)

  let ( .${} ) : string -> (string * t option) list -> t =
   fun sname fill -> Struct_expr (sname, fill)

  let tup xs = Tuple_expr xs

  let sum xs = Summation xs

  let paren x = Paren x

  let ( !& ) x = Borrow x

  let ( >? ) : t -> unit -> t = fun t () -> Question t

  let rec pp =
    let open Common.Printer in
    function
    | Entity e -> Entity.pp e
    | String_lit s -> addstr "\"" |>> addstr s |>> addstr "\""
    | Attribute (v, att) -> pp v |>> addstr "." |>> addstr att
    | Tuple_expr elts ->
        addstr "(" |>> seq ~sep:(addstr ", ") pp elts |>> addstr ")"
    | Struct_expr (sname, fill) ->
        addstr sname |>> addstr " {"
        |>> seq
              ~sep:(addstr ", ")
              (fun (x, y) -> addstr x |>> opt (fun x -> addstr ": " |>> pp x) y)
              fill
        |>> addstr "}"
    | Function_call (f, args) ->
        pp f |>> addstr "(" |>> seq ~sep:(addstr ", ") pp args |>> addstr ")"
    | Paren expr -> addchr '(' |>> pp expr |>> addchr ')'
    | Borrow v -> addstr "&" |>> pp v
    | BorrowMut v -> addstr "&mut " |>> pp v
    | Question v -> pp v |>> addchr '?'
    | Summation vs -> seq ~sep:(addstr " + ") pp vs
end

module Stmt = struct
  type t =
    | Let of mut * string * Type.t option * Expr.t
    | Bare_expression of Expr.t
    | Explicit_return of Expr.t
    | Naked_return of Expr.t

  let pp =
    let open Common.Printer in
    function
    | Let (mut, binding, typesig, value) ->
        addstr "let"
        |>> (match mut with
            | Mutable -> addstr " mut "
            | Immutable -> addstr " ")
        |>> addstr binding
        |>> opt (fun ty -> addstr ": " |>> Type.pp ty) typesig
        |>> addstr " = " |>> Expr.pp value |>> addchr ';'
    | Bare_expression v -> Expr.pp v |>> addchr ';'
    | Naked_return v -> Expr.pp v
    | Explicit_return v -> addstr "return " |>> Expr.pp v |>> addchr ';'
end

module Annot = struct
  type t = Derive of string list | CfgTest | Test | Other of string

  let pp : t -> _ Common.Printer.fragment =
    let open Common.Printer in
    function
    | Derive traits ->
        addstr "#[derive("
        |>> seq ~sep:(addstr ", ") addstr traits
        |>> addstr ")]" |>> addchr '\n'
    | CfgTest -> addstr "#[cfg(test)]" |>> addchr '\n'
    | Test -> addstr "#[test]" |>> addchr '\n'
    | Other verbatim ->
        addstr "#[" |>> addstr verbatim |>> addstr "]" |>> addchr '\n'
end

module Decl = struct
  module Generic = struct
    module Constraints = struct
      type u = string

      type t = u list

      let pp : t -> _ Common.Printer.fragment =
        let open Common.Printer in
        function
        | [] -> ign
        | _ as consts -> addstr ": " |>> seq ~sep:(addstr " + ") addstr consts
    end

    type tparam = string * Constraints.t

    let ( %+ ) : tparam -> Constraints.u -> tparam =
     fun (tp, cs) c' -> (tp, cs @ [c'])

    let ( |: ) : string -> Constraints.u -> tparam = fun tp c -> (tp, [c])

    type t = tparam list

    let ( <> ) : 'a. (t -> 'a -> 'b) -> 'a -> 'b = fun f x -> f [] x

    let ( > ) : 'a. (t -> 'a -> 'b) -> 'a -> 'b = fun f x -> f [] x

    let ( < ) : 'a. (t -> 'a -> 'b) -> tparam -> t -> 'a -> 'b =
     fun f par pars -> f (par :: pars)

    let ( & ) : 'a. (t -> 'a -> 'b) -> tparam -> t -> 'a -> 'b =
     fun f par pars -> f (par :: pars)

    let pp_tparam (param_name, param_constraints) =
      let open Common.Printer in
      addstr param_name |>> Constraints.pp param_constraints

    let pp : t -> _ Common.Printer.fragment =
      let open Common.Printer in
      function
      | [] -> ign
      | _ as tparams ->
          addstr "<" |>> seq ~sep:(addstr ",") pp_tparam tparams |>> addstr ">"
  end

  include Generic

  module Fn = struct
    module Sig = struct
      type param_kind = Value of mut | Borrow of mut

      type param =
        | Named_param of param_kind * (string * Type.t)
        | Self_param of param_kind
            (** We can only have one of these and it must be the first *)

      type paramlist = param list

      type t = paramlist * Type.t option

      let ( #: ) : string -> Type.t -> param =
       fun pname ptype -> Named_param (Value Immutable, (pname, ptype))

      let ( @-> ) : paramlist -> Type.t -> t = fun pars typ -> (pars, Some typ)

      let ( @-| ) : paramlist -> unit -> t = fun pars () -> (pars, None)

      let self = Self_param (Value Immutable)

      let ( !& ) = function
        | Self_param (Value m) -> Self_param (Borrow m)
        | Named_param (Value m, par) -> Named_param (Borrow m, par)
        | Self_param (Borrow _) ->
            invalid_arg "Cannot borrow an already-borrowed self-parameter"
        | Named_param (Borrow _, _) ->
            invalid_arg "Cannot borrow an already-borrowed named parameter"

      let mut = function
        | Self_param (Value Immutable) -> Self_param (Value Mutable)
        | Named_param (Value Immutable, par) -> Named_param (Value Mutable, par)
        | Named_param (Value Mutable, _) | Self_param (Value Mutable) ->
            invalid_arg
              "mut keyword operator should only be used on immutable parameters"
        | Named_param (Borrow _, _) | Self_param (Borrow _) ->
            invalid_arg
              "mut keyword operator should be used before !& prefix operator, \
               not after"

      let pp_paramkind : param_kind -> _ Common.Printer.fragment =
        let open Common.Printer in
        let pp_mut = function Immutable -> ign | Mutable -> addstr "mut " in
        fun pk ->
          match pk with
          | Value m -> pp_mut m
          | Borrow m -> addstr "&" |>> pp_mut m

      let pp_paramlist : paramlist -> _ Common.Printer.fragment =
        let open Common.Printer in
        fun plist ->
          let pp_param = function
            | Named_param (pk, (pname, ptype)) ->
                addstr pname |>> addstr ": " |>> pp_paramkind pk
                |>> Type.pp ptype
            | Self_param pk -> pp_paramkind pk |>> addstr "self"
          in
          addstr "(" |>> seq ~sep:(addstr ", ") pp_param plist |>> addstr ")"

      let pp : t -> _ Common.Printer.fragment =
        let open Common.Printer in
        fun (params, rhs_sig) ->
          pp_paramlist params
          |>> opt (fun x -> addstr " -> " |>> Type.pp x) rhs_sig
    end

    include Sig

    type t = string * Generic.t * Sig.t * Stmt.t list

    let pp : t -> _ Common.Printer.fragment =
      let open Common.Printer in
      fun (fname, generics, fsig, body) ->
        addstr "fn " |>> addstr fname |>> Generic.pp generics |>> Sig.pp fsig
        |>> addstr " "
        |>> pp_block ~inner:Stmt.pp body
  end

  include Fn

  module Type_def = struct
    module Struct = struct
      type field = {
        field_vis : vis option;
        field_name : string;
        field_type : Type.t;
      }

      type anon_arg = {arg_vis : vis option; arg_type : Type.t}

      type t = Unit | Tuple of anon_arg list | Record of field list

      let pub_field : field -> field = fun x -> {x with field_vis = Some Pub}

      let pub_arg : anon_arg -> anon_arg = fun x -> {x with arg_vis = Some Pub}

      let pub : t -> t = function
        | Unit -> Unit
        | Tuple args -> Tuple (List.map pub_arg args)
        | Record fields -> Record (List.map pub_field fields)

      let pp_field {field_vis; field_name; field_type} :
          _ Common.Printer.fragment =
        let open Common.Printer in
        opt pp_vis field_vis |>> addstr field_name |>> addstr ": "
        |>> Type.pp field_type

      let pp_arg {arg_vis; arg_type} : _ Common.Printer.fragment =
        let open Common.Printer in
        opt pp_vis arg_vis |>> Type.pp arg_type

      let pp : ?in_variant:bool -> t -> _ Common.Printer.fragment =
       fun ?(in_variant = false) ->
        let open Common.Printer in
        function
        | Unit -> ign
        | Tuple ts ->
            addstr "("
            |>> seq ~sep:(addstr ",") pp_arg ts
            |>> addchr ')'
            |>> if in_variant then ign else addchr ';'
        | Record fs ->
            addstr " { " |>> seq ~sep:(addstr ", ") pp_field fs |>> addstr " }"
    end

    module Variant = struct
      type _ t =
        | CStyle : string * int -> int t
        | Data : int * string * Struct.t -> Struct.t t

      let pp : type a. a t -> _ Common.Printer.fragment =
        let open Common.Printer in
        function
        | CStyle (variant_name, discr) ->
            addstr variant_name |>> addstr " = "
            |>> addstr (string_of_int discr)
        | Data (tag_val, variant_name, contents) ->
            addstr (string_of_int tag_val)
            |>> addstr " => " |>> addstr variant_name |>> addchr ' '
            |>> Struct.pp ~in_variant:true contents
    end

    module Varlist = struct
      type t =
        | UnC of Type.Prim.Int.repr * int Variant.t list
        | UnData of Type.Prim.Int.disc * string * Struct.t Variant.t list

      let pp : type_name:string -> t -> _ Common.Printer.fragment =
        let open Common.Printer in
        fun ~type_name -> function
          | UnC (repr, enums) ->
              wrap
                (addstr "cstyle!(" |>> addstr type_name |>> addchr ','
                |>> addstr Type.Prim.Int.(_to_string repr)
                |>> addchr ','
                |>> pp_block ~sep:"," ~inner:Variant.pp enums
                |>> addstr ");")
          | UnData (disc, modname, vars) ->
              wrap
                (addstr "data!(" |>> addstr type_name |>> addchr ','
                |>> addstr Type.Prim.Int.(_to_string disc)
                |>> addchr ',' |>> addstr modname |>> addchr ','
                |>> pp_block ~sep:"," ~inner:Variant.pp ~trailing_sep:true vars
                |>> addstr ");")
    end

    type t = Struct of Struct.t | Enum of Varlist.t

    let keyword_for : t -> string option = function
      | Struct _ -> Some "struct"
      | Enum _ -> None

    let pp : type_name:string -> t -> _ Common.Printer.fragment =
      let open Common.Printer in
      fun ~type_name -> function
        | Struct x -> addstr type_name |>> Struct.pp x
        | Enum vl -> Varlist.pp ~type_name vl
  end

  type t =
    | DTypeAlias of vis option * (string * Type.t)
    | DTypeDef of vis option * (string * Type_def.t)
    | DFunction of vis option * Fn.t
    | DTraitImpl of {trait_name : string; type_name : string; methods : t list}
    | DTestsModule of {contents : t list}
    | DAnnot of Annot.t * t

  let talias alias_name definition = DTypeAlias (None, (alias_name, definition))

  let ( .=() ) wrapper_name internals =
    DTypeDef (None, (wrapper_name, Type_def.(Struct (Struct.Tuple internals))))

  let ( .={} ) record_name internals =
    DTypeDef (None, (record_name, Type_def.(Struct (Struct.Record internals))))

  (* FIXME - hardcoded pub on all fields of structs *)
  let ( <:> ) : string -> Type.t -> Type_def.Struct.field =
   fun field_name field_type -> {field_vis = Some Pub; field_name; field_type}

  let ( <=> ) : string -> int -> int Type_def.Variant.t =
   fun var_name var_val -> CStyle (var_name, var_val)

  let ( := ) name def = talias name def

  let enum enum_name variants =
    DTypeDef (None, (enum_name, Type_def.(Enum variants)))

  let fn fname constrs fsig body = DFunction (None, (fname, constrs, fsig, body))

  let testfn tfname body =
    DAnnot (Annot.Test, DFunction (None, (tfname, [], [] @-| (), body)))

  let rec pub : t -> t = function
    | DAnnot (ann, inner) -> DAnnot (ann, pub inner)
    | DTypeAlias (_, def) -> DTypeAlias (Some Pub, def)
    | DTypeDef (_, def) -> DTypeDef (Some Pub, def)
    | DFunction (_, def) -> DFunction (Some Pub, def)
    | other -> other

  let rec pp =
    let open Common.Printer in
    function
    | DAnnot (annot, decl) -> Annot.pp annot |>> pp decl
    | DTypeAlias (_vis, (alias_name, definition)) ->
        opt pp_vis _vis |>> addstr "type " |>> addstr alias_name
        |>> addstr " = " |>> Type.pp definition |>> addstr ";"
    | DTypeDef (_vis, (type_name, definition)) ->
        opt pp_vis _vis
        |>> opt
              (fun x -> addstr x |>> addchr ' ')
              (Type_def.keyword_for definition)
        |>> Type_def.pp ~type_name definition
    | DFunction (_vis, f) -> opt pp_vis _vis |>> Fn.pp f
    | DTraitImpl {trait_name; type_name; methods} ->
        addstr "impl " |>> addstr trait_name |>> addstr " for "
        |>> addstr type_name |>> pp_block ~inner:pp methods
    | DTestsModule {contents} ->
        let prelude =
          addstr "use super::*;" |>> addchr '\n'
          |>> addstr "use rust_runtime::{hex,HexString};"
        in
        let annot = Annot.CfgTest
        and header = addstr "mod tests "
        and body = pp_block ~inner:pp ~prelude contents in
        Annot.pp annot |>> header |>> body
end

type rust_val = Expr.t

type rust_type = Type.t

type rust_entity = Expr.Entity.t

type rust_stmt = Stmt.t

type rust_decl = Decl.t

type rust_module = {preamble : Preamble.t; decls : rust_decl list}

let pp_module {preamble; decls} =
  let open Common.Printer in
  let preamble' = Preamble.pp preamble
  and decls' = seq ~sep:(addchr '\n') Decl.pp decls in
  preamble' |>> addchr '\n' |>> decls'
