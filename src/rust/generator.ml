open Ast
module Simplified = Common.Simplified

module Keywords = struct
  let rectify : string -> bool * string =
   fun s ->
    match s with
    | "as" | "async" | "await" | "break" | "const" | "continue" | "crate"
    | "dyn" | "else" | "enum" | "extern" | "false" | "fn" | "for" | "if"
    | "impl" | "in" | "let" | "loop" | "match" | "mod" | "move" | "mut" | "pub"
    | "ref" | "return" | "self" | "Self" | "static" | "struct" | "super"
    | "trait" | "true" | "type" | "unsafe" | "use" | "where" | "while" ->
        (true, "r#" ^ s) (* Strict Rust keywords as of Rust 2018 *)
    | "abstract" | "become" | "box" | "do" | "final" | "macro" | "override"
    | "priv" | "try" | "typeof" | "unsized" | "virtual" | "yield" ->
        (true, "r#" ^ s) (* Reserved Rust keywords as of Rust 2018 *)
    | _ -> (false, s)
end

let elim_unit_field = List.filter (fun (_, y) -> match y with Simplified.Base Unit -> false | _ -> true)

let sanitize_name ~f ~sep raw_name =
  let sanitize = function
    | s when String.(sub s 0 1 = "0") -> f "proto" ^ f s
    | s -> s |> f
  in
  let undash = String.map (function ' ' | '-' -> '_' | c -> c) in
  String.split_on_char '.' raw_name
  |> List.map (fun x -> x |> undash |> sanitize)
  |> String.concat sep |> Keywords.rectify
  |> fun (_, y) -> y

let normalize_field_id fname =
  sanitize_name ~f:String.lowercase_ascii ~sep:"_" fname

let normalize_type_id : string -> string =
  sanitize_name
    ~f:(fun y ->
      String.split_on_char '_' y
      |> List.map String.capitalize_ascii
      |> String.concat "")
    ~sep:""

let normalize_submodule_id : string -> string =
  sanitize_name ~f:String.lowercase_ascii ~sep:"_"

let normalize_constructor : string -> string =
 fun x -> x |> sanitize_name ~f:Fun.id ~sep:""

(** [sem]: Second Element Map

    Applies the provided function to only the second element of a 2-tuple,
    returning preserving the first element.

    Equivalent to Haskell's [fmap] over the functor [(,) a]. *)
let sem : 'a 'b. ('b -> 'c) -> 'a * 'b -> 'a * 'c = fun f (c, x) -> (c, f x)

(* let optsem : 'a 'b. ('b -> 'c option) -> 'a * 'b -> ('a * 'c) option = fun f
   (c, x) -> match f x with Some y -> Some (c, y) | None -> None

   let map_join : 'a 'b. ('a -> 'b option) -> 'a list -> 'b list option = fun f
   l -> let exception N in try Some (List.map (fun a -> match f a with None ->
   raise N | Some b -> b) l) with N -> None *)

(** [Ctxt]: state monad that allows AST-generator primitives and combinators to
    indicate what imports they need from the runtime library in order for the
    associated AST node to compile properly, as well as an accumulated map of denested
    type definitions to include in the generated module *)
module Ctxt = struct
  (** State-type and assoiated definitions for the Ctxt monad *)
  module State = struct
    (** Abstraction over the implementation of the import-set state *)
    module S = struct
      include Set.Make (String)

      type nonrec t = t * (bool * bool * bool)

      (** Adds a runtime import to import-set, if it is has not already been added

        The strings ["deriveEncode"], ["deriveDecode"], or ["deriveEstimable"]
        are not handled verbatim and are instead interpreted as directives to
        import the indicated derive-macro, as well as the associated trait.

        All other strings are treated as normal runtime imports.
      *)
      let add = function
        | "deriveEncode" ->
            fun (x, (_, dec, est)) -> (add "Encode" x, (true, dec, est))
        | "deriveDecode" ->
            fun (x, (enc, _, est)) -> (add "Decode" x, (enc, true, est))
        | "deriveEstimable" ->
            fun (x, (enc, dec, _)) -> (add "Estimable" x, (enc, dec, true))
        | s -> fun (x, derives) -> (add s x, derives)

      (** Merges two import-sets, taking the union of the items they import *)
      let union (x, (enc, dec, est)) (x', (enc', dec', est')) =
        (union x x', (enc || enc', dec || dec', est || est'))

      (** Empty set of impots *)
      let empty = (empty, (false, false, false))

      (** Returns a tuple consisting of the standard import-list, followed by a tuple of three booleans
      indicating whether the Encode, Decode, and Estimate derive-macros should be imported,
      in that precise order.
      *)
      let elements (x, (enc, dec, est)) = (elements x, (enc, dec, est))
    end

    (** Map whose key are strings holding the normalized names of denested types

      Although technically polymorphic, the associated values will properly be type-definitions
    in practice
    *)
    module T = struct
      include Map.Make (String)

      (** Merges two type-dictionaries, taking the associated definition of the
          second dictionary when keys overlap.

          Takes an optional parameter of an inequality-test function over the entry-type,
          defaulting to [Stdlib.(<>)].

          If keys are duplicated and their entries are deemed to be unequal,
          an error message will be printed but the right-bias will still be
          followed, without any exceptions or divergent logic.
          *)
      let union :
          ?trace:('a -> unit) -> ?neq:('a -> 'a -> bool) -> 'a t -> 'a t -> 'a t
          =
        let f trace neq id x y =
          if neq x y then
            Format.eprintf
              "@[Rust.Generator.Ctxt.State.T.union:@ Conflicting values for \
               key '%s'@,\
               Defaulting to right-biased override...]@."
              id ;
          trace x ;
          trace y ;
          Some y
        in
        fun ?(trace = ignore) ?(neq = Stdlib.( <> )) -> union (f trace neq)
    end

    (** imported entity set-type *)
    type ents = S.t

    (** Type-dictionary for general denested types *)
    type defs = Simplified.t T.t

    (** Type of the state held by the Ctxt monad *)
    type t = {imports : ents; definitions : defs}

    (** The 'empty' state containing no imports or denested types *)
    let empty : t = {imports = S.empty; definitions = T.empty}

    let add_import : string -> t -> t =
     fun s ({imports; _} as st) -> {st with imports = S.add s imports}

    let add_definition : id:string -> entry:Simplified.t -> t -> t =
     fun ~id ~entry ({definitions; _} as st) ->
      {st with definitions = T.add id entry definitions}

    let union : t -> t -> t =
     (* let trace x = prerr_string "value-trace: " ; Simplified.Verbose.pp x |>
        Simplified.Verbose.to_string |> prerr_endline in *)
     fun st st' ->
      {
        imports = S.union st.imports st'.imports;
        definitions = T.union (* ~trace *) st.definitions st'.definitions;
      }

    let has_def : string -> t -> bool = fun s x -> T.mem s x.definitions

    let defs : t -> (string * Simplified.t) list =
     fun x -> T.bindings x.definitions

    let items : t -> string list * (bool * bool * bool) =
     fun x -> S.elements x.imports
  end

  (** Functor that associates a generated AST node with a list of runtime
      entities it requires be imported in the prelude in order to compile and
      any subordinate definitions that must be predefined in addition *)
  type 'a t = 'a * State.t

  let return : 'a -> 'a t = fun x -> (x, State.empty)

  let pure = return

  let add : 'a. string -> 'a t -> 'a t =
   fun s (x, st) -> (x, State.add_import s st)

  let add_def : id:string -> entry:Simplified.t -> 'a t -> 'a t =
   fun ~id ~entry (x, st) -> (x, State.add_definition ~id ~entry st)

  let bind : 'a t -> ('a -> 'b t) -> 'b t =
   fun (x, st) f ->
    let y, st' = f x in
    (y, State.union st st')

  let ( >>= ) = bind

  let obind : 'a t option -> ('a -> 'b t option) -> 'b t option = function
    | None -> fun _ -> None
    | Some (x, st) -> (
        fun f ->
          match f x with
          | None -> None
          | Some (y, st') -> Some (y, State.union st st'))

  let ( >>? ) = obind

  let fmap : ('a -> 'b) -> 'a t -> 'b t = fun f (x, st) -> (f x, st)

  let omap : ('a -> 'b) -> 'a t option -> 'b t option =
   fun f -> Option.map (fmap f)

  let forget : 'a. 'a t -> 'a = fun (x, _) -> x

  let ( >$< ) x f = fmap f x

  let ( <$> ) f x = fmap f x

  let fmerge : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t =
   fun f (a, st_a) (b, st_b) -> (f a b, State.union st_a st_b)

  let rec seq : 'a t list -> 'a list t = function
    | [] -> return []
    | x :: xs -> fmerge List.cons x @@ seq xs

  let rec sseq : ('a * 'b t) list -> ('a * 'b) list t = function
    | [] -> return []
    | (a, x) :: xs -> fmerge List.cons (x >$< fun y -> (a, y)) @@ sseq xs
end

type transcoder_dir = Encode | Decode

(** Convert an assumed-camelcase identifier into snake-case.

    Currently just makes everything lowercase, which is enough for now but not
    the technically correct solution. *)
let to_snakecase = String.lowercase_ascii

type method_kind = Static | Dynamic

module TDef = struct
  type t =
    | Monic of Ast.Type.t
    | Elems of Ast.Type.t list
    | Fields of Ast.Decl.Type_def.Struct.field list
    | CStyle of Type.Prim.Int.repr * (string * int) list
    | Variants of Type.Prim.Int.disc * (int * (string * t)) list

  let rec of_base : Common.Simplified.base_t -> Ast.Type.Atom.t Ctxt.t option =
    let open Type in
    let open Type.Prim in
    let pure = function
      | Type.(Atom x) -> Ctxt.pure x
      | _ -> invalid_arg "only atom types supported by base_to_atom"
    in
    function
    | NamedRef (name, Abstract) ->
        let name = normalize_type_id name in
        Some
          Ctxt.(
            add "AutoBox" @@ return (Atom.Comp (Comp.AutoBox (Atom.LDef name))))
    | NamedRef (name, Concrete def) ->
        let unit_ctxt =
          match of_simplified def with
          | None -> Ctxt.pure ()
          | Some x -> Ctxt.fmap ignore x
        in
        let name = normalize_type_id name in
        Some
          Ctxt.(
            add_def
              ~id:name
              ~entry:def
              (fmerge (fun () x -> x) unit_ctxt (return (Atom.LDef name))))
    | Num (Int (NativeInt nat)) -> (
        match nat with
        | Uint8 -> Some (pure u8)
        | Int8 -> Some (pure i8)
        | Uint16 -> Some (pure u16)
        | Int16 -> Some (pure i16)
        | Uint30 -> Some (Ctxt.add "u30" (pure u30))
        | Int31 -> Some (Ctxt.add "i31" (pure i31))
        | Int32 -> Some (pure i32)
        | Int64 -> Some (pure i64))
    | Num (Int (RangedInt {repr; minimum; maximum})) ->
        let backer : Ast.Type.Prim.Int.range_backed =
          match repr with
          | `Uint8 -> `U8
          | `Uint16 -> `U16
          | `Uint30 -> `U32
          | `Int8 -> `I8
          | `Int16 -> `I16
          | `Int31 -> `I32
        in
        Some
          Ctxt.(
            add
              "RangedInt"
              (return
                 Ast.Type.Atom.(
                   Prim (Integral (RangedInt (backer, minimum, maximum))))))
    | Num (Float Double) -> Some (pure f64)
    | Num (Float (RangedDouble {minimum; maximum})) ->
        Some
          Ctxt.(
            add
              "RangedFloat"
              (return
                 Ast.Type.Atom.(
                   Prim (Floating (RangedFloat (minimum, maximum))))))
    | Bool -> Some (pure bool)
    | Unit -> Some (pure unit)
    | Fixed (Bytes n) -> Some (Ctxt.add "ByteString" (pure (bytestring n)))
    | Fixed (String n) -> Some (Ctxt.add "CharString" (pure (charstring n)))
    | ZarithN -> Some (Ctxt.add "N" (pure _N))
    | ZarithZ -> Some (Ctxt.add "Z" (pure _Z))
    | Var VBytes -> Some (Ctxt.add "Bytes" (pure (Atom (Prim VarBytes))))
    | Var VString -> Some (pure (Atom (Prim VarString)))

  and of_simplified : Simplified.t -> t Ctxt.t option =
    let of_atom : Ast.Type.Atom.t -> t = fun x -> Monic (Ast.Type.Atom x) in
    function
    | Simplified.Base x -> Ctxt.omap of_atom @@ of_base x
    | Simplified.Comp x -> Ctxt.omap of_atom @@ of_comp x
    | Simplified.Prod x -> of_prod x
    | Simplified.Sum x -> of_sum x

  and of_prod : Common.Simplified.prod_t -> t Ctxt.t option = function
    | Tuple {elems; _} ->
        let cata acc x =
          match (acc, x) with
          | Elems xs, Monic flat -> Elems (xs @ [flat])
          | _ -> assert false
        in
        List.fold_left
          (fun acc x ->
            Ctxt.(
              acc >>? fun acc' ->
              of_simplified x >>? fun x' -> Some (return @@ cata acc' x')))
          (Some (Ctxt.pure (Elems [])))
          elems
    | Record {fields; _} ->
        let cata fname acc x =
          match (acc, x) with
          | Fields xs, Monic field_type ->
              let field_name = normalize_field_id fname in
              Fields (xs @ Decl.[field_name <:> field_type])
          | Fields _xs, Elems _ ->
              invalid_arg
                "Unexpected fusion of (Elems _) into Field accumulator"
          | Fields _xs, Fields _ ->
              invalid_arg
                "Unexpected fusion of (Fields _) into Field accumulator"
          | Fields _xs, CStyle _ ->
              invalid_arg
                "Unexpected fusion of (CStyle _) into Field accumulator"
          | Fields _xs, Variants _ ->
              invalid_arg
                "Unexpected fusion of (Variants _) into Field accumulator"
          | _, _ ->
              invalid_arg "Incoherent state reached: non-Field accumulator"
        in
        List.fold_left
          (fun acc (fname, x) ->
            if x = Common.Simplified.Base Unit then acc
            else
              Ctxt.(
                acc >>? fun acc' ->
                of_simplified x >>? fun x' -> Some (return @@ cata fname acc' x')))
          (Some (Ctxt.pure (Fields [])))
          fields

  and repad :
      ?virt:bool -> int -> Common.Simplified.t -> Type.Atom.t Ctxt.t option =
   fun ?(virt = false) n -> function
    | Base x -> (
        match of_base x with
        | Some el ->
            Some
              Ctxt.(
                if virt then
                  add "VPadded" @@ el >$< fun x ->
                  Type.Atom.Comp (VPadded (x, n))
                else
                  add "Padded" @@ el >$< fun x -> Type.Atom.Comp (Padded (x, n)))
        | None -> assert false)
    | Comp x -> (
        match of_comp x with
        | Some el ->
            Some
              Ctxt.(
                if virt then
                  add "VPadded" @@ el >$< fun x ->
                  Type.Atom.Comp (VPadded (x, n))
                else
                  add "Padded" @@ el >$< fun x -> Type.Atom.Comp (Padded (x, n)))
        | None -> assert false)
    | other ->
        invalid_arg
          ("repad: encountered unexpected case: "
          ^ Common__Simplified.Verbose.(to_string @@ pp other))

  and of_comp : Common.Simplified.comp_t -> Type.Atom.t Ctxt.t option =
    let open Type.Comp in
    function
    | Padded (n, x) -> repad n x
    | VPadded (n, x) -> repad ~virt:true n x
    | Opt {has_tag; enc} -> (
        let f ctxt = if has_tag then ctxt else Ctxt.add "Nullable" ctxt in
        match enc with
        | Base x -> (
            match of_base x with
            | Some el ->
                Some
                  Ctxt.(f el >$< fun x -> Type.Atom.Comp (Option (has_tag, x)))
            | None -> assert false)
        | Comp x -> (
            match of_comp x with
            | Some el ->
                Some
                  Ctxt.(f el >$< fun x -> Type.Atom.Comp (Option (has_tag, x)))
            | None -> assert false)
        | _ -> assert false)
    | Seq {elt; lim; len_enc} -> (
        let f el =
          match (lim, len_enc) with
          | No_limit, None ->
              Ctxt.(
                add "Sequence" (el >$< fun x -> Type.Atom.(Comp (Sequence x))))
          | At_most n, None ->
              Ctxt.(
                add
                  "LimSeq"
                  (el >$< fun x -> Type.Atom.(Comp (LimSeq (x, n, None)))))
          | At_most n, Some lenc ->
              let lenc' =
                match lenc with
                | `Uint8 -> Ctxt.pure `U8
                | `Uint16 -> Ctxt.pure `U16
                | `Uint30 -> Ctxt.(add "u30" (pure `U30))
                | `N -> Ctxt.(add "N" (pure `N))
              in
              Ctxt.(
                add
                  "LimSeq"
                  (fmerge
                     (fun x y -> Type.Atom.(Comp (LimSeq (x, n, Some y))))
                     el
                     lenc'))
          | Exactly n, None ->
              Ctxt.(
                add "FixSeq" (el >$< fun x -> Type.Atom.(Comp (FixSeq (x, n)))))
          | (No_limit | Exactly _), Some _ -> assert false
        in
        match elt with
        | Base x -> (
            match of_base x with Some el -> Some (f el) | None -> assert false)
        | Comp x -> (
            match of_comp x with Some el -> Some (f el) | None -> assert false)
        | _ -> assert false)
    | Dyn (k, x) -> (
        let k' =
          match k with
          | `Uint8 -> Ctxt.pure `U8
          | `Uint16 -> Ctxt.pure `U16
          | `Uint30 -> Ctxt.(add "u30" (pure `U30))
          | `N -> Ctxt.(add "N" (pure `N))
        in
        match x with
        | Base x -> (
            match of_base x with
            | Some inner ->
                Some
                  (Ctxt.add
                     "Dynamic"
                     (Ctxt.fmerge
                        (fun k y -> Type.Atom.(Comp (Dynamic (k, y))))
                        k'
                        inner))
            | None -> assert false)
        | Comp x -> (
            match of_comp x with
            | Some inner ->
                Some
                  (Ctxt.add
                     "Dynamic"
                     (Ctxt.fmerge
                        (fun k y -> Type.Atom.Comp (Dynamic (k, y)))
                        k'
                        inner))
            | None -> assert false)
        | _ -> assert false)

  and of_sum : Common.Simplified.sum_t -> t Ctxt.t option = function
    | CStyle {size; enums; _} ->
        let sz' =
          match size with `Uint8 -> `U8 | `Uint16 -> `U16 | `Uint30 -> `U32
        in
        let elems' =
          List.map
            (fun (cname, tag) -> (normalize_constructor cname, tag))
            enums
          |> List.sort (fun (_, tag0) (_, tag1) -> Int.compare tag0 tag1)
        in
        Some (Ctxt.pure (CStyle (sz', elems')))
    | Data {size; vars; _} ->
        let lp' = match size with `Uint8 -> `U8 | `Uint16 -> `U16 in
        let cata vtag vname acc x =
          match acc with
          | Variants (lp, xs) ->
              let new_var = (vtag, (normalize_constructor vname, x)) in
              Variants (lp, new_var :: xs)
          | _ -> assert false
        in
        List.fold_left
          (fun acc (vtag, (vname, x)) ->
            Ctxt.(
              acc >>? fun acc' ->
              of_simplified x >>? fun x' ->
              Some (return @@ cata vtag vname acc' x')))
          (Some (Ctxt.pure (Variants (lp', []))))
          (List.sort (fun (tag0, _) (tag1, _) -> Int.compare tag1 tag0) vars)
end

module Snippets = struct
  let automatic_derive_traits =
    ["Debug"; "Estimable"; "Clone"; "PartialEq"; "PartialOrd"; "Hash"]

  module Encode = struct
    let method_name = "write_to"

    let add_depends : 'a. 'a Ctxt.t -> 'a Ctxt.t =
     fun x -> Ctxt.add "Target" @@ Ctxt.add "resolve_zero" @@ x

    let declare ~fname ~ty ~param ?(gp = "U") ?(tgt = "buf") calls =
      let body =
        [
          Stmt.Naked_return
            Expr.(sum (calls @ [(loc_ent "resolve_zero!").$([loc_ent tgt])]));
        ]
      in
      Decl.(
        DAnnot
          ( Annot.Other "allow(dead_code)",
            pub
              ((fn fname < (gp |: "Target")
               > [param #: Type.(!&ty); !&(mut tgt #: (Type.Generic_param gp))]
                 @-> Type.usize)
                 body) ))
  end

  module Decode = struct
    let method_name = "parse"

    let add_depends : 'a. 'a Ctxt.t -> 'a Ctxt.t =
     fun x -> Ctxt.add "Parser" @@ Ctxt.add "ParseResult" @@ x

    let declare (type a) ~fname ~ty ~src ?(gp = "P")
        ~(of_atom : a -> Ast.rust_val) atom =
      let body = [Stmt.Naked_return (of_atom atom)] in
      Decl.(
        DAnnot
          ( Annot.Other "allow(dead_code)",
            pub
              ((fn fname < (gp |: "Parser")
               > [!&(mut src #: (Type.Generic_param gp))]
                 @-> Type.verb
                 @@ Printf.sprintf "ParseResult<%s>" ty)
                 body) ))
  end
end

let gen_transcoder_call :
    ?kind:method_kind ->
    dir:transcoder_dir ->
    Simplified.t ->
    rust_val list ->
    rust_val Ctxt.t =
 fun ?(kind = Static) ~dir typ ->
  match kind with
  | Static -> (
      let method_name =
        match dir with
        | Encode -> Ctxt.(add "Encode" @@ pure Snippets.Encode.method_name)
        | Decode -> Ctxt.(add "Decode" @@ pure Snippets.Decode.method_name)
      in
      fun operands ->
        Ctxt.(
          method_name >>= fun meth ->
          match typ with
          | Base b -> (
              match TDef.of_base b with
              | None ->
                  assert false
                  (* We currently have no non-primitive base-types *)
              | Some x ->
                  let f pt =
                    let ns_str = Type.Atom.to_string ~expand:true pt in
                    Ctxt.pure Expr.((ent ns_str #:: meth).$(operands))
                  in
                  Ctxt.(x >>= f))
          | Comp c -> (
              match TDef.of_comp c with
              | None ->
                  assert false
                  (* We currently only support base-type composites *)
              | Some x ->
                  let f ct =
                    let ns_str = Type.Atom.to_string ~expand:true ct in
                    Ctxt.pure Expr.((ent ns_str #:: meth).$(operands))
                  in
                  Ctxt.(x >>= f))
          | Prod _ -> assert false
          | Sum _ -> assert false))
  | Dynamic -> (
      (* FIXME: this is deprecated behvaior even if we aren't using it in any
         codepath (decode is a trait on the target type, at least for now) *)
      let method_name =
        match dir with
        | Encode -> Ctxt.(add "Encode" @@ pure Snippets.Encode.method_name)
        | Decode -> Ctxt.(add "Decode" @@ pure Snippets.Decode.method_name)
      in
      function
      | value :: rest -> (
          match typ with
          | Base _ | Comp _ | Prod _ | Sum _ ->
              Ctxt.(method_name >$< fun meth -> Expr.((value $. meth).$(rest))))
      | [] -> assert false)

module Gen = struct
  (** [mk_fname]:

      Outputs an identifier to use for the standalone function that either
      encodes, or decodes, the root type of the codec being generated. The
      direction is a labeled parameter [~dir].

      Takes a labeled argument [~dir] and the string value used as the base name
      associated with the schema/codec.

      This function may be implemented variously, but any such implementation
      will be deterministic, as multiple calls are made during generation and a
      consistent answer must be given; furthermore, per-run consisntency is not
      enough, as multiple compilations of a schema with the same input metadata
      should ideally produce identical output. This helps ensure that
      double-compilation does not cause content differences to emerge in codec
      files. *)
  let mk_fname : dir:transcoder_dir -> string -> string =
   fun ~dir tname ->
    let tname = to_snakecase tname in
    match dir with Encode -> tname ^ "_write" | Decode -> tname ^ "_parse"

  (** [mk_tname]:

      Outputs an identifier to use for the type definition/declaration of the
      root codec type. This function exists mostly as an abstraction around the
      language-specific conventions and restrictions on type-scope identifiers
      in various languages. As this generator happens to be specialized for
      Rust, the implementation attempts to convert snake_case to PascalCase, but
      otherwise does little else.

      Like [[mk_fname]], this function is given a guarantee of deterministic
      behavior. *)
  let mk_tname = normalize_type_id

  module Conf = struct
    module TyDef = struct
      type u = ForceAlias | SuppressAlias

      type v = PublishRoot | PublishAll

      type t = u option * v
    end

    module Impl = struct
      type t = ForceDerive | SuppressDerive
    end

    type tydef = TyDef.t

    type impl = Impl.t
  end

  (** [is_alias]: returns the aliasing policy for a given simplified schema

      Called with an optional argument [?force] of type [[Conf.tydef]].

      When aliasing is not possible, will always return [false].

      When aliasing is possible, returns [true] when [force = Some (ForceAlias)]
      and [false] when [force = Some (SuppressAlias)]. When no override is
      provided ([force = None]), follows an unspecified but consistent internal
      policy. *)
  let is_alias ?(force : Conf.TyDef.u option) : Simplified.t -> bool = function
    | Base _ | Comp _ -> (
        match force with
        | None -> true
        | Some ForceAlias -> true
        | Some SuppressAlias -> false)
    | Prod (Tuple _) -> (
        match force with
        | None -> false
        | Some ForceAlias -> true
        | Some SuppressAlias -> false)
    | Prod (Record _) -> false
    | Sum _ -> false

  (** Algebraic type representing how a mandatory trait is to be provided for
      the schema type*)
  type trait_source =
    | ImplExists
        (** An implementation is already present in runtime or macro-generated,
            and cannot therefore be derived or injected

            May also indicate that there is no impl, but one cannot be injected
            or derived *)
    | CanDerive
        (** Derivation is possible and preferred according to compiler
            configuration *)
    | MustInject
        (** Derivation is not possible, or is suppressed by compiler
            configuration; must inject an impl block *)

  let decode_source ?(force : Conf.impl option) : Simplified.t -> trait_source =
    function
    | Prod _ -> (
        match force with
        | None | Some ForceDerive -> CanDerive
        | Some SuppressDerive -> MustInject)
    | Base _ | Comp _ | Sum _ -> ImplExists

  let encode_source ?(force : Conf.impl option) : Simplified.t -> trait_source =
    function
    | Prod _ -> (
        match force with
        | None | Some ForceDerive -> CanDerive
        | Some SuppressDerive -> MustInject)
    | Base _ | Comp _ | Sum _ -> ImplExists

  let mk_typedef :
      ?tyconf:Conf.tydef ->
      ?force_derive:Conf.impl ->
      ?is_root:bool ->
      string ->
      Simplified.t ->
      rust_decl Ctxt.t =
   fun ?tyconf ?force_derive ?(is_root = true) schema_name ->
    let is_alias = is_alias ?force:(Option.bind tyconf fst) in
    let name = mk_tname schema_name in
    fun rep ->
      let rhs' = TDef.of_simplified rep |> Option.get in
      let derives =
        Ctxt.(
          fmerge
            List.append
            (add "deriveEstimable" @@ pure Snippets.automatic_derive_traits)
          @@ fmerge
               List.append
               (match encode_source ?force:force_derive rep with
               | CanDerive -> add "deriveEncode" @@ pure ["Encode"]
               | _ -> pure [])
               (match decode_source ?force:force_derive rep with
               | CanDerive -> add "deriveDecode" @@ pure ["Decode"]
               | _ -> pure []))
      in
      Ctxt.(
        rhs' >>= function
        | Monic x ->
            Decl.(
              pub
              <$>
              if is_alias rep then pure (name := x)
              else
                derives >$< fun ds ->
                DAnnot
                  (Annot.Derive ds, name.=([{arg_type = x; arg_vis = Some Pub}])))
        | Elems xs ->
            let rhs =
              let xs' =
                List.map
                  (fun x ->
                    {Ast.Decl.Type_def.Struct.arg_vis = None; arg_type = x})
                  xs
              in
              let publish = List.map Ast.Decl.Type_def.Struct.pub_arg in
              match tyconf with
              | None -> xs'
              | Some (_, PublishRoot) -> if is_root then publish xs' else xs'
              | Some (_, PublishAll) -> publish xs'
            in
            Decl.(
              pub
              <$>
              if is_alias rep then pure (name := Ast.Type._Tuple xs)
              else derives >$< fun ds -> DAnnot (Annot.Derive ds, name.=(rhs)))
        | Fields fs ->
            let fs' =
              let publish = List.map Ast.Decl.Type_def.Struct.pub_field in
              match tyconf with
              | None -> fs
              | Some (_, PublishRoot) -> if is_root then publish fs else fs
              | Some (_, PublishAll) -> publish fs
            in
            Decl.(
              pub
              <$> (derives >$< fun ds -> DAnnot (Annot.Derive ds, name.={fs'})))
        | CStyle (sz, elems) ->
            Decl.(
              add "deriveEncode" @@ add "deriveDecode" @@ add "deriveEstimable"
              @@ add "cstyle"
              @@ pure
                   (DTypeDef
                      ( None,
                        ( name,
                          Ast.Decl.Type_def.(
                            Enum
                              (UnC (sz, List.map (fun (s, i) -> s <=> i) elems)))
                        ) )))
        | Variants (sz, vars) ->
            let to_struct :
                ?constr:string -> TDef.t -> Ast.Decl.Type_def.Struct.t =
             fun ?constr ->
              let trace =
                match constr with
                | None -> "@" ^ schema_name
                | Some c -> "@" ^ schema_name ^ ":" ^ c
              in
              let open Ast.Decl.Type_def.Struct in
              let mk_arg arg_type = {arg_type; arg_vis = Some Pub} in
              function
              | Elems [] | Fields [] | Monic (Atom (Prim Unit)) -> Unit
              | Monic x -> Tuple [mk_arg x]
              | Elems xs -> Tuple (List.map mk_arg xs)
              | Fields xs -> Record xs
              | Variants _ | CStyle _ ->
                  invalid_arg
                    ("Unexpected sum-type encountered in to_struct" ^ trace)
            in
            Decl.(
              add "deriveEncode" @@ add "deriveDecode" @@ add "deriveEstimable"
              @@ add "data"
              @@ pure
                   (DTypeDef
                      ( None,
                        ( name,
                          Ast.Decl.Type_def.(
                            Enum
                              (UnData
                                 ( sz,
                                   normalize_submodule_id name,
                                   List.map
                                     (fun (i, (s, v)) ->
                                       Variant.Data (i, s, to_struct ~constr:s v))
                                     vars ))) ) ))))

  let mk_encoder :
      ?tyconf:Conf.tydef -> string -> Simplified.t -> rust_decl Ctxt.t =
    let open Expr in
    let dir = Encode and param = "val" in
    fun ?tyconf schema_name ->
      let is_alias = is_alias ?force:(Option.bind tyconf fst) in
      let fname = mk_fname ~dir schema_name
      and ty = Type.(verb (mk_tname schema_name))
      and tgt = "buf" in
      function
      | (Base _ | Comp _) as rep ->
          let atom =
            if is_alias rep then loc_ent param else !&(loc_ent param $. "0")
          in
          let ret = gen_transcoder_call ~dir rep @@ [atom; loc_ent tgt] in
          Ctxt.(
            Snippets.Encode.add_depends ret >$< fun expr ->
            Snippets.Encode.declare ~fname ~ty ~param ~tgt [expr])
      | Prod (Tuple {elems = ts; _}) ->
          let rets =
            Ctxt.seq
            @@ List.map (fun (atom, rep) ->
                   gen_transcoder_call ~dir rep @@ [atom; loc_ent "buf"])
            @@
            let atom = loc_ent param in
            List.mapi (fun i x -> (!&(atom $. Int.to_string i), x)) ts
          in
          Ctxt.(
            Snippets.Encode.add_depends rets
            >$< Snippets.Encode.declare ~fname ~ty ~param)
      | Prod (Record {fields; _}) ->
          let rets =
            Ctxt.seq
            @@ List.map (fun (atom, rep) ->
                   gen_transcoder_call ~dir rep @@ [atom; loc_ent "buf"])
            @@
            let atom = loc_ent param in
            List.map
              (fun (fld, x) -> (!&(atom $. normalize_field_id fld), x))
              (fields |> elim_unit_field)
          in
          Ctxt.(
            Snippets.Encode.add_depends rets >$< fun exprs ->
            Snippets.Encode.declare ~fname ~ty ~param exprs)
      | Sum _ as rep ->
          let ret =
            gen_transcoder_call ~kind:Dynamic ~dir rep
            @@ [loc_ent param; loc_ent "buf"]
          in
          Ctxt.(
            Snippets.Encode.add_depends ret >$< fun expr ->
            Snippets.Encode.declare ~fname ~ty ~param ~tgt [expr])

  let mk_decoder :
      ?tyconf:Conf.tydef -> string -> Simplified.t -> rust_decl Ctxt.t =
    let dir = Decode in
    fun ?tyconf schema_name ->
      let is_alias = is_alias ?force:(Option.bind tyconf fst) in
      let fname = mk_fname ~dir schema_name in
      let ty = mk_tname schema_name in
      let src = "p" in
      function
      | (Base _ | Comp _) as rep ->
          let of_atom expr =
            let expr' =
              if is_alias rep then expr else Expr.((loc_ent @@ ty).$([expr]))
            in
            Expr.((loc_ent "Ok").$([expr' >? ()]))
          in
          let elt = gen_transcoder_call ~dir rep [Expr.loc_ent src] in
          Snippets.Decode.(
            Ctxt.(declare ~fname ~ty ~src ~of_atom <$> add_depends elt))
      | Prod (Tuple {elems = ts; _}) as rep ->
          let of_atom elts =
            let expr' =
              let elts' = List.map (fun x -> Expr.(x >? ())) elts in
              if is_alias rep then Expr.tup elts'
              else Expr.((loc_ent @@ mk_tname schema_name).$(elts'))
            in
            Expr.((loc_ent "Ok").$([expr']))
          in
          let elems =
            Ctxt.seq
            @@ List.map
                 (fun elt -> gen_transcoder_call ~dir elt [Expr.loc_ent src])
                 ts
          in
          Snippets.Decode.(
            Ctxt.(declare ~fname ~ty ~src ~of_atom <$> add_depends elems))
      | Prod (Record {fields; _}) ->
          let fs =
            let fields' = List.map (fun (x, y) -> let x' = normalize_field_id x in (x', y)) fields in
            fields' |> elim_unit_field
          in
          let of_atom elts =
            let expr' =
              let elts' =
                List.map (sem (fun x -> Expr.(Some (x >? ())))) elts
              in
              Expr.((mk_tname schema_name).${elts'})
            in
            Expr.((loc_ent "Ok").$([expr']))
          in
          let elems =
            Ctxt.(
              sseq
              @@ List.map
                   (sem (fun elt ->
                        gen_transcoder_call ~dir elt [Expr.loc_ent src]))
                   fs)
          in
          Snippets.Decode.(
            Ctxt.(declare ~fname ~ty ~src ~of_atom <$> add_depends elems))
      | Sum _ ->
          let of_atom () =
            Expr.(
              (ent ty #:: Snippets.Decode.method_name).$([Expr.loc_ent src]))
          in
          Snippets.Decode.(
            Ctxt.(declare ~fname ~ty ~src ~of_atom <$> add_depends (pure ())))

  let mk_impl_encode : string -> Simplified.t -> rust_decl Ctxt.t =
    let dir = Encode in
    fun schema_name -> function
      | Base _ | Prod _ | Comp _ ->
          let my_write = Expr.(loc_ent (mk_fname ~dir schema_name)) in
          Ctxt.(
            add "Encode" @@ add "Target"
            @@ return
                 (Decl.DTraitImpl
                    {
                      trait_name = "Encode";
                      type_name = mk_tname schema_name;
                      methods =
                        [
                          Decl.(
                            (fn "write_to" < ("U" |: "Target")
                            > [
                                !&self; !&(mut "buf" #: (Type.Generic_param "U"));
                              ]
                              @-| ())
                              [
                                Naked_return
                                  Expr.(my_write.$([self; loc_ent "buf"]));
                              ]);
                        ];
                    }))
      | Sum _ ->
          invalid_arg
            "Cannot manually add redundant 'impl Encode' block to \
             macro-generated Sum-type definition!"

  let mk_impl_decode : string -> Simplified.t -> rust_decl Ctxt.t =
    let dir = Decode in
    fun schema_name ->
      let my_type = Type.verb @@ Printf.sprintf "ParseResult<%s>" schema_name in
      function
      | Base _ | Prod _ | Comp _ ->
          let my_decode = Expr.(loc_ent (mk_fname ~dir schema_name)) in
          Ctxt.(
            add "Parser" @@ add "Decode"
            @@ return
                 (Decl.DTraitImpl
                    {
                      trait_name = "Decode";
                      type_name = mk_tname schema_name;
                      methods =
                        [
                          Decl.(
                            (fn "parse" < ("P" |: "Parser")
                            > [!&(mut "p" #: Type.(Generic_param "P"))]
                              @-> my_type)
                              [Naked_return Expr.(my_decode.$([loc_ent "p"]))]);
                        ];
                    }))
      | Sum _ ->
          invalid_arg
            "Cannot manually add redundant 'impl Decode' block to \
             macro-generated Sum-type definition!"

  let mk_testsuite : string -> images:string list -> rust_decl Ctxt.t =
   fun name ~images ->
    let rt_function : rust_decl =
      let param = "hex" in
      let my_decode = Expr.(ent (mk_tname name) #:: "decode") in
      let assert_eq = Expr.loc_ent "assert_eq!" in
      Decl.(
        (fn "roundtrip" <> [param #: Type._str] @-| ())
          [
            Bare_expression
              Expr.(
                assert_eq.$([
                              loc_ent param;
                              (my_decode.$([(loc_ent "hex!").$([loc_ent param])])
                              $. "encode::<HexString>").$([]);
                            ]));
          ])
    in
    let mk_roundtrip : string -> rust_stmt =
     fun image ->
      Bare_expression Expr.((loc_ent "roundtrip").$([String_lit image]))
    in
    Ctxt.(
      return
        Decl.(
          DTestsModule
            {
              contents =
                [
                  rt_function;
                  testfn "images_roundtrip" @@ List.map mk_roundtrip images;
                ];
            }))

  let mk_payload :
      ?tyconf:Conf.tydef ->
      ?force_derive:Conf.impl ->
      ?images:string list ->
      string ->
      Simplified.t ->
      rust_decl list Ctxt.t =
   fun ?tyconf ?force_derive ?images name rep ->
    match images with
    | None ->
        Ctxt.seq
          ([
             mk_typedef ?tyconf ?force_derive name rep;
             mk_encoder ?tyconf name rep;
             mk_decoder ?tyconf name rep;
           ]
          @
          if is_alias rep then []
          else
            (match encode_source ?force:force_derive rep with
            | MustInject -> [mk_impl_encode name rep]
            | CanDerive | ImplExists -> [])
            @
            match decode_source ?force:force_derive rep with
            | MustInject -> [mk_impl_decode name rep]
            | CanDerive | ImplExists -> [])
    | Some images ->
        Ctxt.seq
          ([
             mk_typedef ?tyconf name rep;
             mk_encoder ?tyconf name rep;
             mk_decoder ?tyconf name rep;
           ]
          @
          if is_alias rep then [mk_testsuite name ~images]
          else
            (match encode_source ?force:force_derive rep with
            | MustInject -> [mk_impl_encode name rep]
            | CanDerive | ImplExists -> [])
            @ (match decode_source ?force:force_derive rep with
              | MustInject -> [mk_impl_decode name rep]
              | CanDerive | ImplExists -> [])
            @ [mk_testsuite name ~images])

  let generate :
      ?tyconf:Conf.tydef ->
      ?force_derive:Conf.impl ->
      ?images:string list ->
      string ->
      Simplified.t ->
      rust_module =
   fun ?tyconf ?force_derive ?images name rep ->
    let name = normalize_type_id name in
    let ((_, st) as payload) =
      mk_payload ?tyconf ?force_derive ?images name rep
    in
    let prereqs =
      Ctxt.seq
      @@ List.map (fun (id, def) ->
             mk_typedef ?tyconf ?force_derive ~is_root:false id def)
      @@ Ctxt.State.defs st
    in
    let fuse xs ys =
      if Ctxt.State.has_def name st then xs @ List.tl ys else xs @ ys
    in
    let decls, st' = Ctxt.fmerge fuse prereqs payload in
    {preamble = Preamble.of_rt_imports (Ctxt.State.items st'); decls}
end
