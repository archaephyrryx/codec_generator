module Lex = struct
  module Overloads = struct
    type kind_ext = [`Block]

    let preclassify : string -> kind_ext option = function
      | "parameter" | "storage" | "code" -> Some `Block
      | _ -> None
  end

  type kind = [Overloads.kind_ext | `Instruction | `Type | `Constructor]

  exception Classified of kind

  let classify : string -> kind =
   fun tok ->
    match Overloads.preclassify tok with
    | Some k -> (k :> kind)
    | None -> (
        let chrs = String.to_seq tok in
        try
          Seq.fold_left
            (fun acc chr ->
              let lower = Char.lowercase_ascii chr
              and upper = Char.uppercase_ascii chr in
              match (Char.equal chr lower, Char.equal chr upper) with
              | true, true -> acc
              | true, false -> raise (Classified acc)
              | false, true -> (
                  match acc with
                  | `Type -> `Constructor
                  | `Constructor -> raise (Classified `Instruction)
                  | `Instruction -> assert false
                  | #Overloads.kind_ext -> assert false)
              | false, false -> assert false)
            `Type
            chrs
        with Classified x -> x)
end

let addstr = Common.Printer.str

let addchr = Common.Printer.chr

let brace : _ Common.Printer.fragment -> _ Common.Printer.fragment =
 fun inner ->
  let open Common.Printer in
  addstr "{" |>> wrap (addchr '\n' |>> inner) |>> addchr '\n' |>> addstr "}"

let bracket : _ Common.Printer.fragment -> _ Common.Printer.fragment =
 fun inner ->
  let open Common.Printer in
  addstr "[" |>> wrap (addchr '\n' |>> inner) |>> addchr '\n' |>> addstr "]"

let rematch : string list -> _ Common.Printer.fragment =
 fun toks ->
  Common.Printer.(
    addchr '"' |>> addchr '\\' |>> addchr '\\' |>> addchr 'b' |>> addchr '('
    |>> seq ~sep:(addchr '|') addstr toks
    |>> addchr ')' |>> addchr '\\' |>> addchr '\\' |>> addchr 'b' |>> addchr '"')

let qstring : string -> _ Common.Printer.fragment =
 fun x -> Common.Printer.(addchr '"' |>> addstr x |>> addchr '"')

let cut = Common.Printer.(addchr ',' |>> addchr '\n')

let keyvalue key value = Common.Printer.(qstring key |>> addstr ": " |>> value)

module Ast = struct
  type t =
    | Str of string
    | Rgx of string list
    | Arr of t list
    | Obj of (string * t) list

  let rec pp : t -> _ Common.Printer.fragment = function
    | Str s -> qstring s
    | Rgx pats -> rematch pats
    | Arr xs -> bracket (Common.Printer.seq ~sep:cut pp xs)
    | Obj kvs ->
        brace
          (Common.Printer.seq ~sep:cut (fun (x, y) -> keyvalue x (pp y)) kvs)
end

module Generator = struct
  type u = string list

  type t = {blocks : u; instructions : u; types : u; constructors : u}

  let convert : Common.Simplified.t -> t = function
    | Sum (CStyle {enums; _}) ->
        let empty : t =
          {blocks = []; instructions = []; types = []; constructors = []}
        in
        let cata : t -> string -> t =
         fun acc x ->
          match Lex.classify x with
          | `Block -> {acc with blocks = x :: acc.blocks}
          | `Instruction -> {acc with instructions = x :: acc.instructions}
          | `Type -> {acc with types = x :: acc.types}
          | `Constructor -> {acc with constructors = x :: acc.constructors}
        in
        List.fold_left (fun acc (x, _) -> cata acc x) empty enums
    | _ -> invalid_arg "convert"

  let generate : t -> Ast.t =
   fun {blocks; instructions; types; constructors} ->
    Ast.(
      Obj
        [
          ("scopeName", Str "source.michelson");
          ("name", Str "Michelson");
          ( "patterns",
            Arr
              [
                Obj [("include", Str "#bytes")];
                Obj [("include", Str "#string")];
                Obj [("include", Str "#number")];
                Obj [("include", Str "#comment")];
                Obj [("include", Str "#multicomment")];
                Obj [("include", Str "#block")];
                Obj [("include", Str "#data")];
                Obj [("include", Str "#instruction")];
                Obj [("include", Str "#type")];
                Obj [("include", Str "#macros")];
                Obj [("include", Str "#annotations")];
              ] );
          ( "repository",
            Obj
              [
                ( "string",
                  Obj
                    [
                      ("name", Str "string.quoted.michelson");
                      ("begin", Str "\\\"");
                      ("end", Str "\\\"");
                      ( "patterns",
                        Arr
                          [
                            Obj
                              [
                                ("name", Str "string.quoted.michelson");
                                ("match", Str "\\\\\\\\.");
                              ];
                          ] );
                    ] );
                ( "number",
                  Obj
                    [
                      ( "patterns",
                        Arr
                          [
                            Obj
                              [
                                ("name", Str "constant.numeric.michelson");
                                ("match", Rgx ["-?[0-9]+"]);
                              ];
                          ] );
                    ] );
                ( "bytes",
                  Obj
                    [
                      ( "patterns",
                        Arr
                          [
                            Obj
                              [
                                ("name", Str "constant.numeric.michelson");
                                ("match", Rgx ["0x[0-9A-Fa-f]+"]);
                              ];
                          ] );
                    ] );
                ( "comment",
                  Obj
                    [
                      ("name", Str "comment.line.number-sign.michelson");
                      ("begin", Str "#");
                      ("end", Str "\\n");
                      ( "patterns",
                        Arr
                          [
                            Obj
                              [
                                ( "name",
                                  Str "constant.character.escape.michelson" );
                                ("match", Str "wordPattern");
                              ];
                          ] );
                    ] );
                ( "multicomment",
                  Obj
                    [
                      ("name", Str "comment.block.michelson");
                      ("begin", Str "\\\\/\\\\*");
                      ("end", Str "\\\\*\\\\/");
                      ( "patterns",
                        Arr
                          [
                            Obj
                              [
                                ( "name",
                                  Str "constant.character.escape.michelson" );
                                ("match", Str "wordPattern");
                              ];
                          ] );
                    ] );
                ( "block",
                  Obj
                    [
                      ( "patterns",
                        Arr
                          [
                            Obj
                              [
                                ("match", Rgx blocks);
                                ("name", Str "keyword.control.michelson");
                              ];
                          ] );
                    ] );
                ( "data",
                  Obj
                    [
                      ( "patterns",
                        Arr
                          [
                            Obj
                              [
                                ("match", Rgx constructors);
                                ( "name",
                                  Str "variable.other.enummember.michelson" );
                              ];
                          ] );
                    ] );
                ( "instruction",
                  Obj
                    [
                      ( "patterns",
                        Arr
                          [
                            Obj
                              [
                                ("match", Rgx instructions);
                                ("name", Str "support.function.michelson");
                              ];
                          ] );
                    ] );
                ( "type",
                  Obj
                    [
                      ( "patterns",
                        Arr
                          [
                            Obj
                              [
                                ("match", Rgx types);
                                ( "name",
                                  Str
                                    "entity.name.type.michelson \
                                     support.type.michelson" );
                              ];
                          ] );
                    ] );
                ( "annotations",
                  Obj [
                    "patterns", Arr [
                      Obj [
                        "match", Str "(?<=\\\\s)%[A-z_0-9%@]*";
                        "name", Str "entity.other.attribute-name.michelson"
                      ];

                      Obj [
                        "match", Str "(?<=\\\\s)@[A-z_0-9%]+\\\\b";
                        "name", Str "entity.other.attribute-name.michelson"
                      ];

                      Obj [
                        "match", Str "(?<=\\\\s):[A-z_0-9]+\\\\b";
                        "name", Str "entity.other.attribute-name.michelson"
                      ];
                    ]
                  ]

                );
              ] );
        ])

  let transform : Common.Simplified.t -> _ Common.Printer.fragment =
   fun x -> convert x |> generate |> Ast.pp
end
