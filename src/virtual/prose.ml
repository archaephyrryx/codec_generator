module Ctxt = struct
  module Refs = struct
    include Hashtbl.Make (struct
      include String

      let hash = Hashtbl.hash
    end)
  end

  type t = int ref * int Refs.t

  let (ctr, tbl) : t = (ref 0, Refs.create 0)

  let clear () =
    ctr := 0 ;
    Refs.clear tbl

  let cite :
        'a.
        string ->
        'a ->
        define:(string -> int -> 'a -> 'b) ->
        mention:(string -> int -> 'a -> 'b) ->
        'b =
   fun id x ~define ~mention ->
    match Refs.find_opt tbl id with
    | Some ix -> mention id ix x
    | None ->
        let ix = !ctr in
        Refs.add tbl id ix ;
        ctr := ix + 1 ;
        define id ix x
end

module Description = struct
  type 'a block = 'a Common.Printer.fragment

  type 'a post = 'a block Seq.t

  type 'a t = 'a block * 'a post

  let join : 'a t -> 'a t -> 'a t =
   fun (xa, xb) (ya, yb) ->
    let za = Common.Printer.(xa |>> ya) and zb = Seq.append xb yb in
    (za, zb)
end

module Conf = struct
  type t = {max_length : int option}

  let make ?max_length () = {max_length}
end

let shorten : ?max_length:int -> string -> string =
 fun ?(max_length = 32) x ->
  let l = String.length x in
  if l <= max_length then x
  else
    let l' = max_length / 4 in
    let lrun = String.sub x 0 l' and rrun = String.sub x (l - l') l' in
    lrun ^ ".." ^ rrun

let sanitize_verbatim ?max_length ?refid name =
  match max_length with
  | None -> name
  | Some max_length ->
      if String.length name > max_length then
        match refid with
        | Some refid -> shorten ~max_length name ^ Printf.sprintf "[@%d]" refid
        | None -> shorten ~max_length name
      else name

let addstr = Common.Printer.str

let addchr = Common.Printer.chr

module Ast = struct
  module Type = struct
    type num_t = Common.Simplified.num_t

    type t =
      | Unit
      | Bool
      | Num of num_t
      | N
      | Z
      | String of int option
      | Bytes of int option
      | Tuple of t list
      | Record of (string * t) list
      | Seq of (t * Common.Simplified.limit * Common.Simplified.lenpref option)
      | Pad of int * t
      | Opt of bool * t
      | Dyn of Common.Simplified.lenpref * t
      | Enum0 of Common.Simplified.enum_size * (string * int) list
      | EnumPlus of Common.Simplified.tag_size * (int * (string * t)) list
      | SubDef of string * t
      | ExtRef of string

    let rec pp : conf:Conf.t -> t -> Common.Printer.monoidal Description.block =
      let open Common.Printer in
      fun ~conf -> function
        | Unit -> addstr "zero-width value (null or unit)"
        | Bool -> addstr "boolean value"
        | Num (Int (NativeInt Uint8)) -> addstr "8-bit unsigned integer"
        | Num (Int (NativeInt Uint16)) -> addstr "16-bit unsigned integer"
        | Num (Int (NativeInt Uint30)) -> addstr "30-bit unsigned integer"
        | Num (Int (NativeInt Int8)) -> addstr "8-bit signed integer"
        | Num (Int (NativeInt Int16)) -> addstr "16-bit signed integer"
        | Num (Int (NativeInt Int31)) -> addstr "31-bit signed integer"
        | Num (Int (NativeInt Int32)) -> addstr "32-bit signed integer"
        | Num (Int (NativeInt Int64)) -> addstr "64-bit signed integer"
        | Num (Int (RangedInt {repr; minimum; maximum})) ->
            (match repr with
            | `Uint8 -> addstr "8-bit unsigned integer"
            | `Uint16 -> addstr "16-bit unsigned integer"
            | `Uint30 -> addstr "30-bit unsigned integer"
            | `Int8 -> addstr "8-bit signed integer"
            | `Int16 -> addstr "16-bit signed integer"
            | `Int31 -> addstr "31-bit signed integer")
            |>> addchr ' '
            |>> addstr (Printf.sprintf "between %d and %d" minimum maximum)
        | Num (Float Double) -> addstr "IEEE-754 double-precision float"
        | Num (Float (RangedDouble {minimum; maximum})) ->
            addstr "IEEE-754 double-precision float"
            |>> addchr ' '
            |>> addstr (Printf.sprintf "between %f and %f" minimum maximum)
        | N -> addstr "arbitrary-precision natural (non-negative) integer"
        | Z -> addstr "arbitrary-precision integer"
        | String l' ->
            addstr "character string"
            |>> opt
                  (fun l -> addstr (Printf.sprintf " (fixed length: %d)" l))
                  l'
        | Bytes l' ->
            addstr "byte sequence"
            |>> opt
                  (fun l -> addstr (Printf.sprintf " (fixed length: %d)" l))
                  l'
        | Tuple xs ->
            let l = List.length xs in
            let ixs =
              let pair x y = (x, y) in
              List.mapi pair xs
            in
            addstr (Printf.sprintf "%d-tuple : " l)
            |>> wrap
                  (addchr '\n'
                  |>> seq
                        ~sep:(addchr '\n')
                        (fun (ix, t) ->
                          addstr (string_of_int ix)
                          |>> addstr ": "
                          |>> wrap (pp ~conf t))
                        ixs)
        | Record fs ->
            addstr "Record : "
            |>> wrap
                  (addchr '\n'
                  |>> seq
                        ~sep:(addchr '\n')
                        (fun (fn, ft) ->
                          addchr '`' |>> addstr fn |>> addstr "`: "
                          |>> wrap (pp ~conf ft))
                        fs)
        | Seq (elt, lim, lenc) ->
            (match lim with
            | No_limit -> Common.Printer.ign
            | At_most n ->
                let lenc' =
                  Option.map
                    (function
                      | `Uint8 -> "8-bit unsigned integer"
                      | `Uint16 -> "16-bit unsigned integer"
                      | `Uint30 -> "30-bit unsigned integer"
                      | `N -> "dynamic[N] (30-bit precision)")
                    lenc
                in
                opt
                  (fun x ->
                    addstr "Cardinality-prefixed (" |>> addstr x |>> addstr ") ")
                  lenc'
                |>> addstr "bounded (len <= "
                |>> addstr (string_of_int n)
                |>> addstr ") "
            | Exactly n ->
                addstr "exact (len == "
                |>> addstr (string_of_int n)
                |>> addstr ") ")
            |>> addstr "sequence of: "
            |>> wrap (pp ~conf elt)
        | Opt (true, elt) ->
            addstr "[tagged] nullable of: " |>> wrap (pp ~conf elt)
        | Opt (false, elt) ->
            addstr "[untagged] nullable of: " |>> wrap (pp ~conf elt)
        | Pad (n, elt) ->
            addstr (Printf.sprintf "Padded (+%d bytes): " n)
            |>> wrap (pp ~conf elt)
        | Dyn (lp, elt) ->
            let wc =
              let open Either in
              match lp with
              | `Uint8 -> Left 1
              | `Uint16 -> Left 2
              | `Uint30 -> Left 4
              | `N -> Right "dynamic[N]"
            in
            addstr
              (Either.fold
                 ~left:
                   (Printf.sprintf "length-prefixed (prefix width: %d bytes): ")
                 ~right:(Printf.sprintf "length-prefixed (prefix width: %s): ")
                 wc)
            |>> wrap (pp ~conf elt)
        | Enum0 (sz, elts) ->
            let elts =
              List.sort
                (fun (_, disc0) (_, disc1) -> Int.compare disc0 disc1)
                elts
            in
            let wc =
              match sz with `Uint8 -> 1 | `Uint16 -> 2 | `Uint30 -> 4
            in
            addstr (Printf.sprintf "Simple enum with %d-word tag:" wc)
            |>> wrap
                  (addchr '\n'
                  |>> seq
                        ~sep:(addchr '\n')
                        (fun (constr, discr) ->
                          addstr constr
                          |>> addstr (Printf.sprintf " (:= %d)" discr))
                        elts)
        | EnumPlus (sz, vars) ->
            let vars =
              List.sort
                (fun (disc0, _) (disc1, _) -> Int.compare disc0 disc1)
                vars
            in
            let wc = match sz with `Uint8 -> 1 | `Uint16 -> 2 in
            addstr (Printf.sprintf "ADT with %d-word tag:" wc)
            |>> wrap
                  (addchr '\n'
                  |>> seq
                        ~sep:(addchr '\n')
                        (fun (discr, (constr, payload)) ->
                          addstr
                            (sanitize_verbatim
                               ?max_length:conf.max_length
                               constr)
                          |>> addstr (Printf.sprintf " (:= %d)" discr)
                          |>> addstr " ~ "
                          |>> wrap (pp ~conf payload))
                        vars)
        | ExtRef ident ->
            addstr (sanitize_verbatim ?max_length:conf.max_length ident)
        | SubDef (ident, def) ->
            let mention id refid _ =
              wrap
                (addchr '{' |>> addstr "inlined: "
                |>> addstr
                      (sanitize_verbatim ?max_length:conf.max_length ~refid id)
                |>> addstr " (suppressing duplicate definition)}")
            and define id refid def' =
              wrap
                (addchr '{' |>> addstr "inlined: "
                |>> addstr
                      (sanitize_verbatim ?max_length:conf.max_length ~refid id)
                |>> addstr " := "
                |>> wrap (pp ~conf def')
                |>> addchr '}')
            in
            Ctxt.cite ident def ~mention ~define
  end
end

module Generator = struct
  let rec reduce : Common.Simplified.t -> Ast.Type.t =
    let open Ast.Type in
    function
    | Base Unit -> Unit
    | Base Bool -> Bool
    | Base (Num n) -> Num n
    | Base ZarithN -> N
    | Base ZarithZ -> Z
    | Base (Fixed (String l)) -> String (Some l)
    | Base (Fixed (Bytes l)) -> Bytes (Some l)
    | Base (Var VString) -> String None
    | Base (Var VBytes) -> Bytes None
    | Base (NamedRef (id, Concrete def)) -> SubDef (id, reduce def)
    | Base (NamedRef (id, Abstract)) -> ExtRef id
    | Prod (Tuple {elems; _}) -> Tuple (List.map reduce elems)
    | Prod (Record {fields; _}) ->
        Record (List.map (fun (fid, ft) -> (fid, reduce ft)) fields)
    | Comp (Seq {elt; lim; len_enc}) -> Seq (reduce elt, lim, len_enc)
    | Comp (Padded (n, x)) -> Pad (n, reduce x)
    | Comp (VPadded (_, x)) -> reduce x
    | Comp (Opt {has_tag; enc}) -> Opt (has_tag, reduce enc)
    | Comp (Dyn (lp, x)) -> Dyn (lp, reduce x)
    | Sum (CStyle {size; enums; _}) -> Enum0 (size, enums)
    | Sum (Data {size; vars; _}) ->
        EnumPlus
          (size, List.map (fun (disc, (id, pl)) -> (disc, (id, reduce pl))) vars)

  let produce ?max_length x =
    Ctxt.clear () ;
    reduce x |> Ast.Type.pp ~conf:(Conf.make ?max_length ())
end
