open Compiler_lib

external force_linking : unit -> unit = "%opaque"

let () =
  (* Zarith-related encodings *)
  let open Data_encoding in
  Registration.register
    ~pp:Z.pp_print
    (def "ground.Z" ~description:"Arbitrary precision integers" z) ;
  Registration.register
    ~pp:Z.pp_print
    (def "ground.N" ~description:"Arbitrary precision natural numbers" n)

let () =
  (* zero-length encodings *)
  let open Data_encoding in
  Registration.register (def "ground.unit" unit) ;
  Registration.register
    (def "ground.empty" ~description:"An empty (0-field) object or tuple" empty) ;
  Registration.register (def "ground.null" ~description:"A null value" null)

let () =
  (* integers of various sizes encodings *)
  let open Data_encoding in
  Registration.register
    (def "ground.uint8" ~description:"Unsigned 8 bit integers" uint8) ;
  Registration.register
    (def "ground.int8" ~description:"Signed 8 bit integers" int8) ;
  Registration.register
    (def "ground.uint16" ~description:"Unsigned 16 bit integers" uint16) ;
  Registration.register
    (def "ground.int16" ~description:"Signed 16 bit integers" int16) ;
  Registration.register
    (def "ground.int31" ~description:"Signed 31 bit integers" int31) ;
  Registration.register
    (def "ground.int32" ~description:"Signed 32 bit integers" int32) ;
  Registration.register
    (def "ground.int64" ~description:"Signed 64 bit integers" int64)

let () =
  (* string encodings *)
  let open Data_encoding in
  Registration.register (def "ground.string" string) ;
  Registration.register (def "ground.variable.string" Variable.string) ;
  Registration.register (def "ground.bytes" bytes) ;
  Registration.register (def "ground.variable.bytes" Variable.bytes)

let () =
  (* misc other ground encodings *)
  let open Data_encoding in
  Registration.register (def "ground.bool" ~description:"Boolean values" bool) ;
  Registration.register
    (def "ground.float" ~description:"Floating point numbers" float)

let () = force_linking ()

let block_info ~id (module Protocol : Tezos_shell_services.Block_services.PROTO) =
  let module S = Tezos_shell_services.Block_services.Make (Protocol) (Protocol) in
  let encoding = S.block_info_encoding in
  Pipeline.of_encoding_raw (id, encoding)



let () =
  let codecs = Data_encoding__Registration.list () in
  let env = Pipeline.get_env () and conf = Pipeline.configure () in
  let inputs =
    List.filter_map
      (fun (name, _) ->
        Data_encoding.Registration.find_introspectable name |> function
        | None -> None
        | Some enc -> Some (Pipeline.of_registration (name, enc)))
      codecs
  in
  let extra_inputs =
    [ block_info ~id:"014-PtKathma.block_info" (module Tezos_protocol_014_PtKathma.Protocol)
    ; block_info ~id:"015-PtLimaPt.block_info" (module Tezos_protocol_015_PtLimaPt.Protocol)
    ; Pipeline.of_encoding_raw ("015-PtLimaPt.baking_rights", Tezos_protocol_plugin_015_PtLimaPt.RPC.Baking_rights.encoding)
    ]
  in
  Pipeline.run_pipeline ~env ~conf (extra_inputs @ inputs)
