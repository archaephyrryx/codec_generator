(** [monadic]: marker type indicating a sink-like resolver and a thunk-like
    fragment: fragments are combined as sequential applications over a common
    resolver. *)
type monadic

(** [monoidal]: marker type indicating a thunk-like resolver and a monoid-like
    fragment: fragments are combined into larger fragments, which are resolved
    as a fused value. *)
type monoidal

(** ['a resolver]: disjunction over various sink-types, specifically
    [Format.formatter] and [out_channel].

    An [fmt] value represents an object that can be sequentially fed strings,
    which it will eventually output in FIFO order, either formatted in some
    fashion or as-is.

    The choice of underlying sink-type affects the operational semantics of each
    combinator in the [oper] family, to a degree. *)
type _ resolver =
  | Fmt : Format.formatter -> monadic resolver
  | Chan : out_channel -> monadic resolver
  | Doc : out_channel -> monoidal resolver
  | Tee : 'a resolver * 'a resolver -> 'a resolver

(** ['a fragment]: 'Fragment' containing zero or more string-or-character atoms
    that act monadically for [monadic fragment] and monoidally for [monoidal
    fragment].

    Co-application of an ['a fragment] and an ['a resolver] should be performed
    only once the fragment in question is fully constructed, and not as
    sequential partial computations. *)
and _ fragment =
  | Seq : (monadic resolver -> unit) -> monadic fragment
  | Sum : PPrint.document -> monoidal fragment
  | Nil : 'a fragment

(** [putstr]: given an abritrary string, produce a fragment whose semantics are
    to output the unmodified string contents, with possible surrounding
    formatting or alignment.

    The string should not contain newlines. *)
val putstr : string -> monadic fragment

(** [str]: given an abritrary string, produce a fragment containing just that
    string.

    The string should not contain newlines *)
val str : string -> monoidal fragment

(** [putstr]: given an abritrary char, produce a fragment whose semantics are to
    output that character. *)
val putchr : char -> monadic fragment

(** [chr]: given an abritrary char, produce a fragment containing just that
    character. *)
val chr : char -> monoidal fragment

(** [wrap]: logically group an atomic or compound [fragment] as being in its own
    virtual alignment-group, with different per-resolver semantics (may be
    no-op) *)
val wrap : 'a fragment -> 'a fragment

(** [(|>>)]: [fragment] fusion operator

    The expression [frag |>> frag'] produces a homogenously typed [fragment]
    that either fuses (when [monoidal]) or sequences (when [monadic]) the
    internal denotations of the two fragments.

    In practice, this operator can be used in a longer chained expression, such
    as [f1 |>> f2 |>> ... |>> fN].

    It is intended that, in practice, [fstr x |>> fstr y] is a cheaper
    computation than [fstr (x ^ y)] where [fstr] is either [putstr] or [str] *)
val ( |>> ) : 'a fragment -> 'a fragment -> 'a fragment

(** [ign]: polymorphic [fragment] that acts as the identity under fusion and
    produces no output when resolved. *)
val ign : 'a fragment

(** [seq]: generates a compound [fragment] from a list of values, in the
    following manner:

    [seq ~sep f xs] will sequentially apply [f] to each element of [xs], with
    [sep] interposed between the application of [f] to each consecutive
    element-pair.

    Specifically, [seq ~sep f [x]] is equivalent to [f x], while [seq ~sep f (x
    :: xs)] is defined as [f x |>> inter |>> seq ~inter f xs]. Applying [seq] to
    an empty list acts as [ign]. *)
val seq : sep:'a fragment -> ('b -> 'a fragment) -> 'b list -> 'a fragment

(** [opt]: conditional generation of a ['a fragment] over an optional seed

    [opt f (Some x)] yields [f x], while [opt f None] yields [ign]. *)
val opt : ('a -> 'b fragment) -> 'a option -> 'b fragment

(** [iff]: conditional generation of an ['a fragment] based on a simple boolean predicate

    [iff true x] yields [x] while [iff false _] yields [ign]. *)
val iff : bool -> 'a fragment -> 'a fragment

(** [$]: co-application operator that combines a homogenously-typed
    fragment-resolver pair into an impure computation *)
val ( $ ) : 'a fragment -> 'a resolver -> unit
