open PPrint

type monadic

type monoidal

type _ resolver =
  | Fmt : Format.formatter -> monadic resolver
  | Chan : out_channel -> monadic resolver
  | Doc : out_channel -> monoidal resolver
  | Tee : 'a resolver * 'a resolver -> 'a resolver

and _ fragment =
  | Seq : (monadic resolver -> unit) -> monadic fragment
  | Sum : document -> monoidal fragment
  | Nil : 'a fragment

let putstr : string -> monadic fragment =
 fun s ->
  let rec proc = function
    | Fmt fmt -> Format.pp_print_string fmt s
    | Chan out -> Stdlib.output_string out s
    | Tee (a, b) ->
        proc a ;
        proc b
  in
  Seq proc

let str : string -> monoidal fragment = fun s -> Sum (string s)

let putchr : char -> monadic fragment = function
  | '\n' ->
      let rec proc = function
        | Fmt fmt -> Format.pp_print_cut fmt ()
        | Chan out -> Stdlib.output_char out '\n'
        | Tee (a, b) ->
            proc a ;
            proc b
      in
      Seq proc
  | c ->
      let rec proc = function
        | Fmt fmt -> Format.pp_print_char fmt c
        | Chan out -> Stdlib.output_char out c
        | Tee (a, b) ->
            proc a ;
            proc b
      in
      Seq proc

let chr : char -> monoidal fragment = function
  | '\n' -> Sum hardline
  | c -> Sum (char c)

let wrap (type a) : a fragment -> a fragment = function
  | Seq op ->
      let rec proc res =
        match res with
        | Fmt f ->
            Format.(
              pp_open_vbox f 4 ;
              op res ;
              pp_close_box f ())
        | Chan _ -> op res
        | Tee (a, b) ->
            proc a ;
            proc b
      in
      Seq proc
  | Sum doc -> Sum (nest 4 doc)
  | Nil -> Nil

let ( |>> ) (type a) : a fragment -> a fragment -> a fragment =
 fun frag frag' ->
  match (frag, frag') with
  | Nil, other | other, Nil -> other
  | Seq op1, Seq op2 ->
      Seq
        (fun res ->
          op1 res ;
          op2 res)
  | Sum doc1, Sum doc2 -> Sum (doc1 ^^ doc2)

let ign : 'a. 'a fragment = Nil

let seq (type a) : sep:a fragment -> ('a -> a fragment) -> 'a list -> a fragment
    =
 fun ~sep f xs ->
  let rec seq' = function
    | [] -> ign
    | [x] -> f x
    | x :: xt -> f x |>> sep |>> seq' xt
  in
  seq' xs

let opt (type a) : ('a -> a fragment) -> 'a option -> a fragment =
 fun f -> function Some x -> f x | None -> ign

let iff (type a) : bool -> a fragment -> a fragment =
 fun p x -> if p then x else ign

let rec ( $ ) : type a. a fragment -> a resolver -> unit =
 fun frag res ->
  match (frag, res) with
  | _, Tee (a, b) ->
      frag $ a ;
      frag $ b
  | Nil, _ -> ()
  | Sum doc, Doc oc -> PPrint.ToChannel.pretty 0.8 80 oc doc
  | Seq f, _ -> f res
