(** integer sizes used as the underlying type of [RangedInt] codecs *)
type int_size = [`Uint8 | `Int8 | `Uint16 | `Int16 | `Uint30 | `Int31]

type lenpref = [`Uint8 | `Uint16 | `Uint30 | `N]

type enum_size = [`Uint8 | `Uint16 | `Uint30]

type tag_size = [`Uint8 | `Uint16]

type limit =
  (* Data_encoding__Encoding.limit = *)
  | No_limit
  | At_most of int
  | Exactly of int

val eq_int_size : int_size -> int_size -> bool

(** [native]: integer types that ought to be representable using native types in
    a target language, or otherwise require no specific metadata to handle
    properly *)
type native_int =
  | Uint8
  | Uint16
  | Uint30
  | Int8
  | Int16
  | Int31
  | Int32
  | Int64

(** Any integral type, including both predefined types and ad-hoc Int31-based
    ranges *)
type int_t =
  | NativeInt of native_int
  | RangedInt of {repr : int_size; minimum : int; maximum : int}

type float_t = Double | RangedDouble of {minimum : float; maximum : float}

(** Placeholder type for any numeric codec, currently limited to integral codecs
    but eventually intended for floating-point codecs as well. *)
type num_t = Int of int_t | Float of float_t

(** Fixed-length instantiation of a codec kind of potentially variable length,
    specifically strings and byte-blobs of a specific length in words. *)
type fixed_t = String of int | Bytes of int

type var_t = VString | VBytes

(** [base_t]: algebraic data type enumerating the 'base' types in the
    [Data_encoding] internal schema. More specifically, those types which are
    implicitly defined by the standard of the library and contain no references
    to other type definitions.

    While this project is being incrementally developed, only a subset of the
    full set of fundamental types are actually represented by constructors of
    this ADT, and it is therefore to be understood to be partial until marked
    otherwise. *)
type base_t =
  | Unit  (** Aggregate representative for any [unit Data_encoding.t]*)
  | Bool  (** Direct analogue for [Data_encoding.bool] *)
  | Num of num_t
      (** Aggregate representative for [int]- and [float]-based codecs *)
  | ZarithN (* Arbitrary precision Zarith natural codec *)
  | ZarithZ (* Arbitrary precision Zarith integer codec *)
  | Fixed of fixed_t
      (** Aggregate representative for fixed length [bytes] and [string] codecs *)
  | Var of var_t
      (** Aggregate representative for variable-length [bytes] and [string]
          codecs *)
  | NamedRef of string * ref_t
      (** A subordinate type reference defined by a name and its associated
          structure *)

(** [ref_t]: indirection to a named auxilliary type *)
and ref_t =
  | Concrete of t  (** Concrete reference to a novel name->type association *)
  | Abstract  (** Abstract reference to an existing name->type association *)

(** [seq_t]: homogenous sequence with an optional maximum cardinality *)
and seq_t = {elt : t; lim : limit; len_enc : lenpref option}

(** [prod_t]: Representation of a product-type whose constituent terms are of
    type [t]. *)
and prod_t =
  | Tuple of {id : string option; elems : t list}
      (** Product-type over a list of anonymous arguments

          Though not enforced at the type-level, an empty list is illegal, and
          should ideally have two or more elements (a singleton list does not
          make sense here) *)
  | Record of {id : string option; fields : (string * t) list}
      (** Product-type over a list of uniquely identified (named) arguments

          Though not enforced at the type-level, an empty list is illegal, and
          should ideally have one or more elements *)

(** [sum_t]: Representation of a sum-type whose constituent terms are of type
    [t]. *)
and sum_t =
  | CStyle of {
      id : string option;
      size : enum_size;
      enums : (string * int) list;
    }
  | Data of {
      id : string option;
      size : tag_size;
      vars : (int * (string * t)) list;
    }

and opt_t = {has_tag : bool; enc : t}

and comp_t =
  | Seq of seq_t  (** [Seq]: sequence type (array or list) of a given type *)
  | Opt of opt_t  (** [Opt]: optional (nullable) value of a given type *)
  | Padded of int * t
      (** [Padded]: wrapper around a given type that includes N trailing bytes,
          which we assume to be zeroed *)
  | VPadded of int * t
      (** [VPadded]: virtual wrapper around a given type that indicates it is
          followed by a cumulative N bytes of remaining fields within the
          current container type, which are processed separately. *)
  | Dyn of lenpref * t
      (** [Dyn]: dynamically prefixed element whose length in bytes is encoded
          as one of the uint types in [[lenpref]] *)

(** Simplified representation a [Data_encoding__Encoding.desc] containing only
    enough information to construct a codec *)
and t =
  | Base of base_t
      (** Any encoding that is represented by a standalone value of the
          specified type. *)
  | Prod of prod_t
      (** Any encoding that is representable as a cartesian product of
          base-types. *)
  | Comp of comp_t
      (** Any list-based, dynamically-prefixed, or nullable encoding *)
  | Sum of sum_t  (** Any simple enum or discriminated union of case variants *)

(** Superficial equality operator over [t], which is reflexive, symmetric, and
    transitive.

    Note that [Todo =~ Todo] evaluates to [true], this does not imply that the
    LHS and RHS expressions are isomorphic.

    The intended use-case for this operator is as a transitional aid to test
    that abstract expressions of type [t] evaluate to specific
    constructor-values, rather than as an operator to compare two abstract
    expressions. Namely, we would say [of_encoding x =~ Base Uint8] rather than
    [of_encoding x =~ of_encoding y]. *)
val ( =~ ) : t -> t -> bool

(** [classify_integer_range] is a helper function to be used for [RangedInt]
    nodes, taking the minimum and maximum range-bounds and returning a value of
    type [t], in line with the output of
    [Data_encoding__Binary_size.range_to_size]. *)
val classify_integer_range : minimum:int -> maximum:int -> t

val uint8 : t

val uint16 : t

val uint30 : t

val int8 : t

val int16 : t

val int31 : t

val int32 : t

val int64 : t

val bool : t

val unit : t

(** [of_encoding]: convert an ['a encoding] (for arbitrary ['a]) into the
    equivalent value of type [t]. Strips schema metadata and type information,
    and retains only enough details to be able to use the resultant value to
    construct an interoperable codec. *)
val of_encoding : ?debug:unit -> 'a Data_encoding.t -> t

module Verbose : sig
  type u = t

  type t

  val to_string : t -> string

  val pp : u -> t
end
