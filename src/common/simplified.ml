open Data_encoding

exception Unhandled_case of string

let dodge ~in_use desired =
  let module S = Set.Make (String) in
  let avoid = S.of_list in_use in
  if not (S.mem desired avoid) then desired else
  let rec dodge' ctr =
    let attempt = Printf.sprintf "%s%d" desired ctr in
    if not (S.mem attempt avoid) then attempt else
      dodge' (ctr + 1)
    in dodge' 0

(** Ctxt: Transient context used for generating predictable names
    for anonymous entities, as well as avoiding infinite recursion
    when introspecting recursive schemas *)
module Ctxt = struct
  (** Label: constituent fragments of a name-hierarchy *)
  module Label = struct
    (** Polyvar used to distinguish the contexts in which an embedded anonymous type might appear *)
    type nesting_kind =
      [ `AsSequence
      | `AsOption
      | `AsPadded
      | `AsDynamic
      | `AsLHS
      | `AsRHS
      | `AsSum
      ]

    (** describe: generate a short string from a [nesting_kind] to be used as a label *)
    let describe : nesting_kind -> string = function
      | `AsSequence -> "seq"
      | `AsOption -> "opt"
      | `AsPadded -> "pad"
      | `AsDynamic -> "dyn"
      | `AsLHS -> "lhs"
      | `AsRHS -> "rhs"
      | `AsSum -> "sum"

    (** Label.t: aggregate label type that distinguishes the provenance of a label-string *)
    type t =
      | Explicit of string
      | Positional of int
      | FieldName of string
      | Variant of string
      | Nested of nesting_kind

    (** equality function on [Label.t] *)
    let equal : t -> t -> bool = Stdlib.( = )

    (** var: smart-constructor for [Variant] labels, when naming de-nested union-types *)
    let var : string -> t = fun vname -> Variant vname

    (** exp: smart-constructor for [Explicit] labels, for non-anonymous nodes in the type-hierarchy *)
    let exp : string -> t = fun tname -> Explicit tname

    (** ix: smart constructor for [Positional] labels, for naming types included as tuple constituents *)
    let ix : int -> t = fun ix -> Positional ix

    (** fld: smart constructor for [FieldName] labels, for naming types included as record constituents *)
    let fld : string -> t = fun fname -> FieldName fname

    (** nested: smart constructor for [Nested] labels, for naming types that are inside type-wrappers within the primary type hierarchy *)
    let nested x = Nested x
  end

  include Label

  type label = Label.t

  (** _show_label: debugging function that outputs a stringified form of a label, with a prefix for the constructor used

     - [!]: Explicit
     - [@]: Positional
     - [.]: FieldName
     - [|]: Variant
     - [$in_<xyz>]: Nested within [<xyz>]
  *)
  let _show_label = function
    | Explicit s -> "!" ^ s
    | Positional ix -> "@" ^ string_of_int ix
    | FieldName f -> "." ^ f
    | Variant v -> "|" ^ v
    | Nested x -> "$in_" ^ describe x

  (** Ctxt.S:  Internal module encapsulating a `string`-keyed [Hashtbl]

      Used for building an association-table that maps *)
  module S = struct
    module HString : Stdlib.Hashtbl.HashedType with type t = string = struct
      include Stdlib.String

      let hash = Stdlib.Hashtbl.hash
    end

    include Hashtbl.Make (HString)
  end

  (** Ctxt.R: Internal module encapsulating a `string`-valued [Set] *)
  module R = struct
    include Set.Make (String)
  end

  type assocs = label list S.t

  type recursives = R.t

  (** Context Callback: statefull pseudo-monad that holds a thunk to be
      executed only when the payload is required, to ensure that
      the context-state is updated only when necessary *)
  module CC = struct
    type 'a t = 'a * (unit -> unit)

    (** pure: wraps a value with a no-op thunk *)
    let pure : 'a. 'a -> 'a t = fun x -> (x, fun () -> ())

    (** use: extracts the payload after executing the thunk it is paired with *)
    let use : 'a. 'a t -> 'a =
     fun (x, cb) ->
      cb () ;
      x
  end

  (** Produces a string from a label *)
  let label_to_string = function
    | Explicit tname -> tname
    | Positional ix -> "index" ^ string_of_int ix
    | FieldName fname -> fname
    | Variant vname -> vname
    | Nested `AsRHS -> "rhs"
    | Nested kind -> "denest_" ^ describe kind

  (** t: Full context-state type, including both a traversal-path, a table of string->path mappings,
    and a set of recursive types that have been seen before *)
  type t = label list * assocs * recursives

  (** _show_path: debugging function that ouputs the entirety of a path *)
  let _show_path : label list -> string =
   fun labs -> Stdlib.String.concat " << " @@ Stdlib.List.map _show_label labs

  (** _show: debugging function that displays the path element of a [Ctxt.t] *)
  let _show : t -> string = fun (labs, _assocs, _recs) -> _show_path labs

  (** Generates a fresh context, with an optional parameter [guess] for the size of the association-table to build *)
  let create : ?guess:int -> unit -> t =
   fun ?guess () ->
    let init_size = match guess with None -> 0 | Some n -> n in
    ([], S.create init_size, R.empty)

  (** Given a [Ctxt.t option], returns a [Ctxt.t] value without any promises *)
  let reinit : t option -> t = function Some ctxt -> ctxt | None -> create ()

  (** Unwraps a [Ctxt.t option] into a [Ctxt.t], creating a fresh context if [None] *)
  let of_option : t option -> t = function
    | Some ctxt -> ctxt
    | None -> create ()

  (** Returns a [Ctxt.t] whose innermost label-node in the traversal-path is [Positional]
      If this is already the case, this function is the identity function
      Otherwise, descends by one label into a 0-index [Positional] node.
      *)
  let ensure_tuple : t -> t = function
    | (Positional _ :: _, _, _) as x -> x
    | xs, assocs, recs -> (Positional 0 :: xs, assocs, recs)

  (** Follows a specified label in the provided context, returning a
    nearly-identical context whose path reflects the appropriate descent *)
  let follow : ctxt:t -> label -> t =
   fun ~ctxt label ->
    let here, assocs, recs = ctxt in
    (label :: here, assocs, recs)

  (** Given a context and the identifier string of a recursive [Mu] schema-type,
      returns a modified context that records the fact that that recursive type
      has been seen before, and an indicator of whether it has already been recorded
      ([Some ()] if seen, [None] if novel)
    *)
  let visit : ctxt:t -> string -> unit option * t =
   fun ~ctxt muid ->
    let here, assocs, recs = ctxt in
    let exists = R.mem muid recs in
    if exists then (Some (), ctxt) else (None, (here, assocs, R.add muid recs))

  (** Merges a second context into the first, keeping the path of the first,
      the association-table of the second, and the union of their recursion-sets
    *)
  let fuse : t -> t -> t =
   fun (loc, _, recs) (_, assocs, recs') -> (loc, assocs, R.union recs recs')

  (** Increments the positional index of a tuple, or sets it to 0 if not in a Positional context *)
  let incr : t -> t = function
    | Positional n :: xs, assocs, recs ->
        (Positional (n + 1) :: xs, assocs, recs)
    | xs, assocs, recs -> (Positional 0 :: xs, assocs, recs)

  (** Unwinds one layer of descent, raising an exception if called on the root path *)
  let escape : t -> t = function
    | [], _, _ -> failwith "Cannot escape empty context"
    | _ :: ctxt, assocs, recs -> (ctxt, assocs, recs)

  (** Equality test for two paths, stopping on the first label that differs, or
  when two identical [Explicit] labels are encountered at the same height in
  each path *)
  let rec path_equal : label list -> label list -> bool =
   fun xs ys ->
    match (xs, ys) with
    | [], [] -> true
    | _, [] | [], _ -> false
    | Explicit x :: _, Explicit y :: _ -> x = y
    | xh :: xt, yh :: yt -> Label.equal xh yh && path_equal xt yt

  (** Produces a CC-monad around a string that uniquely identifies the current path,
      with a callback that inserts the association into the table once the CC-monad
      is escaped.

      If somehow the string that would be produced is not unique to the current path,
      a suffix '_dedup' is added one or more times until the resulting identifier is unique
      *)
  let produce : ctxt:t -> string option -> string CC.t =
    let rec resolve tbl id loc is_auto =
      if not is_auto then CC.pure id
      else
        match S.find_opt tbl id with
        | None -> (id, fun () -> S.add tbl id loc)
        | Some path ->
            if path_equal path loc then CC.pure id
            else (
              Printf.eprintf
                "%s != %s (name: %s)\n"
                (_show_path path)
                (_show_path loc)
                id ;
              resolve tbl (id ^ "_dedup") loc true)
    in
    fun ~ctxt x ->
      let here, assocs, _recs = ctxt in
      let ret, is_auto =
        match x with
        | Some id -> (id, false)
        | None ->
            let rec f acc = function
              | [] -> acc
              | x :: xs -> (
                  let urjoin = function
                    | "" -> fun x -> x
                    | other -> fun x -> x ^ "_" ^ other
                  in
                  match x with
                  | Explicit x -> urjoin acc x
                  | other -> f (urjoin acc (label_to_string other)) xs)
            in
            (f "" here, true)
      in
      resolve assocs ret here is_auto

  (* let produce ~ctxt local = let ret = produce ~ctxt local in Printf.eprintf
     "produce: `%s` -> \"%s\"\n" (_show ctxt) ret; ret *)
end

type int_size = [`Int31 | `Int16 | `Int8 | `Uint30 | `Uint16 | `Uint8]

type tag_size = [`Uint8 | `Uint16]

type enum_size = [`Uint30 | `Uint16 | `Uint8]

type native_int =
  | Uint8
  | Uint16
  | Uint30
  | Int8
  | Int16
  | Int31
  | Int32
  | Int64

type int_t =
  | NativeInt of native_int
  | RangedInt of {repr : int_size; minimum : int; maximum : int}

let eq_int_size : int_size -> int_size -> bool = Stdlib.( = )

type float_t = Double | RangedDouble of {minimum : float; maximum : float}

type num_t = Int of int_t | Float of float_t

type fixed_t = String of int | Bytes of int

type var_t = VString | VBytes

type ident = string

type lenpref = [`Uint30 | `Uint16 | `Uint8 | `N]

type limit = Data_encoding__Encoding.limit =
  | No_limit
  | At_most of int
  | Exactly of int

type base_t =
  | Unit
  | Bool
  | Num of num_t
  | ZarithN
  | ZarithZ
  | Fixed of fixed_t
  | Var of var_t
  | NamedRef of ident * ref_t

and ref_t = Concrete of t | Abstract

and field = string * t

and string_enum = string * int

and variant = int * (string * t)

and prod_t =
  | Tuple of {id : ident option; elems : t list}
  | Record of {id : ident option; fields : field list}

and seq_t = {elt : t; lim : limit; len_enc : lenpref option}

and opt_t = {has_tag : bool; enc : t}

and comp_t =
  | Seq of seq_t
  | Opt of opt_t
  | Padded of int * t
  | VPadded of int * t
  | Dyn of lenpref * t

and sum_t =
  | CStyle of {id : ident option; size : enum_size; enums : string_enum list}
  | Data of {id : ident option; size : tag_size; vars : variant list}

and t = Base of base_t | Prod of prod_t | Comp of comp_t | Sum of sum_t

let rec use_first : 'a. 'a Ctxt.CC.t option list -> 'a option = function
  | [] -> None
  | None :: xs -> use_first xs
  | Some ccx :: _ -> Some (Ctxt.CC.use ccx)

let purify : 'a option -> 'a Ctxt.CC.t option = Option.map Ctxt.CC.pure

module Verbose = struct
  type u = t

  type t =
    | Flat of string
    | Nested of {body : string; footnotes : (string * t) list}

  let rec to_string : t -> string = function
    | Flat s -> s
    | Nested {body; footnotes} ->
        let open Format in
        let fmt = str_formatter in
        pp_open_vbox fmt 2 ;
        pp_print_string fmt body ;
        pp_print_cut fmt () ;
        List.iter
          (fun (ident, def) ->
            pp_print_string fmt ident ;
            pp_print_char fmt ':' ;
            pp_print_space fmt () ;
            pp_print_string fmt (to_string def))
          footnotes ;
        pp_close_box fmt () ;
        flush_str_formatter ()

  let pp_num : num_t -> t = function
    | Int (NativeInt Uint8) -> Flat "Uint8"
    | Int (NativeInt Uint16) -> Flat "Uint16"
    | Int (NativeInt Uint30) -> Flat "Uint30"
    | Int (NativeInt Int8) -> Flat "Int8"
    | Int (NativeInt Int16) -> Flat "Int16"
    | Int (NativeInt Int31) -> Flat "Int31"
    | Int (NativeInt Int32) -> Flat "Int32"
    | Int (NativeInt Int64) -> Flat "Int64"
    | Float Double -> Flat "double"
    | Float (RangedDouble {minimum; maximum}) ->
        Flat (Printf.sprintf "bounded double [%f,%f]" minimum maximum)
    | Int (RangedInt {minimum; maximum; repr}) ->
        let repr' =
          match repr with
          | `Uint8 -> "Uint8"
          | `Uint16 -> "Uint16"
          | `Uint30 -> "Uint30"
          | `Int8 -> "Int8"
          | `Int16 -> "Int16"
          | `Int31 -> "Int31"
        in
        Flat (Printf.sprintf "bounded %s [%d,%d]" repr' minimum maximum)

  let rec pp_base : base_t -> t = function
    | Unit -> Flat "unit"
    | Bool -> Flat "bool"
    | Num n -> pp_num n
    | ZarithN ->
        Flat "\u{2115}"
        (* Unicode for 'Double-Struck Capital N' = Set of Naturals *)
    | ZarithZ ->
        Flat "\u{2124}"
        (* Unicode for 'Double-Struck Capital Z' = Set of Integers *)
    | Fixed (String n) -> Flat (Printf.sprintf "String (%d bytes)" n)
    | Fixed (Bytes n) -> Flat (Printf.sprintf "Blob (%d bytes)" n)
    | Var VString -> Flat "String"
    | Var VBytes -> Flat "Blob"
    | NamedRef (ident, r) -> (
        match r with
        | Concrete def ->
            Nested {body = "[" ^ ident ^ "]"; footnotes = [(ident, pp def)]}
        | Abstract -> Flat ("[" ^ ident ^ "]"))

  and pp_comp : comp_t -> t = function
    | Seq {elt; lim; len_enc} -> (
        let f =
          match lim with
          | No_limit -> Printf.sprintf "Sequence (%s)"
          | At_most n -> (
              match len_enc with
              | None -> Printf.sprintf "Sequence [len<=%d] (%s)" n
              | Some l_enc ->
                  let l_enc' =
                    match l_enc with
                    | `Uint8 -> "Uint8"
                    | `Uint16 -> "Uint16"
                    | `Uint30 -> "Uint30"
                    | `N -> "Zarith N (max 30-bit precision)"
                  in
                  Printf.sprintf
                    "Sequence [len<=%d, prefixed as %s] (%s)"
                    n
                    l_enc')
          | Exactly n -> Printf.sprintf "Sequence [len==%d] (%s)" n
        in
        match pp elt with
        | Flat s -> Flat (f s)
        | Nested ({body; _} as x) -> Nested {x with body = f body})
    | VPadded (n, elem) -> (
        let f = Printf.sprintf "VPadded [+%d bytes] (%s)" n in
        match pp elem with
        | Flat s -> Flat (f s)
        | Nested ({body; _} as x) -> Nested {x with body = f body})
    | Padded (n, elem) -> (
        let f = Printf.sprintf "Padded [%d bytes] (%s)" n in
        match pp elem with
        | Flat s -> Flat (f s)
        | Nested ({body; _} as x) -> Nested {x with body = f body})
    | Opt {enc; has_tag} -> (
        let f =
          Printf.sprintf
            "Nullable[%s] (%s)"
            (if has_tag then "prefixed" else "raw")
        in
        match pp enc with
        | Flat s -> Flat (f s)
        | Nested ({body; _} as x) -> Nested {x with body = f body})
    | Dyn (lp, elem) -> (
        let lp' =
          match lp with
          | `Uint8 -> "1-byte"
          | `Uint16 -> "2-byte"
          | `Uint30 -> "4-byte"
          | `N -> "Zarith N (max 30-bit precision)"
        in
        let f = Printf.sprintf "Dynamic [%s] (%s)" lp' in
        match pp elem with
        | Flat s -> Flat (f s)
        | Nested ({body; _} as x) -> Nested {x with body = f body})

  and pp_prod : prod_t -> t = function
    | Record {id; fields} ->
        let keyword =
          match id with
          | None -> "Record"
          | Some id -> Printf.sprintf "Record (`%s`)" id
        in
        let descr =
          match fields with
          | [] -> "  <no fields>"
          | _ ->
              let fs =
                List.map
                  (fun (fname, ftype) ->
                    Printf.sprintf "  `%s`: %s" fname (pp ftype |> to_string))
                  fields
              in
              String.concat "\n" fs
        in
        Flat (Printf.sprintf "%s:\n%s" keyword descr)
    | Tuple {id; elems} ->
        let keyword =
          match id with None -> "" | Some id -> Printf.sprintf "`%s` := " id
        in
        let descr =
          match elems with
          | [] -> ""
          | _ ->
              let fs = List.map (fun ftype -> pp ftype |> to_string) elems in
              String.concat "," fs
        in
        Flat (Printf.sprintf "%s(%s)" keyword descr)

  and pp_sum : sum_t -> t =
    let pp_size : [< enum_size] -> string = function
      | `Uint8 -> "Uint8"
      | `Uint16 -> "Uint16"
      | `Uint30 -> "Uint30"
    in
    function
    | CStyle {id; size; enums; _} ->
        let keyword =
          match id with
          | None -> "Enum"
          | Some id -> Printf.sprintf "Enum (`%s`)" id
        in
        Flat
          (Printf.sprintf
             "%s := [%s => (%s)]"
             keyword
             (pp_size size)
             (String.concat
                " | "
                (List.map
                   (fun (constr, tag) -> Printf.sprintf "%s = %d" constr tag)
                   enums)))
    | Data {id; size; vars} ->
        let keyword =
          match id with
          | None -> "Union"
          | Some id -> Printf.sprintf "Union (`%s`)" id
        in
        Flat
          (Printf.sprintf
             "%s := [%s => (%s)]"
             keyword
             (pp_size size)
             (String.concat
                " | "
                (List.map
                   (fun (tag, (constr, def)) ->
                     Printf.sprintf
                       "(%d ~ %s)[..] = %s"
                       tag
                       constr
                       (pp def |> to_string))
                   vars)))

  and pp : u -> t = function
    | Base b -> pp_base b
    | Comp c -> pp_comp c
    | Prod p -> pp_prod p
    | Sum s -> pp_sum s
end

let dump_simplified x = Stdlib.prerr_endline Verbose.(to_string @@ pp x)

let get_id : t -> ident option = function
  | Sum (CStyle {id; _} | Data {id; _}) | Prod (Tuple {id; _} | Record {id; _})
    ->
      id
  | _ -> None

let ( =~ ) : t -> t -> bool = Stdlib.( = )

let classify_integer_range : minimum:int -> maximum:int -> t =
 fun ~minimum ~maximum ->
  let open Data_encoding__Binary_size in
  Base
    (Num
       (Int
          (let repr = range_to_size ~minimum ~maximum in
           if minimum = min_int repr && maximum = max_int repr then
             NativeInt
               (match repr with
               | `Uint8 -> Uint8
               | `Uint16 -> Uint16
               | `Uint30 -> Uint30
               | `Int8 -> Int8
               | `Int16 -> Int16
               | `Int31 -> Int31)
           else RangedInt {repr; minimum; maximum})))

let classify_float_range : minimum:float -> maximum:float -> t =
 fun ~minimum ~maximum -> Base (Num (Float (RangedDouble {minimum; maximum})))

let uint8 = Base (Num (Int (NativeInt Uint8)))

let uint16 = Base (Num (Int (NativeInt Uint16)))

let uint30 = Base (Num (Int (NativeInt Uint30)))

let int8 = Base (Num (Int (NativeInt Int8)))

let int16 = Base (Num (Int (NativeInt Int16)))

let int31 = Base (Num (Int (NativeInt Int31)))

let int32 = Base (Num (Int (NativeInt Int32)))

let int64 = Base (Num (Int (NativeInt Int64)))

let float = Base (Num (Float Double))

let bool = Base Bool

let unit = Base Unit

let enumerate : ('a, string * int) Stdlib.Hashtbl.t -> 'a array -> t =
 fun tbl _vals ->
  let module H = Stdlib.Hashtbl in
  let enums = H.to_seq_values tbl |> Stdlib.List.of_seq in
  let size = Data_encoding__Binary_size.enum_size _vals in
  Sum (CStyle {id = None; size; enums})

let option_of ~kind = function
  | (Base _ | Comp _) as enc ->
      let has_tag = match kind with `Variable -> false | _ -> true in
      Comp (Opt {has_tag; enc})
  | Prod (Tuple _) -> raise (Unhandled_case "option_of@Prod>Tuple")
  | Prod (Record _) -> raise (Unhandled_case "option_of@Prod>Record")
  | Sum (CStyle _) -> raise (Unhandled_case "option_of@Sum>CStyle")
  | Sum (Data _) -> raise (Unhandled_case "option_of@Sum>Data")

let add_padding n = function x -> Comp (Padded (n, x))

let dynamic_of ~kind = function
  | (Base _ | Comp _) as x -> Comp (Dyn (kind, x))
  | Prod (Tuple _) -> raise (Unhandled_case "dynamic_of@Prod>Tuple")
  | Prod (Record _) -> raise (Unhandled_case "dynamic_of@Prod>Record")
  | Sum (CStyle _) -> raise (Unhandled_case "dynamic_of@Sum>CStyle")
  | Sum (Data _) -> raise (Unhandled_case "dynamic_of@Sum>Data")

let sequence_of ~lim ?len_enc = function
  | (Base _ | Comp _) as elt -> (
      match (lim, len_enc) with
      | Exactly _, Some _ ->
          invalid_arg
            "Cannot combine `Exactly`-limit with sequence-length encoding"
      | No_limit, Some _ ->
          invalid_arg
            "Cannot combine `No_limit`-limit with sequence-length encoding"
      | _ -> Comp (Seq {elt; lim; len_enc}))
  | Prod (Tuple _) -> raise (Unhandled_case "sequence_of@Prod>Tuple")
  | Prod (Record _) -> raise (Unhandled_case "sequence_of@Prod>Record")
  | Sum (CStyle _) -> raise (Unhandled_case "sequence_of@Sum>CStyle")
  | Sum (Data _) -> raise (Unhandled_case "sequence_of@Sum>Data")

let set_name id = function
  | Prod (Record r) -> Prod (Record {r with id = Some id})
  | Prod (Tuple t) -> Prod (Tuple {t with id = Some id})
  | Sum (Data d) -> Sum (Data {d with id = Some id})
  | Sum (CStyle c) -> Sum (CStyle {c with id = Some id})
  | (Base _ | Comp _) as x -> x

let convert_nested_to_reference ?name = function
  | (Prod (Record {id; _} | Tuple {id; _}) | Sum (CStyle {id; _} | Data {id; _}))
    as nested -> (
      match use_first [purify id; name] with
      | None ->
          invalid_arg
            "convert_nested_to_reference: could not find any names to use for \
             reference"
      | Some name -> Base (NamedRef (name, Concrete nested)))
  | other -> other

let fuse_tuple ?id left right =
  match (left, right) with
  | ( ((Base _ | Comp _ | Sum _ | Prod (Record _)) as x),
      ((Base _ | Comp _ | Sum _ | Prod (Record _)) as y) ) ->
      Prod (Tuple {id = use_first [id]; elems = [x; y]})
  | ( ((Base _ | Comp _ | Sum _ | Prod (Record _)) as x),
      Prod (Tuple {id = id'; elems}) ) ->
      Prod (Tuple {id = use_first [id; purify id']; elems = x :: elems})
  | ( Prod (Tuple {id = id'; elems}),
      ((Base _ | Comp _ | Sum _ | Prod (Record _)) as x) ) ->
      Prod (Tuple {id = use_first [id; purify id']; elems = elems @ [x]})
  | Prod (Tuple {id = id'; elems}), Prod (Tuple {id = id''; elems = elems'}) ->
      Prod
        (Tuple
           {
             id = use_first [id; purify id'; purify id''];
             elems = elems @ elems';
           })

type fuse_kind = MergeRecords | FoldIntoCases | ReferInPlace | ReferencePair

let classify_fusion : t -> t -> fuse_kind =
 fun left right ->
  match (left, right) with
  | Prod (Record _), Prod (Record _) -> MergeRecords
  | Prod (Record _), _ -> ReferInPlace
  | Sum (Data _), Prod (Record _) -> FoldIntoCases
  | _, _ -> ReferencePair

let rec merge_records ~ctxt ?id left right =
  match (left, right) with
  | ( Prod (Record {id = id'; fields}),
      Prod (Record {id = id''; fields = fields'}) ) ->
      ( Prod
          (Record
             {
               id = use_first [id; purify id'; purify id''];
               fields = fields @ fields';
             }),
        ctxt )
  | _ -> invalid_arg "merge_records: one or both arguments is not a record"

and fold_into_cases ~ctxt ?id left right =
  match (left, right) with
  | Sum (Data {id = id'; size; vars}), (Prod (Record _) as rhs) ->
      let ctxt', vars' =
        List.fold_left_map
          (fun ctxt (tv, (cn, x)) ->
            let x', ctxt' = fuse_record ~ctxt ?id x rhs in
            (ctxt', (tv, (cn, x'))))
          ctxt
          vars
      in
      (Sum (Data {id = use_first [id; purify id']; size; vars = vars'}), ctxt')
  | _ -> invalid_arg "fold_into_cases: arguments must be Union-Record"

and refer_in_place ~ctxt ?id lhs rhs =
  match lhs, rhs with
  | Prod (Record _) , (Sum _ | Prod (Tuple _)) ->
      let name =
        Option.get
        @@ use_first
             [
               Some Ctxt.(produce ~ctxt:(follow ~ctxt (Nested `AsRHS)) None);
             ]
      in
      let ipr =
        Prod
          (Record
             {
               id = None;
               fields = [(name, Base (NamedRef (name, Concrete rhs)))];
             })
      in
      fuse_record ~ctxt ?id lhs ipr
  | Prod (Record { fields ; _}) , (Base _ | Comp _) ->
      let name = dodge ~in_use:(List.map fst fields) "rhs" in
      fuse_record ~ctxt ?id lhs (Prod (Record { id = None; fields = [(name, rhs)] }))
  | _ -> invalid_arg "refer_in_place: LHS argument must be a record"

and fuse_record ~ctxt ?id left right =
  match classify_fusion left right with
  | MergeRecords -> merge_records ~ctxt ?id left right
  | FoldIntoCases -> fold_into_cases ~ctxt ?id left right
  | ReferInPlace -> refer_in_place ~ctxt ?id left right
  | ReferencePair -> reference_pair ~ctxt ?id left right

and reference_pair ~ctxt ?id left right =
  let ctxt_root = ctxt in
  let ctxt_left = Ctxt.follow ~ctxt:ctxt_root (Positional 0) in
  let cc_lname = Ctxt.produce ~ctxt:ctxt_left None in
  let ctxt_root' = Ctxt.escape ctxt_left in
  let ctxt_right = Ctxt.follow ~ctxt:ctxt_root' (Positional 1) in
  let cc_rname = Ctxt.produce ~ctxt:ctxt_right None in
  let ctxt' = Ctxt.escape ctxt_right in
  let lref =
    match left with
    | Base (NamedRef _) -> left
    | _ ->
      let name = Option.get @@ use_first [Some cc_lname] in
      Base (NamedRef (name, Concrete left))
  in
  let rref =
    match right with
    | Base (NamedRef _) -> right
    | _ ->
    let name = Option.get @@ use_first [Some cc_rname] in
    Base (NamedRef (name, Concrete right))
  in
  (Prod (Tuple {id = use_first [id]; elems = [lref; rref]}), ctxt')

(* FIXME: implement actual size-checking feature *)
let check_size _limit x = x

let num_length : num_t -> int = function
  | Int (NativeInt (Uint8 | Int8) | RangedInt {repr = `Uint8 | `Int8; _}) -> 1
  | Int (NativeInt (Uint16 | Int16) | RangedInt {repr = `Uint16 | `Int16; _}) ->
      2
  | Int
      ( NativeInt (Uint30 | Int31 | Int32)
      | RangedInt {repr = `Uint30 | `Int31; _} ) ->
      4
  | Int (NativeInt Int64) -> 8
  | Float _ -> 8

exception Unfixed

let rec fixed_length : t -> int option = function
  | Base Unit -> Some 0
  | Base Bool -> Some 1
  | Base (Num n) -> Some (num_length n)
  | Base (ZarithN | ZarithZ) -> None
  | Base (Fixed (String n | Bytes n)) -> Some n
  | Base (Var (VString | VBytes)) -> None
  | Base (NamedRef (_, Concrete x)) -> fixed_length x
  | Base (NamedRef (_, Abstract)) -> None
  | Prod (Tuple {elems; _}) -> (
      try
        Some
          (List.fold_left
             (fun acc x ->
               match fixed_length x with
               | None -> raise Unfixed
               | Some n -> acc + n)
             0
             elems)
      with Unfixed -> None)
  | Prod (Record {fields; _}) -> (
      try
        Some
          (List.fold_left
             (fun acc (_, x) ->
               match fixed_length x with
               | None -> raise Unfixed
               | Some n -> acc + n)
             0
             fields)
      with Unfixed -> None)
  | Comp (Dyn (lp, x)) ->
      let lpl =
        match lp with
        | `Uint8 -> Some 1
        | `Uint16 -> Some 2
        | `Uint30 -> Some 4
        | `N -> None
      in
      Option.bind lpl (fun pl -> Option.map (fun m -> pl + m) @@ fixed_length x)
  | Comp (VPadded (_, x)) -> fixed_length x
  | Comp (Padded (pl, x)) -> Option.map (fun m -> pl + m) @@ fixed_length x
  | Comp (Seq {lim = Exactly n; elt; len_enc = None}) ->
      Option.map (fun m -> n * m) @@ fixed_length elt
  | Comp (Opt _ | Seq _) -> None
  | Sum (CStyle {size = `Uint8; _}) -> Some 1
  | Sum (CStyle {size = `Uint16; _}) -> Some 2
  | Sum (CStyle {size = `Uint30; _}) -> Some 4
  | Sum (Data {size; vars; _}) ->
      let sizel = match size with `Uint8 -> 1 | `Uint16 -> 2 in
      let varls = List.map (fun (_, (_, x)) -> fixed_length x) vars in
      let rec f n = function
        | [] -> if n < 0 then None else Some n
        | None :: _ -> None
        | Some m :: xs -> if n < 0 || m = n then f m xs else None
      in
      Option.map (fun m -> sizel + m) @@ f ~-1 varls

let rec of_encoding : type a. ?ctxt:Ctxt.t -> a encoding -> t * Ctxt.t option =
 fun ?ctxt {encoding; _} ->
  match encoding with
  | Bool -> (bool, ctxt)
  | Null | Empty | Ignore | Constant _ -> (unit, ctxt)
  | Uint8 -> (uint8, ctxt)
  | Int8 -> (int8, ctxt)
  | Uint16 -> (uint16, ctxt)
  | Int16 -> (int16, ctxt)
  | Int31 -> (int31, ctxt)
  | Int32 -> (int32, ctxt)
  | Int64 -> (int64, ctxt)
  | RangedInt {minimum; maximum} ->
      (classify_integer_range ~minimum ~maximum, ctxt)
  | Float -> (float, ctxt)
  | RangedFloat {minimum; maximum} ->
      (classify_float_range ~minimum ~maximum, ctxt)
  | N -> (Base ZarithN, ctxt)
  | Z -> (Base ZarithZ, ctxt)
  | Bytes (`Fixed n, _jrep) -> (Base (Fixed (Bytes n)), ctxt)
  | String (`Fixed n, _jrep) -> (Base (Fixed (String n)), ctxt)
  | Bytes (`Variable, _jrep) -> (Base (Var VBytes), ctxt)
  | String (`Variable, _jrep) -> (Base (Var VString), ctxt)
  | Dynamic_size {kind; encoding} ->
      let ctxt = Ctxt.of_option ctxt in
      let ctxt = Ctxt.(follow ~ctxt (nested `AsDynamic)) in
      let simplified, ctxt' = of_encoding ~ctxt encoding in
      let orig = get_id simplified and ctxt' = Stdlib.Option.get ctxt' in
      let ctxt = Ctxt.fuse ctxt ctxt' in
      let element =
        convert_nested_to_reference ~name:(Ctxt.produce ~ctxt orig) simplified
      in
      (dynamic_of ~kind element, Some (Ctxt.escape ctxt))
  | Array {length_limit = lim; elts; length_encoding} ->
      let ctxt = Ctxt.of_option ctxt in
      let ctxt = Ctxt.(follow ~ctxt (nested `AsSequence)) in
      let simplified, ctxt' = of_encoding ~ctxt elts in
      let orig = get_id simplified and ctxt' = Stdlib.Option.get ctxt' in
      let ctxt = Ctxt.fuse ctxt ctxt' in
      let element =
        convert_nested_to_reference ~name:(Ctxt.produce ~ctxt orig) simplified
      in
      let to_lenpref = function
        | `Fixed 1 -> `Uint8
        | `Fixed 2 -> `Uint16
        | `Fixed 4 -> `Uint30
        | `Dynamic -> `N
        | `Variable -> invalid_arg "Unexpected Variable kind in to_lenpref"
        | `Fixed _ -> invalid_arg "Unexpected Fixed != 1,2,4 in to_lenpref"
      in
      let len_enc =
        Option.map
          (fun x -> Data_encoding.classify x |> to_lenpref)
          length_encoding
      in
      (sequence_of ~lim ?len_enc element, Some (Ctxt.escape ctxt))
  | List {length_limit = lim; elts; length_encoding} ->
      let ctxt = Ctxt.of_option ctxt in
      let ctxt = Ctxt.(follow ~ctxt (nested `AsSequence)) in
      let simplified, ctxt' = of_encoding ~ctxt elts in
      let orig = get_id simplified and ctxt' = Stdlib.Option.get ctxt' in
      let ctxt = Ctxt.fuse ctxt ctxt' in
      let element =
        convert_nested_to_reference ~name:(Ctxt.produce ~ctxt orig) simplified
      in
      let to_lenpref = function
        | `Fixed 1 -> `Uint8
        | `Fixed 2 -> `Uint16
        | `Fixed 4 -> `Uint30
        | `Dynamic -> `N
        | `Variable -> invalid_arg "Unexpected Variable kind in to_lenpref"
        | `Fixed _ -> invalid_arg "Unexpected Fixed != 1,2,4 in to_lenpref"
      in
      let len_enc =
        Option.map
          (fun x -> Data_encoding.classify x |> to_lenpref)
          length_encoding
      in
      (sequence_of ~lim ?len_enc element, Some (Ctxt.escape ctxt))
  | Tup enc ->
      let ctxt = Ctxt.of_option ctxt in
      let ctxt' = Ctxt.(follow ~ctxt (ix 0)) in
      let simplified, ctxt'' = of_encoding ~ctxt:ctxt' enc in
      let orig = get_id simplified in
      ( convert_nested_to_reference ~name:(Ctxt.produce ~ctxt orig) simplified,
        Some Ctxt.(incr @@ escape @@ Stdlib.Option.get ctxt'') )
  | Tups {kind = _; left; right} ->
      let ctxt = Ctxt.(ensure_tuple @@ of_option ctxt) in
      let left', ctxt = of_encoding ~ctxt left in
      let right', ctxt = of_encoding ?ctxt right in
      (fuse_tuple left' right', ctxt)
  | Obj fld -> (
      match fld with
      | Req {name; encoding; _} | Dft { name; encoding; _ } ->
          let ctxt = Ctxt.of_option ctxt in
          let ctxt = Ctxt.(follow ~ctxt (fld name)) in
          let simplified, ctxt' = of_encoding ~ctxt encoding in
          let orig = get_id simplified in
          let ctxt' = Stdlib.Option.get ctxt' in
          let ctxt = Ctxt.fuse ctxt ctxt' in
          ( Prod
              (Record
                 {
                   id = None;
                   fields =
                     [
                       ( name,
                         convert_nested_to_reference
                           ~name:(Ctxt.produce ~ctxt orig)
                           simplified );
                     ];
                 }),
            Some (Ctxt.escape ctxt) )
      | Opt {name; encoding; kind; _} ->
          let ctxt = Ctxt.of_option ctxt in
          let ctxt = Ctxt.(follow ~ctxt (fld name)) in
          let inner, ctxt' = of_encoding ~ctxt encoding in
          let orig = get_id inner in
          let ctxt' = Stdlib.Option.get ctxt' in
          let ctxt = Ctxt.fuse ctxt ctxt' in
          ( Prod
              (Record
                 {
                   id = None;
                   fields =
                     [
                       ( name,
                         option_of
                           ~kind
                           (convert_nested_to_reference
                              ~name:(Ctxt.produce ~ctxt orig)
                              inner) );
                     ];
                 }),
            Some (Ctxt.escape ctxt) ))
    | Objs {left; right; _} ->
      let ctxt = Ctxt.of_option ctxt in
      let left', ctxt = of_encoding ~ctxt left in
      let right', ctxt = of_encoding ?ctxt right in
      let ctxt = Ctxt.of_option ctxt in
      let ret, ctxt' = fuse_record ~ctxt left' right' in
      (ret, Some ctxt')
  | Describe {encoding; id; _} ->
      let ctxt = Ctxt.(exp id |> follow ~ctxt:(Ctxt.reinit ctxt)) in
      let enc, ctxt = of_encoding ~ctxt encoding in
      (set_name id enc, Stdlib.Option.map Ctxt.escape ctxt)
  | Splitted {encoding; _} -> of_encoding ?ctxt encoding
  | Union {tag_size; cases; _} ->
      let f :
          type b.
          ?ctxt:Ctxt.t ->
          b Data_encoding__Encoding.case ->
          (int * (string * t)) * Ctxt.t option =
       fun ?ctxt (Case {title; tag; encoding; _}) ->
        let tagval = Data_encoding__Uint_option.get tag in
        let ctxt = Ctxt.(follow ~ctxt:(Ctxt.of_option ctxt) (var title)) in
        let enc, ctxt' = of_encoding ~ctxt encoding in
        let enc' = match enc with
          | Sum _ -> convert_nested_to_reference ~name:Ctxt.(produce ~ctxt:(follow ~ctxt (Nested `AsSum)) None) enc
          | _ -> enc
       in
        ((tagval, (title, enc')), Stdlib.Option.map Ctxt.escape ctxt')
      in
      let rec fold_cases ctxt acc = function
        | [] ->
            (Sum (Data {id = None; size = tag_size; vars = List.rev acc}), ctxt)
        | x :: xs ->
            let case, ctxt = f ?ctxt x in
            fold_cases ctxt (case :: acc) xs
      in
      fold_cases ctxt [] cases
  | String_enum (tbl, vals) -> (enumerate tbl vals, ctxt)
  | Conv {encoding; _} -> of_encoding ?ctxt encoding
  | Padded (enc, n) ->
      let ctxt = Ctxt.of_option ctxt in
      let ctxt = Ctxt.(follow ~ctxt (nested `AsPadded)) in
      let simplified, ctxt' = of_encoding ~ctxt enc in
      let orig = get_id simplified and ctxt' = Stdlib.Option.get ctxt' in
      let ctxt = Ctxt.fuse ctxt ctxt' in
      let element =
        convert_nested_to_reference ~name:(Ctxt.produce ~ctxt orig) simplified
      in
      (add_padding n element, Some (Ctxt.escape ctxt))
  | Check_size {limit; encoding} ->
      (* FIXME: limit is ignored, at least for now *)
      ignore limit ;
      let ret, ctxt = of_encoding ?ctxt encoding in
      (check_size limit ret, ctxt)
  | Delayed thunk -> thunk () |> of_encoding ?ctxt
  | Mu {name; fix; _} -> (
      let ctxt = Ctxt.of_option ctxt in
      let status, ctxt' = Ctxt.visit name ~ctxt in
      match status with
      | None ->
          let ret, ctxt =
            fix {encoding; json_encoding = None} |> of_encoding ~ctxt:ctxt'
          in
          (Base (NamedRef (name, Concrete ret)), ctxt)
      | Some () -> (Base (NamedRef (name, Abstract)), Some ctxt'))

let rec needs_lenmark : t -> bool = function
  | Base (Unit | Num _ | Fixed _ | Bool | ZarithN | ZarithZ) -> false
  | Base (Var _) -> true
  | Comp (Dyn _) -> false
  | Comp (Seq {lim = Exactly _; elt; len_enc = None}) -> needs_lenmark elt
  | Comp (Seq _) -> true
  | Base (NamedRef (_, Concrete x)) -> needs_lenmark x
  | Base (NamedRef (_, Abstract)) -> true
  | Comp (Padded (_, x)) -> needs_lenmark x
  | Comp (VPadded _) -> false
  | Comp (Opt {enc; has_tag}) -> (not has_tag) || needs_lenmark enc
  | Prod _ -> false
  | Sum _ -> false

let lenmark : int -> t -> t =
 fun n x -> if needs_lenmark x && n > 0 then Comp (VPadded (n, x)) else x

let rec annotate : t -> t = function
  | Prod (Tuple ({elems; _} as t)) ->
      let _, elems' =
        List.fold_right
          (fun elt (l_after, tail) ->
            match l_after with
            | None -> (None, elt :: tail)
            | Some n ->
                let l_after' =
                  Option.map (fun m -> n + m) @@ fixed_length elt
                in
                (l_after', lenmark n elt :: tail))
          elems
          (Some 0, [])
      in
      Prod (Tuple {t with elems = elems'})
  | Prod (Record ({fields; _} as r)) ->
      let _, fields' =
        List.fold_right
          (fun ((fname, ftype) as fld) (l_after, tail) ->
            match l_after with
            | None -> (None, fld :: tail)
            | Some n ->
                let l_after' =
                  Option.map (fun m -> n + m) @@ fixed_length ftype
                in
                (l_after', (fname, lenmark n ftype) :: tail))
          fields
          (Some 0, [])
      in
      Prod (Record {r with fields = fields'})
  | Sum (Data ({vars; _} as dat)) ->
      let vars' =
        List.map (fun (n, (cname, pload)) -> (n, (cname, annotate pload))) vars
      in
      Sum (Data {dat with vars = vars'})
  | other -> other

let of_encoding : 'a. ?debug:unit -> 'a encoding -> t =
 fun ?debug enc ->
  let simple, _ = of_encoding enc in
  if Stdlib.Option.is_some debug then dump_simplified simple ;
  annotate simple
