let example_bool ?ts () =
  Demo_lib.Pipeline.run
    ?ts
    (module struct
      type t = bool

      let schema_name = "verity"

      let encoding = Data_encoding.bool
    end)

let example0 () =
  Demo_lib.Pipeline.run
    (module struct
      type t = {first : string; middle : string option; last : string}

      let to_tuple {first; middle; last} = (first, middle, last)

      let of_tuple (first, middle, last) = {first; middle; last}

      let schema_name = "Person"

      let encoding =
        Data_encoding.(
          conv
            to_tuple
            of_tuple
            (obj3
               (req "first" string)
               (opt "middle" string)
               (req "last" string)))
    end)

let example1 ?ts () =
  Demo_lib.Pipeline.run
    ?ts
    (module struct
      type t = int * int * int * int

      let schema_name = "IPv4"

      let encoding = Data_encoding.(tup4 uint8 uint8 uint8 uint8)
    end)

let example2 ?ts () =
  Demo_lib.Pipeline.run
    ?ts
    (module struct
      type t = Absolute of string list | Relative of string list

      let schema_name = "FilePath"

      let encoding =
        Data_encoding.(
          union
            [
              case
                (Tag 0)
                ~title:"Absolute"
                (list string)
                (function Absolute xs -> Some xs | _ -> None)
                (fun xs -> Absolute xs);
              case
                (Tag 1)
                ~title:"Relative"
                (list string)
                (function Relative xs -> Some xs | _ -> None)
                (fun xs -> Relative xs);
            ])
    end)

let example3 ?ts () =
  Demo_lib.Pipeline.run
    ?ts
    (module struct
      type t = Z | S of t

      let schema_name = "Nat"

      let encoding =
        Data_encoding.(
          mu "Nat" (fun enc ->
              union
                [
                  case
                    (Tag 0)
                    ~title:"Z"
                    empty
                    (function Z -> Some () | _ -> None)
                    (fun _ -> Z);
                  case
                    (Tag 1)
                    ~title:"S"
                    enc
                    (function S n -> Some n | _ -> None)
                    (fun n -> S n);
                ]))
    end)

let example4 ?ts () =
  Demo_lib.Pipeline.run
    ?ts
    (module struct
      type t = OneLie of bool | TwoTruths of bool * bool

      let schema_name = "TTOL"

      let encoding =
        Data_encoding.(
          union
            [
              case
                (Tag 1)
                ~title:"OneLie"
                (Fixed.add_padding bool 1)
                (function OneLie x -> Some x | _ -> None)
                (fun x -> OneLie x);
              case
                (Tag 2)
                ~title:"TwoTruths"
                (tup2 bool bool)
                (function TwoTruths (x, y) -> Some (x, y) | _ -> None)
                (fun (x, y) -> TwoTruths (x, y));
            ])
    end)

let example5 ?ts () =
  Demo_lib.Pipeline.run
    ?ts
    (module struct
      type t = int list

      let schema_name = "VarTup"

      let encoding = Data_encoding.(Variable.list ~max_length:9 int31)
    end)

let example6 ?ts () =
  Demo_lib.Pipeline.run
    ?ts
    (module struct
      type t = {first : string; middle : string; last : string}

      let to_tuple {first; middle; last} = (first, middle, last)

      let of_tuple (first, middle, last) = {first; middle; last}

      let schema_name = "Person"

      let encoding =
        Data_encoding.(
          conv
            to_tuple
            of_tuple
            (obj3
               (req "first" string)
               (dft "middle" string "Danger")
               (req "last" string)))
    end)

let example7 ?ts () =
  Demo_lib.Pipeline.run
    ?ts
    (module struct
      type tree = Leaf | Node of tree * forest

      and forest = Empty | Children of tree * forest

      type t = tree

      let tree_encoding =
        let open Data_encoding in
        let mu_tree forest_enc =
          mu "tree" @@ fun tree_enc ->
          union
            [
              case
                (Tag 0)
                ~title:"Leaf"
                empty
                (function Leaf -> Some () | _ -> None)
                (fun () -> Leaf);
              case
                (Tag 1)
                ~title:"Node"
                (obj2 (req "first" tree_enc) (req "forest" forest_enc))
                (function
                  | Node (tree, forest) -> Some (tree, forest) | _ -> None)
                (fun (tree, forest) -> Node (tree, forest));
            ]
        in
        let mu_forest =
          mu "forest" @@ fun forest_enc ->
          union
            [
              case
                (Tag 0)
                ~title:"Empty"
                empty
                (function Empty -> Some () | _ -> None)
                (fun () -> Empty);
              case
                (Tag 1)
                ~title:"Children"
                (obj2 (req "tree" (mu_tree forest_enc)) (req "rest" forest_enc))
                (function Children (t, f) -> Some (t, f) | _ -> None)
                (fun (t, f) -> Children (t, f));
            ]
        in
        mu_tree mu_forest

      let encoding = tree_encoding

      let schema_name = "Tree"
    end)
