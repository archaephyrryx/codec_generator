export class Base58UnexpectedLengthError extends Error {
    public readonly name = 'Base58UnexpectedLengthError';
    public readonly expected: number;
    public readonly actual: number;

    constructor({ expected, actual }: { expected: number, actual: number }) {
        super(
            `Base58-encoded string had ${actual} non-prefix bytes when ${expected} were expected`
        );
        this.expected = expected;
        this.actual = actual;
    }
}

export class UnsupportedPublicKeyError extends Error {
    public readonly name = 'UnsupportedPublicKeyError';

    constructor(public prefix: string) {
        super(`Unsupported public key prefix ${prefix}`);
    }
}


export class UnsupportedPublicKeyHashError extends Error {
    public readonly name = 'UnsupportedPublicKeyHashError';

    constructor(public prefix: string) {
        super(`Unsupported public key hash prefix ${prefix}`);
    }
}

export class UnsupportedContractIdError extends Error {
    public readonly name = 'UnsupportedContractIdError';

    constructor(public prefix: string) {
        super(`Unsupported contract-id prefix ${prefix}`);
    }

}