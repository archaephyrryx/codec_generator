import { bytesdecoder } from "../src/primitive";
import { Proto010_PtGRANAD__script__expr } from './codec'
import { uni_to_hex } from "./common";

import script_expr = Proto010_PtGRANAD__script__expr;
import expr = script_expr.NSmicheline__michelson_v1__expression;

type PrimValue = { prim: string; args?: MichelsonValue[]; annots?: string[] };
type BytesValue = { bytes: string };
type StringValue = { string: string };
type IntValue = { int: string };
export type MichelsonValue =
    | PrimValue
    | BytesValue
    | StringValue
    | IntValue
    | (PrimValue | BytesValue | StringValue | IntValue)[];

export const isPrim = (value: MichelsonValue): value is PrimValue => {
    return 'prim' in value;
};

export const isBytes = (value: MichelsonValue): value is BytesValue => {
    // tslint:disable-next-line: strict-type-predicates
    return 'bytes' in value && typeof value.bytes === 'string';
};

export const isString = (value: MichelsonValue): value is StringValue => {
    // tslint:disable-next-line: strict-type-predicates
    return 'string' in value && typeof value.string === 'string';
};

export const isInt = (value: MichelsonValue): value is IntValue => {
    // tslint:disable-next-line: strict-type-predicates
    return 'int' in value && typeof value.int === 'string';
};

export const is_unit_param = (value: PrimValue): boolean => {
    return (value.prim === 'Unit' && !value.args && !value.annots);
}

export const primvalue_to_expression = (value: PrimValue): expr.expression => {
    const args_annot_tag = {
        [0]: {
            [0]: expr.expression_tag.Prim00,
            [1]: expr.expression_tag.Prim01,
        },
        [1]: {
            [0]: expr.expression_tag.Prim10,
            [1]: expr.expression_tag.Prim11,
        },
        [2]: {
            [0]: expr.expression_tag.Prim20,
            [1]: expr.expression_tag.Prim21,
        },
    };
    const n_args = value.args?.length ?? 0;
    const n_annots = value.annots?.length ? 1 : 0;

    const conv_args = (value.args ?? []).map(arg => value_to_expression(arg));
    const conv_annots = (value.annots ?? []).map(arg => uni_to_hex(arg)).join('20'); // 0x20 for ' '

    const prim = script_expr.michelson__v1__primitives[value.prim];
    if (n_args > 2) {
        const _tag = expr.expression_tag.GenericPrim;
        return { _tag, prim, args: conv_args, annots: conv_annots };
    } else {
        const _tag = args_annot_tag[n_args ?? 0][n_annots ?? 0];
        const annots: Partial<expr.expressionGenericPrim> = (n_annots) ? { annots: conv_annots } : {};
        switch (n_args) {
            case 0:
                return { _tag, prim, ...annots };
            case 1:
                return { _tag, prim, arg: conv_args[0], ...annots };
            case 2:
                return { _tag, prim, arg1: conv_args[0], arg2: conv_args[1], ...annots };
            default:
                throw new Error(`Illegal PrimValue: ${n_args} arguments (must be in range [0,2]).`);
        }
    }
}

export const value_to_expression = (value: MichelsonValue): expr.expression => {
    if (Array.isArray(value)) {
        const _tag = expr.expression_tag.Sequence;
        const _anon2 = value.map(x => value_to_expression(x));
        return { _tag, _anon2 };
    } else if (isPrim(value)) {
        return primvalue_to_expression(value);
    } else if (isBytes(value)) {
        return bytesvalue_to_expression(value);
    } else if (isString(value)) {
        return stringvalue_to_expression(value);
    } else if (isInt(value)) {
        return intvalue_to_expression(value);
    }

    throw new Error(`Unexpected value: ${JSON.stringify(value)}`);
};

export const bytesvalue_to_expression = (value: BytesValue): expr.expression => (
    { _tag: expr.expression_tag.Bytes, bytes: bytesdecoder()(value.bytes) }
);

export const stringvalue_to_expression = (value: StringValue): expr.expression => (
    { _tag: expr.expression_tag.String, string: uni_to_hex(value.string) }
);

export const intvalue_to_expression = (value: IntValue): expr.expression => (
    { _tag: expr.expression_tag.Int, int: BigInt(value.int) }
);