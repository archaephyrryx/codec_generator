import { Tagless } from '../src/adt';
import * as schema from './schema';
import { convert } from './dev';
import * as codec from './codec';

import alphacontents = codec.Proto010_PtGRANAD__operation__unsigned.NSoperation__alpha__contents;

type transaction = alphacontents.contentsTransaction;
type delegation = alphacontents.contentsDelegation;
type reveal = alphacontents.contentsReveal;
type ballot = alphacontents.contentsBallot;
type endorsement = alphacontents.contentsEndorsement;
type seed_nonce_revelation = alphacontents.contentsSeed_nonce_revelation;
type proposals = alphacontents.contentsProposals;
type origination = alphacontents.contentsOrigination;
type activation = alphacontents.contentsActivate_account;
const is_transaction = (x: {}): x is Tagless<transaction> => {
    return Object.keys(schema.TransactionSchema).every(k => k in x);
};
const is_delegation = (x: {}): x is Tagless<delegation> => {
    return Object.keys(schema.DelegationSchema).every(k => k in x);
};
const is_reveal = (x: {}): x is Tagless<reveal> => {
    return Object.keys(schema.RevealSchema).every(k => k in x);
};
const is_ballot = (x: {}): x is Tagless<ballot> => {
    return Object.keys(schema.BallotSchema).every(k => k in x);
};
const is_endorsement = (x: {}): x is Tagless<endorsement> => {
    return Object.keys(schema.EndorsementSchema).every(k => k in x);
};
const is_seed_nonce_revelation = (x: {}): x is Tagless<seed_nonce_revelation> => {
    return Object.keys(schema.SeedNonceRevelationSchema).every(k => k in x);
};
const is_proposals = (x: {}): x is Tagless<proposals> => {
    return Object.keys(schema.ProposalsSchema).every(k => k in x);
};
const is_origination = (x: {}): x is Tagless<origination> => {
    return Object.keys(schema.OriginationSchema).every(k => k in x);
};
const is_activation = (x: {}): x is Tagless<activation> => {
    return Object.keys(schema.ActivationSchema).every(k => k in x);
};

export const narrow_contents = (_tag: alphacontents.contents_tag, _contents: unknown): alphacontents.contents => {
    switch (_tag) {
        case alphacontents.contents_tag.Transaction:
            _tag = _tag as alphacontents.contents_tag.Transaction;
            if (is_transaction(_contents))
                return { _tag, ..._contents };
        case alphacontents.contents_tag.Delegation:
            _tag = _tag as alphacontents.contents_tag.Delegation;
            if (is_delegation(_contents))
                return { _tag, ..._contents };
        case alphacontents.contents_tag.Reveal:
            _tag = _tag as alphacontents.contents_tag.Reveal;
            if (is_reveal(_contents))
                return { _tag, ..._contents };
        case alphacontents.contents_tag.Ballot:
            _tag = _tag as alphacontents.contents_tag.Ballot;
            if (is_ballot(_contents))
                return { _tag, ..._contents };
        case alphacontents.contents_tag.Endorsement:
            _tag = _tag as alphacontents.contents_tag.Endorsement;
            if (is_endorsement(_contents))
                return { _tag, ..._contents };
        case alphacontents.contents_tag.Seed_nonce_revelation:
            _tag = _tag as alphacontents.contents_tag.Seed_nonce_revelation;
            if (is_seed_nonce_revelation(_contents))
                return { _tag, ..._contents };
        case alphacontents.contents_tag.Proposals:
            _tag = _tag as alphacontents.contents_tag.Proposals;
            if (is_proposals(_contents))
                return { _tag, ..._contents };
        case alphacontents.contents_tag.Origination:
            _tag = _tag as alphacontents.contents_tag.Origination;
            if (is_origination(_contents))
                return { _tag, ..._contents };
        case alphacontents.contents_tag.Activate_account:
            _tag = _tag as alphacontents.contents_tag.Activate_account;
            if (is_activation(_contents))
                return { _tag, ..._contents };
        default:
            throw new Error(`narrow_contents: unhandled contents case ${alphacontents.contents_tag[_tag]}.`);
    }
};

export const convert_alphacontents = (elem: { kind: string; }): alphacontents.contents => {
    const _kind = elem.kind.charAt(0).toUpperCase() + elem.kind.slice(1);
    var _tag: alphacontents.contents_tag = alphacontents.contents_tag[_kind];
    var _contents = schema.schema_converter(convert)(schema.operation_schema_table[_tag])(elem);
    return narrow_contents(_tag, _contents);
};
