import * as codec from './codec'
import { Base58UnexpectedLengthError, UnsupportedContractIdError, UnsupportedPublicKeyError, UnsupportedPublicKeyHashError } from './error';

import { Buffer } from 'buffer';
import bs58check from 'bs58check';

import { int8 } from '../../ts_runtime/integer/types';
import { Decoder, liftDecoder } from '../../ts_runtime/decode';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import width from '../../ts_runtime/core/width.type';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Codec } from '../../ts_runtime/codec';

import BranchId = codec.CGRIDClass__OperationShell_header_branch
import { Base58Consts, BRANCH, CONTRACT, PK, PKH, PROPOSAL } from './consts';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { Unboxed } from '../../ts_runtime/core/box';
import { SequenceBounded } from '../../ts_runtime/composite/seq/sequence.bounded';

type bytes = Uint8Array

/**
 * Helper function for converting base58-encoded strings into Buffers,
 * dropping a specified number of prefix bytes and optionally checking
 * the length of the resulting slice.
 *
 * @param val Base58-encoded string to marshall into bytes
 * @param params Length constants associated with the base58-encoded element
 * @returns The prefix-stripped buffer decoded from the Base58-Encoded string
 *
 * @throws {@link Base58UnexpectedLengthError}
 * If the prefix-stripped byte array does not have the expected length
 */
export const b58_to_bytes = (val: string, { prefix_len, payload_len }: Base58Consts): bytes => {
  const ret: Buffer = bs58check.decode(val).subarray(prefix_len);
  if (ret.byteLength != payload_len) {
    throw new Base58UnexpectedLengthError({ expected: payload_len, actual: ret.length });
  }
  return ret;
}

export type StringDecoder<T> = (val: string) => T;

export const b58_to_codec =
  <T>(dec: Decoder<T>, params: Base58Consts): StringDecoder<T> =>
    (val: string) => {
      return liftDecoder(dec)(b58_to_bytes(val, params));
    };

export const string_to_branch: StringDecoder<BranchId> = b58_to_codec(BranchId.decode, BRANCH);

export const string_to_pkh: StringDecoder<codec.PublicKeyHash> = (val: string) => {
  const pkhPrefix = val.slice(0, PKH.prefix_len);
  const f = <T extends Codec>(dec: Decoder<T>) => b58_to_codec(dec, PKH)(val);
  switch (pkhPrefix) {
    case 'tz1':
      return {
        kind: codec.CGRIDTag__PublicKeyHash.Ed25519,
        value: f(codec.CGRIDClass__PublicKeyHash__Ed25519.decode),
      };
    case 'tz2':
      return {
        kind: codec.CGRIDTag__PublicKeyHash.Secp256k1,
        value: f(codec.CGRIDClass__PublicKeyHash__Secp256k1.decode),
      };
    case 'tz3':
      return {
        kind: codec.CGRIDTag__PublicKeyHash.P256,
        value: f(codec.CGRIDClass__PublicKeyHash__P256.decode),
      };
    default:
      throw new UnsupportedPublicKeyHashError(pkhPrefix);
  }
};

export const string_to_public_key: StringDecoder<codec.PublicKey> = (val: string) => {
  const pubkeyPrefix = val.slice(0, PK.prefix_len);
  const f = <T extends Codec>(dec: Decoder<T>) => b58_to_codec(dec, PK)(val);
  switch (pubkeyPrefix) {
    case 'edpk':
      return {
        kind: codec.CGRIDTag__PublicKey.Ed25519,
        value: f(codec.CGRIDClass__PublicKey__Ed25519.decode),
      };
    case 'sppk':
      return {
        kind: codec.CGRIDTag__PublicKey.Secp256k1,
        value: f(codec.CGRIDClass__PublicKey__Secp256k1.decode),
      };
    case 'p2pk':
      return {
        kind: codec.CGRIDTag__PublicKey.P256,
        value: f(codec.CGRIDClass__PublicKey__P256.decode),
      };
    default:
      throw new UnsupportedPublicKeyError(pubkeyPrefix);
  }
};

export const string_to_contract_hash: StringDecoder<codec.CGRIDClass__AlphaContractId__Originated> = b58_to_codec(codec.CGRIDClass__AlphaContractId__Originated.decode, CONTRACT);

export const string_to_contract_id: StringDecoder<codec.AlphaContractId> = (val: string) => {
  const addr_prefix_len = 3;
  const pubkeyPrefix = val.slice(0, addr_prefix_len);
  switch (pubkeyPrefix) {
    case 'tz1':
    case 'tz2':
    case 'tz3':
      return {
        kind: codec.CGRIDTag__AlphaContractId.Implicit,
        value: b58_to_codec(codec.CGRIDClass__AlphaContractId__Implicit.decode, PKH)(val)
      };
    case 'KT1':
      return {
        kind: codec.CGRIDTag__AlphaContractId.Originated,
        value: b58_to_codec(codec.CGRIDClass__AlphaContractId__Originated.decode, CONTRACT)(val),
      };
    default:
      throw new UnsupportedContractIdError('Invalid address');
  }
};

export const string_to_proposal: StringDecoder<codec.CGRIDClass__Proto015_PtLimaPtOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq> =
  b58_to_codec(codec.CGRIDClass__Proto015_PtLimaPtOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq.decode, PROPOSAL);

export const string_to_proposals = (proposals: string[]): codec.Proto015PtLimaPtOperationAlphaContents__Proposals['proposals'] => {
  const props = proposals.map(string_to_proposal);
  return new Dynamic(new SequenceBounded(props, 20), width.Uint30);
}

export enum BALLOT {
  yay = 0,
  nay = 1,
  pass = 2,
}

export const string_to_ballot = (ballot: string): int8 => {
  if (ballot in BALLOT) {
    return BALLOT[ballot];
  } else {
    throw new Error(`Unexpected ballot ${ballot}: should be ${Object.values(BALLOT).filter((x: string | BALLOT): x is string => typeof x === 'string').join("|")}`);
  }
}