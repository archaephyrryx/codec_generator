import * as codec from './codec';

export const ManagerOperationSchema = {
    branch: 'branch',
    contents: ['operation'],
};

export const ActivationSchema = {
    pkh: 'tz1',
    secret: 'secret',
};

export const RevealSchema = {
    source: 'pkh',
    fee: 'zarith',
    counter: 'zarith',
    gas_limit: 'zarith',
    storage_limit: 'zarith',
    public_key: 'public_key',
};

export const DelegationSchema = {
    source: 'pkh',
    fee: 'zarith',
    counter: 'zarith',
    gas_limit: 'zarith',
    storage_limit: 'zarith',
    delegate: 'delegate',
};

export const TransactionSchema = {
    source: 'pkh',
    fee: 'zarith',
    counter: 'zarith',
    gas_limit: 'zarith',
    storage_limit: 'zarith',
    amount: 'zarith',
    destination: 'address',
    parameters: 'parameters',
};

export const OriginationSchema = {
    source: 'pkh',
    fee: 'zarith',
    counter: 'zarith',
    gas_limit: 'zarith',
    storage_limit: 'zarith',
    balance: 'zarith',
    delegate: 'delegate',
    script: 'script',
};

export const BallotSchema = {
    source: 'pkh',
    period: 'int32',
    proposal: 'proposal',
    ballot: 'ballotStmt',
};

export const EndorsementSchema = {
    level: 'int32',
};

export const SeedNonceRevelationSchema = {
    level: 'int32',
    nonce: 'raw',
};

export const ProposalsSchema = {
    source: 'pkh',
    period: 'int32',
    proposals: 'proposalArr',
};

import contents = codec.Proto010_PtGRANAD__operation__unsigned.NSoperation__alpha__contents;

export const operation_schema_table = {
    [contents.contents_tag.Activate_account]: ActivationSchema,
    [contents.contents_tag.Ballot]: BallotSchema,
    [contents.contents_tag.Delegation]: DelegationSchema,
    [contents.contents_tag.Endorsement]: EndorsementSchema,
    [contents.contents_tag.Origination]: OriginationSchema,
    [contents.contents_tag.Proposals]: ProposalsSchema,
    [contents.contents_tag.Reveal]: RevealSchema,
    [contents.contents_tag.Seed_nonce_revelation]: SeedNonceRevelationSchema,
    [contents.contents_tag.Transaction]: TransactionSchema,
    /*
    [contents.contents_tag.Double_baking_evidence]: undefined,
    [contents.contents_tag.Double_endorsement_evidence]: undefined,
    [contents.contents_tag.Endorsement_with_slot]: undefined,
    [contents.contents_tag.Failing_noop]: undefined,
    */
}

export enum CODECTYPE {
    SECRET = 'secret',
    RAW = 'raw',
    TZ1 = 'tz1',
    BRANCH = 'branch',
    ZARITH = 'zarith',
    PUBLIC_KEY = 'public_key',
    PKH = 'pkh',
    DELEGATE = 'delegate',
    SCRIPT = 'script',
    BALLOT_STATEMENT = 'ballotStmt',
    PROPOSAL = 'proposal',
    PROPOSAL_ARR = 'proposalArr',
    INT32 = 'int32',
    PARAMETERS = 'parameters',
    ADDRESS = 'address',
    OPERATION = 'operation',
    OP_ACTIVATE_ACCOUNT = 'activate_account',
    OP_DELEGATION = 'delegation',
    OP_TRANSACTION = 'transaction',
    OP_ORIGINATION = 'origination',
    OP_BALLOT = 'ballot',
    OP_ENDORSEMENT = 'endorsement',
    OP_SEED_NONCE_REVELATION = 'seed_nonce_revelation',
    OP_REVEAL = 'reveal',
    OP_PROPOSALS = 'proposals',
    MANAGER = 'manager',
}

export const schema_converter = (converters: { [key: string]: (val: {}) => unknown }) => (schema: {
    [key: string]: string | string[];
}) => <T extends { [key: string]: any }>(value: T): unknown => {
    const keys = Object.keys(schema);
    return keys.reduce((prev, key) => {
        const valueToEncode = schema[key];

        if (Array.isArray(valueToEncode)) {
            const converter = converters[valueToEncode[0]];

            const values = value[key];

            if (!Array.isArray(values)) {
                throw new Error(`Exepected value to be Array ${JSON.stringify(values)}`);
            }
            const converted = values.map(converter);

            return { ...prev, [key]: converted };
        } else {
            const converter = converters[valueToEncode];
            const converted = converter(value[key]);
            return { ...prev, [key]: converted };
        }
    }, {});
};