export interface Base58Consts {
    prefix_len: number,
    payload_len: number,
}

export const BRANCH = {
    prefix_len: 2,
    payload_len: 32
} as const satisfies Base58Consts;

export const PKH = {
    prefix_len: 3,
    payload_len: 20,
} as const satisfies Base58Consts;

export const PK = {
    prefix_len: 4,
    payload_len: 32,
} as const satisfies Base58Consts;

export const CONTRACT = {
    prefix_len: 3,
    payload_len: 20,
} as const satisfies Base58Consts;

export const PROPOSAL = {
    prefix_len: 2,
    payload_len: 32,
} as const satisfies Base58Consts;