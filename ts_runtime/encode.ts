import { Target } from './target';
import { HexString } from './util/hex';

export type OutputBytes = number[]

/**
 * Interface for methods related to data-encoding compatible serialization
 * of arbitrary custom types.
 */
export interface Encode {
    /**
     * Raw serialization method that may be more expensive than preallocation
     * and buffer-filling.
     *
     * @returns an array consisting of the serialized byte-values of the receiver.
     */
    encode(): OutputBytes;

    /**
     * Appends serialized bytes of the receiver to an existing object
     * that implements the Target interface.
     *
     * @param tgt Target object to append serialized bytes to
     */
    writeTarget<T extends Target>(tgt: T): number;
}

/**
 * Type alias for binary serialization functions
 */
export type Encoder<T> = (value: T) => OutputBytes;

export type Writer<T> = (value: T, buf: string) => string;

export const liftEncoder = <T>(enc: Encoder<T>): Writer<T> => (value, buf) => {
    const bytes = enc(value);
    const hex = new HexString(bytes);
    return hex.appendHex(buf);
}