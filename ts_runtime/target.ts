export interface Stateful {
    toByteCount: () => number;
}

export interface Buffered {
    toBytes: () => number[];
}
export abstract class Target {
    abstract writeOne(byte: number): 1;

    public writeArray(bytes: readonly number[]): number {
        bytes.forEach(byte => {
            this.writeOne(byte);
        }, this);
        return bytes.length;
    }

    public writeAll(iter: Iterable<number>): number {
        var count: number = 0;
        for (const byte of iter) {
            count += this.writeOne(byte);
        }
        return count;
    }
}

export class Sink extends Target {
    writeOne(byte: number): 1 {
        return 1;
    }

    public writeArray(bytes: readonly number[]): number {
        return bytes.length;
    }

    public writeAll(iter: Iterable<number>): number {
        return [...iter].length
    }
}

export class Estimator extends Sink implements Stateful {
    #counter: number = 0;

    writeOne(byte: number): 1 {
        this.#counter += 1;
        return 1
    }

    public writeArray(bytes: readonly number[]): number {
        this.#counter += bytes.length;
        return bytes.length;
    }

    public writeAll(iter: Iterable<number>): number {
        const len: number = [...iter].length;
        this.#counter += len;
        return len
    }

    public toByteCount(): number {
        return this.#counter
    }
}

export class ByteTarget extends Target implements Buffered, Stateful {
    #contents: number[];

    constructor() {
        super();
        this.#contents = new Array();
    }

    toBytes(): number[] {
        return this.#contents.slice();
    }

    toByteCount(): number {
        return this.#contents.length;
    }

    writeOne(byte: number): 1 {
        this.#contents.push(byte);
        return 1;
    }

    public writeArray(bytes: readonly number[]): number {
        const lenAdded = bytes.length;
        this.#contents.push(...bytes);
        return lenAdded;
    }

    public writeAll(iter: Iterable<number>): number {
        const len = this.#contents.length;
        const newElems: number[] = [...iter];
        const newLen = this.#contents.push(...newElems);
        if (newLen !== newElems.length + len) {
            throw new Error(`ByteTarget.writeAll: added ${newElems.length} elements but length increased from ${len} to ${newLen}`)
        }
        return newLen - len;
    }
}