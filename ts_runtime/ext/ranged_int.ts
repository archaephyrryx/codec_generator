import { NumericBounds } from '../bounds';
import * as integerKinds from '../integer/kinds';

export type RangedIntKind = integerKinds.IntKindNative;

export type RangedIntExt<K extends RangedIntKind = RangedIntKind, Min extends number = number, Max extends number = number> = {
    readonly intKind: K;
    readonly bounds: NumericBounds<Min, Max>;
};