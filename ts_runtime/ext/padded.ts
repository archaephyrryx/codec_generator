type PaddedExt<N extends number = number> = {
    readonly padding: N;
};

export default PaddedExt;