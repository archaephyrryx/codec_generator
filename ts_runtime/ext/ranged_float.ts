import { NumericBounds } from '../bounds';

type RangedFloatExt<Min extends number = number, Max extends number = number> = {
    readonly bounds: NumericBounds<Min, Max>;
};

export default RangedFloatExt;
