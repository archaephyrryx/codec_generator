type FixedLenExt<N extends number = number> = { readonly len: N; }

export default FixedLenExt;
