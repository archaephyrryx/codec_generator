type RecordExt<T extends Record<string, unknown> = Record<string, unknown>> = {
    order: Array<keyof T>,
}

export default RecordExt;