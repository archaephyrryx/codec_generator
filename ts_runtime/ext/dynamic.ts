import width from '../core/width.type';

type DynamicExt<W extends width = width> = {
    prefixWidth: W
}

export default DynamicExt;
