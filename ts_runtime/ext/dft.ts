type DefaultExt<Hex extends string = string> = {
    readonly dftHex: Hex;
}
export default DefaultExt;
