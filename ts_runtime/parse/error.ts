import { HexString } from '../util/hex';
import { Result } from '../core/result';

// FIXME[epic=scaffolding] - fill in the gaps in WindowError
export class WindowError extends Error {
    static CloseWithResidue(current: number, target: number): WindowError {
        throw new WindowError(`Cannot close: offset ${current} has not reached target ${target}.`);
    }
    public static ConsumeBeyondLimit(offset: number, increment: number, limit: number): WindowError {
        return new WindowError(`Cannot consume: requested ${increment} bytes of ${limit - offset} possible.`);
    }
}

export class TokenError extends Error {
    public static InvalidBoolean(byte: number): TokenError {
        return new TokenError(`Byte invalid as boolean: 0x${byte.toString(16).padStart(2, '0')}`);
    }

    public static NonTerminating(slice: Uint8Array): TokenError {
        return new TokenError(`Termination predicate not met by any byte in current context: '${new HexString(slice).toHex()}'.`);
    }
}

export type ParseError = Error | WindowError;

export type ParseResult<T> = Result<T, ParseError>;