import { assertNat, Nat } from '../core/safe';
import { WindowError } from './error';
import { ContextOffset } from './internal';
import { Parser, TypedParser } from '../parse';
import { HexString } from '../util/hex';


export type ByteBuffer = Buffer;

export class ByteParser extends Parser implements TypedParser {
    protected readonly _buffer: ByteBuffer;
    protected readonly _offset: ContextOffset;

    protected constructor(_buffer: ByteBuffer) {
        super();
        this._buffer = _buffer;
        this._offset = new ContextOffset(_buffer);
    }

    public static fromBuffer(buffer: Uint8Array | readonly number[]): ByteParser {
        return new this(Buffer.from(buffer));
    }

    /**
     *
     *
     * @deprecated Use {@link ByteParser.fromStringHex} instead
     * @param raw String to convert to a byte-buffer
     * @returns Parser over the converted buffer
     */
    public static fromString(raw: string): ByteParser {
        return ByteParser.fromStringHex(raw);
    }

    /**
     * Parses a hex-encoded string and returns a ByteParser object over
     * the resulting buffer.
     *
     * @param raw Hex-string to parse into a byte-buffer
     * @returns ByteParser object over the converted buffer
     */
    public static fromStringHex(raw: string): ByteParser {
        const buffer = HexString.fromStringRaw(raw);
        return ByteParser.fromBuffer(buffer);
    }

    public consumeByte(): number {
        const [ix, adv] = this._offset.advance(1 as Nat);
        if (adv) {
            return this._buffer[ix];
        } else {
            throw WindowError.ConsumeBeyondLimit(ix, 1, this.viewLen);
        }
    }

    public consume(nbytes: number): Uint8Array {
        assertNat(nbytes);
        const [ix, adv] = this._offset.advance(nbytes);
        if (adv) {
            return new Uint8Array([...this._buffer.subarray(ix, ix + nbytes)]);
        } else {
            throw WindowError.ConsumeBeyondLimit(ix, nbytes, this.viewLen);
        }
    }

    public setFit(n: number): void {
        assertNat(n);
        return this._offset.setFit(n);
    }

    public shrinkFit(delta: number): void {
        assertNat(delta);
        return this._offset.shrinkFit(delta);
    }

    public testTarget(): boolean {
        return this._offset.testTarget();
    }

    public enforceTarget(): void {
        return this._offset.enforceTarget();
    }
}
