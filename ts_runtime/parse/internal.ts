import { forceNat, Nat } from '../core/safe';
import { WindowError } from './error';
import { Stack } from '../util/stack';

/**
 * Core methods required for a type that tracks a stateful index into a static buffer.
 */
export interface IndexTracker {
    get index(): Nat;
    get absoluteLimit(): Nat;
    get limit(): Nat;
    advance(n: Nat): [Nat, boolean];
}
/**
 * Determines if the requested stack-push operation is legal by the nesting invariant:
 * all elements on the stack must be less than or equal to the element below them.
 *
 * @param innermost value at the current top of the stack
 * @param request value to push to the top of the stack
 * @throws {@link WindowError} If nesting invariant is violated by the request
 */
const detectError = (innermost: Nat | undefined, request: Nat): void => {
    if (typeof innermost !== 'undefined' && request > innermost) {
        throw new WindowError(`Cannot open new context-window: request (${request}) exceeds current value of ${innermost}`);
    }
    return;
};

class FrameStack extends Stack<Nat> {
    /**
     * Attempts to push a new frame to the stack, determining whether it is valid using the provided function
     * @param item Element to be pushed to the stack if valid
     * @param validate Function that throws a WindowError if item is invalid compared to the previous top of the stack
     */
    protected pushValidated(item: Nat, validate: (top: Nat | undefined, val: Nat) => void): void {
        try {
            validate(this.peek(), item);
            this.push(item);
        } catch (e: unknown) {
            // FIXME[epic=boilerplate] - simplify or distinguish error-handling
            if (e instanceof WindowError) {
                throw e;
            } else {
                throw e;
            }
        }
    }

    /**
     * Pushes a new target-offset to the top of the stack, provided it is between 0 and the current top of the stack
     *
     * The caller is responsible for ensuring that the new target is greater than or equal to the current offset being
     * tracked.
     *
     * @param target New target to push to the top of the stack
     */
    public pushFrame(target: Nat): void {
        this.pushValidated(target, detectError);
    }
}
/**
 * Offset tracker for static-buffer parser implementations
 */
export class ContextOffset implements IndexTracker {
    protected readonly abs: Nat;
    protected frames: FrameStack;
    protected current: Nat;


    public get index(): Nat {
        return this.current;
    }

    public get absoluteLimit(): Nat {
        return this.abs;
    }

    public get limit(): Nat {
        return this.frames.peekOr(this.abs);
    }

    public constructor(buflen: { length: number; } | number) {
        this.abs = forceNat(typeof buflen === 'number' ? buflen : buflen.length);
        this.frames = new FrameStack();
        this.current = 0 as Nat;
    }

    /**
     * Pushes a new context-frame to the top of the frame-stack, allowing
     * a specified number of bytes to be read before it must be enforced
     * and closed.
     *
     * @param width Byte-width of the new context-frame
     *
     * @throws {@link WindowError}
     * If `width` overruns the buffer or violates the nesting condition
     */
    public setFit(width: Nat): void {
        const newTarget = forceNat(this.current + width);
        if (newTarget > this.abs) {
            const bytesLeft = this.abs - width;
            const msg = `Cannot open ${width}-byte context window`;
            const reason = (bytesLeft == 0 ? 'parse-buffer has been fully consumed' : `only ${bytesLeft} bytes remain`);
            throw new WindowError(`${msg}: ${reason}`);
        } else {
            return this.frames.pushFrame(newTarget);
        }
    }


    public shrinkFit(delta: Nat): void {
        const lim = this.limit;
        if (this.current + delta > lim) {
            throw new WindowError(`Cannot shrink by ${delta} when only ${lim - this.current} bytes remain`);
        }
        const newTarget = forceNat(lim - delta);
        return this.frames.pushFrame(newTarget);
    }

    /**
     * Tests for complete consumption of current context-window.
     *
     * Returns `false` if there are no context windows.
     *
     * @returns `true` if the current window has been fully consumed, `false` otherwise
     * @throws {@link WindowError} If offset overflowed the target
     */
    public testTarget(): boolean {
        const topmost = this.frames.peek();
        if (typeof topmost === 'undefined') {
            return false;
        } else {
            const ret = topmost == this.current;
            if (topmost < this.current) {
                throw new WindowError('Offset overflow!');
            }
            return ret;
        }
    }

    public enforceTarget(): void {
        const target = this.frames.peek();
        if (typeof target === 'undefined') {
            throw new WindowError('Cannot close non-existent window!');
        } else {
            if (target == this.current) {
                this.frames.pop();
                return;
            } else if (target < this.current) {
                throw new WindowError('Offset overflow!');
            } else {
                throw WindowError.CloseWithResidue(this.current, target);
            }
        }
    }

    /**
     * Attempts to advance the current offset by a specified increment,
     * returning the previous offset, along with a boolean indicator of
     * the validity of the attempted operation.
     *
     * @param n Number of indices to advance the offset by
     * @returns Original offset and boolean flag (`true` when operation was performed)
     */
    public advance(n: Nat): [Nat, boolean] {
        const curValue = this.current;
        const maxValue = this.limit;
        const isValid = n <= maxValue - curValue;
        if (isValid) {
            this.current = curValue + n as Nat;
        }
        return [curValue, isValid];
    }
}
