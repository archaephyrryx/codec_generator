import { Encode } from './encode';

export interface Estimable<N extends number = number> {
    get encodeLength(): N;
}

export type ConstantLength<N extends number> = Estimable<N> & { __constantLength__: void };

export type Codec = Encode & Estimable