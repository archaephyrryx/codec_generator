import { Box, unboxed } from '../core/box';
import { Codec, ConstantLength } from '../codec';
import { mkSafe } from '../core/safe';
import { floatToBytes } from '../core/ops';
import { unwrap } from '../core/result';
import { Decoder, DecoderProxy, liftDecoder, Reader } from '../decode';
import { Encode, Encoder, liftEncoder, OutputBytes, Writer } from '../encode';
import { double } from './type';
import { Parser } from '../parse';
import { Target } from '../target';
import { PrimWidth } from '../core/consts';

export class Double extends Box<double> implements Codec, ConstantLength<PrimWidth.Double> {
    readonly __constantLength__: void = undefined;

    constructor(value: number) {
        mkSafe(value);
        super(value);
    }

    get constLen(): PrimWidth.Double {
        return 8;
    }

    get encodeLength() {
        return this.constLen;
    }

    static decode: Decoder<Double> = (p: Parser) => {
        return new Double(unwrap(p.takeDouble()));
    };

    encode(): OutputBytes {
        return floatToBytes(this.value);
    }

    static encoder(other: Double): OutputBytes {
        return other.encode();
    }

    static promote(value: double): Double {
        return new this(value);
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }
}

export const double_encoder: Encoder<double> = (value: double) => Double.promote(value).encode();

export const double_decoder: Decoder<double> = unboxed(Double.decode);

export const double_reader: Reader<double> = liftDecoder(double_decoder);

export const double_writer: Writer<double> = liftEncoder(double_encoder);