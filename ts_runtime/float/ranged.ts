import { clampWith, NumericBounds } from '../bounds';
import { Box } from '../core/box';
import { Codec, ConstantLength } from '../codec';
import Ext from '../ext/ranged_float';
import { floatToBytes } from '../core/ops';
import { unwrap } from '../core/result';
import { DecoderProxy } from '../decode';
import { Encode, OutputBytes } from '../encode';
import { double } from './type';
import { Parser } from '../parse';
import { Target } from '../target';
import { PrimWidth } from '../core/consts';

export class RangedDouble<Min extends number = number, Max extends number = number> extends Box<double> implements Codec {
    protected readonly min: Min;
    protected readonly max: Max;

    protected readonly bounds: NumericBounds<number>;

    public static readonly decode: DecoderProxy<RangedDouble, Ext> = <Min extends number, Max extends number>(proxy: Ext<Min, Max>) => (p: Parser): RangedDouble<Min, Max> => {
        const raw = unwrap(p.takeDouble());
        return new RangedDouble(raw, proxy.bounds);
    };

    constructor(value: number, { min, max }: NumericBounds<Min, Max>) {
        const bounds = { min, max };
        validateBounds(bounds);
        super(clampWith(value, bounds, { strict: true, domain: 'float' }));

        this.bounds = bounds;
        this.min = min;
        this.max = max;
    }

    get constLen(): PrimWidth.Double {
        return 8;
    }

    get encodeLength(): number {
        return this.constLen;
    }

    __components(): Encode[] {
        return [this];
    }

    encode(): OutputBytes {
        validateBounds(this.bounds);
        filterSafe(this.value);
        if (this.value < this.min || this.value > this.max) {
            throw new Error(`Invalid instance of RangedDouble: ${this.value} not in range [${this.min} , ${this.max}]`);
        }
        return floatToBytes(this.value);
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }
}

function filterSafe(...numbers: number[]): true {
    for (const n of numbers) {
        if (Number.isNaN(n) || !Number.isFinite(n)) {
            throw new Error(`mkSafe: value ${n} is either NaN or infinite`);
        }
    }
    return true;
}

function validateBounds<Min extends number, Max extends number>({ min, max }: NumericBounds<Min, Max>): true {
    filterSafe(min, max);
    if (min > max) {
        throw new RangeError(`Incoherent parameters: RangedDouble Min (${min}) > Max (${max})`);
    } else {
        return true;
    }
}
