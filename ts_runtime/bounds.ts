type Numeric = number | bigint

export interface NumericBounds<T1 extends Numeric = number, T2 extends Numeric = T1> {
    readonly min: T1;
    readonly max: T2;
}

type HomogenousBounds<T extends Numeric = Numeric> = Extract<NumericBounds<number> | NumericBounds<bigint>, NumericBounds<T>>

function assertHomogenous<T extends Numeric>(bounds: NumericBounds<T, Numeric>): asserts bounds is HomogenousBounds<T> {
    if (typeof bounds.min !== typeof bounds.max) {
        throw new Error(`Bounds must be either both bigint, or both number`);
    }
    return;
}

export function raiseBounds(bounds: HomogenousBounds<number>): HomogenousBounds<bigint> {
    return { min: BigInt(bounds.min), max: BigInt(bounds.max) };
}

export type Domain = 'integer' | 'float';

export type Mode = { strict: boolean; domain: Domain }

export const defaultMode: Mode = { strict: false, domain: 'integer' };

/**
 * Clamp a number to the range specified by the provided bounds,
 * returning the closest in-range integral value.
 *
 * @param int Integer to clamp
 * @param bounds Bounds to restrict the returned value to
 * @returns The closest value to `int` that is consistent with `bounds`
 */
export function clampWith(int: number, bounds: NumericBounds<number>): number;

/**
 * Clamp a number to the range specified by the provided bounds,
 * returning the closest in-range value.
 *
 * Unless overridden by `mode`, this clamping will force `n` to be integral.
 * (Passing in a mode with the key `{ domain: 'float' }` will prevent this.)
 *
 * Unless overridden by `mode`, this clamping is non-strict, and a non-conformant
 * value will *not* result in an exception being thrown.
 * (Passing in `{ strict: true }` will enable these exceptions).
 *
 * @param n Numeric value to clamp
 * @param bounds Bounds to restrict the returned value to
 * @param mode Overrides for {@link defaultMode}: `strict` controls
 */
export function clampWith(n: number, bounds: NumericBounds<number>, mode: Partial<Mode>): number;

export function clampWith(n: bigint, bounds: NumericBounds<bigint>): bigint;
/**
 * Clamp a bigint to the range specified by the provided bounds,
 * returning the closest in-range value.
 *
 * Unless mode is `{ strict: true }`, this clamping is non-strict, and a non-conformant
 * value will *not* result in an exception being thrown.
 * (Passing in `{ strict: true}` will enable these exceptions)
 *
 * @param n Numeric value to clamp
 * @param bounds Bounds to restrict the returned value to
 * @param mode Override to determine strictness level
 */
export function clampWith(n: bigint, bounds: NumericBounds<bigint>, mode: { strict: boolean }): bigint;

/**
 * Restricts a numeric value to the specified integer range
 *
 * In strict-mode (`strict == true`), will throw errors instead of
 * returning in-range values when argument `n` is not an in-range integer.
 *
 * @param n Numeric value to clamp
 * @param bounds placeholder object to infer bounds from
 * @param mode Optional overrides to apply to {@link defaultMode}
 * @returns Closest in-range value to {n}, including {n} itself if it is already in-range
 *
 * @throws {@link RangeError}
 * If value is non-integral or out-of-range in strict-mode
 *
 * @throws {@link Error}
 * If the provided bounds are inverted or non-integral
 *
 * @throws {@link Error}
 * If the provided value is NaN
 */
export function clampWith<T extends Numeric>(n: T, bounds: HomogenousBounds<T>, mode: Partial<Mode> = {}): T {
    const { strict, domain } = { ...defaultMode, ...mode };
    checkBounds(bounds);

    let result: typeof n;
    if (typeof n === 'number' && domain === 'integer') {
        result = Math.trunc(n) as T;
    } else {
        result = n;
    }

    if (Number.isNaN(result)) {
        throw new Error(`clampWith: NaN value cannot be clamped`);
    }
    if (result < bounds.min) {
        if (strict) {
            throw new RangeError(`clampWith[strict]: value ${result} less than minimum ${bounds.min}`)
        }
        return bounds.min;
    } else if (result > bounds.max) {
        if (strict) {
            throw new RangeError(`clampWith[strict]: value ${result} greater than maximum ${bounds.max}`)
        }
        return bounds.max
    } else {
        if (strict && domain === 'integer' && result !== n) {
            throw new RangeError(`clampWith[strict, integer]: value ${n} (~> ${result}) in-range, but not integral`)
        }
        return result;
    }
}

/**
 * Performs runtime validation on a {@link NumericBounds} object,
 * throwing an error if they are inverted or non-integral, and
 * otherwise returning a literal `true`-value.
 *
 * @param param0 Bounds to validate
 * @returns `true`
 *
 * @throws {@link RangeError}
 * If `min > max`, or if either bound is non-integral
 */
function checkBounds(bounds: HomogenousBounds, domain: Domain = 'integer'): true {
    const { min, max } = bounds;
    if (min > max) {
        throw new RangeError(`Cannot clamp between inverted bounds: min (${min}) > max (${max})`);
    } else if (domain === 'integer' && (typeof min === 'number' && min !== Math.trunc(min) || (typeof max === 'number' && max !== Math.trunc(max)))) {
        throw new RangeError(`Cannot clamp between non-integer bounds: [${min},${max}]`)
    } else {
        return true;
    }
}