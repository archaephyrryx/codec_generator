import { Parser } from './parse';
import { ByteParser } from './parse/byteparser';
import { Codec } from './codec';
import { AnyExt, NoExt } from './ext';

type InputHex = string
type InputBytes = readonly number[]
type InputByteArray = Uint8Array

/**
 * Union over most useful types that one might wish to decode
 */
export type Input = InputHex | InputBytes | InputByteArray | Parser;

const isBytes: (arg: Input) => arg is InputBytes = Array.isArray;


/**
 * Value-level coercion of {@link Input} values to {@link Parser} types.
 *
 * @param inp Arbitrary parsable Input value
 * @returns Parser object generated from `inp`
 */
export function toParser(inp: Input): Parser {
    if (inp instanceof Parser) {
        return inp;
    } else if (typeof inp === 'string') {
        return ByteParser.fromStringHex(inp);
    } else if (isBytes(inp)) {
        return ByteParser.fromBuffer(new Uint8Array(inp));
    } else {
        return ByteParser.fromBuffer(inp);
    }
}

/**
 * Function that unpacks `T`-values from valid {@link Input}
 */
export type Reader<T> = (p: Input) => T;

/**
 * Function that extracts a `T` value from a {@link Parser} object,
 * mutating it in the process
 */
export type Decoder<T> = (p: Parser) => T;

export type ProxyBase<X> = {
    dec: Decoder<X>;
};

export type ProxyExt<Ext extends AnyExt, X extends Codec = Codec> = ProxyBase<X> & Ext;

/**
 * Type alias for abstract decoders that must be reified by passing
 * in a proxy decoder for a constituent element.
 */
export type Proxy<F, Ext extends AnyExt = NoExt> = <X extends Codec>(proxy: ProxyExt<Ext, X>) => F;

export type PseudoProxy<F, Ext extends AnyExt = NoExt> = (proxy: Ext) => F;

export type DecoderProxy<T, Ext extends AnyExt = NoExt>
    = Decoder<T>
    | Proxy<Decoder<T>, Ext>
    | PseudoProxy<Decoder<T>, Ext>

/**
 * Adapter that lifts a {@link Decoder<T>} into a {@link Reader<T>}
 *
 * @param f Generic Decoder function
 * @returns Equivalent Reader function
 */
export const liftDecoder = <T>(f: Decoder<T>): Reader<T> => (inp) => f(toParser(inp));

/**
 * Adapter that lifts a {@link Decoder<T>} to a {@link Reader<T>} within a {@link Proxy}
 *
 * @param f Proxy-based generic decoder function
 * @returns Equivalent Proxy-based generic reader function
 */
export const liftProxyDecoder = <U, E extends AnyExt>(f: Proxy<Decoder<U>, E>): Proxy<Reader<U>, E> => (proxy) => liftDecoder(f(proxy));

/**
 * Adapter that lifts a {@link Decoder<T>} to a {@link Reader<T>} within a {@link PseudoProxy}
 *
 * @param f PseudoProxy-based parametric decoder function
 * @returns Equivalent PseudoProxy-based generic reader function
 */
export const liftPseudoProxyDecoder = <U, E extends AnyExt>(f: PseudoProxy<Decoder<U>, E>): PseudoProxy<Reader<U>, E> => (proxy) => liftDecoder(f(proxy));