import { Codec } from "../../codec";
import { Box } from "../../core/box";
import { MemoCell } from "../../core/memo";
import { Width, width_decoder } from "../../core/width";
import width from "../../core/width.type";
import { Decoder, liftDecoder } from "../../decode";
import { liftEncoder, OutputBytes } from "../../encode";
import { Parser } from "../../parse";
import { Target } from "../../target";
import { None, OptionNone, PrimOption, isSome, option } from "../opt/option.type";
import { sequence } from "./sequence.type";

type ShouldBreak = boolean;

type BreakCheck = <T>(arr: T[]) => ShouldBreak;

/**
 * Function that creates a closure that pre-consumes a
 * bounded-sequence length encoding if required, and
 * returns a closure that will return true if an array-filling
 * loop should break early.
 *
 * @param encoding list-length encoding type for the associated construction
 * @param p parser object that will be used to fill the array
 * @returns A {@link BreakCheck} closure for the Array-filling loop
 */
const shortCircuit = (encoding: option<width>, p: Parser): BreakCheck => {
    if (isSome(encoding)) {
        const exactCount = width_decoder(encoding.contents)(p);
        return (arr) => (arr.length >= exactCount);
    } else {
        return (_) => false;
    }
}

export class SequenceBounded<T extends Codec, N extends number, E extends option<width> = OptionNone>
    extends Box<sequence<T>>
    implements Codec {

    readonly #encodeLength: MemoCell<number>;
    readonly maxLength: N;
    readonly encoding: E;

    public constructor(value: readonly T[], maxLength: N, encoding: E) {
        if (value.length > maxLength) {
            throw new Error(
                `Cannot construct sequence: length ${value.length} exceeds '${maxLength}'`
            );
        }
        super(value);
        this.maxLength = maxLength;
        this.encoding = encoding;
        this.#encodeLength = MemoCell.Empty();
    }

    get encodeLength(): number {
        // STUB - add in logic for dataencoding v0.7 length prefixes
        const value = this.value;
        const encoding = this.encoding;
        return this.#encodeLength.compute(() => {
            const payloadLength = value.reduce((acc, elem) => acc + elem.encodeLength, 0);
            const prefixLength = PrimOption.getMapped(encoding, 0, (w: width) => Width.prefixBytes(w, payloadLength));
            return payloadLength + prefixLength;
        });
    }

    static decode<X extends Codec, N extends number, E extends option<width>>(dec: Decoder<X>, maxLength: N, encoding: E): Decoder<SequenceBounded<X, N, E>> {
        return (p) => {
            const array = new Array(maxLength);
            const shouldBreak = shortCircuit(encoding, p);
            while (p.remainder > 0) {
                array.push(dec(p));
                if (shouldBreak(array)) {
                    break;
                }
            }

            return new SequenceBounded(array, maxLength, encoding);
        };
    }

    encode(): OutputBytes {
        return this.value.flatMap((x) => x.encode());
    }

    static encoder<X extends Codec, N extends number, E extends option<width>>(value: SequenceBounded<X, N, E>): OutputBytes {
        return value.encode();
    }

    writeTarget<T extends Target>(tgt: T): number {
        return this.value.reduce((acc, elem) => acc + elem.writeTarget(tgt), 0);
    }
}

export const sequenceBounded_encoder = SequenceBounded.encoder;

export const sequenceBounded_decoder = SequenceBounded.decode;

export const sequenceBounded_reader = <T extends Codec, N extends number>(dec: Decoder<T>, maxLength: N, encoding: option<width> = None) => liftDecoder(sequenceBounded_decoder(dec, maxLength, encoding));

export const sequenceBounded_writer = liftEncoder(sequenceBounded_encoder);
