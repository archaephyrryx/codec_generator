import { Codec } from '../../codec';
import { Box } from '../../core/box';
import { MemoCell } from '../../core/memo';
import { Decoder, liftDecoder } from '../../decode';
import { liftEncoder, OutputBytes } from '../../encode';
import { Target } from '../../target';
import { sequence } from './sequence.type';

export class SequenceFixed<T extends Codec, N extends number> extends Box<sequence<T>> implements Codec {
    readonly #encodeLength: MemoCell<number>;

    constructor(value: readonly T[], public readonly length: N) {
        if (value.length !== length) {
            throw new Error(`Cannot construct sequence: length ${value.length} not equal to '${length}'`);
        }
        super(value);
        this.#encodeLength = MemoCell.Empty();
    }

    get encodeLength(): number {
        const value = this.value;
        return this.#encodeLength.compute(() => {
            return value.reduce((acc, elem) => acc + elem.encodeLength, 0);
        });
    }

    static readonly decode = <X extends Codec, N extends number>(dec: Decoder<X>, length: N): Decoder<SequenceFixed<X, N>> => (p) => {
        const array = new Array<X>(length);
        while (p.remainder > 0 && array.length < length) {
            array.push(dec(p));
        }

        return new SequenceFixed(array, length);
    };

    encode(): OutputBytes {
        return this.value.flatMap(x => x.encode());
    }

    static encoder<X extends Codec, N extends number>(value: SequenceFixed<X, N>): OutputBytes {
        return value.encode();
    }

    writeTarget<T extends Target>(tgt: T): number {
        const buf = this.encode();
        return tgt.writeArray(buf);
    }
}

export const sequenceFixed_encoder = SequenceFixed.encoder;

export const sequenceFixed_decoder = SequenceFixed.decode;

export const sequenceFixed_reader = <X extends Codec, N extends number>(dec: Decoder<X>, length: N) => liftDecoder(sequenceFixed_decoder(dec, length));

export const sequenceFixed_writer = liftEncoder(sequenceFixed_encoder);