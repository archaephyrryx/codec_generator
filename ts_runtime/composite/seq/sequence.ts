import { Codec } from '../../codec';
import { Box, onValue, unboxed } from '../../core/box';
import { MemoCell } from '../../core/memo';
import { Decoder, liftDecoder } from '../../decode';
import { Encoder, liftEncoder, OutputBytes } from '../../encode';
import { Target } from '../../target';
import { sequence } from './sequence.type';

export class Sequence<T extends Codec> extends Box<sequence<T>> implements Codec {
    readonly #encodeLength: MemoCell<number>;

    public constructor(value: readonly T[]) {
        super(value);
        this.#encodeLength = MemoCell.Empty();
    }

    get encodeLength(): number {
        const value = this.value;
        return this.#encodeLength.compute(() => {
            return value.reduce((acc, elem) => acc + elem.encodeLength, 0);
        })
    }

    static promote<T extends Codec>(value: sequence<T>): Sequence<T> {
        return new Sequence(value);
    }

    static readonly decode = <X extends Codec>(dec: Decoder<X>): Decoder<Sequence<X>> => (p) => {
        const array = [];
        while (p.remainder > 0) {
            array.push(dec(p));
        }
        return new Sequence(array);
    }

    encode(): OutputBytes {
        return this.value.flatMap(x => x.encode());
    }

    static encoder<X extends Codec>(value: sequence<X> | Sequence<X>): OutputBytes {
        return onValue((x: sequence<X>) => x.flatMap(y => y.encode()))(value);
    }

    writeTarget<T extends Target>(tgt: T): number {
        return this.value.reduce((acc, elem) => acc + elem.writeTarget(tgt), 0);
    }
}

export const sequence_encoder: Encoder<sequence<Codec> | Sequence<Codec>> = Sequence.encoder;

export const sequence_decoder = <T extends Codec>(dec: Decoder<T>): Decoder<sequence<T>> => unboxed(Sequence.decode(dec));

export const sequence_reader = <T extends Codec>(dec: Decoder<T>) => liftDecoder(sequence_decoder(dec));

export const sequence_writer = liftEncoder(sequence_encoder);