import { Codec } from '../codec';
import { Box } from '../core/box';
import { forceNat } from '../core/safe';
import { Decoder } from '../decode';
import { OutputBytes } from '../encode';
import { Target } from '../target';

export class VPadded<T extends Codec, N extends number> extends Box<T> implements Codec {
    static decode<X extends Codec, N extends number>(dec: Decoder<X>, _padding: N): Decoder<VPadded<X, N>> {
        return p => {
            p.shrinkFit(_padding);
            const value: X = dec(p);
            p.enforceTarget();
            return new VPadded(value, _padding);
        };
    }

    constructor(value: T, readonly _padding: N) {
        super(value);
        this._padding = forceNat(_padding);
    }

    get encodeLength(): number {
        return this.value.encodeLength;
    }

    encode(): OutputBytes {
        return this.value.encode();
    }

    static encoder<X extends Codec, N extends number>(value: VPadded<X, N>): OutputBytes {
        return value.encode();
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.value.encode());
    }
}

export type GenericVPadded = VPadded<Codec, number>