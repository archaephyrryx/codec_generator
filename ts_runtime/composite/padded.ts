import { Codec, ConstantLength } from '../codec';
import { Box } from '../core/box';
import { forceNat } from '../core/safe';
import { Decoder, liftDecoder } from '../decode';
import { liftEncoder, OutputBytes } from '../encode';
import { Target } from '../target';
import { mem_is0 } from '../util/arrops';

const validator = mem_is0;
// const validator = (_: Uint8Array) => true;

// NOTE[epic=obstacle] - We want to be able to enforce constant-length payload, but cannot as-is
export class Padded<T extends Codec, N extends number> extends Box<T> implements Codec {
    static decode<X extends Codec, N extends number>(dec: Decoder<X>, padding: N): Decoder<Padded<X, N>> {
        return p => {
            const value = dec(p);
            /* NOTE[epic=design] - Currently set to validate padding, can be suppressed at will */
            p.skipPadding(padding, validator);
            return new Padded(value, padding);
        }
    };

    constructor(value: T, readonly padding: N) {
        super(value);
        this.padding = forceNat(padding);
    }

    get encodeLength() {
        return this.value.encodeLength + this.padding;
    }

    encode(): OutputBytes {
        return [...this.value.encode(), ...new Array(this.padding).fill(0x00)];
    }

    static encoder<T extends Codec, N extends number>(other: Padded<T, N>): OutputBytes {
        return other.encode();
    }

    static promoter<T extends Codec, N extends number>(padding: N) {
        return (value: T) => new Padded(value, padding);
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.value.encode()) + tgt.writeArray(new Array(this.padding).fill(0x00));
    }
}

export type GenericPadded = Padded<Codec, number>

export const padded_encoder = Padded.encoder;

export const padded_decoder = Padded.decode;

export const padded_reader = <T extends Codec, N extends number>(dec: Decoder<T>, padding: N) => liftDecoder(padded_decoder(dec, padding));

export const padded_writer = liftEncoder(padded_encoder);
