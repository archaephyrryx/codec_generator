import { Codec } from '../codec';
import { Box, unboxed } from '../core/box';
import { MemoCell } from '../core/memo';
import { Width, width_decoder, WidthType } from '../core/width';
import width from '../core/width.type';
import { Decoder, liftDecoder } from '../decode';
import { OutputBytes, liftEncoder, Writer } from '../encode';
import { Parser } from '../parse';
import { Target } from '../target';

export type dynamic<T extends Codec, LP extends width> = { prefixWidth: LP, payload: T };

function payloadLengthValidated<LP extends width>({ prefixWidth, payload }: dynamic<Codec, LP>): WidthType[LP] {
    return Width.from(prefixWidth, payload.encodeLength);
}

export class Dynamic<T extends Codec, LP extends width> extends Box<T> implements Codec {
    readonly #payloadLength: MemoCell<WidthType[LP]>;
    readonly prefixWidth: LP;

    constructor(value: T, prefixWidth: LP) {
        super(value);
        this.prefixWidth = prefixWidth;
        this.#payloadLength = MemoCell.Empty();
    }

    get payloadLength(): WidthType[LP] {
        const payload = this.value;
        const prefixWidth = this.prefixWidth;
        return this.#payloadLength.compute(() => {
            return payloadLengthValidated({ prefixWidth, payload });
        });
    }

    get encodeLength(): number {
        const payloadLength = this.payloadLength;
        return payloadLength.encodeLength + Number(payloadLength.value);
    }

    static decode<X extends Codec, LP extends width>(dec: Decoder<X>, prefixWidth: LP): Decoder<Dynamic<X, LP>> {
        return (p: Parser) => {
            const nBytes = width_decoder(prefixWidth)(p);
            p.setFit(nBytes);
            const ret = new Dynamic(dec(p), prefixWidth);
            p.enforceTarget();
            return ret;
        };
    }

    encode(): OutputBytes {
        const lp = this.payloadLength.encode();
        const payload = this.value.encode();
        return [...lp, ...payload];
    }

    static encoder<X extends Codec, LP extends width>(value: Dynamic<X, LP>): OutputBytes {
        return value.encode();
    }

    /**
     * Takes a prefix-width and returns a closure that uses that prefix width
     * to instantiate Dynamic values over generic Codec values.
     * @param prefixWidth Constant width to use in the returned closure
     * @returns Closure that instantiates {@link Dynamic} Codecs using the provided argument as value
     */
    static readonly promoter = <LP extends width>(prefixWidth: LP) => <T extends Codec>(value: T) => {
        return new Dynamic(value, prefixWidth);
    }

    /**
     * Derived factory method that inherits the prefix width of the receiever to
     * instantiate new {@link Dynamic} objects.
     * @param value Payload value of the new object to construct
     * @returns Dynamic object over {@link value} with the same prefix width as the receiver object
     */
    promoter<U extends Codec>(value: U): Dynamic<U, LP> {
        return new Dynamic(value, this.prefixWidth);
    }

    writeTarget<T extends Target>(tgt: T): number {
        return this.payloadLength.writeTarget(tgt) + this.value.writeTarget(tgt);
    }
}

export const dynamic_encoder = <LP extends width>(prefixWidth: LP) => <T extends Codec>(value: T): OutputBytes => {
    return Dynamic.encoder(new Dynamic(value, prefixWidth));
}

export const dynamic_decoder = <T extends Codec, LP extends width>(dec: Decoder<T>, prefixWidth: LP) => unboxed(Dynamic.decode(dec, prefixWidth));

export const dynamic_reader = <T extends Codec, LP extends width>(dec: Decoder<T>, prefixWidth: LP) => liftDecoder(dynamic_decoder(dec, prefixWidth));

export const dynamic_writer = <LP extends width>(prefixWidth: LP) => liftEncoder(dynamic_encoder(prefixWidth));