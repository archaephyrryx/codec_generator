import { Box } from '../../core/box';

export type OptionSome<T> = {
    readonly hasValue: true;
    readonly contents: T;
};

export type OptionNone = { readonly hasValue: false; };

export type option<T> = OptionSome<T> | OptionNone;

export const Some = <T>(contents: T): OptionSome<T> => {
    return { hasValue: true, contents: contents };
};

export const None: OptionNone = ({ hasValue: false });

export const isSome = <T>(opt: option<T>): opt is OptionSome<T> => {
    return opt.hasValue;
};

export const isNone = <T>(opt: option<T>): opt is OptionNone => {
    return !opt.hasValue;
};

export class PrimOption<T> extends Box<option<T>> {
    static getMapped<T, U>(value: option<T>, dft: U, f: (x: T) => U): U {
        if (isSome(value)) {
            return f(value.contents);
        } else {
            return dft;
        }
    }

    orDefault(dft: T): T {
        return (this.value.hasValue ? this.value.contents : dft);
    }

    get hasValue(): boolean {
        return this.value.hasValue;
    }

    get contents(): T | undefined {
        return this.value.hasValue ? this.value.contents : undefined;
    }
}