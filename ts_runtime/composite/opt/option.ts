import { Codec } from '../../codec';
import { onValue, unboxed } from '../../core/box';
import { Decoder, liftDecoder, Reader } from '../../decode';
import { liftEncoder, OutputBytes } from '../../encode';
import { Parser } from '../../parse';
import { Bool, boolean_decoder } from '../../primitive/bool';
import { Target } from '../../target';
import { option, PrimOption } from './option.type';

export class Option<T extends Codec> extends PrimOption<T> implements Codec {
    constructor(value: option<T>) {
        super(value);
    }

    get encodeLength(): number {
        return this.value.hasValue ? 1 + this.value.contents.encodeLength : 1;
    }

    public static decode<X extends Codec>(dec: Decoder<X>): Decoder<Option<X>> {
        return (p: Parser) => {
            const hasValue = boolean_decoder(p);
            if (hasValue) {
                return new Option({ hasValue, contents: dec(p) });
            } else {
                return new Option({ hasValue })
            }
        }
    }

    static promote<T extends Codec>(opt: option<T> | PrimOption<T>): Option<T> {
        return onValue((opt: option<T>) => new Option(opt))(opt);
    }

    encode(): OutputBytes {
        if (this.value.hasValue) {
            return [0xff, ...this.value.contents.encode()];
        } else {
            return [0x00];
        }
    }

    writeTarget<T extends Target>(tgt: T): number {
        const flag = new Bool(this.hasValue);
        if (!this.value.hasValue) {
            return flag.writeTarget(tgt);
        } else {
            return flag.writeTarget(tgt) + this.value.contents.writeTarget(tgt);
        }
    }
}

export const option_encoder = <T extends Codec>(value: option<T>) => Option.promote(value).encode();

export const option_decoder = <X extends Codec>(dec: Decoder<X>): Decoder<option<X>> => unboxed(Option.decode(dec));

export const option_reader = <X extends Codec>(dec: Decoder<X>): Reader<option<X>> => liftDecoder(option_decoder(dec));

export const option_writer = liftEncoder(option_encoder);