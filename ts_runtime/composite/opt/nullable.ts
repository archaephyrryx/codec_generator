import { Codec, Estimable } from '../../codec';
import { onValue, unboxed } from '../../core/box';
import { Decoder, liftDecoder, Reader } from '../../decode';
import { liftEncoder, OutputBytes } from '../../encode';
import { Parser } from '../../parse';
import { Target } from '../../target';
import { None, option, PrimOption, Some } from './option.type';

export class Nullable<T extends Codec> extends PrimOption<T> implements Codec, Estimable {
    constructor(value: option<T>) {
        super(value);
    }

    static Some<T extends Codec>(value: T): Nullable<T> {
        return new Nullable(Some(value));
    }

    static None<T extends Codec>(): Nullable<T> {
        return new Nullable(None);
    }

    static promote<T extends Codec>(value: PrimOption<T> | option<T>): Nullable<T> {
        return onValue((x: option<T>) => new Nullable(x))(value);
    }

    get encodeLength(): number {
        return this.value.hasValue ? this.value.contents.encodeLength : 0;
    }

    static decode<X extends Codec>(dec: Decoder<X>): Decoder<Nullable<X>> {
        return (p: Parser) => {
            if (p.remainder > 0) {
                return Nullable.Some(dec(p));
            } else {
                return Nullable.None();
            }
        }
    }

    get contents(): T | undefined {
        return this.value.hasValue ? this.value.contents : undefined;
    }

    encode(): OutputBytes {
        if (this.value.hasValue) {
            return this.value.contents.encode();
        } else {
            return [];
        }
    }

    writeTarget<T extends Target>(tgt: T): number {
        if (!this.value.hasValue) {
            return 0;
        } else {
            return this.value.contents.writeTarget(tgt);
        }
    }
}

export const nullable_encoder = <X extends Codec>(value: option<X>) => Nullable.promote(value).encode();

export const nullable_decoder = <X extends Codec>(dec: Decoder<X>): Decoder<option<X>> => unboxed(Nullable.decode(dec));

export const nullable_reader = <X extends Codec>(dec: Decoder<X>): Reader<option<X>> => liftDecoder(nullable_decoder(dec));

export const nullable_writer = liftEncoder(nullable_encoder);
