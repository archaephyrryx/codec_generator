import { unreachable } from './safe';
import * as intKinds from '../integer/kinds';
import * as intTypes from '../integer/types';
import * as primops from './primops';

export function fromBytes(kindNative: intKinds.IntKindNative, buf: Uint8Array): intTypes.nativeint;
export function fromBytes(kindNumber: intKinds.IntKindNumber, buf: Uint8Array): number;
export function fromBytes(i64kind: intKinds.Int64Kind, buf: Uint8Array): intTypes.int64;
export function fromBytes({ sig, bits }: intKinds.IntKind, buf: Uint8Array): number | bigint {
    if (buf.byteLength !== Math.ceil(bits / 8)) {
        throw new RangeError(`fromBytes: cannot parse ${bits}-bit quantity from ${buf.byteLength}-byte buffer`);
    }
    switch (bits) {
        case 64: {
            return primops.int64_from_be_bytes(buf);
        }
        case 32: {
            return primops.int32_from_be_bytes(buf);
        }
        case 31: {
            return primops.int31_from_be_bytes(buf);
        }
        case 30: {
            return primops.uint30_from_be_bytes(buf);
        }
        case 16: {
            switch (sig) {
                case 'signed': {
                    return primops.int16_from_be_bytes(buf);
                }
                case 'unsigned': {
                    return primops.uint16_from_be_bytes(buf);
                }
                default:
                    return unreachable(sig);
            }
        }
        case 8: {
            switch (sig) {
                case 'signed': {
                    return primops.int8_from_be_bytes(buf);
                }
                case 'unsigned': {
                    return primops.uint8_from_be_bytes(buf);
                }
            }
        }
    }
}

export function toBytes(valNative: intKinds.TaggedIntNative): number[]
export function toBytes(valNumber: intKinds.TaggedIntNumber): number[]
export function toBytes(valBigint: intKinds.TaggedBigInt): number[]
export function toBytes(valInt: intKinds.TaggedInt): number[]

export function toBytes({ sig, bits, value }: intKinds.TaggedInt): number[] {
    switch (bits) {
        case 64: {
            return primops.int64_to_be_bytes(value);
        }
        case 32: {
            return primops.int32_to_be_bytes(value);
        }
        case 31: {
            return primops.int31_to_be_bytes(value);
        }
        case 30: {
            return primops.uint30_to_be_bytes(value);
        }
        case 16: {
            switch (sig) {
                case 'signed': {
                    return primops.int16_to_be_bytes(value);
                }
                case 'unsigned': {
                    return primops.uint16_to_be_bytes(value);
                }
                default:
                    return unreachable(sig);
            }
        }
        case 8: {
            switch (sig) {
                case 'signed': {
                    return primops.int8_to_be_bytes(value);
                }
                case 'unsigned': {
                    return [value];
                }
                default: {
                    return unreachable(sig);
                }
            }
        }
        default: {
            return unreachable({ bits, sig, value });
        }
    }
}

export const floatFromBytes = (buf: Uint8Array): number => {
    if (buf.byteLength !== 8) {
        throw new RangeError(`floatFromBytes: expected buffer length 8, got ${buf.byteLength}`);
    }
    return primops.float64_from_be_bytes(buf);
};

export const floatToBytes = (f64: number): number[] => {
    return primops.float64_to_be_bytes(f64);
};