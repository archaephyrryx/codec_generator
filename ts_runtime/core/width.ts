import { clampWith, NumericBounds, raiseBounds } from '../bounds';
import { Decoder } from '../decode';
import { Encoder } from '../encode';
import { Uint16, uint16_encoder, Uint30, uint30_encoder, Uint8, uint8_encoder } from '../integer/integer';
import { PrimUint30 } from '../integer/types';
import { Parser } from '../parse';
import { N, n_decoder, n_encoder } from '../zarith/natural';
import { onValue } from './box';
import { unwrap } from './result';
import { forceNat, Nat } from './safe';
import width from './width.type';

export const width_encoder = (w: width): Encoder<number> => (value) => {
    switch (w) {
        case width.Uint8: {
            return uint8_encoder(value);
        }
        case width.Uint16: {
            return uint16_encoder(value);
        }
        case width.Uint30: {
            return uint30_encoder(value);
        }
        case width.N: {
            return n_encoder(BigInt(clampWith(value, PrimUint30.bounds, { strict: true })));
        }
    }
};

export type WidthTypes = WidthType[keyof WidthType]

export type WidthType = {
    [width.Uint8]: Uint8,
    [width.Uint16]: Uint16,
    [width.Uint30]: Uint30,
    [width.N]: N,
}

export class Width {
    static from(prefixWidth: width.Uint8, value: number): Uint8;
    static from(prefixWidth: width.Uint16, value: number): Uint16;
    static from(prefixWidth: width.Uint30, value: number): Uint30;
    static from(prefixWidth: width.N, value: number): N;
    static from<W extends width>(prefixWidth: W, value: number): WidthType[W];
    static from(prefixWidth: width, payloadLength: number): WidthTypes {
        switch (prefixWidth) {
            case width.Uint8: return new Uint8(payloadLength, true);
            case width.Uint16: return new Uint16(payloadLength, true);
            case width.Uint30: return new Uint30(payloadLength, true);
            case width.N: return new N(new Uint30(payloadLength, true).value);
        }
    }

    public static widthBounds(w: width): NumericBounds<number> {
        switch (w) {
            case width.Uint8: return Uint8.bounds;
            case width.Uint16: return Uint16.bounds;
            case width.Uint30: return Uint30.bounds;
            case width.N: return Uint30.bounds;
        }
    }

    /**
     * Returns the number of extra bytes to reserve for a length-prefix
     * whose magnitude is determined by the length of a payload, and whose
     * encoding is directly associated with a particular {@link width}-type.
     *
     * @param w width-type to base the computation on
     * @param payloadLength byte-length or cardinality that is to be represented by the prefix
     * @returns Number of bytes to encode {@link payloadLength} according to {@link w}
     */
    public static prefixBytes(w: width, payloadLength: number): number {
        return Width.from(w, payloadLength).encodeLength;
    }

    public static decode<W extends width>(w: W): Decoder<WidthType[W]>;
    public static decode(w: width) {
        switch (w) {
            case width.Uint8: return Uint8.decode;
            case width.Uint16: return Uint16.decode;
            case width.Uint30: return Uint30.decode;
            case width.N: return (p: Parser) => new N(onValue((x: bigint) => clampWith(x, raiseBounds(Uint30.bounds), { strict: true }))(N.decode(p)));
        }
    }
}


export const width_decoder = (w: width) => (p: Parser): Nat => {
    switch (w) {
        case width.Uint8: {
            return forceNat(unwrap(p.takeUint8()));
        }
        case width.Uint16: {
            return forceNat(unwrap(p.takeUint16()));
        }
        case width.Uint30: {
            return forceNat(unwrap(p.takeUint30()));
        }
        case width.N: {
            const raw = n_decoder(p);
            const clamped_raw = clampWith(raw, raiseBounds(PrimUint30.bounds), { strict: true });
            const ret = BigInt.asUintN(30, clamped_raw);
            return forceNat(Number(ret));
        }
    }
};