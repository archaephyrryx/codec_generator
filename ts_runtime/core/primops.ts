import { clampWith } from '../bounds';
import { PrimInt31, PrimUint30 } from '../integer/types';

export const float64_from_be_bytes = (buf: Uint8Array): number => {
    const view = new DataView(buf.buffer);
    const ret = view.getFloat64(0, false);
    return ret;
};

export const int64_from_be_bytes = (buf: Uint8Array): bigint => {
    const view = new DataView(buf.buffer);
    const ret = view.getBigInt64(0, false);
    return ret;
};

export const int32_from_be_bytes = (buf: Uint8Array): number => {
    const view = new DataView(buf.buffer);
    const ret = view.getInt32(0, false);
    return ret;
};

export const int31_from_be_bytes = (buf: Uint8Array): number => {
    const ret: number = int32_from_be_bytes(buf);
    return clampWith(ret, PrimInt31.bounds, { strict: true });
};

export const uint30_from_be_bytes = (buf: Uint8Array): number => {
    const ret: number = int32_from_be_bytes(buf);
    return clampWith(ret, PrimUint30.bounds, { strict: true });
};

export const int16_from_be_bytes = (buf: Uint8Array): number => {
    const view = new DataView(buf.buffer);
    const ret = view.getInt16(0, false);
    return ret;
};

export const uint16_from_be_bytes = (buf: Uint8Array): number => {
    const view = new DataView(buf.buffer);
    const ret = view.getUint16(0, false);
    return ret;
};

export const int8_from_be_bytes = (buf: Uint8Array): number => {
    const view = new DataView(buf.buffer);
    const ret = view.getInt8(0);
    return ret;
};

export const uint8_from_be_bytes = (buf: Uint8Array): number => {
    const ret: number = buf[0];
    return ret;
};


type Expander<T = number> = (value: T) => number[];

export const float64_to_be_bytes: Expander = (value) => {
    const buf = new Uint8Array(8);
    const view = new DataView(buf.buffer);
    view.setFloat64(0, value, false);
    return [...buf];
};

export const int64_to_be_bytes: Expander<bigint> = (value) => {
    const buf = new Uint8Array(8);
    const view = new DataView(buf.buffer);
    view.setBigInt64(0, value, false);
    return [...buf];
};

export const int32_to_be_bytes: Expander = (value) => {
    const buf = new Uint8Array(4);
    const view = new DataView(buf.buffer);
    view.setInt32(0, value, false);
    return [...buf];
};

export const int31_to_be_bytes: Expander = (value) => {
    const buf = new Uint8Array(4);
    const view = new DataView(buf.buffer);
    if (value > PrimInt31.max || value < PrimInt31.min) {
        throw new RangeError(`int31_to_be_bytes: value ${value} outside of valid range ${PrimInt31.min} ~ ${PrimInt31.max}`);
    }
    view.setInt32(0, value, false);
    return [...buf];
};

export const uint30_to_be_bytes: Expander = (value) => {
    const buf = new Uint8Array(4);
    const view = new DataView(buf.buffer);
    if (value > PrimUint30.max) {
        throw new RangeError(`uint30_to_be_bytes: value ${value} exceeds maximum ${PrimUint30.max}`);
    }
    // NOTE: We do not use setUint32 as expected, as Uint30 < Int31 < Int32
    view.setInt32(0, value, false);
    return [...buf];
};

export const int16_to_be_bytes: Expander = (value) => {
    const buf = new Uint8Array(2);
    const view = new DataView(buf.buffer);
    view.setInt16(0, value, false);
    return [...buf];
};

export const uint16_to_be_bytes: Expander = (value) => {
    const buf = new Uint8Array(2);
    const view = new DataView(buf.buffer);
    view.setUint16(0, value, false);
    return [...buf];
};

export const int8_to_be_bytes = (value: number) => {
    const buf = new Uint8Array(1);
    const view = new DataView(buf.buffer);
    view.setInt8(0, value);
    return [...buf];
};