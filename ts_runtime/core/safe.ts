import { UnreachableError, UnsafeNumberError, WidthError } from './error';
import { bytes } from '../primitive/types';
import { strByteLen } from '../util/strops';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export type Phantom<X> = void

export type FixedWidth<T, N> = T & { __byteLength__: Phantom<N> }

function assertWidth<T extends bytes | string, N extends number>(value: T, width: N): asserts value is FixedWidth<T, N> {
    const len = (typeof value === 'string') ? strByteLen(value) : value.length;

    if (len !== width) {
        throw new WidthError(`assertWidth: mismatched asserted width ${width} and actual length ${len}`);
    }
    return;
}

export function forceWidth<N extends number>(x: bytes, len: N): FixedWidth<bytes, N>;
export function forceWidth<N extends number>(x: string, len: N): FixedWidth<string, N>;

export function forceWidth<T extends string | bytes, N extends number>(x: T, len: N): FixedWidth<T, N> {
    assertWidth(x, len);
    return x;
}


type UnsignedBrand = { __unsigned__: void }
type IntBrand = { __int__: void; }

export type Unsigned<N extends number = number> = N & UnsignedBrand
export type Int<N extends number = number> = N & IntBrand
export type Nat<N extends number = number> = Unsigned<Int<N>>

export function assertUnsigned<N extends number>(n: N): asserts n is Unsigned<N> {
    if (n < 0) {
        throw new Error(`assertUnsigned: unexpected negative value ${n}`);
    }
}

export function assertInt<N extends number>(n: N): asserts n is Int<N> {
    if (n !== Math.trunc(n)) {
        throw new Error(`assertInt: unexpected non-integer value ${n}`);
    }
}

export function assertNat<N extends number>(n: N): asserts n is Nat<N> {
    assertInt(n);
    assertUnsigned(n);
}

export const forceUnsigned = <N extends number>(n: N): Unsigned<N> => {
    assertUnsigned(n);
    return n;
};

export const forceInt = <N extends number>(n: N): Int<N> => {
    assertInt(n);
    return n;
};

export const forceNat = <N extends number>(n: N): Nat<N> => {
    assertNat(n);
    return n;
};

function assertNum<T>(x: T | number): asserts x is number {
    if (typeof x !== 'number') {
        throw new Error(`Assertion failed: ${x} is not a number`);
    }
}

function assertBigint<T>(x: T | bigint): asserts x is bigint {
    if (typeof x !== 'bigint') {
        throw new Error(`Assertion failed: ${x} is not a bigint`);
    }
}

export const forceNum = <T>(x: T | number): number => {
    assertNum(x);
    return x;
};


export const forceBigint = <T>(x: T | bigint): bigint => {
    assertBigint(x);
    return x;
};



/**
 * Smart type-guard that filters out infinite and `NaN` values
 *
 * @param n Value to validate
 * @returns `true` when `n` is a number, `false` otherwise
 *
 * @throws {@link UnsafeNumberError}
 * If `n` is `NaN` or infinite
 */
export const mkSafe = (n: unknown): n is number => {
    if (Number.isNaN(n)) {
        throw UnsafeNumberError.UnsafeNaNError();
    } else if (Number.isFinite(n)) {
        return true;
    } else if (typeof n === 'number') {
        throw UnsafeNumberError.UnsafeInfiniteError();
    } else {
        return false;
    }
};

export type NeverObject = { [k: string | number]: never }


/**
 * Utility function to signal to the type checker that a certain branch
 * or case should be unreachable, through exhaustive narrowing of a standalone
 * value to `never`.
 *
 * @param val Value which is expected to have been narrowed to `never`
 *
 * @throws {@link UnreachableError}, unconditionally
 */
export function unreachable(val: never): never;

/**
 * Utility function to signal to the type checker that a certain branch
 * or case should be unreachable, through exhaustive narrowing of all
 * field-types of an object to `never`.
 *
 * @param obj Object whose fields are expected to have been narrowed to `never`
 *
 * @throws {@link UnreachableError}, unconditionally
 */
export function unreachable(obj: NeverObject): never;

export function unreachable(x: never | NeverObject): never {
    throw new UnreachableError(`assumed-unreachable code-path was reached! (argument: ${x})`);
}