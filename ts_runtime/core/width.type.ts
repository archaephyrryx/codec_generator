const enum width {
    Uint8 = "uint8",
    Uint16 = "uint16",
    Uint30 = "uint30",
    N = "N"
} export default width;

