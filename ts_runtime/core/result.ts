const TagOk = 'OK' as const;
const TagErr = 'ERR' as const;

type TagOk = typeof TagOk
type TagErr = typeof TagErr

type ResultOk<T> = { readonly kind: TagOk; readonly value: T; };
type ResultErr<E> = { readonly kind: TagErr; readonly reason: E; };

export type Result<T, E> = ResultOk<T> | ResultErr<E>;

export const Ok = <T>(contents: T): ResultOk<T> => {
    return { kind: TagOk, value: contents };
};

export const Err = <E>(reason: E): ResultErr<E> => {
    return { kind: TagErr, reason };
};

export const isOk = <T, E>(res: Result<T, E>): res is ResultOk<T> => {
    return res.kind === TagOk;
};

export const isErr = <T, E>(res: Result<T, E>): res is ResultErr<E> => {
    return res.kind === TagErr;
};

export const desugar = <T, E>(res: Result<T, E>): T | E => {
    return isOk(res) ? res.value : res.reason;
};

export const extract = <T>(res: Result<T, unknown>): T | undefined => {
    return isOk(res) ? res.value : undefined;
};

function assertOk<T>(res: Result<T, unknown>): asserts res is ResultOk<T> {
    if (res.kind === TagErr) {
        if (res.reason instanceof Error) {
            throw res.reason;
        } else {
            throw new Error(`assertOk: found Err: ${res.reason}`);
        }
    }
    return;
}

export const unwrap = <T, E>(res: Result<T, E>): T => {
    assertOk(res);
    return res.value;
};

export const mapOk = <T, U, E>(res: Result<T, E>, f: (x: T) => U): Result<U, E> => {
    if (isOk(res)) {
        return Ok(f(res.value));
    } else {
        return res;
    }
};

export const bindResult = <T, U, E2>(f: (x: T) => Result<U, E2>) => <E1>(res: Result<T, E1>): Result<U, E1 | E2> => {
    if (isOk(res)) {
        return f(res.value);
    } else {
        return res;
    }
};