type MemoUnset = { set: false }
type MemoSet<T> = { set: true, mem: T }

type Memo<T> = MemoSet<T> | MemoUnset

/**
 * Cell class for memoized computations whose result is invariant
 * but which have non-trivial computation costs or overhead.
 *
 * Useful when you want to amortize the computation cost of a predicate
 * or method that you expect to call more than once on the same object.
 */
export class MemoCell<T> {
    #cell: Memo<T>;

    private constructor(mem?: T) {
        if (mem === undefined) {
            this.#cell = { set: false };
        } else {
            this.#cell = { set: true, mem };
        }
    }

    public static Empty<T>(): MemoCell<T> {
        return new this();
    }

    public isEmpty(): boolean {
        return !this.#cell.set;
    }

    public static Cell<T>(value: T): MemoCell<T> {
        return new this(value);
    }

    public get value(): T | undefined {
        return this.#cell.set ? this.#cell.mem : undefined;
    }

    private swap(newValue: T): T | undefined {
        const oldValue = this.value;
        this.#cell = { set: true, mem: newValue };
        return oldValue;
    }

    overwrite(newValue: T): T {
        this.#cell = { set: true, mem: newValue };
        return newValue;
    }

    /**
     * Returns the memoized result of an invariant computation passed in as
     * a thunk.
     *
     * If a value is already present in the cell, it is returned and the thunk is
     * not called.
     *
     * Otherwise, the thunk is evaluated, and the return value is first used
     * to populate the cell, and then returned.
     *
     * @param thunk Delayed computation to evaluate if empty
     * @returns Value stored in the cell after conditional overwrite
     */
    public compute(thunk: () => T): T {
        if (this.#cell.set) {
            return this.#cell.mem;
        } else {
            return this.overwrite(thunk());
        }
    }

    private take(): T | undefined {
        const oldValue = this.value;
        this.#cell = { set: false };
        return oldValue;
    }

    private apply(f: (x: T) => T): T | undefined {
        const oldValue = this.value;
        if (this.#cell.set) {
            this.#cell.mem = f(this.#cell.mem);
        }
        return oldValue;
    }

    private bind<U>(f: (x: T) => U | undefined): MemoCell<U> {
        if (this.#cell.set) {
            return new MemoCell<U>(f(this.#cell.mem));
        } else {
            return new MemoCell<U>();
        }
    }
}