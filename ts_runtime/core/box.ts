/**
 * Type-wrapper to allow sub-classing of TypeScript-native types
 * via indirection.
 *
 * Semantically equivalent to an immutable value of type `T`.
 */
export class Box<T> {
    public constructor(public readonly value: T) { }
}

export const box: <T>(val: T) => Box<T> = (val) => new Box(val);
export const unbox: <T>(val: Box<T>) => T = (val) => val.value;

export type Unboxed<T> = T extends Box<infer U> ? U : never;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
type Argument<F> = F extends (x: infer A) => infer B ? A : never;
type UnboxedReturn<F> = F extends (x: infer A) => infer B ? B extends Box<infer C> ? C : never : never;
type UnboxedFunction<F> = (x: Argument<F>) => UnboxedReturn<F>;

type PolyBoxed<U, Y> = <B extends Box<U>>(x: U | B) => Y

type PolyBoxer = <U, Y>(f: (x: U) => Y) => PolyBoxed<U, Y>

/**
 * Adapter that unboxes the return type of a generic function.
 *
 * @param f Function that returns a boxed type
 * @returns Equivalent function that returns the unboxed type
 */
export const unboxed = <F extends CallableFunction>(f: F): UnboxedFunction<F> => (x) => f(x).value;

export const onValue : PolyBoxer = (f) => (x) => {
    if (x instanceof Box) {
        return f(x.value);
    } else {
        return f(x);
    }
};