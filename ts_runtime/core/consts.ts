export const enum PrimWidth {
    Unit = 0,
    Bool = 1,
    Uint8 = 1,
    Int8 = 1,
    Uint16 = 2,
    Int16 = 2,
    Uint30 = 4,
    Int31 = 4,
    Int32 = 4,
    Int64 = 8,
    Double = 8
}