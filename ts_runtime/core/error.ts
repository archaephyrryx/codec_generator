export class UnsafeNumberError extends Error {
    public static UnsafeNaNError() {
        return new UnsafeNumberError('NaN value encountered');
    }

    public static UnsafeInfiniteError() {
        return new UnsafeNumberError('Infinite value encountered');
    }
}

/**
 * Error class representing failure of assumption that a particular
 * branch or block of code is unreachable.
 */
export class UnreachableError extends Error {
}

export class WidthError extends Error { }