import { Box, unboxed } from '../core/box';
import { Codec, Estimable } from '../codec';
import { unwrap } from '../core/result';
import { Reader, liftDecoder, Decoder } from '../decode';
import { Encode, Encoder, liftEncoder, OutputBytes, Writer } from '../encode';
import { HexString } from '../util/hex';
import { Parser } from '../parse';
import { Target } from '../target';
import isFinalByte from './predicate';


export type t = bigint;

export type integer = t;

type u = Uint8Array;

enum sign_bit {
    Natural = 0x00,
    Negative = 0x40
}

type int_parts = { abs: bigint; readonly signum: sign_bit; };

function intoParts(z: integer): int_parts {
    if (z < 0) {
        return { abs: -z, signum: sign_bit.Negative };
    } else {
        return { abs: z, signum: sign_bit.Natural };
    }
}

export function lenBytes(z: integer): number {
    const bits = z.toString(2).length;
    const bytes = Math.ceil((bits - 6) / 7) + 1;
    return bytes;
}

/**
 * Converts an integer into its binary form
 *
 * @param z integer to serialize
 * @returns binary encoding of z
 */
function toBytes(z: integer): u {
    const { abs, signum } = intoParts(z);
    const n_bytes = lenBytes(abs);
    const buf = new Uint8Array(n_bytes);
    let ix = 0;

    let zz = abs;
    let last: boolean;

    let byte = Number(zz & 0x3fn) | signum;
    zz >>= 6n;
    last = !zz;
    if (!last)
        byte |= 0x80;
    buf[ix++] = byte;

    while (!last) {
        let byte = Number(zz & 0x7fn);
        zz >>= 7n;
        last = !zz;
        if (!last)
            byte |= 0x80;
        buf[ix++] = byte;
    }

    return buf;
}

function fromBytes(bytes: u): integer {
    let n = 0n;
    let bits = 0n;
    let is_negative = false;

    if (bytes.length === 0) {
        throw new Error('Cannot parse empty byte-array as Zarith integer');
    }

    bytes.forEach((byte, index) => {
        let val: number;
        if (index === 0) {
            is_negative = !!(byte & 0x40);
            val = byte & 0x3f;
            n |= (BigInt(val) << bits);
            bits += 6n;
        } else {
            val = byte & 0x7f;
            n |= (BigInt(val) << bits);
            bits += 7n;
            if (!byte)
                throw new Error('Unexpected trailing zero byte in Zarith integer byte-array');
        }
    });

    if (is_negative) {
        return -n;
    } else {
        return n;
    }
}

export class Z extends Box<integer> implements Codec, Estimable {
    constructor(value: number | integer) {
        super(BigInt(value));
    }

    get encodeLength(): number {
        return lenBytes(this.value);
    }

    static decode(p: Parser): Z {
        const bytes = unwrap(p.takeSelfTerminating(isFinalByte));
        return new Z(fromBytes(bytes));
    }


    __components(): Encode[] {
        return [this];
    }
    encode(): OutputBytes {
        return [...toBytes(this.value)];
    }

    writeTarget<T extends Target>(tgt: T): number {
        const bytes: Uint8Array = toBytes(this.value);
        return tgt.writeAll(bytes);
    }
}

export const z_encoder: Encoder<integer> = (value) => {
    return new Z(value).encode();
};

export const z_decoder: Decoder<integer> = unboxed(Z.decode);

export const z_reader: Reader<integer> = liftDecoder(z_decoder);

export const z_writer: Writer<integer> = liftEncoder(z_encoder);