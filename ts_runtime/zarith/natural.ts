import { Codec, Estimable } from '../codec';
import { Box, unboxed } from '../core/box';
import { unwrap } from '../core/result';
import { Decoder, liftDecoder, Reader } from '../decode';
import { Encode, Encoder, OutputBytes } from '../encode';
import { Parser } from '../parse';
import { Target } from '../target';
import { HexString } from '../util/hex';
import isFinalByte from './predicate';


export type t = bigint;

export type natural = t;

export function lenBytes(n: natural): number {
    const bits = n.toString(2).length;
    const bytes = Math.ceil(bits / 7);
    return bytes;
}

/**
 * Converts a non-negative bigint integer into a bytestring,
 * using the standard `data-encoding` strategy for Zarith
 * naturals
 *
 * @param n natural number to serialize
 * @returns binary encoding of n

 * @throws {@link RangeError} If `n` is negative
 */
function toBytes(n: natural): Buffer {
    if (n < 0) {
        throw new RangeError(`Invalid (negative) Zarith natural: ${n}`);
    }

    const nBytes = lenBytes(n);
    const buf = Buffer.alloc(nBytes);
    let ix = 0;

    let nn = n;
    let last = false;

    while (!last) {
        let byte = Number(nn & 0x7fn);
        nn >>= 7n;
        last = !nn;
        if (!last)
            byte |= 0x80;
        buf[ix++] = byte;
    }

    return buf;
}

function fromBytes(bytes: readonly number[] | Uint8Array): natural {
    let n: natural = 0n;
    let bits: natural = 0n;

    if (bytes.length === 0) {
        throw new Error('Cannot parse empty byte-array as Zarith natural');
    }

    bytes.forEach((byte, index) => {
        const val = byte & 0x7f;
        n |= (BigInt(val) << bits);
        bits += 7n;
        if (!byte && index > 0)
            throw new Error('Unexpected trailing zero byte in Zarith natural byte-array');
    });

    return n;
}

export class N extends Box<natural> implements Codec, Estimable {
    /**
     * Constructs a new instance of the N class with the given
     * numeric value.
     *
     * @param val Numeric value to store in the new Zarith natural
     *
     * @throws {@link RangeError} If the provided value is negative
     */
    constructor(val: natural | number) {
        let value: bigint;
        if (typeof val === 'number') {
            if (val < 0) {
                throw new RangeError(`new N(): argument must be non-negative, found ${val}`);
            }
            value = BigInt(val);
        } else {
            if (val < 0n) {
                throw new RangeError(`new N(): argument must be non-negative, found ${val}`);
            }
            value = val;
        }
        super(value);
    }

    get encodeLength(): number {
        return lenBytes(this.value);
    }

    static decode(p: Parser): N {
        const bytes: Uint8Array = unwrap(p.takeSelfTerminating(isFinalByte));
        return new N(fromBytes(bytes));
    }

    __components(): Encode[] {
        return [this];
    }

    encode(): OutputBytes {
        return [...toBytes(this.value)];
    }

    static encoder(value: N): OutputBytes {
        return value.encode();
    }

    writeTarget<T extends Target>(tgt: T): number {
        const bytes: Uint8Array = toBytes(this.value);
        return tgt.writeAll(bytes);
    }
}

export const n_encoder: Encoder<natural> = (value) => {
    return new N(value).encode();
};

export const n_decoder: Decoder<natural> = unboxed(N.decode);

export const n_reader: Reader<natural> = liftDecoder(n_decoder);

export const write_to = (value: natural, buf: string): string => {
    const hex = new HexString(toBytes(value));
    return hex.appendHex(buf);
};