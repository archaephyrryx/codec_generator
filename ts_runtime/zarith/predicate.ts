export default function isFinalByte(byte: number) {
    return (byte & 0x80) === 0;
}