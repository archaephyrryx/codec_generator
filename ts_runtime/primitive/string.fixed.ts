import { unboxed } from '../core/box';
import { Codec, ConstantLength } from '../codec';
import Ext from '../ext/fixed_len';
import { FixedWidth } from '../core/safe';
import { WidthError } from '../core/error';
import { unwrap } from '../core/result';
import { liftDecoder, Reader } from '../decode';
import { Encode, OutputBytes } from '../encode';
import { Parser } from '../parse';
import { PrimString, __string } from './types';
import { Target } from '../target';

export class FixedString<N extends number> extends PrimString implements Codec, ConstantLength<N> {
    readonly #length: N;
    readonly __constantLength__: void = undefined;

    constructor(value: __string, length: N) {
        if (value.length !== length) {
            throw new WidthError(`FixedString: expected string length to be ${length}, found ${value.length} ("${value}")`);
        }
        super(value);
        this.#length = length;
    }
    get constLen(): N {
        return this.#length;
    }

    get encodeLength(): N {
        return this.constLen;
    }

    public static promote(value: string): FixedString<number>;
    public static promote<N extends number>(value: FixedWidth<string, N>): FixedString<N>;
    public static promote<N extends number>(value: __string & { length: N }): FixedString<N>;
    public static promote<N extends number>(value: (__string & { length: N }) | FixedWidth<string, N> | string) {
        const bufval = Buffer.from(value);
        return new FixedString(bufval, bufval.length);
    }

    __components(): Encode[] {
        return [this];
    }

    encode(): OutputBytes {
        return [...this.value];
    }

    static readonly decode = <N extends number>(proxy: Ext<N>) => (p: Parser): FixedString<N> => {
        const bytes = unwrap(p.takeFixed(proxy.len));
        return new FixedString(Buffer.from(bytes), proxy.len);
    };

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }

}

export type GenericFixedString<N extends number = number> = FixedString<N>

export const fixedstring_decoder = <N extends number>(proxy: Ext<N>) => (p: Parser) => {
    return unboxed(FixedString.decode(proxy))(p).toString('utf8');
};

export const fixedstring_reader = <N extends number>(proxy: Ext<N>) => liftDecoder(fixedstring_decoder(proxy));
