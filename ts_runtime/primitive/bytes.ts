import { Codec, Estimable } from '../codec';
import { onValue, unboxed } from '../core/box';
import { unwrap } from '../core/result';
import { Decoder, liftDecoder, Reader } from '../decode';
import { Encoder, liftEncoder, OutputBytes, Writer } from '../encode';
import { Parser } from '../parse';
import { Target } from '../target';
import { mem_eq } from '../util/arrops';
import { bytes, PrimBytes } from './types';

export class Bytes extends PrimBytes implements Codec {
    get encodeLength(): number {
        return this.byteLength;
    }

    static decode: Decoder<Bytes> = (p: Parser) => {
        const u8arr: Uint8Array = unwrap(p.takeDynamic());
        return new Bytes([...u8arr]);
    };

    static fromArray(arr: Uint8Array | readonly number[]): Bytes {
        return new Bytes([...arr]);
    }

    equals(other: Bytes): boolean {
        return mem_eq(this.value, other.value);
    }

    encode(): OutputBytes {
        return [...this.value];
    }

    static encoder(other: Bytes): OutputBytes {
        return [...other.value];
    }

    static promote(value: bytes | Uint8Array | PrimBytes): Bytes {
        return onValue(Bytes.fromArray)(value);
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.value);
    }
}

export const bytes_encoder: Encoder<bytes> = (value) => [...value];

export const bytes_decoder: Decoder<bytes> = unboxed(Bytes.decode);

export const bytes_reader: Reader<bytes> = liftDecoder(bytes_decoder);

export const bytes_writer: Writer<bytes> = liftEncoder(bytes_encoder);