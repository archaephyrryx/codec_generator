import { Buffer } from 'buffer';
import { Box } from '../core/box';
import { HexString } from '../util/hex';

export type bytes = readonly number[];

export class PrimBytes extends Box<bytes> {
    get byteLength(): number {
        return this.value.length;
    }

    toString(): string {
        return HexString.bufToHex(this.value);
    }
}

export type __string = Buffer;

export class PrimString extends Box<__string> {
    get byteLength(): number {
        return this.value.byteLength;
    }

    toString(): string {
        return this.value.toString('utf8');
    }
}