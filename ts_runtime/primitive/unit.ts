import { Codec, ConstantLength } from '../codec';
import { Decoder, liftDecoder, Reader } from '../decode';
import { Encoder, OutputBytes, Writer } from '../encode';
import { Parser } from '../parse';
import { Target } from '../target';

export type unit = []

export const unit: unit = [];

export class Unit implements Codec, ConstantLength<0> {
    readonly __constantLength__: void = undefined;

    get constLen(): 0 {
        return 0;
    }

    get encodeLength(): 0 {
        return this.constLen;
    }

    public static decode(_: Parser): Unit {
        return new Unit();
    }

    encode(): OutputBytes {
        return [];
    }

    static promote(_: unit): Unit {
        return new Unit();
    }

    writeTarget<T extends Target>(_: T): number {
        return 0;
    }
}

export const UNIT: Unit = new Unit();

export const promote = (_: unit): Unit => new Unit();

export const unit_encoder: Encoder<unit | Unit> = (_: unit | Unit) => [];

export const unit_decoder: Decoder<unit> = (_p: Parser): unit => unit;

export const unit_reader: Reader<unit> = liftDecoder(unit_decoder);

export const unit_writer: Writer<unit | Unit> = (_: unit | Unit, buf: string) => buf;