import { Codec, Estimable } from '../codec';
import { unwrap } from '../core/result';
import { Decoder, liftDecoder, Reader } from '../decode';
import { Encoder, liftEncoder, OutputBytes, Writer } from '../encode';
import { Parser } from '../parse';
import { PrimString, __string, } from './types';
import { Target } from '../target';
import { strToBytes } from '../util/strops';
import { unboxed } from '../runtime';

export class U8String extends PrimString implements Codec, Estimable {
    get encodeLength(): number {
        return this.byteLength
    }

    public static decode: Decoder<U8String> = (p: Parser) => {
        const u8arr: Uint8Array = unwrap(p.takeDynamic());
        return new U8String(Buffer.from(u8arr));
    };

    public static fromString(s: string): U8String {
        return new U8String(strToBytes(s));
    }

    equals(other: U8String): boolean {
        return this.byteLength === other.byteLength &&
            this.value.every((codepoint, ix) => codepoint === other.value[ix]);
    }

    encode(): OutputBytes {
        return [...this.value];
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }
}

export const u8string_encoder: Encoder<__string> = (value) => [...value];

export const u8string_decoder: Decoder<__string> = unboxed(U8String.decode);

export const u8string_reader: Reader<__string> = liftDecoder(u8string_decoder);

export const u8string_writer: Writer<__string> = liftEncoder(u8string_encoder);
