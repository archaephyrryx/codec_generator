import { onValue, unboxed } from '../core/box';
import { Codec } from '../codec';
import { unwrap } from '../core/result';
import { Decoder, liftDecoder, Reader } from '../decode';
import { Encoder, liftEncoder, OutputBytes, Writer } from '../encode';
import { HexString } from '../util/hex';
import { Parser } from '../parse';
import { PrimBool } from './bool.type';
import { Target } from '../target';

function asByte(val: boolean): number {
    return val ? 0xFF : 0x00;
}

function asBytes(val: boolean): number[] {
    return [asByte(val)];
}

export class Bool extends PrimBool implements Codec {
    get constLen(): 1 {
        return 1;
    }

    get encodeLength(): 1 {
        return this.constLen;
    }

    public static decode(p: Parser): Bool {
        const raw = p.takeBoolean();
        return new Bool(unwrap(raw));
    }

    static readonly encoder = (value: boolean | PrimBool): OutputBytes => {
        return onValue<boolean, OutputBytes>(asBytes)(value);
    }

    encode(): OutputBytes {
        return asBytes(this.value);
    }

    writeTarget<T extends Target>(tgt: T): 1 {
        return tgt.writeOne(asByte(this.value));
    }

    static promote(value: boolean | PrimBool): Bool {
        return onValue((raw: boolean) => new Bool(raw))(value);
    }
}

export const TRUE: Bool = new Bool(true);

export const FALSE: Bool = new Bool(false);

export const boolean_encoder: Encoder<boolean | PrimBool> = Bool.encoder;

export const boolean_decoder: Decoder<boolean> = unboxed(Bool.decode);

export const boolean_reader: Reader<boolean> = liftDecoder(boolean_decoder);

export const boolean_writer: Writer<boolean | PrimBool> = liftEncoder(boolean_encoder);