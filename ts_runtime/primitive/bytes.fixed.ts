import { Box, unboxed } from '../core/box';
import { Codec, ConstantLength } from '../codec';
import FixedLenExt from '../ext/fixed_len';
import { FixedWidth } from '../core/safe';
import { WidthError } from '../core/error';
import { unwrap } from '../core/result';
import { Decoder, liftDecoder, liftPseudoProxyDecoder, Reader } from '../decode';
import { Encode, Encoder, OutputBytes } from '../encode';
import { bytes } from './types';
import { Target } from '../target';

export type Ext<N extends number = number> = FixedLenExt<N>;

export class FixedBytes<N extends number> extends Box<bytes> implements Codec, ConstantLength<N> {
    readonly #length: N;
    readonly __constantLength__: void = undefined;

    constructor(value: bytes, length: N) {
        if (value.length !== length) {
            throw new WidthError(`FixedString: expected string length to be ${length}, found ${value.length} ("${value}")`);
        }
        super(value.slice());
        this.#length = length;
    }

    get constLen(): N {
        return this.#length;
    }

    get encodeLength(): N {
        return this.constLen;
    }

    public static promote(value: bytes): FixedBytes<number>;
    public static promote<N extends number>(value: FixedWidth<bytes, N> | (bytes & { length: N })): FixedBytes<N>;
    public static promote<N extends number>(value: (bytes & { length: N }) | FixedWidth<bytes, N> | bytes) {
        return new FixedBytes(value, value.length);
    }

    __components(): Encode[] {
        if (this.value.length !== this.#length) {
            throw new WidthError(`FixedBytes: expected ${this.#length} bytes, found ${this.value.length} instead`);
        }
        return [this];
    }

    encode(): OutputBytes {
        if (this.value.length !== this.#length) {
            throw new WidthError(`FixedBytes: expected ${this.#length} bytes, found ${this.value.length} instead`);
        }
        return this.value.slice();
    }

    get length(): number {
        if (this.value.length !== this.#length) {
            throw new WidthError(`FixedBytes: expected ${this.#length} bytes, found ${this.value.length} instead`);
        }
        return this.#length;
    }

    static readonly decode = <N extends number>(proxy: Ext<N>): Decoder<FixedBytes<N>> => (p): FixedBytes<N> => {
        const bytes = unwrap(p.takeFixed(proxy.len));
        const value: bytes = [...bytes];
        return new FixedBytes(value, proxy.len);
    };

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }
}

export type GenericFixedBytes<N extends number = number> = FixedBytes<N>

export const fixedbytes_encoder: Encoder<bytes> = (value) => value.slice();

export const fixedbytes_decoder = <N extends number>(proxy: Ext<N>) => {
    return unboxed(FixedBytes.decode(proxy));
};

export const fixedbytes_reader = <N extends number>(proxy: Ext<N>) => liftDecoder(fixedbytes_decoder(proxy));
