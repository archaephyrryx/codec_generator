import { Box } from '../core/box';

export type bool = boolean;

export class PrimBool extends Box<bool> {
    static readonly True: PrimBool = new PrimBool(true);
    static readonly False: PrimBool = new PrimBool(false);
}
