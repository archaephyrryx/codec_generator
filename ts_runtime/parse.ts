import { floatFromBytes, fromBytes } from './core/ops';
import { Err, mapOk, Ok } from './core/result';
import { Nat } from './core/safe';
import { double } from './float/type';
import * as kinds from './integer/kinds';
import * as int_t from './integer/types';
import { ParseResult, TokenError, WindowError } from './parse/error';
import { IndexTracker } from './parse/internal';
import { HexString } from './util/hex';

export interface TypedParser {
    takeUint8(): ParseResult<int_t.uint8>;
    takePrimUint8(): ParseResult<int_t.PrimUint8>;

    takeInt8(): ParseResult<int_t.int8>;
    takePrimInt8(): ParseResult<int_t.PrimInt8>;

    takeUint16(): ParseResult<int_t.uint16>;
    takePrimUint16(): ParseResult<int_t.PrimUint16>;

    takeInt16(): ParseResult<int_t.int16>;
    takePrimInt16(): ParseResult<int_t.PrimInt16>;

    takeUint30(): ParseResult<int_t.uint30>;
    takePrimUint30(): ParseResult<int_t.PrimUint30>;
    takeInt31(): ParseResult<int_t.int31>;
    takePrimInt31(): ParseResult<int_t.PrimInt31>;

    takeInt32(): ParseResult<int_t.int32>;
    takePrimInt32(): ParseResult<int_t.PrimInt32>;

    takeInt64(): ParseResult<int_t.int64>;
    takePrimInt64(): ParseResult<int_t.PrimInt64>;

    takeIntNative(kind: kinds.IntKindNative): ParseResult<int_t.nativeint>;

    takeDouble(): ParseResult<double>;

    takeBoolean(): ParseResult<boolean>;
    takeSelfTerminating(f: (byte: number) => boolean): ParseResult<Uint8Array>;

    takeFixed<N extends number>(len: N): ParseResult<Uint8Array>;
    skipPadding<N extends number>(nBytes: N, validator?: (padding: Uint8Array) => boolean): ParseResult<true>;
    takeDynamic(): ParseResult<Uint8Array>;
}

export abstract class Parser implements TypedParser {
    protected abstract _buffer: Uint8Array;
    protected abstract _offset: IndexTracker;

    /**
     * Logical length of the buffer in the current context,
     * counting from the actual 0-index to the final accessible
     * index, which may not always be the final byte in the buffer.
     */
    public get viewLen(): Nat {
        return this._offset.limit;
    }

    /**
     * Offset from the start of the buffer that subsequent consume-operations
     * will start from.
     *
     * Starts at 0 in a fresh parser, and is bounded above by {@link Parser.viewLen}
     */
    public get offset(): Nat {
        return this._offset.index;
    }

    /**
     * Number of bytes that remain unconsumed, starting from the byte at the offset index
     * up until the last legally consumable index in the current view.
     */
    public get remainder(): Nat {
        return (this.viewLen - this.offset) as Nat;
    }

    /**
     * Consume a single byte and return it,
     * advancing the offset by one position if successful.
     *
     * @throws {@link WindowError}
     * If the current context-frame (or the entire buffer) has been exhausted, and no
     * further bytes can be legally consumed without escaping the current context.
     */
    public abstract consumeByte(): number;

    /**
     * Consumes a specified number of bytes and returns them in an array,
     * advancing the offset by the corresponding number of indices if successful.
     *
     * @param nbytes Number of bytes to consume.
     *
     * @throws {@link WindowError}
     * If the requested number of bytes cannot be consumed without overrunning the
     * end of the context-frame, or the end of the buffer itself.
     */
    public abstract consume(nbytes: number): Uint8Array;

    /**
     * Creates and enters a new context-frame, in which only a specified number of
     * further bytes can be legally consumed.
     *
     * Due to the nature of context-windows, this number should always be between 0
     * and the instantaneous value of {@link remainder} (inclusive of both). If either such bound is violated,
     * an appropriate error should be thrown.
     *
     * @param n Exact number of bytes to leave consumable in the new context-frame
     */
    public abstract setFit(n: number): void;

    /**
     * Creates and enters a new context frame, which ends {@link delta} bytes before
     * the end of the current context at call-time. This effectively 'reserves' a specific
     * number of bytes to be consumed only after escaping the narrowed context.
     *
     * @param delta Number of tail-bytes to reserve when creating the new context-frame
     */
    public abstract shrinkFit(delta: number): void;

    /**
     * Determines whether the current context-frame's target offset has been met
     * precisely, and therefore whether {@link enforceTarget} would succeed if called.
     *
     * @returns `true` if the target has been met, false if it has not
     *
     * @throws {@link WindowError}
     * If the current state is somehow illegal, indicating an implementation bug
     * or unsound state-manipulation.
     */
    public abstract testTarget(): boolean;

    /**
     * Attempts to close the current context-frame, succeeding only
     * if there is one to close, and it has been exhausted (i.e. the current
     * offset has reached the implicit target-offset), throwing a suitable
     * error otherwise.
     */
    public abstract enforceTarget(): void;

    public takeUint8(): ParseResult<int_t.uint8> {
        try {
            const byte: number = this.consumeByte();
            return Ok(byte);
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`unexpected throw: ${e}`);
            }
        }
    }

    public takePrimUint8(): ParseResult<int_t.PrimUint8> {
        const byte: ParseResult<int_t.uint8> = this.takeUint8();
        return mapOk(byte, x => new int_t.PrimUint8(x, true));
    }

    public takeInt8(): ParseResult<int_t.int8> {
        try {
            const slice1 = this.consume(1);
            const kind = kinds.Int8Kind;
            const ret: int_t.int8 = fromBytes(kind, slice1);
            return Ok(ret);
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`caught non-Error error: ${e}`);
            }
        }
    }

    public takePrimInt8(): ParseResult<int_t.PrimInt8> {
        const ret: ParseResult<int_t.int8> = this.takeInt8();
        return mapOk(ret, x => new int_t.PrimInt8(x, true));
    }

    public takeInt16(): ParseResult<int_t.int16> {
        try {
            const slice2 = this.consume(2);
            const kind = kinds.Int16Kind;
            const ret: int_t.int16 = fromBytes(kind, slice2);
            return Ok(ret);
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`caught non-Error error: ${e}`);
            }
        }
    }

    public takePrimInt16(): ParseResult<int_t.PrimInt16> {
        const ret: ParseResult<int_t.int16> = this.takeInt16();
        return mapOk(ret, x => new int_t.PrimInt16(x, true));
    }

    public takeUint16(): ParseResult<int_t.uint16> {
        try {
            const slice2 = this.consume(2);
            const kind = kinds.Uint16Kind;
            const ret: int_t.uint16 = fromBytes(kind, slice2);
            return Ok(ret);
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`caught non-Error error: ${e}`);
            }
        }
    }

    public takePrimUint16(): ParseResult<int_t.PrimUint16> {
        const ret: ParseResult<int_t.uint16> = this.takeUint16();
        return mapOk(ret, x => new int_t.PrimUint16(x, true));
    }

    public takeUint30(): ParseResult<int_t.uint30> {
        try {
            const slice4 = this.consume(4);
            const kind = kinds.Uint30Kind;
            const ret: int_t.uint30 = fromBytes(kind, slice4);
            return Ok(ret);
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`caught non-Error error: ${e}`);
            }
        }
    }

    public takePrimUint30(): ParseResult<int_t.PrimUint30> {
        const ret = this.takeUint30();
        return mapOk(ret, x => new int_t.PrimUint30(x, true));
    }

    public takeInt31(): ParseResult<int_t.int31> {
        try {
            const slice4 = this.consume(4);
            const kind = kinds.Int31Kind;
            const ret: int_t.int31 = fromBytes(kind, slice4);
            return Ok(ret);
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`caught non-Error error: ${e}`);
            }
        }
    }

    public takePrimInt31(): ParseResult<int_t.PrimInt31> {
        const ret = this.takeInt31();
        return mapOk(ret, x => new int_t.PrimInt31(x, true));
    }

    public takeInt32(): ParseResult<int_t.int32> {
        try {
            const slice4 = this.consume(4);
            const kind = kinds.Int32Kind;
            const ret: int_t.int32 = fromBytes(kind, slice4);
            return Ok(ret);
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`caught non-Error error: ${e}`);
            }
        }
    }

    public takePrimInt32(): ParseResult<int_t.PrimInt32> {
        const ret = this.takeInt32();
        return mapOk(ret, x => new int_t.PrimInt32(x, true));
    }

    public takeInt64(): ParseResult<int_t.int64> {
        try {
            const slice8 = this.consume(8);
            const kind = kinds.Int64Kind;
            const ret: int_t.int64 = fromBytes(kind, slice8);
            return Ok(ret);
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`caught non-Error error: ${e}`);
            }
        }
    }

    public takePrimInt64(): ParseResult<int_t.PrimInt64> {
        const ret = this.takeInt64();
        return mapOk(ret, x => new int_t.PrimInt64(x, true));
    }

    public takeDouble(): ParseResult<double> {
        try {
            const slice8 = this.consume(8);
            const ret: double = floatFromBytes(slice8);
            return Ok(ret);
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`caught non-Error error: ${e}`);
            }
        }
    }

    public takeIntNative(kind: kinds.IntKindNative): ParseResult<int_t.nativeint> {
        switch (kind) {
            case kinds.Int8Kind: {
                return this.takeInt8();
            }
            case kinds.Int16Kind: {
                return this.takeInt16();
            }
            case kinds.Int31Kind: {
                return this.takeInt31();
            }
            case kinds.Uint8Kind: {
                return this.takeUint8();
            }
            case kinds.Uint16Kind: {
                return this.takeUint16();
            }
            case kinds.Uint30Kind: {
                return this.takeUint30();
            }
            default: {
                throw new Error(`Implementation bug: no takeIntNative case matched ${kind}.`);
            }
        }
    }

    skipPadding<N extends number>(nBytes: N, validator: (padding: Uint8Array) => boolean = _ => true): ParseResult<true> {
        try {
            const _padding = this.consume(nBytes);
            if (validator(_padding)) {
                return Ok(true);
            } else {
                return Err(new TokenError(`Invalid padding bytes: ${HexString.bufToHex(_padding)}`));
            }
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`caught non-Error error: ${e}`);
            }
        }
    }

    takeFixed<N extends number>(len: N): ParseResult<Uint8Array> {
        try {
            const sliceN = this.consume(len);
            return Ok(sliceN);
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`caught non-Error error: ${e}`);
            }
        }
    }

    takeDynamic(): ParseResult<Uint8Array> {
        try {
            const sliceAll = this.consume(this.remainder);
            return Ok(sliceAll);
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                throw e;
            } else {
                throw new Error(`caught non-Error error: ${e}`);
            }
        }
    }

    /**
     * Unboxed primitive method for parsing a boolean value from the buffer,
     * used to implement higher-level combinators.
     *
     * @returns Interpreted boolean value of the parsed byte
     *
     * @throws {@link TokenError}
     * If the parsed byte is not a legal boolean representation (`0x00` or `0xff`)
     *
     * @throws {@link WindowError}
     * If the attempt to consume the requisite number of bytes
     * throws a {@link WindowError} due to context-frame overrun.
     */
    public takeBoolean(): ParseResult<boolean> {
        try {
            const byte: number = this.consumeByte();
            switch (byte) {
                case 0xff: {
                    return Ok(true);
                }
                case 0x00: {
                    return Ok(false);
                }
                default: {
                    return Err(TokenError.InvalidBoolean(byte));
                }
            }
        } catch (e: unknown) {
            if (e instanceof WindowError) {
                return Err(e);
            } else if (e instanceof Error) {
                // NOTE - this branch should be unreachable
                throw e;
            } else {
                throw new Error(`unexpected throw: ${e}`);
            }
        }
    }

    /**
     * Generic combinator for parsing a dynamic-length value whose final byte
     * can be identified by a byte-level predicate, without any outside context.
     *
     * The return value of this method is the sequence of bytes that
     * begins with the first unconsumed byte and ends with the first byte for which
     * the predicate function returned `true`. If the first unconsumed byte met
     * the predicate, the return value will be an array of length 1, which is the
     * minimum possible length of a valid return-value.
     *
     * This method is designed for parsing arbitrary-precision Zarith integers
     * and naturals, but it is not restricted to these uses alone.
     *
     * @param isTerminal predicate for termination, returning `true` only for the final byte to be included
     * @returns Array of consumed bytes, whose final index is the first byte for which the predicate returned `true`
     *
     * @throws {@link TokenError}
     * If every remaining byte in the current context-window failed the termination predicate.
     */
    public takeSelfTerminating(isTerminal: (byte: number) => boolean): ParseResult<Uint8Array> {
        const start = this._offset.index;
        const view = this._buffer.slice(start, this.viewLen);
        const terminalByte: number = view.findIndex(isTerminal);
        if (terminalByte === -1) {
            return Err(TokenError.NonTerminating(view));
        } else {
            try {
                return Ok(this.consume(terminalByte + 1));
            } catch (e: unknown) {
                if (e instanceof Error) {
                    throw new Error(`Unexpected error in assumed-infallible consume: ${e.message}`);
                } else {
                    throw new Error(`Unexpected non-error in assumed-infallible consume: ${e}`);
                }
            }
        }
    }
}