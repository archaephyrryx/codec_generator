import { Codec, ConstantLength } from '../codec';
import { onValue, unboxed } from '../core/box';
import { toBytes } from '../core/ops';
import { mapOk, unwrap } from '../core/result';
import { Decoder, liftDecoder, Reader } from '../decode';
import { Encode, Encoder, liftEncoder, OutputBytes, Writer } from '../encode';
import { Parser } from '../parse';
import { HexString } from '../runtime';
import { Target } from '../target';
import { getIntegerBytes, int16, int31, int32, int64, int8, IntegerBytes, PrimInt16, PrimInt31, PrimInt32, PrimInt64, PrimInt8, PrimUint16, PrimUint30, PrimUint8, uint16, uint30, uint8 } from './types';

export class Uint8 extends PrimUint8 implements Codec, ConstantLength<IntegerBytes<PrimUint8>> {
    readonly __constantLength__: void = undefined;

    public static decode(p: Parser): Uint8 {
        return unwrap(mapOk(p.takePrimUint8(), Uint8.promote));
    }

    public constructor(public readonly value: number, strict = false) {
        super(value, strict);
    }

    get constLen(): 1 {
        return getIntegerBytes(this.kind);
    }

    get encodeLength() {
        return this.constLen;
    }

    encode(): OutputBytes {
        return toBytes({ ...this.kind, ...this });
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeOne(this.value);
    }

    static readonly promote = onValue<uint8, Uint8>(x => new Uint8(x));
}

export const uint8_encoder: Encoder<uint8> = (value) => Uint8.promote(value).encode();

export const uint8_decoder: Decoder<uint8> = unboxed(Uint8.decode);

export const uint8_reader: Reader<uint8> = liftDecoder(uint8_decoder);

export const uint8_writer: Writer<uint8> = liftEncoder(uint8_encoder);

export class Int8 extends PrimInt8 implements Codec, ConstantLength<IntegerBytes<PrimInt8>> {
    readonly __constantLength__: void = undefined;

    public static decode(p: Parser): Int8 {
        return unwrap(mapOk(p.takePrimInt8(), Int8.promote));
    }

    public constructor(public readonly value: number, strict = false) {
        super(value, strict);
    }

    get constLen(): 1 {
        return getIntegerBytes(this.kind);
    }

    get encodeLength() {
        return this.constLen;
    }

    encode(): OutputBytes {
        return toBytes({ ...this.kind, ...this });
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeOne(this.value & 0xff);
    }

    static readonly promote = onValue<int8, Int8>(x => new Int8(x));
}

export const int8_encoder: Encoder<int8> = (value) => Int8.promote(value).encode();

export const int8_decoder: Decoder<int8> = unboxed(Int8.decode);

export const int8_reader: Reader<int8> = liftDecoder(int8_decoder);

export const int8_writer = liftEncoder(int8_encoder);

export class Uint16 extends PrimUint16 implements Codec, ConstantLength<IntegerBytes<PrimUint16>> {
    readonly __constantLength__: void = undefined;

    public static decode(p: Parser): Uint16 {
        return unwrap(mapOk(p.takePrimUint16(), Uint16.promote));
    }

    public constructor(public readonly value: number, strict = false) {
        super(value, strict);
    }

    get constLen(): 2 {
        return getIntegerBytes(this.kind);
    }

    get encodeLength() {
        return this.constLen;
    }

    encode(): OutputBytes {
        return toBytes({ ...this.kind, ...this });
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }

    static readonly promote = onValue<uint16, Uint16>(x => new Uint16(x));
}

export const uint16_encoder: Encoder<uint16> = (value) => Uint16.promote(value).encode();

export const uint16_decoder: Decoder<uint16> = unboxed(Uint16.decode);

export const uint16_reader: Reader<uint16> = liftDecoder(uint16_decoder);

export const uint16_writer: Writer<uint16> = liftEncoder(uint16_encoder);

export class Int16 extends PrimInt16 implements Codec, ConstantLength<IntegerBytes<PrimInt16>> {
    readonly __constantLength__: void = undefined;

    public static decode(p: Parser): Int16 {
        return unwrap(mapOk(p.takeInt16(), Int16.promote));
    }

    public constructor(public readonly value: number, strict = false) {
        super(value, strict);
    }

    get constLen(): 2 {
        return getIntegerBytes(this.kind);
    }

    get encodeLength() {
        return this.constLen;
    }

    encode(): OutputBytes {
        return toBytes({ ...this.kind, ...this });
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }

    static readonly promote = onValue<int16, Int16>(x => new Int16(x));
}

export const int16_encoder: Encoder<int16> = (value) => Int16.promote(value).encode();

export const int16_decoder: Decoder<int16> = unboxed(Int16.decode);

export const int16_reader: Reader<int16> = liftDecoder(int16_decoder);

export const int16_writer: Writer<int16> = liftEncoder(int16_encoder);

export class Uint30 extends PrimUint30 implements Codec, ConstantLength<IntegerBytes<PrimUint30>> {
    readonly __constantLength__: void = undefined;

    public static decode(p: Parser): Uint30 {
        return unwrap(mapOk(p.takePrimUint30(), Uint30.promote));
    }

    public constructor(public readonly value: number, strict = false) {
        super(value, strict);
    }

    get constLen(): 4 {
        return getIntegerBytes(this.kind);
    }

    get encodeLength() {
        return this.constLen;
    }

    encode(): OutputBytes {
        return toBytes({ ...this.kind, ...this });
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }

    static readonly promote = onValue<uint30, Uint30>(x => new Uint30(x));
}

export const uint30_encoder: Encoder<uint30> = (value) => {
    return Uint30.promote(value).encode();
};

export const uint30_decoder: Decoder<uint30> = unboxed(Uint30.decode);

export const uint30_reader: Reader<uint30> = liftDecoder(uint30_decoder);

export const writeTo = (value: uint30, buf: string): string => {
    const bytes = uint30_encoder(value);
    const hex = new HexString(bytes);
    return hex.appendHex(buf);
};

export class Int31 extends PrimInt31 implements Codec, ConstantLength<IntegerBytes<PrimInt31>> {
    readonly __constantLength__: void = undefined;

    public static decode(p: Parser): Int31 {
        return unwrap(mapOk(p.takePrimInt31(), Int31.promote));
    }

    public constructor(public readonly value: number, strict = false) {
        super(value, strict);
    }

    get constLen(): 4 {
        return getIntegerBytes(this.kind);
    }

    get encodeLength() {
        return this.constLen;
    }

    encode(): OutputBytes {
        return toBytes({ ...this.kind, ...this });
    }

    static encoder(other: Int31): OutputBytes {
        return other.encode();
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }

    static readonly promote = onValue<int31, Int31>(x => new Int31(x));
}

export const int31_encoder: Encoder<int31> = (value) => Int31.promote(value).encode();

export const int31_decoder: Decoder<int31> = unboxed(Int31.decode);

export const int31_reader: Reader<int31> = liftDecoder(int31_decoder);

export const int31_writer: Writer<int31> = liftEncoder(int31_encoder);

export class Int32 extends PrimInt32 implements Codec, ConstantLength<IntegerBytes<PrimInt32>> {
    readonly __constantLength__: void = undefined;

    public static decode(p: Parser): Int32 {
        return unwrap(mapOk(p.takePrimInt32(), Int32.promote));
    }

    public constructor(public readonly value: number, strict = false) {
        super(value, strict);
    }

    get constLen(): 4 {
        return getIntegerBytes(this.kind);
    }

    get encodeLength() {
        return this.constLen;
    }

    encode(): OutputBytes {
        return toBytes({ ...this.kind, ...this });
    }

    static encoder(other: Int32): OutputBytes {
        return other.encode();
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }

    static readonly promote = onValue<int32, Int32>(x => new Int32(x));
}

export const int32_encoder: Encoder<int32> = (value) => Int32.promote(value).encode();

export const int32_decoder: Decoder<int32> = unboxed(Int32.decode);

export const int32_reader: Reader<int32> = liftDecoder(int32_decoder);

export const int32_writer: Writer<int32> = liftEncoder(int32_encoder);

export class Int64 extends PrimInt64 implements Codec, ConstantLength<IntegerBytes<PrimInt64>> {
    readonly __constantLength__: void = undefined;

    public static decode(p: Parser): Int64 {
        return unwrap(mapOk(p.takePrimInt64(), Int64.promote));
    }


    public constructor(public readonly value: bigint, strict = false) {
        super(value, strict);
    }

    get constLen(): 8 {
        return getIntegerBytes(this.kind);
    }

    get encodeLength() {
        return this.constLen;
    }

    encode(): number[] {
        return toBytes({ ...this.kind, ...this });
    }

    static encoder: Encoder<Int64> = (val) => val.encode();

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }

    static readonly promote = onValue<int64, Int64>(x => new Int64(x));
}

export const int64_encoder: Encoder<int64> = (value) => Int64.promote(value).encode();

export const int64_decoder: Decoder<int64> = unboxed(Int64.decode);

export const int64_reader: Reader<int64> = liftDecoder(int64_decoder);

export const int64_writer: Writer<int64> = liftEncoder(int64_encoder);