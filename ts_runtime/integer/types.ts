import { NumericBounds, clampWith } from '../bounds';
import { Box } from '../core/box';
import * as kinds from './kinds';

export type uint8 = number;
export type int8 = number;
export type uint16 = number;
export type int16 = number;
export type uint30 = number;
export type int31 = number;
export type int32 = number;
export type int64 = bigint;

export type nativeint = uint8 | int8 | uint16 | int16 | uint30 | int31;

export class PrimUint8 extends Box<uint8> {
    public static readonly min: uint8 = 0x00;
    public static readonly max: uint8 = 0xff;

    public static readonly bounds: NumericBounds = { ...PrimUint8 };
    public static readonly kind: kinds.Uint8Kind = kinds.Uint8Kind;
    public readonly kind: kinds.Uint8Kind = kinds.Uint8Kind;

    public constructor(public readonly value: number, strict = false) {
        const val = clampWith(value, PrimUint8.bounds, { strict });
        super(val);
    }
}

export class PrimInt8 extends Box<int8> {
    public static readonly min: int8 = -0x80;
    public static readonly max: int8 = 0x7f;

    public static readonly bounds: NumericBounds = { ...PrimInt8 };
    public static readonly kind: kinds.Int8Kind = kinds.Int8Kind;

    public readonly kind: kinds.Int8Kind = kinds.Int8Kind;

    public constructor(public readonly value: number, strict = false) {
        const valid = clampWith(value, PrimInt8.bounds, { strict });
        super(valid);
    }
}

export class PrimUint16 extends Box<uint16> {
    public static readonly min: uint16 = 0x0000;
    public static readonly max: uint16 = 0xffff;

    public static readonly bounds: NumericBounds = { ...PrimUint16 };

    public readonly kind: kinds.Uint16Kind = kinds.Uint16Kind;
    public static readonly kind: kinds.Uint16Kind = kinds.Uint16Kind;

    public constructor(public readonly value: number, strict = false) {
        const valid = clampWith(value, PrimUint16.bounds, { strict });
        super(valid);
    }
}

export class PrimInt16 extends Box<int16> {
    public static readonly min: int16 = -0x8000;
    public static readonly max: int16 = 0x7fff;

    public static readonly bounds: NumericBounds = { ...PrimInt16 };

    public readonly kind: kinds.Int16Kind = kinds.Int16Kind;
    public static readonly kind: kinds.Int16Kind = kinds.Int16Kind;

    public constructor(public readonly value: number, strict = false) {
        const inRange = clampWith(value, PrimInt16.bounds, { strict });
        super(inRange);
    }
}

export class PrimUint30 extends Box<uint30> {
    public static readonly min: uint30 = 0x0000_0000;
    public static readonly max: uint30 = 0x3fff_ffff;

    public static readonly bounds: NumericBounds = { ...PrimUint30 };

    public static readonly kind: kinds.Uint30Kind = kinds.Uint30Kind;
    public readonly kind: kinds.Uint30Kind = kinds.Uint30Kind;

    public constructor(public readonly value: number, strict = false) {
        const inRange = clampWith(value, PrimUint30.bounds, { strict });
        super(inRange);
    }
}

export class PrimInt31 extends Box<int31> {
    public static readonly min: int31 = -0x4000_0000;
    public static readonly max: int31 = 0x3fff_ffff;

    public static readonly bounds: NumericBounds = { ...PrimInt31 };

    public readonly kind: kinds.Int31Kind = kinds.Int31Kind;
    public static readonly kind: kinds.Int31Kind = kinds.Int31Kind;

    public constructor(public readonly value: number, strict = false) {
        const valid = clampWith(value, PrimInt31.bounds, { strict });
        super(valid);
    }
}

export class PrimInt32 extends Box<int32> {
    public static readonly min: int32 = -0x8000_0000;
    public static readonly max: int32 = 0x7fff_ffff;

    public static readonly bounds: NumericBounds = { ...PrimInt32 };

    public readonly kind: kinds.Int32Kind = kinds.Int32Kind;
    public static readonly kind: kinds.Int32Kind = kinds.Int32Kind;

    public constructor(public readonly value: number, strict = false) {
        const valid = clampWith(value, PrimInt32.bounds, { strict });
        super(valid);
    }
}

export class PrimInt64 extends Box<int64> {
    public static readonly min: int64 = -0x8000_0000_0000_0000n;
    public static readonly max: int64 = 0x7fff_ffff_ffff_ffffn;

    public static readonly bounds: NumericBounds<bigint> = { ...PrimInt64 };

    public readonly kind: kinds.Int64Kind = kinds.Int64Kind;
    public static readonly kind: kinds.Int64Kind = kinds.Int64Kind;

    public constructor(public readonly value: int64, strict = false) {
        const valid = clampWith(value, PrimInt64.bounds, { strict });
        super(valid);
    }
}

export type PrimIntNative =
    PrimUint8 | PrimUint16 | PrimUint30 | PrimInt8 | PrimInt16 | PrimInt31

export type PrimIntNumber =
    PrimIntNative | PrimInt32;

export type PrimInt =
    PrimIntNumber | PrimInt64;

export const getIntegerBounds = (kind: kinds.IntKind): NumericBounds<number | bigint> => {
    switch (kind.bits) {
        case 64: return PrimInt64.bounds;
        case 32: return PrimInt32.bounds;
        default: {
            return getNativeBounds(kind);
        }
    }
};

export const getNativeBounds = (kind: kinds.IntKindNative): NumericBounds<nativeint> => {
    switch (kind.bits) {
        case 31: return PrimInt31.bounds;
        case 30: return PrimUint30.bounds;
        case 16: {
            if (kind.sig === 'signed') {
                return PrimInt16.bounds;
            } else {
                return PrimUint16.bounds;
            }
        }
        case 8: {
            if (kind.sig === 'signed') {
                return PrimInt8.bounds;
            } else {
                return PrimUint8.bounds;
            }
        }
    }
};

export const getIntegerName = (kind: kinds.IntKind): string => {
    return `${kind.sig === 'signed' ? 'Int' : 'Uint'}${kind.bits}`;
};




export function getIntegerBytes(kind: kinds.Uint8Kind): 1;
export function getIntegerBytes(kind: kinds.Int8Kind): 1;
export function getIntegerBytes(kind: kinds.Uint16Kind): 2;
export function getIntegerBytes(kind: kinds.Int16Kind): 2;
export function getIntegerBytes(kind: kinds.Uint30Kind): 4;
export function getIntegerBytes(kind: kinds.Int31Kind): 4;
export function getIntegerBytes(kind: kinds.Int32Kind): 4;
export function getIntegerBytes(kind: kinds.Int64Kind): 8;
export function getIntegerBytes<K extends kinds.IntKindNative>(kind: K): IntegerBytes<K>;
export function getIntegerBytes(kind: kinds.IntKind): number {
    return Math.ceil(kind.bits / 8);
}

type IntBytesTable = {
    [kinds.Uint8Kind.bits]: 1,
    [kinds.Uint16Kind.bits]: 2,
    [kinds.Uint30Kind.bits]: 4,
    [kinds.Int8Kind.bits]: 1,
    [kinds.Int16Kind.bits]: 2,
    [kinds.Int31Kind.bits]: 4,
    [kinds.Int32Kind.bits]: 4,
    [kinds.Int64Kind.bits]: 8,
}

export type PrimBytes<T extends PrimInt> = IntBytesTable[T['kind']['bits']]
export type KindBytes<T extends kinds.IntKind> = IntBytesTable[T['bits']]

export type IntegerBytes<T extends keyof kinds.BitType | kinds.IntKind | PrimInt> =
    T extends keyof kinds.BitType
    ? IntBytesTable[T]
    : T extends kinds.IntKind
    ? IntBytesTable[T['bits']]
    : T extends PrimInt
    ? IntBytesTable[T['kind']['bits']]
    : never
    ;

