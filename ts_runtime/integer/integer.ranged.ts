import { clampWith, NumericBounds } from '../bounds';
import { Codec } from '../codec';
import { Box } from '../core/box';
import { toBytes } from '../core/ops';
import { unwrap } from '../core/result';
import { liftPseudoProxyDecoder } from '../decode';
import { Encode, Encoder, OutputBytes } from '../encode';
import { RangedIntExt, RangedIntKind } from '../ext/ranged_int';
import { Parser } from '../parse';
import { Target } from '../target';
import { IntKindNative } from './kinds';
import { getIntegerBytes, getIntegerName, getNativeBounds, IntegerBytes, nativeint } from './types';

export type Kind = RangedIntKind;

export type Ext<K extends Kind = Kind, Min extends number = number, Max extends number = number> = RangedIntExt<K, Min, Max>;

/**
 * Integer value with type-level range constraints
 *
 * As there is currently no distinction at the type-level between the unboxed contents
 * of OCaml-native integer kinds, the value held is the numeric value, rather than the
 * binary-equivalent value.
 */
export class RangedInt<K extends Kind, Min extends number, Max extends number> extends Box<nativeint> implements Codec {
    protected get min(): Min { return this.bounds.min; }
    protected get max(): Max { return this.bounds.max; }

    public static readonly decode = <K extends Kind, Min extends number, Max extends number>(proxy: Ext<K, Min, Max>) => (p: Parser): RangedInt<K, Min, Max> => {
        const { min } = proxy.bounds;


        let shift: number;
        if (min > 0) {
            shift = min;
        } else {
            shift = 0;
        }

        const raw = unwrap(p.takeIntNative(proxy.intKind));
        return new RangedInt(raw + shift, proxy.intKind, proxy.bounds);
    };

    constructor(value: nativeint, public readonly kind: K, protected readonly bounds: NumericBounds<Min, Max>) {
        validateBounds(bounds, kind);
        super(clampWith(value, bounds, { strict: true, domain: 'integer' }));
    }

    __components(): Encode[] {
        return [this];
    }

    get constLen(): IntegerBytes<K> {
        return getIntegerBytes(this.kind);
    }

    get encodeLength(): number {
        return this.constLen;
    }

    encode(): OutputBytes {
        let shift: number;
        if (this.min > 0) {
            shift = this.min;
        } else {
            shift = 0;
        }
        if (this.value < this.min || this.value > this.max) {
            throw new Error(`Invalid instance of RangedInt: ${this.value} not in range [${this.min} , ${this.max}]`);
        }
        validateBounds(this.bounds, this.kind);
        const raw = this.value - shift;

        return toBytes({ ...this.kind, value: raw });
    }

    writeTarget<T extends Target>(tgt: T): number {
        return tgt.writeArray(this.encode());
    }
}

/**
 * Performs validation to ensure that the specific range of a RangedInt type
 * is conformant with the inherent range of the representative integer kind.
 *
 * @param bounds Range-bounds to validate
 * @param kind representative type for the RangedInt instance
 * @returns `true` (if no exception is thrown)
 *
 * @throws {@link RangeError}
 * If the bounds are inverted (bounds.min > bounds.max)
 *
 * @throws {@link RangeError}
 * If `bounds.min > 0` for a signed representative integer kind
 *
 * @throws {@link RangeError}
 * If the implicit range of `bounds` contains values outside of the inherent
 * range of the representative integer kind
 */
function validateBounds<K extends IntKindNative, Min extends number, Max extends number>(bounds: NumericBounds<Min, Max>, kind: K): true {
    const { min, max }: NumericBounds = getNativeBounds(kind);

    let rangeMin: number;
    let rangeMax: number;

    if (Number.isNaN(bounds.min) || Number.isNaN(bounds.max)) {
        throw new Error(`Invalid bounds: ${bounds.min} and ${bounds.max} cannot be NaN`);
    }

    if (bounds.min > bounds.max) {
        throw new RangeError(`Incoherent parameters: RangedInt Min (${bounds.min}) > Max (${bounds.max})`);
    }

    if (bounds.min > 0) {
        if (kind.sig === 'signed') {
            throw new RangeError(`Incoherent parameters: RangedInt cannot have signed backer with positive Min-bound (${bounds.min}) `);
        }

        rangeMin = 0;
        rangeMax = bounds.max - bounds.min;
    } else {
        rangeMin = bounds.min;
        rangeMax = bounds.max;
    }

    if (rangeMin < min || rangeMax > max) {
        throw new RangeError(`Incoherent parameters: RangedInt bounds [${bounds.min}, ${bounds.max}] are too wide for the chosen integer kind ${getIntegerName(kind)}`);
    }
    return true;
}

/**
 * Type alias for RangedInt that fills in missing generic parameters with their widest possible type
 *
 */
export type GenericRangedInt<K extends Kind = Kind, Min extends number = number, Max extends number = number> = RangedInt<K, Min, Max>;

export const rangedint_encoder = <K extends Kind, Min extends number, Max extends number>(kind: K, bounds: NumericBounds<Min, Max>): Encoder<nativeint> => (value) => new RangedInt(value, kind, bounds).encode();

export const rangedint_decoder = RangedInt.decode;

export const rangedint_reader = liftPseudoProxyDecoder(rangedint_decoder);