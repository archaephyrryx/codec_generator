export type Uint8Kind = { readonly sig: 'unsigned'; readonly bits: 8; };
export type Int8Kind = { readonly sig: 'signed'; readonly bits: 8; };
export type Uint16Kind = { readonly sig: 'unsigned'; readonly bits: 16; };
export type Int16Kind = { readonly sig: 'signed'; readonly bits: 16; };
export type Uint30Kind = { readonly sig: 'unsigned'; readonly bits: 30; };
export type Int31Kind = { readonly sig: 'signed'; readonly bits: 31; };
export type Int32Kind = { readonly sig: 'signed'; readonly bits: 32; };
export type Int64Kind = { readonly sig: 'signed'; readonly bits: 64; };

export const Uint8Kind: Uint8Kind = { sig: 'unsigned', bits: 8 };
export const Int8Kind: Int8Kind = { sig: 'signed', bits: 8 };
export const Uint16Kind: Uint16Kind = { sig: 'unsigned', bits: 16 };
export const Int16Kind: Int16Kind = { sig: 'signed', bits: 16 };
export const Uint30Kind: Uint30Kind = { sig: 'unsigned', bits: 30 };
export const Int31Kind: Int31Kind = { sig: 'signed', bits: 31 };
export const Int32Kind: Int32Kind = { sig: 'signed', bits: 32 };
export const Int64Kind: Int64Kind = { sig: 'signed', bits: 64 };

export type IntKindNative =
    Uint8Kind |
    Int8Kind |
    Uint16Kind |
    Int16Kind |
    Uint30Kind |
    Int31Kind;


export type IntKindNumber = IntKindNative | Int32Kind;

export type IntKind = IntKindNumber | Int64Kind;

export type BitType = {
    64: bigint;
    32: number;
    31: number;
    30: number;
    16: number;
    8: number;
};


export type TaggedIntNative = (IntKindNative & { readonly value: number })

export type TaggedIntNumber = TaggedIntNative | (Int32Kind & { readonly value: number })

export type TaggedBigInt = (Int64Kind & {
    readonly value: bigint;
});

export type TaggedInt = TaggedIntNumber | TaggedBigInt;