export function arr_eq<T>(x: readonly T[], y: readonly T[]): boolean {
    return x.length === y.length && x.every((el, ix) => el === y[ix]);
}

export function mem_eq(x: Uint8Array | readonly number[], y: Uint8Array | readonly number[]) {
    const xbuf = Buffer.from(x);
    const ybuf = Buffer.from(y);
    return xbuf.compare(ybuf) === 0;
}

export function mem_is0(x: Uint8Array) {
    return Buffer.from(x).equals(Buffer.alloc(x.byteLength));
}