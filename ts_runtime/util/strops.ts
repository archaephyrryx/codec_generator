export function strToBytes(u8str: string): Buffer {
    return Buffer.from(u8str, 'utf8');
}

export function strByteLen(u8str: string): number {
    return Buffer.byteLength(u8str, 'utf8');
}

export function strFromBytes(bytes: Uint8Array | readonly number[]): string {
    return Buffer.from(bytes).toString('utf8');
}