import { OutputBytes } from '../encode';
import { mem_eq } from './arrops';

const parseHex: (word: string) => number = (word) => parseInt(word, 16);
const byteToHex: (byte: number) => string = (byte) => byte.toString(16).padStart(2, '0');

export type NonHexReason = 'unspecified' | 'parity' | 'character';

export class NonHexError extends Error {
    readonly reason: NonHexReason;

    constructor(msg: string, reason: NonHexReason = 'unspecified') {
        super(msg);
        Object.setPrototypeOf(this, NonHexError.prototype);
        this.reason = reason;
    }
}

export class HexString extends Uint8Array {
    static fromBytes(bytes: readonly number[]) {
        return new HexString(bytes);
    }

    /**
     * Static converter that narrows a Uint8Array into a HexString
     *
     * @param arr Array to promote to HexString
     * @returns HexString wrapper of the provided array
     */
    static promote(arr: Uint8Array): HexString {
        return new HexString(arr);
    }

    /**
     * Static factory that converts strings into HexStrings, provided
     * that they are valid as hex-string encodings.
     *
     * In order to be a valid hex-string encoding, a string must have even length,
     * and consist entirely of valid (ascii) hex-digits. These conditions are tested
     * simultaneously, and result in the same error being thrown; however, the instance
     * of the error may carry additional information regarding the particular condition
     * that was violated by the input string.
     *
     * @param maybeHex string to convert, which is validated by this method
     * @returns the interpreted HexString
     *
     * @throws {@link NonHexError}
     * This exception is thrown if the provided string is invalid for any reason.
     */
    public static fromString(maybeHex: string): HexString {
        return this.promote(this.fromStringRaw(maybeHex));
    }

    /**
     * Variant of {@link HexString.fromString} that performs the same core operation, but returns
     * an original `Uint8Array` rather than a constructed `HexString`.
          *
     * @param maybeHex string to convert, which is validated by this method
     * @returns the interpreted Uint8Array
     *
     * @throws {@link NonHexError}
     * This exception is thrown if the provided string is invalid for any reason.
     */
    public static fromStringRaw(maybeHex: string): Uint8Array {
        /* We simplify the case-logic by converting the argument to lowercase */
        const canonical = maybeHex.toLowerCase();

        /* We use a test-pattern that matches all valid lowercase hex-strings */
        if (/^(([a-f]|\d){2})*$/.test(canonical)) {
            const arr: ReadonlyArray<number> = (canonical.match(/([a-f]|\d){2}/g) ?? []).map(parseHex);
            return new Uint8Array([...arr]);
        } else {
            throw new NonHexError('Invalid hex string');
        }
    }

    /**
     * Outputs a string containing a hexadecimal representation of the byte array as an
     * undelimited raw hexadecimal sequence
     *
     * @returns hex-string representation of the contained bytes
     */
    public toHex(): string {
        return HexString.bufToHex(this);
    }

    public equals(other: string | readonly number[] | Uint8Array): boolean {
        if (typeof other === 'string') {
            return this.equalsString(other);
        } else {
            return this.equalsBytes(other);
        }
    }

    equalsBytes(other: Uint8Array | readonly number[]): boolean {
        return mem_eq(this, other);
    }

    equalsString(other: string): boolean {
        return mem_eq(this, HexString.fromStringRaw(other));
    }

    public static bufToHex(buf: Uint8Array | readonly number[]): string {
        const bytes = [...Buffer.from(buf)];

        return bytes.map(byteToHex).join('');
    }

    /**
     * Appends the hex-converted bytes of a HexString to an existing
     * string.
     *
     * @param buf Existing string to append to
     * @returns the concatenation of buf and the stringified receiver value
     */
    public appendHex(buf: string): string {
        return buf + this.toHex();
    }
}