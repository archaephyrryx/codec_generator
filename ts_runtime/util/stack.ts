export interface LIFO<T> {
    push(val: T): void;
    pop(): T | undefined;
    peek(): T | undefined;
    peekOr(def: T): T;
    size(): number;
}

export class Stack<T> implements LIFO<T> {
    /**
     * Initializes a new stack, empty if no argument is provided
     *
     * @param _stack Array to initialize stack from, with highest index value at the top of the stack
     */
    constructor(protected _stack: T[] = new Array<T>()) { }

    /**
     * push: Pushes a value to the top of the stack
     *
     * @param val Value to push onto the stack
     */
    public push(val: T) {
        this._stack.push(val);
    }

    /**
     * pop: Removes and returns the top element of the stack
     *
     * @returns Topmost element of the stack, or `undefined` if empty
     */
    public pop(): T | undefined {
        return this._stack.pop();
    }

    public peek(): T | undefined {
        const len = this._stack.length;

        if (len == 0) {
            return undefined;
        } else {
            return this._stack[len - 1];
        }
    }

    public peekOr(def: T): T {
        const len = this._stack.length;

        if (len == 0) {
            return def;
        } else {
            return this._stack[len - 1];
        }
    }

    public size(): number {
        return this._stack.length;
    }
}