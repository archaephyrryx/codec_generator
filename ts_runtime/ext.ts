import PaddedExt from './ext/padded';
import { RangedIntExt } from './ext/ranged_int';
import RangedFloatExt from './ext/ranged_float';
import FixedLenExt from './ext/fixed_len';
import DynamicExt from './ext/dynamic';
import DefaultExt from './ext/dft';
import RecordExt from './ext/record';

/** Filler type for invariant schema types (i.e. ones that do not have any extensions) */
export type NoExt = {}

/** Union over all non-degenerate extension objects */
export type SomeExt = RangedIntExt | RangedFloatExt | FixedLenExt | PaddedExt | DynamicExt | DefaultExt | RecordExt

/** All possible transcoder extension objects, including the empty extension object */
export type AnyExt = SomeExt | NoExt