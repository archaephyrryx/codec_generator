export { lenBytes as natBytes } from './zarith/natural';
export { lenBytes as intBytes } from './zarith/integer';

export * as n_type from './zarith/natural';
export * as z_type from './zarith/integer';

