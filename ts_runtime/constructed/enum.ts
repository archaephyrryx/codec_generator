import { Width } from "../core/width";
import width from "../core/width.type";
import { OutputBytes } from "../encode";
import { Parser } from "../parse";

export const enum_encoder = (size: width) => <Enum extends number>(value: Enum): OutputBytes => Width.from(size, value).encode()

export const enum_decoder = (size: Exclude<width, width.N>) => <Enum extends number>(f: (value: number) => value is Enum) => (p: Parser): Enum => {
    const raw = Width.decode(size)(p).value;
    if (f(raw)) {
        return raw;
    } else {
        throw new Error(`Invalid enum member ${raw}`);
    }
}