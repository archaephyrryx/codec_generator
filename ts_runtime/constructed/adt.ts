import { Codec } from '../codec';
import { width_decoder, width_encoder } from '../core/width';
import width from '../core/width.type';
import { Decoder } from '../decode';

export type TagWidth = width.Uint8 | width.Uint16;

export interface Variant<U extends number, T extends Codec> { kind: U, value: T };

export type KindOf<T extends Variant<number, Codec>> = T['kind']

export function kindOf<T extends Variant<number, Codec>>(x: T): KindOf<T> {
    return x.kind;
}

export type ValueOf<T extends Variant<number, Codec>> = T['value'];

export function valueOf<T extends Variant<number, Codec>>(x: T): ValueOf<T> {
    return x.value;
}

export interface VariantDecoder<U extends number, T extends Variant<U, Codec>> {
    (disc: U): Decoder<ValueOf<T>>,
    isValid: (tagval: number) => tagval is U,
}

export const variant_encoder = <U extends number, T extends Variant<U, Codec>>(tag_width: TagWidth) => ({ kind, value }: T) => {
    const tagbytes = width_encoder(tag_width)(kind);
    return [...tagbytes, ...value.encode()];
}

export const variant_decoder = (tag_width: TagWidth) => <U extends number, T extends Variant<U, Codec>>(vardec: VariantDecoder<U, T>): Decoder<T> => (p) => {
    const tag: number = width_decoder(tag_width)(p);
    if (!vardec.isValid(tag)) {
        throw new Error(`variant_decoder: tag ${tag} is not valid for unknown ADT codec`);
    }
    return { kind: tag, value: vardec(tag)(p) } as T;
}