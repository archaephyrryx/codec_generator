import { Codec } from '../codec';
import { Dynamic } from '../composite/dynamic';
import { Option } from '../composite/opt/option';
import { Some } from '../composite/opt/option.type';
import { Box } from '../core/box';
import width from '../core/width.type';
import { Decoder, liftDecoder } from '../decode';
import { Encode, OutputBytes } from '../encode';
import RecordExt from '../ext/record';
import { Parser } from '../parse';
import { U8String } from '../primitive/string';
import { Target } from '../target';
import { record_decoder } from './record';

type PersonUnboxed = { first: Dynamic<U8String, width.Uint30>, middle: Option<Dynamic<U8String, width.Uint30>>, last: Dynamic<U8String, width.Uint30> }

const traversal: RecordExt<Record<keyof PersonUnboxed, unknown>> = { order: ['first', 'middle', 'last'] };

class Person extends Box<PersonUnboxed> implements Codec {
    readonly order: typeof traversal['order'] = traversal.order;

    __components(): Encode[] {
        return this.order.map(key => this.value[key], this);
    }

    encode(): OutputBytes {
        return this.order.flatMap(key => this.value[key].encode(), this);
    }

    static readonly decode: Decoder<Person> = (p: Parser) => {
        const value: PersonUnboxed =
            record_decoder<PersonUnboxed>(
                {
                    first: Dynamic.decode(U8String.decode, width.Uint30),
                    middle: Option.decode(Dynamic.decode(U8String.decode, width.Uint30)),
                    last: Dynamic.decode(U8String.decode, width.Uint30)
                }, traversal)(p);

        return new Person(value);
    }

    writeTarget<T extends Target>(tgt: T): number {
        return this.order.reduce((acc, key) => this.value[key].writeTarget(tgt) + acc, 0);
    }

    get encodeLength(): number {
        return this.order.reduce((acc, key) => acc + this.value[key].encodeLength, 0);
    }
}

test('person', () => {
    const promoter = Dynamic.promoter(width.Uint30);
    const first = promoter(U8String.fromString("Haskell"))
    const middle = Option.promote(Some(promoter(U8String.fromString("Brooks"))));
    const last = promoter(U8String.fromString("Curry"));


    expect(liftDecoder(Person.decode)([...first.encode(), ...middle.encode(), ...last.encode()]).value).toStrictEqual({ first, middle, last });
    expect(new Person({ first, middle, last }).encode()).toEqual([...first.encode(), ...middle.encode(), ...last.encode()]);
})
