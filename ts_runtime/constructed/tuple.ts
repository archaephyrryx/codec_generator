import { Codec } from '../codec';
import { Box, onValue } from '../core/box';
import { Decoder, ProxyBase } from '../decode';
import { OutputBytes } from '../encode';

export type Tuple = Codec[];

export type Distributed<T extends Tuple> =
    T extends [] ? [] :
    T extends [infer X, ...infer U]
    ? U extends Tuple
    ? [Decoder<X>, ...Distributed<U>]
    : never
    : never;

export type AnyDecoders = Decoder<Codec>[]

export type Unified<D extends AnyDecoders> =
    D extends [] ? [] :
    D extends [Decoder<infer X>, ...infer U]
    ? U extends AnyDecoders
    ? [X, ...Unified<U>]
    : never
    : never;

function isDepleted(arr: Array<unknown>): arr is [] {
    return (arr.length === 0);
}

function mergeDecoders<D extends AnyDecoders>(...decs: D): Decoder<Unified<D>> {
    return (p) => {
        if (isDepleted(decs)) {
            return decs as Unified<D>;
        } else {
            const [head, ...tail] = decs;
            const headElem = head(p);
            const tailElems = mergeDecoders(...tail)(p);
            return [headElem, ...tailElems];
        }
    }
}

export function spreadDecoders<D extends AnyDecoders>(...decs: D): ProxyBase<Unified<D>> {
    return { dec: mergeDecoders(...decs) }
}

export type Identity<T extends Tuple> = Unified<Distributed<T>>;

export const tuple_encoder = <T extends Tuple, B extends Box<T> = Box<T>>(val: T | B) => {
    return onValue<T, OutputBytes>(value => value.flatMap(elem => elem.encode()))(val);
}

export const tuple_decoder = <T extends Tuple>(...decs: Distributed<T>): Decoder<Identity<T>> => mergeDecoders<Distributed<T>>(...decs);