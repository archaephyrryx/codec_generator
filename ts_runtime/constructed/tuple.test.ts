import { Codec, ConstantLength } from '../codec';
import { Box, unbox } from '../core/box';
import { Decoder, liftDecoder } from '../decode';
import { OutputBytes } from '../encode';
import { Uint8 } from '../integer/integer';
import { Parser } from '../parse';
import { Target } from '../target';
import { tuple_decoder, tuple_encoder } from './tuple';

type IPv4Unboxed = [Uint8, Uint8, Uint8, Uint8];

class IPv4 extends Box<IPv4Unboxed> implements Codec, ConstantLength<4> {
    __constantLength__: void = undefined;

    encode(): OutputBytes {
        return tuple_encoder(this.value);
    }

    static encoder(other: IPv4): OutputBytes {
        return other.encode();
    }

    static readonly decode: Decoder<IPv4> = (p: Parser) => {
        const value = tuple_decoder<IPv4Unboxed>(Uint8.decode, Uint8.decode, Uint8.decode, Uint8.decode)(p);
        return new IPv4(value);
    }

    writeTarget<T extends Target>(tgt: T): number {
        return this.value.reduce((acc, elem) => acc + elem.writeTarget(tgt), 0);
    }

    get encodeLength() {
        return this.constLen;
    }

    get constLen(): 4 {
        return 4;
    }
}

test('ipv4', () => {
    expect(liftDecoder(IPv4.decode)([127, 0, 0, 1]).value.flatMap(unbox)).toEqual([127, 0, 0, 1])
    expect(new IPv4([new Uint8(192), new Uint8(168), new Uint8(0), new Uint8(1)]).encode()).toStrictEqual([192, 168, 0, 1]);
})