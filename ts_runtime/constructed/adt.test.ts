import { Encoder } from '../encode';
import { Decoder } from '../decode';
import width from '../core/width.type';
import { valueOf, ValueOf, VariantDecoder, variant_encoder } from './adt';
import { Bool, FALSE, TRUE } from '../primitive/bool';
import { bool_type } from '../primitive';
import { Int64 } from '../integer/integer';
import { Codec } from '../codec';

export enum EitherTag {
    LEFT = 0,
    RIGHT = 1,
};

interface EitherLeft<A extends Codec> {
    kind: EitherTag.LEFT,
    value: A
}

interface EitherRight<B extends Codec> {
    kind: EitherTag.RIGHT,
    value: B
}

type Either<A extends Codec, B extends Codec> = EitherLeft<A> | EitherRight<B>;

function eitherDecoder<A extends Codec, B extends Codec>(left: Decoder<A>, right: Decoder<B>): VariantDecoder<EitherTag, Either<A, B>> {
    function f(disc: EitherTag) {
        switch (disc) {
            case EitherTag.LEFT: return left;
            case EitherTag.RIGHT: return right;
        }
    }
    f.isValid = (tagval: number): tagval is EitherTag => Object.values(EitherTag).includes(tagval);
    return f;
}

const Left = <A extends Codec, B extends Codec>(left: A): Either<A, B> => ({ kind: EitherTag.LEFT, value: left });
const Right = <A extends Codec, B extends Codec>(right: B): Either<A, B> => ({ kind: EitherTag.RIGHT, value: right });

const either_encoder = <A extends Codec, B extends Codec>(l: Encoder<A>, r: Encoder<B>): Encoder<Either<A, B>> =>
    variant_encoder<EitherTag, Either<A, B>>(width.Uint8);

test('either', () => {
    const encoder = either_encoder<Bool, Int64>(Bool.encoder, Int64.encoder);
    expect(encoder(Left(TRUE))).toEqual([0x00, 0xff]);
    expect(encoder(Left(FALSE))).toEqual([0x00, 0x00]);
    expect(encoder(Right(Int64.promote(0n)))).toEqual([0x01, ...new Array(8).fill(0x00)]);
})