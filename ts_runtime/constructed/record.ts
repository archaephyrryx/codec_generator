import { Codec } from '../codec';
import { Decoder, ProxyBase } from '../decode';
import { Encoder } from '../encode';
import Ext from '../ext/record';

export type SchemaRecord = Record<string, Codec>

export type DecoderTable<T extends SchemaRecord> = {
    [K in keyof T]: Decoder<T[K]>
}

export type AnyTable = Record<string, Decoder<Codec>>

export type RecordFromTable<D extends AnyTable> = {
    [K in keyof D]: ReturnType<D[K]>
}

function composeTable<D extends AnyTable>(decs: D, hint: Ext<D>): Decoder<RecordFromTable<D>> {
    return (p) => {
        let ret: Record<keyof D, Codec> = {} as Record<keyof D, Codec>;
        for (const key of hint.order) {
            ret[key] = decs[key](p);
        }
        return ret as RecordFromTable<D>;
    }
}

export function mapTable<D extends AnyTable>(decs: D, hint: Ext<D>): ProxyBase<RecordFromTable<D>> {
    return { dec: composeTable(decs, hint) }
}

export type Identity<T extends SchemaRecord> = RecordFromTable<DecoderTable<T>>;

export function identify<R extends SchemaRecord>(_value: Identity<R>): asserts _value is R & Identity<R> {
    return;
}

export const record_decoder = <T extends SchemaRecord>(decs: DecoderTable<T>, hint: Ext<DecoderTable<T>>): Decoder<Identity<T>> => composeTable<DecoderTable<T>>(decs, hint);

export const record_encoder = <R extends SchemaRecord>(hint: Ext<R>): Encoder<R> => (val: R) => {
    return hint.order.flatMap(key => val[key].encode());
}
