import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export type AlphaOperationRaw = { branch: CGRIDClass__OperationShell_header_branch, data: Bytes };
export class CGRIDClass__AlphaOperationRaw extends Box<AlphaOperationRaw> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'data']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationRaw {
        return new this(record_decoder<AlphaOperationRaw>({branch: CGRIDClass__OperationShell_header_branch.decode, data: Bytes.decode}, {order: ['branch', 'data']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.data.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.data.writeTarget(tgt));
    }
}
export const alpha_operation_raw_encoder = (value: AlphaOperationRaw): OutputBytes => {
    return record_encoder({order: ['branch', 'data']})(value);
}
export const alpha_operation_raw_decoder = (p: Parser): AlphaOperationRaw => {
    return record_decoder<AlphaOperationRaw>({branch: CGRIDClass__OperationShell_header_branch.decode, data: Bytes.decode}, {order: ['branch', 'data']})(p);
}
