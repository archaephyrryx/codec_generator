import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Nullable } from '../../ts_runtime/composite/opt/nullable';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { SequenceBounded } from '../../ts_runtime/composite/seq/sequence.bounded';
import { VPadded } from '../../ts_runtime/composite/vpadded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int16, Int31, Int32, Int64, Int8, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Bls generated for PublicKey__Bls
export class CGRIDClass__PublicKey__Bls extends Box<PublicKey__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Bls {
        return new this(record_decoder<PublicKey__Bls>({bls12_381_public_key: FixedBytes.decode<48>({len: 48})}, {order: ['bls12_381_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Bls generated for PublicKeyHash__Bls
export class CGRIDClass__PublicKeyHash__Bls extends Box<PublicKeyHash__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Bls {
        return new this(record_decoder<PublicKeyHash__Bls>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__String generated for MichelineAlphaMichelsonV1Expression__String
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__String extends Box<MichelineAlphaMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__String {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence generated for MichelineAlphaMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence extends Box<MichelineAlphaMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineAlphaMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic generated for MichelineAlphaMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic extends Box<MichelineAlphaMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineAlphaMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Int generated for MichelineAlphaMichelsonV1Expression__Int
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Int extends Box<MichelineAlphaMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes generated for MichelineAlphaMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes extends Box<MichelineAlphaMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
// Class CGRIDClass__BlsSignaturePrefix__Bls_prefix generated for BlsSignaturePrefix__Bls_prefix
export class CGRIDClass__BlsSignaturePrefix__Bls_prefix extends Box<BlsSignaturePrefix__Bls_prefix> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__BlsSignaturePrefix__Bls_prefix {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_update generated for AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_update
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_update extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'update']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_update {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_update>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, zk_rollup: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_zk_rollup.decode, update: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'update']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.zk_rollup.encodeLength +  this.value.update.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.zk_rollup.writeTarget(tgt) +  this.value.update.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_publish generated for AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_publish
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_publish extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_publish> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'op']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_publish {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_publish>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, zk_rollup: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_zk_rollup.decode, op: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'op']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.zk_rollup.encodeLength +  this.value.op.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.zk_rollup.writeTarget(tgt) +  this.value.op.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_origination generated for AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_origination
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_origination extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_parameters', 'circuits_info', 'init_state', 'nb_ops']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_origination {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_origination>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_parameters: Dynamic.decode(Bytes.decode, width.Uint30), circuits_info: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq.decode), width.Uint30), init_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30), nb_ops: Int31.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_parameters', 'circuits_info', 'init_state', 'nb_ops']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_parameters.encodeLength +  this.value.circuits_info.encodeLength +  this.value.init_state.encodeLength +  this.value.nb_ops.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_parameters.writeTarget(tgt) +  this.value.circuits_info.writeTarget(tgt) +  this.value.init_state.writeTarget(tgt) +  this.value.nb_ops.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Vdf_revelation generated for AlphaOperationAlphaContentsOrSignaturePrefix__Vdf_revelation
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Vdf_revelation extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Vdf_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['solution']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Vdf_revelation {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Vdf_revelation>({solution: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Vdf_revelation_solution.decode}, {order: ['solution']})(p));
    };
    get encodeLength(): number {
        return (this.value.solution.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.solution.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Update_consensus_key generated for AlphaOperationAlphaContentsOrSignaturePrefix__Update_consensus_key
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Update_consensus_key extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Update_consensus_key> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pk']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Update_consensus_key {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Update_consensus_key>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Update_consensus_key_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, pk: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Update_consensus_key_pk.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pk']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.pk.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.pk.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_submit_batch generated for AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_submit_batch
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_submit_batch extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_submit_batch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'content', 'burn_limit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_submit_batch {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_submit_batch>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_submit_batch_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode, content: Dynamic.decode(U8String.decode, width.Uint30), burn_limit: Option.decode(N.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'content', 'burn_limit']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.content.encodeLength +  this.value.burn_limit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.content.writeTarget(tgt) +  this.value.burn_limit.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_return_bond generated for AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_return_bond
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_return_bond extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_return_bond> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_return_bond {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_return_bond>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_return_bond_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_remove_commitment generated for AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_remove_commitment
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_remove_commitment extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_remove_commitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_remove_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_remove_commitment>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_remove_commitment_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_rejection generated for AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_rejection
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_rejection extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_rejection> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'level', 'message', 'message_position', 'message_path', 'message_result_hash', 'message_result_path', 'previous_message_result', 'previous_message_result_path', 'proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_rejection {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_rejection>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode, level: Int32.decode, message: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message.decode, message_position: N.decode, message_path: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_path_denest_dyn_denest_seq.decode), width.Uint30), message_result_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_result_hash.decode, message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), previous_message_result: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result.decode, previous_message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), proof: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'level', 'message', 'message_position', 'message_path', 'message_result_hash', 'message_result_path', 'previous_message_result', 'previous_message_result_path', 'proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.level.encodeLength +  this.value.message.encodeLength +  this.value.message_position.encodeLength +  this.value.message_path.encodeLength +  this.value.message_result_hash.encodeLength +  this.value.message_result_path.encodeLength +  this.value.previous_message_result.encodeLength +  this.value.previous_message_result_path.encodeLength +  this.value.proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.message.writeTarget(tgt) +  this.value.message_position.writeTarget(tgt) +  this.value.message_path.writeTarget(tgt) +  this.value.message_result_hash.writeTarget(tgt) +  this.value.message_result_path.writeTarget(tgt) +  this.value.previous_message_result.writeTarget(tgt) +  this.value.previous_message_result_path.writeTarget(tgt) +  this.value.proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_origination generated for AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_origination
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_origination extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup_origination']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_origination {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_origination>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, tx_rollup_origination: Unit.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup_origination']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.tx_rollup_origination.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.tx_rollup_origination.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_finalize_commitment generated for AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_finalize_commitment
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_finalize_commitment extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_finalize_commitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_finalize_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_finalize_commitment>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_finalize_commitment_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_dispatch_tickets generated for AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_dispatch_tickets
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_dispatch_tickets extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_dispatch_tickets> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup', 'level', 'context_hash', 'message_index', 'message_result_path', 'tickets_info']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_dispatch_tickets {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_dispatch_tickets>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, tx_rollup: CGRIDClass__AlphaTx_rollup_id.decode, level: Int32.decode, context_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_context_hash.decode, message_index: Int31.decode, message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), tickets_info: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup', 'level', 'context_hash', 'message_index', 'message_result_path', 'tickets_info']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.tx_rollup.encodeLength +  this.value.level.encodeLength +  this.value.context_hash.encodeLength +  this.value.message_index.encodeLength +  this.value.message_result_path.encodeLength +  this.value.tickets_info.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.tx_rollup.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.context_hash.writeTarget(tgt) +  this.value.message_index.writeTarget(tgt) +  this.value.message_result_path.writeTarget(tgt) +  this.value.tickets_info.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_commit generated for AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_commit
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_commit extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_commit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_commit {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_commit>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode, commitment: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Transfer_ticket generated for AlphaOperationAlphaContentsOrSignaturePrefix__Transfer_ticket
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Transfer_ticket extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Transfer_ticket> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'ticket_contents', 'ticket_ty', 'ticket_ticketer', 'ticket_amount', 'destination', 'entrypoint']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Transfer_ticket {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Transfer_ticket>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transfer_ticket_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, ticket_contents: Dynamic.decode(Bytes.decode, width.Uint30), ticket_ty: Dynamic.decode(Bytes.decode, width.Uint30), ticket_ticketer: CGRIDClass__AlphaContract_id.decode, ticket_amount: N.decode, destination: CGRIDClass__AlphaContract_id.decode, entrypoint: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'ticket_contents', 'ticket_ty', 'ticket_ticketer', 'ticket_amount', 'destination', 'entrypoint']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.ticket_contents.encodeLength +  this.value.ticket_ty.encodeLength +  this.value.ticket_ticketer.encodeLength +  this.value.ticket_amount.encodeLength +  this.value.destination.encodeLength +  this.value.entrypoint.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.ticket_contents.writeTarget(tgt) +  this.value.ticket_ty.writeTarget(tgt) +  this.value.ticket_ticketer.writeTarget(tgt) +  this.value.ticket_amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.entrypoint.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Transaction generated for AlphaOperationAlphaContentsOrSignaturePrefix__Transaction
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Transaction extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Transaction {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Transaction>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transaction_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: N.decode, destination: CGRIDClass__AlphaContract_id.decode, parameters: Option.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transaction_parameters.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_timeout generated for AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_timeout
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_timeout extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_timeout> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'stakers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_timeout {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_timeout>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaSmart_rollup_address.decode, stakers: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'stakers']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.stakers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.stakers.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_refute generated for AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_refute
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_refute extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_refute> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'opponent', 'refutation']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_refute {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_refute>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaSmart_rollup_address.decode, opponent: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_opponent.decode, refutation: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'opponent', 'refutation']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.opponent.encodeLength +  this.value.refutation.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.opponent.writeTarget(tgt) +  this.value.refutation.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_recover_bond generated for AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_recover_bond
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_recover_bond extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_recover_bond> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'staker']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_recover_bond {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_recover_bond>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_rollup.decode, staker: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_staker.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'staker']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.staker.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.staker.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_publish generated for AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_publish
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_publish extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_publish> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_publish {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_publish>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaSmart_rollup_address.decode, commitment: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_originate generated for AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_originate
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_originate extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_originate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pvm_kind', 'kernel', 'origination_proof', 'parameters_ty']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_originate {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_originate>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_originate_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, pvm_kind: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_originate_pvm_kind.decode, kernel: Dynamic.decode(U8String.decode, width.Uint30), origination_proof: Dynamic.decode(U8String.decode, width.Uint30), parameters_ty: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pvm_kind', 'kernel', 'origination_proof', 'parameters_ty']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.pvm_kind.encodeLength +  this.value.kernel.encodeLength +  this.value.origination_proof.encodeLength +  this.value.parameters_ty.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.pvm_kind.writeTarget(tgt) +  this.value.kernel.writeTarget(tgt) +  this.value.origination_proof.writeTarget(tgt) +  this.value.parameters_ty.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_execute_outbox_message generated for AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_execute_outbox_message
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_execute_outbox_message extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_execute_outbox_message> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'cemented_commitment', 'output_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_execute_outbox_message {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_execute_outbox_message>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_execute_outbox_message_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaSmart_rollup_address.decode, cemented_commitment: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_execute_outbox_message_cemented_commitment.decode, output_proof: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'cemented_commitment', 'output_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.cemented_commitment.encodeLength +  this.value.output_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.cemented_commitment.writeTarget(tgt) +  this.value.output_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_cement generated for AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_cement
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_cement extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_cement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_cement {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_cement>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_cement_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaSmart_rollup_address.decode, commitment: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_cement_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_add_messages generated for AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_add_messages
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_add_messages extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_add_messages> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'message']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_add_messages {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_add_messages>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_add_messages_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, message: Dynamic.decode(Sequence.decode(Dynamic.decode(U8String.decode, width.Uint30)), width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'message']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.message.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.message.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Signature_prefix generated for AlphaOperationAlphaContentsOrSignaturePrefix__Signature_prefix
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Signature_prefix extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Signature_prefix> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_prefix']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Signature_prefix {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Signature_prefix>({signature_prefix: CGRIDClass__Bls_signature_prefix.decode}, {order: ['signature_prefix']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_prefix.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_prefix.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Set_deposits_limit generated for AlphaOperationAlphaContentsOrSignaturePrefix__Set_deposits_limit
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Set_deposits_limit extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Set_deposits_limit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'limit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Set_deposits_limit {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Set_deposits_limit>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Set_deposits_limit_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, limit: Option.decode(N.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'limit']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.limit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.limit.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Seed_nonce_revelation generated for AlphaOperationAlphaContentsOrSignaturePrefix__Seed_nonce_revelation
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Seed_nonce_revelation extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Seed_nonce_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Seed_nonce_revelation {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Seed_nonce_revelation>({level: Int32.decode, nonce: FixedBytes.decode<32>({len: 32})}, {order: ['level', 'nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Reveal generated for AlphaOperationAlphaContentsOrSignaturePrefix__Reveal
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Reveal extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Reveal {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Reveal>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Reveal_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_key: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Reveal_public_key.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Register_global_constant generated for AlphaOperationAlphaContentsOrSignaturePrefix__Register_global_constant
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Register_global_constant extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Register_global_constant> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Register_global_constant {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Register_global_constant>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Register_global_constant_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Proposals generated for AlphaOperationAlphaContentsOrSignaturePrefix__Proposals
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Proposals extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Proposals> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposals']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Proposals {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Proposals>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Proposals_source.decode, period: Int32.decode, proposals: Dynamic.decode(SequenceBounded.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Proposals_proposals_denest_dyn_denest_seq.decode, 20, None), width.Uint30)}, {order: ['source', 'period', 'proposals']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposals.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposals.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Preendorsement generated for AlphaOperationAlphaContentsOrSignaturePrefix__Preendorsement
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Preendorsement extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Preendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Preendorsement {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Preendorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Preendorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Origination generated for AlphaOperationAlphaContentsOrSignaturePrefix__Origination
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Origination extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Origination {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Origination>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, balance: N.decode, delegate: Option.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Origination_delegate.decode), script: CGRIDClass__AlphaScriptedContracts.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Increase_paid_storage generated for AlphaOperationAlphaContentsOrSignaturePrefix__Increase_paid_storage
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Increase_paid_storage extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Increase_paid_storage> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Increase_paid_storage {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Increase_paid_storage>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Increase_paid_storage_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: Z.decode, destination: CGRIDClass__AlphaContract_idOriginated.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Failing_noop generated for AlphaOperationAlphaContentsOrSignaturePrefix__Failing_noop
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Failing_noop extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Failing_noop> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['arbitrary']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Failing_noop {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Failing_noop>({arbitrary: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['arbitrary']})(p));
    };
    get encodeLength(): number {
        return (this.value.arbitrary.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.arbitrary.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Endorsement generated for AlphaOperationAlphaContentsOrSignaturePrefix__Endorsement
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Endorsement extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Endorsement {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Endorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Endorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Drain_delegate generated for AlphaOperationAlphaContentsOrSignaturePrefix__Drain_delegate
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Drain_delegate extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Drain_delegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['consensus_key', 'delegate', 'destination']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Drain_delegate {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Drain_delegate>({consensus_key: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_consensus_key.decode, delegate: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_delegate.decode, destination: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_destination.decode}, {order: ['consensus_key', 'delegate', 'destination']})(p));
    };
    get encodeLength(): number {
        return (this.value.consensus_key.encodeLength +  this.value.delegate.encodeLength +  this.value.destination.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.consensus_key.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.destination.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_preendorsement_evidence generated for AlphaOperationAlphaContentsOrSignaturePrefix__Double_preendorsement_evidence
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_preendorsement_evidence extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Double_preendorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_preendorsement_evidence {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Double_preendorsement_evidence>({op1: Dynamic.decode(CGRIDClass__AlphaInlinedPreendorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__AlphaInlinedPreendorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_endorsement_evidence generated for AlphaOperationAlphaContentsOrSignaturePrefix__Double_endorsement_evidence
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_endorsement_evidence extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Double_endorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_endorsement_evidence {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Double_endorsement_evidence>({op1: Dynamic.decode(CGRIDClass__AlphaInlinedEndorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__AlphaInlinedEndorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_baking_evidence generated for AlphaOperationAlphaContentsOrSignaturePrefix__Double_baking_evidence
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_baking_evidence extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Double_baking_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bh1', 'bh2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_baking_evidence {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Double_baking_evidence>({bh1: Dynamic.decode(CGRIDClass__AlphaBlock_headerAlphaFull_header.decode, width.Uint30), bh2: Dynamic.decode(CGRIDClass__AlphaBlock_headerAlphaFull_header.decode, width.Uint30)}, {order: ['bh1', 'bh2']})(p));
    };
    get encodeLength(): number {
        return (this.value.bh1.encodeLength +  this.value.bh2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bh1.writeTarget(tgt) +  this.value.bh2.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Delegation generated for AlphaOperationAlphaContentsOrSignaturePrefix__Delegation
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Delegation extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Delegation {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Delegation>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Delegation_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, delegate: Option.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Delegation_delegate.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Dal_publish_slot_header generated for AlphaOperationAlphaContentsOrSignaturePrefix__Dal_publish_slot_header
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Dal_publish_slot_header extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Dal_publish_slot_header> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'slot_header']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Dal_publish_slot_header {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Dal_publish_slot_header>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, slot_header: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_slot_header.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'slot_header']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.slot_header.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.slot_header.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Dal_attestation generated for AlphaOperationAlphaContentsOrSignaturePrefix__Dal_attestation
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Dal_attestation extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Dal_attestation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['attestor', 'attestation', 'level']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Dal_attestation {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Dal_attestation>({attestor: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_attestation_attestor.decode, attestation: Z.decode, level: Int32.decode}, {order: ['attestor', 'attestation', 'level']})(p));
    };
    get encodeLength(): number {
        return (this.value.attestor.encodeLength +  this.value.attestation.encodeLength +  this.value.level.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.attestor.writeTarget(tgt) +  this.value.attestation.writeTarget(tgt) +  this.value.level.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Ballot generated for AlphaOperationAlphaContentsOrSignaturePrefix__Ballot
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Ballot extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Ballot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposal', 'ballot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Ballot {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Ballot>({source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Ballot_source.decode, period: Int32.decode, proposal: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Ballot_proposal.decode, ballot: Int8.decode}, {order: ['source', 'period', 'proposal', 'ballot']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposal.encodeLength +  this.value.ballot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposal.writeTarget(tgt) +  this.value.ballot.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Activate_account generated for AlphaOperationAlphaContentsOrSignaturePrefix__Activate_account
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Activate_account extends Box<AlphaOperationAlphaContentsOrSignaturePrefix__Activate_account> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'secret']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Activate_account {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefix__Activate_account>({pkh: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Activate_account_pkh.decode, secret: FixedBytes.decode<20>({len: 20})}, {order: ['pkh', 'secret']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.secret.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.secret.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__Some generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__Some
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__Some extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__Some> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contents', 'ty', 'ticketer']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__Some {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__Some>({contents: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, ty: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, ticketer: CGRIDClass__AlphaContract_id.decode}, {order: ['contents', 'ty', 'ticketer']})(p));
    };
    get encodeLength(): number {
        return (this.value.contents.encodeLength +  this.value.ty.encodeLength +  this.value.ticketer.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contents.writeTarget(tgt) +  this.value.ty.writeTarget(tgt) +  this.value.ticketer.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__None generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__None
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__None extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_public']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public>({_public: Unit.decode}, {order: ['_public']})(p));
    };
    get encodeLength(): number {
        return (this.value._public.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._public.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_private']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private>({_private: Unit.decode}, {order: ['_private']})(p));
    };
    get encodeLength(): number {
        return (this.value._private.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._private.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['fee']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee>({fee: Unit.decode}, {order: ['fee']})(p));
    };
    get encodeLength(): number {
        return (this.value.fee.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.fee.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Deposit generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Deposit
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Deposit extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Deposit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['deposit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Deposit {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Deposit>({deposit: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit.decode}, {order: ['deposit']})(p));
    };
    get encodeLength(): number {
        return (this.value.deposit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.deposit.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Batch generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Batch
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Batch extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Batch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['batch']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Batch {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Batch>({batch: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['batch']})(p));
    };
    get encodeLength(): number {
        return (this.value.batch.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.batch.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_3 generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_3
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_3 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_3> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_3 {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_2 generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_2
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_2 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_2> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_2 {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_1 generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_1
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_1 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_1> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_1 {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_0 generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_0
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_0 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_0> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_0 {
        return new this(Uint8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 {
        return new this(Uint8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__Some generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__Some
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__Some extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__Some> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__Some {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__Some>({commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__None generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__None
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__None extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Start generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Start
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Start extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Start> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['refutation_kind', 'player_commitment_hash', 'opponent_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Start {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Start>({refutation_kind: Unit.decode, player_commitment_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Start_player_commitment_hash.decode, opponent_commitment_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Start_opponent_commitment_hash.decode}, {order: ['refutation_kind', 'player_commitment_hash', 'opponent_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.refutation_kind.encodeLength +  this.value.player_commitment_hash.encodeLength +  this.value.opponent_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.refutation_kind.writeTarget(tgt) +  this.value.player_commitment_hash.writeTarget(tgt) +  this.value.opponent_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Move generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Move
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Move extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Move> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['refutation_kind', 'choice', 'step']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Move {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Move>({refutation_kind: Unit.decode, choice: N.decode, step: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step.decode}, {order: ['refutation_kind', 'choice', 'step']})(p));
    };
    get encodeLength(): number {
        return (this.value.refutation_kind.encodeLength +  this.value.choice.encodeLength +  this.value.step.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.refutation_kind.writeTarget(tgt) +  this.value.choice.writeTarget(tgt) +  this.value.step.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Proof generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Proof
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Proof extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pvm_step', 'input_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Proof {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Proof>({pvm_step: Dynamic.decode(U8String.decode, width.Uint30), input_proof: Option.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof.decode)}, {order: ['pvm_step', 'input_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.pvm_step.encodeLength +  this.value.input_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pvm_step.writeTarget(tgt) +  this.value.input_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Dissection generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Dissection
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Dissection extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Dissection> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Dissection {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['input_proof_kind', 'reveal_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof>({input_proof_kind: Unit.decode, reveal_proof: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof.decode}, {order: ['input_proof_kind', 'reveal_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.input_proof_kind.encodeLength +  this.value.reveal_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.input_proof_kind.writeTarget(tgt) +  this.value.reveal_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['input_proof_kind', 'level', 'message_counter', 'serialized_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof>({input_proof_kind: Unit.decode, level: Int32.decode, message_counter: N.decode, serialized_proof: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['input_proof_kind', 'level', 'message_counter', 'serialized_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.input_proof_kind.encodeLength +  this.value.level.encodeLength +  this.value.message_counter.encodeLength +  this.value.serialized_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.input_proof_kind.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.message_counter.writeTarget(tgt) +  this.value.serialized_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__first_input generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__first_input
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__first_input extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__first_input> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['input_proof_kind']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__first_input {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__first_input>({input_proof_kind: Unit.decode}, {order: ['input_proof_kind']})(p));
    };
    get encodeLength(): number {
        return (this.value.input_proof_kind.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.input_proof_kind.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['reveal_proof_kind', 'raw_data']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof>({reveal_proof_kind: Unit.decode, raw_data: Dynamic.decode(U8String.decode, width.Uint16)}, {order: ['reveal_proof_kind', 'raw_data']})(p));
    };
    get encodeLength(): number {
        return (this.value.reveal_proof_kind.encodeLength +  this.value.raw_data.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.reveal_proof_kind.writeTarget(tgt) +  this.value.raw_data.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['reveal_proof_kind']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof>({reveal_proof_kind: Unit.decode}, {order: ['reveal_proof_kind']})(p));
    };
    get encodeLength(): number {
        return (this.value.reveal_proof_kind.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.reveal_proof_kind.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof
export class CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['reveal_proof_kind', 'dal_page_id', 'dal_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof>({reveal_proof_kind: Unit.decode, dal_page_id: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id.decode, dal_proof: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['reveal_proof_kind', 'dal_page_id', 'dal_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.reveal_proof_kind.encodeLength +  this.value.dal_page_id.encodeLength +  this.value.dal_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.reveal_proof_kind.writeTarget(tgt) +  this.value.dal_page_id.writeTarget(tgt) +  this.value.dal_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedPreendorsementContents__Preendorsement generated for AlphaInlinedPreendorsementContents__Preendorsement
export class CGRIDClass__AlphaInlinedPreendorsementContents__Preendorsement extends Box<AlphaInlinedPreendorsementContents__Preendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedPreendorsementContents__Preendorsement {
        return new this(record_decoder<AlphaInlinedPreendorsementContents__Preendorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__AlphaInlinedPreendorsementContents_Preendorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedEndorsementMempoolContents__Endorsement generated for AlphaInlinedEndorsementMempoolContents__Endorsement
export class CGRIDClass__AlphaInlinedEndorsementMempoolContents__Endorsement extends Box<AlphaInlinedEndorsementMempoolContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedEndorsementMempoolContents__Endorsement {
        return new this(record_decoder<AlphaInlinedEndorsementMempoolContents__Endorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__AlphaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaEntrypoint__set_delegate generated for AlphaEntrypoint__set_delegate
export class CGRIDClass__AlphaEntrypoint__set_delegate extends Box<AlphaEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__root generated for AlphaEntrypoint__root
export class CGRIDClass__AlphaEntrypoint__root extends Box<AlphaEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__remove_delegate generated for AlphaEntrypoint__remove_delegate
export class CGRIDClass__AlphaEntrypoint__remove_delegate extends Box<AlphaEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__named generated for AlphaEntrypoint__named
export class CGRIDClass__AlphaEntrypoint__named extends Box<AlphaEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__deposit generated for AlphaEntrypoint__deposit
export class CGRIDClass__AlphaEntrypoint__deposit extends Box<AlphaEntrypoint__deposit> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__deposit {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint___do generated for AlphaEntrypoint___do
export class CGRIDClass__AlphaEntrypoint___do extends Box<AlphaEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint___default generated for AlphaEntrypoint___default
export class CGRIDClass__AlphaEntrypoint___default extends Box<AlphaEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaContractId__Originated generated for AlphaContractId__Originated
export class CGRIDClass__AlphaContractId__Originated extends Box<AlphaContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaContractId__Originated {
        return new this(Padded.decode(CGRIDClass__AlphaContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaContractId__Implicit generated for AlphaContractId__Implicit
export class CGRIDClass__AlphaContractId__Implicit extends Box<AlphaContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContractId__Implicit {
        return new this(record_decoder<AlphaContractId__Implicit>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContractIdOriginated__Originated generated for AlphaContractIdOriginated__Originated
export class CGRIDClass__AlphaContractIdOriginated__Originated extends Box<AlphaContractIdOriginated__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaContractIdOriginated__Originated {
        return new this(Padded.decode(CGRIDClass__AlphaContract_idOriginated_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKey__Bls = { bls12_381_public_key: FixedBytes<48> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Bls = { bls12_381_public_key_hash: FixedBytes<20> };
export type MichelineAlphaMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineAlphaMichelson_v1Expression>,width.Uint30>;
export type MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives };
export type MichelineAlphaMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__AlphaMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineAlphaMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression };
export type MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression };
export type MichelineAlphaMichelsonV1Expression__Int = { int: Z };
export type MichelineAlphaMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
export type BlsSignaturePrefix__Bls_prefix = FixedBytes<32>;
export type AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_update = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_source, fee: N, counter: N, gas_limit: N, storage_limit: N, zk_rollup: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_zk_rollup, update: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_publish = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_source, fee: N, counter: N, gas_limit: N, storage_limit: N, zk_rollup: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_zk_rollup, op: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq>,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_origination = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_parameters: Dynamic<Bytes,width.Uint30>, circuits_info: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq>,width.Uint30>, init_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30>, nb_ops: Int31 };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Vdf_revelation = { solution: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Vdf_revelation_solution };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Update_consensus_key = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Update_consensus_key_source, fee: N, counter: N, gas_limit: N, storage_limit: N, pk: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Update_consensus_key_pk };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_submit_batch = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_submit_batch_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id, content: Dynamic<U8String,width.Uint30>, burn_limit: Option<N> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_return_bond = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_return_bond_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_remove_commitment = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_remove_commitment_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_rejection = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id, level: Int32, message: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message, message_position: N, message_path: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_path_denest_dyn_denest_seq>,width.Uint30>, message_result_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_result_hash, message_result_path: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq>,width.Uint30>, previous_message_result: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result, previous_message_result_path: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq>,width.Uint30>, proof: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_origination = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, tx_rollup_origination: Unit };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_finalize_commitment = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_finalize_commitment_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_dispatch_tickets = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_source, fee: N, counter: N, gas_limit: N, storage_limit: N, tx_rollup: CGRIDClass__AlphaTx_rollup_id, level: Int32, context_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_context_hash, message_index: Int31, message_result_path: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq>,width.Uint30>, tickets_info: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq>,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_commit = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id, commitment: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Transfer_ticket = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transfer_ticket_source, fee: N, counter: N, gas_limit: N, storage_limit: N, ticket_contents: Dynamic<Bytes,width.Uint30>, ticket_ty: Dynamic<Bytes,width.Uint30>, ticket_ticketer: CGRIDClass__AlphaContract_id, ticket_amount: N, destination: CGRIDClass__AlphaContract_id, entrypoint: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Transaction = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transaction_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: N, destination: CGRIDClass__AlphaContract_id, parameters: Option<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transaction_parameters> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_timeout = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaSmart_rollup_address, stakers: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_refute = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaSmart_rollup_address, opponent: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_opponent, refutation: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_recover_bond = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_rollup, staker: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_staker };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_publish = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaSmart_rollup_address, commitment: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_originate = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_originate_source, fee: N, counter: N, gas_limit: N, storage_limit: N, pvm_kind: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_originate_pvm_kind, kernel: Dynamic<U8String,width.Uint30>, origination_proof: Dynamic<U8String,width.Uint30>, parameters_ty: Dynamic<Bytes,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_execute_outbox_message = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_execute_outbox_message_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaSmart_rollup_address, cemented_commitment: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_execute_outbox_message_cemented_commitment, output_proof: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_cement = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_cement_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaSmart_rollup_address, commitment: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_cement_commitment };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_add_messages = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_add_messages_source, fee: N, counter: N, gas_limit: N, storage_limit: N, message: Dynamic<Sequence<Dynamic<U8String,width.Uint30>>,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Signature_prefix = { signature_prefix: CGRIDClass__Bls_signature_prefix };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Set_deposits_limit = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Set_deposits_limit_source, fee: N, counter: N, gas_limit: N, storage_limit: N, limit: Option<N> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Seed_nonce_revelation = { level: Int32, nonce: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Reveal = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Reveal_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_key: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Reveal_public_key };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Register_global_constant = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Register_global_constant_source, fee: N, counter: N, gas_limit: N, storage_limit: N, value: Dynamic<Bytes,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Proposals = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Proposals_source, period: Int32, proposals: Dynamic<SequenceBounded<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Proposals_proposals_denest_dyn_denest_seq,20>,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Preendorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Preendorsement_block_payload_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Origination = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, balance: N, delegate: Option<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Origination_delegate>, script: CGRIDClass__AlphaScriptedContracts };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Increase_paid_storage = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Increase_paid_storage_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: Z, destination: CGRIDClass__AlphaContract_idOriginated };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Failing_noop = { arbitrary: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Endorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Endorsement_block_payload_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Drain_delegate = { consensus_key: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_consensus_key, delegate: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_delegate, destination: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_destination };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Double_preendorsement_evidence = { op1: Dynamic<CGRIDClass__AlphaInlinedPreendorsement,width.Uint30>, op2: Dynamic<CGRIDClass__AlphaInlinedPreendorsement,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Double_endorsement_evidence = { op1: Dynamic<CGRIDClass__AlphaInlinedEndorsement,width.Uint30>, op2: Dynamic<CGRIDClass__AlphaInlinedEndorsement,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Double_baking_evidence = { bh1: Dynamic<CGRIDClass__AlphaBlock_headerAlphaFull_header,width.Uint30>, bh2: Dynamic<CGRIDClass__AlphaBlock_headerAlphaFull_header,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Delegation = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Delegation_source, fee: N, counter: N, gas_limit: N, storage_limit: N, delegate: Option<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Delegation_delegate> };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Dal_publish_slot_header = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_source, fee: N, counter: N, gas_limit: N, storage_limit: N, slot_header: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_slot_header };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Dal_attestation = { attestor: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_attestation_attestor, attestation: Z, level: Int32 };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Ballot = { source: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Ballot_source, period: Int32, proposal: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Ballot_proposal, ballot: Int8 };
export type AlphaOperationAlphaContentsOrSignaturePrefix__Activate_account = { pkh: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Activate_account_pkh, secret: FixedBytes<20> };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__Some = { contents: CGRIDClass__MichelineAlphaMichelson_v1Expression, ty: CGRIDClass__MichelineAlphaMichelson_v1Expression, ticketer: CGRIDClass__AlphaContract_id };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__None = Unit;
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public = { _public: Unit };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private = { _private: Unit };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee = { fee: Unit };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Deposit = { deposit: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Batch = { batch: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_3 = Int64;
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_2 = Int32;
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_1 = Uint16;
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_0 = Uint8;
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 = Int64;
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 = Int32;
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 = Uint16;
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 = Uint8;
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__Some = { commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__None = Unit;
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Start = { refutation_kind: Unit, player_commitment_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Start_player_commitment_hash, opponent_commitment_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Start_opponent_commitment_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Move = { refutation_kind: Unit, choice: N, step: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Proof = { pvm_step: Dynamic<U8String,width.Uint30>, input_proof: Option<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Dissection = Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq>,width.Uint30>;
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof = { input_proof_kind: Unit, reveal_proof: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof = { input_proof_kind: Unit, level: Int32, message_counter: N, serialized_proof: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__first_input = { input_proof_kind: Unit };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof = { reveal_proof_kind: Unit, raw_data: Dynamic<U8String,width.Uint16> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof = { reveal_proof_kind: Unit };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof = { reveal_proof_kind: Unit, dal_page_id: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id, dal_proof: Dynamic<Bytes,width.Uint30> };
export type AlphaInlinedPreendorsementContents__Preendorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__AlphaInlinedPreendorsementContents_Preendorsement_block_payload_hash };
export type AlphaInlinedEndorsementMempoolContents__Endorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__AlphaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash };
export type AlphaEntrypoint__set_delegate = Unit;
export type AlphaEntrypoint__root = Unit;
export type AlphaEntrypoint__remove_delegate = Unit;
export type AlphaEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type AlphaEntrypoint__deposit = Unit;
export type AlphaEntrypoint___do = Unit;
export type AlphaEntrypoint___default = Unit;
export type AlphaContractId__Originated = Padded<CGRIDClass__AlphaContract_id_Originated_denest_pad,1>;
export type AlphaContractId__Implicit = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaContractIdOriginated__Originated = Padded<CGRIDClass__AlphaContract_idOriginated_Originated_denest_pad,1>;
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
            case CGRIDTag__PublicKeyHash.Bls: return CGRIDClass__PublicKeyHash__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
            case CGRIDTag__PublicKey.Bls: return CGRIDClass__PublicKey__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelson_v1Expression generated for MichelineAlphaMichelsonV1Expression
export function michelinealphamichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineAlphaMichelsonV1Expression,MichelineAlphaMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineAlphaMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Int: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.String: return CGRIDClass__MichelineAlphaMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Sequence: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Bytes: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineAlphaMichelsonV1Expression => Object.values(CGRIDTag__MichelineAlphaMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineAlphaMichelson_v1Expression extends Box<MichelineAlphaMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineAlphaMichelsonV1Expression>, MichelineAlphaMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelinealphamichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Bls_signature_prefix generated for BlsSignaturePrefix
export function blssignatureprefix_mkDecoder(): VariantDecoder<CGRIDTag__BlsSignaturePrefix,BlsSignaturePrefix> {
    function f(disc: CGRIDTag__BlsSignaturePrefix) {
        switch (disc) {
            case CGRIDTag__BlsSignaturePrefix.Bls_prefix: return CGRIDClass__BlsSignaturePrefix__Bls_prefix.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__BlsSignaturePrefix => Object.values(CGRIDTag__BlsSignaturePrefix).includes(tagval);
    return f;
}
export class CGRIDClass__Bls_signature_prefix extends Box<BlsSignaturePrefix> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<BlsSignaturePrefix>, BlsSignaturePrefix>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Bls_signature_prefix {
        return new this(variant_decoder(width.Uint8)(blssignatureprefix_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_predecessor generated for BlockHeaderShellPredecessor
export class CGRIDClass__Block_headerShell_predecessor extends Box<BlockHeaderShellPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_predecessor {
        return new this(record_decoder<BlockHeaderShellPredecessor>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_operations_hash generated for BlockHeaderShellOperationsHash
export class CGRIDClass__Block_headerShell_operations_hash extends Box<BlockHeaderShellOperationsHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_list_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_operations_hash {
        return new this(record_decoder<BlockHeaderShellOperationsHash>({operation_list_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_list_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_list_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_list_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_context generated for BlockHeaderShellContext
export class CGRIDClass__Block_headerShell_context extends Box<BlockHeaderShellContext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_context {
        return new this(record_decoder<BlockHeaderShellContext>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaTx_rollup_id generated for AlphaTxRollupId
export class CGRIDClass__AlphaTx_rollup_id extends Box<AlphaTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaTx_rollup_id {
        return new this(record_decoder<AlphaTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaSmart_rollup_address generated for AlphaSmartRollupAddress
export class CGRIDClass__AlphaSmart_rollup_address extends Box<AlphaSmartRollupAddress> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaSmart_rollup_address {
        return new this(record_decoder<AlphaSmartRollupAddress>({smart_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['smart_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaScriptedContracts generated for AlphaScriptedContracts
export class CGRIDClass__AlphaScriptedContracts extends Box<AlphaScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaScriptedContracts {
        return new this(record_decoder<AlphaScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_zk_rollup generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateZkRollup
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_zk_rollup extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateZkRollup> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['zk_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_zk_rollup {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateZkRollup>({zk_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['zk_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.zk_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.zk_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1 generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['new_state', 'fee']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1 {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1>({new_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30), fee: FixedBytes.decode<32>({len: 32})}, {order: ['new_state', 'fee']})(p));
    };
    get encodeLength(): number {
        return (this.value.new_state.encodeLength +  this.value.fee.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.new_state.writeTarget(tgt) +  this.value.fee.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq {
        return new this(tuple_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq>(Dynamic.decode(U8String.decode, width.Uint30), CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1 generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['new_state', 'fee', 'exit_validity']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1 {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1>({new_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30), fee: FixedBytes.decode<32>({len: 32}), exit_validity: Bool.decode}, {order: ['new_state', 'fee', 'exit_validity']})(p));
    };
    get encodeLength(): number {
        return (this.value.new_state.encodeLength +  this.value.fee.encodeLength +  this.value.exit_validity.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.new_state.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.exit_validity.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq {
        return new this(tuple_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeq>(Dynamic.decode(U8String.decode, width.Uint30), CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_fee_pi generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdateFeePi
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_fee_pi extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdateFeePi> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['new_state']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_fee_pi {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdateFeePi>({new_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['new_state']})(p));
    };
    get encodeLength(): number {
        return (this.value.new_state.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.new_state.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdate
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pending_pis', 'private_pis', 'fee_pi', 'proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdate>({pending_pis: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq.decode), width.Uint30), private_pis: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq.decode), width.Uint30), fee_pi: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_fee_pi.decode, proof: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['pending_pis', 'private_pis', 'fee_pi', 'proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.pending_pis.encodeLength +  this.value.private_pis.encodeLength +  this.value.fee_pi.encodeLength +  this.value.proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pending_pis.writeTarget(tgt) +  this.value.private_pis.writeTarget(tgt) +  this.value.fee_pi.writeTarget(tgt) +  this.value.proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_source generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_zk_rollup generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishZkRollup
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_zk_rollup extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishZkRollup> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['zk_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_zk_rollup {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishZkRollup>({zk_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['zk_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.zk_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.zk_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_source generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index1 generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1
export function alphaoperationalphacontentsorsignatureprefixzkrolluppublishopdenestdyndenestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1,AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1.None: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__None.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1.Some: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1 => Object.values(CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index1 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1>, AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentsorsignatureprefixzkrolluppublishopdenestdyndenestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['zk_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId>({zk_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['zk_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.zk_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.zk_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['id', 'amount']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price>({id: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id.decode, amount: Z.decode}, {order: ['id', 'amount']})(p));
    };
    get encodeLength(): number {
        return (this.value.id.encodeLength +  this.value.amount.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.id.writeTarget(tgt) +  this.value.amount.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0 generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op_code', 'price', 'l1_dst', 'rollup_id', 'payload']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0 {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0>({op_code: Int31.decode, price: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price.decode, l1_dst: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst.decode, rollup_id: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id.decode, payload: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['op_code', 'price', 'l1_dst', 'rollup_id', 'payload']})(p));
    };
    get encodeLength(): number {
        return (this.value.op_code.encodeLength +  this.value.price.encodeLength +  this.value.l1_dst.encodeLength +  this.value.rollup_id.encodeLength +  this.value.payload.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op_code.writeTarget(tgt) +  this.value.price.writeTarget(tgt) +  this.value.l1_dst.writeTarget(tgt) +  this.value.rollup_id.writeTarget(tgt) +  this.value.payload.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq {
        return new this(tuple_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeq>(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0.decode, CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_source generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1 generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1
export function alphaoperationalphacontentsorsignatureprefixzkrolluporiginationcircuitsinfodenestdyndenestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1,AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Public: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Private: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Fee: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1 => Object.values(CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1 extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1>, AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentsorsignatureprefixzkrolluporiginationcircuitsinfodenestdyndenestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq {
        return new this(tuple_decoder<AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeq>(Dynamic.decode(U8String.decode, width.Uint30), CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Vdf_revelation_solution generated for AlphaOperationAlphaContentsOrSignaturePrefixVdfRevelationSolution
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Vdf_revelation_solution extends Box<AlphaOperationAlphaContentsOrSignaturePrefixVdfRevelationSolution> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Vdf_revelation_solution {
        return new this(tuple_decoder<AlphaOperationAlphaContentsOrSignaturePrefixVdfRevelationSolution>(FixedBytes.decode<100>({len: 100}), FixedBytes.decode<100>({len: 100}))(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Update_consensus_key_source generated for AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeySource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Update_consensus_key_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeySource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Update_consensus_key_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeySource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Update_consensus_key_pk generated for AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeyPk
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Update_consensus_key_pk extends Box<AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeyPk> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Update_consensus_key_pk {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeyPk>({signature_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_submit_batch_source generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupSubmitBatchSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_submit_batch_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupSubmitBatchSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_submit_batch_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupSubmitBatchSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_return_bond_source generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupReturnBondSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_return_bond_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupReturnBondSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_return_bond_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupReturnBondSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_remove_commitment_source generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRemoveCommitmentSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_remove_commitment_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRemoveCommitmentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_remove_commitment_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRemoveCommitmentSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_source generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_withdraw_list_hash generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultWithdrawListHash
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_withdraw_list_hash extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultWithdrawListHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['withdraw_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_withdraw_list_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultWithdrawListHash>({withdraw_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['withdraw_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.withdraw_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.withdraw_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_context_hash generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultContextHash
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_context_hash extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultContextHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_context_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultContextHash>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResult
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResult> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash', 'withdraw_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResult>({context_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_context_hash.decode, withdraw_list_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_withdraw_list_hash.decode}, {order: ['context_hash', 'withdraw_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength +  this.value.withdraw_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt) +  this.value.withdraw_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultPathDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_result_hash generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultHash
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_result_hash extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_result_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultHash>({message_result_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_path_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessagePathDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_path_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessagePathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['inbox_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_path_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessagePathDenestDynDenestSeq>({inbox_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['inbox_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.inbox_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.inbox_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositTicketHash
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositTicketHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositTicketHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_sender generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositSender
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_sender extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositSender> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_sender {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositSender>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_destination generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositDestination
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_destination extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositDestination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_destination {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositDestination>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_amount generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount
export function alphaoperationalphacontentsorsignatureprefixtxrolluprejectionmessagedepositdepositamount_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount,AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount.case_0: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_0.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount.case_1: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_1.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount.case_2: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_2.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount.case_3: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_3.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount => Object.values(CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_amount extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount>, AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_amount {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentsorsignatureprefixtxrolluprejectionmessagedepositdepositamount_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDeposit
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDeposit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['sender', 'destination', 'ticket_hash', 'amount']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDeposit>({sender: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_sender.decode, destination: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_destination.decode, ticket_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash.decode, amount: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_amount.decode}, {order: ['sender', 'destination', 'ticket_hash', 'amount']})(p));
    };
    get encodeLength(): number {
        return (this.value.sender.encodeLength +  this.value.destination.encodeLength +  this.value.ticket_hash.encodeLength +  this.value.amount.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.sender.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.ticket_hash.writeTarget(tgt) +  this.value.amount.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage
export function alphaoperationalphacontentsorsignatureprefixtxrolluprejectionmessage_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage,AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage.Batch: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Batch.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage.Deposit: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Deposit.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage => Object.values(CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage>, AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentsorsignatureprefixtxrolluprejectionmessage_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_origination_source generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupOriginationSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_origination_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_origination_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupOriginationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_finalize_commitment_source generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupFinalizeCommitmentSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_finalize_commitment_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupFinalizeCommitmentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_finalize_commitment_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupFinalizeCommitmentSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount
export function alphaoperationalphacontentsorsignatureprefixtxrollupdispatchticketsticketsinfodenestdyndenestseqamount_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_0: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_1: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_2: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_3: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount => Object.values(CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount>, AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentsorsignatureprefixtxrollupdispatchticketsticketsinfodenestdyndenestseqamount_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contents', 'ty', 'ticketer', 'amount', 'claimer']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>({contents: Dynamic.decode(Bytes.decode, width.Uint30), ty: Dynamic.decode(Bytes.decode, width.Uint30), ticketer: CGRIDClass__AlphaContract_id.decode, amount: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount.decode, claimer: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer.decode}, {order: ['contents', 'ty', 'ticketer', 'amount', 'claimer']})(p));
    };
    get encodeLength(): number {
        return (this.value.contents.encodeLength +  this.value.ty.encodeLength +  this.value.ticketer.encodeLength +  this.value.amount.encodeLength +  this.value.claimer.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contents.writeTarget(tgt) +  this.value.ty.writeTarget(tgt) +  this.value.ticketer.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.claimer.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_source generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_context_hash generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsContextHash
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_context_hash extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsContextHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_context_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsContextHash>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_source generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_predecessor generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor
export function alphaoperationalphacontentsorsignatureprefixtxrollupcommitcommitmentpredecessor_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor,AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor.None: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__None.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor.Some: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor => Object.values(CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_predecessor extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor>, AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_predecessor {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentsorsignatureprefixtxrollupcommitcommitmentpredecessor_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentMessagesDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentMessagesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentMessagesDenestDynDenestSeq>({message_result_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_inbox_merkle_root generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentInboxMerkleRoot
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_inbox_merkle_root extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentInboxMerkleRoot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['inbox_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_inbox_merkle_root {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentInboxMerkleRoot>({inbox_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['inbox_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.inbox_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.inbox_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment generated for AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitment
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'messages', 'predecessor', 'inbox_merkle_root']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitment>({level: Int32.decode, messages: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq.decode), width.Uint30), predecessor: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_predecessor.decode, inbox_merkle_root: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_inbox_merkle_root.decode}, {order: ['level', 'messages', 'predecessor', 'inbox_merkle_root']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.messages.encodeLength +  this.value.predecessor.encodeLength +  this.value.inbox_merkle_root.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.messages.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.inbox_merkle_root.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transfer_ticket_source generated for AlphaOperationAlphaContentsOrSignaturePrefixTransferTicketSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transfer_ticket_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTransferTicketSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transfer_ticket_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTransferTicketSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transaction_source generated for AlphaOperationAlphaContentsOrSignaturePrefixTransactionSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transaction_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTransactionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transaction_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTransactionSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transaction_parameters generated for AlphaOperationAlphaContentsOrSignaturePrefixTransactionParameters
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transaction_parameters extends Box<AlphaOperationAlphaContentsOrSignaturePrefixTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Transaction_parameters {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixTransactionParameters>({entrypoint: CGRIDClass__AlphaEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers_bob generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersBob
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers_bob extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersBob> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers_bob {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersBob>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers_alice generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersAlice
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers_alice extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersAlice> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers_alice {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersAlice>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakers
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakers> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['alice', 'bob']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakers>({alice: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers_alice.decode, bob: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers_bob.decode}, {order: ['alice', 'bob']})(p));
    };
    get encodeLength(): number {
        return (this.value.alice.encodeLength +  this.value.bob.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.alice.writeTarget(tgt) +  this.value.bob.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_source generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_source generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Start_player_commitment_hash generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartPlayerCommitmentHash
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Start_player_commitment_hash extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartPlayerCommitmentHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Start_player_commitment_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartPlayerCommitmentHash>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Start_opponent_commitment_hash generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartOpponentCommitmentHash
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Start_opponent_commitment_hash extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartOpponentCommitmentHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Start_opponent_commitment_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartOpponentCommitmentHash>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['published_level', 'slot_index', 'page_index']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId>({published_level: Int32.decode, slot_index: Uint8.decode, page_index: Int16.decode}, {order: ['published_level', 'slot_index', 'page_index']})(p));
    };
    get encodeLength(): number {
        return (this.value.published_level.encodeLength +  this.value.slot_index.encodeLength +  this.value.page_index.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.published_level.writeTarget(tgt) +  this.value.slot_index.writeTarget(tgt) +  this.value.page_index.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof
export function alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestepproofinputproofrevealproofrevealproof_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof,AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.raw_data_proof: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.metadata_proof: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.dal_page_proof: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof => Object.values(CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof>, AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestepproofinputproofrevealproofrevealproof_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof
export function alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestepproofinputproof_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof,AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof.inbox_proof: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof.reveal_proof: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof.first_input: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__first_input.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof => Object.values(CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof>, AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Proof_input_proof {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestepproofinputproof_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_state_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState>({smart_rollup_state_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_state_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_state_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_state_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['state', 'tick']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq>({state: Option.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state.decode), tick: N.decode}, {order: ['state', 'tick']})(p));
    };
    get encodeLength(): number {
        return (this.value.state.encodeLength +  this.value.tick.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.state.writeTarget(tgt) +  this.value.tick.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep
export function alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestep_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep,AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep.Dissection: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Dissection.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep.Proof: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Proof.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep => Object.values(CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep>, AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutationmovestep_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation
export function alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutation_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation,AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation.Start: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Start.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation.Move: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Move.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation => Object.values(CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation>, AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentsorsignatureprefixsmartrolluprefuterefutation_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_opponent generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteOpponent
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_opponent extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteOpponent> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_opponent {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteOpponent>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_staker generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondStaker
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_staker extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondStaker> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_staker {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondStaker>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_source generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_rollup generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondRollup
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_rollup extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondRollup> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_recover_bond_rollup {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondRollup>({smart_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['smart_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_source generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment_predecessor generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentPredecessor
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment_predecessor extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment_predecessor {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentPredecessor>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment_compressed_state generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentCompressedState
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment_compressed_state extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentCompressedState> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_state_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment_compressed_state {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentCompressedState>({smart_rollup_state_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_state_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_state_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_state_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitment
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['compressed_state', 'inbox_level', 'predecessor', 'number_of_ticks']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitment>({compressed_state: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment_compressed_state.decode, inbox_level: Int32.decode, predecessor: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment_predecessor.decode, number_of_ticks: Int64.decode}, {order: ['compressed_state', 'inbox_level', 'predecessor', 'number_of_ticks']})(p));
    };
    get encodeLength(): number {
        return (this.value.compressed_state.encodeLength +  this.value.inbox_level.encodeLength +  this.value.predecessor.encodeLength +  this.value.number_of_ticks.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.compressed_state.writeTarget(tgt) +  this.value.inbox_level.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.number_of_ticks.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_originate_source generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginateSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_originate_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginateSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_originate_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginateSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_originate_pvm_kind generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginatePvmKind
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_originate_pvm_kind extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginatePvmKind> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginatePvmKind>(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_originate_pvm_kind {
        return new this(enum_decoder(width.Uint8)((x): x is AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginatePvmKind => (Object.values(AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginatePvmKind).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_execute_outbox_message_source generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_execute_outbox_message_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_execute_outbox_message_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_execute_outbox_message_cemented_commitment generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageCementedCommitment
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_execute_outbox_message_cemented_commitment extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageCementedCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_execute_outbox_message_cemented_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageCementedCommitment>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_cement_source generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_cement_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_cement_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_cement_commitment generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementCommitment
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_cement_commitment extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_cement_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementCommitment>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_add_messages_source generated for AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupAddMessagesSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_add_messages_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupAddMessagesSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_add_messages_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupAddMessagesSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Set_deposits_limit_source generated for AlphaOperationAlphaContentsOrSignaturePrefixSetDepositsLimitSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Set_deposits_limit_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixSetDepositsLimitSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Set_deposits_limit_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixSetDepositsLimitSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Reveal_source generated for AlphaOperationAlphaContentsOrSignaturePrefixRevealSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Reveal_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixRevealSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Reveal_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixRevealSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Reveal_public_key generated for AlphaOperationAlphaContentsOrSignaturePrefixRevealPublicKey
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Reveal_public_key extends Box<AlphaOperationAlphaContentsOrSignaturePrefixRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Reveal_public_key {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixRevealPublicKey>({signature_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Register_global_constant_source generated for AlphaOperationAlphaContentsOrSignaturePrefixRegisterGlobalConstantSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Register_global_constant_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixRegisterGlobalConstantSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Register_global_constant_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixRegisterGlobalConstantSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Proposals_source generated for AlphaOperationAlphaContentsOrSignaturePrefixProposalsSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Proposals_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixProposalsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Proposals_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixProposalsSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Proposals_proposals_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsOrSignaturePrefixProposalsProposalsDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Proposals_proposals_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsOrSignaturePrefixProposalsProposalsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Proposals_proposals_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixProposalsProposalsDenestDynDenestSeq>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Preendorsement_block_payload_hash generated for AlphaOperationAlphaContentsOrSignaturePrefixPreendorsementBlockPayloadHash
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Preendorsement_block_payload_hash extends Box<AlphaOperationAlphaContentsOrSignaturePrefixPreendorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Preendorsement_block_payload_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixPreendorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Origination_source generated for AlphaOperationAlphaContentsOrSignaturePrefixOriginationSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Origination_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Origination_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixOriginationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Origination_delegate generated for AlphaOperationAlphaContentsOrSignaturePrefixOriginationDelegate
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Origination_delegate extends Box<AlphaOperationAlphaContentsOrSignaturePrefixOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Origination_delegate {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixOriginationDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Increase_paid_storage_source generated for AlphaOperationAlphaContentsOrSignaturePrefixIncreasePaidStorageSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Increase_paid_storage_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixIncreasePaidStorageSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Increase_paid_storage_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixIncreasePaidStorageSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Endorsement_block_payload_hash generated for AlphaOperationAlphaContentsOrSignaturePrefixEndorsementBlockPayloadHash
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Endorsement_block_payload_hash extends Box<AlphaOperationAlphaContentsOrSignaturePrefixEndorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Endorsement_block_payload_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixEndorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_destination generated for AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDestination
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_destination extends Box<AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDestination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_destination {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDestination>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_delegate generated for AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDelegate
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_delegate extends Box<AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_delegate {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_consensus_key generated for AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateConsensusKey
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_consensus_key extends Box<AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateConsensusKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Drain_delegate_consensus_key {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateConsensusKey>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Delegation_source generated for AlphaOperationAlphaContentsOrSignaturePrefixDelegationSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Delegation_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixDelegationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Delegation_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixDelegationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Delegation_delegate generated for AlphaOperationAlphaContentsOrSignaturePrefixDelegationDelegate
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Delegation_delegate extends Box<AlphaOperationAlphaContentsOrSignaturePrefixDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Delegation_delegate {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixDelegationDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_source generated for AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_slot_header_commitment generated for AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeaderCommitment
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_slot_header_commitment extends Box<AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeaderCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['dal_commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_slot_header_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeaderCommitment>({dal_commitment: FixedBytes.decode<48>({len: 48})}, {order: ['dal_commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.dal_commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.dal_commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_slot_header generated for AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeader
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_slot_header extends Box<AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['published_level', 'slot_index', 'commitment', 'commitment_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_slot_header {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeader>({published_level: Int32.decode, slot_index: Uint8.decode, commitment: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_slot_header_commitment.decode, commitment_proof: FixedBytes.decode<48>({len: 48})}, {order: ['published_level', 'slot_index', 'commitment', 'commitment_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.published_level.encodeLength +  this.value.slot_index.encodeLength +  this.value.commitment.encodeLength +  this.value.commitment_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.published_level.writeTarget(tgt) +  this.value.slot_index.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt) +  this.value.commitment_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_attestation_attestor generated for AlphaOperationAlphaContentsOrSignaturePrefixDalAttestationAttestor
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_attestation_attestor extends Box<AlphaOperationAlphaContentsOrSignaturePrefixDalAttestationAttestor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_attestation_attestor {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixDalAttestationAttestor>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Ballot_source generated for AlphaOperationAlphaContentsOrSignaturePrefixBallotSource
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Ballot_source extends Box<AlphaOperationAlphaContentsOrSignaturePrefixBallotSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Ballot_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixBallotSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Ballot_proposal generated for AlphaOperationAlphaContentsOrSignaturePrefixBallotProposal
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Ballot_proposal extends Box<AlphaOperationAlphaContentsOrSignaturePrefixBallotProposal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Ballot_proposal {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixBallotProposal>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Activate_account_pkh generated for AlphaOperationAlphaContentsOrSignaturePrefixActivateAccountPkh
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Activate_account_pkh extends Box<AlphaOperationAlphaContentsOrSignaturePrefixActivateAccountPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Activate_account_pkh {
        return new this(record_decoder<AlphaOperationAlphaContentsOrSignaturePrefixActivateAccountPkh>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix generated for AlphaOperationAlphaContentsOrSignaturePrefix
export function alphaoperationalphacontentsorsignatureprefix_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix,AlphaOperationAlphaContentsOrSignaturePrefix> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Seed_nonce_revelation: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Seed_nonce_revelation.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Double_endorsement_evidence: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_endorsement_evidence.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Double_baking_evidence: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_baking_evidence.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Activate_account: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Activate_account.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Proposals: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Proposals.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Ballot: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Ballot.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Double_preendorsement_evidence: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_preendorsement_evidence.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Vdf_revelation: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Vdf_revelation.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Drain_delegate: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Drain_delegate.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Failing_noop: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Failing_noop.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Preendorsement: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Preendorsement.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Endorsement: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Endorsement.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Dal_attestation: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Dal_attestation.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Reveal: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Reveal.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Transaction: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Transaction.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Origination: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Origination.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Delegation: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Delegation.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Register_global_constant: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Register_global_constant.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Set_deposits_limit: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Set_deposits_limit.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Increase_paid_storage: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Increase_paid_storage.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Update_consensus_key: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Update_consensus_key.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_origination: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_origination.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_submit_batch: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_submit_batch.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_commit: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_commit.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_return_bond: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_return_bond.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_finalize_commitment: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_finalize_commitment.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_remove_commitment: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_remove_commitment.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_rejection: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_rejection.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_dispatch_tickets: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_dispatch_tickets.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Transfer_ticket: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Transfer_ticket.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_originate: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_originate.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_add_messages: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_add_messages.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_cement: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_cement.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_publish: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_publish.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_refute: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_refute.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_timeout: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_timeout.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_execute_outbox_message: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_execute_outbox_message.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_recover_bond: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_recover_bond.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Dal_publish_slot_header: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Dal_publish_slot_header.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Zk_rollup_origination: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_origination.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Zk_rollup_publish: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_publish.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Zk_rollup_update: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_update.decode;
            case CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Signature_prefix: return CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Signature_prefix.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix => Object.values(CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix extends Box<AlphaOperationAlphaContentsOrSignaturePrefix> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsOrSignaturePrefix>, AlphaOperationAlphaContentsOrSignaturePrefix>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentsorsignatureprefix_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaMichelsonV1Primitives generated for AlphaMichelsonV1Primitives
export class CGRIDClass__AlphaMichelsonV1Primitives extends Box<AlphaMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<AlphaMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is AlphaMichelsonV1Primitives => (Object.values(AlphaMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaInlinedPreendorsement_signature generated for AlphaInlinedPreendorsementSignature
export class CGRIDClass__AlphaInlinedPreendorsement_signature extends Box<AlphaInlinedPreendorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedPreendorsement_signature {
        return new this(record_decoder<AlphaInlinedPreendorsementSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedPreendorsementContents_Preendorsement_block_payload_hash generated for AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash
export class CGRIDClass__AlphaInlinedPreendorsementContents_Preendorsement_block_payload_hash extends Box<AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedPreendorsementContents_Preendorsement_block_payload_hash {
        return new this(record_decoder<AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedPreendorsementContents generated for AlphaInlinedPreendorsementContents
export function alphainlinedpreendorsementcontents_mkDecoder(): VariantDecoder<CGRIDTag__AlphaInlinedPreendorsementContents,AlphaInlinedPreendorsementContents> {
    function f(disc: CGRIDTag__AlphaInlinedPreendorsementContents) {
        switch (disc) {
            case CGRIDTag__AlphaInlinedPreendorsementContents.Preendorsement: return CGRIDClass__AlphaInlinedPreendorsementContents__Preendorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaInlinedPreendorsementContents => Object.values(CGRIDTag__AlphaInlinedPreendorsementContents).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaInlinedPreendorsementContents extends Box<AlphaInlinedPreendorsementContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaInlinedPreendorsementContents>, AlphaInlinedPreendorsementContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedPreendorsementContents {
        return new this(variant_decoder(width.Uint8)(alphainlinedpreendorsementcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedPreendorsement generated for AlphaInlinedPreendorsement
export class CGRIDClass__AlphaInlinedPreendorsement extends Box<AlphaInlinedPreendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedPreendorsement {
        return new this(record_decoder<AlphaInlinedPreendorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__AlphaInlinedPreendorsementContents.decode, signature: Nullable.decode(CGRIDClass__AlphaInlinedPreendorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedEndorsement_signature generated for AlphaInlinedEndorsementSignature
export class CGRIDClass__AlphaInlinedEndorsement_signature extends Box<AlphaInlinedEndorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedEndorsement_signature {
        return new this(record_decoder<AlphaInlinedEndorsementSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash generated for AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash
export class CGRIDClass__AlphaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash extends Box<AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash {
        return new this(record_decoder<AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedEndorsement_mempoolContents generated for AlphaInlinedEndorsementMempoolContents
export function alphainlinedendorsementmempoolcontents_mkDecoder(): VariantDecoder<CGRIDTag__AlphaInlinedEndorsementMempoolContents,AlphaInlinedEndorsementMempoolContents> {
    function f(disc: CGRIDTag__AlphaInlinedEndorsementMempoolContents) {
        switch (disc) {
            case CGRIDTag__AlphaInlinedEndorsementMempoolContents.Endorsement: return CGRIDClass__AlphaInlinedEndorsementMempoolContents__Endorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaInlinedEndorsementMempoolContents => Object.values(CGRIDTag__AlphaInlinedEndorsementMempoolContents).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaInlinedEndorsement_mempoolContents extends Box<AlphaInlinedEndorsementMempoolContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaInlinedEndorsementMempoolContents>, AlphaInlinedEndorsementMempoolContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedEndorsement_mempoolContents {
        return new this(variant_decoder(width.Uint8)(alphainlinedendorsementmempoolcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedEndorsement generated for AlphaInlinedEndorsement
export class CGRIDClass__AlphaInlinedEndorsement extends Box<AlphaInlinedEndorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedEndorsement {
        return new this(record_decoder<AlphaInlinedEndorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__AlphaInlinedEndorsement_mempoolContents.decode, signature: Nullable.decode(CGRIDClass__AlphaInlinedEndorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaEntrypoint generated for AlphaEntrypoint
export function alphaentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__AlphaEntrypoint,AlphaEntrypoint> {
    function f(disc: CGRIDTag__AlphaEntrypoint) {
        switch (disc) {
            case CGRIDTag__AlphaEntrypoint._default: return CGRIDClass__AlphaEntrypoint___default.decode;
            case CGRIDTag__AlphaEntrypoint.root: return CGRIDClass__AlphaEntrypoint__root.decode;
            case CGRIDTag__AlphaEntrypoint._do: return CGRIDClass__AlphaEntrypoint___do.decode;
            case CGRIDTag__AlphaEntrypoint.set_delegate: return CGRIDClass__AlphaEntrypoint__set_delegate.decode;
            case CGRIDTag__AlphaEntrypoint.remove_delegate: return CGRIDClass__AlphaEntrypoint__remove_delegate.decode;
            case CGRIDTag__AlphaEntrypoint.deposit: return CGRIDClass__AlphaEntrypoint__deposit.decode;
            case CGRIDTag__AlphaEntrypoint.named: return CGRIDClass__AlphaEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaEntrypoint => Object.values(CGRIDTag__AlphaEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaEntrypoint extends Box<AlphaEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaEntrypoint>, AlphaEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint {
        return new this(variant_decoder(width.Uint8)(alphaentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContract_id_Originated_denest_pad generated for AlphaContractIdOriginatedDenestPad
export class CGRIDClass__AlphaContract_id_Originated_denest_pad extends Box<AlphaContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContract_id_Originated_denest_pad {
        return new this(record_decoder<AlphaContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContract_idOriginated_Originated_denest_pad generated for AlphaContractIdOriginatedOriginatedDenestPad
export class CGRIDClass__AlphaContract_idOriginated_Originated_denest_pad extends Box<AlphaContractIdOriginatedOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContract_idOriginated_Originated_denest_pad {
        return new this(record_decoder<AlphaContractIdOriginatedOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContract_idOriginated generated for AlphaContractIdOriginated
export function alphacontractidoriginated_mkDecoder(): VariantDecoder<CGRIDTag__AlphaContractIdOriginated,AlphaContractIdOriginated> {
    function f(disc: CGRIDTag__AlphaContractIdOriginated) {
        switch (disc) {
            case CGRIDTag__AlphaContractIdOriginated.Originated: return CGRIDClass__AlphaContractIdOriginated__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaContractIdOriginated => Object.values(CGRIDTag__AlphaContractIdOriginated).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaContract_idOriginated extends Box<AlphaContractIdOriginated> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaContractIdOriginated>, AlphaContractIdOriginated>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContract_idOriginated {
        return new this(variant_decoder(width.Uint8)(alphacontractidoriginated_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContract_id generated for AlphaContractId
export function alphacontractid_mkDecoder(): VariantDecoder<CGRIDTag__AlphaContractId,AlphaContractId> {
    function f(disc: CGRIDTag__AlphaContractId) {
        switch (disc) {
            case CGRIDTag__AlphaContractId.Implicit: return CGRIDClass__AlphaContractId__Implicit.decode;
            case CGRIDTag__AlphaContractId.Originated: return CGRIDClass__AlphaContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaContractId => Object.values(CGRIDTag__AlphaContractId).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaContract_id extends Box<AlphaContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaContractId>, AlphaContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContract_id {
        return new this(variant_decoder(width.Uint8)(alphacontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash generated for AlphaBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash extends Box<AlphaBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<AlphaBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBlock_headerAlphaSigned_contents_signature generated for AlphaBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__AlphaBlock_headerAlphaSigned_contents_signature extends Box<AlphaBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<AlphaBlockHeaderAlphaSignedContentsSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBlock_headerAlphaFull_header generated for AlphaBlockHeaderAlphaFullHeader
export class CGRIDClass__AlphaBlock_headerAlphaFull_header extends Box<AlphaBlockHeaderAlphaFullHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBlock_headerAlphaFull_header {
        return new this(record_decoder<AlphaBlockHeaderAlphaFullHeader>({level: Int32.decode, proto: Uint8.decode, predecessor: CGRIDClass__Block_headerShell_predecessor.decode, timestamp: Int64.decode, validation_pass: Uint8.decode, operations_hash: CGRIDClass__Block_headerShell_operations_hash.decode, fitness: Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30), context: CGRIDClass__Block_headerShell_context.decode, payload_hash: CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__AlphaBlock_headerAlphaSigned_contents_signature.decode}, {order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.proto.encodeLength +  this.value.predecessor.encodeLength +  this.value.timestamp.encodeLength +  this.value.validation_pass.encodeLength +  this.value.operations_hash.encodeLength +  this.value.fitness.encodeLength +  this.value.context.encodeLength +  this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_toggle_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.proto.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.timestamp.writeTarget(tgt) +  this.value.validation_pass.writeTarget(tgt) +  this.value.operations_hash.writeTarget(tgt) +  this.value.fitness.writeTarget(tgt) +  this.value.context.writeTarget(tgt) +  this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_toggle_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256,
    Bls: CGRIDClass__PublicKeyHash__Bls
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] } | { kind: CGRIDTag__PublicKeyHash.Bls, value: CGRIDMap__PublicKeyHash['Bls'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256,
    Bls: CGRIDClass__PublicKey__Bls
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] } | { kind: CGRIDTag__PublicKey.Bls, value: CGRIDMap__PublicKey['Bls'] };
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export enum CGRIDTag__MichelineAlphaMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineAlphaMichelsonV1Expression {
    Int: CGRIDClass__MichelineAlphaMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineAlphaMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes
}
export type MichelineAlphaMichelsonV1Expression = { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Int, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.String, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Bytes'] };
export enum CGRIDTag__BlsSignaturePrefix{
    Bls_prefix = 3
}
export interface CGRIDMap__BlsSignaturePrefix {
    Bls_prefix: CGRIDClass__BlsSignaturePrefix__Bls_prefix
}
export type BlsSignaturePrefix = { kind: CGRIDTag__BlsSignaturePrefix.Bls_prefix, value: CGRIDMap__BlsSignaturePrefix['Bls_prefix'] };
export type BlockHeaderShellPredecessor = { block_hash: FixedBytes<32> };
export type BlockHeaderShellOperationsHash = { operation_list_list_hash: FixedBytes<32> };
export type BlockHeaderShellContext = { context_hash: FixedBytes<32> };
export type AlphaTxRollupId = { rollup_hash: FixedBytes<20> };
export type AlphaSmartRollupAddress = { smart_rollup_hash: FixedBytes<20> };
export type AlphaScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateZkRollup = { zk_rollup_hash: FixedBytes<20> };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1 = { new_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30>, fee: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq = [Dynamic<U8String,width.Uint30>, CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1];
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1 = { new_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30>, fee: FixedBytes<32>, exit_validity: Bool };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdatePendingPisDenestDynDenestSeq = [Dynamic<U8String,width.Uint30>, CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1];
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdateFeePi = { new_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateUpdate = { pending_pis: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq>,width.Uint30>, private_pis: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq>,width.Uint30>, fee_pi: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_update_update_fee_pi, proof: Dynamic<Bytes,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupUpdateSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishZkRollup = { zk_rollup_hash: FixedBytes<20> };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1{
    None = 0,
    Some = 1
}
export interface CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1 {
    None: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__None,
    Some: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1__Some
}
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1 = { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1.None, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1['None'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1.Some, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex1['Some'] };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId = { zk_rollup_hash: FixedBytes<20> };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId = { script_expr: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price = { id: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id, amount: Z };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeqIndex0 = { op_code: Int31, price: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price, l1_dst: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst, rollup_id: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id, payload: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupPublishOpDenestDynDenestSeq = [CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index0, CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_publish_op_denest_dyn_denest_seq_index1];
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1{
    Public = 0,
    Private = 1,
    Fee = 2
}
export interface CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1 {
    Public: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public,
    Private: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private,
    Fee: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee
}
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1 = { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Public, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1['Public'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Private, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1['Private'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Fee, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1['Fee'] };
export type AlphaOperationAlphaContentsOrSignaturePrefixZkRollupOriginationCircuitsInfoDenestDynDenestSeq = [Dynamic<U8String,width.Uint30>, CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1];
export type AlphaOperationAlphaContentsOrSignaturePrefixVdfRevelationSolution = [FixedBytes<100>, FixedBytes<100>];
export type AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeySource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixUpdateConsensusKeyPk = { signature_public_key: CGRIDClass__Public_key };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupSubmitBatchSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupReturnBondSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRemoveCommitmentSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultWithdrawListHash = { withdraw_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResultContextHash = { context_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionPreviousMessageResult = { context_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_context_hash, withdraw_list_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_previous_message_result_withdraw_list_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageResultHash = { message_result_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessagePathDenestDynDenestSeq = { inbox_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositTicketHash = { script_expr: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositSender = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositDestination = { bls12_381_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3
}
export interface CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount {
    case_0: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_0,
    case_1: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_1,
    case_2: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_2,
    case_3: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount__case_3
}
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount = { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount.case_0, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount['case_0'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount.case_1, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount['case_1'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount.case_2, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount['case_2'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount.case_3, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDepositAmount['case_3'] };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessageDepositDeposit = { sender: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_sender, destination: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_destination, ticket_hash: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash, amount: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_rejection_message_Deposit_deposit_amount };
export enum CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage{
    Batch = 0,
    Deposit = 1
}
export interface CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage {
    Batch: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Batch,
    Deposit: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage__Deposit
}
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage = { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage.Batch, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage['Batch'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage.Deposit, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupRejectionMessage['Deposit'] };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupOriginationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupFinalizeCommitmentSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3
}
export interface CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount {
    case_0: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0,
    case_1: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1,
    case_2: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2,
    case_3: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3
}
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount = { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_0, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_0'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_1, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_1'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_2, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_2'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_3, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_3'] };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq = { contents: Dynamic<Bytes,width.Uint30>, ty: Dynamic<Bytes,width.Uint30>, ticketer: CGRIDClass__AlphaContract_id, amount: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount, claimer: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupDispatchTicketsContextHash = { context_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor{
    None = 0,
    Some = 1
}
export interface CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor {
    None: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__None,
    Some: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor__Some
}
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor = { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor.None, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor['None'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor.Some, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentPredecessor['Some'] };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentMessagesDenestDynDenestSeq = { message_result_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitmentInboxMerkleRoot = { inbox_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixTxRollupCommitCommitment = { level: Int32, messages: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq>,width.Uint30>, predecessor: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_predecessor, inbox_merkle_root: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Tx_rollup_commit_commitment_inbox_merkle_root };
export type AlphaOperationAlphaContentsOrSignaturePrefixTransferTicketSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixTransactionSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixTransactionParameters = { entrypoint: CGRIDClass__AlphaEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersBob = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakersAlice = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutStakers = { alice: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers_alice, bob: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_timeout_stakers_bob };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupTimeoutSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartPlayerCommitmentHash = { smart_rollup_commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationStartOpponentCommitmentHash = { smart_rollup_commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId = { published_level: Int32, slot_index: Uint8, page_index: Int16 };
export enum CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof{
    raw_data_proof = 0,
    metadata_proof = 1,
    dal_page_proof = 2
}
export interface CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof {
    raw_data_proof: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof,
    metadata_proof: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof,
    dal_page_proof: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof
}
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof = { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.raw_data_proof, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof['raw_data_proof'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.metadata_proof, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof['metadata_proof'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.dal_page_proof, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof['dal_page_proof'] };
export enum CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof{
    inbox_proof = 0,
    reveal_proof = 1,
    first_input = 2
}
export interface CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof {
    inbox_proof: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof,
    reveal_proof: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof,
    first_input: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof__first_input
}
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof = { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof.inbox_proof, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof['inbox_proof'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof.reveal_proof, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof['reveal_proof'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof.first_input, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepProofInputProof['first_input'] };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState = { smart_rollup_state_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq = { state: Option<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state>, tick: N };
export enum CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep{
    Dissection = 0,
    Proof = 1
}
export interface CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep {
    Dissection: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Dissection,
    Proof: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep__Proof
}
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep = { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep.Dissection, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep['Dissection'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep.Proof, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutationMoveStep['Proof'] };
export enum CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation{
    Start = 0,
    Move = 1
}
export interface CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation {
    Start: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Start,
    Move: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation__Move
}
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation = { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation.Start, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation['Start'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation.Move, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteRefutation['Move'] };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRefuteOpponent = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondStaker = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupRecoverBondRollup = { smart_rollup_hash: FixedBytes<20> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentPredecessor = { smart_rollup_commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitmentCompressedState = { smart_rollup_state_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupPublishCommitment = { compressed_state: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment_compressed_state, inbox_level: Int32, predecessor: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Smart_rollup_publish_commitment_predecessor, number_of_ticks: Int64 };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginateSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupOriginatePvmKind{
    arith = 0,
    wasm_2_0_0 = 1
}
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupExecuteOutboxMessageCementedCommitment = { smart_rollup_commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupCementCommitment = { smart_rollup_commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixSmartRollupAddMessagesSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixSetDepositsLimitSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixRevealSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixRevealPublicKey = { signature_public_key: CGRIDClass__Public_key };
export type AlphaOperationAlphaContentsOrSignaturePrefixRegisterGlobalConstantSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixProposalsSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixProposalsProposalsDenestDynDenestSeq = { protocol_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixPreendorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixOriginationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixOriginationDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixIncreasePaidStorageSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixEndorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDestination = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixDrainDelegateConsensusKey = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixDelegationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixDelegationDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeaderCommitment = { dal_commitment: FixedBytes<48> };
export type AlphaOperationAlphaContentsOrSignaturePrefixDalPublishSlotHeaderSlotHeader = { published_level: Int32, slot_index: Uint8, commitment: CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix_Dal_publish_slot_header_slot_header_commitment, commitment_proof: FixedBytes<48> };
export type AlphaOperationAlphaContentsOrSignaturePrefixDalAttestationAttestor = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixBallotSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOrSignaturePrefixBallotProposal = { protocol_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOrSignaturePrefixActivateAccountPkh = { ed25519_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix{
    Seed_nonce_revelation = 1,
    Double_endorsement_evidence = 2,
    Double_baking_evidence = 3,
    Activate_account = 4,
    Proposals = 5,
    Ballot = 6,
    Double_preendorsement_evidence = 7,
    Vdf_revelation = 8,
    Drain_delegate = 9,
    Failing_noop = 17,
    Preendorsement = 20,
    Endorsement = 21,
    Dal_attestation = 22,
    Reveal = 107,
    Transaction = 108,
    Origination = 109,
    Delegation = 110,
    Register_global_constant = 111,
    Set_deposits_limit = 112,
    Increase_paid_storage = 113,
    Update_consensus_key = 114,
    Tx_rollup_origination = 150,
    Tx_rollup_submit_batch = 151,
    Tx_rollup_commit = 152,
    Tx_rollup_return_bond = 153,
    Tx_rollup_finalize_commitment = 154,
    Tx_rollup_remove_commitment = 155,
    Tx_rollup_rejection = 156,
    Tx_rollup_dispatch_tickets = 157,
    Transfer_ticket = 158,
    Smart_rollup_originate = 200,
    Smart_rollup_add_messages = 201,
    Smart_rollup_cement = 202,
    Smart_rollup_publish = 203,
    Smart_rollup_refute = 204,
    Smart_rollup_timeout = 205,
    Smart_rollup_execute_outbox_message = 206,
    Smart_rollup_recover_bond = 207,
    Dal_publish_slot_header = 230,
    Zk_rollup_origination = 250,
    Zk_rollup_publish = 251,
    Zk_rollup_update = 252,
    Signature_prefix = 255
}
export interface CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix {
    Seed_nonce_revelation: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Seed_nonce_revelation,
    Double_endorsement_evidence: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_endorsement_evidence,
    Double_baking_evidence: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_baking_evidence,
    Activate_account: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Activate_account,
    Proposals: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Proposals,
    Ballot: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Ballot,
    Double_preendorsement_evidence: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Double_preendorsement_evidence,
    Vdf_revelation: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Vdf_revelation,
    Drain_delegate: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Drain_delegate,
    Failing_noop: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Failing_noop,
    Preendorsement: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Preendorsement,
    Endorsement: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Endorsement,
    Dal_attestation: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Dal_attestation,
    Reveal: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Reveal,
    Transaction: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Transaction,
    Origination: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Origination,
    Delegation: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Delegation,
    Register_global_constant: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Register_global_constant,
    Set_deposits_limit: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Set_deposits_limit,
    Increase_paid_storage: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Increase_paid_storage,
    Update_consensus_key: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Update_consensus_key,
    Tx_rollup_origination: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_origination,
    Tx_rollup_submit_batch: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_submit_batch,
    Tx_rollup_commit: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_commit,
    Tx_rollup_return_bond: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_return_bond,
    Tx_rollup_finalize_commitment: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_finalize_commitment,
    Tx_rollup_remove_commitment: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_remove_commitment,
    Tx_rollup_rejection: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_rejection,
    Tx_rollup_dispatch_tickets: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Tx_rollup_dispatch_tickets,
    Transfer_ticket: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Transfer_ticket,
    Smart_rollup_originate: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_originate,
    Smart_rollup_add_messages: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_add_messages,
    Smart_rollup_cement: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_cement,
    Smart_rollup_publish: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_publish,
    Smart_rollup_refute: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_refute,
    Smart_rollup_timeout: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_timeout,
    Smart_rollup_execute_outbox_message: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_execute_outbox_message,
    Smart_rollup_recover_bond: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Smart_rollup_recover_bond,
    Dal_publish_slot_header: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Dal_publish_slot_header,
    Zk_rollup_origination: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_origination,
    Zk_rollup_publish: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_publish,
    Zk_rollup_update: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Zk_rollup_update,
    Signature_prefix: CGRIDClass__AlphaOperationAlphaContentsOrSignaturePrefix__Signature_prefix
}
export type AlphaOperationAlphaContentsOrSignaturePrefix = { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Seed_nonce_revelation, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Seed_nonce_revelation'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Double_endorsement_evidence, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Double_endorsement_evidence'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Double_baking_evidence, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Double_baking_evidence'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Activate_account, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Activate_account'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Proposals, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Proposals'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Ballot, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Ballot'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Double_preendorsement_evidence, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Double_preendorsement_evidence'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Vdf_revelation, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Vdf_revelation'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Drain_delegate, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Drain_delegate'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Failing_noop, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Failing_noop'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Preendorsement, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Preendorsement'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Endorsement, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Endorsement'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Dal_attestation, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Dal_attestation'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Reveal, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Reveal'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Transaction, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Transaction'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Origination, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Origination'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Delegation, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Delegation'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Register_global_constant, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Register_global_constant'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Set_deposits_limit, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Set_deposits_limit'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Increase_paid_storage, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Increase_paid_storage'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Update_consensus_key, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Update_consensus_key'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_origination, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Tx_rollup_origination'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_submit_batch, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Tx_rollup_submit_batch'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_commit, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Tx_rollup_commit'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_return_bond, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Tx_rollup_return_bond'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_finalize_commitment, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Tx_rollup_finalize_commitment'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_remove_commitment, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Tx_rollup_remove_commitment'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_rejection, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Tx_rollup_rejection'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Tx_rollup_dispatch_tickets, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Tx_rollup_dispatch_tickets'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Transfer_ticket, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Transfer_ticket'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_originate, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Smart_rollup_originate'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_add_messages, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Smart_rollup_add_messages'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_cement, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Smart_rollup_cement'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_publish, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Smart_rollup_publish'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_refute, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Smart_rollup_refute'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_timeout, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Smart_rollup_timeout'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_execute_outbox_message, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Smart_rollup_execute_outbox_message'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Smart_rollup_recover_bond, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Smart_rollup_recover_bond'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Dal_publish_slot_header, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Dal_publish_slot_header'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Zk_rollup_origination, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Zk_rollup_origination'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Zk_rollup_publish, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Zk_rollup_publish'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Zk_rollup_update, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Zk_rollup_update'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsOrSignaturePrefix.Signature_prefix, value: CGRIDMap__AlphaOperationAlphaContentsOrSignaturePrefix['Signature_prefix'] };
export enum AlphaMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117,
    LEVEL = 118,
    SELF_ADDRESS = 119,
    never = 120,
    NEVER = 121,
    UNPAIR = 122,
    VOTING_POWER = 123,
    TOTAL_VOTING_POWER = 124,
    KECCAK = 125,
    SHA3 = 126,
    PAIRING_CHECK = 127,
    bls12_381_g1 = 128,
    bls12_381_g2 = 129,
    bls12_381_fr = 130,
    sapling_state = 131,
    sapling_transaction_deprecated = 132,
    SAPLING_EMPTY_STATE = 133,
    SAPLING_VERIFY_UPDATE = 134,
    ticket = 135,
    TICKET_DEPRECATED = 136,
    READ_TICKET = 137,
    SPLIT_TICKET = 138,
    JOIN_TICKETS = 139,
    GET_AND_UPDATE = 140,
    chest = 141,
    chest_key = 142,
    OPEN_CHEST = 143,
    VIEW = 144,
    view = 145,
    constant = 146,
    SUB_MUTEZ = 147,
    tx_rollup_l2_address = 148,
    MIN_BLOCK_TIME = 149,
    sapling_transaction = 150,
    EMIT = 151,
    Lambda_rec = 152,
    LAMBDA_REC = 153,
    TICKET = 154,
    BYTES = 155,
    NAT = 156
}
export type AlphaInlinedPreendorsementSignature = { signature_v1: Bytes };
export type AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export enum CGRIDTag__AlphaInlinedPreendorsementContents{
    Preendorsement = 20
}
export interface CGRIDMap__AlphaInlinedPreendorsementContents {
    Preendorsement: CGRIDClass__AlphaInlinedPreendorsementContents__Preendorsement
}
export type AlphaInlinedPreendorsementContents = { kind: CGRIDTag__AlphaInlinedPreendorsementContents.Preendorsement, value: CGRIDMap__AlphaInlinedPreendorsementContents['Preendorsement'] };
export type AlphaInlinedPreendorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__AlphaInlinedPreendorsementContents, signature: Nullable<CGRIDClass__AlphaInlinedPreendorsement_signature> };
export type AlphaInlinedEndorsementSignature = { signature_v1: Bytes };
export type AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export enum CGRIDTag__AlphaInlinedEndorsementMempoolContents{
    Endorsement = 21
}
export interface CGRIDMap__AlphaInlinedEndorsementMempoolContents {
    Endorsement: CGRIDClass__AlphaInlinedEndorsementMempoolContents__Endorsement
}
export type AlphaInlinedEndorsementMempoolContents = { kind: CGRIDTag__AlphaInlinedEndorsementMempoolContents.Endorsement, value: CGRIDMap__AlphaInlinedEndorsementMempoolContents['Endorsement'] };
export type AlphaInlinedEndorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__AlphaInlinedEndorsement_mempoolContents, signature: Nullable<CGRIDClass__AlphaInlinedEndorsement_signature> };
export enum CGRIDTag__AlphaEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    deposit = 5,
    named = 255
}
export interface CGRIDMap__AlphaEntrypoint {
    _default: CGRIDClass__AlphaEntrypoint___default,
    root: CGRIDClass__AlphaEntrypoint__root,
    _do: CGRIDClass__AlphaEntrypoint___do,
    set_delegate: CGRIDClass__AlphaEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__AlphaEntrypoint__remove_delegate,
    deposit: CGRIDClass__AlphaEntrypoint__deposit,
    named: CGRIDClass__AlphaEntrypoint__named
}
export type AlphaEntrypoint = { kind: CGRIDTag__AlphaEntrypoint._default, value: CGRIDMap__AlphaEntrypoint['_default'] } | { kind: CGRIDTag__AlphaEntrypoint.root, value: CGRIDMap__AlphaEntrypoint['root'] } | { kind: CGRIDTag__AlphaEntrypoint._do, value: CGRIDMap__AlphaEntrypoint['_do'] } | { kind: CGRIDTag__AlphaEntrypoint.set_delegate, value: CGRIDMap__AlphaEntrypoint['set_delegate'] } | { kind: CGRIDTag__AlphaEntrypoint.remove_delegate, value: CGRIDMap__AlphaEntrypoint['remove_delegate'] } | { kind: CGRIDTag__AlphaEntrypoint.deposit, value: CGRIDMap__AlphaEntrypoint['deposit'] } | { kind: CGRIDTag__AlphaEntrypoint.named, value: CGRIDMap__AlphaEntrypoint['named'] };
export type AlphaContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export type AlphaContractIdOriginatedOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__AlphaContractIdOriginated{
    Originated = 1
}
export interface CGRIDMap__AlphaContractIdOriginated {
    Originated: CGRIDClass__AlphaContractIdOriginated__Originated
}
export type AlphaContractIdOriginated = { kind: CGRIDTag__AlphaContractIdOriginated.Originated, value: CGRIDMap__AlphaContractIdOriginated['Originated'] };
export enum CGRIDTag__AlphaContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__AlphaContractId {
    Implicit: CGRIDClass__AlphaContractId__Implicit,
    Originated: CGRIDClass__AlphaContractId__Originated
}
export type AlphaContractId = { kind: CGRIDTag__AlphaContractId.Implicit, value: CGRIDMap__AlphaContractId['Implicit'] } | { kind: CGRIDTag__AlphaContractId.Originated, value: CGRIDMap__AlphaContractId['Originated'] };
export type AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type AlphaBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type AlphaBlockHeaderAlphaSignedContentsSignature = { signature_v1: Bytes };
export type AlphaBlockHeaderAlphaFullHeader = { level: Int32, proto: Uint8, predecessor: CGRIDClass__Block_headerShell_predecessor, timestamp: Int64, validation_pass: Uint8, operations_hash: CGRIDClass__Block_headerShell_operations_hash, fitness: Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>, context: CGRIDClass__Block_headerShell_context, payload_hash: CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_toggle_vote: Int8, signature: CGRIDClass__AlphaBlock_headerAlphaSigned_contents_signature };
export type AlphaOperation = { branch: CGRIDClass__OperationShell_header_branch, contents_and_signature_prefix: VPadded<Sequence<CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix>,64>, signature_suffix: FixedBytes<64> };
export class CGRIDClass__AlphaOperation extends Box<AlphaOperation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'contents_and_signature_prefix', 'signature_suffix']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperation {
        return new this(record_decoder<AlphaOperation>({branch: CGRIDClass__OperationShell_header_branch.decode, contents_and_signature_prefix: VPadded.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix.decode), 64), signature_suffix: FixedBytes.decode<64>({len: 64})}, {order: ['branch', 'contents_and_signature_prefix', 'signature_suffix']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.contents_and_signature_prefix.encodeLength +  this.value.signature_suffix.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.contents_and_signature_prefix.writeTarget(tgt) +  this.value.signature_suffix.writeTarget(tgt));
    }
}
export const alpha_operation_encoder = (value: AlphaOperation): OutputBytes => {
    return record_encoder({order: ['branch', 'contents_and_signature_prefix', 'signature_suffix']})(value);
}
export const alpha_operation_decoder = (p: Parser): AlphaOperation => {
    return record_decoder<AlphaOperation>({branch: CGRIDClass__OperationShell_header_branch.decode, contents_and_signature_prefix: VPadded.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_or_signature_prefix.decode), 64), signature_suffix: FixedBytes.decode<64>({len: 64})}, {order: ['branch', 'contents_and_signature_prefix', 'signature_suffix']})(p);
}
