import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
export type AlphaGasCost = Z;
export class CGRIDClass__AlphaGasCost extends Box<AlphaGasCost> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaGasCost {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const alpha_gas_cost_encoder = (value: AlphaGasCost): OutputBytes => {
    return value.encode();
}
export const alpha_gas_cost_decoder = (p: Parser): AlphaGasCost => {
    return Z.decode(p);
}
