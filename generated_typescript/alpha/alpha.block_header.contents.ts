import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash generated for AlphaBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash extends Box<AlphaBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<AlphaBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
export type AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type AlphaBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type AlphaBlockHeaderContents = { payload_hash: CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_toggle_vote: Int8 };
export class CGRIDClass__AlphaBlockHeaderContents extends Box<AlphaBlockHeaderContents> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBlockHeaderContents {
        return new this(record_decoder<AlphaBlockHeaderContents>({payload_hash: CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote']})(p));
    };
    get encodeLength(): number {
        return (this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_toggle_vote.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_toggle_vote.writeTarget(tgt));
    }
}
export const alpha_block_header_contents_encoder = (value: AlphaBlockHeaderContents): OutputBytes => {
    return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote']})(value);
}
export const alpha_block_header_contents_decoder = (p: Parser): AlphaBlockHeaderContents => {
    return record_decoder<AlphaBlockHeaderContents>({payload_hash: CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote']})(p);
}
