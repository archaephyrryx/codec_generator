import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type AlphaVoteBallot = Int8;
export class CGRIDClass__AlphaVoteBallot extends Box<AlphaVoteBallot> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaVoteBallot {
        return new this(Int8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const alpha_vote_ballot_encoder = (value: AlphaVoteBallot): OutputBytes => {
    return value.encode();
}
export const alpha_vote_ballot_decoder = (p: Parser): AlphaVoteBallot => {
    return Int8.decode(p);
}
