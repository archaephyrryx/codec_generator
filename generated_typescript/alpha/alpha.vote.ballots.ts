import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type AlphaVoteBallots = { yay: Int64, nay: Int64, pass: Int64 };
export class CGRIDClass__AlphaVoteBallots extends Box<AlphaVoteBallots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['yay', 'nay', 'pass']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaVoteBallots {
        return new this(record_decoder<AlphaVoteBallots>({yay: Int64.decode, nay: Int64.decode, pass: Int64.decode}, {order: ['yay', 'nay', 'pass']})(p));
    };
    get encodeLength(): number {
        return (this.value.yay.encodeLength +  this.value.nay.encodeLength +  this.value.pass.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.yay.writeTarget(tgt) +  this.value.nay.writeTarget(tgt) +  this.value.pass.writeTarget(tgt));
    }
}
export const alpha_vote_ballots_encoder = (value: AlphaVoteBallots): OutputBytes => {
    return record_encoder({order: ['yay', 'nay', 'pass']})(value);
}
export const alpha_vote_ballots_decoder = (p: Parser): AlphaVoteBallots => {
    return record_decoder<AlphaVoteBallots>({yay: Int64.decode, nay: Int64.decode, pass: Int64.decode}, {order: ['yay', 'nay', 'pass']})(p);
}
