import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int31 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type AlphaScriptLoc = Int31;
export class CGRIDClass__AlphaScriptLoc extends Box<AlphaScriptLoc> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaScriptLoc {
        return new this(Int31.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const alpha_script_loc_encoder = (value: AlphaScriptLoc): OutputBytes => {
    return value.encode();
}
export const alpha_script_loc_decoder = (p: Parser): AlphaScriptLoc => {
    return Int31.decode(p);
}
