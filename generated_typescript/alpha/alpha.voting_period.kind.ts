import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__AlphaVotingPeriodKind__exploration generated for AlphaVotingPeriodKind__exploration
export class CGRIDClass__AlphaVotingPeriodKind__exploration extends Box<AlphaVotingPeriodKind__exploration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriodKind__exploration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaVotingPeriodKind__Proposal generated for AlphaVotingPeriodKind__Proposal
export class CGRIDClass__AlphaVotingPeriodKind__Proposal extends Box<AlphaVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaVotingPeriodKind__Promotion generated for AlphaVotingPeriodKind__Promotion
export class CGRIDClass__AlphaVotingPeriodKind__Promotion extends Box<AlphaVotingPeriodKind__Promotion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriodKind__Promotion {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaVotingPeriodKind__Cooldown generated for AlphaVotingPeriodKind__Cooldown
export class CGRIDClass__AlphaVotingPeriodKind__Cooldown extends Box<AlphaVotingPeriodKind__Cooldown> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriodKind__Cooldown {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaVotingPeriodKind__Adoption generated for AlphaVotingPeriodKind__Adoption
export class CGRIDClass__AlphaVotingPeriodKind__Adoption extends Box<AlphaVotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type AlphaVotingPeriodKind__exploration = Unit;
export type AlphaVotingPeriodKind__Proposal = Unit;
export type AlphaVotingPeriodKind__Promotion = Unit;
export type AlphaVotingPeriodKind__Cooldown = Unit;
export type AlphaVotingPeriodKind__Adoption = Unit;
export enum CGRIDTag__AlphaVotingPeriodKind{
    Proposal = 0,
    exploration = 1,
    Cooldown = 2,
    Promotion = 3,
    Adoption = 4
}
export interface CGRIDMap__AlphaVotingPeriodKind {
    Proposal: CGRIDClass__AlphaVotingPeriodKind__Proposal,
    exploration: CGRIDClass__AlphaVotingPeriodKind__exploration,
    Cooldown: CGRIDClass__AlphaVotingPeriodKind__Cooldown,
    Promotion: CGRIDClass__AlphaVotingPeriodKind__Promotion,
    Adoption: CGRIDClass__AlphaVotingPeriodKind__Adoption
}
export type AlphaVotingPeriodKind = { kind: CGRIDTag__AlphaVotingPeriodKind.Proposal, value: CGRIDMap__AlphaVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__AlphaVotingPeriodKind.exploration, value: CGRIDMap__AlphaVotingPeriodKind['exploration'] } | { kind: CGRIDTag__AlphaVotingPeriodKind.Cooldown, value: CGRIDMap__AlphaVotingPeriodKind['Cooldown'] } | { kind: CGRIDTag__AlphaVotingPeriodKind.Promotion, value: CGRIDMap__AlphaVotingPeriodKind['Promotion'] } | { kind: CGRIDTag__AlphaVotingPeriodKind.Adoption, value: CGRIDMap__AlphaVotingPeriodKind['Adoption'] };
export function alphavotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__AlphaVotingPeriodKind,AlphaVotingPeriodKind> {
    function f(disc: CGRIDTag__AlphaVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__AlphaVotingPeriodKind.Proposal: return CGRIDClass__AlphaVotingPeriodKind__Proposal.decode;
            case CGRIDTag__AlphaVotingPeriodKind.exploration: return CGRIDClass__AlphaVotingPeriodKind__exploration.decode;
            case CGRIDTag__AlphaVotingPeriodKind.Cooldown: return CGRIDClass__AlphaVotingPeriodKind__Cooldown.decode;
            case CGRIDTag__AlphaVotingPeriodKind.Promotion: return CGRIDClass__AlphaVotingPeriodKind__Promotion.decode;
            case CGRIDTag__AlphaVotingPeriodKind.Adoption: return CGRIDClass__AlphaVotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaVotingPeriodKind => Object.values(CGRIDTag__AlphaVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaVotingPeriodKind extends Box<AlphaVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaVotingPeriodKind>, AlphaVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriodKind {
        return new this(variant_decoder(width.Uint8)(alphavotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const alpha_voting_period_kind_encoder = (value: AlphaVotingPeriodKind): OutputBytes => {
    return variant_encoder<KindOf<AlphaVotingPeriodKind>, AlphaVotingPeriodKind>(width.Uint8)(value);
}
export const alpha_voting_period_kind_decoder = (p: Parser): AlphaVotingPeriodKind => {
    return variant_decoder(width.Uint8)(alphavotingperiodkind_mkDecoder())(p);
}
