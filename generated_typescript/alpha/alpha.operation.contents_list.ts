import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Nullable } from '../../ts_runtime/composite/opt/nullable';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { SequenceBounded } from '../../ts_runtime/composite/seq/sequence.bounded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int16, Int31, Int32, Int64, Int8, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Bls generated for PublicKey__Bls
export class CGRIDClass__PublicKey__Bls extends Box<PublicKey__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Bls {
        return new this(record_decoder<PublicKey__Bls>({bls12_381_public_key: FixedBytes.decode<48>({len: 48})}, {order: ['bls12_381_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Bls generated for PublicKeyHash__Bls
export class CGRIDClass__PublicKeyHash__Bls extends Box<PublicKeyHash__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Bls {
        return new this(record_decoder<PublicKeyHash__Bls>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__String generated for MichelineAlphaMichelsonV1Expression__String
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__String extends Box<MichelineAlphaMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__String {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence generated for MichelineAlphaMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence extends Box<MichelineAlphaMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineAlphaMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic generated for MichelineAlphaMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic extends Box<MichelineAlphaMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineAlphaMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Int generated for MichelineAlphaMichelsonV1Expression__Int
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Int extends Box<MichelineAlphaMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes generated for MichelineAlphaMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes extends Box<MichelineAlphaMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_update generated for AlphaOperationAlphaContents__Zk_rollup_update
export class CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_update extends Box<AlphaOperationAlphaContents__Zk_rollup_update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'update']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_update {
        return new this(record_decoder<AlphaOperationAlphaContents__Zk_rollup_update>({source: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, zk_rollup: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_zk_rollup.decode, update: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'update']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.zk_rollup.encodeLength +  this.value.update.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.zk_rollup.writeTarget(tgt) +  this.value.update.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_publish generated for AlphaOperationAlphaContents__Zk_rollup_publish
export class CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_publish extends Box<AlphaOperationAlphaContents__Zk_rollup_publish> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'op']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_publish {
        return new this(record_decoder<AlphaOperationAlphaContents__Zk_rollup_publish>({source: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, zk_rollup: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_zk_rollup.decode, op: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'op']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.zk_rollup.encodeLength +  this.value.op.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.zk_rollup.writeTarget(tgt) +  this.value.op.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_origination generated for AlphaOperationAlphaContents__Zk_rollup_origination
export class CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_origination extends Box<AlphaOperationAlphaContents__Zk_rollup_origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_parameters', 'circuits_info', 'init_state', 'nb_ops']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_origination {
        return new this(record_decoder<AlphaOperationAlphaContents__Zk_rollup_origination>({source: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_parameters: Dynamic.decode(Bytes.decode, width.Uint30), circuits_info: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq.decode), width.Uint30), init_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30), nb_ops: Int31.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_parameters', 'circuits_info', 'init_state', 'nb_ops']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_parameters.encodeLength +  this.value.circuits_info.encodeLength +  this.value.init_state.encodeLength +  this.value.nb_ops.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_parameters.writeTarget(tgt) +  this.value.circuits_info.writeTarget(tgt) +  this.value.init_state.writeTarget(tgt) +  this.value.nb_ops.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Vdf_revelation generated for AlphaOperationAlphaContents__Vdf_revelation
export class CGRIDClass__AlphaOperationAlphaContents__Vdf_revelation extends Box<AlphaOperationAlphaContents__Vdf_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['solution']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Vdf_revelation {
        return new this(record_decoder<AlphaOperationAlphaContents__Vdf_revelation>({solution: CGRIDClass__AlphaOperationAlphaContents_Vdf_revelation_solution.decode}, {order: ['solution']})(p));
    };
    get encodeLength(): number {
        return (this.value.solution.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.solution.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Update_consensus_key generated for AlphaOperationAlphaContents__Update_consensus_key
export class CGRIDClass__AlphaOperationAlphaContents__Update_consensus_key extends Box<AlphaOperationAlphaContents__Update_consensus_key> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pk']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Update_consensus_key {
        return new this(record_decoder<AlphaOperationAlphaContents__Update_consensus_key>({source: CGRIDClass__AlphaOperationAlphaContents_Update_consensus_key_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, pk: CGRIDClass__AlphaOperationAlphaContents_Update_consensus_key_pk.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pk']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.pk.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.pk.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_submit_batch generated for AlphaOperationAlphaContents__Tx_rollup_submit_batch
export class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_submit_batch extends Box<AlphaOperationAlphaContents__Tx_rollup_submit_batch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'content', 'burn_limit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_submit_batch {
        return new this(record_decoder<AlphaOperationAlphaContents__Tx_rollup_submit_batch>({source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_submit_batch_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode, content: Dynamic.decode(U8String.decode, width.Uint30), burn_limit: Option.decode(N.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'content', 'burn_limit']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.content.encodeLength +  this.value.burn_limit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.content.writeTarget(tgt) +  this.value.burn_limit.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_return_bond generated for AlphaOperationAlphaContents__Tx_rollup_return_bond
export class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_return_bond extends Box<AlphaOperationAlphaContents__Tx_rollup_return_bond> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_return_bond {
        return new this(record_decoder<AlphaOperationAlphaContents__Tx_rollup_return_bond>({source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_return_bond_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_remove_commitment generated for AlphaOperationAlphaContents__Tx_rollup_remove_commitment
export class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_remove_commitment extends Box<AlphaOperationAlphaContents__Tx_rollup_remove_commitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_remove_commitment {
        return new this(record_decoder<AlphaOperationAlphaContents__Tx_rollup_remove_commitment>({source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_remove_commitment_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_rejection generated for AlphaOperationAlphaContents__Tx_rollup_rejection
export class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_rejection extends Box<AlphaOperationAlphaContents__Tx_rollup_rejection> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'level', 'message', 'message_position', 'message_path', 'message_result_hash', 'message_result_path', 'previous_message_result', 'previous_message_result_path', 'proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_rejection {
        return new this(record_decoder<AlphaOperationAlphaContents__Tx_rollup_rejection>({source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode, level: Int32.decode, message: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message.decode, message_position: N.decode, message_path: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq.decode), width.Uint30), message_result_hash: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_result_hash.decode, message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), previous_message_result: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result.decode, previous_message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), proof: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'level', 'message', 'message_position', 'message_path', 'message_result_hash', 'message_result_path', 'previous_message_result', 'previous_message_result_path', 'proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.level.encodeLength +  this.value.message.encodeLength +  this.value.message_position.encodeLength +  this.value.message_path.encodeLength +  this.value.message_result_hash.encodeLength +  this.value.message_result_path.encodeLength +  this.value.previous_message_result.encodeLength +  this.value.previous_message_result_path.encodeLength +  this.value.proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.message.writeTarget(tgt) +  this.value.message_position.writeTarget(tgt) +  this.value.message_path.writeTarget(tgt) +  this.value.message_result_hash.writeTarget(tgt) +  this.value.message_result_path.writeTarget(tgt) +  this.value.previous_message_result.writeTarget(tgt) +  this.value.previous_message_result_path.writeTarget(tgt) +  this.value.proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_origination generated for AlphaOperationAlphaContents__Tx_rollup_origination
export class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_origination extends Box<AlphaOperationAlphaContents__Tx_rollup_origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup_origination']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_origination {
        return new this(record_decoder<AlphaOperationAlphaContents__Tx_rollup_origination>({source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, tx_rollup_origination: Unit.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup_origination']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.tx_rollup_origination.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.tx_rollup_origination.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_finalize_commitment generated for AlphaOperationAlphaContents__Tx_rollup_finalize_commitment
export class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_finalize_commitment extends Box<AlphaOperationAlphaContents__Tx_rollup_finalize_commitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_finalize_commitment {
        return new this(record_decoder<AlphaOperationAlphaContents__Tx_rollup_finalize_commitment>({source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_finalize_commitment_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_dispatch_tickets generated for AlphaOperationAlphaContents__Tx_rollup_dispatch_tickets
export class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_dispatch_tickets extends Box<AlphaOperationAlphaContents__Tx_rollup_dispatch_tickets> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup', 'level', 'context_hash', 'message_index', 'message_result_path', 'tickets_info']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_dispatch_tickets {
        return new this(record_decoder<AlphaOperationAlphaContents__Tx_rollup_dispatch_tickets>({source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, tx_rollup: CGRIDClass__AlphaTx_rollup_id.decode, level: Int32.decode, context_hash: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash.decode, message_index: Int31.decode, message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), tickets_info: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup', 'level', 'context_hash', 'message_index', 'message_result_path', 'tickets_info']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.tx_rollup.encodeLength +  this.value.level.encodeLength +  this.value.context_hash.encodeLength +  this.value.message_index.encodeLength +  this.value.message_result_path.encodeLength +  this.value.tickets_info.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.tx_rollup.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.context_hash.writeTarget(tgt) +  this.value.message_index.writeTarget(tgt) +  this.value.message_result_path.writeTarget(tgt) +  this.value.tickets_info.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_commit generated for AlphaOperationAlphaContents__Tx_rollup_commit
export class CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_commit extends Box<AlphaOperationAlphaContents__Tx_rollup_commit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_commit {
        return new this(record_decoder<AlphaOperationAlphaContents__Tx_rollup_commit>({source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaTx_rollup_id.decode, commitment: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Transfer_ticket generated for AlphaOperationAlphaContents__Transfer_ticket
export class CGRIDClass__AlphaOperationAlphaContents__Transfer_ticket extends Box<AlphaOperationAlphaContents__Transfer_ticket> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'ticket_contents', 'ticket_ty', 'ticket_ticketer', 'ticket_amount', 'destination', 'entrypoint']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Transfer_ticket {
        return new this(record_decoder<AlphaOperationAlphaContents__Transfer_ticket>({source: CGRIDClass__AlphaOperationAlphaContents_Transfer_ticket_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, ticket_contents: Dynamic.decode(Bytes.decode, width.Uint30), ticket_ty: Dynamic.decode(Bytes.decode, width.Uint30), ticket_ticketer: CGRIDClass__AlphaContract_id.decode, ticket_amount: N.decode, destination: CGRIDClass__AlphaContract_id.decode, entrypoint: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'ticket_contents', 'ticket_ty', 'ticket_ticketer', 'ticket_amount', 'destination', 'entrypoint']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.ticket_contents.encodeLength +  this.value.ticket_ty.encodeLength +  this.value.ticket_ticketer.encodeLength +  this.value.ticket_amount.encodeLength +  this.value.destination.encodeLength +  this.value.entrypoint.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.ticket_contents.writeTarget(tgt) +  this.value.ticket_ty.writeTarget(tgt) +  this.value.ticket_ticketer.writeTarget(tgt) +  this.value.ticket_amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.entrypoint.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Transaction generated for AlphaOperationAlphaContents__Transaction
export class CGRIDClass__AlphaOperationAlphaContents__Transaction extends Box<AlphaOperationAlphaContents__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Transaction {
        return new this(record_decoder<AlphaOperationAlphaContents__Transaction>({source: CGRIDClass__AlphaOperationAlphaContents_Transaction_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: N.decode, destination: CGRIDClass__AlphaContract_id.decode, parameters: Option.decode(CGRIDClass__AlphaOperationAlphaContents_Transaction_parameters.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_timeout generated for AlphaOperationAlphaContents__Smart_rollup_timeout
export class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_timeout extends Box<AlphaOperationAlphaContents__Smart_rollup_timeout> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'stakers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_timeout {
        return new this(record_decoder<AlphaOperationAlphaContents__Smart_rollup_timeout>({source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaSmart_rollup_address.decode, stakers: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'stakers']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.stakers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.stakers.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_refute generated for AlphaOperationAlphaContents__Smart_rollup_refute
export class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_refute extends Box<AlphaOperationAlphaContents__Smart_rollup_refute> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'opponent', 'refutation']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_refute {
        return new this(record_decoder<AlphaOperationAlphaContents__Smart_rollup_refute>({source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaSmart_rollup_address.decode, opponent: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_opponent.decode, refutation: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'opponent', 'refutation']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.opponent.encodeLength +  this.value.refutation.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.opponent.writeTarget(tgt) +  this.value.refutation.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_recover_bond generated for AlphaOperationAlphaContents__Smart_rollup_recover_bond
export class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_recover_bond extends Box<AlphaOperationAlphaContents__Smart_rollup_recover_bond> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'staker']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_recover_bond {
        return new this(record_decoder<AlphaOperationAlphaContents__Smart_rollup_recover_bond>({source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_rollup.decode, staker: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_staker.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'staker']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.staker.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.staker.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_publish generated for AlphaOperationAlphaContents__Smart_rollup_publish
export class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_publish extends Box<AlphaOperationAlphaContents__Smart_rollup_publish> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_publish {
        return new this(record_decoder<AlphaOperationAlphaContents__Smart_rollup_publish>({source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaSmart_rollup_address.decode, commitment: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_originate generated for AlphaOperationAlphaContents__Smart_rollup_originate
export class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_originate extends Box<AlphaOperationAlphaContents__Smart_rollup_originate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pvm_kind', 'kernel', 'origination_proof', 'parameters_ty']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_originate {
        return new this(record_decoder<AlphaOperationAlphaContents__Smart_rollup_originate>({source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_originate_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, pvm_kind: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_originate_pvm_kind.decode, kernel: Dynamic.decode(U8String.decode, width.Uint30), origination_proof: Dynamic.decode(U8String.decode, width.Uint30), parameters_ty: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pvm_kind', 'kernel', 'origination_proof', 'parameters_ty']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.pvm_kind.encodeLength +  this.value.kernel.encodeLength +  this.value.origination_proof.encodeLength +  this.value.parameters_ty.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.pvm_kind.writeTarget(tgt) +  this.value.kernel.writeTarget(tgt) +  this.value.origination_proof.writeTarget(tgt) +  this.value.parameters_ty.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_execute_outbox_message generated for AlphaOperationAlphaContents__Smart_rollup_execute_outbox_message
export class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_execute_outbox_message extends Box<AlphaOperationAlphaContents__Smart_rollup_execute_outbox_message> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'cemented_commitment', 'output_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_execute_outbox_message {
        return new this(record_decoder<AlphaOperationAlphaContents__Smart_rollup_execute_outbox_message>({source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_execute_outbox_message_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaSmart_rollup_address.decode, cemented_commitment: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_execute_outbox_message_cemented_commitment.decode, output_proof: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'cemented_commitment', 'output_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.cemented_commitment.encodeLength +  this.value.output_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.cemented_commitment.writeTarget(tgt) +  this.value.output_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_cement generated for AlphaOperationAlphaContents__Smart_rollup_cement
export class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_cement extends Box<AlphaOperationAlphaContents__Smart_rollup_cement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_cement {
        return new this(record_decoder<AlphaOperationAlphaContents__Smart_rollup_cement>({source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_cement_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__AlphaSmart_rollup_address.decode, commitment: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_cement_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_add_messages generated for AlphaOperationAlphaContents__Smart_rollup_add_messages
export class CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_add_messages extends Box<AlphaOperationAlphaContents__Smart_rollup_add_messages> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'message']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_add_messages {
        return new this(record_decoder<AlphaOperationAlphaContents__Smart_rollup_add_messages>({source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_add_messages_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, message: Dynamic.decode(Sequence.decode(Dynamic.decode(U8String.decode, width.Uint30)), width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'message']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.message.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.message.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Set_deposits_limit generated for AlphaOperationAlphaContents__Set_deposits_limit
export class CGRIDClass__AlphaOperationAlphaContents__Set_deposits_limit extends Box<AlphaOperationAlphaContents__Set_deposits_limit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'limit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Set_deposits_limit {
        return new this(record_decoder<AlphaOperationAlphaContents__Set_deposits_limit>({source: CGRIDClass__AlphaOperationAlphaContents_Set_deposits_limit_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, limit: Option.decode(N.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'limit']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.limit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.limit.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Seed_nonce_revelation generated for AlphaOperationAlphaContents__Seed_nonce_revelation
export class CGRIDClass__AlphaOperationAlphaContents__Seed_nonce_revelation extends Box<AlphaOperationAlphaContents__Seed_nonce_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Seed_nonce_revelation {
        return new this(record_decoder<AlphaOperationAlphaContents__Seed_nonce_revelation>({level: Int32.decode, nonce: FixedBytes.decode<32>({len: 32})}, {order: ['level', 'nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Reveal generated for AlphaOperationAlphaContents__Reveal
export class CGRIDClass__AlphaOperationAlphaContents__Reveal extends Box<AlphaOperationAlphaContents__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Reveal {
        return new this(record_decoder<AlphaOperationAlphaContents__Reveal>({source: CGRIDClass__AlphaOperationAlphaContents_Reveal_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_key: CGRIDClass__AlphaOperationAlphaContents_Reveal_public_key.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Register_global_constant generated for AlphaOperationAlphaContents__Register_global_constant
export class CGRIDClass__AlphaOperationAlphaContents__Register_global_constant extends Box<AlphaOperationAlphaContents__Register_global_constant> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Register_global_constant {
        return new this(record_decoder<AlphaOperationAlphaContents__Register_global_constant>({source: CGRIDClass__AlphaOperationAlphaContents_Register_global_constant_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Proposals generated for AlphaOperationAlphaContents__Proposals
export class CGRIDClass__AlphaOperationAlphaContents__Proposals extends Box<AlphaOperationAlphaContents__Proposals> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposals']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Proposals {
        return new this(record_decoder<AlphaOperationAlphaContents__Proposals>({source: CGRIDClass__AlphaOperationAlphaContents_Proposals_source.decode, period: Int32.decode, proposals: Dynamic.decode(SequenceBounded.decode(CGRIDClass__AlphaOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq.decode, 20, None), width.Uint30)}, {order: ['source', 'period', 'proposals']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposals.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposals.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Preendorsement generated for AlphaOperationAlphaContents__Preendorsement
export class CGRIDClass__AlphaOperationAlphaContents__Preendorsement extends Box<AlphaOperationAlphaContents__Preendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Preendorsement {
        return new this(record_decoder<AlphaOperationAlphaContents__Preendorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__AlphaOperationAlphaContents_Preendorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Origination generated for AlphaOperationAlphaContents__Origination
export class CGRIDClass__AlphaOperationAlphaContents__Origination extends Box<AlphaOperationAlphaContents__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Origination {
        return new this(record_decoder<AlphaOperationAlphaContents__Origination>({source: CGRIDClass__AlphaOperationAlphaContents_Origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, balance: N.decode, delegate: Option.decode(CGRIDClass__AlphaOperationAlphaContents_Origination_delegate.decode), script: CGRIDClass__AlphaScriptedContracts.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Increase_paid_storage generated for AlphaOperationAlphaContents__Increase_paid_storage
export class CGRIDClass__AlphaOperationAlphaContents__Increase_paid_storage extends Box<AlphaOperationAlphaContents__Increase_paid_storage> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Increase_paid_storage {
        return new this(record_decoder<AlphaOperationAlphaContents__Increase_paid_storage>({source: CGRIDClass__AlphaOperationAlphaContents_Increase_paid_storage_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: Z.decode, destination: CGRIDClass__AlphaContract_idOriginated.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Failing_noop generated for AlphaOperationAlphaContents__Failing_noop
export class CGRIDClass__AlphaOperationAlphaContents__Failing_noop extends Box<AlphaOperationAlphaContents__Failing_noop> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['arbitrary']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Failing_noop {
        return new this(record_decoder<AlphaOperationAlphaContents__Failing_noop>({arbitrary: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['arbitrary']})(p));
    };
    get encodeLength(): number {
        return (this.value.arbitrary.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.arbitrary.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Endorsement generated for AlphaOperationAlphaContents__Endorsement
export class CGRIDClass__AlphaOperationAlphaContents__Endorsement extends Box<AlphaOperationAlphaContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Endorsement {
        return new this(record_decoder<AlphaOperationAlphaContents__Endorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__AlphaOperationAlphaContents_Endorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Drain_delegate generated for AlphaOperationAlphaContents__Drain_delegate
export class CGRIDClass__AlphaOperationAlphaContents__Drain_delegate extends Box<AlphaOperationAlphaContents__Drain_delegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['consensus_key', 'delegate', 'destination']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Drain_delegate {
        return new this(record_decoder<AlphaOperationAlphaContents__Drain_delegate>({consensus_key: CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_consensus_key.decode, delegate: CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_delegate.decode, destination: CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_destination.decode}, {order: ['consensus_key', 'delegate', 'destination']})(p));
    };
    get encodeLength(): number {
        return (this.value.consensus_key.encodeLength +  this.value.delegate.encodeLength +  this.value.destination.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.consensus_key.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.destination.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Double_preendorsement_evidence generated for AlphaOperationAlphaContents__Double_preendorsement_evidence
export class CGRIDClass__AlphaOperationAlphaContents__Double_preendorsement_evidence extends Box<AlphaOperationAlphaContents__Double_preendorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Double_preendorsement_evidence {
        return new this(record_decoder<AlphaOperationAlphaContents__Double_preendorsement_evidence>({op1: Dynamic.decode(CGRIDClass__AlphaInlinedPreendorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__AlphaInlinedPreendorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Double_endorsement_evidence generated for AlphaOperationAlphaContents__Double_endorsement_evidence
export class CGRIDClass__AlphaOperationAlphaContents__Double_endorsement_evidence extends Box<AlphaOperationAlphaContents__Double_endorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Double_endorsement_evidence {
        return new this(record_decoder<AlphaOperationAlphaContents__Double_endorsement_evidence>({op1: Dynamic.decode(CGRIDClass__AlphaInlinedEndorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__AlphaInlinedEndorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Double_baking_evidence generated for AlphaOperationAlphaContents__Double_baking_evidence
export class CGRIDClass__AlphaOperationAlphaContents__Double_baking_evidence extends Box<AlphaOperationAlphaContents__Double_baking_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bh1', 'bh2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Double_baking_evidence {
        return new this(record_decoder<AlphaOperationAlphaContents__Double_baking_evidence>({bh1: Dynamic.decode(CGRIDClass__AlphaBlock_headerAlphaFull_header.decode, width.Uint30), bh2: Dynamic.decode(CGRIDClass__AlphaBlock_headerAlphaFull_header.decode, width.Uint30)}, {order: ['bh1', 'bh2']})(p));
    };
    get encodeLength(): number {
        return (this.value.bh1.encodeLength +  this.value.bh2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bh1.writeTarget(tgt) +  this.value.bh2.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Delegation generated for AlphaOperationAlphaContents__Delegation
export class CGRIDClass__AlphaOperationAlphaContents__Delegation extends Box<AlphaOperationAlphaContents__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Delegation {
        return new this(record_decoder<AlphaOperationAlphaContents__Delegation>({source: CGRIDClass__AlphaOperationAlphaContents_Delegation_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, delegate: Option.decode(CGRIDClass__AlphaOperationAlphaContents_Delegation_delegate.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Dal_publish_slot_header generated for AlphaOperationAlphaContents__Dal_publish_slot_header
export class CGRIDClass__AlphaOperationAlphaContents__Dal_publish_slot_header extends Box<AlphaOperationAlphaContents__Dal_publish_slot_header> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'slot_header']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Dal_publish_slot_header {
        return new this(record_decoder<AlphaOperationAlphaContents__Dal_publish_slot_header>({source: CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, slot_header: CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_slot_header.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'slot_header']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.slot_header.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.slot_header.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Dal_attestation generated for AlphaOperationAlphaContents__Dal_attestation
export class CGRIDClass__AlphaOperationAlphaContents__Dal_attestation extends Box<AlphaOperationAlphaContents__Dal_attestation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['attestor', 'attestation', 'level']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Dal_attestation {
        return new this(record_decoder<AlphaOperationAlphaContents__Dal_attestation>({attestor: CGRIDClass__AlphaOperationAlphaContents_Dal_attestation_attestor.decode, attestation: Z.decode, level: Int32.decode}, {order: ['attestor', 'attestation', 'level']})(p));
    };
    get encodeLength(): number {
        return (this.value.attestor.encodeLength +  this.value.attestation.encodeLength +  this.value.level.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.attestor.writeTarget(tgt) +  this.value.attestation.writeTarget(tgt) +  this.value.level.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Ballot generated for AlphaOperationAlphaContents__Ballot
export class CGRIDClass__AlphaOperationAlphaContents__Ballot extends Box<AlphaOperationAlphaContents__Ballot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposal', 'ballot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Ballot {
        return new this(record_decoder<AlphaOperationAlphaContents__Ballot>({source: CGRIDClass__AlphaOperationAlphaContents_Ballot_source.decode, period: Int32.decode, proposal: CGRIDClass__AlphaOperationAlphaContents_Ballot_proposal.decode, ballot: Int8.decode}, {order: ['source', 'period', 'proposal', 'ballot']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposal.encodeLength +  this.value.ballot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposal.writeTarget(tgt) +  this.value.ballot.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents__Activate_account generated for AlphaOperationAlphaContents__Activate_account
export class CGRIDClass__AlphaOperationAlphaContents__Activate_account extends Box<AlphaOperationAlphaContents__Activate_account> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'secret']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents__Activate_account {
        return new this(record_decoder<AlphaOperationAlphaContents__Activate_account>({pkh: CGRIDClass__AlphaOperationAlphaContents_Activate_account_pkh.decode, secret: FixedBytes.decode<20>({len: 20})}, {order: ['pkh', 'secret']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.secret.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.secret.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some generated for AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some
export class CGRIDClass__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some extends Box<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contents', 'ty', 'ticketer']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some>({contents: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, ty: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, ticketer: CGRIDClass__AlphaContract_id.decode}, {order: ['contents', 'ty', 'ticketer']})(p));
    };
    get encodeLength(): number {
        return (this.value.contents.encodeLength +  this.value.ty.encodeLength +  this.value.ticketer.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contents.writeTarget(tgt) +  this.value.ty.writeTarget(tgt) +  this.value.ticketer.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None generated for AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None
export class CGRIDClass__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None extends Box<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public generated for AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public
export class CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public extends Box<AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_public']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public>({_public: Unit.decode}, {order: ['_public']})(p));
    };
    get encodeLength(): number {
        return (this.value._public.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._public.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private generated for AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private
export class CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private extends Box<AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_private']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private>({_private: Unit.decode}, {order: ['_private']})(p));
    };
    get encodeLength(): number {
        return (this.value._private.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._private.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee generated for AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee
export class CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee extends Box<AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['fee']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee>({fee: Unit.decode}, {order: ['fee']})(p));
    };
    get encodeLength(): number {
        return (this.value.fee.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.fee.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessage__Deposit generated for AlphaOperationAlphaContentsTxRollupRejectionMessage__Deposit
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessage__Deposit extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessage__Deposit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['deposit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessage__Deposit {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionMessage__Deposit>({deposit: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit.decode}, {order: ['deposit']})(p));
    };
    get encodeLength(): number {
        return (this.value.deposit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.deposit.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessage__Batch generated for AlphaOperationAlphaContentsTxRollupRejectionMessage__Batch
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessage__Batch extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessage__Batch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['batch']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessage__Batch {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionMessage__Batch>({batch: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['batch']})(p));
    };
    get encodeLength(): number {
        return (this.value.batch.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.batch.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 generated for AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 generated for AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 generated for AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 generated for AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 {
        return new this(Uint8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 generated for AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 extends Box<AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 generated for AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 extends Box<AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 generated for AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 extends Box<AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 generated for AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 extends Box<AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 {
        return new this(Uint8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some generated for AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some extends Box<AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some>({commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None generated for AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None
export class CGRIDClass__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None extends Box<AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Start generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Start
export class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Start extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Start> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['refutation_kind', 'player_commitment_hash', 'opponent_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Start {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Start>({refutation_kind: Unit.decode, player_commitment_hash: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Start_player_commitment_hash.decode, opponent_commitment_hash: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Start_opponent_commitment_hash.decode}, {order: ['refutation_kind', 'player_commitment_hash', 'opponent_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.refutation_kind.encodeLength +  this.value.player_commitment_hash.encodeLength +  this.value.opponent_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.refutation_kind.writeTarget(tgt) +  this.value.player_commitment_hash.writeTarget(tgt) +  this.value.opponent_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Move generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Move
export class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Move extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Move> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['refutation_kind', 'choice', 'step']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Move {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Move>({refutation_kind: Unit.decode, choice: N.decode, step: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step.decode}, {order: ['refutation_kind', 'choice', 'step']})(p));
    };
    get encodeLength(): number {
        return (this.value.refutation_kind.encodeLength +  this.value.choice.encodeLength +  this.value.step.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.refutation_kind.writeTarget(tgt) +  this.value.choice.writeTarget(tgt) +  this.value.step.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof
export class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pvm_step', 'input_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof>({pvm_step: Dynamic.decode(U8String.decode, width.Uint30), input_proof: Option.decode(CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof.decode)}, {order: ['pvm_step', 'input_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.pvm_step.encodeLength +  this.value.input_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pvm_step.writeTarget(tgt) +  this.value.input_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection
export class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof
export class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['input_proof_kind', 'reveal_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof>({input_proof_kind: Unit.decode, reveal_proof: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof.decode}, {order: ['input_proof_kind', 'reveal_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.input_proof_kind.encodeLength +  this.value.reveal_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.input_proof_kind.writeTarget(tgt) +  this.value.reveal_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof
export class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['input_proof_kind', 'level', 'message_counter', 'serialized_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof>({input_proof_kind: Unit.decode, level: Int32.decode, message_counter: N.decode, serialized_proof: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['input_proof_kind', 'level', 'message_counter', 'serialized_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.input_proof_kind.encodeLength +  this.value.level.encodeLength +  this.value.message_counter.encodeLength +  this.value.serialized_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.input_proof_kind.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.message_counter.writeTarget(tgt) +  this.value.serialized_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input
export class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['input_proof_kind']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input>({input_proof_kind: Unit.decode}, {order: ['input_proof_kind']})(p));
    };
    get encodeLength(): number {
        return (this.value.input_proof_kind.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.input_proof_kind.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof
export class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['reveal_proof_kind', 'raw_data']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof>({reveal_proof_kind: Unit.decode, raw_data: Dynamic.decode(U8String.decode, width.Uint16)}, {order: ['reveal_proof_kind', 'raw_data']})(p));
    };
    get encodeLength(): number {
        return (this.value.reveal_proof_kind.encodeLength +  this.value.raw_data.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.reveal_proof_kind.writeTarget(tgt) +  this.value.raw_data.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof
export class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['reveal_proof_kind']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof>({reveal_proof_kind: Unit.decode}, {order: ['reveal_proof_kind']})(p));
    };
    get encodeLength(): number {
        return (this.value.reveal_proof_kind.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.reveal_proof_kind.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof
export class CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['reveal_proof_kind', 'dal_page_id', 'dal_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof>({reveal_proof_kind: Unit.decode, dal_page_id: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id.decode, dal_proof: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['reveal_proof_kind', 'dal_page_id', 'dal_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.reveal_proof_kind.encodeLength +  this.value.dal_page_id.encodeLength +  this.value.dal_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.reveal_proof_kind.writeTarget(tgt) +  this.value.dal_page_id.writeTarget(tgt) +  this.value.dal_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedPreendorsementContents__Preendorsement generated for AlphaInlinedPreendorsementContents__Preendorsement
export class CGRIDClass__AlphaInlinedPreendorsementContents__Preendorsement extends Box<AlphaInlinedPreendorsementContents__Preendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedPreendorsementContents__Preendorsement {
        return new this(record_decoder<AlphaInlinedPreendorsementContents__Preendorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__AlphaInlinedPreendorsementContents_Preendorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedEndorsementMempoolContents__Endorsement generated for AlphaInlinedEndorsementMempoolContents__Endorsement
export class CGRIDClass__AlphaInlinedEndorsementMempoolContents__Endorsement extends Box<AlphaInlinedEndorsementMempoolContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedEndorsementMempoolContents__Endorsement {
        return new this(record_decoder<AlphaInlinedEndorsementMempoolContents__Endorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__AlphaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaEntrypoint__set_delegate generated for AlphaEntrypoint__set_delegate
export class CGRIDClass__AlphaEntrypoint__set_delegate extends Box<AlphaEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__root generated for AlphaEntrypoint__root
export class CGRIDClass__AlphaEntrypoint__root extends Box<AlphaEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__remove_delegate generated for AlphaEntrypoint__remove_delegate
export class CGRIDClass__AlphaEntrypoint__remove_delegate extends Box<AlphaEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__named generated for AlphaEntrypoint__named
export class CGRIDClass__AlphaEntrypoint__named extends Box<AlphaEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__deposit generated for AlphaEntrypoint__deposit
export class CGRIDClass__AlphaEntrypoint__deposit extends Box<AlphaEntrypoint__deposit> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__deposit {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint___do generated for AlphaEntrypoint___do
export class CGRIDClass__AlphaEntrypoint___do extends Box<AlphaEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint___default generated for AlphaEntrypoint___default
export class CGRIDClass__AlphaEntrypoint___default extends Box<AlphaEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaContractId__Originated generated for AlphaContractId__Originated
export class CGRIDClass__AlphaContractId__Originated extends Box<AlphaContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaContractId__Originated {
        return new this(Padded.decode(CGRIDClass__AlphaContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaContractId__Implicit generated for AlphaContractId__Implicit
export class CGRIDClass__AlphaContractId__Implicit extends Box<AlphaContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContractId__Implicit {
        return new this(record_decoder<AlphaContractId__Implicit>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContractIdOriginated__Originated generated for AlphaContractIdOriginated__Originated
export class CGRIDClass__AlphaContractIdOriginated__Originated extends Box<AlphaContractIdOriginated__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaContractIdOriginated__Originated {
        return new this(Padded.decode(CGRIDClass__AlphaContract_idOriginated_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKey__Bls = { bls12_381_public_key: FixedBytes<48> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Bls = { bls12_381_public_key_hash: FixedBytes<20> };
export type MichelineAlphaMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineAlphaMichelson_v1Expression>,width.Uint30>;
export type MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives };
export type MichelineAlphaMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__AlphaMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineAlphaMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression };
export type MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression };
export type MichelineAlphaMichelsonV1Expression__Int = { int: Z };
export type MichelineAlphaMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
export type AlphaOperationAlphaContents__Zk_rollup_update = { source: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_source, fee: N, counter: N, gas_limit: N, storage_limit: N, zk_rollup: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_zk_rollup, update: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update };
export type AlphaOperationAlphaContents__Zk_rollup_publish = { source: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_source, fee: N, counter: N, gas_limit: N, storage_limit: N, zk_rollup: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_zk_rollup, op: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq>,width.Uint30> };
export type AlphaOperationAlphaContents__Zk_rollup_origination = { source: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_parameters: Dynamic<Bytes,width.Uint30>, circuits_info: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq>,width.Uint30>, init_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30>, nb_ops: Int31 };
export type AlphaOperationAlphaContents__Vdf_revelation = { solution: CGRIDClass__AlphaOperationAlphaContents_Vdf_revelation_solution };
export type AlphaOperationAlphaContents__Update_consensus_key = { source: CGRIDClass__AlphaOperationAlphaContents_Update_consensus_key_source, fee: N, counter: N, gas_limit: N, storage_limit: N, pk: CGRIDClass__AlphaOperationAlphaContents_Update_consensus_key_pk };
export type AlphaOperationAlphaContents__Tx_rollup_submit_batch = { source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_submit_batch_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id, content: Dynamic<U8String,width.Uint30>, burn_limit: Option<N> };
export type AlphaOperationAlphaContents__Tx_rollup_return_bond = { source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_return_bond_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id };
export type AlphaOperationAlphaContents__Tx_rollup_remove_commitment = { source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_remove_commitment_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id };
export type AlphaOperationAlphaContents__Tx_rollup_rejection = { source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id, level: Int32, message: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message, message_position: N, message_path: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq>,width.Uint30>, message_result_hash: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_result_hash, message_result_path: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq>,width.Uint30>, previous_message_result: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result, previous_message_result_path: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq>,width.Uint30>, proof: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContents__Tx_rollup_origination = { source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, tx_rollup_origination: Unit };
export type AlphaOperationAlphaContents__Tx_rollup_finalize_commitment = { source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_finalize_commitment_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id };
export type AlphaOperationAlphaContents__Tx_rollup_dispatch_tickets = { source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_source, fee: N, counter: N, gas_limit: N, storage_limit: N, tx_rollup: CGRIDClass__AlphaTx_rollup_id, level: Int32, context_hash: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash, message_index: Int31, message_result_path: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq>,width.Uint30>, tickets_info: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq>,width.Uint30> };
export type AlphaOperationAlphaContents__Tx_rollup_commit = { source: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaTx_rollup_id, commitment: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment };
export type AlphaOperationAlphaContents__Transfer_ticket = { source: CGRIDClass__AlphaOperationAlphaContents_Transfer_ticket_source, fee: N, counter: N, gas_limit: N, storage_limit: N, ticket_contents: Dynamic<Bytes,width.Uint30>, ticket_ty: Dynamic<Bytes,width.Uint30>, ticket_ticketer: CGRIDClass__AlphaContract_id, ticket_amount: N, destination: CGRIDClass__AlphaContract_id, entrypoint: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContents__Transaction = { source: CGRIDClass__AlphaOperationAlphaContents_Transaction_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: N, destination: CGRIDClass__AlphaContract_id, parameters: Option<CGRIDClass__AlphaOperationAlphaContents_Transaction_parameters> };
export type AlphaOperationAlphaContents__Smart_rollup_timeout = { source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaSmart_rollup_address, stakers: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers };
export type AlphaOperationAlphaContents__Smart_rollup_refute = { source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaSmart_rollup_address, opponent: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_opponent, refutation: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation };
export type AlphaOperationAlphaContents__Smart_rollup_recover_bond = { source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_rollup, staker: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_staker };
export type AlphaOperationAlphaContents__Smart_rollup_publish = { source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaSmart_rollup_address, commitment: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment };
export type AlphaOperationAlphaContents__Smart_rollup_originate = { source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_originate_source, fee: N, counter: N, gas_limit: N, storage_limit: N, pvm_kind: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_originate_pvm_kind, kernel: Dynamic<U8String,width.Uint30>, origination_proof: Dynamic<U8String,width.Uint30>, parameters_ty: Dynamic<Bytes,width.Uint30> };
export type AlphaOperationAlphaContents__Smart_rollup_execute_outbox_message = { source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_execute_outbox_message_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaSmart_rollup_address, cemented_commitment: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_execute_outbox_message_cemented_commitment, output_proof: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContents__Smart_rollup_cement = { source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_cement_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__AlphaSmart_rollup_address, commitment: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_cement_commitment };
export type AlphaOperationAlphaContents__Smart_rollup_add_messages = { source: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_add_messages_source, fee: N, counter: N, gas_limit: N, storage_limit: N, message: Dynamic<Sequence<Dynamic<U8String,width.Uint30>>,width.Uint30> };
export type AlphaOperationAlphaContents__Set_deposits_limit = { source: CGRIDClass__AlphaOperationAlphaContents_Set_deposits_limit_source, fee: N, counter: N, gas_limit: N, storage_limit: N, limit: Option<N> };
export type AlphaOperationAlphaContents__Seed_nonce_revelation = { level: Int32, nonce: FixedBytes<32> };
export type AlphaOperationAlphaContents__Reveal = { source: CGRIDClass__AlphaOperationAlphaContents_Reveal_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_key: CGRIDClass__AlphaOperationAlphaContents_Reveal_public_key };
export type AlphaOperationAlphaContents__Register_global_constant = { source: CGRIDClass__AlphaOperationAlphaContents_Register_global_constant_source, fee: N, counter: N, gas_limit: N, storage_limit: N, value: Dynamic<Bytes,width.Uint30> };
export type AlphaOperationAlphaContents__Proposals = { source: CGRIDClass__AlphaOperationAlphaContents_Proposals_source, period: Int32, proposals: Dynamic<SequenceBounded<CGRIDClass__AlphaOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq,20>,width.Uint30> };
export type AlphaOperationAlphaContents__Preendorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__AlphaOperationAlphaContents_Preendorsement_block_payload_hash };
export type AlphaOperationAlphaContents__Origination = { source: CGRIDClass__AlphaOperationAlphaContents_Origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, balance: N, delegate: Option<CGRIDClass__AlphaOperationAlphaContents_Origination_delegate>, script: CGRIDClass__AlphaScriptedContracts };
export type AlphaOperationAlphaContents__Increase_paid_storage = { source: CGRIDClass__AlphaOperationAlphaContents_Increase_paid_storage_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: Z, destination: CGRIDClass__AlphaContract_idOriginated };
export type AlphaOperationAlphaContents__Failing_noop = { arbitrary: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContents__Endorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__AlphaOperationAlphaContents_Endorsement_block_payload_hash };
export type AlphaOperationAlphaContents__Drain_delegate = { consensus_key: CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_consensus_key, delegate: CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_delegate, destination: CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_destination };
export type AlphaOperationAlphaContents__Double_preendorsement_evidence = { op1: Dynamic<CGRIDClass__AlphaInlinedPreendorsement,width.Uint30>, op2: Dynamic<CGRIDClass__AlphaInlinedPreendorsement,width.Uint30> };
export type AlphaOperationAlphaContents__Double_endorsement_evidence = { op1: Dynamic<CGRIDClass__AlphaInlinedEndorsement,width.Uint30>, op2: Dynamic<CGRIDClass__AlphaInlinedEndorsement,width.Uint30> };
export type AlphaOperationAlphaContents__Double_baking_evidence = { bh1: Dynamic<CGRIDClass__AlphaBlock_headerAlphaFull_header,width.Uint30>, bh2: Dynamic<CGRIDClass__AlphaBlock_headerAlphaFull_header,width.Uint30> };
export type AlphaOperationAlphaContents__Delegation = { source: CGRIDClass__AlphaOperationAlphaContents_Delegation_source, fee: N, counter: N, gas_limit: N, storage_limit: N, delegate: Option<CGRIDClass__AlphaOperationAlphaContents_Delegation_delegate> };
export type AlphaOperationAlphaContents__Dal_publish_slot_header = { source: CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_source, fee: N, counter: N, gas_limit: N, storage_limit: N, slot_header: CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_slot_header };
export type AlphaOperationAlphaContents__Dal_attestation = { attestor: CGRIDClass__AlphaOperationAlphaContents_Dal_attestation_attestor, attestation: Z, level: Int32 };
export type AlphaOperationAlphaContents__Ballot = { source: CGRIDClass__AlphaOperationAlphaContents_Ballot_source, period: Int32, proposal: CGRIDClass__AlphaOperationAlphaContents_Ballot_proposal, ballot: Int8 };
export type AlphaOperationAlphaContents__Activate_account = { pkh: CGRIDClass__AlphaOperationAlphaContents_Activate_account_pkh, secret: FixedBytes<20> };
export type AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some = { contents: CGRIDClass__MichelineAlphaMichelson_v1Expression, ty: CGRIDClass__MichelineAlphaMichelson_v1Expression, ticketer: CGRIDClass__AlphaContract_id };
export type AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None = Unit;
export type AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public = { _public: Unit };
export type AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private = { _private: Unit };
export type AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee = { fee: Unit };
export type AlphaOperationAlphaContentsTxRollupRejectionMessage__Deposit = { deposit: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit };
export type AlphaOperationAlphaContentsTxRollupRejectionMessage__Batch = { batch: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 = Int64;
export type AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 = Int32;
export type AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 = Uint16;
export type AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 = Uint8;
export type AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 = Int64;
export type AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 = Int32;
export type AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 = Uint16;
export type AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 = Uint8;
export type AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some = { commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None = Unit;
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Start = { refutation_kind: Unit, player_commitment_hash: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Start_player_commitment_hash, opponent_commitment_hash: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Start_opponent_commitment_hash };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Move = { refutation_kind: Unit, choice: N, step: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof = { pvm_step: Dynamic<U8String,width.Uint30>, input_proof: Option<CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof> };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection = Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq>,width.Uint30>;
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof = { input_proof_kind: Unit, reveal_proof: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof = { input_proof_kind: Unit, level: Int32, message_counter: N, serialized_proof: Dynamic<U8String,width.Uint30> };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input = { input_proof_kind: Unit };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof = { reveal_proof_kind: Unit, raw_data: Dynamic<U8String,width.Uint16> };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof = { reveal_proof_kind: Unit };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof = { reveal_proof_kind: Unit, dal_page_id: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id, dal_proof: Dynamic<Bytes,width.Uint30> };
export type AlphaInlinedPreendorsementContents__Preendorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__AlphaInlinedPreendorsementContents_Preendorsement_block_payload_hash };
export type AlphaInlinedEndorsementMempoolContents__Endorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__AlphaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash };
export type AlphaEntrypoint__set_delegate = Unit;
export type AlphaEntrypoint__root = Unit;
export type AlphaEntrypoint__remove_delegate = Unit;
export type AlphaEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type AlphaEntrypoint__deposit = Unit;
export type AlphaEntrypoint___do = Unit;
export type AlphaEntrypoint___default = Unit;
export type AlphaContractId__Originated = Padded<CGRIDClass__AlphaContract_id_Originated_denest_pad,1>;
export type AlphaContractId__Implicit = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaContractIdOriginated__Originated = Padded<CGRIDClass__AlphaContract_idOriginated_Originated_denest_pad,1>;
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
            case CGRIDTag__PublicKeyHash.Bls: return CGRIDClass__PublicKeyHash__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
            case CGRIDTag__PublicKey.Bls: return CGRIDClass__PublicKey__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelson_v1Expression generated for MichelineAlphaMichelsonV1Expression
export function michelinealphamichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineAlphaMichelsonV1Expression,MichelineAlphaMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineAlphaMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Int: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.String: return CGRIDClass__MichelineAlphaMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Sequence: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Bytes: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineAlphaMichelsonV1Expression => Object.values(CGRIDTag__MichelineAlphaMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineAlphaMichelson_v1Expression extends Box<MichelineAlphaMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineAlphaMichelsonV1Expression>, MichelineAlphaMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelinealphamichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_predecessor generated for BlockHeaderShellPredecessor
export class CGRIDClass__Block_headerShell_predecessor extends Box<BlockHeaderShellPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_predecessor {
        return new this(record_decoder<BlockHeaderShellPredecessor>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_operations_hash generated for BlockHeaderShellOperationsHash
export class CGRIDClass__Block_headerShell_operations_hash extends Box<BlockHeaderShellOperationsHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_list_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_operations_hash {
        return new this(record_decoder<BlockHeaderShellOperationsHash>({operation_list_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_list_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_list_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_list_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_context generated for BlockHeaderShellContext
export class CGRIDClass__Block_headerShell_context extends Box<BlockHeaderShellContext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_context {
        return new this(record_decoder<BlockHeaderShellContext>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaTx_rollup_id generated for AlphaTxRollupId
export class CGRIDClass__AlphaTx_rollup_id extends Box<AlphaTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaTx_rollup_id {
        return new this(record_decoder<AlphaTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaSmart_rollup_address generated for AlphaSmartRollupAddress
export class CGRIDClass__AlphaSmart_rollup_address extends Box<AlphaSmartRollupAddress> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaSmart_rollup_address {
        return new this(record_decoder<AlphaSmartRollupAddress>({smart_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['smart_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaScriptedContracts generated for AlphaScriptedContracts
export class CGRIDClass__AlphaScriptedContracts extends Box<AlphaScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaScriptedContracts {
        return new this(record_decoder<AlphaScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_zk_rollup generated for AlphaOperationAlphaContentsZkRollupUpdateZkRollup
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_zk_rollup extends Box<AlphaOperationAlphaContentsZkRollupUpdateZkRollup> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['zk_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_zk_rollup {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupUpdateZkRollup>({zk_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['zk_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.zk_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.zk_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1 generated for AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1 extends Box<AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['new_state', 'fee']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1 {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1>({new_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30), fee: FixedBytes.decode<32>({len: 32})}, {order: ['new_state', 'fee']})(p));
    };
    get encodeLength(): number {
        return (this.value.new_state.encodeLength +  this.value.fee.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.new_state.writeTarget(tgt) +  this.value.fee.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq {
        return new this(tuple_decoder<AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq>(Dynamic.decode(U8String.decode, width.Uint30), CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1 generated for AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1 extends Box<AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['new_state', 'fee', 'exit_validity']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1 {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1>({new_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30), fee: FixedBytes.decode<32>({len: 32}), exit_validity: Bool.decode}, {order: ['new_state', 'fee', 'exit_validity']})(p));
    };
    get encodeLength(): number {
        return (this.value.new_state.encodeLength +  this.value.fee.encodeLength +  this.value.exit_validity.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.new_state.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.exit_validity.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq {
        return new this(tuple_decoder<AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq>(Dynamic.decode(U8String.decode, width.Uint30), CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_fee_pi generated for AlphaOperationAlphaContentsZkRollupUpdateUpdateFeePi
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_fee_pi extends Box<AlphaOperationAlphaContentsZkRollupUpdateUpdateFeePi> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['new_state']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_fee_pi {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupUpdateUpdateFeePi>({new_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['new_state']})(p));
    };
    get encodeLength(): number {
        return (this.value.new_state.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.new_state.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update generated for AlphaOperationAlphaContentsZkRollupUpdateUpdate
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update extends Box<AlphaOperationAlphaContentsZkRollupUpdateUpdate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pending_pis', 'private_pis', 'fee_pi', 'proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupUpdateUpdate>({pending_pis: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq.decode), width.Uint30), private_pis: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq.decode), width.Uint30), fee_pi: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_fee_pi.decode, proof: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['pending_pis', 'private_pis', 'fee_pi', 'proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.pending_pis.encodeLength +  this.value.private_pis.encodeLength +  this.value.fee_pi.encodeLength +  this.value.proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pending_pis.writeTarget(tgt) +  this.value.private_pis.writeTarget(tgt) +  this.value.fee_pi.writeTarget(tgt) +  this.value.proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_source generated for AlphaOperationAlphaContentsZkRollupUpdateSource
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_source extends Box<AlphaOperationAlphaContentsZkRollupUpdateSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_source {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupUpdateSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_zk_rollup generated for AlphaOperationAlphaContentsZkRollupPublishZkRollup
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_zk_rollup extends Box<AlphaOperationAlphaContentsZkRollupPublishZkRollup> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['zk_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_zk_rollup {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupPublishZkRollup>({zk_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['zk_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.zk_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.zk_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_source generated for AlphaOperationAlphaContentsZkRollupPublishSource
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_source extends Box<AlphaOperationAlphaContentsZkRollupPublishSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_source {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupPublishSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index1 generated for AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1
export function alphaoperationalphacontentszkrolluppublishopdenestdyndenestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1,AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1.None: return CGRIDClass__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None.decode;
            case CGRIDTag__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1.Some: return CGRIDClass__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1 => Object.values(CGRIDTag__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index1 extends Box<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1>, AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentszkrolluppublishopdenestdyndenestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id generated for AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id extends Box<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['zk_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId>({zk_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['zk_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.zk_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.zk_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id generated for AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id extends Box<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price generated for AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price extends Box<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['id', 'amount']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price>({id: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id.decode, amount: Z.decode}, {order: ['id', 'amount']})(p));
    };
    get encodeLength(): number {
        return (this.value.id.encodeLength +  this.value.amount.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.id.writeTarget(tgt) +  this.value.amount.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst generated for AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst extends Box<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0 generated for AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0 extends Box<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op_code', 'price', 'l1_dst', 'rollup_id', 'payload']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0 {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0>({op_code: Int31.decode, price: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price.decode, l1_dst: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst.decode, rollup_id: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id.decode, payload: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['op_code', 'price', 'l1_dst', 'rollup_id', 'payload']})(p));
    };
    get encodeLength(): number {
        return (this.value.op_code.encodeLength +  this.value.price.encodeLength +  this.value.l1_dst.encodeLength +  this.value.rollup_id.encodeLength +  this.value.payload.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op_code.writeTarget(tgt) +  this.value.price.writeTarget(tgt) +  this.value.l1_dst.writeTarget(tgt) +  this.value.rollup_id.writeTarget(tgt) +  this.value.payload.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq {
        return new this(tuple_decoder<AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq>(CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0.decode, CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_source generated for AlphaOperationAlphaContentsZkRollupOriginationSource
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_source extends Box<AlphaOperationAlphaContentsZkRollupOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_source {
        return new this(record_decoder<AlphaOperationAlphaContentsZkRollupOriginationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1 generated for AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1
export function alphaoperationalphacontentszkrolluporiginationcircuitsinfodenestdyndenestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1,AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Public: return CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public.decode;
            case CGRIDTag__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Private: return CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private.decode;
            case CGRIDTag__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Fee: return CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1 => Object.values(CGRIDTag__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1 extends Box<AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1>, AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentszkrolluporiginationcircuitsinfodenestdyndenestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq {
        return new this(tuple_decoder<AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq>(Dynamic.decode(U8String.decode, width.Uint30), CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Vdf_revelation_solution generated for AlphaOperationAlphaContentsVdfRevelationSolution
export class CGRIDClass__AlphaOperationAlphaContents_Vdf_revelation_solution extends Box<AlphaOperationAlphaContentsVdfRevelationSolution> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Vdf_revelation_solution {
        return new this(tuple_decoder<AlphaOperationAlphaContentsVdfRevelationSolution>(FixedBytes.decode<100>({len: 100}), FixedBytes.decode<100>({len: 100}))(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Update_consensus_key_source generated for AlphaOperationAlphaContentsUpdateConsensusKeySource
export class CGRIDClass__AlphaOperationAlphaContents_Update_consensus_key_source extends Box<AlphaOperationAlphaContentsUpdateConsensusKeySource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Update_consensus_key_source {
        return new this(record_decoder<AlphaOperationAlphaContentsUpdateConsensusKeySource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Update_consensus_key_pk generated for AlphaOperationAlphaContentsUpdateConsensusKeyPk
export class CGRIDClass__AlphaOperationAlphaContents_Update_consensus_key_pk extends Box<AlphaOperationAlphaContentsUpdateConsensusKeyPk> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Update_consensus_key_pk {
        return new this(record_decoder<AlphaOperationAlphaContentsUpdateConsensusKeyPk>({signature_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_submit_batch_source generated for AlphaOperationAlphaContentsTxRollupSubmitBatchSource
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_submit_batch_source extends Box<AlphaOperationAlphaContentsTxRollupSubmitBatchSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_submit_batch_source {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupSubmitBatchSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_return_bond_source generated for AlphaOperationAlphaContentsTxRollupReturnBondSource
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_return_bond_source extends Box<AlphaOperationAlphaContentsTxRollupReturnBondSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_return_bond_source {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupReturnBondSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_remove_commitment_source generated for AlphaOperationAlphaContentsTxRollupRemoveCommitmentSource
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_remove_commitment_source extends Box<AlphaOperationAlphaContentsTxRollupRemoveCommitmentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_remove_commitment_source {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRemoveCommitmentSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_source generated for AlphaOperationAlphaContentsTxRollupRejectionSource
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_source extends Box<AlphaOperationAlphaContentsTxRollupRejectionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_source {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash generated for AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash extends Box<AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['withdraw_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash>({withdraw_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['withdraw_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.withdraw_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.withdraw_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash generated for AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash extends Box<AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result generated for AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResult
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result extends Box<AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResult> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash', 'withdraw_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResult>({context_hash: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash.decode, withdraw_list_hash: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash.decode}, {order: ['context_hash', 'withdraw_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength +  this.value.withdraw_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt) +  this.value.withdraw_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_result_hash generated for AlphaOperationAlphaContentsTxRollupRejectionMessageResultHash
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_result_hash extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessageResultHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_result_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionMessageResultHash>({message_result_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['inbox_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq>({inbox_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['inbox_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.inbox_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.inbox_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash generated for AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender generated for AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination generated for AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount generated for AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount
export function alphaoperationalphacontentstxrolluprejectionmessagedepositdepositamount_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount,AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_0: return CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0.decode;
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_1: return CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1.decode;
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_2: return CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2.decode;
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_3: return CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount => Object.values(CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount>, AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentstxrolluprejectionmessagedepositdepositamount_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit generated for AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDeposit
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDeposit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['sender', 'destination', 'ticket_hash', 'amount']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDeposit>({sender: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender.decode, destination: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination.decode, ticket_hash: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash.decode, amount: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount.decode}, {order: ['sender', 'destination', 'ticket_hash', 'amount']})(p));
    };
    get encodeLength(): number {
        return (this.value.sender.encodeLength +  this.value.destination.encodeLength +  this.value.ticket_hash.encodeLength +  this.value.amount.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.sender.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.ticket_hash.writeTarget(tgt) +  this.value.amount.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message generated for AlphaOperationAlphaContentsTxRollupRejectionMessage
export function alphaoperationalphacontentstxrolluprejectionmessage_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessage,AlphaOperationAlphaContentsTxRollupRejectionMessage> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessage) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessage.Batch: return CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessage__Batch.decode;
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessage.Deposit: return CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessage__Deposit.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessage => Object.values(CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessage).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message extends Box<AlphaOperationAlphaContentsTxRollupRejectionMessage> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsTxRollupRejectionMessage>, AlphaOperationAlphaContentsTxRollupRejectionMessage>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentstxrolluprejectionmessage_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_origination_source generated for AlphaOperationAlphaContentsTxRollupOriginationSource
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_origination_source extends Box<AlphaOperationAlphaContentsTxRollupOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_origination_source {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupOriginationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_finalize_commitment_source generated for AlphaOperationAlphaContentsTxRollupFinalizeCommitmentSource
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_finalize_commitment_source extends Box<AlphaOperationAlphaContentsTxRollupFinalizeCommitmentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_finalize_commitment_source {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupFinalizeCommitmentSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer generated for AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer extends Box<AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount generated for AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount
export function alphaoperationalphacontentstxrollupdispatchticketsticketsinfodenestdyndenestseqamount_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_0: return CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0.decode;
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_1: return CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1.decode;
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_2: return CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2.decode;
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_3: return CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount => Object.values(CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount extends Box<AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount>, AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentstxrollupdispatchticketsticketsinfodenestdyndenestseqamount_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contents', 'ty', 'ticketer', 'amount', 'claimer']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>({contents: Dynamic.decode(Bytes.decode, width.Uint30), ty: Dynamic.decode(Bytes.decode, width.Uint30), ticketer: CGRIDClass__AlphaContract_id.decode, amount: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount.decode, claimer: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer.decode}, {order: ['contents', 'ty', 'ticketer', 'amount', 'claimer']})(p));
    };
    get encodeLength(): number {
        return (this.value.contents.encodeLength +  this.value.ty.encodeLength +  this.value.ticketer.encodeLength +  this.value.amount.encodeLength +  this.value.claimer.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contents.writeTarget(tgt) +  this.value.ty.writeTarget(tgt) +  this.value.ticketer.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.claimer.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_source generated for AlphaOperationAlphaContentsTxRollupDispatchTicketsSource
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_source extends Box<AlphaOperationAlphaContentsTxRollupDispatchTicketsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_source {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupDispatchTicketsSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash generated for AlphaOperationAlphaContentsTxRollupDispatchTicketsContextHash
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash extends Box<AlphaOperationAlphaContentsTxRollupDispatchTicketsContextHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupDispatchTicketsContextHash>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_source generated for AlphaOperationAlphaContentsTxRollupCommitSource
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_source extends Box<AlphaOperationAlphaContentsTxRollupCommitSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_source {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupCommitSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_predecessor generated for AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor
export function alphaoperationalphacontentstxrollupcommitcommitmentpredecessor_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor,AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor.None: return CGRIDClass__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None.decode;
            case CGRIDTag__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor.Some: return CGRIDClass__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor => Object.values(CGRIDTag__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_predecessor extends Box<AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor>, AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_predecessor {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentstxrollupcommitcommitmentpredecessor_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq>({message_result_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root generated for AlphaOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root extends Box<AlphaOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['inbox_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot>({inbox_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['inbox_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.inbox_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.inbox_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment generated for AlphaOperationAlphaContentsTxRollupCommitCommitment
export class CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment extends Box<AlphaOperationAlphaContentsTxRollupCommitCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'messages', 'predecessor', 'inbox_merkle_root']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsTxRollupCommitCommitment>({level: Int32.decode, messages: Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq.decode), width.Uint30), predecessor: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_predecessor.decode, inbox_merkle_root: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root.decode}, {order: ['level', 'messages', 'predecessor', 'inbox_merkle_root']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.messages.encodeLength +  this.value.predecessor.encodeLength +  this.value.inbox_merkle_root.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.messages.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.inbox_merkle_root.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Transfer_ticket_source generated for AlphaOperationAlphaContentsTransferTicketSource
export class CGRIDClass__AlphaOperationAlphaContents_Transfer_ticket_source extends Box<AlphaOperationAlphaContentsTransferTicketSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Transfer_ticket_source {
        return new this(record_decoder<AlphaOperationAlphaContentsTransferTicketSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Transaction_source generated for AlphaOperationAlphaContentsTransactionSource
export class CGRIDClass__AlphaOperationAlphaContents_Transaction_source extends Box<AlphaOperationAlphaContentsTransactionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Transaction_source {
        return new this(record_decoder<AlphaOperationAlphaContentsTransactionSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Transaction_parameters generated for AlphaOperationAlphaContentsTransactionParameters
export class CGRIDClass__AlphaOperationAlphaContents_Transaction_parameters extends Box<AlphaOperationAlphaContentsTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Transaction_parameters {
        return new this(record_decoder<AlphaOperationAlphaContentsTransactionParameters>({entrypoint: CGRIDClass__AlphaEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers_bob generated for AlphaOperationAlphaContentsSmartRollupTimeoutStakersBob
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers_bob extends Box<AlphaOperationAlphaContentsSmartRollupTimeoutStakersBob> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers_bob {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupTimeoutStakersBob>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers_alice generated for AlphaOperationAlphaContentsSmartRollupTimeoutStakersAlice
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers_alice extends Box<AlphaOperationAlphaContentsSmartRollupTimeoutStakersAlice> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers_alice {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupTimeoutStakersAlice>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers generated for AlphaOperationAlphaContentsSmartRollupTimeoutStakers
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers extends Box<AlphaOperationAlphaContentsSmartRollupTimeoutStakers> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['alice', 'bob']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupTimeoutStakers>({alice: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers_alice.decode, bob: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers_bob.decode}, {order: ['alice', 'bob']})(p));
    };
    get encodeLength(): number {
        return (this.value.alice.encodeLength +  this.value.bob.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.alice.writeTarget(tgt) +  this.value.bob.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_source generated for AlphaOperationAlphaContentsSmartRollupTimeoutSource
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_source extends Box<AlphaOperationAlphaContentsSmartRollupTimeoutSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_source {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupTimeoutSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_source generated for AlphaOperationAlphaContentsSmartRollupRefuteSource
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_source extends Box<AlphaOperationAlphaContentsSmartRollupRefuteSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_source {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Start_player_commitment_hash generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Start_player_commitment_hash extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Start_player_commitment_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Start_opponent_commitment_hash generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Start_opponent_commitment_hash extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Start_opponent_commitment_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['published_level', 'slot_index', 'page_index']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId>({published_level: Int32.decode, slot_index: Uint8.decode, page_index: Int16.decode}, {order: ['published_level', 'slot_index', 'page_index']})(p));
    };
    get encodeLength(): number {
        return (this.value.published_level.encodeLength +  this.value.slot_index.encodeLength +  this.value.page_index.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.published_level.writeTarget(tgt) +  this.value.slot_index.writeTarget(tgt) +  this.value.page_index.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof
export function alphaoperationalphacontentssmartrolluprefuterefutationmovestepproofinputproofrevealproofrevealproof_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof,AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.raw_data_proof: return CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof.decode;
            case CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.metadata_proof: return CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof.decode;
            case CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.dal_page_proof: return CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof => Object.values(CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof>, AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentssmartrolluprefuterefutationmovestepproofinputproofrevealproofrevealproof_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof
export function alphaoperationalphacontentssmartrolluprefuterefutationmovestepproofinputproof_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof,AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.inbox_proof: return CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof.decode;
            case CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.reveal_proof: return CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof.decode;
            case CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.first_input: return CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof => Object.values(CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof>, AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentssmartrolluprefuterefutationmovestepproofinputproof_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_state_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState>({smart_rollup_state_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_state_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_state_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_state_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['state', 'tick']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq>({state: Option.decode(CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state.decode), tick: N.decode}, {order: ['state', 'tick']})(p));
    };
    get encodeLength(): number {
        return (this.value.state.encodeLength +  this.value.tick.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.state.writeTarget(tgt) +  this.value.tick.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep
export function alphaoperationalphacontentssmartrolluprefuterefutationmovestep_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep,AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep.Dissection: return CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection.decode;
            case CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep.Proof: return CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep => Object.values(CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep>, AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentssmartrolluprefuterefutationmovestep_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation generated for AlphaOperationAlphaContentsSmartRollupRefuteRefutation
export function alphaoperationalphacontentssmartrolluprefuterefutation_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutation,AlphaOperationAlphaContentsSmartRollupRefuteRefutation> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutation) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutation.Start: return CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Start.decode;
            case CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutation.Move: return CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Move.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutation => Object.values(CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutation).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation extends Box<AlphaOperationAlphaContentsSmartRollupRefuteRefutation> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContentsSmartRollupRefuteRefutation>, AlphaOperationAlphaContentsSmartRollupRefuteRefutation>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontentssmartrolluprefuterefutation_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_opponent generated for AlphaOperationAlphaContentsSmartRollupRefuteOpponent
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_opponent extends Box<AlphaOperationAlphaContentsSmartRollupRefuteOpponent> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_opponent {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRefuteOpponent>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_staker generated for AlphaOperationAlphaContentsSmartRollupRecoverBondStaker
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_staker extends Box<AlphaOperationAlphaContentsSmartRollupRecoverBondStaker> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_staker {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRecoverBondStaker>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_source generated for AlphaOperationAlphaContentsSmartRollupRecoverBondSource
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_source extends Box<AlphaOperationAlphaContentsSmartRollupRecoverBondSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_source {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRecoverBondSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_rollup generated for AlphaOperationAlphaContentsSmartRollupRecoverBondRollup
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_rollup extends Box<AlphaOperationAlphaContentsSmartRollupRecoverBondRollup> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_recover_bond_rollup {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupRecoverBondRollup>({smart_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['smart_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_source generated for AlphaOperationAlphaContentsSmartRollupPublishSource
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_source extends Box<AlphaOperationAlphaContentsSmartRollupPublishSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_source {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupPublishSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment_predecessor generated for AlphaOperationAlphaContentsSmartRollupPublishCommitmentPredecessor
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment_predecessor extends Box<AlphaOperationAlphaContentsSmartRollupPublishCommitmentPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment_predecessor {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupPublishCommitmentPredecessor>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment_compressed_state generated for AlphaOperationAlphaContentsSmartRollupPublishCommitmentCompressedState
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment_compressed_state extends Box<AlphaOperationAlphaContentsSmartRollupPublishCommitmentCompressedState> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_state_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment_compressed_state {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupPublishCommitmentCompressedState>({smart_rollup_state_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_state_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_state_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_state_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment generated for AlphaOperationAlphaContentsSmartRollupPublishCommitment
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment extends Box<AlphaOperationAlphaContentsSmartRollupPublishCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['compressed_state', 'inbox_level', 'predecessor', 'number_of_ticks']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupPublishCommitment>({compressed_state: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment_compressed_state.decode, inbox_level: Int32.decode, predecessor: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment_predecessor.decode, number_of_ticks: Int64.decode}, {order: ['compressed_state', 'inbox_level', 'predecessor', 'number_of_ticks']})(p));
    };
    get encodeLength(): number {
        return (this.value.compressed_state.encodeLength +  this.value.inbox_level.encodeLength +  this.value.predecessor.encodeLength +  this.value.number_of_ticks.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.compressed_state.writeTarget(tgt) +  this.value.inbox_level.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.number_of_ticks.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_originate_source generated for AlphaOperationAlphaContentsSmartRollupOriginateSource
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_originate_source extends Box<AlphaOperationAlphaContentsSmartRollupOriginateSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_originate_source {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupOriginateSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_originate_pvm_kind generated for AlphaOperationAlphaContentsSmartRollupOriginatePvmKind
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_originate_pvm_kind extends Box<AlphaOperationAlphaContentsSmartRollupOriginatePvmKind> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<AlphaOperationAlphaContentsSmartRollupOriginatePvmKind>(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_originate_pvm_kind {
        return new this(enum_decoder(width.Uint8)((x): x is AlphaOperationAlphaContentsSmartRollupOriginatePvmKind => (Object.values(AlphaOperationAlphaContentsSmartRollupOriginatePvmKind).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_execute_outbox_message_source generated for AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageSource
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_execute_outbox_message_source extends Box<AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_execute_outbox_message_source {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_execute_outbox_message_cemented_commitment generated for AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_execute_outbox_message_cemented_commitment extends Box<AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_execute_outbox_message_cemented_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_cement_source generated for AlphaOperationAlphaContentsSmartRollupCementSource
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_cement_source extends Box<AlphaOperationAlphaContentsSmartRollupCementSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_cement_source {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupCementSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_cement_commitment generated for AlphaOperationAlphaContentsSmartRollupCementCommitment
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_cement_commitment extends Box<AlphaOperationAlphaContentsSmartRollupCementCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_cement_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupCementCommitment>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_add_messages_source generated for AlphaOperationAlphaContentsSmartRollupAddMessagesSource
export class CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_add_messages_source extends Box<AlphaOperationAlphaContentsSmartRollupAddMessagesSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_add_messages_source {
        return new this(record_decoder<AlphaOperationAlphaContentsSmartRollupAddMessagesSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Set_deposits_limit_source generated for AlphaOperationAlphaContentsSetDepositsLimitSource
export class CGRIDClass__AlphaOperationAlphaContents_Set_deposits_limit_source extends Box<AlphaOperationAlphaContentsSetDepositsLimitSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Set_deposits_limit_source {
        return new this(record_decoder<AlphaOperationAlphaContentsSetDepositsLimitSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Reveal_source generated for AlphaOperationAlphaContentsRevealSource
export class CGRIDClass__AlphaOperationAlphaContents_Reveal_source extends Box<AlphaOperationAlphaContentsRevealSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Reveal_source {
        return new this(record_decoder<AlphaOperationAlphaContentsRevealSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Reveal_public_key generated for AlphaOperationAlphaContentsRevealPublicKey
export class CGRIDClass__AlphaOperationAlphaContents_Reveal_public_key extends Box<AlphaOperationAlphaContentsRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Reveal_public_key {
        return new this(record_decoder<AlphaOperationAlphaContentsRevealPublicKey>({signature_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Register_global_constant_source generated for AlphaOperationAlphaContentsRegisterGlobalConstantSource
export class CGRIDClass__AlphaOperationAlphaContents_Register_global_constant_source extends Box<AlphaOperationAlphaContentsRegisterGlobalConstantSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Register_global_constant_source {
        return new this(record_decoder<AlphaOperationAlphaContentsRegisterGlobalConstantSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Proposals_source generated for AlphaOperationAlphaContentsProposalsSource
export class CGRIDClass__AlphaOperationAlphaContents_Proposals_source extends Box<AlphaOperationAlphaContentsProposalsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Proposals_source {
        return new this(record_decoder<AlphaOperationAlphaContentsProposalsSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq generated for AlphaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq
export class CGRIDClass__AlphaOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq extends Box<AlphaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq {
        return new this(record_decoder<AlphaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Preendorsement_block_payload_hash generated for AlphaOperationAlphaContentsPreendorsementBlockPayloadHash
export class CGRIDClass__AlphaOperationAlphaContents_Preendorsement_block_payload_hash extends Box<AlphaOperationAlphaContentsPreendorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Preendorsement_block_payload_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsPreendorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Origination_source generated for AlphaOperationAlphaContentsOriginationSource
export class CGRIDClass__AlphaOperationAlphaContents_Origination_source extends Box<AlphaOperationAlphaContentsOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Origination_source {
        return new this(record_decoder<AlphaOperationAlphaContentsOriginationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Origination_delegate generated for AlphaOperationAlphaContentsOriginationDelegate
export class CGRIDClass__AlphaOperationAlphaContents_Origination_delegate extends Box<AlphaOperationAlphaContentsOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Origination_delegate {
        return new this(record_decoder<AlphaOperationAlphaContentsOriginationDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Increase_paid_storage_source generated for AlphaOperationAlphaContentsIncreasePaidStorageSource
export class CGRIDClass__AlphaOperationAlphaContents_Increase_paid_storage_source extends Box<AlphaOperationAlphaContentsIncreasePaidStorageSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Increase_paid_storage_source {
        return new this(record_decoder<AlphaOperationAlphaContentsIncreasePaidStorageSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Endorsement_block_payload_hash generated for AlphaOperationAlphaContentsEndorsementBlockPayloadHash
export class CGRIDClass__AlphaOperationAlphaContents_Endorsement_block_payload_hash extends Box<AlphaOperationAlphaContentsEndorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Endorsement_block_payload_hash {
        return new this(record_decoder<AlphaOperationAlphaContentsEndorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_destination generated for AlphaOperationAlphaContentsDrainDelegateDestination
export class CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_destination extends Box<AlphaOperationAlphaContentsDrainDelegateDestination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_destination {
        return new this(record_decoder<AlphaOperationAlphaContentsDrainDelegateDestination>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_delegate generated for AlphaOperationAlphaContentsDrainDelegateDelegate
export class CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_delegate extends Box<AlphaOperationAlphaContentsDrainDelegateDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_delegate {
        return new this(record_decoder<AlphaOperationAlphaContentsDrainDelegateDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_consensus_key generated for AlphaOperationAlphaContentsDrainDelegateConsensusKey
export class CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_consensus_key extends Box<AlphaOperationAlphaContentsDrainDelegateConsensusKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Drain_delegate_consensus_key {
        return new this(record_decoder<AlphaOperationAlphaContentsDrainDelegateConsensusKey>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Delegation_source generated for AlphaOperationAlphaContentsDelegationSource
export class CGRIDClass__AlphaOperationAlphaContents_Delegation_source extends Box<AlphaOperationAlphaContentsDelegationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Delegation_source {
        return new this(record_decoder<AlphaOperationAlphaContentsDelegationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Delegation_delegate generated for AlphaOperationAlphaContentsDelegationDelegate
export class CGRIDClass__AlphaOperationAlphaContents_Delegation_delegate extends Box<AlphaOperationAlphaContentsDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Delegation_delegate {
        return new this(record_decoder<AlphaOperationAlphaContentsDelegationDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_source generated for AlphaOperationAlphaContentsDalPublishSlotHeaderSource
export class CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_source extends Box<AlphaOperationAlphaContentsDalPublishSlotHeaderSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_source {
        return new this(record_decoder<AlphaOperationAlphaContentsDalPublishSlotHeaderSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_slot_header_commitment generated for AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment
export class CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_slot_header_commitment extends Box<AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['dal_commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_slot_header_commitment {
        return new this(record_decoder<AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment>({dal_commitment: FixedBytes.decode<48>({len: 48})}, {order: ['dal_commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.dal_commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.dal_commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_slot_header generated for AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeader
export class CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_slot_header extends Box<AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['published_level', 'slot_index', 'commitment', 'commitment_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_slot_header {
        return new this(record_decoder<AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeader>({published_level: Int32.decode, slot_index: Uint8.decode, commitment: CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_slot_header_commitment.decode, commitment_proof: FixedBytes.decode<48>({len: 48})}, {order: ['published_level', 'slot_index', 'commitment', 'commitment_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.published_level.encodeLength +  this.value.slot_index.encodeLength +  this.value.commitment.encodeLength +  this.value.commitment_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.published_level.writeTarget(tgt) +  this.value.slot_index.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt) +  this.value.commitment_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Dal_attestation_attestor generated for AlphaOperationAlphaContentsDalAttestationAttestor
export class CGRIDClass__AlphaOperationAlphaContents_Dal_attestation_attestor extends Box<AlphaOperationAlphaContentsDalAttestationAttestor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Dal_attestation_attestor {
        return new this(record_decoder<AlphaOperationAlphaContentsDalAttestationAttestor>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Ballot_source generated for AlphaOperationAlphaContentsBallotSource
export class CGRIDClass__AlphaOperationAlphaContents_Ballot_source extends Box<AlphaOperationAlphaContentsBallotSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Ballot_source {
        return new this(record_decoder<AlphaOperationAlphaContentsBallotSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Ballot_proposal generated for AlphaOperationAlphaContentsBallotProposal
export class CGRIDClass__AlphaOperationAlphaContents_Ballot_proposal extends Box<AlphaOperationAlphaContentsBallotProposal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Ballot_proposal {
        return new this(record_decoder<AlphaOperationAlphaContentsBallotProposal>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents_Activate_account_pkh generated for AlphaOperationAlphaContentsActivateAccountPkh
export class CGRIDClass__AlphaOperationAlphaContents_Activate_account_pkh extends Box<AlphaOperationAlphaContentsActivateAccountPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents_Activate_account_pkh {
        return new this(record_decoder<AlphaOperationAlphaContentsActivateAccountPkh>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationAlphaContents generated for AlphaOperationAlphaContents
export function alphaoperationalphacontents_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationAlphaContents,AlphaOperationAlphaContents> {
    function f(disc: CGRIDTag__AlphaOperationAlphaContents) {
        switch (disc) {
            case CGRIDTag__AlphaOperationAlphaContents.Seed_nonce_revelation: return CGRIDClass__AlphaOperationAlphaContents__Seed_nonce_revelation.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Double_endorsement_evidence: return CGRIDClass__AlphaOperationAlphaContents__Double_endorsement_evidence.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Double_baking_evidence: return CGRIDClass__AlphaOperationAlphaContents__Double_baking_evidence.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Activate_account: return CGRIDClass__AlphaOperationAlphaContents__Activate_account.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Proposals: return CGRIDClass__AlphaOperationAlphaContents__Proposals.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Ballot: return CGRIDClass__AlphaOperationAlphaContents__Ballot.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Double_preendorsement_evidence: return CGRIDClass__AlphaOperationAlphaContents__Double_preendorsement_evidence.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Vdf_revelation: return CGRIDClass__AlphaOperationAlphaContents__Vdf_revelation.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Drain_delegate: return CGRIDClass__AlphaOperationAlphaContents__Drain_delegate.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Failing_noop: return CGRIDClass__AlphaOperationAlphaContents__Failing_noop.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Preendorsement: return CGRIDClass__AlphaOperationAlphaContents__Preendorsement.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Endorsement: return CGRIDClass__AlphaOperationAlphaContents__Endorsement.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Dal_attestation: return CGRIDClass__AlphaOperationAlphaContents__Dal_attestation.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Reveal: return CGRIDClass__AlphaOperationAlphaContents__Reveal.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Transaction: return CGRIDClass__AlphaOperationAlphaContents__Transaction.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Origination: return CGRIDClass__AlphaOperationAlphaContents__Origination.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Delegation: return CGRIDClass__AlphaOperationAlphaContents__Delegation.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Register_global_constant: return CGRIDClass__AlphaOperationAlphaContents__Register_global_constant.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Set_deposits_limit: return CGRIDClass__AlphaOperationAlphaContents__Set_deposits_limit.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Increase_paid_storage: return CGRIDClass__AlphaOperationAlphaContents__Increase_paid_storage.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Update_consensus_key: return CGRIDClass__AlphaOperationAlphaContents__Update_consensus_key.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_origination: return CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_origination.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_submit_batch: return CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_submit_batch.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_commit: return CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_commit.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_return_bond: return CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_return_bond.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_finalize_commitment: return CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_finalize_commitment.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_remove_commitment: return CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_remove_commitment.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_rejection: return CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_rejection.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_dispatch_tickets: return CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_dispatch_tickets.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Transfer_ticket: return CGRIDClass__AlphaOperationAlphaContents__Transfer_ticket.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_originate: return CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_originate.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_add_messages: return CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_add_messages.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_cement: return CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_cement.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_publish: return CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_publish.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_refute: return CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_refute.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_timeout: return CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_timeout.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_execute_outbox_message: return CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_execute_outbox_message.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_recover_bond: return CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_recover_bond.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Dal_publish_slot_header: return CGRIDClass__AlphaOperationAlphaContents__Dal_publish_slot_header.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Zk_rollup_origination: return CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_origination.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Zk_rollup_publish: return CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_publish.decode;
            case CGRIDTag__AlphaOperationAlphaContents.Zk_rollup_update: return CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_update.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationAlphaContents => Object.values(CGRIDTag__AlphaOperationAlphaContents).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperationAlphaContents extends Box<AlphaOperationAlphaContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationAlphaContents>, AlphaOperationAlphaContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationAlphaContents {
        return new this(variant_decoder(width.Uint8)(alphaoperationalphacontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaMichelsonV1Primitives generated for AlphaMichelsonV1Primitives
export class CGRIDClass__AlphaMichelsonV1Primitives extends Box<AlphaMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<AlphaMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is AlphaMichelsonV1Primitives => (Object.values(AlphaMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaInlinedPreendorsement_signature generated for AlphaInlinedPreendorsementSignature
export class CGRIDClass__AlphaInlinedPreendorsement_signature extends Box<AlphaInlinedPreendorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedPreendorsement_signature {
        return new this(record_decoder<AlphaInlinedPreendorsementSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedPreendorsementContents_Preendorsement_block_payload_hash generated for AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash
export class CGRIDClass__AlphaInlinedPreendorsementContents_Preendorsement_block_payload_hash extends Box<AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedPreendorsementContents_Preendorsement_block_payload_hash {
        return new this(record_decoder<AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedPreendorsementContents generated for AlphaInlinedPreendorsementContents
export function alphainlinedpreendorsementcontents_mkDecoder(): VariantDecoder<CGRIDTag__AlphaInlinedPreendorsementContents,AlphaInlinedPreendorsementContents> {
    function f(disc: CGRIDTag__AlphaInlinedPreendorsementContents) {
        switch (disc) {
            case CGRIDTag__AlphaInlinedPreendorsementContents.Preendorsement: return CGRIDClass__AlphaInlinedPreendorsementContents__Preendorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaInlinedPreendorsementContents => Object.values(CGRIDTag__AlphaInlinedPreendorsementContents).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaInlinedPreendorsementContents extends Box<AlphaInlinedPreendorsementContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaInlinedPreendorsementContents>, AlphaInlinedPreendorsementContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedPreendorsementContents {
        return new this(variant_decoder(width.Uint8)(alphainlinedpreendorsementcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedPreendorsement generated for AlphaInlinedPreendorsement
export class CGRIDClass__AlphaInlinedPreendorsement extends Box<AlphaInlinedPreendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedPreendorsement {
        return new this(record_decoder<AlphaInlinedPreendorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__AlphaInlinedPreendorsementContents.decode, signature: Nullable.decode(CGRIDClass__AlphaInlinedPreendorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedEndorsement_signature generated for AlphaInlinedEndorsementSignature
export class CGRIDClass__AlphaInlinedEndorsement_signature extends Box<AlphaInlinedEndorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedEndorsement_signature {
        return new this(record_decoder<AlphaInlinedEndorsementSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash generated for AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash
export class CGRIDClass__AlphaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash extends Box<AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash {
        return new this(record_decoder<AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedEndorsement_mempoolContents generated for AlphaInlinedEndorsementMempoolContents
export function alphainlinedendorsementmempoolcontents_mkDecoder(): VariantDecoder<CGRIDTag__AlphaInlinedEndorsementMempoolContents,AlphaInlinedEndorsementMempoolContents> {
    function f(disc: CGRIDTag__AlphaInlinedEndorsementMempoolContents) {
        switch (disc) {
            case CGRIDTag__AlphaInlinedEndorsementMempoolContents.Endorsement: return CGRIDClass__AlphaInlinedEndorsementMempoolContents__Endorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaInlinedEndorsementMempoolContents => Object.values(CGRIDTag__AlphaInlinedEndorsementMempoolContents).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaInlinedEndorsement_mempoolContents extends Box<AlphaInlinedEndorsementMempoolContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaInlinedEndorsementMempoolContents>, AlphaInlinedEndorsementMempoolContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedEndorsement_mempoolContents {
        return new this(variant_decoder(width.Uint8)(alphainlinedendorsementmempoolcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaInlinedEndorsement generated for AlphaInlinedEndorsement
export class CGRIDClass__AlphaInlinedEndorsement extends Box<AlphaInlinedEndorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaInlinedEndorsement {
        return new this(record_decoder<AlphaInlinedEndorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__AlphaInlinedEndorsement_mempoolContents.decode, signature: Nullable.decode(CGRIDClass__AlphaInlinedEndorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaEntrypoint generated for AlphaEntrypoint
export function alphaentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__AlphaEntrypoint,AlphaEntrypoint> {
    function f(disc: CGRIDTag__AlphaEntrypoint) {
        switch (disc) {
            case CGRIDTag__AlphaEntrypoint._default: return CGRIDClass__AlphaEntrypoint___default.decode;
            case CGRIDTag__AlphaEntrypoint.root: return CGRIDClass__AlphaEntrypoint__root.decode;
            case CGRIDTag__AlphaEntrypoint._do: return CGRIDClass__AlphaEntrypoint___do.decode;
            case CGRIDTag__AlphaEntrypoint.set_delegate: return CGRIDClass__AlphaEntrypoint__set_delegate.decode;
            case CGRIDTag__AlphaEntrypoint.remove_delegate: return CGRIDClass__AlphaEntrypoint__remove_delegate.decode;
            case CGRIDTag__AlphaEntrypoint.deposit: return CGRIDClass__AlphaEntrypoint__deposit.decode;
            case CGRIDTag__AlphaEntrypoint.named: return CGRIDClass__AlphaEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaEntrypoint => Object.values(CGRIDTag__AlphaEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaEntrypoint extends Box<AlphaEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaEntrypoint>, AlphaEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint {
        return new this(variant_decoder(width.Uint8)(alphaentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContract_id_Originated_denest_pad generated for AlphaContractIdOriginatedDenestPad
export class CGRIDClass__AlphaContract_id_Originated_denest_pad extends Box<AlphaContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContract_id_Originated_denest_pad {
        return new this(record_decoder<AlphaContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContract_idOriginated_Originated_denest_pad generated for AlphaContractIdOriginatedOriginatedDenestPad
export class CGRIDClass__AlphaContract_idOriginated_Originated_denest_pad extends Box<AlphaContractIdOriginatedOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContract_idOriginated_Originated_denest_pad {
        return new this(record_decoder<AlphaContractIdOriginatedOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContract_idOriginated generated for AlphaContractIdOriginated
export function alphacontractidoriginated_mkDecoder(): VariantDecoder<CGRIDTag__AlphaContractIdOriginated,AlphaContractIdOriginated> {
    function f(disc: CGRIDTag__AlphaContractIdOriginated) {
        switch (disc) {
            case CGRIDTag__AlphaContractIdOriginated.Originated: return CGRIDClass__AlphaContractIdOriginated__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaContractIdOriginated => Object.values(CGRIDTag__AlphaContractIdOriginated).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaContract_idOriginated extends Box<AlphaContractIdOriginated> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaContractIdOriginated>, AlphaContractIdOriginated>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContract_idOriginated {
        return new this(variant_decoder(width.Uint8)(alphacontractidoriginated_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContract_id generated for AlphaContractId
export function alphacontractid_mkDecoder(): VariantDecoder<CGRIDTag__AlphaContractId,AlphaContractId> {
    function f(disc: CGRIDTag__AlphaContractId) {
        switch (disc) {
            case CGRIDTag__AlphaContractId.Implicit: return CGRIDClass__AlphaContractId__Implicit.decode;
            case CGRIDTag__AlphaContractId.Originated: return CGRIDClass__AlphaContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaContractId => Object.values(CGRIDTag__AlphaContractId).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaContract_id extends Box<AlphaContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaContractId>, AlphaContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContract_id {
        return new this(variant_decoder(width.Uint8)(alphacontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash generated for AlphaBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash extends Box<AlphaBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<AlphaBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBlock_headerAlphaSigned_contents_signature generated for AlphaBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__AlphaBlock_headerAlphaSigned_contents_signature extends Box<AlphaBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<AlphaBlockHeaderAlphaSignedContentsSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBlock_headerAlphaFull_header generated for AlphaBlockHeaderAlphaFullHeader
export class CGRIDClass__AlphaBlock_headerAlphaFull_header extends Box<AlphaBlockHeaderAlphaFullHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBlock_headerAlphaFull_header {
        return new this(record_decoder<AlphaBlockHeaderAlphaFullHeader>({level: Int32.decode, proto: Uint8.decode, predecessor: CGRIDClass__Block_headerShell_predecessor.decode, timestamp: Int64.decode, validation_pass: Uint8.decode, operations_hash: CGRIDClass__Block_headerShell_operations_hash.decode, fitness: Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30), context: CGRIDClass__Block_headerShell_context.decode, payload_hash: CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__AlphaBlock_headerAlphaSigned_contents_signature.decode}, {order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.proto.encodeLength +  this.value.predecessor.encodeLength +  this.value.timestamp.encodeLength +  this.value.validation_pass.encodeLength +  this.value.operations_hash.encodeLength +  this.value.fitness.encodeLength +  this.value.context.encodeLength +  this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_toggle_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.proto.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.timestamp.writeTarget(tgt) +  this.value.validation_pass.writeTarget(tgt) +  this.value.operations_hash.writeTarget(tgt) +  this.value.fitness.writeTarget(tgt) +  this.value.context.writeTarget(tgt) +  this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_toggle_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256,
    Bls: CGRIDClass__PublicKeyHash__Bls
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] } | { kind: CGRIDTag__PublicKeyHash.Bls, value: CGRIDMap__PublicKeyHash['Bls'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256,
    Bls: CGRIDClass__PublicKey__Bls
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] } | { kind: CGRIDTag__PublicKey.Bls, value: CGRIDMap__PublicKey['Bls'] };
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export enum CGRIDTag__MichelineAlphaMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineAlphaMichelsonV1Expression {
    Int: CGRIDClass__MichelineAlphaMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineAlphaMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes
}
export type MichelineAlphaMichelsonV1Expression = { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Int, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.String, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Bytes'] };
export type BlockHeaderShellPredecessor = { block_hash: FixedBytes<32> };
export type BlockHeaderShellOperationsHash = { operation_list_list_hash: FixedBytes<32> };
export type BlockHeaderShellContext = { context_hash: FixedBytes<32> };
export type AlphaTxRollupId = { rollup_hash: FixedBytes<20> };
export type AlphaSmartRollupAddress = { smart_rollup_hash: FixedBytes<20> };
export type AlphaScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export type AlphaOperationAlphaContentsZkRollupUpdateZkRollup = { zk_rollup_hash: FixedBytes<20> };
export type AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1 = { new_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30>, fee: FixedBytes<32> };
export type AlphaOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq = [Dynamic<U8String,width.Uint30>, CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1];
export type AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1 = { new_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30>, fee: FixedBytes<32>, exit_validity: Bool };
export type AlphaOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq = [Dynamic<U8String,width.Uint30>, CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1];
export type AlphaOperationAlphaContentsZkRollupUpdateUpdateFeePi = { new_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type AlphaOperationAlphaContentsZkRollupUpdateUpdate = { pending_pis: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq>,width.Uint30>, private_pis: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq>,width.Uint30>, fee_pi: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_update_update_fee_pi, proof: Dynamic<Bytes,width.Uint30> };
export type AlphaOperationAlphaContentsZkRollupUpdateSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsZkRollupPublishZkRollup = { zk_rollup_hash: FixedBytes<20> };
export type AlphaOperationAlphaContentsZkRollupPublishSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1{
    None = 0,
    Some = 1
}
export interface CGRIDMap__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1 {
    None: CGRIDClass__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None,
    Some: CGRIDClass__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some
}
export type AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1 = { kind: CGRIDTag__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1.None, value: CGRIDMap__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1['None'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1.Some, value: CGRIDMap__AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1['Some'] };
export type AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId = { zk_rollup_hash: FixedBytes<20> };
export type AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId = { script_expr: FixedBytes<32> };
export type AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price = { id: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id, amount: Z };
export type AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0 = { op_code: Int31, price: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price, l1_dst: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst, rollup_id: CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id, payload: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type AlphaOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq = [CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0, CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index1];
export type AlphaOperationAlphaContentsZkRollupOriginationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1{
    Public = 0,
    Private = 1,
    Fee = 2
}
export interface CGRIDMap__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1 {
    Public: CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public,
    Private: CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private,
    Fee: CGRIDClass__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee
}
export type AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1 = { kind: CGRIDTag__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Public, value: CGRIDMap__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1['Public'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Private, value: CGRIDMap__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1['Private'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Fee, value: CGRIDMap__AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1['Fee'] };
export type AlphaOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq = [Dynamic<U8String,width.Uint30>, CGRIDClass__AlphaOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1];
export type AlphaOperationAlphaContentsVdfRevelationSolution = [FixedBytes<100>, FixedBytes<100>];
export type AlphaOperationAlphaContentsUpdateConsensusKeySource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsUpdateConsensusKeyPk = { signature_public_key: CGRIDClass__Public_key };
export type AlphaOperationAlphaContentsTxRollupSubmitBatchSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsTxRollupReturnBondSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsTxRollupRemoveCommitmentSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsTxRollupRejectionSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash = { withdraw_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash = { context_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupRejectionPreviousMessageResult = { context_hash: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash, withdraw_list_hash: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash };
export type AlphaOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupRejectionMessageResultHash = { message_result_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq = { inbox_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash = { script_expr: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination = { bls12_381_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3
}
export interface CGRIDMap__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount {
    case_0: CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0,
    case_1: CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1,
    case_2: CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2,
    case_3: CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3
}
export type AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount = { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_0, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_0'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_1, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_1'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_2, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_2'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_3, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_3'] };
export type AlphaOperationAlphaContentsTxRollupRejectionMessageDepositDeposit = { sender: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender, destination: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination, ticket_hash: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash, amount: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount };
export enum CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessage{
    Batch = 0,
    Deposit = 1
}
export interface CGRIDMap__AlphaOperationAlphaContentsTxRollupRejectionMessage {
    Batch: CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessage__Batch,
    Deposit: CGRIDClass__AlphaOperationAlphaContentsTxRollupRejectionMessage__Deposit
}
export type AlphaOperationAlphaContentsTxRollupRejectionMessage = { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessage.Batch, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupRejectionMessage['Batch'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupRejectionMessage.Deposit, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupRejectionMessage['Deposit'] };
export type AlphaOperationAlphaContentsTxRollupOriginationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsTxRollupFinalizeCommitmentSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3
}
export interface CGRIDMap__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount {
    case_0: CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0,
    case_1: CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1,
    case_2: CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2,
    case_3: CGRIDClass__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3
}
export type AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount = { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_0, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_0'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_1, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_1'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_2, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_2'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_3, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_3'] };
export type AlphaOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq = { contents: Dynamic<Bytes,width.Uint30>, ty: Dynamic<Bytes,width.Uint30>, ticketer: CGRIDClass__AlphaContract_id, amount: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount, claimer: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer };
export type AlphaOperationAlphaContentsTxRollupDispatchTicketsSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupDispatchTicketsContextHash = { context_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupCommitSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor{
    None = 0,
    Some = 1
}
export interface CGRIDMap__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor {
    None: CGRIDClass__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None,
    Some: CGRIDClass__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some
}
export type AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor = { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor.None, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor['None'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor.Some, value: CGRIDMap__AlphaOperationAlphaContentsTxRollupCommitCommitmentPredecessor['Some'] };
export type AlphaOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq = { message_result_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot = { inbox_list_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsTxRollupCommitCommitment = { level: Int32, messages: Dynamic<Sequence<CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq>,width.Uint30>, predecessor: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_predecessor, inbox_merkle_root: CGRIDClass__AlphaOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root };
export type AlphaOperationAlphaContentsTransferTicketSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsTransactionSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsTransactionParameters = { entrypoint: CGRIDClass__AlphaEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type AlphaOperationAlphaContentsSmartRollupTimeoutStakersBob = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsSmartRollupTimeoutStakersAlice = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsSmartRollupTimeoutStakers = { alice: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers_alice, bob: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_timeout_stakers_bob };
export type AlphaOperationAlphaContentsSmartRollupTimeoutSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsSmartRollupRefuteSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash = { smart_rollup_commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash = { smart_rollup_commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId = { published_level: Int32, slot_index: Uint8, page_index: Int16 };
export enum CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof{
    raw_data_proof = 0,
    metadata_proof = 1,
    dal_page_proof = 2
}
export interface CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof {
    raw_data_proof: CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof,
    metadata_proof: CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof,
    dal_page_proof: CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof
}
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof = { kind: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.raw_data_proof, value: CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof['raw_data_proof'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.metadata_proof, value: CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof['metadata_proof'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.dal_page_proof, value: CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof['dal_page_proof'] };
export enum CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof{
    inbox_proof = 0,
    reveal_proof = 1,
    first_input = 2
}
export interface CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof {
    inbox_proof: CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof,
    reveal_proof: CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof,
    first_input: CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input
}
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof = { kind: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.inbox_proof, value: CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof['inbox_proof'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.reveal_proof, value: CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof['reveal_proof'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.first_input, value: CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof['first_input'] };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState = { smart_rollup_state_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq = { state: Option<CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state>, tick: N };
export enum CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep{
    Dissection = 0,
    Proof = 1
}
export interface CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep {
    Dissection: CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection,
    Proof: CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof
}
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep = { kind: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep.Dissection, value: CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep['Dissection'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep.Proof, value: CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutationMoveStep['Proof'] };
export enum CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutation{
    Start = 0,
    Move = 1
}
export interface CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutation {
    Start: CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Start,
    Move: CGRIDClass__AlphaOperationAlphaContentsSmartRollupRefuteRefutation__Move
}
export type AlphaOperationAlphaContentsSmartRollupRefuteRefutation = { kind: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutation.Start, value: CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutation['Start'] } | { kind: CGRIDTag__AlphaOperationAlphaContentsSmartRollupRefuteRefutation.Move, value: CGRIDMap__AlphaOperationAlphaContentsSmartRollupRefuteRefutation['Move'] };
export type AlphaOperationAlphaContentsSmartRollupRefuteOpponent = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsSmartRollupRecoverBondStaker = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsSmartRollupRecoverBondSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsSmartRollupRecoverBondRollup = { smart_rollup_hash: FixedBytes<20> };
export type AlphaOperationAlphaContentsSmartRollupPublishSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsSmartRollupPublishCommitmentPredecessor = { smart_rollup_commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsSmartRollupPublishCommitmentCompressedState = { smart_rollup_state_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsSmartRollupPublishCommitment = { compressed_state: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment_compressed_state, inbox_level: Int32, predecessor: CGRIDClass__AlphaOperationAlphaContents_Smart_rollup_publish_commitment_predecessor, number_of_ticks: Int64 };
export type AlphaOperationAlphaContentsSmartRollupOriginateSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum AlphaOperationAlphaContentsSmartRollupOriginatePvmKind{
    arith = 0,
    wasm_2_0_0 = 1
}
export type AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment = { smart_rollup_commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsSmartRollupCementSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsSmartRollupCementCommitment = { smart_rollup_commitment_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsSmartRollupAddMessagesSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsSetDepositsLimitSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsRevealSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsRevealPublicKey = { signature_public_key: CGRIDClass__Public_key };
export type AlphaOperationAlphaContentsRegisterGlobalConstantSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsProposalsSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq = { protocol_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsPreendorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsOriginationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsOriginationDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsIncreasePaidStorageSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsEndorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsDrainDelegateDestination = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsDrainDelegateDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsDrainDelegateConsensusKey = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsDelegationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsDelegationDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsDalPublishSlotHeaderSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment = { dal_commitment: FixedBytes<48> };
export type AlphaOperationAlphaContentsDalPublishSlotHeaderSlotHeader = { published_level: Int32, slot_index: Uint8, commitment: CGRIDClass__AlphaOperationAlphaContents_Dal_publish_slot_header_slot_header_commitment, commitment_proof: FixedBytes<48> };
export type AlphaOperationAlphaContentsDalAttestationAttestor = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsBallotSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationAlphaContentsBallotProposal = { protocol_hash: FixedBytes<32> };
export type AlphaOperationAlphaContentsActivateAccountPkh = { ed25519_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__AlphaOperationAlphaContents{
    Seed_nonce_revelation = 1,
    Double_endorsement_evidence = 2,
    Double_baking_evidence = 3,
    Activate_account = 4,
    Proposals = 5,
    Ballot = 6,
    Double_preendorsement_evidence = 7,
    Vdf_revelation = 8,
    Drain_delegate = 9,
    Failing_noop = 17,
    Preendorsement = 20,
    Endorsement = 21,
    Dal_attestation = 22,
    Reveal = 107,
    Transaction = 108,
    Origination = 109,
    Delegation = 110,
    Register_global_constant = 111,
    Set_deposits_limit = 112,
    Increase_paid_storage = 113,
    Update_consensus_key = 114,
    Tx_rollup_origination = 150,
    Tx_rollup_submit_batch = 151,
    Tx_rollup_commit = 152,
    Tx_rollup_return_bond = 153,
    Tx_rollup_finalize_commitment = 154,
    Tx_rollup_remove_commitment = 155,
    Tx_rollup_rejection = 156,
    Tx_rollup_dispatch_tickets = 157,
    Transfer_ticket = 158,
    Smart_rollup_originate = 200,
    Smart_rollup_add_messages = 201,
    Smart_rollup_cement = 202,
    Smart_rollup_publish = 203,
    Smart_rollup_refute = 204,
    Smart_rollup_timeout = 205,
    Smart_rollup_execute_outbox_message = 206,
    Smart_rollup_recover_bond = 207,
    Dal_publish_slot_header = 230,
    Zk_rollup_origination = 250,
    Zk_rollup_publish = 251,
    Zk_rollup_update = 252
}
export interface CGRIDMap__AlphaOperationAlphaContents {
    Seed_nonce_revelation: CGRIDClass__AlphaOperationAlphaContents__Seed_nonce_revelation,
    Double_endorsement_evidence: CGRIDClass__AlphaOperationAlphaContents__Double_endorsement_evidence,
    Double_baking_evidence: CGRIDClass__AlphaOperationAlphaContents__Double_baking_evidence,
    Activate_account: CGRIDClass__AlphaOperationAlphaContents__Activate_account,
    Proposals: CGRIDClass__AlphaOperationAlphaContents__Proposals,
    Ballot: CGRIDClass__AlphaOperationAlphaContents__Ballot,
    Double_preendorsement_evidence: CGRIDClass__AlphaOperationAlphaContents__Double_preendorsement_evidence,
    Vdf_revelation: CGRIDClass__AlphaOperationAlphaContents__Vdf_revelation,
    Drain_delegate: CGRIDClass__AlphaOperationAlphaContents__Drain_delegate,
    Failing_noop: CGRIDClass__AlphaOperationAlphaContents__Failing_noop,
    Preendorsement: CGRIDClass__AlphaOperationAlphaContents__Preendorsement,
    Endorsement: CGRIDClass__AlphaOperationAlphaContents__Endorsement,
    Dal_attestation: CGRIDClass__AlphaOperationAlphaContents__Dal_attestation,
    Reveal: CGRIDClass__AlphaOperationAlphaContents__Reveal,
    Transaction: CGRIDClass__AlphaOperationAlphaContents__Transaction,
    Origination: CGRIDClass__AlphaOperationAlphaContents__Origination,
    Delegation: CGRIDClass__AlphaOperationAlphaContents__Delegation,
    Register_global_constant: CGRIDClass__AlphaOperationAlphaContents__Register_global_constant,
    Set_deposits_limit: CGRIDClass__AlphaOperationAlphaContents__Set_deposits_limit,
    Increase_paid_storage: CGRIDClass__AlphaOperationAlphaContents__Increase_paid_storage,
    Update_consensus_key: CGRIDClass__AlphaOperationAlphaContents__Update_consensus_key,
    Tx_rollup_origination: CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_origination,
    Tx_rollup_submit_batch: CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_submit_batch,
    Tx_rollup_commit: CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_commit,
    Tx_rollup_return_bond: CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_return_bond,
    Tx_rollup_finalize_commitment: CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_finalize_commitment,
    Tx_rollup_remove_commitment: CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_remove_commitment,
    Tx_rollup_rejection: CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_rejection,
    Tx_rollup_dispatch_tickets: CGRIDClass__AlphaOperationAlphaContents__Tx_rollup_dispatch_tickets,
    Transfer_ticket: CGRIDClass__AlphaOperationAlphaContents__Transfer_ticket,
    Smart_rollup_originate: CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_originate,
    Smart_rollup_add_messages: CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_add_messages,
    Smart_rollup_cement: CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_cement,
    Smart_rollup_publish: CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_publish,
    Smart_rollup_refute: CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_refute,
    Smart_rollup_timeout: CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_timeout,
    Smart_rollup_execute_outbox_message: CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_execute_outbox_message,
    Smart_rollup_recover_bond: CGRIDClass__AlphaOperationAlphaContents__Smart_rollup_recover_bond,
    Dal_publish_slot_header: CGRIDClass__AlphaOperationAlphaContents__Dal_publish_slot_header,
    Zk_rollup_origination: CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_origination,
    Zk_rollup_publish: CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_publish,
    Zk_rollup_update: CGRIDClass__AlphaOperationAlphaContents__Zk_rollup_update
}
export type AlphaOperationAlphaContents = { kind: CGRIDTag__AlphaOperationAlphaContents.Seed_nonce_revelation, value: CGRIDMap__AlphaOperationAlphaContents['Seed_nonce_revelation'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Double_endorsement_evidence, value: CGRIDMap__AlphaOperationAlphaContents['Double_endorsement_evidence'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Double_baking_evidence, value: CGRIDMap__AlphaOperationAlphaContents['Double_baking_evidence'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Activate_account, value: CGRIDMap__AlphaOperationAlphaContents['Activate_account'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Proposals, value: CGRIDMap__AlphaOperationAlphaContents['Proposals'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Ballot, value: CGRIDMap__AlphaOperationAlphaContents['Ballot'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Double_preendorsement_evidence, value: CGRIDMap__AlphaOperationAlphaContents['Double_preendorsement_evidence'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Vdf_revelation, value: CGRIDMap__AlphaOperationAlphaContents['Vdf_revelation'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Drain_delegate, value: CGRIDMap__AlphaOperationAlphaContents['Drain_delegate'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Failing_noop, value: CGRIDMap__AlphaOperationAlphaContents['Failing_noop'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Preendorsement, value: CGRIDMap__AlphaOperationAlphaContents['Preendorsement'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Endorsement, value: CGRIDMap__AlphaOperationAlphaContents['Endorsement'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Dal_attestation, value: CGRIDMap__AlphaOperationAlphaContents['Dal_attestation'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Reveal, value: CGRIDMap__AlphaOperationAlphaContents['Reveal'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Transaction, value: CGRIDMap__AlphaOperationAlphaContents['Transaction'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Origination, value: CGRIDMap__AlphaOperationAlphaContents['Origination'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Delegation, value: CGRIDMap__AlphaOperationAlphaContents['Delegation'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Register_global_constant, value: CGRIDMap__AlphaOperationAlphaContents['Register_global_constant'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Set_deposits_limit, value: CGRIDMap__AlphaOperationAlphaContents['Set_deposits_limit'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Increase_paid_storage, value: CGRIDMap__AlphaOperationAlphaContents['Increase_paid_storage'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Update_consensus_key, value: CGRIDMap__AlphaOperationAlphaContents['Update_consensus_key'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_origination, value: CGRIDMap__AlphaOperationAlphaContents['Tx_rollup_origination'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_submit_batch, value: CGRIDMap__AlphaOperationAlphaContents['Tx_rollup_submit_batch'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_commit, value: CGRIDMap__AlphaOperationAlphaContents['Tx_rollup_commit'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_return_bond, value: CGRIDMap__AlphaOperationAlphaContents['Tx_rollup_return_bond'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_finalize_commitment, value: CGRIDMap__AlphaOperationAlphaContents['Tx_rollup_finalize_commitment'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_remove_commitment, value: CGRIDMap__AlphaOperationAlphaContents['Tx_rollup_remove_commitment'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_rejection, value: CGRIDMap__AlphaOperationAlphaContents['Tx_rollup_rejection'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Tx_rollup_dispatch_tickets, value: CGRIDMap__AlphaOperationAlphaContents['Tx_rollup_dispatch_tickets'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Transfer_ticket, value: CGRIDMap__AlphaOperationAlphaContents['Transfer_ticket'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_originate, value: CGRIDMap__AlphaOperationAlphaContents['Smart_rollup_originate'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_add_messages, value: CGRIDMap__AlphaOperationAlphaContents['Smart_rollup_add_messages'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_cement, value: CGRIDMap__AlphaOperationAlphaContents['Smart_rollup_cement'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_publish, value: CGRIDMap__AlphaOperationAlphaContents['Smart_rollup_publish'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_refute, value: CGRIDMap__AlphaOperationAlphaContents['Smart_rollup_refute'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_timeout, value: CGRIDMap__AlphaOperationAlphaContents['Smart_rollup_timeout'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_execute_outbox_message, value: CGRIDMap__AlphaOperationAlphaContents['Smart_rollup_execute_outbox_message'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Smart_rollup_recover_bond, value: CGRIDMap__AlphaOperationAlphaContents['Smart_rollup_recover_bond'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Dal_publish_slot_header, value: CGRIDMap__AlphaOperationAlphaContents['Dal_publish_slot_header'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Zk_rollup_origination, value: CGRIDMap__AlphaOperationAlphaContents['Zk_rollup_origination'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Zk_rollup_publish, value: CGRIDMap__AlphaOperationAlphaContents['Zk_rollup_publish'] } | { kind: CGRIDTag__AlphaOperationAlphaContents.Zk_rollup_update, value: CGRIDMap__AlphaOperationAlphaContents['Zk_rollup_update'] };
export enum AlphaMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117,
    LEVEL = 118,
    SELF_ADDRESS = 119,
    never = 120,
    NEVER = 121,
    UNPAIR = 122,
    VOTING_POWER = 123,
    TOTAL_VOTING_POWER = 124,
    KECCAK = 125,
    SHA3 = 126,
    PAIRING_CHECK = 127,
    bls12_381_g1 = 128,
    bls12_381_g2 = 129,
    bls12_381_fr = 130,
    sapling_state = 131,
    sapling_transaction_deprecated = 132,
    SAPLING_EMPTY_STATE = 133,
    SAPLING_VERIFY_UPDATE = 134,
    ticket = 135,
    TICKET_DEPRECATED = 136,
    READ_TICKET = 137,
    SPLIT_TICKET = 138,
    JOIN_TICKETS = 139,
    GET_AND_UPDATE = 140,
    chest = 141,
    chest_key = 142,
    OPEN_CHEST = 143,
    VIEW = 144,
    view = 145,
    constant = 146,
    SUB_MUTEZ = 147,
    tx_rollup_l2_address = 148,
    MIN_BLOCK_TIME = 149,
    sapling_transaction = 150,
    EMIT = 151,
    Lambda_rec = 152,
    LAMBDA_REC = 153,
    TICKET = 154,
    BYTES = 155,
    NAT = 156
}
export type AlphaInlinedPreendorsementSignature = { signature_v1: Bytes };
export type AlphaInlinedPreendorsementContentsPreendorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export enum CGRIDTag__AlphaInlinedPreendorsementContents{
    Preendorsement = 20
}
export interface CGRIDMap__AlphaInlinedPreendorsementContents {
    Preendorsement: CGRIDClass__AlphaInlinedPreendorsementContents__Preendorsement
}
export type AlphaInlinedPreendorsementContents = { kind: CGRIDTag__AlphaInlinedPreendorsementContents.Preendorsement, value: CGRIDMap__AlphaInlinedPreendorsementContents['Preendorsement'] };
export type AlphaInlinedPreendorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__AlphaInlinedPreendorsementContents, signature: Nullable<CGRIDClass__AlphaInlinedPreendorsement_signature> };
export type AlphaInlinedEndorsementSignature = { signature_v1: Bytes };
export type AlphaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export enum CGRIDTag__AlphaInlinedEndorsementMempoolContents{
    Endorsement = 21
}
export interface CGRIDMap__AlphaInlinedEndorsementMempoolContents {
    Endorsement: CGRIDClass__AlphaInlinedEndorsementMempoolContents__Endorsement
}
export type AlphaInlinedEndorsementMempoolContents = { kind: CGRIDTag__AlphaInlinedEndorsementMempoolContents.Endorsement, value: CGRIDMap__AlphaInlinedEndorsementMempoolContents['Endorsement'] };
export type AlphaInlinedEndorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__AlphaInlinedEndorsement_mempoolContents, signature: Nullable<CGRIDClass__AlphaInlinedEndorsement_signature> };
export enum CGRIDTag__AlphaEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    deposit = 5,
    named = 255
}
export interface CGRIDMap__AlphaEntrypoint {
    _default: CGRIDClass__AlphaEntrypoint___default,
    root: CGRIDClass__AlphaEntrypoint__root,
    _do: CGRIDClass__AlphaEntrypoint___do,
    set_delegate: CGRIDClass__AlphaEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__AlphaEntrypoint__remove_delegate,
    deposit: CGRIDClass__AlphaEntrypoint__deposit,
    named: CGRIDClass__AlphaEntrypoint__named
}
export type AlphaEntrypoint = { kind: CGRIDTag__AlphaEntrypoint._default, value: CGRIDMap__AlphaEntrypoint['_default'] } | { kind: CGRIDTag__AlphaEntrypoint.root, value: CGRIDMap__AlphaEntrypoint['root'] } | { kind: CGRIDTag__AlphaEntrypoint._do, value: CGRIDMap__AlphaEntrypoint['_do'] } | { kind: CGRIDTag__AlphaEntrypoint.set_delegate, value: CGRIDMap__AlphaEntrypoint['set_delegate'] } | { kind: CGRIDTag__AlphaEntrypoint.remove_delegate, value: CGRIDMap__AlphaEntrypoint['remove_delegate'] } | { kind: CGRIDTag__AlphaEntrypoint.deposit, value: CGRIDMap__AlphaEntrypoint['deposit'] } | { kind: CGRIDTag__AlphaEntrypoint.named, value: CGRIDMap__AlphaEntrypoint['named'] };
export type AlphaContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export type AlphaContractIdOriginatedOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__AlphaContractIdOriginated{
    Originated = 1
}
export interface CGRIDMap__AlphaContractIdOriginated {
    Originated: CGRIDClass__AlphaContractIdOriginated__Originated
}
export type AlphaContractIdOriginated = { kind: CGRIDTag__AlphaContractIdOriginated.Originated, value: CGRIDMap__AlphaContractIdOriginated['Originated'] };
export enum CGRIDTag__AlphaContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__AlphaContractId {
    Implicit: CGRIDClass__AlphaContractId__Implicit,
    Originated: CGRIDClass__AlphaContractId__Originated
}
export type AlphaContractId = { kind: CGRIDTag__AlphaContractId.Implicit, value: CGRIDMap__AlphaContractId['Implicit'] } | { kind: CGRIDTag__AlphaContractId.Originated, value: CGRIDMap__AlphaContractId['Originated'] };
export type AlphaBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type AlphaBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type AlphaBlockHeaderAlphaSignedContentsSignature = { signature_v1: Bytes };
export type AlphaBlockHeaderAlphaFullHeader = { level: Int32, proto: Uint8, predecessor: CGRIDClass__Block_headerShell_predecessor, timestamp: Int64, validation_pass: Uint8, operations_hash: CGRIDClass__Block_headerShell_operations_hash, fitness: Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>, context: CGRIDClass__Block_headerShell_context, payload_hash: CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__AlphaBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_toggle_vote: Int8, signature: CGRIDClass__AlphaBlock_headerAlphaSigned_contents_signature };
export type AlphaOperationContentsList = Sequence<CGRIDClass__AlphaOperationAlphaContents>;
export class CGRIDClass__AlphaOperationContentsList extends Box<AlphaOperationContentsList> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationContentsList {
        return new this(Sequence.decode(CGRIDClass__AlphaOperationAlphaContents.decode)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const alpha_operation_contents_list_encoder = (value: AlphaOperationContentsList): OutputBytes => {
    return value.encode();
}
export const alpha_operation_contents_list_decoder = (p: Parser): AlphaOperationContentsList => {
    return Sequence.decode(CGRIDClass__AlphaOperationAlphaContents.decode)(p);
}
