import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__AlphaGas__Unaccounted generated for AlphaGas__Unaccounted
export class CGRIDClass__AlphaGas__Unaccounted extends Box<AlphaGas__Unaccounted> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaGas__Unaccounted {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaGas__Limited generated for AlphaGas__Limited
export class CGRIDClass__AlphaGas__Limited extends Box<AlphaGas__Limited> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaGas__Limited {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type AlphaGas__Unaccounted = Unit;
export type AlphaGas__Limited = Z;
export enum CGRIDTag__AlphaGas{
    Limited = 0,
    Unaccounted = 1
}
export interface CGRIDMap__AlphaGas {
    Limited: CGRIDClass__AlphaGas__Limited,
    Unaccounted: CGRIDClass__AlphaGas__Unaccounted
}
export type AlphaGas = { kind: CGRIDTag__AlphaGas.Limited, value: CGRIDMap__AlphaGas['Limited'] } | { kind: CGRIDTag__AlphaGas.Unaccounted, value: CGRIDMap__AlphaGas['Unaccounted'] };
export function alphagas_mkDecoder(): VariantDecoder<CGRIDTag__AlphaGas,AlphaGas> {
    function f(disc: CGRIDTag__AlphaGas) {
        switch (disc) {
            case CGRIDTag__AlphaGas.Limited: return CGRIDClass__AlphaGas__Limited.decode;
            case CGRIDTag__AlphaGas.Unaccounted: return CGRIDClass__AlphaGas__Unaccounted.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaGas => Object.values(CGRIDTag__AlphaGas).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaGas extends Box<AlphaGas> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaGas>, AlphaGas>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaGas {
        return new this(variant_decoder(width.Uint8)(alphagas_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const alpha_gas_encoder = (value: AlphaGas): OutputBytes => {
    return variant_encoder<KindOf<AlphaGas>, AlphaGas>(width.Uint8)(value);
}
export const alpha_gas_decoder = (p: Parser): AlphaGas => {
    return variant_decoder(width.Uint8)(alphagas_mkDecoder())(p);
}
