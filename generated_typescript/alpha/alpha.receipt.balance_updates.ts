import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Bls generated for PublicKeyHash__Bls
export class CGRIDClass__PublicKeyHash__Bls extends Box<PublicKeyHash__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Bls {
        return new this(record_decoder<PublicKeyHash__Bls>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Subsidy generated for AlphaOperationMetadataAlphaUpdateOriginOrigin__Subsidy
export class CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Subsidy extends Box<AlphaOperationMetadataAlphaUpdateOriginOrigin__Subsidy> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Subsidy {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Simulation generated for AlphaOperationMetadataAlphaUpdateOriginOrigin__Simulation
export class CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Simulation extends Box<AlphaOperationMetadataAlphaUpdateOriginOrigin__Simulation> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Simulation {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration generated for AlphaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration
export class CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration extends Box<AlphaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Block_application generated for AlphaOperationMetadataAlphaUpdateOriginOrigin__Block_application
export class CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Block_application extends Box<AlphaOperationMetadataAlphaUpdateOriginOrigin__Block_application> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Block_application {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards generated for AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards extends Box<AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments generated for AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments extends Box<AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Storage_fees generated for AlphaOperationMetadataAlphaBalance__Storage_fees
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Storage_fees extends Box<AlphaOperationMetadataAlphaBalance__Storage_fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Storage_fees {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Storage_fees>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards generated for AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards extends Box<AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments generated for AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments extends Box<AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Nonce_revelation_rewards generated for AlphaOperationMetadataAlphaBalance__Nonce_revelation_rewards
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Nonce_revelation_rewards extends Box<AlphaOperationMetadataAlphaBalance__Nonce_revelation_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Nonce_revelation_rewards {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Nonce_revelation_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Minted generated for AlphaOperationMetadataAlphaBalance__Minted
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Minted extends Box<AlphaOperationMetadataAlphaBalance__Minted> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Minted {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Minted>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Lost_endorsing_rewards generated for AlphaOperationMetadataAlphaBalance__Lost_endorsing_rewards
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Lost_endorsing_rewards extends Box<AlphaOperationMetadataAlphaBalance__Lost_endorsing_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'participation', 'revelation', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Lost_endorsing_rewards {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Lost_endorsing_rewards>({category: Unit.decode, delegate: CGRIDClass__AlphaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate.decode, participation: Bool.decode, revelation: Bool.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'participation', 'revelation', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.participation.encodeLength +  this.value.revelation.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.participation.writeTarget(tgt) +  this.value.revelation.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Liquidity_baking_subsidies generated for AlphaOperationMetadataAlphaBalance__Liquidity_baking_subsidies
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Liquidity_baking_subsidies extends Box<AlphaOperationMetadataAlphaBalance__Liquidity_baking_subsidies> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Liquidity_baking_subsidies {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Liquidity_baking_subsidies>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Invoice generated for AlphaOperationMetadataAlphaBalance__Invoice
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Invoice extends Box<AlphaOperationMetadataAlphaBalance__Invoice> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Invoice {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Invoice>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Initial_commitments generated for AlphaOperationMetadataAlphaBalance__Initial_commitments
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Initial_commitments extends Box<AlphaOperationMetadataAlphaBalance__Initial_commitments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Initial_commitments {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Initial_commitments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Frozen_bonds generated for AlphaOperationMetadataAlphaBalance__Frozen_bonds
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Frozen_bonds extends Box<AlphaOperationMetadataAlphaBalance__Frozen_bonds> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'contract', 'bond_id', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Frozen_bonds {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Frozen_bonds>({category: Unit.decode, contract: CGRIDClass__AlphaContract_id.decode, bond_id: CGRIDClass__AlphaBond_id.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'contract', 'bond_id', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.contract.encodeLength +  this.value.bond_id.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.contract.writeTarget(tgt) +  this.value.bond_id.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Endorsing_rewards generated for AlphaOperationMetadataAlphaBalance__Endorsing_rewards
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Endorsing_rewards extends Box<AlphaOperationMetadataAlphaBalance__Endorsing_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Endorsing_rewards {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Endorsing_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Double_signing_punishments generated for AlphaOperationMetadataAlphaBalance__Double_signing_punishments
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Double_signing_punishments extends Box<AlphaOperationMetadataAlphaBalance__Double_signing_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Double_signing_punishments {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Double_signing_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Double_signing_evidence_rewards generated for AlphaOperationMetadataAlphaBalance__Double_signing_evidence_rewards
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Double_signing_evidence_rewards extends Box<AlphaOperationMetadataAlphaBalance__Double_signing_evidence_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Double_signing_evidence_rewards {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Double_signing_evidence_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Deposits generated for AlphaOperationMetadataAlphaBalance__Deposits
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Deposits extends Box<AlphaOperationMetadataAlphaBalance__Deposits> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Deposits {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Deposits>({category: Unit.decode, delegate: CGRIDClass__AlphaOperation_metadataAlphaBalance_Deposits_delegate.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Contract generated for AlphaOperationMetadataAlphaBalance__Contract
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Contract extends Box<AlphaOperationMetadataAlphaBalance__Contract> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Contract {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Contract>({contract: CGRIDClass__AlphaContract_id.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['contract', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Commitments generated for AlphaOperationMetadataAlphaBalance__Commitments
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Commitments extends Box<AlphaOperationMetadataAlphaBalance__Commitments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'committer', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Commitments {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Commitments>({category: Unit.decode, committer: CGRIDClass__AlphaOperation_metadataAlphaBalance_Commitments_committer.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'committer', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.committer.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.committer.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Burned generated for AlphaOperationMetadataAlphaBalance__Burned
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Burned extends Box<AlphaOperationMetadataAlphaBalance__Burned> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Burned {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Burned>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Bootstrap generated for AlphaOperationMetadataAlphaBalance__Bootstrap
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Bootstrap extends Box<AlphaOperationMetadataAlphaBalance__Bootstrap> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Bootstrap {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Bootstrap>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Block_fees generated for AlphaOperationMetadataAlphaBalance__Block_fees
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Block_fees extends Box<AlphaOperationMetadataAlphaBalance__Block_fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Block_fees {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Block_fees>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Baking_rewards generated for AlphaOperationMetadataAlphaBalance__Baking_rewards
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Baking_rewards extends Box<AlphaOperationMetadataAlphaBalance__Baking_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Baking_rewards {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Baking_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperationMetadataAlphaBalance__Baking_bonuses generated for AlphaOperationMetadataAlphaBalance__Baking_bonuses
export class CGRIDClass__AlphaOperationMetadataAlphaBalance__Baking_bonuses extends Box<AlphaOperationMetadataAlphaBalance__Baking_bonuses> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationMetadataAlphaBalance__Baking_bonuses {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalance__Baking_bonuses>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContractId__Originated generated for AlphaContractId__Originated
export class CGRIDClass__AlphaContractId__Originated extends Box<AlphaContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaContractId__Originated {
        return new this(Padded.decode(CGRIDClass__AlphaContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaContractId__Implicit generated for AlphaContractId__Implicit
export class CGRIDClass__AlphaContractId__Implicit extends Box<AlphaContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContractId__Implicit {
        return new this(record_decoder<AlphaContractId__Implicit>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBondId__Tx_rollup_bond_id generated for AlphaBondId__Tx_rollup_bond_id
export class CGRIDClass__AlphaBondId__Tx_rollup_bond_id extends Box<AlphaBondId__Tx_rollup_bond_id> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['tx_rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBondId__Tx_rollup_bond_id {
        return new this(record_decoder<AlphaBondId__Tx_rollup_bond_id>({tx_rollup: CGRIDClass__AlphaTx_rollup_id.decode}, {order: ['tx_rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.tx_rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.tx_rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBondId__Smart_rollup_bond_id generated for AlphaBondId__Smart_rollup_bond_id
export class CGRIDClass__AlphaBondId__Smart_rollup_bond_id extends Box<AlphaBondId__Smart_rollup_bond_id> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBondId__Smart_rollup_bond_id {
        return new this(record_decoder<AlphaBondId__Smart_rollup_bond_id>({smart_rollup: CGRIDClass__AlphaSmart_rollup_address.decode}, {order: ['smart_rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Bls = { bls12_381_public_key_hash: FixedBytes<20> };
export type AlphaOperationMetadataAlphaUpdateOriginOrigin__Subsidy = Unit;
export type AlphaOperationMetadataAlphaUpdateOriginOrigin__Simulation = Unit;
export type AlphaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration = Unit;
export type AlphaOperationMetadataAlphaUpdateOriginOrigin__Block_application = Unit;
export type AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Storage_fees = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Nonce_revelation_rewards = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Minted = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Lost_endorsing_rewards = { category: Unit, delegate: CGRIDClass__AlphaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate, participation: Bool, revelation: Bool, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Liquidity_baking_subsidies = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Invoice = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Initial_commitments = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Frozen_bonds = { category: Unit, contract: CGRIDClass__AlphaContract_id, bond_id: CGRIDClass__AlphaBond_id, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Endorsing_rewards = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Double_signing_punishments = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Double_signing_evidence_rewards = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Deposits = { category: Unit, delegate: CGRIDClass__AlphaOperation_metadataAlphaBalance_Deposits_delegate, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Contract = { contract: CGRIDClass__AlphaContract_id, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Commitments = { category: Unit, committer: CGRIDClass__AlphaOperation_metadataAlphaBalance_Commitments_committer, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Burned = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Bootstrap = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Block_fees = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Baking_rewards = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaOperationMetadataAlphaBalance__Baking_bonuses = { category: Unit, change: Int64, origin: CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin };
export type AlphaContractId__Originated = Padded<CGRIDClass__AlphaContract_id_Originated_denest_pad,1>;
export type AlphaContractId__Implicit = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaBondId__Tx_rollup_bond_id = { tx_rollup: CGRIDClass__AlphaTx_rollup_id };
export type AlphaBondId__Smart_rollup_bond_id = { smart_rollup: CGRIDClass__AlphaSmart_rollup_address };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
            case CGRIDTag__PublicKeyHash.Bls: return CGRIDClass__PublicKeyHash__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaTx_rollup_id generated for AlphaTxRollupId
export class CGRIDClass__AlphaTx_rollup_id extends Box<AlphaTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaTx_rollup_id {
        return new this(record_decoder<AlphaTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaSmart_rollup_address generated for AlphaSmartRollupAddress
export class CGRIDClass__AlphaSmart_rollup_address extends Box<AlphaSmartRollupAddress> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaSmart_rollup_address {
        return new this(record_decoder<AlphaSmartRollupAddress>({smart_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['smart_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin generated for AlphaOperationMetadataAlphaUpdateOriginOrigin
export function alphaoperationmetadataalphaupdateoriginorigin_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin,AlphaOperationMetadataAlphaUpdateOriginOrigin> {
    function f(disc: CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin) {
        switch (disc) {
            case CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin.Block_application: return CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Block_application.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration: return CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin.Subsidy: return CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Subsidy.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin.Simulation: return CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Simulation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin => Object.values(CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin extends Box<AlphaOperationMetadataAlphaUpdateOriginOrigin> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationMetadataAlphaUpdateOriginOrigin>, AlphaOperationMetadataAlphaUpdateOriginOrigin>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperation_metadataAlphaUpdate_origin_origin {
        return new this(variant_decoder(width.Uint8)(alphaoperationmetadataalphaupdateoriginorigin_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate generated for AlphaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate
export class CGRIDClass__AlphaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate extends Box<AlphaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperation_metadataAlphaBalance_Deposits_delegate generated for AlphaOperationMetadataAlphaBalanceDepositsDelegate
export class CGRIDClass__AlphaOperation_metadataAlphaBalance_Deposits_delegate extends Box<AlphaOperationMetadataAlphaBalanceDepositsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperation_metadataAlphaBalance_Deposits_delegate {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalanceDepositsDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperation_metadataAlphaBalance_Commitments_committer generated for AlphaOperationMetadataAlphaBalanceCommitmentsCommitter
export class CGRIDClass__AlphaOperation_metadataAlphaBalance_Commitments_committer extends Box<AlphaOperationMetadataAlphaBalanceCommitmentsCommitter> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['blinded_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperation_metadataAlphaBalance_Commitments_committer {
        return new this(record_decoder<AlphaOperationMetadataAlphaBalanceCommitmentsCommitter>({blinded_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['blinded_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.blinded_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.blinded_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaOperation_metadataAlphaBalance generated for AlphaOperationMetadataAlphaBalance
export function alphaoperationmetadataalphabalance_mkDecoder(): VariantDecoder<CGRIDTag__AlphaOperationMetadataAlphaBalance,AlphaOperationMetadataAlphaBalance> {
    function f(disc: CGRIDTag__AlphaOperationMetadataAlphaBalance) {
        switch (disc) {
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Contract: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Contract.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Block_fees: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Block_fees.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Deposits: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Deposits.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Nonce_revelation_rewards: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Nonce_revelation_rewards.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Double_signing_evidence_rewards: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Double_signing_evidence_rewards.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Endorsing_rewards: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Endorsing_rewards.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Baking_rewards: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Baking_rewards.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Baking_bonuses: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Baking_bonuses.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Storage_fees: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Storage_fees.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Double_signing_punishments: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Double_signing_punishments.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Lost_endorsing_rewards: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Lost_endorsing_rewards.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Liquidity_baking_subsidies: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Liquidity_baking_subsidies.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Burned: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Burned.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Commitments: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Commitments.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Bootstrap: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Bootstrap.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Invoice: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Invoice.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Initial_commitments: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Initial_commitments.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Minted: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Minted.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Frozen_bonds: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Frozen_bonds.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Tx_rollup_rejection_rewards: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Tx_rollup_rejection_punishments: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Smart_rollup_refutation_punishments: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments.decode;
            case CGRIDTag__AlphaOperationMetadataAlphaBalance.Smart_rollup_refutation_rewards: return CGRIDClass__AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaOperationMetadataAlphaBalance => Object.values(CGRIDTag__AlphaOperationMetadataAlphaBalance).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaOperation_metadataAlphaBalance extends Box<AlphaOperationMetadataAlphaBalance> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaOperationMetadataAlphaBalance>, AlphaOperationMetadataAlphaBalance>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperation_metadataAlphaBalance {
        return new this(variant_decoder(width.Uint8)(alphaoperationmetadataalphabalance_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContract_id_Originated_denest_pad generated for AlphaContractIdOriginatedDenestPad
export class CGRIDClass__AlphaContract_id_Originated_denest_pad extends Box<AlphaContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContract_id_Originated_denest_pad {
        return new this(record_decoder<AlphaContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaContract_id generated for AlphaContractId
export function alphacontractid_mkDecoder(): VariantDecoder<CGRIDTag__AlphaContractId,AlphaContractId> {
    function f(disc: CGRIDTag__AlphaContractId) {
        switch (disc) {
            case CGRIDTag__AlphaContractId.Implicit: return CGRIDClass__AlphaContractId__Implicit.decode;
            case CGRIDTag__AlphaContractId.Originated: return CGRIDClass__AlphaContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaContractId => Object.values(CGRIDTag__AlphaContractId).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaContract_id extends Box<AlphaContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaContractId>, AlphaContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaContract_id {
        return new this(variant_decoder(width.Uint8)(alphacontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaBond_id generated for AlphaBondId
export function alphabondid_mkDecoder(): VariantDecoder<CGRIDTag__AlphaBondId,AlphaBondId> {
    function f(disc: CGRIDTag__AlphaBondId) {
        switch (disc) {
            case CGRIDTag__AlphaBondId.Tx_rollup_bond_id: return CGRIDClass__AlphaBondId__Tx_rollup_bond_id.decode;
            case CGRIDTag__AlphaBondId.Smart_rollup_bond_id: return CGRIDClass__AlphaBondId__Smart_rollup_bond_id.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaBondId => Object.values(CGRIDTag__AlphaBondId).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaBond_id extends Box<AlphaBondId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaBondId>, AlphaBondId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaBond_id {
        return new this(variant_decoder(width.Uint8)(alphabondid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256,
    Bls: CGRIDClass__PublicKeyHash__Bls
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] } | { kind: CGRIDTag__PublicKeyHash.Bls, value: CGRIDMap__PublicKeyHash['Bls'] };
export type AlphaTxRollupId = { rollup_hash: FixedBytes<20> };
export type AlphaSmartRollupAddress = { smart_rollup_hash: FixedBytes<20> };
export enum CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin{
    Block_application = 0,
    Protocol_migration = 1,
    Subsidy = 2,
    Simulation = 3
}
export interface CGRIDMap__AlphaOperationMetadataAlphaUpdateOriginOrigin {
    Block_application: CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Block_application,
    Protocol_migration: CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration,
    Subsidy: CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Subsidy,
    Simulation: CGRIDClass__AlphaOperationMetadataAlphaUpdateOriginOrigin__Simulation
}
export type AlphaOperationMetadataAlphaUpdateOriginOrigin = { kind: CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin.Block_application, value: CGRIDMap__AlphaOperationMetadataAlphaUpdateOriginOrigin['Block_application'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration, value: CGRIDMap__AlphaOperationMetadataAlphaUpdateOriginOrigin['Protocol_migration'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin.Subsidy, value: CGRIDMap__AlphaOperationMetadataAlphaUpdateOriginOrigin['Subsidy'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaUpdateOriginOrigin.Simulation, value: CGRIDMap__AlphaOperationMetadataAlphaUpdateOriginOrigin['Simulation'] };
export type AlphaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationMetadataAlphaBalanceDepositsDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationMetadataAlphaBalanceCommitmentsCommitter = { blinded_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__AlphaOperationMetadataAlphaBalance{
    Contract = 0,
    Block_fees = 2,
    Deposits = 4,
    Nonce_revelation_rewards = 5,
    Double_signing_evidence_rewards = 6,
    Endorsing_rewards = 7,
    Baking_rewards = 8,
    Baking_bonuses = 9,
    Storage_fees = 11,
    Double_signing_punishments = 12,
    Lost_endorsing_rewards = 13,
    Liquidity_baking_subsidies = 14,
    Burned = 15,
    Commitments = 16,
    Bootstrap = 17,
    Invoice = 18,
    Initial_commitments = 19,
    Minted = 20,
    Frozen_bonds = 21,
    Tx_rollup_rejection_rewards = 22,
    Tx_rollup_rejection_punishments = 23,
    Smart_rollup_refutation_punishments = 24,
    Smart_rollup_refutation_rewards = 25
}
export interface CGRIDMap__AlphaOperationMetadataAlphaBalance {
    Contract: CGRIDClass__AlphaOperationMetadataAlphaBalance__Contract,
    Block_fees: CGRIDClass__AlphaOperationMetadataAlphaBalance__Block_fees,
    Deposits: CGRIDClass__AlphaOperationMetadataAlphaBalance__Deposits,
    Nonce_revelation_rewards: CGRIDClass__AlphaOperationMetadataAlphaBalance__Nonce_revelation_rewards,
    Double_signing_evidence_rewards: CGRIDClass__AlphaOperationMetadataAlphaBalance__Double_signing_evidence_rewards,
    Endorsing_rewards: CGRIDClass__AlphaOperationMetadataAlphaBalance__Endorsing_rewards,
    Baking_rewards: CGRIDClass__AlphaOperationMetadataAlphaBalance__Baking_rewards,
    Baking_bonuses: CGRIDClass__AlphaOperationMetadataAlphaBalance__Baking_bonuses,
    Storage_fees: CGRIDClass__AlphaOperationMetadataAlphaBalance__Storage_fees,
    Double_signing_punishments: CGRIDClass__AlphaOperationMetadataAlphaBalance__Double_signing_punishments,
    Lost_endorsing_rewards: CGRIDClass__AlphaOperationMetadataAlphaBalance__Lost_endorsing_rewards,
    Liquidity_baking_subsidies: CGRIDClass__AlphaOperationMetadataAlphaBalance__Liquidity_baking_subsidies,
    Burned: CGRIDClass__AlphaOperationMetadataAlphaBalance__Burned,
    Commitments: CGRIDClass__AlphaOperationMetadataAlphaBalance__Commitments,
    Bootstrap: CGRIDClass__AlphaOperationMetadataAlphaBalance__Bootstrap,
    Invoice: CGRIDClass__AlphaOperationMetadataAlphaBalance__Invoice,
    Initial_commitments: CGRIDClass__AlphaOperationMetadataAlphaBalance__Initial_commitments,
    Minted: CGRIDClass__AlphaOperationMetadataAlphaBalance__Minted,
    Frozen_bonds: CGRIDClass__AlphaOperationMetadataAlphaBalance__Frozen_bonds,
    Tx_rollup_rejection_rewards: CGRIDClass__AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards,
    Tx_rollup_rejection_punishments: CGRIDClass__AlphaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments,
    Smart_rollup_refutation_punishments: CGRIDClass__AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments,
    Smart_rollup_refutation_rewards: CGRIDClass__AlphaOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards
}
export type AlphaOperationMetadataAlphaBalance = { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Contract, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Contract'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Block_fees, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Block_fees'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Deposits, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Deposits'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Nonce_revelation_rewards, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Nonce_revelation_rewards'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Double_signing_evidence_rewards, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Double_signing_evidence_rewards'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Endorsing_rewards, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Endorsing_rewards'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Baking_rewards, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Baking_rewards'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Baking_bonuses, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Baking_bonuses'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Storage_fees, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Storage_fees'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Double_signing_punishments, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Double_signing_punishments'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Lost_endorsing_rewards, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Lost_endorsing_rewards'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Liquidity_baking_subsidies, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Liquidity_baking_subsidies'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Burned, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Burned'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Commitments, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Commitments'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Bootstrap, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Bootstrap'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Invoice, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Invoice'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Initial_commitments, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Initial_commitments'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Minted, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Minted'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Frozen_bonds, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Frozen_bonds'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Tx_rollup_rejection_rewards, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Tx_rollup_rejection_rewards'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Tx_rollup_rejection_punishments, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Tx_rollup_rejection_punishments'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Smart_rollup_refutation_punishments, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Smart_rollup_refutation_punishments'] } | { kind: CGRIDTag__AlphaOperationMetadataAlphaBalance.Smart_rollup_refutation_rewards, value: CGRIDMap__AlphaOperationMetadataAlphaBalance['Smart_rollup_refutation_rewards'] };
export type AlphaContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__AlphaContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__AlphaContractId {
    Implicit: CGRIDClass__AlphaContractId__Implicit,
    Originated: CGRIDClass__AlphaContractId__Originated
}
export type AlphaContractId = { kind: CGRIDTag__AlphaContractId.Implicit, value: CGRIDMap__AlphaContractId['Implicit'] } | { kind: CGRIDTag__AlphaContractId.Originated, value: CGRIDMap__AlphaContractId['Originated'] };
export enum CGRIDTag__AlphaBondId{
    Tx_rollup_bond_id = 0,
    Smart_rollup_bond_id = 1
}
export interface CGRIDMap__AlphaBondId {
    Tx_rollup_bond_id: CGRIDClass__AlphaBondId__Tx_rollup_bond_id,
    Smart_rollup_bond_id: CGRIDClass__AlphaBondId__Smart_rollup_bond_id
}
export type AlphaBondId = { kind: CGRIDTag__AlphaBondId.Tx_rollup_bond_id, value: CGRIDMap__AlphaBondId['Tx_rollup_bond_id'] } | { kind: CGRIDTag__AlphaBondId.Smart_rollup_bond_id, value: CGRIDMap__AlphaBondId['Smart_rollup_bond_id'] };
export type AlphaReceiptBalanceUpdates = Dynamic<Sequence<CGRIDClass__AlphaOperation_metadataAlphaBalance>,width.Uint30>;
export class CGRIDClass__AlphaReceiptBalanceUpdates extends Box<AlphaReceiptBalanceUpdates> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaReceiptBalanceUpdates {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperation_metadataAlphaBalance.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const alpha_receipt_balance_updates_encoder = (value: AlphaReceiptBalanceUpdates): OutputBytes => {
    return value.encode();
}
export const alpha_receipt_balance_updates_decoder = (p: Parser): AlphaReceiptBalanceUpdates => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__AlphaOperation_metadataAlphaBalance.decode), width.Uint30)(p);
}
