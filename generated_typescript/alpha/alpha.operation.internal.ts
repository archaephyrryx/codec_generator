import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Bls generated for PublicKeyHash__Bls
export class CGRIDClass__PublicKeyHash__Bls extends Box<PublicKeyHash__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Bls {
        return new this(record_decoder<PublicKeyHash__Bls>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__String generated for MichelineAlphaMichelsonV1Expression__String
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__String extends Box<MichelineAlphaMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__String {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence generated for MichelineAlphaMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence extends Box<MichelineAlphaMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineAlphaMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic generated for MichelineAlphaMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic extends Box<MichelineAlphaMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineAlphaMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__AlphaMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Int generated for MichelineAlphaMichelsonV1Expression__Int
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Int extends Box<MichelineAlphaMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes generated for MichelineAlphaMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes extends Box<MichelineAlphaMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineAlphaMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaTransactionDestination__Zk_rollup generated for AlphaTransactionDestination__Zk_rollup
export class CGRIDClass__AlphaTransactionDestination__Zk_rollup extends Box<AlphaTransactionDestination__Zk_rollup> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaTransactionDestination__Zk_rollup {
        return new this(Padded.decode(CGRIDClass__AlphaTransaction_destination_Zk_rollup_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaTransactionDestination__Tx_rollup generated for AlphaTransactionDestination__Tx_rollup
export class CGRIDClass__AlphaTransactionDestination__Tx_rollup extends Box<AlphaTransactionDestination__Tx_rollup> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaTransactionDestination__Tx_rollup {
        return new this(Padded.decode(CGRIDClass__AlphaTx_rollup_id.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaTransactionDestination__Smart_rollup generated for AlphaTransactionDestination__Smart_rollup
export class CGRIDClass__AlphaTransactionDestination__Smart_rollup extends Box<AlphaTransactionDestination__Smart_rollup> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaTransactionDestination__Smart_rollup {
        return new this(Padded.decode(CGRIDClass__AlphaTransaction_destination_Smart_rollup_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaTransactionDestination__Originated generated for AlphaTransactionDestination__Originated
export class CGRIDClass__AlphaTransactionDestination__Originated extends Box<AlphaTransactionDestination__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaTransactionDestination__Originated {
        return new this(Padded.decode(CGRIDClass__AlphaTransaction_destination_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaTransactionDestination__Implicit generated for AlphaTransactionDestination__Implicit
export class CGRIDClass__AlphaTransactionDestination__Implicit extends Box<AlphaTransactionDestination__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaTransactionDestination__Implicit {
        return new this(record_decoder<AlphaTransactionDestination__Implicit>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaEntrypoint__set_delegate generated for AlphaEntrypoint__set_delegate
export class CGRIDClass__AlphaEntrypoint__set_delegate extends Box<AlphaEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__root generated for AlphaEntrypoint__root
export class CGRIDClass__AlphaEntrypoint__root extends Box<AlphaEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__remove_delegate generated for AlphaEntrypoint__remove_delegate
export class CGRIDClass__AlphaEntrypoint__remove_delegate extends Box<AlphaEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__named generated for AlphaEntrypoint__named
export class CGRIDClass__AlphaEntrypoint__named extends Box<AlphaEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint__deposit generated for AlphaEntrypoint__deposit
export class CGRIDClass__AlphaEntrypoint__deposit extends Box<AlphaEntrypoint__deposit> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint__deposit {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint___do generated for AlphaEntrypoint___do
export class CGRIDClass__AlphaEntrypoint___do extends Box<AlphaEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint___default generated for AlphaEntrypoint___default
export class CGRIDClass__AlphaEntrypoint___default extends Box<AlphaEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Transaction generated for AlphaApplyInternalResultsAlphaOperationResultRhs__Transaction
export class CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Transaction extends Box<AlphaApplyInternalResultsAlphaOperationResultRhs__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Transaction {
        return new this(record_decoder<AlphaApplyInternalResultsAlphaOperationResultRhs__Transaction>({amount: N.decode, destination: CGRIDClass__AlphaTransaction_destination.decode, parameters: Option.decode(CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Transaction_parameters.decode)}, {order: ['amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Origination generated for AlphaApplyInternalResultsAlphaOperationResultRhs__Origination
export class CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Origination extends Box<AlphaApplyInternalResultsAlphaOperationResultRhs__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Origination {
        return new this(record_decoder<AlphaApplyInternalResultsAlphaOperationResultRhs__Origination>({balance: N.decode, delegate: Option.decode(CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Origination_delegate.decode), script: CGRIDClass__AlphaScriptedContracts.decode}, {order: ['balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Event generated for AlphaApplyInternalResultsAlphaOperationResultRhs__Event
export class CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Event extends Box<AlphaApplyInternalResultsAlphaOperationResultRhs__Event> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_type', 'tag', 'payload']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Event {
        return new this(record_decoder<AlphaApplyInternalResultsAlphaOperationResultRhs__Event>({_type: CGRIDClass__MichelineAlphaMichelson_v1Expression.decode, tag: Option.decode(CGRIDClass__AlphaEntrypoint.decode), payload: Option.decode(CGRIDClass__MichelineAlphaMichelson_v1Expression.decode)}, {order: ['_type', 'tag', 'payload']})(p));
    };
    get encodeLength(): number {
        return (this.value._type.encodeLength +  this.value.tag.encodeLength +  this.value.payload.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._type.writeTarget(tgt) +  this.value.tag.writeTarget(tgt) +  this.value.payload.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Delegation generated for AlphaApplyInternalResultsAlphaOperationResultRhs__Delegation
export class CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Delegation extends Box<AlphaApplyInternalResultsAlphaOperationResultRhs__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Delegation {
        return new this(record_decoder<AlphaApplyInternalResultsAlphaOperationResultRhs__Delegation>({delegate: Option.decode(CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Delegation_delegate.decode)}, {order: ['delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.delegate.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Bls = { bls12_381_public_key_hash: FixedBytes<20> };
export type MichelineAlphaMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineAlphaMichelson_v1Expression>,width.Uint30>;
export type MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives };
export type MichelineAlphaMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__AlphaMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineAlphaMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg1: CGRIDClass__MichelineAlphaMichelson_v1Expression, arg2: CGRIDClass__MichelineAlphaMichelson_v1Expression };
export type MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__AlphaMichelsonV1Primitives, arg: CGRIDClass__MichelineAlphaMichelson_v1Expression };
export type MichelineAlphaMichelsonV1Expression__Int = { int: Z };
export type MichelineAlphaMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
export type AlphaTransactionDestination__Zk_rollup = Padded<CGRIDClass__AlphaTransaction_destination_Zk_rollup_denest_pad,1>;
export type AlphaTransactionDestination__Tx_rollup = Padded<CGRIDClass__AlphaTx_rollup_id,1>;
export type AlphaTransactionDestination__Smart_rollup = Padded<CGRIDClass__AlphaTransaction_destination_Smart_rollup_denest_pad,1>;
export type AlphaTransactionDestination__Originated = Padded<CGRIDClass__AlphaTransaction_destination_Originated_denest_pad,1>;
export type AlphaTransactionDestination__Implicit = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaEntrypoint__set_delegate = Unit;
export type AlphaEntrypoint__root = Unit;
export type AlphaEntrypoint__remove_delegate = Unit;
export type AlphaEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type AlphaEntrypoint__deposit = Unit;
export type AlphaEntrypoint___do = Unit;
export type AlphaEntrypoint___default = Unit;
export type AlphaApplyInternalResultsAlphaOperationResultRhs__Transaction = { amount: N, destination: CGRIDClass__AlphaTransaction_destination, parameters: Option<CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Transaction_parameters> };
export type AlphaApplyInternalResultsAlphaOperationResultRhs__Origination = { balance: N, delegate: Option<CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Origination_delegate>, script: CGRIDClass__AlphaScriptedContracts };
export type AlphaApplyInternalResultsAlphaOperationResultRhs__Event = { _type: CGRIDClass__MichelineAlphaMichelson_v1Expression, tag: Option<CGRIDClass__AlphaEntrypoint>, payload: Option<CGRIDClass__MichelineAlphaMichelson_v1Expression> };
export type AlphaApplyInternalResultsAlphaOperationResultRhs__Delegation = { delegate: Option<CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Delegation_delegate> };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
            case CGRIDTag__PublicKeyHash.Bls: return CGRIDClass__PublicKeyHash__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineAlphaMichelson_v1Expression generated for MichelineAlphaMichelsonV1Expression
export function michelinealphamichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineAlphaMichelsonV1Expression,MichelineAlphaMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineAlphaMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Int: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.String: return CGRIDClass__MichelineAlphaMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Sequence: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineAlphaMichelsonV1Expression.Bytes: return CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineAlphaMichelsonV1Expression => Object.values(CGRIDTag__MichelineAlphaMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineAlphaMichelson_v1Expression extends Box<MichelineAlphaMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineAlphaMichelsonV1Expression>, MichelineAlphaMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineAlphaMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelinealphamichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaTx_rollup_id generated for AlphaTxRollupId
export class CGRIDClass__AlphaTx_rollup_id extends Box<AlphaTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaTx_rollup_id {
        return new this(record_decoder<AlphaTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaTransaction_destination_Zk_rollup_denest_pad generated for AlphaTransactionDestinationZkRollupDenestPad
export class CGRIDClass__AlphaTransaction_destination_Zk_rollup_denest_pad extends Box<AlphaTransactionDestinationZkRollupDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['zk_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaTransaction_destination_Zk_rollup_denest_pad {
        return new this(record_decoder<AlphaTransactionDestinationZkRollupDenestPad>({zk_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['zk_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.zk_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.zk_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaTransaction_destination_Smart_rollup_denest_pad generated for AlphaTransactionDestinationSmartRollupDenestPad
export class CGRIDClass__AlphaTransaction_destination_Smart_rollup_denest_pad extends Box<AlphaTransactionDestinationSmartRollupDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaTransaction_destination_Smart_rollup_denest_pad {
        return new this(record_decoder<AlphaTransactionDestinationSmartRollupDenestPad>({smart_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['smart_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaTransaction_destination_Originated_denest_pad generated for AlphaTransactionDestinationOriginatedDenestPad
export class CGRIDClass__AlphaTransaction_destination_Originated_denest_pad extends Box<AlphaTransactionDestinationOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaTransaction_destination_Originated_denest_pad {
        return new this(record_decoder<AlphaTransactionDestinationOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaTransaction_destination generated for AlphaTransactionDestination
export function alphatransactiondestination_mkDecoder(): VariantDecoder<CGRIDTag__AlphaTransactionDestination,AlphaTransactionDestination> {
    function f(disc: CGRIDTag__AlphaTransactionDestination) {
        switch (disc) {
            case CGRIDTag__AlphaTransactionDestination.Implicit: return CGRIDClass__AlphaTransactionDestination__Implicit.decode;
            case CGRIDTag__AlphaTransactionDestination.Originated: return CGRIDClass__AlphaTransactionDestination__Originated.decode;
            case CGRIDTag__AlphaTransactionDestination.Tx_rollup: return CGRIDClass__AlphaTransactionDestination__Tx_rollup.decode;
            case CGRIDTag__AlphaTransactionDestination.Smart_rollup: return CGRIDClass__AlphaTransactionDestination__Smart_rollup.decode;
            case CGRIDTag__AlphaTransactionDestination.Zk_rollup: return CGRIDClass__AlphaTransactionDestination__Zk_rollup.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaTransactionDestination => Object.values(CGRIDTag__AlphaTransactionDestination).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaTransaction_destination extends Box<AlphaTransactionDestination> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaTransactionDestination>, AlphaTransactionDestination>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaTransaction_destination {
        return new this(variant_decoder(width.Uint8)(alphatransactiondestination_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaScriptedContracts generated for AlphaScriptedContracts
export class CGRIDClass__AlphaScriptedContracts extends Box<AlphaScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaScriptedContracts {
        return new this(record_decoder<AlphaScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaMichelsonV1Primitives generated for AlphaMichelsonV1Primitives
export class CGRIDClass__AlphaMichelsonV1Primitives extends Box<AlphaMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<AlphaMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is AlphaMichelsonV1Primitives => (Object.values(AlphaMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaEntrypoint generated for AlphaEntrypoint
export function alphaentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__AlphaEntrypoint,AlphaEntrypoint> {
    function f(disc: CGRIDTag__AlphaEntrypoint) {
        switch (disc) {
            case CGRIDTag__AlphaEntrypoint._default: return CGRIDClass__AlphaEntrypoint___default.decode;
            case CGRIDTag__AlphaEntrypoint.root: return CGRIDClass__AlphaEntrypoint__root.decode;
            case CGRIDTag__AlphaEntrypoint._do: return CGRIDClass__AlphaEntrypoint___do.decode;
            case CGRIDTag__AlphaEntrypoint.set_delegate: return CGRIDClass__AlphaEntrypoint__set_delegate.decode;
            case CGRIDTag__AlphaEntrypoint.remove_delegate: return CGRIDClass__AlphaEntrypoint__remove_delegate.decode;
            case CGRIDTag__AlphaEntrypoint.deposit: return CGRIDClass__AlphaEntrypoint__deposit.decode;
            case CGRIDTag__AlphaEntrypoint.named: return CGRIDClass__AlphaEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaEntrypoint => Object.values(CGRIDTag__AlphaEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaEntrypoint extends Box<AlphaEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaEntrypoint>, AlphaEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaEntrypoint {
        return new this(variant_decoder(width.Uint8)(alphaentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_rhs generated for AlphaApplyInternalResultsAlphaOperationResultRhs
export function alphaapplyinternalresultsalphaoperationresultrhs_mkDecoder(): VariantDecoder<CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs,AlphaApplyInternalResultsAlphaOperationResultRhs> {
    function f(disc: CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs) {
        switch (disc) {
            case CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs.Transaction: return CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Transaction.decode;
            case CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs.Origination: return CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Origination.decode;
            case CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs.Delegation: return CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Delegation.decode;
            case CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs.Event: return CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Event.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs => Object.values(CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_rhs extends Box<AlphaApplyInternalResultsAlphaOperationResultRhs> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaApplyInternalResultsAlphaOperationResultRhs>, AlphaApplyInternalResultsAlphaOperationResultRhs>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_rhs {
        return new this(variant_decoder(width.Uint8)(alphaapplyinternalresultsalphaoperationresultrhs_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Transaction_parameters generated for AlphaApplyInternalResultsAlphaOperationResultTransactionParameters
export class CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Transaction_parameters extends Box<AlphaApplyInternalResultsAlphaOperationResultTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Transaction_parameters {
        return new this(record_decoder<AlphaApplyInternalResultsAlphaOperationResultTransactionParameters>({entrypoint: CGRIDClass__AlphaEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Origination_delegate generated for AlphaApplyInternalResultsAlphaOperationResultOriginationDelegate
export class CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Origination_delegate extends Box<AlphaApplyInternalResultsAlphaOperationResultOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Origination_delegate {
        return new this(record_decoder<AlphaApplyInternalResultsAlphaOperationResultOriginationDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Delegation_delegate generated for AlphaApplyInternalResultsAlphaOperationResultDelegationDelegate
export class CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Delegation_delegate extends Box<AlphaApplyInternalResultsAlphaOperationResultDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_Delegation_delegate {
        return new this(record_decoder<AlphaApplyInternalResultsAlphaOperationResultDelegationDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256,
    Bls: CGRIDClass__PublicKeyHash__Bls
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] } | { kind: CGRIDTag__PublicKeyHash.Bls, value: CGRIDMap__PublicKeyHash['Bls'] };
export enum CGRIDTag__MichelineAlphaMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineAlphaMichelsonV1Expression {
    Int: CGRIDClass__MichelineAlphaMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineAlphaMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineAlphaMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineAlphaMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineAlphaMichelsonV1Expression__Bytes
}
export type MichelineAlphaMichelsonV1Expression = { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Int, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.String, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineAlphaMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineAlphaMichelsonV1Expression['Bytes'] };
export type AlphaTxRollupId = { rollup_hash: FixedBytes<20> };
export type AlphaTransactionDestinationZkRollupDenestPad = { zk_rollup_hash: FixedBytes<20> };
export type AlphaTransactionDestinationSmartRollupDenestPad = { smart_rollup_hash: FixedBytes<20> };
export type AlphaTransactionDestinationOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__AlphaTransactionDestination{
    Implicit = 0,
    Originated = 1,
    Tx_rollup = 2,
    Smart_rollup = 3,
    Zk_rollup = 4
}
export interface CGRIDMap__AlphaTransactionDestination {
    Implicit: CGRIDClass__AlphaTransactionDestination__Implicit,
    Originated: CGRIDClass__AlphaTransactionDestination__Originated,
    Tx_rollup: CGRIDClass__AlphaTransactionDestination__Tx_rollup,
    Smart_rollup: CGRIDClass__AlphaTransactionDestination__Smart_rollup,
    Zk_rollup: CGRIDClass__AlphaTransactionDestination__Zk_rollup
}
export type AlphaTransactionDestination = { kind: CGRIDTag__AlphaTransactionDestination.Implicit, value: CGRIDMap__AlphaTransactionDestination['Implicit'] } | { kind: CGRIDTag__AlphaTransactionDestination.Originated, value: CGRIDMap__AlphaTransactionDestination['Originated'] } | { kind: CGRIDTag__AlphaTransactionDestination.Tx_rollup, value: CGRIDMap__AlphaTransactionDestination['Tx_rollup'] } | { kind: CGRIDTag__AlphaTransactionDestination.Smart_rollup, value: CGRIDMap__AlphaTransactionDestination['Smart_rollup'] } | { kind: CGRIDTag__AlphaTransactionDestination.Zk_rollup, value: CGRIDMap__AlphaTransactionDestination['Zk_rollup'] };
export type AlphaScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export enum AlphaMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117,
    LEVEL = 118,
    SELF_ADDRESS = 119,
    never = 120,
    NEVER = 121,
    UNPAIR = 122,
    VOTING_POWER = 123,
    TOTAL_VOTING_POWER = 124,
    KECCAK = 125,
    SHA3 = 126,
    PAIRING_CHECK = 127,
    bls12_381_g1 = 128,
    bls12_381_g2 = 129,
    bls12_381_fr = 130,
    sapling_state = 131,
    sapling_transaction_deprecated = 132,
    SAPLING_EMPTY_STATE = 133,
    SAPLING_VERIFY_UPDATE = 134,
    ticket = 135,
    TICKET_DEPRECATED = 136,
    READ_TICKET = 137,
    SPLIT_TICKET = 138,
    JOIN_TICKETS = 139,
    GET_AND_UPDATE = 140,
    chest = 141,
    chest_key = 142,
    OPEN_CHEST = 143,
    VIEW = 144,
    view = 145,
    constant = 146,
    SUB_MUTEZ = 147,
    tx_rollup_l2_address = 148,
    MIN_BLOCK_TIME = 149,
    sapling_transaction = 150,
    EMIT = 151,
    Lambda_rec = 152,
    LAMBDA_REC = 153,
    TICKET = 154,
    BYTES = 155,
    NAT = 156
}
export enum CGRIDTag__AlphaEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    deposit = 5,
    named = 255
}
export interface CGRIDMap__AlphaEntrypoint {
    _default: CGRIDClass__AlphaEntrypoint___default,
    root: CGRIDClass__AlphaEntrypoint__root,
    _do: CGRIDClass__AlphaEntrypoint___do,
    set_delegate: CGRIDClass__AlphaEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__AlphaEntrypoint__remove_delegate,
    deposit: CGRIDClass__AlphaEntrypoint__deposit,
    named: CGRIDClass__AlphaEntrypoint__named
}
export type AlphaEntrypoint = { kind: CGRIDTag__AlphaEntrypoint._default, value: CGRIDMap__AlphaEntrypoint['_default'] } | { kind: CGRIDTag__AlphaEntrypoint.root, value: CGRIDMap__AlphaEntrypoint['root'] } | { kind: CGRIDTag__AlphaEntrypoint._do, value: CGRIDMap__AlphaEntrypoint['_do'] } | { kind: CGRIDTag__AlphaEntrypoint.set_delegate, value: CGRIDMap__AlphaEntrypoint['set_delegate'] } | { kind: CGRIDTag__AlphaEntrypoint.remove_delegate, value: CGRIDMap__AlphaEntrypoint['remove_delegate'] } | { kind: CGRIDTag__AlphaEntrypoint.deposit, value: CGRIDMap__AlphaEntrypoint['deposit'] } | { kind: CGRIDTag__AlphaEntrypoint.named, value: CGRIDMap__AlphaEntrypoint['named'] };
export enum CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs{
    Transaction = 1,
    Origination = 2,
    Delegation = 3,
    Event = 4
}
export interface CGRIDMap__AlphaApplyInternalResultsAlphaOperationResultRhs {
    Transaction: CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Transaction,
    Origination: CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Origination,
    Delegation: CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Delegation,
    Event: CGRIDClass__AlphaApplyInternalResultsAlphaOperationResultRhs__Event
}
export type AlphaApplyInternalResultsAlphaOperationResultRhs = { kind: CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs.Transaction, value: CGRIDMap__AlphaApplyInternalResultsAlphaOperationResultRhs['Transaction'] } | { kind: CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs.Origination, value: CGRIDMap__AlphaApplyInternalResultsAlphaOperationResultRhs['Origination'] } | { kind: CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs.Delegation, value: CGRIDMap__AlphaApplyInternalResultsAlphaOperationResultRhs['Delegation'] } | { kind: CGRIDTag__AlphaApplyInternalResultsAlphaOperationResultRhs.Event, value: CGRIDMap__AlphaApplyInternalResultsAlphaOperationResultRhs['Event'] };
export type AlphaApplyInternalResultsAlphaOperationResultTransactionParameters = { entrypoint: CGRIDClass__AlphaEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type AlphaApplyInternalResultsAlphaOperationResultOriginationDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaApplyInternalResultsAlphaOperationResultDelegationDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type AlphaOperationInternal = { source: CGRIDClass__AlphaTransaction_destination, nonce: Uint16, alpha_apply_internal_results_alpha_operation_result_rhs: CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_rhs };
export class CGRIDClass__AlphaOperationInternal extends Box<AlphaOperationInternal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'nonce', 'alpha_apply_internal_results_alpha_operation_result_rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaOperationInternal {
        return new this(record_decoder<AlphaOperationInternal>({source: CGRIDClass__AlphaTransaction_destination.decode, nonce: Uint16.decode, alpha_apply_internal_results_alpha_operation_result_rhs: CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_rhs.decode}, {order: ['source', 'nonce', 'alpha_apply_internal_results_alpha_operation_result_rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.nonce.encodeLength +  this.value.alpha_apply_internal_results_alpha_operation_result_rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt) +  this.value.alpha_apply_internal_results_alpha_operation_result_rhs.writeTarget(tgt));
    }
}
export const alpha_operation_internal_encoder = (value: AlphaOperationInternal): OutputBytes => {
    return record_encoder({order: ['source', 'nonce', 'alpha_apply_internal_results_alpha_operation_result_rhs']})(value);
}
export const alpha_operation_internal_decoder = (p: Parser): AlphaOperationInternal => {
    return record_decoder<AlphaOperationInternal>({source: CGRIDClass__AlphaTransaction_destination.decode, nonce: Uint16.decode, alpha_apply_internal_results_alpha_operation_result_rhs: CGRIDClass__AlphaApply_internal_resultsAlphaOperation_result_rhs.decode}, {order: ['source', 'nonce', 'alpha_apply_internal_results_alpha_operation_result_rhs']})(p);
}
