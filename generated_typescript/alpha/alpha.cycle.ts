import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type AlphaCycle = Int32;
export class CGRIDClass__AlphaCycle extends Box<AlphaCycle> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaCycle {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const alpha_cycle_encoder = (value: AlphaCycle): OutputBytes => {
    return value.encode();
}
export const alpha_cycle_decoder = (p: Parser): AlphaCycle => {
    return Int32.decode(p);
}
