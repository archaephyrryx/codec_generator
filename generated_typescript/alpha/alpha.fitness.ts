import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__AlphaFitnessLockedRound__Some generated for AlphaFitnessLockedRound__Some
export class CGRIDClass__AlphaFitnessLockedRound__Some extends Box<AlphaFitnessLockedRound__Some> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaFitnessLockedRound__Some {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaFitnessLockedRound__None generated for AlphaFitnessLockedRound__None
export class CGRIDClass__AlphaFitnessLockedRound__None extends Box<AlphaFitnessLockedRound__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaFitnessLockedRound__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type AlphaFitnessLockedRound__Some = Int32;
export type AlphaFitnessLockedRound__None = Unit;
// Class CGRIDClass__AlphaFitness_locked_round generated for AlphaFitnessLockedRound
export function alphafitnesslockedround_mkDecoder(): VariantDecoder<CGRIDTag__AlphaFitnessLockedRound,AlphaFitnessLockedRound> {
    function f(disc: CGRIDTag__AlphaFitnessLockedRound) {
        switch (disc) {
            case CGRIDTag__AlphaFitnessLockedRound.None: return CGRIDClass__AlphaFitnessLockedRound__None.decode;
            case CGRIDTag__AlphaFitnessLockedRound.Some: return CGRIDClass__AlphaFitnessLockedRound__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaFitnessLockedRound => Object.values(CGRIDTag__AlphaFitnessLockedRound).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaFitness_locked_round extends Box<AlphaFitnessLockedRound> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaFitnessLockedRound>, AlphaFitnessLockedRound>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaFitness_locked_round {
        return new this(variant_decoder(width.Uint8)(alphafitnesslockedround_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__AlphaFitnessLockedRound{
    None = 0,
    Some = 1
}
export interface CGRIDMap__AlphaFitnessLockedRound {
    None: CGRIDClass__AlphaFitnessLockedRound__None,
    Some: CGRIDClass__AlphaFitnessLockedRound__Some
}
export type AlphaFitnessLockedRound = { kind: CGRIDTag__AlphaFitnessLockedRound.None, value: CGRIDMap__AlphaFitnessLockedRound['None'] } | { kind: CGRIDTag__AlphaFitnessLockedRound.Some, value: CGRIDMap__AlphaFitnessLockedRound['Some'] };
export type AlphaFitness = { level: Int32, locked_round: CGRIDClass__AlphaFitness_locked_round, predecessor_round: Int32, round: Int32 };
export class CGRIDClass__AlphaFitness extends Box<AlphaFitness> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaFitness {
        return new this(record_decoder<AlphaFitness>({level: Int32.decode, locked_round: CGRIDClass__AlphaFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.locked_round.encodeLength +  this.value.predecessor_round.encodeLength +  this.value.round.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.locked_round.writeTarget(tgt) +  this.value.predecessor_round.writeTarget(tgt) +  this.value.round.writeTarget(tgt));
    }
}
export const alpha_fitness_encoder = (value: AlphaFitness): OutputBytes => {
    return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(value);
}
export const alpha_fitness_decoder = (p: Parser): AlphaFitness => {
    return record_decoder<AlphaFitness>({level: Int32.decode, locked_round: CGRIDClass__AlphaFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p);
}
