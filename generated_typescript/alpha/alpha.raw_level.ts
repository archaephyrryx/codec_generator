import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type AlphaRawLevel = Int32;
export class CGRIDClass__AlphaRawLevel extends Box<AlphaRawLevel> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaRawLevel {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const alpha_raw_level_encoder = (value: AlphaRawLevel): OutputBytes => {
    return value.encode();
}
export const alpha_raw_level_decoder = (p: Parser): AlphaRawLevel => {
    return Int32.decode(p);
}
