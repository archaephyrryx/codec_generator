import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type AlphaNonce = FixedBytes<32>;
export class CGRIDClass__AlphaNonce extends Box<AlphaNonce> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaNonce {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const alpha_nonce_encoder = (value: AlphaNonce): OutputBytes => {
    return value.encode();
}
export const alpha_nonce_decoder = (p: Parser): AlphaNonce => {
    return FixedBytes.decode<32>({len: 32})(p);
}
