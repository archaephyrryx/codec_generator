import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__AlphaVotingPeriodKind__exploration generated for AlphaVotingPeriodKind__exploration
export class CGRIDClass__AlphaVotingPeriodKind__exploration extends Box<AlphaVotingPeriodKind__exploration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriodKind__exploration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaVotingPeriodKind__Proposal generated for AlphaVotingPeriodKind__Proposal
export class CGRIDClass__AlphaVotingPeriodKind__Proposal extends Box<AlphaVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaVotingPeriodKind__Promotion generated for AlphaVotingPeriodKind__Promotion
export class CGRIDClass__AlphaVotingPeriodKind__Promotion extends Box<AlphaVotingPeriodKind__Promotion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriodKind__Promotion {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaVotingPeriodKind__Cooldown generated for AlphaVotingPeriodKind__Cooldown
export class CGRIDClass__AlphaVotingPeriodKind__Cooldown extends Box<AlphaVotingPeriodKind__Cooldown> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriodKind__Cooldown {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__AlphaVotingPeriodKind__Adoption generated for AlphaVotingPeriodKind__Adoption
export class CGRIDClass__AlphaVotingPeriodKind__Adoption extends Box<AlphaVotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type AlphaVotingPeriodKind__exploration = Unit;
export type AlphaVotingPeriodKind__Proposal = Unit;
export type AlphaVotingPeriodKind__Promotion = Unit;
export type AlphaVotingPeriodKind__Cooldown = Unit;
export type AlphaVotingPeriodKind__Adoption = Unit;
// Class CGRIDClass__AlphaVoting_period_kind generated for AlphaVotingPeriodKind
export function alphavotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__AlphaVotingPeriodKind,AlphaVotingPeriodKind> {
    function f(disc: CGRIDTag__AlphaVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__AlphaVotingPeriodKind.Proposal: return CGRIDClass__AlphaVotingPeriodKind__Proposal.decode;
            case CGRIDTag__AlphaVotingPeriodKind.exploration: return CGRIDClass__AlphaVotingPeriodKind__exploration.decode;
            case CGRIDTag__AlphaVotingPeriodKind.Cooldown: return CGRIDClass__AlphaVotingPeriodKind__Cooldown.decode;
            case CGRIDTag__AlphaVotingPeriodKind.Promotion: return CGRIDClass__AlphaVotingPeriodKind__Promotion.decode;
            case CGRIDTag__AlphaVotingPeriodKind.Adoption: return CGRIDClass__AlphaVotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__AlphaVotingPeriodKind => Object.values(CGRIDTag__AlphaVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__AlphaVoting_period_kind extends Box<AlphaVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<AlphaVotingPeriodKind>, AlphaVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaVoting_period_kind {
        return new this(variant_decoder(width.Uint8)(alphavotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__AlphaVotingPeriodKind{
    Proposal = 0,
    exploration = 1,
    Cooldown = 2,
    Promotion = 3,
    Adoption = 4
}
export interface CGRIDMap__AlphaVotingPeriodKind {
    Proposal: CGRIDClass__AlphaVotingPeriodKind__Proposal,
    exploration: CGRIDClass__AlphaVotingPeriodKind__exploration,
    Cooldown: CGRIDClass__AlphaVotingPeriodKind__Cooldown,
    Promotion: CGRIDClass__AlphaVotingPeriodKind__Promotion,
    Adoption: CGRIDClass__AlphaVotingPeriodKind__Adoption
}
export type AlphaVotingPeriodKind = { kind: CGRIDTag__AlphaVotingPeriodKind.Proposal, value: CGRIDMap__AlphaVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__AlphaVotingPeriodKind.exploration, value: CGRIDMap__AlphaVotingPeriodKind['exploration'] } | { kind: CGRIDTag__AlphaVotingPeriodKind.Cooldown, value: CGRIDMap__AlphaVotingPeriodKind['Cooldown'] } | { kind: CGRIDTag__AlphaVotingPeriodKind.Promotion, value: CGRIDMap__AlphaVotingPeriodKind['Promotion'] } | { kind: CGRIDTag__AlphaVotingPeriodKind.Adoption, value: CGRIDMap__AlphaVotingPeriodKind['Adoption'] };
export type AlphaVotingPeriod = { index: Int32, kind: CGRIDClass__AlphaVoting_period_kind, start_position: Int32 };
export class CGRIDClass__AlphaVotingPeriod extends Box<AlphaVotingPeriod> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['index', 'kind', 'start_position']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__AlphaVotingPeriod {
        return new this(record_decoder<AlphaVotingPeriod>({index: Int32.decode, kind: CGRIDClass__AlphaVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p));
    };
    get encodeLength(): number {
        return (this.value.index.encodeLength +  this.value.kind.encodeLength +  this.value.start_position.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.index.writeTarget(tgt) +  this.value.kind.writeTarget(tgt) +  this.value.start_position.writeTarget(tgt));
    }
}
export const alpha_voting_period_encoder = (value: AlphaVotingPeriod): OutputBytes => {
    return record_encoder({order: ['index', 'kind', 'start_position']})(value);
}
export const alpha_voting_period_decoder = (p: Parser): AlphaVotingPeriod => {
    return record_decoder<AlphaVotingPeriod>({index: Int32.decode, kind: CGRIDClass__AlphaVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p);
}
