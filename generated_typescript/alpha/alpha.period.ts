import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type AlphaPeriod = Int64;
export class CGRIDClass__AlphaPeriod extends Box<AlphaPeriod> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__AlphaPeriod {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const alpha_period_encoder = (value: AlphaPeriod): OutputBytes => {
    return value.encode();
}
export const alpha_period_decoder = (p: Parser): AlphaPeriod => {
    return Int64.decode(p);
}
