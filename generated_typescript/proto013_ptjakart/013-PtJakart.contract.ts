import { Codec } from '../../ts_runtime/codec';
import { Padded } from '../../ts_runtime/composite/padded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad generated for Proto013PtJakartContractIdOriginatedDenestPad
export class CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad extends Box<Proto013PtJakartContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto013PtJakartContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartContract__Originated generated for Proto013PtJakartContract__Originated
export class CGRIDClass__Proto013PtJakartContract__Originated extends Box<Proto013PtJakartContract__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartContract__Originated {
        return new this(Padded.decode(CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartContract__Implicit generated for Proto013PtJakartContract__Implicit
export class CGRIDClass__Proto013PtJakartContract__Implicit extends Box<Proto013PtJakartContract__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartContract__Implicit {
        return new this(record_decoder<Proto013PtJakartContract__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export type Proto013PtJakartContract__Originated = Padded<CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad,1>;
export type Proto013PtJakartContract__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto013PtJakartContract{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto013PtJakartContract {
    Implicit: CGRIDClass__Proto013PtJakartContract__Implicit,
    Originated: CGRIDClass__Proto013PtJakartContract__Originated
}
export type Proto013PtJakartContract = { kind: CGRIDTag__Proto013PtJakartContract.Implicit, value: CGRIDMap__Proto013PtJakartContract['Implicit'] } | { kind: CGRIDTag__Proto013PtJakartContract.Originated, value: CGRIDMap__Proto013PtJakartContract['Originated'] };
export function proto013ptjakartcontract_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartContract,Proto013PtJakartContract> {
    function f(disc: CGRIDTag__Proto013PtJakartContract) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartContract.Implicit: return CGRIDClass__Proto013PtJakartContract__Implicit.decode;
            case CGRIDTag__Proto013PtJakartContract.Originated: return CGRIDClass__Proto013PtJakartContract__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartContract => Object.values(CGRIDTag__Proto013PtJakartContract).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013PtJakartContract extends Box<Proto013PtJakartContract> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartContract>, Proto013PtJakartContract>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartContract {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartcontract_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto013_ptjakart_contract_encoder = (value: Proto013PtJakartContract): OutputBytes => {
    return variant_encoder<KindOf<Proto013PtJakartContract>, Proto013PtJakartContract>(width.Uint8)(value);
}
export const proto013_ptjakart_contract_decoder = (p: Parser): Proto013PtJakartContract => {
    return variant_decoder(width.Uint8)(proto013ptjakartcontract_mkDecoder())(p);
}
