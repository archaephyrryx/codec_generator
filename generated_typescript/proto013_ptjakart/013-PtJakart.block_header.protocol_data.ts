import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_payload_hash generated for Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_payload_hash extends Box<Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartBlock_headerAlphaSigned_contents_signature generated for Proto013PtJakartBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto013_PtJakartBlock_headerAlphaSigned_contents_signature extends Box<Proto013PtJakartBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto013PtJakartBlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
export type Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type Proto013PtJakartBlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto013PtJakartBlockHeaderProtocolData = { payload_hash: CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_toggle_vote: Int8, signature: CGRIDClass__Proto013_PtJakartBlock_headerAlphaSigned_contents_signature };
export class CGRIDClass__Proto013PtJakartBlockHeaderProtocolData extends Box<Proto013PtJakartBlockHeaderProtocolData> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartBlockHeaderProtocolData {
        return new this(record_decoder<Proto013PtJakartBlockHeaderProtocolData>({payload_hash: CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__Proto013_PtJakartBlock_headerAlphaSigned_contents_signature.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_toggle_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_toggle_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export const proto013_ptjakart_block_header_protocol_data_encoder = (value: Proto013PtJakartBlockHeaderProtocolData): OutputBytes => {
    return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(value);
}
export const proto013_ptjakart_block_header_protocol_data_decoder = (p: Parser): Proto013PtJakartBlockHeaderProtocolData => {
    return record_decoder<Proto013PtJakartBlockHeaderProtocolData>({payload_hash: CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__Proto013_PtJakartBlock_headerAlphaSigned_contents_signature.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p);
}
