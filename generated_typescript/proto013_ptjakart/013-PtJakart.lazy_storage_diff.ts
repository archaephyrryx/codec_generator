import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__sapling_state generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__sapling_state
export class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__sapling_state extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__sapling_state> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['id', 'diff']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__sapling_state {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__sapling_state>({id: Z.decode, diff: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff.decode}, {order: ['id', 'diff']})(p));
    };
    get encodeLength(): number {
        return (this.value.id.encodeLength +  this.value.diff.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.id.writeTarget(tgt) +  this.value.diff.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__big_map generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__big_map
export class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__big_map extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__big_map> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['id', 'diff']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__big_map {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__big_map>({id: Z.decode, diff: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff.decode}, {order: ['id', 'diff']})(p));
    };
    get encodeLength(): number {
        return (this.value.id.encodeLength +  this.value.diff.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.id.writeTarget(tgt) +  this.value.diff.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update
export class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update>({action: Unit.decode, updates: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates.decode}, {order: ['action', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove
export class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove>({action: Unit.decode}, {order: ['action']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy
export class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'source', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy>({action: Unit.decode, source: Z.decode, updates: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates.decode}, {order: ['action', 'source', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.source.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.source.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc
export class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates', 'memo_size']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc>({action: Unit.decode, updates: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates.decode, memo_size: Uint16.decode}, {order: ['action', 'updates', 'memo_size']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength +  this.value.memo_size.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt) +  this.value.memo_size.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__update generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__update
export class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__update extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__update {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__update>({action: Unit.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['action', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove
export class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove>({action: Unit.decode}, {order: ['action']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy
export class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'source', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy>({action: Unit.decode, source: Z.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['action', 'source', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.source.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.source.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc
export class CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates', 'key_type', 'value_type']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc>({action: Unit.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq.decode), width.Uint30), key_type: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, value_type: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode}, {order: ['action', 'updates', 'key_type', 'value_type']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength +  this.value.key_type.encodeLength +  this.value.value_type.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt) +  this.value.key_type.writeTarget(tgt) +  this.value.value_type.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__String generated for MichelineProto013PtJakartMichelsonV1Expression__String
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__String extends Box<MichelineProto013PtJakartMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__String {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Sequence generated for MichelineProto013PtJakartMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Sequence extends Box<MichelineProto013PtJakartMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__generic generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__generic extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Int generated for MichelineProto013PtJakartMichelsonV1Expression__Int
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Int extends Box<MichelineProto013PtJakartMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Bytes generated for MichelineProto013PtJakartMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Bytes extends Box<MichelineProto013PtJakartMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__sapling_state = { id: Z, diff: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__big_map = { id: Z, diff: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update = { action: Unit, updates: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove = { action: Unit };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy = { action: Unit, source: Z, updates: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc = { action: Unit, updates: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates, memo_size: Uint16 };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__update = { action: Unit, updates: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq>,width.Uint30> };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove = { action: Unit };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy = { action: Unit, source: Z, updates: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq>,width.Uint30> };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc = { action: Unit, updates: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq>,width.Uint30>, key_type: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, value_type: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression };
export type MichelineProto013PtJakartMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineProto013PtJakartMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression>,width.Uint30>;
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, arg2: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, arg2: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, arg: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, arg: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression };
export type MichelineProto013PtJakartMichelsonV1Expression__Int = { int: Z };
export type MichelineProto013PtJakartMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
// Class CGRIDClass__SaplingTransactionCiphertext generated for SaplingTransactionCiphertext
export class CGRIDClass__SaplingTransactionCiphertext extends Box<SaplingTransactionCiphertext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionCiphertext {
        return new this(record_decoder<SaplingTransactionCiphertext>({cv: FixedBytes.decode<32>({len: 32}), epk: FixedBytes.decode<32>({len: 32}), payload_enc: Dynamic.decode(Bytes.decode, width.Uint30), nonce_enc: FixedBytes.decode<24>({len: 24}), payload_out: FixedBytes.decode<80>({len: 80}), nonce_out: FixedBytes.decode<24>({len: 24})}, {order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(p));
    };
    get encodeLength(): number {
        return (this.value.cv.encodeLength +  this.value.epk.encodeLength +  this.value.payload_enc.encodeLength +  this.value.nonce_enc.encodeLength +  this.value.payload_out.encodeLength +  this.value.nonce_out.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cv.writeTarget(tgt) +  this.value.epk.writeTarget(tgt) +  this.value.payload_enc.writeTarget(tgt) +  this.value.nonce_enc.writeTarget(tgt) +  this.value.payload_out.writeTarget(tgt) +  this.value.nonce_out.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartMichelsonV1Primitives generated for Proto013PtJakartMichelsonV1Primitives
export class CGRIDClass__Proto013_PtJakartMichelsonV1Primitives extends Box<Proto013PtJakartMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<Proto013PtJakartMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is Proto013PtJakartMichelsonV1Primitives => (Object.values(Proto013PtJakartMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff
export function proto013ptjakartlazystoragediffdenestdyndenestseqsaplingstatediff_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff,Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff> {
    function f(disc: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.update: return CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update.decode;
            case CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.remove: return CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove.decode;
            case CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.copy: return CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy.decode;
            case CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.alloc: return CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff => Object.values(CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff>, Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartlazystoragediffdenestdyndenestseqsaplingstatediff_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff
export function proto013ptjakartlazystoragediffdenestdyndenestseqbigmapdiff_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff,Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff> {
    function f(disc: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff.update: return CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__update.decode;
            case CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff.remove: return CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove.decode;
            case CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff.copy: return CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy.decode;
            case CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff.alloc: return CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff => Object.values(CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff>, Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartlazystoragediffdenestdyndenestseqbigmapdiff_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq generated for Proto013PtJakartLazyStorageDiffDenestDynDenestSeq
export function proto013ptjakartlazystoragediffdenestdyndenestseq_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq,Proto013PtJakartLazyStorageDiffDenestDynDenestSeq> {
    function f(disc: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq.big_map: return CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__big_map.decode;
            case CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq.sapling_state: return CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__sapling_state.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq => Object.values(CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq extends Box<Proto013PtJakartLazyStorageDiffDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartLazyStorageDiffDenestDynDenestSeq>, Proto013PtJakartLazyStorageDiffDenestDynDenestSeq>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartlazystoragediffdenestdyndenestseq_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression generated for MichelineProto013PtJakartMichelsonV1Expression
export function michelineproto013ptjakartmichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression,MichelineProto013PtJakartMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Int: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.String: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Sequence: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Bytes: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression => Object.values(CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression extends Box<MichelineProto013PtJakartMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineProto013PtJakartMichelsonV1Expression>, MichelineProto013PtJakartMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelineproto013ptjakartmichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export type SaplingTransactionCiphertext = { cv: FixedBytes<32>, epk: FixedBytes<32>, payload_enc: Dynamic<Bytes,width.Uint30>, nonce_enc: FixedBytes<24>, payload_out: FixedBytes<80>, nonce_out: FixedBytes<24> };
export enum CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression {
    Int: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Bytes
}
export type MichelineProto013PtJakartMichelsonV1Expression = { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Int, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.String, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Bytes'] };
export enum Proto013PtJakartMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117,
    LEVEL = 118,
    SELF_ADDRESS = 119,
    never = 120,
    NEVER = 121,
    UNPAIR = 122,
    VOTING_POWER = 123,
    TOTAL_VOTING_POWER = 124,
    KECCAK = 125,
    SHA3 = 126,
    PAIRING_CHECK = 127,
    bls12_381_g1 = 128,
    bls12_381_g2 = 129,
    bls12_381_fr = 130,
    sapling_state = 131,
    sapling_transaction_deprecated = 132,
    SAPLING_EMPTY_STATE = 133,
    SAPLING_VERIFY_UPDATE = 134,
    ticket = 135,
    TICKET = 136,
    READ_TICKET = 137,
    SPLIT_TICKET = 138,
    JOIN_TICKETS = 139,
    GET_AND_UPDATE = 140,
    chest = 141,
    chest_key = 142,
    OPEN_CHEST = 143,
    VIEW = 144,
    view = 145,
    constant = 146,
    SUB_MUTEZ = 147,
    tx_rollup_l2_address = 148,
    MIN_BLOCK_TIME = 149,
    sapling_transaction = 150
}
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export enum CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff{
    update = 0,
    remove = 1,
    copy = 2,
    alloc = 3
}
export interface CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff {
    update: CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update,
    remove: CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove,
    copy: CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy,
    alloc: CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc
}
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff = { kind: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.update, value: CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['update'] } | { kind: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.remove, value: CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['remove'] } | { kind: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.copy, value: CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['copy'] } | { kind: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.alloc, value: CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['alloc'] };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression> };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression> };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression> };
export enum CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff{
    update = 0,
    remove = 1,
    copy = 2,
    alloc = 3
}
export interface CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff {
    update: CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__update,
    remove: CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove,
    copy: CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy,
    alloc: CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc
}
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff = { kind: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff.update, value: CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff['update'] } | { kind: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff.remove, value: CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff['remove'] } | { kind: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff.copy, value: CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff['copy'] } | { kind: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff.alloc, value: CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeqBigMapDiff['alloc'] };
export enum CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq{
    big_map = 0,
    sapling_state = 1
}
export interface CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq {
    big_map: CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__big_map,
    sapling_state: CGRIDClass__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq__sapling_state
}
export type Proto013PtJakartLazyStorageDiffDenestDynDenestSeq = { kind: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq.big_map, value: CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq['big_map'] } | { kind: CGRIDTag__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq.sapling_state, value: CGRIDMap__Proto013PtJakartLazyStorageDiffDenestDynDenestSeq['sapling_state'] };
export type Proto013PtJakartLazyStorageDiff = Dynamic<Sequence<CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq>,width.Uint30>;
export class CGRIDClass__Proto013PtJakartLazyStorageDiff extends Box<Proto013PtJakartLazyStorageDiff> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLazyStorageDiff {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto013_ptjakart_lazy_storage_diff_encoder = (value: Proto013PtJakartLazyStorageDiff): OutputBytes => {
    return value.encode();
}
export const proto013_ptjakart_lazy_storage_diff_decoder = (p: Parser): Proto013PtJakartLazyStorageDiff => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartLazy_storage_diff_denest_dyn_denest_seq.decode), width.Uint30)(p);
}
