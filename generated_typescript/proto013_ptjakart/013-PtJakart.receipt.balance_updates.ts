import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Subsidy generated for Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Subsidy
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Subsidy extends Box<Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Subsidy> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Subsidy {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Simulation generated for Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Simulation
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Simulation extends Box<Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Simulation> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Simulation {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration generated for Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration extends Box<Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Block_application generated for Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Block_application
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Block_application extends Box<Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Block_application> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Block_application {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards generated for Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments generated for Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Storage_fees generated for Proto013PtJakartOperationMetadataAlphaBalance__Storage_fees
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Storage_fees extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Storage_fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Storage_fees {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Storage_fees>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Nonce_revelation_rewards generated for Proto013PtJakartOperationMetadataAlphaBalance__Nonce_revelation_rewards
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Nonce_revelation_rewards extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Nonce_revelation_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Nonce_revelation_rewards {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Nonce_revelation_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Minted generated for Proto013PtJakartOperationMetadataAlphaBalance__Minted
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Minted extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Minted> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Minted {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Minted>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Lost_endorsing_rewards generated for Proto013PtJakartOperationMetadataAlphaBalance__Lost_endorsing_rewards
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Lost_endorsing_rewards extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Lost_endorsing_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'participation', 'revelation', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Lost_endorsing_rewards {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Lost_endorsing_rewards>({category: Unit.decode, delegate: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate.decode, participation: Bool.decode, revelation: Bool.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'participation', 'revelation', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.participation.encodeLength +  this.value.revelation.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.participation.writeTarget(tgt) +  this.value.revelation.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Liquidity_baking_subsidies generated for Proto013PtJakartOperationMetadataAlphaBalance__Liquidity_baking_subsidies
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Liquidity_baking_subsidies extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Liquidity_baking_subsidies> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Liquidity_baking_subsidies {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Liquidity_baking_subsidies>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Invoice generated for Proto013PtJakartOperationMetadataAlphaBalance__Invoice
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Invoice extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Invoice> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Invoice {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Invoice>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Initial_commitments generated for Proto013PtJakartOperationMetadataAlphaBalance__Initial_commitments
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Initial_commitments extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Initial_commitments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Initial_commitments {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Initial_commitments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Frozen_bonds generated for Proto013PtJakartOperationMetadataAlphaBalance__Frozen_bonds
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Frozen_bonds extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Frozen_bonds> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'contract', 'bond_id', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Frozen_bonds {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Frozen_bonds>({category: Unit.decode, contract: CGRIDClass__Proto013_PtJakartContract_id.decode, bond_id: CGRIDClass__Proto013_PtJakartBond_id.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'contract', 'bond_id', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.contract.encodeLength +  this.value.bond_id.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.contract.writeTarget(tgt) +  this.value.bond_id.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Endorsing_rewards generated for Proto013PtJakartOperationMetadataAlphaBalance__Endorsing_rewards
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Endorsing_rewards extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Endorsing_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Endorsing_rewards {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Endorsing_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_punishments generated for Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_punishments
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_punishments extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_punishments {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_evidence_rewards generated for Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_evidence_rewards
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_evidence_rewards extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_evidence_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_evidence_rewards {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_evidence_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Deposits generated for Proto013PtJakartOperationMetadataAlphaBalance__Deposits
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Deposits extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Deposits> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Deposits {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Deposits>({category: Unit.decode, delegate: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Deposits_delegate.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Contract generated for Proto013PtJakartOperationMetadataAlphaBalance__Contract
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Contract extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Contract> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Contract {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Contract>({contract: CGRIDClass__Proto013_PtJakartContract_id.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['contract', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Commitments generated for Proto013PtJakartOperationMetadataAlphaBalance__Commitments
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Commitments extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Commitments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'committer', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Commitments {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Commitments>({category: Unit.decode, committer: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Commitments_committer.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'committer', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.committer.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.committer.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Burned generated for Proto013PtJakartOperationMetadataAlphaBalance__Burned
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Burned extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Burned> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Burned {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Burned>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Bootstrap generated for Proto013PtJakartOperationMetadataAlphaBalance__Bootstrap
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Bootstrap extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Bootstrap> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Bootstrap {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Bootstrap>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Block_fees generated for Proto013PtJakartOperationMetadataAlphaBalance__Block_fees
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Block_fees extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Block_fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Block_fees {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Block_fees>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Baking_rewards generated for Proto013PtJakartOperationMetadataAlphaBalance__Baking_rewards
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Baking_rewards extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Baking_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Baking_rewards {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Baking_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Baking_bonuses generated for Proto013PtJakartOperationMetadataAlphaBalance__Baking_bonuses
export class CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Baking_bonuses extends Box<Proto013PtJakartOperationMetadataAlphaBalance__Baking_bonuses> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Baking_bonuses {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalance__Baking_bonuses>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartContractId__Originated generated for Proto013PtJakartContractId__Originated
export class CGRIDClass__Proto013PtJakartContractId__Originated extends Box<Proto013PtJakartContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartContractId__Implicit generated for Proto013PtJakartContractId__Implicit
export class CGRIDClass__Proto013PtJakartContractId__Implicit extends Box<Proto013PtJakartContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartContractId__Implicit {
        return new this(record_decoder<Proto013PtJakartContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartBondId__Tx_rollup_bond_id generated for Proto013PtJakartBondId__Tx_rollup_bond_id
export class CGRIDClass__Proto013PtJakartBondId__Tx_rollup_bond_id extends Box<Proto013PtJakartBondId__Tx_rollup_bond_id> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['tx_rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartBondId__Tx_rollup_bond_id {
        return new this(record_decoder<Proto013PtJakartBondId__Tx_rollup_bond_id>({tx_rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id.decode}, {order: ['tx_rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.tx_rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.tx_rollup.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Subsidy = Unit;
export type Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Simulation = Unit;
export type Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration = Unit;
export type Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Block_application = Unit;
export type Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Storage_fees = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Nonce_revelation_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Minted = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Lost_endorsing_rewards = { category: Unit, delegate: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate, participation: Bool, revelation: Bool, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Liquidity_baking_subsidies = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Invoice = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Initial_commitments = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Frozen_bonds = { category: Unit, contract: CGRIDClass__Proto013_PtJakartContract_id, bond_id: CGRIDClass__Proto013_PtJakartBond_id, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Endorsing_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_punishments = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_evidence_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Deposits = { category: Unit, delegate: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Deposits_delegate, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Contract = { contract: CGRIDClass__Proto013_PtJakartContract_id, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Commitments = { category: Unit, committer: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Commitments_committer, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Burned = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Bootstrap = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Block_fees = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Baking_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartOperationMetadataAlphaBalance__Baking_bonuses = { category: Unit, change: Int64, origin: CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin };
export type Proto013PtJakartContractId__Originated = Padded<CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad,1>;
export type Proto013PtJakartContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartBondId__Tx_rollup_bond_id = { tx_rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartTx_rollup_id generated for Proto013PtJakartTxRollupId
export class CGRIDClass__Proto013_PtJakartTx_rollup_id extends Box<Proto013PtJakartTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartTx_rollup_id {
        return new this(record_decoder<Proto013PtJakartTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin generated for Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin
export function proto013ptjakartoperationmetadataalphaupdateoriginorigin_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin,Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin.Block_application: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Block_application.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin.Subsidy: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Subsidy.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin.Simulation: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Simulation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin => Object.values(CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin extends Box<Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin>, Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperation_metadataAlphaUpdate_origin_origin {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationmetadataalphaupdateoriginorigin_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate generated for Proto013PtJakartOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate
export class CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate extends Box<Proto013PtJakartOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Deposits_delegate generated for Proto013PtJakartOperationMetadataAlphaBalanceDepositsDelegate
export class CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Deposits_delegate extends Box<Proto013PtJakartOperationMetadataAlphaBalanceDepositsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Deposits_delegate {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalanceDepositsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Commitments_committer generated for Proto013PtJakartOperationMetadataAlphaBalanceCommitmentsCommitter
export class CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Commitments_committer extends Box<Proto013PtJakartOperationMetadataAlphaBalanceCommitmentsCommitter> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['blinded_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance_Commitments_committer {
        return new this(record_decoder<Proto013PtJakartOperationMetadataAlphaBalanceCommitmentsCommitter>({blinded_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['blinded_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.blinded_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.blinded_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance generated for Proto013PtJakartOperationMetadataAlphaBalance
export function proto013ptjakartoperationmetadataalphabalance_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance,Proto013PtJakartOperationMetadataAlphaBalance> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Contract: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Contract.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Block_fees: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Block_fees.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Deposits: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Deposits.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Nonce_revelation_rewards: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Nonce_revelation_rewards.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Double_signing_evidence_rewards: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_evidence_rewards.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Endorsing_rewards: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Endorsing_rewards.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Baking_rewards: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Baking_rewards.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Baking_bonuses: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Baking_bonuses.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Storage_fees: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Storage_fees.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Double_signing_punishments: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_punishments.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Lost_endorsing_rewards: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Lost_endorsing_rewards.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Liquidity_baking_subsidies: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Liquidity_baking_subsidies.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Burned: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Burned.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Commitments: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Commitments.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Bootstrap: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Bootstrap.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Invoice: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Invoice.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Initial_commitments: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Initial_commitments.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Minted: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Minted.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Frozen_bonds: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Frozen_bonds.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Tx_rollup_rejection_rewards: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards.decode;
            case CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Tx_rollup_rejection_punishments: return CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance => Object.values(CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance extends Box<Proto013PtJakartOperationMetadataAlphaBalance> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationMetadataAlphaBalance>, Proto013PtJakartOperationMetadataAlphaBalance>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationmetadataalphabalance_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad generated for Proto013PtJakartContractIdOriginatedDenestPad
export class CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad extends Box<Proto013PtJakartContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto013PtJakartContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartContract_id generated for Proto013PtJakartContractId
export function proto013ptjakartcontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartContractId,Proto013PtJakartContractId> {
    function f(disc: CGRIDTag__Proto013PtJakartContractId) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartContractId.Implicit: return CGRIDClass__Proto013PtJakartContractId__Implicit.decode;
            case CGRIDTag__Proto013PtJakartContractId.Originated: return CGRIDClass__Proto013PtJakartContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartContractId => Object.values(CGRIDTag__Proto013PtJakartContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartContract_id extends Box<Proto013PtJakartContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartContractId>, Proto013PtJakartContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartContract_id {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartcontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartBond_id generated for Proto013PtJakartBondId
export function proto013ptjakartbondid_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartBondId,Proto013PtJakartBondId> {
    function f(disc: CGRIDTag__Proto013PtJakartBondId) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartBondId.Tx_rollup_bond_id: return CGRIDClass__Proto013PtJakartBondId__Tx_rollup_bond_id.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartBondId => Object.values(CGRIDTag__Proto013PtJakartBondId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartBond_id extends Box<Proto013PtJakartBondId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartBondId>, Proto013PtJakartBondId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartBond_id {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartbondid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export type Proto013PtJakartTxRollupId = { rollup_hash: FixedBytes<20> };
export enum CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin{
    Block_application = 0,
    Protocol_migration = 1,
    Subsidy = 2,
    Simulation = 3
}
export interface CGRIDMap__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin {
    Block_application: CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Block_application,
    Protocol_migration: CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration,
    Subsidy: CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Subsidy,
    Simulation: CGRIDClass__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin__Simulation
}
export type Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin = { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin.Block_application, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin['Block_application'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin['Protocol_migration'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin.Subsidy, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin['Subsidy'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin.Simulation, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaUpdateOriginOrigin['Simulation'] };
export type Proto013PtJakartOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationMetadataAlphaBalanceDepositsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationMetadataAlphaBalanceCommitmentsCommitter = { blinded_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance{
    Contract = 0,
    Block_fees = 2,
    Deposits = 4,
    Nonce_revelation_rewards = 5,
    Double_signing_evidence_rewards = 6,
    Endorsing_rewards = 7,
    Baking_rewards = 8,
    Baking_bonuses = 9,
    Storage_fees = 11,
    Double_signing_punishments = 12,
    Lost_endorsing_rewards = 13,
    Liquidity_baking_subsidies = 14,
    Burned = 15,
    Commitments = 16,
    Bootstrap = 17,
    Invoice = 18,
    Initial_commitments = 19,
    Minted = 20,
    Frozen_bonds = 21,
    Tx_rollup_rejection_rewards = 22,
    Tx_rollup_rejection_punishments = 23
}
export interface CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance {
    Contract: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Contract,
    Block_fees: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Block_fees,
    Deposits: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Deposits,
    Nonce_revelation_rewards: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Nonce_revelation_rewards,
    Double_signing_evidence_rewards: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_evidence_rewards,
    Endorsing_rewards: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Endorsing_rewards,
    Baking_rewards: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Baking_rewards,
    Baking_bonuses: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Baking_bonuses,
    Storage_fees: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Storage_fees,
    Double_signing_punishments: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Double_signing_punishments,
    Lost_endorsing_rewards: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Lost_endorsing_rewards,
    Liquidity_baking_subsidies: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Liquidity_baking_subsidies,
    Burned: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Burned,
    Commitments: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Commitments,
    Bootstrap: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Bootstrap,
    Invoice: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Invoice,
    Initial_commitments: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Initial_commitments,
    Minted: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Minted,
    Frozen_bonds: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Frozen_bonds,
    Tx_rollup_rejection_rewards: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards,
    Tx_rollup_rejection_punishments: CGRIDClass__Proto013PtJakartOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments
}
export type Proto013PtJakartOperationMetadataAlphaBalance = { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Contract, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Contract'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Block_fees, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Block_fees'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Deposits, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Deposits'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Nonce_revelation_rewards, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Nonce_revelation_rewards'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Double_signing_evidence_rewards, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Double_signing_evidence_rewards'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Endorsing_rewards, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Endorsing_rewards'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Baking_rewards, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Baking_rewards'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Baking_bonuses, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Baking_bonuses'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Storage_fees, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Storage_fees'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Double_signing_punishments, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Double_signing_punishments'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Lost_endorsing_rewards, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Lost_endorsing_rewards'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Liquidity_baking_subsidies, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Liquidity_baking_subsidies'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Burned, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Burned'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Commitments, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Commitments'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Bootstrap, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Bootstrap'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Invoice, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Invoice'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Initial_commitments, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Initial_commitments'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Minted, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Minted'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Frozen_bonds, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Frozen_bonds'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Tx_rollup_rejection_rewards, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Tx_rollup_rejection_rewards'] } | { kind: CGRIDTag__Proto013PtJakartOperationMetadataAlphaBalance.Tx_rollup_rejection_punishments, value: CGRIDMap__Proto013PtJakartOperationMetadataAlphaBalance['Tx_rollup_rejection_punishments'] };
export type Proto013PtJakartContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto013PtJakartContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto013PtJakartContractId {
    Implicit: CGRIDClass__Proto013PtJakartContractId__Implicit,
    Originated: CGRIDClass__Proto013PtJakartContractId__Originated
}
export type Proto013PtJakartContractId = { kind: CGRIDTag__Proto013PtJakartContractId.Implicit, value: CGRIDMap__Proto013PtJakartContractId['Implicit'] } | { kind: CGRIDTag__Proto013PtJakartContractId.Originated, value: CGRIDMap__Proto013PtJakartContractId['Originated'] };
export enum CGRIDTag__Proto013PtJakartBondId{
    Tx_rollup_bond_id = 0
}
export interface CGRIDMap__Proto013PtJakartBondId {
    Tx_rollup_bond_id: CGRIDClass__Proto013PtJakartBondId__Tx_rollup_bond_id
}
export type Proto013PtJakartBondId = { kind: CGRIDTag__Proto013PtJakartBondId.Tx_rollup_bond_id, value: CGRIDMap__Proto013PtJakartBondId['Tx_rollup_bond_id'] };
export type Proto013PtJakartReceiptBalanceUpdates = Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance>,width.Uint30>;
export class CGRIDClass__Proto013PtJakartReceiptBalanceUpdates extends Box<Proto013PtJakartReceiptBalanceUpdates> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartReceiptBalanceUpdates {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto013_ptjakart_receipt_balance_updates_encoder = (value: Proto013PtJakartReceiptBalanceUpdates): OutputBytes => {
    return value.encode();
}
export const proto013_ptjakart_receipt_balance_updates_decoder = (p: Parser): Proto013PtJakartReceiptBalanceUpdates => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperation_metadataAlphaBalance.decode), width.Uint30)(p);
}
