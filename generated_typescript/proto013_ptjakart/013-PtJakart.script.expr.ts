import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__String generated for MichelineProto013PtJakartMichelsonV1Expression__String
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__String extends Box<MichelineProto013PtJakartMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__String {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Sequence generated for MichelineProto013PtJakartMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Sequence extends Box<MichelineProto013PtJakartMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__generic generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__generic extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Int generated for MichelineProto013PtJakartMichelsonV1Expression__Int
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Int extends Box<MichelineProto013PtJakartMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Bytes generated for MichelineProto013PtJakartMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Bytes extends Box<MichelineProto013PtJakartMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineProto013PtJakartMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
export type MichelineProto013PtJakartMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineProto013PtJakartMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression>,width.Uint30>;
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, arg2: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, arg2: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, arg: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__Proto013_PtJakartMichelsonV1Primitives, arg: CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression };
export type MichelineProto013PtJakartMichelsonV1Expression__Int = { int: Z };
export type MichelineProto013PtJakartMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
// Class CGRIDClass__Proto013_PtJakartMichelsonV1Primitives generated for Proto013PtJakartMichelsonV1Primitives
export class CGRIDClass__Proto013_PtJakartMichelsonV1Primitives extends Box<Proto013PtJakartMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<Proto013PtJakartMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is Proto013PtJakartMichelsonV1Primitives => (Object.values(Proto013PtJakartMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression generated for MichelineProto013PtJakartMichelsonV1Expression
export function michelineproto013ptjakartmichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression,MichelineProto013PtJakartMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Int: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.String: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Sequence: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Bytes: return CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression => Object.values(CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression extends Box<MichelineProto013PtJakartMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineProto013PtJakartMichelsonV1Expression>, MichelineProto013PtJakartMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelineproto013ptjakartmichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression {
    Int: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineProto013PtJakartMichelsonV1Expression__Bytes
}
export type MichelineProto013PtJakartMichelsonV1Expression = { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Int, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.String, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineProto013PtJakartMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineProto013PtJakartMichelsonV1Expression['Bytes'] };
export enum Proto013PtJakartMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117,
    LEVEL = 118,
    SELF_ADDRESS = 119,
    never = 120,
    NEVER = 121,
    UNPAIR = 122,
    VOTING_POWER = 123,
    TOTAL_VOTING_POWER = 124,
    KECCAK = 125,
    SHA3 = 126,
    PAIRING_CHECK = 127,
    bls12_381_g1 = 128,
    bls12_381_g2 = 129,
    bls12_381_fr = 130,
    sapling_state = 131,
    sapling_transaction_deprecated = 132,
    SAPLING_EMPTY_STATE = 133,
    SAPLING_VERIFY_UPDATE = 134,
    ticket = 135,
    TICKET = 136,
    READ_TICKET = 137,
    SPLIT_TICKET = 138,
    JOIN_TICKETS = 139,
    GET_AND_UPDATE = 140,
    chest = 141,
    chest_key = 142,
    OPEN_CHEST = 143,
    VIEW = 144,
    view = 145,
    constant = 146,
    SUB_MUTEZ = 147,
    tx_rollup_l2_address = 148,
    MIN_BLOCK_TIME = 149,
    sapling_transaction = 150
}
export type Proto013PtJakartScriptExpr = CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression;
export class CGRIDClass__Proto013PtJakartScriptExpr extends Box<Proto013PtJakartScriptExpr> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartScriptExpr {
        return new this(CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto013_ptjakart_script_expr_encoder = (value: Proto013PtJakartScriptExpr): OutputBytes => {
    return value.encode();
}
export const proto013_ptjakart_script_expr_decoder = (p: Parser): Proto013PtJakartScriptExpr => {
    return CGRIDClass__MichelineProto013_PtJakartMichelson_v1Expression.decode(p);
}
