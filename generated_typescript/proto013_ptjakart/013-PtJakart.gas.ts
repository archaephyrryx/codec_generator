import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto013PtJakartGas__Unaccounted generated for Proto013PtJakartGas__Unaccounted
export class CGRIDClass__Proto013PtJakartGas__Unaccounted extends Box<Proto013PtJakartGas__Unaccounted> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartGas__Unaccounted {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartGas__Limited generated for Proto013PtJakartGas__Limited
export class CGRIDClass__Proto013PtJakartGas__Limited extends Box<Proto013PtJakartGas__Limited> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartGas__Limited {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto013PtJakartGas__Unaccounted = Unit;
export type Proto013PtJakartGas__Limited = Z;
export enum CGRIDTag__Proto013PtJakartGas{
    Limited = 0,
    Unaccounted = 1
}
export interface CGRIDMap__Proto013PtJakartGas {
    Limited: CGRIDClass__Proto013PtJakartGas__Limited,
    Unaccounted: CGRIDClass__Proto013PtJakartGas__Unaccounted
}
export type Proto013PtJakartGas = { kind: CGRIDTag__Proto013PtJakartGas.Limited, value: CGRIDMap__Proto013PtJakartGas['Limited'] } | { kind: CGRIDTag__Proto013PtJakartGas.Unaccounted, value: CGRIDMap__Proto013PtJakartGas['Unaccounted'] };
export function proto013ptjakartgas_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartGas,Proto013PtJakartGas> {
    function f(disc: CGRIDTag__Proto013PtJakartGas) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartGas.Limited: return CGRIDClass__Proto013PtJakartGas__Limited.decode;
            case CGRIDTag__Proto013PtJakartGas.Unaccounted: return CGRIDClass__Proto013PtJakartGas__Unaccounted.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartGas => Object.values(CGRIDTag__Proto013PtJakartGas).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013PtJakartGas extends Box<Proto013PtJakartGas> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartGas>, Proto013PtJakartGas>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartGas {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartgas_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto013_ptjakart_gas_encoder = (value: Proto013PtJakartGas): OutputBytes => {
    return variant_encoder<KindOf<Proto013PtJakartGas>, Proto013PtJakartGas>(width.Uint8)(value);
}
export const proto013_ptjakart_gas_decoder = (p: Parser): Proto013PtJakartGas => {
    return variant_decoder(width.Uint8)(proto013ptjakartgas_mkDecoder())(p);
}
