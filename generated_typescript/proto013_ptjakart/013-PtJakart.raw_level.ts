import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto013PtJakartRawLevel = Int32;
export class CGRIDClass__Proto013PtJakartRawLevel extends Box<Proto013PtJakartRawLevel> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartRawLevel {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto013_ptjakart_raw_level_encoder = (value: Proto013PtJakartRawLevel): OutputBytes => {
    return value.encode();
}
export const proto013_ptjakart_raw_level_decoder = (p: Parser): Proto013PtJakartRawLevel => {
    return Int32.decode(p);
}
