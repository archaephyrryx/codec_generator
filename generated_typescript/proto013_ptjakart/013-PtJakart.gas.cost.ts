import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
export type Proto013PtJakartGasCost = Z;
export class CGRIDClass__Proto013PtJakartGasCost extends Box<Proto013PtJakartGasCost> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartGasCost {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto013_ptjakart_gas_cost_encoder = (value: Proto013PtJakartGasCost): OutputBytes => {
    return value.encode();
}
export const proto013_ptjakart_gas_cost_decoder = (p: Parser): Proto013PtJakartGasCost => {
    return Z.decode(p);
}
