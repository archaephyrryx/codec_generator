import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int31, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto013PtJakartConstantsFixed = { proof_of_work_nonce_size: Uint8, nonce_length: Uint8, max_anon_ops_per_block: Uint8, max_operation_data_length: Int31, max_proposals_per_delegate: Uint8, max_micheline_node_count: Int31, max_micheline_bytes_limit: Int31, max_allowed_global_constants_depth: Int31, cache_layout_size: Uint8, michelson_maximum_type_size: Uint16 };
export class CGRIDClass__Proto013PtJakartConstantsFixed extends Box<Proto013PtJakartConstantsFixed> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['proof_of_work_nonce_size', 'nonce_length', 'max_anon_ops_per_block', 'max_operation_data_length', 'max_proposals_per_delegate', 'max_micheline_node_count', 'max_micheline_bytes_limit', 'max_allowed_global_constants_depth', 'cache_layout_size', 'michelson_maximum_type_size']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartConstantsFixed {
        return new this(record_decoder<Proto013PtJakartConstantsFixed>({proof_of_work_nonce_size: Uint8.decode, nonce_length: Uint8.decode, max_anon_ops_per_block: Uint8.decode, max_operation_data_length: Int31.decode, max_proposals_per_delegate: Uint8.decode, max_micheline_node_count: Int31.decode, max_micheline_bytes_limit: Int31.decode, max_allowed_global_constants_depth: Int31.decode, cache_layout_size: Uint8.decode, michelson_maximum_type_size: Uint16.decode}, {order: ['proof_of_work_nonce_size', 'nonce_length', 'max_anon_ops_per_block', 'max_operation_data_length', 'max_proposals_per_delegate', 'max_micheline_node_count', 'max_micheline_bytes_limit', 'max_allowed_global_constants_depth', 'cache_layout_size', 'michelson_maximum_type_size']})(p));
    };
    get encodeLength(): number {
        return (this.value.proof_of_work_nonce_size.encodeLength +  this.value.nonce_length.encodeLength +  this.value.max_anon_ops_per_block.encodeLength +  this.value.max_operation_data_length.encodeLength +  this.value.max_proposals_per_delegate.encodeLength +  this.value.max_micheline_node_count.encodeLength +  this.value.max_micheline_bytes_limit.encodeLength +  this.value.max_allowed_global_constants_depth.encodeLength +  this.value.cache_layout_size.encodeLength +  this.value.michelson_maximum_type_size.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.proof_of_work_nonce_size.writeTarget(tgt) +  this.value.nonce_length.writeTarget(tgt) +  this.value.max_anon_ops_per_block.writeTarget(tgt) +  this.value.max_operation_data_length.writeTarget(tgt) +  this.value.max_proposals_per_delegate.writeTarget(tgt) +  this.value.max_micheline_node_count.writeTarget(tgt) +  this.value.max_micheline_bytes_limit.writeTarget(tgt) +  this.value.max_allowed_global_constants_depth.writeTarget(tgt) +  this.value.cache_layout_size.writeTarget(tgt) +  this.value.michelson_maximum_type_size.writeTarget(tgt));
    }
}
export const proto013_ptjakart_constants_fixed_encoder = (value: Proto013PtJakartConstantsFixed): OutputBytes => {
    return record_encoder({order: ['proof_of_work_nonce_size', 'nonce_length', 'max_anon_ops_per_block', 'max_operation_data_length', 'max_proposals_per_delegate', 'max_micheline_node_count', 'max_micheline_bytes_limit', 'max_allowed_global_constants_depth', 'cache_layout_size', 'michelson_maximum_type_size']})(value);
}
export const proto013_ptjakart_constants_fixed_decoder = (p: Parser): Proto013PtJakartConstantsFixed => {
    return record_decoder<Proto013PtJakartConstantsFixed>({proof_of_work_nonce_size: Uint8.decode, nonce_length: Uint8.decode, max_anon_ops_per_block: Uint8.decode, max_operation_data_length: Int31.decode, max_proposals_per_delegate: Uint8.decode, max_micheline_node_count: Int31.decode, max_micheline_bytes_limit: Int31.decode, max_allowed_global_constants_depth: Int31.decode, cache_layout_size: Uint8.decode, michelson_maximum_type_size: Uint16.decode}, {order: ['proof_of_work_nonce_size', 'nonce_length', 'max_anon_ops_per_block', 'max_operation_data_length', 'max_proposals_per_delegate', 'max_micheline_node_count', 'max_micheline_bytes_limit', 'max_allowed_global_constants_depth', 'cache_layout_size', 'michelson_maximum_type_size']})(p);
}
