import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { Target } from '../../ts_runtime/target';
export type Proto013PtJakartLevel = { level: Int32, level_position: Int32, cycle: Int32, cycle_position: Int32, expected_commitment: Bool };
export class CGRIDClass__Proto013PtJakartLevel extends Box<Proto013PtJakartLevel> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'level_position', 'cycle', 'cycle_position', 'expected_commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartLevel {
        return new this(record_decoder<Proto013PtJakartLevel>({level: Int32.decode, level_position: Int32.decode, cycle: Int32.decode, cycle_position: Int32.decode, expected_commitment: Bool.decode}, {order: ['level', 'level_position', 'cycle', 'cycle_position', 'expected_commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.level_position.encodeLength +  this.value.cycle.encodeLength +  this.value.cycle_position.encodeLength +  this.value.expected_commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.level_position.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.cycle_position.writeTarget(tgt) +  this.value.expected_commitment.writeTarget(tgt));
    }
}
export const proto013_ptjakart_level_encoder = (value: Proto013PtJakartLevel): OutputBytes => {
    return record_encoder({order: ['level', 'level_position', 'cycle', 'cycle_position', 'expected_commitment']})(value);
}
export const proto013_ptjakart_level_decoder = (p: Parser): Proto013PtJakartLevel => {
    return record_decoder<Proto013PtJakartLevel>({level: Int32.decode, level_position: Int32.decode, cycle: Int32.decode, cycle_position: Int32.decode, expected_commitment: Bool.decode}, {order: ['level', 'level_position', 'cycle', 'cycle_position', 'expected_commitment']})(p);
}
