import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartTransactionDestination__Tx_rollup generated for Proto013PtJakartTransactionDestination__Tx_rollup
export class CGRIDClass__Proto013PtJakartTransactionDestination__Tx_rollup extends Box<Proto013PtJakartTransactionDestination__Tx_rollup> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartTransactionDestination__Tx_rollup {
        return new this(Padded.decode(CGRIDClass__Proto013_PtJakartTx_rollup_id.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartTransactionDestination__Originated generated for Proto013PtJakartTransactionDestination__Originated
export class CGRIDClass__Proto013PtJakartTransactionDestination__Originated extends Box<Proto013PtJakartTransactionDestination__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartTransactionDestination__Originated {
        return new this(Padded.decode(CGRIDClass__Proto013_PtJakartTransaction_destination_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartTransactionDestination__Implicit generated for Proto013PtJakartTransactionDestination__Implicit
export class CGRIDClass__Proto013PtJakartTransactionDestination__Implicit extends Box<Proto013PtJakartTransactionDestination__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartTransactionDestination__Implicit {
        return new this(record_decoder<Proto013PtJakartTransactionDestination__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint__set_delegate generated for Proto013PtJakartEntrypoint__set_delegate
export class CGRIDClass__Proto013PtJakartEntrypoint__set_delegate extends Box<Proto013PtJakartEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint__root generated for Proto013PtJakartEntrypoint__root
export class CGRIDClass__Proto013PtJakartEntrypoint__root extends Box<Proto013PtJakartEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint__remove_delegate generated for Proto013PtJakartEntrypoint__remove_delegate
export class CGRIDClass__Proto013PtJakartEntrypoint__remove_delegate extends Box<Proto013PtJakartEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint__named generated for Proto013PtJakartEntrypoint__named
export class CGRIDClass__Proto013PtJakartEntrypoint__named extends Box<Proto013PtJakartEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint___do generated for Proto013PtJakartEntrypoint___do
export class CGRIDClass__Proto013PtJakartEntrypoint___do extends Box<Proto013PtJakartEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint___default generated for Proto013PtJakartEntrypoint___default
export class CGRIDClass__Proto013PtJakartEntrypoint___default extends Box<Proto013PtJakartEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartContractId__Originated generated for Proto013PtJakartContractId__Originated
export class CGRIDClass__Proto013PtJakartContractId__Originated extends Box<Proto013PtJakartContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartContractId__Implicit generated for Proto013PtJakartContractId__Implicit
export class CGRIDClass__Proto013PtJakartContractId__Implicit extends Box<Proto013PtJakartContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartContractId__Implicit {
        return new this(record_decoder<Proto013PtJakartContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Transaction generated for Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Transaction
export class CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Transaction extends Box<Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Transaction {
        return new this(record_decoder<Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Transaction>({amount: N.decode, destination: CGRIDClass__Proto013_PtJakartTransaction_destination.decode, parameters: Option.decode(CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Transaction_parameters.decode)}, {order: ['amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Origination generated for Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Origination
export class CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Origination extends Box<Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Origination {
        return new this(record_decoder<Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Origination>({balance: N.decode, delegate: Option.decode(CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Origination_delegate.decode), script: CGRIDClass__Proto013_PtJakartScriptedContracts.decode}, {order: ['balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Delegation generated for Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Delegation
export class CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Delegation extends Box<Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Delegation {
        return new this(record_decoder<Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Delegation>({delegate: Option.decode(CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Delegation_delegate.decode)}, {order: ['delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.delegate.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto013PtJakartTransactionDestination__Tx_rollup = Padded<CGRIDClass__Proto013_PtJakartTx_rollup_id,1>;
export type Proto013PtJakartTransactionDestination__Originated = Padded<CGRIDClass__Proto013_PtJakartTransaction_destination_Originated_denest_pad,1>;
export type Proto013PtJakartTransactionDestination__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartEntrypoint__set_delegate = Unit;
export type Proto013PtJakartEntrypoint__root = Unit;
export type Proto013PtJakartEntrypoint__remove_delegate = Unit;
export type Proto013PtJakartEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto013PtJakartEntrypoint___do = Unit;
export type Proto013PtJakartEntrypoint___default = Unit;
export type Proto013PtJakartContractId__Originated = Padded<CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad,1>;
export type Proto013PtJakartContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Transaction = { amount: N, destination: CGRIDClass__Proto013_PtJakartTransaction_destination, parameters: Option<CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Transaction_parameters> };
export type Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Origination = { balance: N, delegate: Option<CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Origination_delegate>, script: CGRIDClass__Proto013_PtJakartScriptedContracts };
export type Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Delegation = { delegate: Option<CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Delegation_delegate> };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartTx_rollup_id generated for Proto013PtJakartTxRollupId
export class CGRIDClass__Proto013_PtJakartTx_rollup_id extends Box<Proto013PtJakartTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartTx_rollup_id {
        return new this(record_decoder<Proto013PtJakartTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartTransaction_destination_Originated_denest_pad generated for Proto013PtJakartTransactionDestinationOriginatedDenestPad
export class CGRIDClass__Proto013_PtJakartTransaction_destination_Originated_denest_pad extends Box<Proto013PtJakartTransactionDestinationOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartTransaction_destination_Originated_denest_pad {
        return new this(record_decoder<Proto013PtJakartTransactionDestinationOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartTransaction_destination generated for Proto013PtJakartTransactionDestination
export function proto013ptjakarttransactiondestination_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartTransactionDestination,Proto013PtJakartTransactionDestination> {
    function f(disc: CGRIDTag__Proto013PtJakartTransactionDestination) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartTransactionDestination.Implicit: return CGRIDClass__Proto013PtJakartTransactionDestination__Implicit.decode;
            case CGRIDTag__Proto013PtJakartTransactionDestination.Originated: return CGRIDClass__Proto013PtJakartTransactionDestination__Originated.decode;
            case CGRIDTag__Proto013PtJakartTransactionDestination.Tx_rollup: return CGRIDClass__Proto013PtJakartTransactionDestination__Tx_rollup.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartTransactionDestination => Object.values(CGRIDTag__Proto013PtJakartTransactionDestination).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartTransaction_destination extends Box<Proto013PtJakartTransactionDestination> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartTransactionDestination>, Proto013PtJakartTransactionDestination>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartTransaction_destination {
        return new this(variant_decoder(width.Uint8)(proto013ptjakarttransactiondestination_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartScriptedContracts generated for Proto013PtJakartScriptedContracts
export class CGRIDClass__Proto013_PtJakartScriptedContracts extends Box<Proto013PtJakartScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartScriptedContracts {
        return new this(record_decoder<Proto013PtJakartScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartEntrypoint generated for Proto013PtJakartEntrypoint
export function proto013ptjakartentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartEntrypoint,Proto013PtJakartEntrypoint> {
    function f(disc: CGRIDTag__Proto013PtJakartEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartEntrypoint._default: return CGRIDClass__Proto013PtJakartEntrypoint___default.decode;
            case CGRIDTag__Proto013PtJakartEntrypoint.root: return CGRIDClass__Proto013PtJakartEntrypoint__root.decode;
            case CGRIDTag__Proto013PtJakartEntrypoint._do: return CGRIDClass__Proto013PtJakartEntrypoint___do.decode;
            case CGRIDTag__Proto013PtJakartEntrypoint.set_delegate: return CGRIDClass__Proto013PtJakartEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto013PtJakartEntrypoint.remove_delegate: return CGRIDClass__Proto013PtJakartEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto013PtJakartEntrypoint.named: return CGRIDClass__Proto013PtJakartEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartEntrypoint => Object.values(CGRIDTag__Proto013PtJakartEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartEntrypoint extends Box<Proto013PtJakartEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartEntrypoint>, Proto013PtJakartEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad generated for Proto013PtJakartContractIdOriginatedDenestPad
export class CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad extends Box<Proto013PtJakartContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto013PtJakartContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartContract_id generated for Proto013PtJakartContractId
export function proto013ptjakartcontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartContractId,Proto013PtJakartContractId> {
    function f(disc: CGRIDTag__Proto013PtJakartContractId) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartContractId.Implicit: return CGRIDClass__Proto013PtJakartContractId__Implicit.decode;
            case CGRIDTag__Proto013PtJakartContractId.Originated: return CGRIDClass__Proto013PtJakartContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartContractId => Object.values(CGRIDTag__Proto013PtJakartContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartContract_id extends Box<Proto013PtJakartContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartContractId>, Proto013PtJakartContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartContract_id {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartcontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_rhs generated for Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs
export function proto013ptjakartapplyresultsalphainternaloperationresultrhs_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs,Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs> {
    function f(disc: CGRIDTag__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs.Transaction: return CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Transaction.decode;
            case CGRIDTag__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs.Origination: return CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Origination.decode;
            case CGRIDTag__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs.Delegation: return CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Delegation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs => Object.values(CGRIDTag__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_rhs extends Box<Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs>, Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_rhs {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartapplyresultsalphainternaloperationresultrhs_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Transaction_parameters generated for Proto013PtJakartApplyResultsAlphaInternalOperationResultTransactionParameters
export class CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Transaction_parameters extends Box<Proto013PtJakartApplyResultsAlphaInternalOperationResultTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Transaction_parameters {
        return new this(record_decoder<Proto013PtJakartApplyResultsAlphaInternalOperationResultTransactionParameters>({entrypoint: CGRIDClass__Proto013_PtJakartEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Origination_delegate generated for Proto013PtJakartApplyResultsAlphaInternalOperationResultOriginationDelegate
export class CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Origination_delegate extends Box<Proto013PtJakartApplyResultsAlphaInternalOperationResultOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Origination_delegate {
        return new this(record_decoder<Proto013PtJakartApplyResultsAlphaInternalOperationResultOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Delegation_delegate generated for Proto013PtJakartApplyResultsAlphaInternalOperationResultDelegationDelegate
export class CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Delegation_delegate extends Box<Proto013PtJakartApplyResultsAlphaInternalOperationResultDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_Delegation_delegate {
        return new this(record_decoder<Proto013PtJakartApplyResultsAlphaInternalOperationResultDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export type Proto013PtJakartTxRollupId = { rollup_hash: FixedBytes<20> };
export type Proto013PtJakartTransactionDestinationOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto013PtJakartTransactionDestination{
    Implicit = 0,
    Originated = 1,
    Tx_rollup = 2
}
export interface CGRIDMap__Proto013PtJakartTransactionDestination {
    Implicit: CGRIDClass__Proto013PtJakartTransactionDestination__Implicit,
    Originated: CGRIDClass__Proto013PtJakartTransactionDestination__Originated,
    Tx_rollup: CGRIDClass__Proto013PtJakartTransactionDestination__Tx_rollup
}
export type Proto013PtJakartTransactionDestination = { kind: CGRIDTag__Proto013PtJakartTransactionDestination.Implicit, value: CGRIDMap__Proto013PtJakartTransactionDestination['Implicit'] } | { kind: CGRIDTag__Proto013PtJakartTransactionDestination.Originated, value: CGRIDMap__Proto013PtJakartTransactionDestination['Originated'] } | { kind: CGRIDTag__Proto013PtJakartTransactionDestination.Tx_rollup, value: CGRIDMap__Proto013PtJakartTransactionDestination['Tx_rollup'] };
export type Proto013PtJakartScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export enum CGRIDTag__Proto013PtJakartEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto013PtJakartEntrypoint {
    _default: CGRIDClass__Proto013PtJakartEntrypoint___default,
    root: CGRIDClass__Proto013PtJakartEntrypoint__root,
    _do: CGRIDClass__Proto013PtJakartEntrypoint___do,
    set_delegate: CGRIDClass__Proto013PtJakartEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto013PtJakartEntrypoint__remove_delegate,
    named: CGRIDClass__Proto013PtJakartEntrypoint__named
}
export type Proto013PtJakartEntrypoint = { kind: CGRIDTag__Proto013PtJakartEntrypoint._default, value: CGRIDMap__Proto013PtJakartEntrypoint['_default'] } | { kind: CGRIDTag__Proto013PtJakartEntrypoint.root, value: CGRIDMap__Proto013PtJakartEntrypoint['root'] } | { kind: CGRIDTag__Proto013PtJakartEntrypoint._do, value: CGRIDMap__Proto013PtJakartEntrypoint['_do'] } | { kind: CGRIDTag__Proto013PtJakartEntrypoint.set_delegate, value: CGRIDMap__Proto013PtJakartEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto013PtJakartEntrypoint.remove_delegate, value: CGRIDMap__Proto013PtJakartEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto013PtJakartEntrypoint.named, value: CGRIDMap__Proto013PtJakartEntrypoint['named'] };
export type Proto013PtJakartContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto013PtJakartContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto013PtJakartContractId {
    Implicit: CGRIDClass__Proto013PtJakartContractId__Implicit,
    Originated: CGRIDClass__Proto013PtJakartContractId__Originated
}
export type Proto013PtJakartContractId = { kind: CGRIDTag__Proto013PtJakartContractId.Implicit, value: CGRIDMap__Proto013PtJakartContractId['Implicit'] } | { kind: CGRIDTag__Proto013PtJakartContractId.Originated, value: CGRIDMap__Proto013PtJakartContractId['Originated'] };
export enum CGRIDTag__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs{
    Transaction = 1,
    Origination = 2,
    Delegation = 3
}
export interface CGRIDMap__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs {
    Transaction: CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Transaction,
    Origination: CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Origination,
    Delegation: CGRIDClass__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs__Delegation
}
export type Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs = { kind: CGRIDTag__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs.Transaction, value: CGRIDMap__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs['Transaction'] } | { kind: CGRIDTag__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs.Origination, value: CGRIDMap__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs['Origination'] } | { kind: CGRIDTag__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs.Delegation, value: CGRIDMap__Proto013PtJakartApplyResultsAlphaInternalOperationResultRhs['Delegation'] };
export type Proto013PtJakartApplyResultsAlphaInternalOperationResultTransactionParameters = { entrypoint: CGRIDClass__Proto013_PtJakartEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto013PtJakartApplyResultsAlphaInternalOperationResultOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartApplyResultsAlphaInternalOperationResultDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationInternal = { source: CGRIDClass__Proto013_PtJakartContract_id, nonce: Uint16, proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs: CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_rhs };
export class CGRIDClass__Proto013PtJakartOperationInternal extends Box<Proto013PtJakartOperationInternal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'nonce', 'proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationInternal {
        return new this(record_decoder<Proto013PtJakartOperationInternal>({source: CGRIDClass__Proto013_PtJakartContract_id.decode, nonce: Uint16.decode, proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs: CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_rhs.decode}, {order: ['source', 'nonce', 'proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.nonce.encodeLength +  this.value.proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt) +  this.value.proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs.writeTarget(tgt));
    }
}
export const proto013_ptjakart_operation_internal_encoder = (value: Proto013PtJakartOperationInternal): OutputBytes => {
    return record_encoder({order: ['source', 'nonce', 'proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs']})(value);
}
export const proto013_ptjakart_operation_internal_decoder = (p: Parser): Proto013PtJakartOperationInternal => {
    return record_decoder<Proto013PtJakartOperationInternal>({source: CGRIDClass__Proto013_PtJakartContract_id.decode, nonce: Uint16.decode, proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs: CGRIDClass__Proto013_PtJakartApply_resultsAlphaInternal_operation_result_rhs.decode}, {order: ['source', 'nonce', 'proto013_ptjakart_apply_results_alpha_internal_operation_result_rhs']})(p);
}
