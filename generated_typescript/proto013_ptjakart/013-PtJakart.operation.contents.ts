import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Nullable } from '../../ts_runtime/composite/opt/nullable';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { SequenceFixed } from '../../ts_runtime/composite/seq/sequence.fixed';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int16, Int31, Int32, Int64, Int8, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartTransactionDestination__Tx_rollup generated for Proto013PtJakartTransactionDestination__Tx_rollup
export class CGRIDClass__Proto013PtJakartTransactionDestination__Tx_rollup extends Box<Proto013PtJakartTransactionDestination__Tx_rollup> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartTransactionDestination__Tx_rollup {
        return new this(Padded.decode(CGRIDClass__Proto013_PtJakartTx_rollup_id.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartTransactionDestination__Originated generated for Proto013PtJakartTransactionDestination__Originated
export class CGRIDClass__Proto013PtJakartTransactionDestination__Originated extends Box<Proto013PtJakartTransactionDestination__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartTransactionDestination__Originated {
        return new this(Padded.decode(CGRIDClass__Proto013_PtJakartTransaction_destination_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartTransactionDestination__Implicit generated for Proto013PtJakartTransactionDestination__Implicit
export class CGRIDClass__Proto013PtJakartTransactionDestination__Implicit extends Box<Proto013PtJakartTransactionDestination__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartTransactionDestination__Implicit {
        return new this(record_decoder<Proto013PtJakartTransactionDestination__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_3 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_3
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_3 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_3> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_3 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_3>(Int16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index1.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index2.decode, Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq.decode), width.Uint30))(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength +  this.value[3].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt) +  this.value[3].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_2
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_2> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_2 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_2>(Int16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index1.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index2.decode, Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq.decode), width.Uint30))(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength +  this.value[3].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt) +  this.value[3].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_1>(Int16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index1.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index2.decode, Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq.decode), width.Uint30))(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength +  this.value[3].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt) +  this.value[3].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_0> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_0 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_0>(Int16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index1.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index2.decode, Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq.decode), width.Uint30))(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength +  this.value[3].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt) +  this.value[3].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_9 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_9
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_9 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_9> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_9 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_9>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_8 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_8
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_8 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_8> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_8 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_8>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_7 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_7
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_7 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_7> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_7 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_7>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_6 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_6
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_6 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_6> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_6 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_6>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_5 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_5
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_5 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_5> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_5 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_5>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_4 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_4
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_4 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_4> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_4 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_4>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_3 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_3
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_3 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_3> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_3 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_3>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_227 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_227
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_227 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_227> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_227 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_227>(Int64.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_226 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_226
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_226 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_226> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_226 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_226>(Int32.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_225 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_225
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_225 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_225> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_225 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_225>(Uint16.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_224 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_224
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_224 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_224> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_224 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_224>(Uint8.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_2
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_2> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_2 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_2>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_195 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_195
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_195 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_195> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_195 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_193 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_193
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_193 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_193> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_193 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint16)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_192 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_192
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_192 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_192> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_192 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_15 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_15
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_15 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_15> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_15 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_15>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_14 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_14
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_14 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_14> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_14 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_14>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_131 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_131
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_131 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_131> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_131 {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_130 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_130
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_130 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_130> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_130 {
        return new this(SequenceFixed.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq.decode, 2)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_13 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_13
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_13 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_13> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_13 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_13>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_129 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_129
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_129 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_129> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_129 {
        return new this(SequenceFixed.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_128 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_128
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_128 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_128> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_128 {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_12 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_12
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_12 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_12> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_12 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_12>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_11 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_11
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_11 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_11> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_11 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_11>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_10 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_10
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_10 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_10> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_10 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_10>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_1>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_0> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_0 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_0>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_9 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_9
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_9 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_9> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_9 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_9>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_8 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_8
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_8 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_8> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_8 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_8>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_7 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_7
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_7 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_7> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_7 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_7>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_6 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_6
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_6 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_6> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_6 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_6>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_5 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_5
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_5 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_5> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_5 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_5>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_4 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_4
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_4 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_4> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_4 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_4>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_3 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_3
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_3 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_3> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_3 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_3>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_227 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_227
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_227 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_227> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_227 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_227>(Int64.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_226 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_226
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_226 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_226> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_226 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_226>(Int32.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_225 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_225
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_225 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_225> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_225 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_225>(Uint16.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_224 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_224
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_224 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_224> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_224 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_224>(Uint8.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_2
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_2> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_2 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_2>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_195 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_195
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_195 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_195> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_195 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_193 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_193
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_193 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_193> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_193 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint16)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_192 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_192
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_192 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_192> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_192 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_15 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_15
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_15 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_15> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_15 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_15>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_14 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_14
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_14 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_14> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_14 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_14>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_131 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_131
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_131 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_131> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_131 {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_130 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_130
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_130 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_130> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_130 {
        return new this(SequenceFixed.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq.decode, 2)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_13 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_13
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_13 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_13> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_13 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_13>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_129 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_129
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_129 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_129> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_129 {
        return new this(SequenceFixed.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_128 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_128
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_128 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_128> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_128 {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_12 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_12
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_12 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_12> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_12 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_12>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_11 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_11
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_11 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_11> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_11 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_11>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_10 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_10
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_10 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_10> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_10 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_10>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_1>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_0> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_0 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_0>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_9 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_9
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_9 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_9> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_9 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_9>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_8 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_8
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_8 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_8> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_8 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_8>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_7 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_7
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_7 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_7> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_7 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_7>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_6 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_6
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_6 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_6> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_6 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_6>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_5 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_5
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_5 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_5> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_5 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_5>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_4 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_4
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_4 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_4> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_4 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_4>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_3 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_3
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_3 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_3> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_3 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_3>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_227 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_227
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_227 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_227> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_227 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_227>(Int64.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_226 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_226
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_226 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_226> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_226 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_226>(Int32.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_225 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_225
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_225 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_225> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_225 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_225>(Uint16.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_224 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_224
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_224 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_224> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_224 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_224>(Uint8.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_2
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_2> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_2 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_2>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_195 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_195
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_195 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_195> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_195 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_193 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_193
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_193 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_193> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_193 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint16)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_192 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_192
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_192 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_192> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_192 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_15 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_15
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_15 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_15> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_15 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_15>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_14 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_14
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_14 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_14> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_14 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_14>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_131 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_131
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_131 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_131> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_131 {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_130 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_130
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_130 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_130> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_130 {
        return new this(SequenceFixed.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq.decode, 2)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_13 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_13
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_13 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_13> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_13 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_13>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_129 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_129
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_129 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_129> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_129 {
        return new this(SequenceFixed.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_128 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_128
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_128 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_128> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_128 {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_12 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_12
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_12 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_12> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_12 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_12>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_11 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_11
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_11 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_11> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_11 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_11>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_10 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_10
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_10 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_10> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_10 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_10>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_1>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_0> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_0 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_0>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_9 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_9
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_9 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_9> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_9 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_9>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_8 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_8
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_8 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_8> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_8 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_8>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_7 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_7
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_7 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_7> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_7 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_7>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_6 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_6
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_6 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_6> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_6 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_6>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_5 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_5
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_5 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_5> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_5 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_5>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_4 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_4
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_4 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_4> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_4 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_4>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_3 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_3
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_3 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_3> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_3 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_3>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_227 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_227
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_227 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_227> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_227 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_227>(Int64.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_226 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_226
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_226 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_226> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_226 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_226>(Int32.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_225 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_225
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_225 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_225> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_225 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_225>(Uint16.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_224 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_224
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_224 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_224> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_224 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_224>(Uint8.decode, Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength +  this.value[2].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt) +  this.value[2].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_2
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_2> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_2 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_2>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_195 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_195
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_195 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_195> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_195 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_193 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_193
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_193 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_193> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_193 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint16)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_192 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_192
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_192 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_192> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_192 {
        return new this(Dynamic.decode(Bytes.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_15 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_15
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_15 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_15> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_15 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_15>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_14 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_14
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_14 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_14> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_14 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_14>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_131 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_131
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_131 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_131> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_131 {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_130 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_130
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_130 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_130> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_130 {
        return new this(SequenceFixed.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq.decode, 2)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_13 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_13
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_13 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_13> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_13 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_13>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_129 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_129
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_129 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_129> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_129 {
        return new this(SequenceFixed.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_128 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_128
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_128 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_128> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_128 {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_12 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_12
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_12 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_12> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_12 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_12>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_11 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_11
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_11 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_11> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_11 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_11>(Int64.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_10 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_10
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_10 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_10> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_10 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_10>(Int32.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_1>(Uint16.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_0> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_0 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_0>(Uint8.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Deposit generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Deposit
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Deposit extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Deposit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['deposit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Deposit {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Deposit>({deposit: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit.decode}, {order: ['deposit']})(p));
    };
    get encodeLength(): number {
        return (this.value.deposit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.deposit.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Batch generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Batch
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Batch extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Batch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['batch']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Batch {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Batch>({batch: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['batch']})(p));
    };
    get encodeLength(): number {
        return (this.value.batch.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.batch.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 {
        return new this(Uint8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 generated for Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 generated for Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 generated for Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 generated for Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 {
        return new this(Uint8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some generated for Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some extends Box<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some>({commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None generated for Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None extends Box<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind__Example_arith_smart_contract_rollup_kind generated for Proto013PtJakartOperationAlphaContentsScRollupOriginateKind__Example_arith_smart_contract_rollup_kind
export class CGRIDClass__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind__Example_arith_smart_contract_rollup_kind extends Box<Proto013PtJakartOperationAlphaContentsScRollupOriginateKind__Example_arith_smart_contract_rollup_kind> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind__Example_arith_smart_contract_rollup_kind {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartInlinedPreendorsementContents__Preendorsement generated for Proto013PtJakartInlinedPreendorsementContents__Preendorsement
export class CGRIDClass__Proto013PtJakartInlinedPreendorsementContents__Preendorsement extends Box<Proto013PtJakartInlinedPreendorsementContents__Preendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartInlinedPreendorsementContents__Preendorsement {
        return new this(record_decoder<Proto013PtJakartInlinedPreendorsementContents__Preendorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto013_PtJakartInlinedPreendorsementContents_Preendorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartInlinedEndorsementMempoolContents__Endorsement generated for Proto013PtJakartInlinedEndorsementMempoolContents__Endorsement
export class CGRIDClass__Proto013PtJakartInlinedEndorsementMempoolContents__Endorsement extends Box<Proto013PtJakartInlinedEndorsementMempoolContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartInlinedEndorsementMempoolContents__Endorsement {
        return new this(record_decoder<Proto013PtJakartInlinedEndorsementMempoolContents__Endorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto013_PtJakartInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint__set_delegate generated for Proto013PtJakartEntrypoint__set_delegate
export class CGRIDClass__Proto013PtJakartEntrypoint__set_delegate extends Box<Proto013PtJakartEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint__root generated for Proto013PtJakartEntrypoint__root
export class CGRIDClass__Proto013PtJakartEntrypoint__root extends Box<Proto013PtJakartEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint__remove_delegate generated for Proto013PtJakartEntrypoint__remove_delegate
export class CGRIDClass__Proto013PtJakartEntrypoint__remove_delegate extends Box<Proto013PtJakartEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint__named generated for Proto013PtJakartEntrypoint__named
export class CGRIDClass__Proto013PtJakartEntrypoint__named extends Box<Proto013PtJakartEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint___do generated for Proto013PtJakartEntrypoint___do
export class CGRIDClass__Proto013PtJakartEntrypoint___do extends Box<Proto013PtJakartEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartEntrypoint___default generated for Proto013PtJakartEntrypoint___default
export class CGRIDClass__Proto013PtJakartEntrypoint___default extends Box<Proto013PtJakartEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartContractId__Originated generated for Proto013PtJakartContractId__Originated
export class CGRIDClass__Proto013PtJakartContractId__Originated extends Box<Proto013PtJakartContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartContractId__Implicit generated for Proto013PtJakartContractId__Implicit
export class CGRIDClass__Proto013PtJakartContractId__Implicit extends Box<Proto013PtJakartContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartContractId__Implicit {
        return new this(record_decoder<Proto013PtJakartContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto013PtJakartTransactionDestination__Tx_rollup = Padded<CGRIDClass__Proto013_PtJakartTx_rollup_id,1>;
export type Proto013PtJakartTransactionDestination__Originated = Padded<CGRIDClass__Proto013_PtJakartTransaction_destination_Originated_denest_pad,1>;
export type Proto013PtJakartTransactionDestination__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_3 = [Int16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index1, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index2, Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq>,width.Uint30>];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_2 = [Int16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index1, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index2, Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq>,width.Uint30>];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_1 = [Int16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index1, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index2, Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq>,width.Uint30>];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_0 = [Int16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index1, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index2, Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq>,width.Uint30>];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_9 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_8 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_7 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_6 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_5 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_4 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_3 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_227 = [Int64, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_226 = [Int32, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_225 = [Uint16, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_224 = [Uint8, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_2 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_195 = Dynamic<Bytes,width.Uint30>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_193 = Dynamic<Bytes,width.Uint16>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_192 = Dynamic<Bytes,width.Uint8>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_15 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_14 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_131 = Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq>,width.Uint30>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_130 = SequenceFixed<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq,2>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_13 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_129 = SequenceFixed<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq,1>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_128 = Unit;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_12 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_11 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_10 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_1 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_0 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_9 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_8 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_7 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_6 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_5 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_4 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_3 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_227 = [Int64, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_226 = [Int32, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_225 = [Uint16, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_224 = [Uint8, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_2 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_195 = Dynamic<Bytes,width.Uint30>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_193 = Dynamic<Bytes,width.Uint16>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_192 = Dynamic<Bytes,width.Uint8>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_15 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_14 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_131 = Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq>,width.Uint30>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_130 = SequenceFixed<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq,2>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_13 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_129 = SequenceFixed<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq,1>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_128 = Unit;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_12 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_11 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_10 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_1 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_0 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_9 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_8 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_7 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_6 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_5 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_4 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_3 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_227 = [Int64, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_226 = [Int32, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_225 = [Uint16, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_224 = [Uint8, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_2 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_195 = Dynamic<Bytes,width.Uint30>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_193 = Dynamic<Bytes,width.Uint16>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_192 = Dynamic<Bytes,width.Uint8>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_15 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_14 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_131 = Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq>,width.Uint30>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_130 = SequenceFixed<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq,2>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_13 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_129 = SequenceFixed<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq,1>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_128 = Unit;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_12 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_11 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_10 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_1 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_0 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_9 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_8 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_7 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_6 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_5 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_4 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_3 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_227 = [Int64, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_226 = [Int32, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_225 = [Uint16, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_224 = [Uint8, Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_2 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_195 = Dynamic<Bytes,width.Uint30>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_193 = Dynamic<Bytes,width.Uint16>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_192 = Dynamic<Bytes,width.Uint8>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_15 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_14 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_131 = Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq>,width.Uint30>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_130 = SequenceFixed<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq,2>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_13 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_129 = SequenceFixed<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq,1>;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_128 = Unit;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_12 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_11 = [Int64, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_10 = [Int32, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_1 = [Uint16, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_0 = [Uint8, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Deposit = { deposit: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Batch = { batch: Dynamic<U8String,width.Uint30> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 = Int64;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 = Int32;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 = Uint16;
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 = Uint8;
export type Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 = Int64;
export type Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 = Int32;
export type Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 = Uint16;
export type Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 = Uint8;
export type Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some = { commitment_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None = Unit;
export type Proto013PtJakartOperationAlphaContentsScRollupOriginateKind__Example_arith_smart_contract_rollup_kind = Unit;
export type Proto013PtJakartInlinedPreendorsementContents__Preendorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto013_PtJakartInlinedPreendorsementContents_Preendorsement_block_payload_hash };
export type Proto013PtJakartInlinedEndorsementMempoolContents__Endorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto013_PtJakartInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash };
export type Proto013PtJakartEntrypoint__set_delegate = Unit;
export type Proto013PtJakartEntrypoint__root = Unit;
export type Proto013PtJakartEntrypoint__remove_delegate = Unit;
export type Proto013PtJakartEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto013PtJakartEntrypoint___do = Unit;
export type Proto013PtJakartEntrypoint___default = Unit;
export type Proto013PtJakartContractId__Originated = Padded<CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad,1>;
export type Proto013PtJakartContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartTx_rollup_id generated for Proto013PtJakartTxRollupId
export class CGRIDClass__Proto013_PtJakartTx_rollup_id extends Box<Proto013PtJakartTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartTx_rollup_id {
        return new this(record_decoder<Proto013PtJakartTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartTransaction_destination_Originated_denest_pad generated for Proto013PtJakartTransactionDestinationOriginatedDenestPad
export class CGRIDClass__Proto013_PtJakartTransaction_destination_Originated_denest_pad extends Box<Proto013PtJakartTransactionDestinationOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartTransaction_destination_Originated_denest_pad {
        return new this(record_decoder<Proto013PtJakartTransactionDestinationOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartTransaction_destination generated for Proto013PtJakartTransactionDestination
export function proto013ptjakarttransactiondestination_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartTransactionDestination,Proto013PtJakartTransactionDestination> {
    function f(disc: CGRIDTag__Proto013PtJakartTransactionDestination) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartTransactionDestination.Implicit: return CGRIDClass__Proto013PtJakartTransactionDestination__Implicit.decode;
            case CGRIDTag__Proto013PtJakartTransactionDestination.Originated: return CGRIDClass__Proto013PtJakartTransactionDestination__Originated.decode;
            case CGRIDTag__Proto013PtJakartTransactionDestination.Tx_rollup: return CGRIDClass__Proto013PtJakartTransactionDestination__Tx_rollup.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartTransactionDestination => Object.values(CGRIDTag__Proto013PtJakartTransactionDestination).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartTransaction_destination extends Box<Proto013PtJakartTransactionDestination> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartTransactionDestination>, Proto013PtJakartTransactionDestination>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartTransaction_destination {
        return new this(variant_decoder(width.Uint8)(proto013ptjakarttransactiondestination_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartScriptedContracts generated for Proto013PtJakartScriptedContracts
export class CGRIDClass__Proto013_PtJakartScriptedContracts extends Box<Proto013PtJakartScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartScriptedContracts {
        return new this(record_decoder<Proto013PtJakartScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_submit_batch_source generated for Proto013PtJakartOperationAlphaContentsTxRollupSubmitBatchSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_submit_batch_source extends Box<Proto013PtJakartOperationAlphaContentsTxRollupSubmitBatchSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_submit_batch_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupSubmitBatchSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_return_bond_source generated for Proto013PtJakartOperationAlphaContentsTxRollupReturnBondSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_return_bond_source extends Box<Proto013PtJakartOperationAlphaContentsTxRollupReturnBondSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_return_bond_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupReturnBondSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_remove_commitment_source generated for Proto013PtJakartOperationAlphaContentsTxRollupRemoveCommitmentSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_remove_commitment_source extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRemoveCommitmentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_remove_commitment_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRemoveCommitmentSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_source generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_source extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseq_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_1.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_2: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_2.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_3: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_3.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_4: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_4.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_5: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_5.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_6: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_6.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_7: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_7.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_8: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_8.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_9: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_9.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_10: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_10.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_11: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_11.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_12: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_12.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_13: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_13.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_14: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_14.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_15: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_15.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_128: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_128.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_129: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_129.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_130: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_130.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_131: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_131.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_192: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_192.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_193: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_193.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_195: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_195.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_224: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_224.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_225: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_225.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_226: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_226.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_227: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_227.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0index3index0denestdyndenestseq_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseq_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_1.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_2: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_2.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_3: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_3.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_4: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_4.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_5: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_5.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_6: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_6.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_7: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_7.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_8: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_8.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_9: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_9.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_10: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_10.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_11: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_11.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_12: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_12.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_13: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_13.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_14: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_14.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_15: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_15.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_128: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_128.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_129: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_129.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_130: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_130.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_131: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_131.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_192: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_192.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_193: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_193.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_195: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_195.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_224: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_224.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_225: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_225.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_226: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_226.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_227: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_227.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2index3index0denestdyndenestseq_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseq_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_1.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_2: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_2.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_3: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_3.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_4: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_4.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_5: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_5.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_6: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_6.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_7: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_7.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_8: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_8.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_9: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_9.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_10: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_10.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_11: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_11.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_12: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_12.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_13: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_13.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_14: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_14.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_15: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_15.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_128: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_128.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_129: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_129.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_130: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_130.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_131: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_131.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_192: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_192.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_193: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_193.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_195: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_195.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_224: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_224.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_225: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_225.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_226: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_226.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_227: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_227.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1index3index0denestdyndenestseq_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1>(Unit.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1>(Unit.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0.decode, Unit.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1 {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1>(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0.decode, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_224_case_225_case_226_case_227_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case131denestdyndenestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case130denestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseqcase0case8case4case12case1case9case5case13case2case10case6case14case3case11case7case15case129denestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq {
        return new this(tuple_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq>(Dynamic.decode(Bytes.decode, width.Uint8), CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq
export function proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseq_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_1.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_2: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_2.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_3: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_3.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_4: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_4.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_5: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_5.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_6: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_6.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_7: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_7.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_8: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_8.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_9: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_9.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_10: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_10.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_11: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_11.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_12: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_12.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_13: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_13.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_14: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_14.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_15: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_15.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_128: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_128.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_129: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_129.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_130: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_130.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_131: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_131.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_192: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_192.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_193: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_193.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_195: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_195.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_224: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_224.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_225: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_225.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_226: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_226.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_227: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_227.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproofcase0case2case1case3index3index0denestdyndenestseq_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index2 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index2
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index2 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index2> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index2 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index2>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index1 generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index1
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index1 extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index1 {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index1>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof
export function proto013ptjakartoperationalphacontentstxrolluprejectionproof_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof,Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_1.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof.case_2: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_2.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof.case_3: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_3.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionproof_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['withdraw_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash>({withdraw_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['withdraw_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.withdraw_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.withdraw_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResult
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResult> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash', 'withdraw_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResult>({context_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash.decode, withdraw_list_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash.decode}, {order: ['context_hash', 'withdraw_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength +  this.value.withdraw_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt) +  this.value.withdraw_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_result_hash generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultHash
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_result_hash extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_result_hash {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultHash>({message_result_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['inbox_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq>({inbox_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['inbox_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.inbox_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.inbox_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['tx_rollup_l2_address']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination>({tx_rollup_l2_address: FixedBytes.decode<20>({len: 20})}, {order: ['tx_rollup_l2_address']})(p));
    };
    get encodeLength(): number {
        return (this.value.tx_rollup_l2_address.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.tx_rollup_l2_address.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount
export function proto013ptjakartoperationalphacontentstxrolluprejectionmessagedepositdepositamount_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount,Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_2: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_3: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionmessagedepositdepositamount_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDeposit
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDeposit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['sender', 'destination', 'ticket_hash', 'amount']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDeposit>({sender: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender.decode, destination: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination.decode, ticket_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash.decode, amount: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount.decode}, {order: ['sender', 'destination', 'ticket_hash', 'amount']})(p));
    };
    get encodeLength(): number {
        return (this.value.sender.encodeLength +  this.value.destination.encodeLength +  this.value.ticket_hash.encodeLength +  this.value.amount.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.sender.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.ticket_hash.writeTarget(tgt) +  this.value.amount.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message generated for Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage
export function proto013ptjakartoperationalphacontentstxrolluprejectionmessage_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage,Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage.Batch: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Batch.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage.Deposit: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Deposit.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message extends Box<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage>, Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrolluprejectionmessage_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_origination_source generated for Proto013PtJakartOperationAlphaContentsTxRollupOriginationSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_origination_source extends Box<Proto013PtJakartOperationAlphaContentsTxRollupOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_origination_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupOriginationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_finalize_commitment_source generated for Proto013PtJakartOperationAlphaContentsTxRollupFinalizeCommitmentSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_finalize_commitment_source extends Box<Proto013PtJakartOperationAlphaContentsTxRollupFinalizeCommitmentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_finalize_commitment_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupFinalizeCommitmentSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer generated for Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer extends Box<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount generated for Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount
export function proto013ptjakartoperationalphacontentstxrollupdispatchticketsticketsinfodenestdyndenestseqamount_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_0: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_1: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_2: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_3: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount extends Box<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount>, Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrollupdispatchticketsticketsinfodenestdyndenestseqamount_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contents', 'ty', 'ticketer', 'amount', 'claimer']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>({contents: Dynamic.decode(Bytes.decode, width.Uint30), ty: Dynamic.decode(Bytes.decode, width.Uint30), ticketer: CGRIDClass__Proto013_PtJakartContract_id.decode, amount: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount.decode, claimer: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer.decode}, {order: ['contents', 'ty', 'ticketer', 'amount', 'claimer']})(p));
    };
    get encodeLength(): number {
        return (this.value.contents.encodeLength +  this.value.ty.encodeLength +  this.value.ticketer.encodeLength +  this.value.amount.encodeLength +  this.value.claimer.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contents.writeTarget(tgt) +  this.value.ty.writeTarget(tgt) +  this.value.ticketer.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.claimer.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_source generated for Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_source extends Box<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash generated for Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsContextHash
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash extends Box<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsContextHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsContextHash>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_source generated for Proto013PtJakartOperationAlphaContentsTxRollupCommitSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_source extends Box<Proto013PtJakartOperationAlphaContentsTxRollupCommitSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupCommitSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_predecessor generated for Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor
export function proto013ptjakartoperationalphacontentstxrollupcommitcommitmentpredecessor_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor,Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor.None: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None.decode;
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor.Some: return CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_predecessor extends Box<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor>, Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_predecessor {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationalphacontentstxrollupcommitcommitmentpredecessor_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq>({message_result_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root generated for Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root extends Box<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['inbox_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot>({inbox_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['inbox_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.inbox_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.inbox_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment generated for Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitment
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment extends Box<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'messages', 'predecessor', 'inbox_merkle_root']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitment>({level: Int32.decode, messages: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq.decode), width.Uint30), predecessor: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_predecessor.decode, inbox_merkle_root: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root.decode}, {order: ['level', 'messages', 'predecessor', 'inbox_merkle_root']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.messages.encodeLength +  this.value.predecessor.encodeLength +  this.value.inbox_merkle_root.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.messages.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.inbox_merkle_root.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transfer_ticket_source generated for Proto013PtJakartOperationAlphaContentsTransferTicketSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transfer_ticket_source extends Box<Proto013PtJakartOperationAlphaContentsTransferTicketSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transfer_ticket_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTransferTicketSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transaction_source generated for Proto013PtJakartOperationAlphaContentsTransactionSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transaction_source extends Box<Proto013PtJakartOperationAlphaContentsTransactionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transaction_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTransactionSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transaction_parameters generated for Proto013PtJakartOperationAlphaContentsTransactionParameters
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transaction_parameters extends Box<Proto013PtJakartOperationAlphaContentsTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transaction_parameters {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsTransactionParameters>({entrypoint: CGRIDClass__Proto013_PtJakartEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Set_deposits_limit_source generated for Proto013PtJakartOperationAlphaContentsSetDepositsLimitSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Set_deposits_limit_source extends Box<Proto013PtJakartOperationAlphaContentsSetDepositsLimitSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Set_deposits_limit_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsSetDepositsLimitSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_source generated for Proto013PtJakartOperationAlphaContentsScRollupPublishSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_source extends Box<Proto013PtJakartOperationAlphaContentsScRollupPublishSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsScRollupPublishSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment_predecessor generated for Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentPredecessor
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment_predecessor extends Box<Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment_predecessor {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentPredecessor>({commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment_compressed_state generated for Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentCompressedState
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment_compressed_state extends Box<Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentCompressedState> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['state_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment_compressed_state {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentCompressedState>({state_hash: FixedBytes.decode<32>({len: 32})}, {order: ['state_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.state_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.state_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment generated for Proto013PtJakartOperationAlphaContentsScRollupPublishCommitment
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment extends Box<Proto013PtJakartOperationAlphaContentsScRollupPublishCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['compressed_state', 'inbox_level', 'predecessor', 'number_of_messages', 'number_of_ticks']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsScRollupPublishCommitment>({compressed_state: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment_compressed_state.decode, inbox_level: Int32.decode, predecessor: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment_predecessor.decode, number_of_messages: Int32.decode, number_of_ticks: Int32.decode}, {order: ['compressed_state', 'inbox_level', 'predecessor', 'number_of_messages', 'number_of_ticks']})(p));
    };
    get encodeLength(): number {
        return (this.value.compressed_state.encodeLength +  this.value.inbox_level.encodeLength +  this.value.predecessor.encodeLength +  this.value.number_of_messages.encodeLength +  this.value.number_of_ticks.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.compressed_state.writeTarget(tgt) +  this.value.inbox_level.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.number_of_messages.writeTarget(tgt) +  this.value.number_of_ticks.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_originate_source generated for Proto013PtJakartOperationAlphaContentsScRollupOriginateSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_originate_source extends Box<Proto013PtJakartOperationAlphaContentsScRollupOriginateSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_originate_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsScRollupOriginateSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_originate_kind generated for Proto013PtJakartOperationAlphaContentsScRollupOriginateKind
export function proto013ptjakartoperationalphacontentsscrolluporiginatekind_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind,Proto013PtJakartOperationAlphaContentsScRollupOriginateKind> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind.Example_arith_smart_contract_rollup_kind: return CGRIDClass__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind__Example_arith_smart_contract_rollup_kind.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind => Object.values(CGRIDTag__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_originate_kind extends Box<Proto013PtJakartOperationAlphaContentsScRollupOriginateKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationAlphaContentsScRollupOriginateKind>, Proto013PtJakartOperationAlphaContentsScRollupOriginateKind>(width.Uint16)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_originate_kind {
        return new this(variant_decoder(width.Uint16)(proto013ptjakartoperationalphacontentsscrolluporiginatekind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (2 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint16, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_cement_source generated for Proto013PtJakartOperationAlphaContentsScRollupCementSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_cement_source extends Box<Proto013PtJakartOperationAlphaContentsScRollupCementSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_cement_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsScRollupCementSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_cement_commitment generated for Proto013PtJakartOperationAlphaContentsScRollupCementCommitment
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_cement_commitment extends Box<Proto013PtJakartOperationAlphaContentsScRollupCementCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_cement_commitment {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsScRollupCementCommitment>({commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_add_messages_source generated for Proto013PtJakartOperationAlphaContentsScRollupAddMessagesSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_add_messages_source extends Box<Proto013PtJakartOperationAlphaContentsScRollupAddMessagesSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_add_messages_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsScRollupAddMessagesSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Reveal_source generated for Proto013PtJakartOperationAlphaContentsRevealSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Reveal_source extends Box<Proto013PtJakartOperationAlphaContentsRevealSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Reveal_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsRevealSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Reveal_public_key generated for Proto013PtJakartOperationAlphaContentsRevealPublicKey
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Reveal_public_key extends Box<Proto013PtJakartOperationAlphaContentsRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Reveal_public_key {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Register_global_constant_source generated for Proto013PtJakartOperationAlphaContentsRegisterGlobalConstantSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Register_global_constant_source extends Box<Proto013PtJakartOperationAlphaContentsRegisterGlobalConstantSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Register_global_constant_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsRegisterGlobalConstantSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Proposals_source generated for Proto013PtJakartOperationAlphaContentsProposalsSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Proposals_source extends Box<Proto013PtJakartOperationAlphaContentsProposalsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Proposals_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsProposalsSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq generated for Proto013PtJakartOperationAlphaContentsProposalsProposalsDenestDynDenestSeq
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq extends Box<Proto013PtJakartOperationAlphaContentsProposalsProposalsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Preendorsement_block_payload_hash generated for Proto013PtJakartOperationAlphaContentsPreendorsementBlockPayloadHash
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Preendorsement_block_payload_hash extends Box<Proto013PtJakartOperationAlphaContentsPreendorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Preendorsement_block_payload_hash {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsPreendorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Origination_source generated for Proto013PtJakartOperationAlphaContentsOriginationSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Origination_source extends Box<Proto013PtJakartOperationAlphaContentsOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Origination_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsOriginationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Origination_delegate generated for Proto013PtJakartOperationAlphaContentsOriginationDelegate
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Origination_delegate extends Box<Proto013PtJakartOperationAlphaContentsOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Origination_delegate {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Endorsement_block_payload_hash generated for Proto013PtJakartOperationAlphaContentsEndorsementBlockPayloadHash
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Endorsement_block_payload_hash extends Box<Proto013PtJakartOperationAlphaContentsEndorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Endorsement_block_payload_hash {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsEndorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Delegation_source generated for Proto013PtJakartOperationAlphaContentsDelegationSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Delegation_source extends Box<Proto013PtJakartOperationAlphaContentsDelegationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Delegation_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsDelegationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Delegation_delegate generated for Proto013PtJakartOperationAlphaContentsDelegationDelegate
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Delegation_delegate extends Box<Proto013PtJakartOperationAlphaContentsDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Delegation_delegate {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Ballot_source generated for Proto013PtJakartOperationAlphaContentsBallotSource
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Ballot_source extends Box<Proto013PtJakartOperationAlphaContentsBallotSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Ballot_source {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsBallotSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Ballot_proposal generated for Proto013PtJakartOperationAlphaContentsBallotProposal
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Ballot_proposal extends Box<Proto013PtJakartOperationAlphaContentsBallotProposal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Ballot_proposal {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsBallotProposal>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Activate_account_pkh generated for Proto013PtJakartOperationAlphaContentsActivateAccountPkh
export class CGRIDClass__Proto013_PtJakartOperationAlphaContents_Activate_account_pkh extends Box<Proto013PtJakartOperationAlphaContentsActivateAccountPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartOperationAlphaContents_Activate_account_pkh {
        return new this(record_decoder<Proto013PtJakartOperationAlphaContentsActivateAccountPkh>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartInlinedPreendorsement_signature generated for Proto013PtJakartInlinedPreendorsementSignature
export class CGRIDClass__Proto013_PtJakartInlinedPreendorsement_signature extends Box<Proto013PtJakartInlinedPreendorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartInlinedPreendorsement_signature {
        return new this(record_decoder<Proto013PtJakartInlinedPreendorsementSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartInlinedPreendorsementContents_Preendorsement_block_payload_hash generated for Proto013PtJakartInlinedPreendorsementContentsPreendorsementBlockPayloadHash
export class CGRIDClass__Proto013_PtJakartInlinedPreendorsementContents_Preendorsement_block_payload_hash extends Box<Proto013PtJakartInlinedPreendorsementContentsPreendorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartInlinedPreendorsementContents_Preendorsement_block_payload_hash {
        return new this(record_decoder<Proto013PtJakartInlinedPreendorsementContentsPreendorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartInlinedPreendorsementContents generated for Proto013PtJakartInlinedPreendorsementContents
export function proto013ptjakartinlinedpreendorsementcontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartInlinedPreendorsementContents,Proto013PtJakartInlinedPreendorsementContents> {
    function f(disc: CGRIDTag__Proto013PtJakartInlinedPreendorsementContents) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartInlinedPreendorsementContents.Preendorsement: return CGRIDClass__Proto013PtJakartInlinedPreendorsementContents__Preendorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartInlinedPreendorsementContents => Object.values(CGRIDTag__Proto013PtJakartInlinedPreendorsementContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartInlinedPreendorsementContents extends Box<Proto013PtJakartInlinedPreendorsementContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartInlinedPreendorsementContents>, Proto013PtJakartInlinedPreendorsementContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartInlinedPreendorsementContents {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartinlinedpreendorsementcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartInlinedPreendorsement generated for Proto013PtJakartInlinedPreendorsement
export class CGRIDClass__Proto013_PtJakartInlinedPreendorsement extends Box<Proto013PtJakartInlinedPreendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartInlinedPreendorsement {
        return new this(record_decoder<Proto013PtJakartInlinedPreendorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__Proto013_PtJakartInlinedPreendorsementContents.decode, signature: Nullable.decode(CGRIDClass__Proto013_PtJakartInlinedPreendorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartInlinedEndorsement_signature generated for Proto013PtJakartInlinedEndorsementSignature
export class CGRIDClass__Proto013_PtJakartInlinedEndorsement_signature extends Box<Proto013PtJakartInlinedEndorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartInlinedEndorsement_signature {
        return new this(record_decoder<Proto013PtJakartInlinedEndorsementSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash generated for Proto013PtJakartInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash
export class CGRIDClass__Proto013_PtJakartInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash extends Box<Proto013PtJakartInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash {
        return new this(record_decoder<Proto013PtJakartInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartInlinedEndorsement_mempoolContents generated for Proto013PtJakartInlinedEndorsementMempoolContents
export function proto013ptjakartinlinedendorsementmempoolcontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartInlinedEndorsementMempoolContents,Proto013PtJakartInlinedEndorsementMempoolContents> {
    function f(disc: CGRIDTag__Proto013PtJakartInlinedEndorsementMempoolContents) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartInlinedEndorsementMempoolContents.Endorsement: return CGRIDClass__Proto013PtJakartInlinedEndorsementMempoolContents__Endorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartInlinedEndorsementMempoolContents => Object.values(CGRIDTag__Proto013PtJakartInlinedEndorsementMempoolContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartInlinedEndorsement_mempoolContents extends Box<Proto013PtJakartInlinedEndorsementMempoolContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartInlinedEndorsementMempoolContents>, Proto013PtJakartInlinedEndorsementMempoolContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartInlinedEndorsement_mempoolContents {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartinlinedendorsementmempoolcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartInlinedEndorsement generated for Proto013PtJakartInlinedEndorsement
export class CGRIDClass__Proto013_PtJakartInlinedEndorsement extends Box<Proto013PtJakartInlinedEndorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartInlinedEndorsement {
        return new this(record_decoder<Proto013PtJakartInlinedEndorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__Proto013_PtJakartInlinedEndorsement_mempoolContents.decode, signature: Nullable.decode(CGRIDClass__Proto013_PtJakartInlinedEndorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartEntrypoint generated for Proto013PtJakartEntrypoint
export function proto013ptjakartentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartEntrypoint,Proto013PtJakartEntrypoint> {
    function f(disc: CGRIDTag__Proto013PtJakartEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartEntrypoint._default: return CGRIDClass__Proto013PtJakartEntrypoint___default.decode;
            case CGRIDTag__Proto013PtJakartEntrypoint.root: return CGRIDClass__Proto013PtJakartEntrypoint__root.decode;
            case CGRIDTag__Proto013PtJakartEntrypoint._do: return CGRIDClass__Proto013PtJakartEntrypoint___do.decode;
            case CGRIDTag__Proto013PtJakartEntrypoint.set_delegate: return CGRIDClass__Proto013PtJakartEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto013PtJakartEntrypoint.remove_delegate: return CGRIDClass__Proto013PtJakartEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto013PtJakartEntrypoint.named: return CGRIDClass__Proto013PtJakartEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartEntrypoint => Object.values(CGRIDTag__Proto013PtJakartEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartEntrypoint extends Box<Proto013PtJakartEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartEntrypoint>, Proto013PtJakartEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad generated for Proto013PtJakartContractIdOriginatedDenestPad
export class CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad extends Box<Proto013PtJakartContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto013PtJakartContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartContract_id generated for Proto013PtJakartContractId
export function proto013ptjakartcontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartContractId,Proto013PtJakartContractId> {
    function f(disc: CGRIDTag__Proto013PtJakartContractId) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartContractId.Implicit: return CGRIDClass__Proto013PtJakartContractId__Implicit.decode;
            case CGRIDTag__Proto013PtJakartContractId.Originated: return CGRIDClass__Proto013PtJakartContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartContractId => Object.values(CGRIDTag__Proto013PtJakartContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartContract_id extends Box<Proto013PtJakartContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartContractId>, Proto013PtJakartContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartContract_id {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartcontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_payload_hash generated for Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_payload_hash extends Box<Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartBlock_headerAlphaSigned_contents_signature generated for Proto013PtJakartBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto013_PtJakartBlock_headerAlphaSigned_contents_signature extends Box<Proto013PtJakartBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto013PtJakartBlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013_PtJakartBlock_headerAlphaFull_header generated for Proto013PtJakartBlockHeaderAlphaFullHeader
export class CGRIDClass__Proto013_PtJakartBlock_headerAlphaFull_header extends Box<Proto013PtJakartBlockHeaderAlphaFullHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartBlock_headerAlphaFull_header {
        return new this(record_decoder<Proto013PtJakartBlockHeaderAlphaFullHeader>({level: Int32.decode, proto: Uint8.decode, predecessor: CGRIDClass__Block_headerShell_predecessor.decode, timestamp: Int64.decode, validation_pass: Uint8.decode, operations_hash: CGRIDClass__Block_headerShell_operations_hash.decode, fitness: Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30), context: CGRIDClass__Block_headerShell_context.decode, payload_hash: CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__Proto013_PtJakartBlock_headerAlphaSigned_contents_signature.decode}, {order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.proto.encodeLength +  this.value.predecessor.encodeLength +  this.value.timestamp.encodeLength +  this.value.validation_pass.encodeLength +  this.value.operations_hash.encodeLength +  this.value.fitness.encodeLength +  this.value.context.encodeLength +  this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_toggle_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.proto.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.timestamp.writeTarget(tgt) +  this.value.validation_pass.writeTarget(tgt) +  this.value.operations_hash.writeTarget(tgt) +  this.value.fitness.writeTarget(tgt) +  this.value.context.writeTarget(tgt) +  this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_toggle_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_submit_batch generated for Proto013PtJakartOperationContents__Tx_rollup_submit_batch
export class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_submit_batch extends Box<Proto013PtJakartOperationContents__Tx_rollup_submit_batch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'content', 'burn_limit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_submit_batch {
        return new this(record_decoder<Proto013PtJakartOperationContents__Tx_rollup_submit_batch>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_submit_batch_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id.decode, content: Dynamic.decode(Bytes.decode, width.Uint30), burn_limit: Option.decode(N.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'content', 'burn_limit']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.content.encodeLength +  this.value.burn_limit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.content.writeTarget(tgt) +  this.value.burn_limit.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_return_bond generated for Proto013PtJakartOperationContents__Tx_rollup_return_bond
export class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_return_bond extends Box<Proto013PtJakartOperationContents__Tx_rollup_return_bond> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_return_bond {
        return new this(record_decoder<Proto013PtJakartOperationContents__Tx_rollup_return_bond>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_return_bond_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_remove_commitment generated for Proto013PtJakartOperationContents__Tx_rollup_remove_commitment
export class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_remove_commitment extends Box<Proto013PtJakartOperationContents__Tx_rollup_remove_commitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_remove_commitment {
        return new this(record_decoder<Proto013PtJakartOperationContents__Tx_rollup_remove_commitment>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_remove_commitment_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_rejection generated for Proto013PtJakartOperationContents__Tx_rollup_rejection
export class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_rejection extends Box<Proto013PtJakartOperationContents__Tx_rollup_rejection> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'level', 'message', 'message_position', 'message_path', 'message_result_hash', 'message_result_path', 'previous_message_result', 'previous_message_result_path', 'proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_rejection {
        return new this(record_decoder<Proto013PtJakartOperationContents__Tx_rollup_rejection>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id.decode, level: Int32.decode, message: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message.decode, message_position: N.decode, message_path: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq.decode), width.Uint30), message_result_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_result_hash.decode, message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), previous_message_result: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result.decode, previous_message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), proof: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'level', 'message', 'message_position', 'message_path', 'message_result_hash', 'message_result_path', 'previous_message_result', 'previous_message_result_path', 'proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.level.encodeLength +  this.value.message.encodeLength +  this.value.message_position.encodeLength +  this.value.message_path.encodeLength +  this.value.message_result_hash.encodeLength +  this.value.message_result_path.encodeLength +  this.value.previous_message_result.encodeLength +  this.value.previous_message_result_path.encodeLength +  this.value.proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.message.writeTarget(tgt) +  this.value.message_position.writeTarget(tgt) +  this.value.message_path.writeTarget(tgt) +  this.value.message_result_hash.writeTarget(tgt) +  this.value.message_result_path.writeTarget(tgt) +  this.value.previous_message_result.writeTarget(tgt) +  this.value.previous_message_result_path.writeTarget(tgt) +  this.value.proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_origination generated for Proto013PtJakartOperationContents__Tx_rollup_origination
export class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_origination extends Box<Proto013PtJakartOperationContents__Tx_rollup_origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup_origination']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_origination {
        return new this(record_decoder<Proto013PtJakartOperationContents__Tx_rollup_origination>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, tx_rollup_origination: Unit.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup_origination']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.tx_rollup_origination.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.tx_rollup_origination.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_finalize_commitment generated for Proto013PtJakartOperationContents__Tx_rollup_finalize_commitment
export class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_finalize_commitment extends Box<Proto013PtJakartOperationContents__Tx_rollup_finalize_commitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_finalize_commitment {
        return new this(record_decoder<Proto013PtJakartOperationContents__Tx_rollup_finalize_commitment>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_finalize_commitment_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_dispatch_tickets generated for Proto013PtJakartOperationContents__Tx_rollup_dispatch_tickets
export class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_dispatch_tickets extends Box<Proto013PtJakartOperationContents__Tx_rollup_dispatch_tickets> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup', 'level', 'context_hash', 'message_index', 'message_result_path', 'tickets_info']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_dispatch_tickets {
        return new this(record_decoder<Proto013PtJakartOperationContents__Tx_rollup_dispatch_tickets>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, tx_rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id.decode, level: Int32.decode, context_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash.decode, message_index: Int31.decode, message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), tickets_info: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup', 'level', 'context_hash', 'message_index', 'message_result_path', 'tickets_info']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.tx_rollup.encodeLength +  this.value.level.encodeLength +  this.value.context_hash.encodeLength +  this.value.message_index.encodeLength +  this.value.message_result_path.encodeLength +  this.value.tickets_info.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.tx_rollup.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.context_hash.writeTarget(tgt) +  this.value.message_index.writeTarget(tgt) +  this.value.message_result_path.writeTarget(tgt) +  this.value.tickets_info.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_commit generated for Proto013PtJakartOperationContents__Tx_rollup_commit
export class CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_commit extends Box<Proto013PtJakartOperationContents__Tx_rollup_commit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_commit {
        return new this(record_decoder<Proto013PtJakartOperationContents__Tx_rollup_commit>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id.decode, commitment: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Transfer_ticket generated for Proto013PtJakartOperationContents__Transfer_ticket
export class CGRIDClass__Proto013PtJakartOperationContents__Transfer_ticket extends Box<Proto013PtJakartOperationContents__Transfer_ticket> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'ticket_contents', 'ticket_ty', 'ticket_ticketer', 'ticket_amount', 'destination', 'entrypoint']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Transfer_ticket {
        return new this(record_decoder<Proto013PtJakartOperationContents__Transfer_ticket>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transfer_ticket_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, ticket_contents: Dynamic.decode(Bytes.decode, width.Uint30), ticket_ty: Dynamic.decode(Bytes.decode, width.Uint30), ticket_ticketer: CGRIDClass__Proto013_PtJakartContract_id.decode, ticket_amount: N.decode, destination: CGRIDClass__Proto013_PtJakartContract_id.decode, entrypoint: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'ticket_contents', 'ticket_ty', 'ticket_ticketer', 'ticket_amount', 'destination', 'entrypoint']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.ticket_contents.encodeLength +  this.value.ticket_ty.encodeLength +  this.value.ticket_ticketer.encodeLength +  this.value.ticket_amount.encodeLength +  this.value.destination.encodeLength +  this.value.entrypoint.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.ticket_contents.writeTarget(tgt) +  this.value.ticket_ty.writeTarget(tgt) +  this.value.ticket_ticketer.writeTarget(tgt) +  this.value.ticket_amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.entrypoint.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Transaction generated for Proto013PtJakartOperationContents__Transaction
export class CGRIDClass__Proto013PtJakartOperationContents__Transaction extends Box<Proto013PtJakartOperationContents__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Transaction {
        return new this(record_decoder<Proto013PtJakartOperationContents__Transaction>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transaction_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: N.decode, destination: CGRIDClass__Proto013_PtJakartTransaction_destination.decode, parameters: Option.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transaction_parameters.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Set_deposits_limit generated for Proto013PtJakartOperationContents__Set_deposits_limit
export class CGRIDClass__Proto013PtJakartOperationContents__Set_deposits_limit extends Box<Proto013PtJakartOperationContents__Set_deposits_limit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'limit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Set_deposits_limit {
        return new this(record_decoder<Proto013PtJakartOperationContents__Set_deposits_limit>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Set_deposits_limit_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, limit: Option.decode(N.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'limit']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.limit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.limit.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Seed_nonce_revelation generated for Proto013PtJakartOperationContents__Seed_nonce_revelation
export class CGRIDClass__Proto013PtJakartOperationContents__Seed_nonce_revelation extends Box<Proto013PtJakartOperationContents__Seed_nonce_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Seed_nonce_revelation {
        return new this(record_decoder<Proto013PtJakartOperationContents__Seed_nonce_revelation>({level: Int32.decode, nonce: FixedBytes.decode<32>({len: 32})}, {order: ['level', 'nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_publish generated for Proto013PtJakartOperationContents__Sc_rollup_publish
export class CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_publish extends Box<Proto013PtJakartOperationContents__Sc_rollup_publish> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_publish {
        return new this(record_decoder<Proto013PtJakartOperationContents__Sc_rollup_publish>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: Dynamic.decode(U8String.decode, width.Uint30), commitment: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_originate generated for Proto013PtJakartOperationContents__Sc_rollup_originate
export class CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_originate extends Box<Proto013PtJakartOperationContents__Sc_rollup_originate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'kind', 'boot_sector']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_originate {
        return new this(record_decoder<Proto013PtJakartOperationContents__Sc_rollup_originate>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_originate_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, kind: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_originate_kind.decode, boot_sector: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'kind', 'boot_sector']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.kind.encodeLength +  this.value.boot_sector.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.kind.writeTarget(tgt) +  this.value.boot_sector.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_cement generated for Proto013PtJakartOperationContents__Sc_rollup_cement
export class CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_cement extends Box<Proto013PtJakartOperationContents__Sc_rollup_cement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_cement {
        return new this(record_decoder<Proto013PtJakartOperationContents__Sc_rollup_cement>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_cement_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: Dynamic.decode(U8String.decode, width.Uint30), commitment: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_cement_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_add_messages generated for Proto013PtJakartOperationContents__Sc_rollup_add_messages
export class CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_add_messages extends Box<Proto013PtJakartOperationContents__Sc_rollup_add_messages> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'message']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_add_messages {
        return new this(record_decoder<Proto013PtJakartOperationContents__Sc_rollup_add_messages>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_add_messages_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: Dynamic.decode(U8String.decode, width.Uint30), message: Dynamic.decode(Sequence.decode(Dynamic.decode(U8String.decode, width.Uint30)), width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'message']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.message.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.message.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Reveal generated for Proto013PtJakartOperationContents__Reveal
export class CGRIDClass__Proto013PtJakartOperationContents__Reveal extends Box<Proto013PtJakartOperationContents__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Reveal {
        return new this(record_decoder<Proto013PtJakartOperationContents__Reveal>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Reveal_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_key: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Reveal_public_key.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Register_global_constant generated for Proto013PtJakartOperationContents__Register_global_constant
export class CGRIDClass__Proto013PtJakartOperationContents__Register_global_constant extends Box<Proto013PtJakartOperationContents__Register_global_constant> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Register_global_constant {
        return new this(record_decoder<Proto013PtJakartOperationContents__Register_global_constant>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Register_global_constant_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Proposals generated for Proto013PtJakartOperationContents__Proposals
export class CGRIDClass__Proto013PtJakartOperationContents__Proposals extends Box<Proto013PtJakartOperationContents__Proposals> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposals']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Proposals {
        return new this(record_decoder<Proto013PtJakartOperationContents__Proposals>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Proposals_source.decode, period: Int32.decode, proposals: Dynamic.decode(Sequence.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'period', 'proposals']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposals.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposals.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Preendorsement generated for Proto013PtJakartOperationContents__Preendorsement
export class CGRIDClass__Proto013PtJakartOperationContents__Preendorsement extends Box<Proto013PtJakartOperationContents__Preendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Preendorsement {
        return new this(record_decoder<Proto013PtJakartOperationContents__Preendorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Preendorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Origination generated for Proto013PtJakartOperationContents__Origination
export class CGRIDClass__Proto013PtJakartOperationContents__Origination extends Box<Proto013PtJakartOperationContents__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Origination {
        return new this(record_decoder<Proto013PtJakartOperationContents__Origination>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, balance: N.decode, delegate: Option.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Origination_delegate.decode), script: CGRIDClass__Proto013_PtJakartScriptedContracts.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Failing_noop generated for Proto013PtJakartOperationContents__Failing_noop
export class CGRIDClass__Proto013PtJakartOperationContents__Failing_noop extends Box<Proto013PtJakartOperationContents__Failing_noop> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['arbitrary']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Failing_noop {
        return new this(record_decoder<Proto013PtJakartOperationContents__Failing_noop>({arbitrary: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['arbitrary']})(p));
    };
    get encodeLength(): number {
        return (this.value.arbitrary.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.arbitrary.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Endorsement generated for Proto013PtJakartOperationContents__Endorsement
export class CGRIDClass__Proto013PtJakartOperationContents__Endorsement extends Box<Proto013PtJakartOperationContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Endorsement {
        return new this(record_decoder<Proto013PtJakartOperationContents__Endorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Endorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Double_preendorsement_evidence generated for Proto013PtJakartOperationContents__Double_preendorsement_evidence
export class CGRIDClass__Proto013PtJakartOperationContents__Double_preendorsement_evidence extends Box<Proto013PtJakartOperationContents__Double_preendorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Double_preendorsement_evidence {
        return new this(record_decoder<Proto013PtJakartOperationContents__Double_preendorsement_evidence>({op1: Dynamic.decode(CGRIDClass__Proto013_PtJakartInlinedPreendorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__Proto013_PtJakartInlinedPreendorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Double_endorsement_evidence generated for Proto013PtJakartOperationContents__Double_endorsement_evidence
export class CGRIDClass__Proto013PtJakartOperationContents__Double_endorsement_evidence extends Box<Proto013PtJakartOperationContents__Double_endorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Double_endorsement_evidence {
        return new this(record_decoder<Proto013PtJakartOperationContents__Double_endorsement_evidence>({op1: Dynamic.decode(CGRIDClass__Proto013_PtJakartInlinedEndorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__Proto013_PtJakartInlinedEndorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Double_baking_evidence generated for Proto013PtJakartOperationContents__Double_baking_evidence
export class CGRIDClass__Proto013PtJakartOperationContents__Double_baking_evidence extends Box<Proto013PtJakartOperationContents__Double_baking_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bh1', 'bh2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Double_baking_evidence {
        return new this(record_decoder<Proto013PtJakartOperationContents__Double_baking_evidence>({bh1: Dynamic.decode(CGRIDClass__Proto013_PtJakartBlock_headerAlphaFull_header.decode, width.Uint30), bh2: Dynamic.decode(CGRIDClass__Proto013_PtJakartBlock_headerAlphaFull_header.decode, width.Uint30)}, {order: ['bh1', 'bh2']})(p));
    };
    get encodeLength(): number {
        return (this.value.bh1.encodeLength +  this.value.bh2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bh1.writeTarget(tgt) +  this.value.bh2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Delegation generated for Proto013PtJakartOperationContents__Delegation
export class CGRIDClass__Proto013PtJakartOperationContents__Delegation extends Box<Proto013PtJakartOperationContents__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Delegation {
        return new this(record_decoder<Proto013PtJakartOperationContents__Delegation>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Delegation_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, delegate: Option.decode(CGRIDClass__Proto013_PtJakartOperationAlphaContents_Delegation_delegate.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Ballot generated for Proto013PtJakartOperationContents__Ballot
export class CGRIDClass__Proto013PtJakartOperationContents__Ballot extends Box<Proto013PtJakartOperationContents__Ballot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposal', 'ballot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Ballot {
        return new this(record_decoder<Proto013PtJakartOperationContents__Ballot>({source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Ballot_source.decode, period: Int32.decode, proposal: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Ballot_proposal.decode, ballot: Int8.decode}, {order: ['source', 'period', 'proposal', 'ballot']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposal.encodeLength +  this.value.ballot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposal.writeTarget(tgt) +  this.value.ballot.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto013PtJakartOperationContents__Activate_account generated for Proto013PtJakartOperationContents__Activate_account
export class CGRIDClass__Proto013PtJakartOperationContents__Activate_account extends Box<Proto013PtJakartOperationContents__Activate_account> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'secret']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents__Activate_account {
        return new this(record_decoder<Proto013PtJakartOperationContents__Activate_account>({pkh: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Activate_account_pkh.decode, secret: FixedBytes.decode<20>({len: 20})}, {order: ['pkh', 'secret']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.secret.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.secret.writeTarget(tgt));
    }
}
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_predecessor generated for BlockHeaderShellPredecessor
export class CGRIDClass__Block_headerShell_predecessor extends Box<BlockHeaderShellPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_predecessor {
        return new this(record_decoder<BlockHeaderShellPredecessor>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_operations_hash generated for BlockHeaderShellOperationsHash
export class CGRIDClass__Block_headerShell_operations_hash extends Box<BlockHeaderShellOperationsHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_list_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_operations_hash {
        return new this(record_decoder<BlockHeaderShellOperationsHash>({operation_list_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_list_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_list_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_list_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_context generated for BlockHeaderShellContext
export class CGRIDClass__Block_headerShell_context extends Box<BlockHeaderShellContext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_context {
        return new this(record_decoder<BlockHeaderShellContext>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export type BlockHeaderShellPredecessor = { block_hash: FixedBytes<32> };
export type BlockHeaderShellOperationsHash = { operation_list_list_hash: FixedBytes<32> };
export type BlockHeaderShellContext = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationContents__Tx_rollup_submit_batch = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_submit_batch_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id, content: Dynamic<Bytes,width.Uint30>, burn_limit: Option<N> };
export type Proto013PtJakartOperationContents__Tx_rollup_return_bond = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_return_bond_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id };
export type Proto013PtJakartOperationContents__Tx_rollup_remove_commitment = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_remove_commitment_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id };
export type Proto013PtJakartOperationContents__Tx_rollup_rejection = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id, level: Int32, message: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message, message_position: N, message_path: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq>,width.Uint30>, message_result_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_result_hash, message_result_path: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq>,width.Uint30>, previous_message_result: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result, previous_message_result_path: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq>,width.Uint30>, proof: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof };
export type Proto013PtJakartOperationContents__Tx_rollup_origination = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, tx_rollup_origination: Unit };
export type Proto013PtJakartOperationContents__Tx_rollup_finalize_commitment = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_finalize_commitment_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id };
export type Proto013PtJakartOperationContents__Tx_rollup_dispatch_tickets = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_source, fee: N, counter: N, gas_limit: N, storage_limit: N, tx_rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id, level: Int32, context_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash, message_index: Int31, message_result_path: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq>,width.Uint30>, tickets_info: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq>,width.Uint30> };
export type Proto013PtJakartOperationContents__Tx_rollup_commit = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto013_PtJakartTx_rollup_id, commitment: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment };
export type Proto013PtJakartOperationContents__Transfer_ticket = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transfer_ticket_source, fee: N, counter: N, gas_limit: N, storage_limit: N, ticket_contents: Dynamic<Bytes,width.Uint30>, ticket_ty: Dynamic<Bytes,width.Uint30>, ticket_ticketer: CGRIDClass__Proto013_PtJakartContract_id, ticket_amount: N, destination: CGRIDClass__Proto013_PtJakartContract_id, entrypoint: Dynamic<U8String,width.Uint30> };
export type Proto013PtJakartOperationContents__Transaction = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transaction_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: N, destination: CGRIDClass__Proto013_PtJakartTransaction_destination, parameters: Option<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Transaction_parameters> };
export type Proto013PtJakartOperationContents__Set_deposits_limit = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Set_deposits_limit_source, fee: N, counter: N, gas_limit: N, storage_limit: N, limit: Option<N> };
export type Proto013PtJakartOperationContents__Seed_nonce_revelation = { level: Int32, nonce: FixedBytes<32> };
export type Proto013PtJakartOperationContents__Sc_rollup_publish = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: Dynamic<U8String,width.Uint30>, commitment: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment };
export type Proto013PtJakartOperationContents__Sc_rollup_originate = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_originate_source, fee: N, counter: N, gas_limit: N, storage_limit: N, kind: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_originate_kind, boot_sector: Dynamic<U8String,width.Uint30> };
export type Proto013PtJakartOperationContents__Sc_rollup_cement = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_cement_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: Dynamic<U8String,width.Uint30>, commitment: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_cement_commitment };
export type Proto013PtJakartOperationContents__Sc_rollup_add_messages = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_add_messages_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: Dynamic<U8String,width.Uint30>, message: Dynamic<Sequence<Dynamic<U8String,width.Uint30>>,width.Uint30> };
export type Proto013PtJakartOperationContents__Reveal = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Reveal_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_key: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Reveal_public_key };
export type Proto013PtJakartOperationContents__Register_global_constant = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Register_global_constant_source, fee: N, counter: N, gas_limit: N, storage_limit: N, value: Dynamic<Bytes,width.Uint30> };
export type Proto013PtJakartOperationContents__Proposals = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Proposals_source, period: Int32, proposals: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq>,width.Uint30> };
export type Proto013PtJakartOperationContents__Preendorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Preendorsement_block_payload_hash };
export type Proto013PtJakartOperationContents__Origination = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, balance: N, delegate: Option<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Origination_delegate>, script: CGRIDClass__Proto013_PtJakartScriptedContracts };
export type Proto013PtJakartOperationContents__Failing_noop = { arbitrary: Dynamic<U8String,width.Uint30> };
export type Proto013PtJakartOperationContents__Endorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Endorsement_block_payload_hash };
export type Proto013PtJakartOperationContents__Double_preendorsement_evidence = { op1: Dynamic<CGRIDClass__Proto013_PtJakartInlinedPreendorsement,width.Uint30>, op2: Dynamic<CGRIDClass__Proto013_PtJakartInlinedPreendorsement,width.Uint30> };
export type Proto013PtJakartOperationContents__Double_endorsement_evidence = { op1: Dynamic<CGRIDClass__Proto013_PtJakartInlinedEndorsement,width.Uint30>, op2: Dynamic<CGRIDClass__Proto013_PtJakartInlinedEndorsement,width.Uint30> };
export type Proto013PtJakartOperationContents__Double_baking_evidence = { bh1: Dynamic<CGRIDClass__Proto013_PtJakartBlock_headerAlphaFull_header,width.Uint30>, bh2: Dynamic<CGRIDClass__Proto013_PtJakartBlock_headerAlphaFull_header,width.Uint30> };
export type Proto013PtJakartOperationContents__Delegation = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Delegation_source, fee: N, counter: N, gas_limit: N, storage_limit: N, delegate: Option<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Delegation_delegate> };
export type Proto013PtJakartOperationContents__Ballot = { source: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Ballot_source, period: Int32, proposal: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Ballot_proposal, ballot: Int8 };
export type Proto013PtJakartOperationContents__Activate_account = { pkh: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Activate_account_pkh, secret: FixedBytes<20> };
export type Proto013PtJakartTxRollupId = { rollup_hash: FixedBytes<20> };
export type Proto013PtJakartTransactionDestinationOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto013PtJakartTransactionDestination{
    Implicit = 0,
    Originated = 1,
    Tx_rollup = 2
}
export interface CGRIDMap__Proto013PtJakartTransactionDestination {
    Implicit: CGRIDClass__Proto013PtJakartTransactionDestination__Implicit,
    Originated: CGRIDClass__Proto013PtJakartTransactionDestination__Originated,
    Tx_rollup: CGRIDClass__Proto013PtJakartTransactionDestination__Tx_rollup
}
export type Proto013PtJakartTransactionDestination = { kind: CGRIDTag__Proto013PtJakartTransactionDestination.Implicit, value: CGRIDMap__Proto013PtJakartTransactionDestination['Implicit'] } | { kind: CGRIDTag__Proto013PtJakartTransactionDestination.Originated, value: CGRIDMap__Proto013PtJakartTransactionDestination['Originated'] } | { kind: CGRIDTag__Proto013PtJakartTransactionDestination.Tx_rollup, value: CGRIDMap__Proto013PtJakartTransactionDestination['Tx_rollup'] };
export type Proto013PtJakartScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export type Proto013PtJakartOperationAlphaContentsTxRollupSubmitBatchSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsTxRollupReturnBondSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsTxRollupRemoveCommitmentSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 = { context_hash: FixedBytes<32> };
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3,
    case_4 = 4,
    case_5 = 5,
    case_6 = 6,
    case_7 = 7,
    case_8 = 8,
    case_9 = 9,
    case_10 = 10,
    case_11 = 11,
    case_12 = 12,
    case_13 = 13,
    case_14 = 14,
    case_15 = 15,
    case_128 = 128,
    case_129 = 129,
    case_130 = 130,
    case_131 = 131,
    case_192 = 192,
    case_193 = 193,
    case_195 = 195,
    case_224 = 224,
    case_225 = 225,
    case_226 = 226,
    case_227 = 227
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_1,
    case_2: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_2,
    case_3: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_3,
    case_4: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_4,
    case_5: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_5,
    case_6: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_6,
    case_7: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_7,
    case_8: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_8,
    case_9: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_9,
    case_10: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_10,
    case_11: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_11,
    case_12: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_12,
    case_13: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_13,
    case_14: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_14,
    case_15: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_15,
    case_128: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_128,
    case_129: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_129,
    case_130: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_130,
    case_131: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_131,
    case_192: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_192,
    case_193: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_193,
    case_195: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_195,
    case_224: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_224,
    case_225: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_225,
    case_226: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_226,
    case_227: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq__case_227
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_1'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_2, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_2'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_3, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_3'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_4, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_4'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_5, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_5'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_6, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_6'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_7, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_7'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_8, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_8'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_9, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_9'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_10, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_10'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_11, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_11'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_12, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_12'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_13, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_13'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_14, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_14'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_15, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_15'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_128, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_128'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_129, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_129'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_130, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_130'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_131, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_131'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_192, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_192'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_193, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_193'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_195, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_195'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_224, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_224'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_225, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_225'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_226, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_226'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq.case_227, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index3Index0DenestDynDenestSeq['case_227'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 = { context_hash: FixedBytes<32> };
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3,
    case_4 = 4,
    case_5 = 5,
    case_6 = 6,
    case_7 = 7,
    case_8 = 8,
    case_9 = 9,
    case_10 = 10,
    case_11 = 11,
    case_12 = 12,
    case_13 = 13,
    case_14 = 14,
    case_15 = 15,
    case_128 = 128,
    case_129 = 129,
    case_130 = 130,
    case_131 = 131,
    case_192 = 192,
    case_193 = 193,
    case_195 = 195,
    case_224 = 224,
    case_225 = 225,
    case_226 = 226,
    case_227 = 227
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_1,
    case_2: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_2,
    case_3: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_3,
    case_4: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_4,
    case_5: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_5,
    case_6: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_6,
    case_7: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_7,
    case_8: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_8,
    case_9: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_9,
    case_10: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_10,
    case_11: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_11,
    case_12: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_12,
    case_13: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_13,
    case_14: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_14,
    case_15: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_15,
    case_128: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_128,
    case_129: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_129,
    case_130: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_130,
    case_131: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_131,
    case_192: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_192,
    case_193: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_193,
    case_195: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_195,
    case_224: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_224,
    case_225: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_225,
    case_226: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_226,
    case_227: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq__case_227
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_1'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_2, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_2'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_3, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_3'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_4, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_4'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_5, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_5'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_6, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_6'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_7, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_7'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_8, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_8'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_9, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_9'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_10, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_10'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_11, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_11'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_12, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_12'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_13, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_13'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_14, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_14'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_15, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_15'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_128, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_128'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_129, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_129'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_130, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_130'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_131, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_131'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_192, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_192'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_193, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_193'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_195, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_195'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_224, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_224'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_225, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_225'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_226, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_226'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq.case_227, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index3Index0DenestDynDenestSeq['case_227'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 = { context_hash: FixedBytes<32> };
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3,
    case_4 = 4,
    case_5 = 5,
    case_6 = 6,
    case_7 = 7,
    case_8 = 8,
    case_9 = 9,
    case_10 = 10,
    case_11 = 11,
    case_12 = 12,
    case_13 = 13,
    case_14 = 14,
    case_15 = 15,
    case_128 = 128,
    case_129 = 129,
    case_130 = 130,
    case_131 = 131,
    case_192 = 192,
    case_193 = 193,
    case_195 = 195,
    case_224 = 224,
    case_225 = 225,
    case_226 = 226,
    case_227 = 227
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_1,
    case_2: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_2,
    case_3: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_3,
    case_4: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_4,
    case_5: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_5,
    case_6: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_6,
    case_7: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_7,
    case_8: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_8,
    case_9: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_9,
    case_10: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_10,
    case_11: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_11,
    case_12: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_12,
    case_13: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_13,
    case_14: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_14,
    case_15: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_15,
    case_128: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_128,
    case_129: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_129,
    case_130: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_130,
    case_131: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_131,
    case_192: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_192,
    case_193: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_193,
    case_195: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_195,
    case_224: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_224,
    case_225: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_225,
    case_226: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_226,
    case_227: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq__case_227
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_1'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_2, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_2'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_3, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_3'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_4, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_4'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_5, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_5'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_6, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_6'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_7, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_7'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_8, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_8'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_9, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_9'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_10, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_10'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_11, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_11'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_12, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_12'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_13, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_13'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_14, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_14'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_15, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_15'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_128, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_128'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_129, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_129'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_130, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_130'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_131, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_131'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_192, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_192'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_193, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_193'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_195, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_195'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_224, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_224'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_225, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_225'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_226, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_226'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq.case_227, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index3Index0DenestDynDenestSeq['case_227'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Index1 = [Unit, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Index1 = [Unit, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_index1_index0, Unit];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index1 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1Index0 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Index1 = [CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index0, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_index1_index1];
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case224Case225Case226Case227Index2 = { context_hash: FixedBytes<32> };
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case131DenestDynDenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_131_denest_dyn_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case130DenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_130_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1{
    case_0 = 0,
    case_1 = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1__case_1
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1 = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeqIndex1['case_1'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeqCase0Case8Case4Case12Case1Case9Case5Case13Case2Case10Case6Case14Case3Case11Case7Case15Case129DenestSeq = [Dynamic<Bytes,width.Uint8>, CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_proof_case_0_case_2_case_1_case_3_index3_index0_denest_dyn_denest_seq_case_0_case_8_case_4_case_12_case_1_case_9_case_5_case_13_case_2_case_10_case_6_case_14_case_3_case_11_case_7_case_15_case_129_denest_seq_index1];
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3,
    case_4 = 4,
    case_5 = 5,
    case_6 = 6,
    case_7 = 7,
    case_8 = 8,
    case_9 = 9,
    case_10 = 10,
    case_11 = 11,
    case_12 = 12,
    case_13 = 13,
    case_14 = 14,
    case_15 = 15,
    case_128 = 128,
    case_129 = 129,
    case_130 = 130,
    case_131 = 131,
    case_192 = 192,
    case_193 = 193,
    case_195 = 195,
    case_224 = 224,
    case_225 = 225,
    case_226 = 226,
    case_227 = 227
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_1,
    case_2: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_2,
    case_3: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_3,
    case_4: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_4,
    case_5: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_5,
    case_6: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_6,
    case_7: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_7,
    case_8: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_8,
    case_9: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_9,
    case_10: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_10,
    case_11: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_11,
    case_12: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_12,
    case_13: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_13,
    case_14: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_14,
    case_15: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_15,
    case_128: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_128,
    case_129: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_129,
    case_130: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_130,
    case_131: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_131,
    case_192: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_192,
    case_193: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_193,
    case_195: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_195,
    case_224: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_224,
    case_225: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_225,
    case_226: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_226,
    case_227: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq__case_227
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_1'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_2, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_2'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_3, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_3'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_4, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_4'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_5, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_5'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_6, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_6'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_7, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_7'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_8, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_8'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_9, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_9'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_10, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_10'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_11, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_11'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_12, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_12'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_13, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_13'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_14, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_14'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_15, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_15'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_128, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_128'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_129, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_129'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_130, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_130'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_131, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_131'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_192, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_192'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_193, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_193'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_195, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_195'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_224, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_224'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_225, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_225'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_226, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_226'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq.case_227, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index3Index0DenestDynDenestSeq['case_227'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index2 = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProofCase0Case2Case1Case3Index1 = { context_hash: FixedBytes<32> };
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_1,
    case_2: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_2,
    case_3: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof__case_3
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof['case_1'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof.case_2, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof['case_2'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof.case_3, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionProof['case_3'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash = { withdraw_list_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionPreviousMessageResult = { context_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash, withdraw_list_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageResultHash = { message_result_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq = { inbox_list_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash = { script_expr: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination = { tx_rollup_l2_address: FixedBytes<20> };
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1,
    case_2: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2,
    case_3: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_1'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_2, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_2'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_3, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_3'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessageDepositDeposit = { sender: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender, destination: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination, ticket_hash: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash, amount: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount };
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage{
    Batch = 0,
    Deposit = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage {
    Batch: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Batch,
    Deposit: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage__Deposit
}
export type Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage.Batch, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage['Batch'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage.Deposit, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupRejectionMessage['Deposit'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupOriginationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsTxRollupFinalizeCommitmentSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount {
    case_0: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0,
    case_1: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1,
    case_2: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2,
    case_3: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3
}
export type Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_0, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_0'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_1, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_1'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_2, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_2'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_3, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_3'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq = { contents: Dynamic<Bytes,width.Uint30>, ty: Dynamic<Bytes,width.Uint30>, ticketer: CGRIDClass__Proto013_PtJakartContract_id, amount: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount, claimer: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer };
export type Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupDispatchTicketsContextHash = { context_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupCommitSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor{
    None = 0,
    Some = 1
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor {
    None: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None,
    Some: CGRIDClass__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some
}
export type Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor.None, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor['None'] } | { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor.Some, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentPredecessor['Some'] };
export type Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq = { message_result_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot = { inbox_list_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsTxRollupCommitCommitment = { level: Int32, messages: Dynamic<Sequence<CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq>,width.Uint30>, predecessor: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_predecessor, inbox_merkle_root: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root };
export type Proto013PtJakartOperationAlphaContentsTransferTicketSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsTransactionSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsTransactionParameters = { entrypoint: CGRIDClass__Proto013_PtJakartEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto013PtJakartOperationAlphaContentsSetDepositsLimitSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsScRollupPublishSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentPredecessor = { commitment_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsScRollupPublishCommitmentCompressedState = { state_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsScRollupPublishCommitment = { compressed_state: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment_compressed_state, inbox_level: Int32, predecessor: CGRIDClass__Proto013_PtJakartOperationAlphaContents_Sc_rollup_publish_commitment_predecessor, number_of_messages: Int32, number_of_ticks: Int32 };
export type Proto013PtJakartOperationAlphaContentsScRollupOriginateSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind{
    Example_arith_smart_contract_rollup_kind = 0
}
export interface CGRIDMap__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind {
    Example_arith_smart_contract_rollup_kind: CGRIDClass__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind__Example_arith_smart_contract_rollup_kind
}
export type Proto013PtJakartOperationAlphaContentsScRollupOriginateKind = { kind: CGRIDTag__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind.Example_arith_smart_contract_rollup_kind, value: CGRIDMap__Proto013PtJakartOperationAlphaContentsScRollupOriginateKind['Example_arith_smart_contract_rollup_kind'] };
export type Proto013PtJakartOperationAlphaContentsScRollupCementSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsScRollupCementCommitment = { commitment_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsScRollupAddMessagesSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsRevealSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto013PtJakartOperationAlphaContentsRegisterGlobalConstantSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsProposalsSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsProposalsProposalsDenestDynDenestSeq = { protocol_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsPreendorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsOriginationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsEndorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsDelegationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsBallotSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto013PtJakartOperationAlphaContentsBallotProposal = { protocol_hash: FixedBytes<32> };
export type Proto013PtJakartOperationAlphaContentsActivateAccountPkh = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto013PtJakartInlinedPreendorsementSignature = { signature_v0: FixedBytes<64> };
export type Proto013PtJakartInlinedPreendorsementContentsPreendorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export enum CGRIDTag__Proto013PtJakartInlinedPreendorsementContents{
    Preendorsement = 20
}
export interface CGRIDMap__Proto013PtJakartInlinedPreendorsementContents {
    Preendorsement: CGRIDClass__Proto013PtJakartInlinedPreendorsementContents__Preendorsement
}
export type Proto013PtJakartInlinedPreendorsementContents = { kind: CGRIDTag__Proto013PtJakartInlinedPreendorsementContents.Preendorsement, value: CGRIDMap__Proto013PtJakartInlinedPreendorsementContents['Preendorsement'] };
export type Proto013PtJakartInlinedPreendorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__Proto013_PtJakartInlinedPreendorsementContents, signature: Nullable<CGRIDClass__Proto013_PtJakartInlinedPreendorsement_signature> };
export type Proto013PtJakartInlinedEndorsementSignature = { signature_v0: FixedBytes<64> };
export type Proto013PtJakartInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export enum CGRIDTag__Proto013PtJakartInlinedEndorsementMempoolContents{
    Endorsement = 21
}
export interface CGRIDMap__Proto013PtJakartInlinedEndorsementMempoolContents {
    Endorsement: CGRIDClass__Proto013PtJakartInlinedEndorsementMempoolContents__Endorsement
}
export type Proto013PtJakartInlinedEndorsementMempoolContents = { kind: CGRIDTag__Proto013PtJakartInlinedEndorsementMempoolContents.Endorsement, value: CGRIDMap__Proto013PtJakartInlinedEndorsementMempoolContents['Endorsement'] };
export type Proto013PtJakartInlinedEndorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__Proto013_PtJakartInlinedEndorsement_mempoolContents, signature: Nullable<CGRIDClass__Proto013_PtJakartInlinedEndorsement_signature> };
export enum CGRIDTag__Proto013PtJakartEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto013PtJakartEntrypoint {
    _default: CGRIDClass__Proto013PtJakartEntrypoint___default,
    root: CGRIDClass__Proto013PtJakartEntrypoint__root,
    _do: CGRIDClass__Proto013PtJakartEntrypoint___do,
    set_delegate: CGRIDClass__Proto013PtJakartEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto013PtJakartEntrypoint__remove_delegate,
    named: CGRIDClass__Proto013PtJakartEntrypoint__named
}
export type Proto013PtJakartEntrypoint = { kind: CGRIDTag__Proto013PtJakartEntrypoint._default, value: CGRIDMap__Proto013PtJakartEntrypoint['_default'] } | { kind: CGRIDTag__Proto013PtJakartEntrypoint.root, value: CGRIDMap__Proto013PtJakartEntrypoint['root'] } | { kind: CGRIDTag__Proto013PtJakartEntrypoint._do, value: CGRIDMap__Proto013PtJakartEntrypoint['_do'] } | { kind: CGRIDTag__Proto013PtJakartEntrypoint.set_delegate, value: CGRIDMap__Proto013PtJakartEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto013PtJakartEntrypoint.remove_delegate, value: CGRIDMap__Proto013PtJakartEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto013PtJakartEntrypoint.named, value: CGRIDMap__Proto013PtJakartEntrypoint['named'] };
export type Proto013PtJakartContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto013PtJakartContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto013PtJakartContractId {
    Implicit: CGRIDClass__Proto013PtJakartContractId__Implicit,
    Originated: CGRIDClass__Proto013PtJakartContractId__Originated
}
export type Proto013PtJakartContractId = { kind: CGRIDTag__Proto013PtJakartContractId.Implicit, value: CGRIDMap__Proto013PtJakartContractId['Implicit'] } | { kind: CGRIDTag__Proto013PtJakartContractId.Originated, value: CGRIDMap__Proto013PtJakartContractId['Originated'] };
export type Proto013PtJakartBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto013PtJakartBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type Proto013PtJakartBlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto013PtJakartBlockHeaderAlphaFullHeader = { level: Int32, proto: Uint8, predecessor: CGRIDClass__Block_headerShell_predecessor, timestamp: Int64, validation_pass: Uint8, operations_hash: CGRIDClass__Block_headerShell_operations_hash, fitness: Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>, context: CGRIDClass__Block_headerShell_context, payload_hash: CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto013_PtJakartBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_toggle_vote: Int8, signature: CGRIDClass__Proto013_PtJakartBlock_headerAlphaSigned_contents_signature };
export enum CGRIDTag__Proto013PtJakartOperationContents{
    Seed_nonce_revelation = 1,
    Double_endorsement_evidence = 2,
    Double_baking_evidence = 3,
    Activate_account = 4,
    Proposals = 5,
    Ballot = 6,
    Double_preendorsement_evidence = 7,
    Failing_noop = 17,
    Preendorsement = 20,
    Endorsement = 21,
    Reveal = 107,
    Transaction = 108,
    Origination = 109,
    Delegation = 110,
    Register_global_constant = 111,
    Set_deposits_limit = 112,
    Tx_rollup_origination = 150,
    Tx_rollup_submit_batch = 151,
    Tx_rollup_commit = 152,
    Tx_rollup_return_bond = 153,
    Tx_rollup_finalize_commitment = 154,
    Tx_rollup_remove_commitment = 155,
    Tx_rollup_rejection = 156,
    Tx_rollup_dispatch_tickets = 157,
    Transfer_ticket = 158,
    Sc_rollup_originate = 200,
    Sc_rollup_add_messages = 201,
    Sc_rollup_cement = 202,
    Sc_rollup_publish = 203
}
export interface CGRIDMap__Proto013PtJakartOperationContents {
    Seed_nonce_revelation: CGRIDClass__Proto013PtJakartOperationContents__Seed_nonce_revelation,
    Double_endorsement_evidence: CGRIDClass__Proto013PtJakartOperationContents__Double_endorsement_evidence,
    Double_baking_evidence: CGRIDClass__Proto013PtJakartOperationContents__Double_baking_evidence,
    Activate_account: CGRIDClass__Proto013PtJakartOperationContents__Activate_account,
    Proposals: CGRIDClass__Proto013PtJakartOperationContents__Proposals,
    Ballot: CGRIDClass__Proto013PtJakartOperationContents__Ballot,
    Double_preendorsement_evidence: CGRIDClass__Proto013PtJakartOperationContents__Double_preendorsement_evidence,
    Failing_noop: CGRIDClass__Proto013PtJakartOperationContents__Failing_noop,
    Preendorsement: CGRIDClass__Proto013PtJakartOperationContents__Preendorsement,
    Endorsement: CGRIDClass__Proto013PtJakartOperationContents__Endorsement,
    Reveal: CGRIDClass__Proto013PtJakartOperationContents__Reveal,
    Transaction: CGRIDClass__Proto013PtJakartOperationContents__Transaction,
    Origination: CGRIDClass__Proto013PtJakartOperationContents__Origination,
    Delegation: CGRIDClass__Proto013PtJakartOperationContents__Delegation,
    Register_global_constant: CGRIDClass__Proto013PtJakartOperationContents__Register_global_constant,
    Set_deposits_limit: CGRIDClass__Proto013PtJakartOperationContents__Set_deposits_limit,
    Tx_rollup_origination: CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_origination,
    Tx_rollup_submit_batch: CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_submit_batch,
    Tx_rollup_commit: CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_commit,
    Tx_rollup_return_bond: CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_return_bond,
    Tx_rollup_finalize_commitment: CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_finalize_commitment,
    Tx_rollup_remove_commitment: CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_remove_commitment,
    Tx_rollup_rejection: CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_rejection,
    Tx_rollup_dispatch_tickets: CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_dispatch_tickets,
    Transfer_ticket: CGRIDClass__Proto013PtJakartOperationContents__Transfer_ticket,
    Sc_rollup_originate: CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_originate,
    Sc_rollup_add_messages: CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_add_messages,
    Sc_rollup_cement: CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_cement,
    Sc_rollup_publish: CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_publish
}
export type Proto013PtJakartOperationContents = { kind: CGRIDTag__Proto013PtJakartOperationContents.Seed_nonce_revelation, value: CGRIDMap__Proto013PtJakartOperationContents['Seed_nonce_revelation'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Double_endorsement_evidence, value: CGRIDMap__Proto013PtJakartOperationContents['Double_endorsement_evidence'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Double_baking_evidence, value: CGRIDMap__Proto013PtJakartOperationContents['Double_baking_evidence'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Activate_account, value: CGRIDMap__Proto013PtJakartOperationContents['Activate_account'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Proposals, value: CGRIDMap__Proto013PtJakartOperationContents['Proposals'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Ballot, value: CGRIDMap__Proto013PtJakartOperationContents['Ballot'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Double_preendorsement_evidence, value: CGRIDMap__Proto013PtJakartOperationContents['Double_preendorsement_evidence'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Failing_noop, value: CGRIDMap__Proto013PtJakartOperationContents['Failing_noop'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Preendorsement, value: CGRIDMap__Proto013PtJakartOperationContents['Preendorsement'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Endorsement, value: CGRIDMap__Proto013PtJakartOperationContents['Endorsement'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Reveal, value: CGRIDMap__Proto013PtJakartOperationContents['Reveal'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Transaction, value: CGRIDMap__Proto013PtJakartOperationContents['Transaction'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Origination, value: CGRIDMap__Proto013PtJakartOperationContents['Origination'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Delegation, value: CGRIDMap__Proto013PtJakartOperationContents['Delegation'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Register_global_constant, value: CGRIDMap__Proto013PtJakartOperationContents['Register_global_constant'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Set_deposits_limit, value: CGRIDMap__Proto013PtJakartOperationContents['Set_deposits_limit'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_origination, value: CGRIDMap__Proto013PtJakartOperationContents['Tx_rollup_origination'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_submit_batch, value: CGRIDMap__Proto013PtJakartOperationContents['Tx_rollup_submit_batch'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_commit, value: CGRIDMap__Proto013PtJakartOperationContents['Tx_rollup_commit'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_return_bond, value: CGRIDMap__Proto013PtJakartOperationContents['Tx_rollup_return_bond'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_finalize_commitment, value: CGRIDMap__Proto013PtJakartOperationContents['Tx_rollup_finalize_commitment'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_remove_commitment, value: CGRIDMap__Proto013PtJakartOperationContents['Tx_rollup_remove_commitment'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_rejection, value: CGRIDMap__Proto013PtJakartOperationContents['Tx_rollup_rejection'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_dispatch_tickets, value: CGRIDMap__Proto013PtJakartOperationContents['Tx_rollup_dispatch_tickets'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Transfer_ticket, value: CGRIDMap__Proto013PtJakartOperationContents['Transfer_ticket'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Sc_rollup_originate, value: CGRIDMap__Proto013PtJakartOperationContents['Sc_rollup_originate'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Sc_rollup_add_messages, value: CGRIDMap__Proto013PtJakartOperationContents['Sc_rollup_add_messages'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Sc_rollup_cement, value: CGRIDMap__Proto013PtJakartOperationContents['Sc_rollup_cement'] } | { kind: CGRIDTag__Proto013PtJakartOperationContents.Sc_rollup_publish, value: CGRIDMap__Proto013PtJakartOperationContents['Sc_rollup_publish'] };
export function proto013ptjakartoperationcontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartOperationContents,Proto013PtJakartOperationContents> {
    function f(disc: CGRIDTag__Proto013PtJakartOperationContents) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartOperationContents.Seed_nonce_revelation: return CGRIDClass__Proto013PtJakartOperationContents__Seed_nonce_revelation.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Double_endorsement_evidence: return CGRIDClass__Proto013PtJakartOperationContents__Double_endorsement_evidence.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Double_baking_evidence: return CGRIDClass__Proto013PtJakartOperationContents__Double_baking_evidence.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Activate_account: return CGRIDClass__Proto013PtJakartOperationContents__Activate_account.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Proposals: return CGRIDClass__Proto013PtJakartOperationContents__Proposals.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Ballot: return CGRIDClass__Proto013PtJakartOperationContents__Ballot.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Double_preendorsement_evidence: return CGRIDClass__Proto013PtJakartOperationContents__Double_preendorsement_evidence.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Failing_noop: return CGRIDClass__Proto013PtJakartOperationContents__Failing_noop.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Preendorsement: return CGRIDClass__Proto013PtJakartOperationContents__Preendorsement.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Endorsement: return CGRIDClass__Proto013PtJakartOperationContents__Endorsement.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Reveal: return CGRIDClass__Proto013PtJakartOperationContents__Reveal.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Transaction: return CGRIDClass__Proto013PtJakartOperationContents__Transaction.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Origination: return CGRIDClass__Proto013PtJakartOperationContents__Origination.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Delegation: return CGRIDClass__Proto013PtJakartOperationContents__Delegation.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Register_global_constant: return CGRIDClass__Proto013PtJakartOperationContents__Register_global_constant.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Set_deposits_limit: return CGRIDClass__Proto013PtJakartOperationContents__Set_deposits_limit.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_origination: return CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_origination.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_submit_batch: return CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_submit_batch.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_commit: return CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_commit.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_return_bond: return CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_return_bond.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_finalize_commitment: return CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_finalize_commitment.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_remove_commitment: return CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_remove_commitment.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_rejection: return CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_rejection.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Tx_rollup_dispatch_tickets: return CGRIDClass__Proto013PtJakartOperationContents__Tx_rollup_dispatch_tickets.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Transfer_ticket: return CGRIDClass__Proto013PtJakartOperationContents__Transfer_ticket.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Sc_rollup_originate: return CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_originate.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Sc_rollup_add_messages: return CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_add_messages.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Sc_rollup_cement: return CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_cement.decode;
            case CGRIDTag__Proto013PtJakartOperationContents.Sc_rollup_publish: return CGRIDClass__Proto013PtJakartOperationContents__Sc_rollup_publish.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartOperationContents => Object.values(CGRIDTag__Proto013PtJakartOperationContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013PtJakartOperationContents extends Box<Proto013PtJakartOperationContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartOperationContents>, Proto013PtJakartOperationContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartOperationContents {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartoperationcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto013_ptjakart_operation_contents_encoder = (value: Proto013PtJakartOperationContents): OutputBytes => {
    return variant_encoder<KindOf<Proto013PtJakartOperationContents>, Proto013PtJakartOperationContents>(width.Uint8)(value);
}
export const proto013_ptjakart_operation_contents_decoder = (p: Parser): Proto013PtJakartOperationContents => {
    return variant_decoder(width.Uint8)(proto013ptjakartoperationcontents_mkDecoder())(p);
}
