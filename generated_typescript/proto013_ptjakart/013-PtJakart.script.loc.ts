import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int31 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto013PtJakartScriptLoc = Int31;
export class CGRIDClass__Proto013PtJakartScriptLoc extends Box<Proto013PtJakartScriptLoc> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartScriptLoc {
        return new this(Int31.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto013_ptjakart_script_loc_encoder = (value: Proto013PtJakartScriptLoc): OutputBytes => {
    return value.encode();
}
export const proto013_ptjakart_script_loc_decoder = (p: Parser): Proto013PtJakartScriptLoc => {
    return Int31.decode(p);
}
