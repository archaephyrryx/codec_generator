import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto013PtJakartFa12TokenTransfer = { token_contract: Dynamic<U8String,width.Uint30>, destination: Dynamic<U8String,width.Uint30>, amount: Z, tez_amount: Option<Dynamic<U8String,width.Uint30>>, fee: Option<Dynamic<U8String,width.Uint30>>, gas_limit: Option<N>, storage_limit: Option<Z> };
export class CGRIDClass__Proto013PtJakartFa12TokenTransfer extends Box<Proto013PtJakartFa12TokenTransfer> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['token_contract', 'destination', 'amount', 'tez_amount', 'fee', 'gas_limit', 'storage_limit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartFa12TokenTransfer {
        return new this(record_decoder<Proto013PtJakartFa12TokenTransfer>({token_contract: Dynamic.decode(U8String.decode, width.Uint30), destination: Dynamic.decode(U8String.decode, width.Uint30), amount: Z.decode, tez_amount: Option.decode(Dynamic.decode(U8String.decode, width.Uint30)), fee: Option.decode(Dynamic.decode(U8String.decode, width.Uint30)), gas_limit: Option.decode(N.decode), storage_limit: Option.decode(Z.decode)}, {order: ['token_contract', 'destination', 'amount', 'tez_amount', 'fee', 'gas_limit', 'storage_limit']})(p));
    };
    get encodeLength(): number {
        return (this.value.token_contract.encodeLength +  this.value.destination.encodeLength +  this.value.amount.encodeLength +  this.value.tez_amount.encodeLength +  this.value.fee.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.token_contract.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.tez_amount.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt));
    }
}
export const proto013_ptjakart_fa1_2_token_transfer_encoder = (value: Proto013PtJakartFa12TokenTransfer): OutputBytes => {
    return record_encoder({order: ['token_contract', 'destination', 'amount', 'tez_amount', 'fee', 'gas_limit', 'storage_limit']})(value);
}
export const proto013_ptjakart_fa1_2_token_transfer_decoder = (p: Parser): Proto013PtJakartFa12TokenTransfer => {
    return record_decoder<Proto013PtJakartFa12TokenTransfer>({token_contract: Dynamic.decode(U8String.decode, width.Uint30), destination: Dynamic.decode(U8String.decode, width.Uint30), amount: Z.decode, tez_amount: Option.decode(Dynamic.decode(U8String.decode, width.Uint30)), fee: Option.decode(Dynamic.decode(U8String.decode, width.Uint30)), gas_limit: Option.decode(N.decode), storage_limit: Option.decode(Z.decode)}, {order: ['token_contract', 'destination', 'amount', 'tez_amount', 'fee', 'gas_limit', 'storage_limit']})(p);
}
