import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto013PtJakartVotingPeriodKind__exploration generated for Proto013PtJakartVotingPeriodKind__exploration
export class CGRIDClass__Proto013PtJakartVotingPeriodKind__exploration extends Box<Proto013PtJakartVotingPeriodKind__exploration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartVotingPeriodKind__exploration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartVotingPeriodKind__Proposal generated for Proto013PtJakartVotingPeriodKind__Proposal
export class CGRIDClass__Proto013PtJakartVotingPeriodKind__Proposal extends Box<Proto013PtJakartVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartVotingPeriodKind__Promotion generated for Proto013PtJakartVotingPeriodKind__Promotion
export class CGRIDClass__Proto013PtJakartVotingPeriodKind__Promotion extends Box<Proto013PtJakartVotingPeriodKind__Promotion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartVotingPeriodKind__Promotion {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartVotingPeriodKind__Cooldown generated for Proto013PtJakartVotingPeriodKind__Cooldown
export class CGRIDClass__Proto013PtJakartVotingPeriodKind__Cooldown extends Box<Proto013PtJakartVotingPeriodKind__Cooldown> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartVotingPeriodKind__Cooldown {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartVotingPeriodKind__Adoption generated for Proto013PtJakartVotingPeriodKind__Adoption
export class CGRIDClass__Proto013PtJakartVotingPeriodKind__Adoption extends Box<Proto013PtJakartVotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartVotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto013PtJakartVotingPeriodKind__exploration = Unit;
export type Proto013PtJakartVotingPeriodKind__Proposal = Unit;
export type Proto013PtJakartVotingPeriodKind__Promotion = Unit;
export type Proto013PtJakartVotingPeriodKind__Cooldown = Unit;
export type Proto013PtJakartVotingPeriodKind__Adoption = Unit;
export enum CGRIDTag__Proto013PtJakartVotingPeriodKind{
    Proposal = 0,
    exploration = 1,
    Cooldown = 2,
    Promotion = 3,
    Adoption = 4
}
export interface CGRIDMap__Proto013PtJakartVotingPeriodKind {
    Proposal: CGRIDClass__Proto013PtJakartVotingPeriodKind__Proposal,
    exploration: CGRIDClass__Proto013PtJakartVotingPeriodKind__exploration,
    Cooldown: CGRIDClass__Proto013PtJakartVotingPeriodKind__Cooldown,
    Promotion: CGRIDClass__Proto013PtJakartVotingPeriodKind__Promotion,
    Adoption: CGRIDClass__Proto013PtJakartVotingPeriodKind__Adoption
}
export type Proto013PtJakartVotingPeriodKind = { kind: CGRIDTag__Proto013PtJakartVotingPeriodKind.Proposal, value: CGRIDMap__Proto013PtJakartVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto013PtJakartVotingPeriodKind.exploration, value: CGRIDMap__Proto013PtJakartVotingPeriodKind['exploration'] } | { kind: CGRIDTag__Proto013PtJakartVotingPeriodKind.Cooldown, value: CGRIDMap__Proto013PtJakartVotingPeriodKind['Cooldown'] } | { kind: CGRIDTag__Proto013PtJakartVotingPeriodKind.Promotion, value: CGRIDMap__Proto013PtJakartVotingPeriodKind['Promotion'] } | { kind: CGRIDTag__Proto013PtJakartVotingPeriodKind.Adoption, value: CGRIDMap__Proto013PtJakartVotingPeriodKind['Adoption'] };
export function proto013ptjakartvotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartVotingPeriodKind,Proto013PtJakartVotingPeriodKind> {
    function f(disc: CGRIDTag__Proto013PtJakartVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartVotingPeriodKind.Proposal: return CGRIDClass__Proto013PtJakartVotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto013PtJakartVotingPeriodKind.exploration: return CGRIDClass__Proto013PtJakartVotingPeriodKind__exploration.decode;
            case CGRIDTag__Proto013PtJakartVotingPeriodKind.Cooldown: return CGRIDClass__Proto013PtJakartVotingPeriodKind__Cooldown.decode;
            case CGRIDTag__Proto013PtJakartVotingPeriodKind.Promotion: return CGRIDClass__Proto013PtJakartVotingPeriodKind__Promotion.decode;
            case CGRIDTag__Proto013PtJakartVotingPeriodKind.Adoption: return CGRIDClass__Proto013PtJakartVotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartVotingPeriodKind => Object.values(CGRIDTag__Proto013PtJakartVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013PtJakartVotingPeriodKind extends Box<Proto013PtJakartVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartVotingPeriodKind>, Proto013PtJakartVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartVotingPeriodKind {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartvotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto013_ptjakart_voting_period_kind_encoder = (value: Proto013PtJakartVotingPeriodKind): OutputBytes => {
    return variant_encoder<KindOf<Proto013PtJakartVotingPeriodKind>, Proto013PtJakartVotingPeriodKind>(width.Uint8)(value);
}
export const proto013_ptjakart_voting_period_kind_decoder = (p: Parser): Proto013PtJakartVotingPeriodKind => {
    return variant_decoder(width.Uint8)(proto013ptjakartvotingperiodkind_mkDecoder())(p);
}
