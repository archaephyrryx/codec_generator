import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto013PtJakartFitnessLockedRound__Some generated for Proto013PtJakartFitnessLockedRound__Some
export class CGRIDClass__Proto013PtJakartFitnessLockedRound__Some extends Box<Proto013PtJakartFitnessLockedRound__Some> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartFitnessLockedRound__Some {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto013PtJakartFitnessLockedRound__None generated for Proto013PtJakartFitnessLockedRound__None
export class CGRIDClass__Proto013PtJakartFitnessLockedRound__None extends Box<Proto013PtJakartFitnessLockedRound__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartFitnessLockedRound__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto013PtJakartFitnessLockedRound__Some = Int32;
export type Proto013PtJakartFitnessLockedRound__None = Unit;
// Class CGRIDClass__Proto013_PtJakartFitness_locked_round generated for Proto013PtJakartFitnessLockedRound
export function proto013ptjakartfitnesslockedround_mkDecoder(): VariantDecoder<CGRIDTag__Proto013PtJakartFitnessLockedRound,Proto013PtJakartFitnessLockedRound> {
    function f(disc: CGRIDTag__Proto013PtJakartFitnessLockedRound) {
        switch (disc) {
            case CGRIDTag__Proto013PtJakartFitnessLockedRound.None: return CGRIDClass__Proto013PtJakartFitnessLockedRound__None.decode;
            case CGRIDTag__Proto013PtJakartFitnessLockedRound.Some: return CGRIDClass__Proto013PtJakartFitnessLockedRound__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto013PtJakartFitnessLockedRound => Object.values(CGRIDTag__Proto013PtJakartFitnessLockedRound).includes(tagval);
    return f;
}
export class CGRIDClass__Proto013_PtJakartFitness_locked_round extends Box<Proto013PtJakartFitnessLockedRound> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto013PtJakartFitnessLockedRound>, Proto013PtJakartFitnessLockedRound>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013_PtJakartFitness_locked_round {
        return new this(variant_decoder(width.Uint8)(proto013ptjakartfitnesslockedround_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__Proto013PtJakartFitnessLockedRound{
    None = 0,
    Some = 1
}
export interface CGRIDMap__Proto013PtJakartFitnessLockedRound {
    None: CGRIDClass__Proto013PtJakartFitnessLockedRound__None,
    Some: CGRIDClass__Proto013PtJakartFitnessLockedRound__Some
}
export type Proto013PtJakartFitnessLockedRound = { kind: CGRIDTag__Proto013PtJakartFitnessLockedRound.None, value: CGRIDMap__Proto013PtJakartFitnessLockedRound['None'] } | { kind: CGRIDTag__Proto013PtJakartFitnessLockedRound.Some, value: CGRIDMap__Proto013PtJakartFitnessLockedRound['Some'] };
export type Proto013PtJakartFitness = { level: Int32, locked_round: CGRIDClass__Proto013_PtJakartFitness_locked_round, predecessor_round: Int32, round: Int32 };
export class CGRIDClass__Proto013PtJakartFitness extends Box<Proto013PtJakartFitness> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartFitness {
        return new this(record_decoder<Proto013PtJakartFitness>({level: Int32.decode, locked_round: CGRIDClass__Proto013_PtJakartFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.locked_round.encodeLength +  this.value.predecessor_round.encodeLength +  this.value.round.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.locked_round.writeTarget(tgt) +  this.value.predecessor_round.writeTarget(tgt) +  this.value.round.writeTarget(tgt));
    }
}
export const proto013_ptjakart_fitness_encoder = (value: Proto013PtJakartFitness): OutputBytes => {
    return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(value);
}
export const proto013_ptjakart_fitness_decoder = (p: Parser): Proto013PtJakartFitness => {
    return record_decoder<Proto013PtJakartFitness>({level: Int32.decode, locked_round: CGRIDClass__Proto013_PtJakartFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p);
}
