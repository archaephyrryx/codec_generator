import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto013PtJakartTez = N;
export class CGRIDClass__Proto013PtJakartTez extends Box<Proto013PtJakartTez> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartTez {
        return new this(N.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto013_ptjakart_tez_encoder = (value: Proto013PtJakartTez): OutputBytes => {
    return value.encode();
}
export const proto013_ptjakart_tez_decoder = (p: Parser): Proto013PtJakartTez => {
    return N.decode(p);
}
