import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto013PtJakartPeriod = Int64;
export class CGRIDClass__Proto013PtJakartPeriod extends Box<Proto013PtJakartPeriod> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto013PtJakartPeriod {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto013_ptjakart_period_encoder = (value: Proto013PtJakartPeriod): OutputBytes => {
    return value.encode();
}
export const proto013_ptjakart_period_decoder = (p: Parser): Proto013PtJakartPeriod => {
    return Int64.decode(p);
}
