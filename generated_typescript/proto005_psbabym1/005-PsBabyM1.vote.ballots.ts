import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto005PsBabyM1VoteBallots = { yay: Int32, nay: Int32, pass: Int32 };
export class CGRIDClass__Proto005PsBabyM1VoteBallots extends Box<Proto005PsBabyM1VoteBallots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['yay', 'nay', 'pass']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1VoteBallots {
        return new this(record_decoder<Proto005PsBabyM1VoteBallots>({yay: Int32.decode, nay: Int32.decode, pass: Int32.decode}, {order: ['yay', 'nay', 'pass']})(p));
    };
    get encodeLength(): number {
        return (this.value.yay.encodeLength +  this.value.nay.encodeLength +  this.value.pass.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.yay.writeTarget(tgt) +  this.value.nay.writeTarget(tgt) +  this.value.pass.writeTarget(tgt));
    }
}
export const proto005_psbabym1_vote_ballots_encoder = (value: Proto005PsBabyM1VoteBallots): OutputBytes => {
    return record_encoder({order: ['yay', 'nay', 'pass']})(value);
}
export const proto005_psbabym1_vote_ballots_decoder = (p: Parser): Proto005PsBabyM1VoteBallots => {
    return record_decoder<Proto005PsBabyM1VoteBallots>({yay: Int32.decode, nay: Int32.decode, pass: Int32.decode}, {order: ['yay', 'nay', 'pass']})(p);
}
