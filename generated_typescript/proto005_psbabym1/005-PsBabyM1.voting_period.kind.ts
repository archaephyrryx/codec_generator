import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Testing_vote generated for Proto005PsBabyM1VotingPeriodKind__Testing_vote
export class CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Testing_vote extends Box<Proto005PsBabyM1VotingPeriodKind__Testing_vote> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Testing_vote {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Testing generated for Proto005PsBabyM1VotingPeriodKind__Testing
export class CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Testing extends Box<Proto005PsBabyM1VotingPeriodKind__Testing> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Testing {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Proposal generated for Proto005PsBabyM1VotingPeriodKind__Proposal
export class CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Proposal extends Box<Proto005PsBabyM1VotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Promotion_vote generated for Proto005PsBabyM1VotingPeriodKind__Promotion_vote
export class CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Promotion_vote extends Box<Proto005PsBabyM1VotingPeriodKind__Promotion_vote> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Promotion_vote {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto005PsBabyM1VotingPeriodKind__Testing_vote = Unit;
export type Proto005PsBabyM1VotingPeriodKind__Testing = Unit;
export type Proto005PsBabyM1VotingPeriodKind__Proposal = Unit;
export type Proto005PsBabyM1VotingPeriodKind__Promotion_vote = Unit;
export enum CGRIDTag__Proto005PsBabyM1VotingPeriodKind{
    Proposal = 0,
    Testing_vote = 1,
    Testing = 2,
    Promotion_vote = 3
}
export interface CGRIDMap__Proto005PsBabyM1VotingPeriodKind {
    Proposal: CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Proposal,
    Testing_vote: CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Testing_vote,
    Testing: CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Testing,
    Promotion_vote: CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Promotion_vote
}
export type Proto005PsBabyM1VotingPeriodKind = { kind: CGRIDTag__Proto005PsBabyM1VotingPeriodKind.Proposal, value: CGRIDMap__Proto005PsBabyM1VotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto005PsBabyM1VotingPeriodKind.Testing_vote, value: CGRIDMap__Proto005PsBabyM1VotingPeriodKind['Testing_vote'] } | { kind: CGRIDTag__Proto005PsBabyM1VotingPeriodKind.Testing, value: CGRIDMap__Proto005PsBabyM1VotingPeriodKind['Testing'] } | { kind: CGRIDTag__Proto005PsBabyM1VotingPeriodKind.Promotion_vote, value: CGRIDMap__Proto005PsBabyM1VotingPeriodKind['Promotion_vote'] };
export function proto005psbabym1votingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto005PsBabyM1VotingPeriodKind,Proto005PsBabyM1VotingPeriodKind> {
    function f(disc: CGRIDTag__Proto005PsBabyM1VotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto005PsBabyM1VotingPeriodKind.Proposal: return CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto005PsBabyM1VotingPeriodKind.Testing_vote: return CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Testing_vote.decode;
            case CGRIDTag__Proto005PsBabyM1VotingPeriodKind.Testing: return CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Testing.decode;
            case CGRIDTag__Proto005PsBabyM1VotingPeriodKind.Promotion_vote: return CGRIDClass__Proto005PsBabyM1VotingPeriodKind__Promotion_vote.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto005PsBabyM1VotingPeriodKind => Object.values(CGRIDTag__Proto005PsBabyM1VotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto005PsBabyM1VotingPeriodKind extends Box<Proto005PsBabyM1VotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto005PsBabyM1VotingPeriodKind>, Proto005PsBabyM1VotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1VotingPeriodKind {
        return new this(variant_decoder(width.Uint8)(proto005psbabym1votingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto005_psbabym1_voting_period_kind_encoder = (value: Proto005PsBabyM1VotingPeriodKind): OutputBytes => {
    return variant_encoder<KindOf<Proto005PsBabyM1VotingPeriodKind>, Proto005PsBabyM1VotingPeriodKind>(width.Uint8)(value);
}
export const proto005_psbabym1_voting_period_kind_decoder = (p: Parser): Proto005PsBabyM1VotingPeriodKind => {
    return variant_decoder(width.Uint8)(proto005psbabym1votingperiodkind_mkDecoder())(p);
}
