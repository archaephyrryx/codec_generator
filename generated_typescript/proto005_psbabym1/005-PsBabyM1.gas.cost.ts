import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
export type Proto005PsBabyM1GasCost = { allocations: Z, steps: Z, reads: Z, writes: Z, bytes_read: Z, bytes_written: Z };
export class CGRIDClass__Proto005PsBabyM1GasCost extends Box<Proto005PsBabyM1GasCost> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['allocations', 'steps', 'reads', 'writes', 'bytes_read', 'bytes_written']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1GasCost {
        return new this(record_decoder<Proto005PsBabyM1GasCost>({allocations: Z.decode, steps: Z.decode, reads: Z.decode, writes: Z.decode, bytes_read: Z.decode, bytes_written: Z.decode}, {order: ['allocations', 'steps', 'reads', 'writes', 'bytes_read', 'bytes_written']})(p));
    };
    get encodeLength(): number {
        return (this.value.allocations.encodeLength +  this.value.steps.encodeLength +  this.value.reads.encodeLength +  this.value.writes.encodeLength +  this.value.bytes_read.encodeLength +  this.value.bytes_written.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.allocations.writeTarget(tgt) +  this.value.steps.writeTarget(tgt) +  this.value.reads.writeTarget(tgt) +  this.value.writes.writeTarget(tgt) +  this.value.bytes_read.writeTarget(tgt) +  this.value.bytes_written.writeTarget(tgt));
    }
}
export const proto005_psbabym1_gas_cost_encoder = (value: Proto005PsBabyM1GasCost): OutputBytes => {
    return record_encoder({order: ['allocations', 'steps', 'reads', 'writes', 'bytes_read', 'bytes_written']})(value);
}
export const proto005_psbabym1_gas_cost_decoder = (p: Parser): Proto005PsBabyM1GasCost => {
    return record_decoder<Proto005PsBabyM1GasCost>({allocations: Z.decode, steps: Z.decode, reads: Z.decode, writes: Z.decode, bytes_read: Z.decode, bytes_written: Z.decode}, {order: ['allocations', 'steps', 'reads', 'writes', 'bytes_read', 'bytes_written']})(p);
}
