import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int31, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto005PsBabyM1ConstantsFixed = { proof_of_work_nonce_size: Uint8, nonce_length: Uint8, max_revelations_per_block: Uint8, max_operation_data_length: Int31, max_proposals_per_delegate: Uint8 };
export class CGRIDClass__Proto005PsBabyM1ConstantsFixed extends Box<Proto005PsBabyM1ConstantsFixed> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['proof_of_work_nonce_size', 'nonce_length', 'max_revelations_per_block', 'max_operation_data_length', 'max_proposals_per_delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1ConstantsFixed {
        return new this(record_decoder<Proto005PsBabyM1ConstantsFixed>({proof_of_work_nonce_size: Uint8.decode, nonce_length: Uint8.decode, max_revelations_per_block: Uint8.decode, max_operation_data_length: Int31.decode, max_proposals_per_delegate: Uint8.decode}, {order: ['proof_of_work_nonce_size', 'nonce_length', 'max_revelations_per_block', 'max_operation_data_length', 'max_proposals_per_delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.proof_of_work_nonce_size.encodeLength +  this.value.nonce_length.encodeLength +  this.value.max_revelations_per_block.encodeLength +  this.value.max_operation_data_length.encodeLength +  this.value.max_proposals_per_delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.proof_of_work_nonce_size.writeTarget(tgt) +  this.value.nonce_length.writeTarget(tgt) +  this.value.max_revelations_per_block.writeTarget(tgt) +  this.value.max_operation_data_length.writeTarget(tgt) +  this.value.max_proposals_per_delegate.writeTarget(tgt));
    }
}
export const proto005_psbabym1_constants_fixed_encoder = (value: Proto005PsBabyM1ConstantsFixed): OutputBytes => {
    return record_encoder({order: ['proof_of_work_nonce_size', 'nonce_length', 'max_revelations_per_block', 'max_operation_data_length', 'max_proposals_per_delegate']})(value);
}
export const proto005_psbabym1_constants_fixed_decoder = (p: Parser): Proto005PsBabyM1ConstantsFixed => {
    return record_decoder<Proto005PsBabyM1ConstantsFixed>({proof_of_work_nonce_size: Uint8.decode, nonce_length: Uint8.decode, max_revelations_per_block: Uint8.decode, max_operation_data_length: Int31.decode, max_proposals_per_delegate: Uint8.decode}, {order: ['proof_of_work_nonce_size', 'nonce_length', 'max_revelations_per_block', 'max_operation_data_length', 'max_proposals_per_delegate']})(p);
}
