import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto005PsBabyM1Period = Int64;
export class CGRIDClass__Proto005PsBabyM1Period extends Box<Proto005PsBabyM1Period> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1Period {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto005_psbabym1_period_encoder = (value: Proto005PsBabyM1Period): OutputBytes => {
    return value.encode();
}
export const proto005_psbabym1_period_decoder = (p: Parser): Proto005PsBabyM1Period => {
    return Int64.decode(p);
}
