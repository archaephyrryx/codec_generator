import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto005_PsBabyM1Block_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto005PsBabyM1BlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto005_PsBabyM1Block_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto005PsBabyM1BlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto005_PsBabyM1Block_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto005PsBabyM1BlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto005_PsBabyM1Block_headerAlphaSigned_contents_signature generated for Proto005PsBabyM1BlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto005_PsBabyM1Block_headerAlphaSigned_contents_signature extends Box<Proto005PsBabyM1BlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto005_PsBabyM1Block_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto005PsBabyM1BlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
export type Proto005PsBabyM1BlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto005PsBabyM1BlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto005PsBabyM1BlockHeaderProtocolData = { priority: Uint16, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto005_PsBabyM1Block_headerAlphaUnsigned_contents_seed_nonce_hash>, signature: CGRIDClass__Proto005_PsBabyM1Block_headerAlphaSigned_contents_signature };
export class CGRIDClass__Proto005PsBabyM1BlockHeaderProtocolData extends Box<Proto005PsBabyM1BlockHeaderProtocolData> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1BlockHeaderProtocolData {
        return new this(record_decoder<Proto005PsBabyM1BlockHeaderProtocolData>({priority: Uint16.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto005_PsBabyM1Block_headerAlphaUnsigned_contents_seed_nonce_hash.decode), signature: CGRIDClass__Proto005_PsBabyM1Block_headerAlphaSigned_contents_signature.decode}, {order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.priority.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.priority.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export const proto005_psbabym1_block_header_protocol_data_encoder = (value: Proto005PsBabyM1BlockHeaderProtocolData): OutputBytes => {
    return record_encoder({order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(value);
}
export const proto005_psbabym1_block_header_protocol_data_decoder = (p: Parser): Proto005PsBabyM1BlockHeaderProtocolData => {
    return record_decoder<Proto005PsBabyM1BlockHeaderProtocolData>({priority: Uint16.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto005_PsBabyM1Block_headerAlphaUnsigned_contents_seed_nonce_hash.decode), signature: CGRIDClass__Proto005_PsBabyM1Block_headerAlphaSigned_contents_signature.decode}, {order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(p);
}
