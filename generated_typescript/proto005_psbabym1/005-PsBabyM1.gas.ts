import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto005PsBabyM1Gas__Unaccounted generated for Proto005PsBabyM1Gas__Unaccounted
export class CGRIDClass__Proto005PsBabyM1Gas__Unaccounted extends Box<Proto005PsBabyM1Gas__Unaccounted> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1Gas__Unaccounted {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto005PsBabyM1Gas__Limited generated for Proto005PsBabyM1Gas__Limited
export class CGRIDClass__Proto005PsBabyM1Gas__Limited extends Box<Proto005PsBabyM1Gas__Limited> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1Gas__Limited {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto005PsBabyM1Gas__Unaccounted = Unit;
export type Proto005PsBabyM1Gas__Limited = Z;
export enum CGRIDTag__Proto005PsBabyM1Gas{
    Limited = 0,
    Unaccounted = 1
}
export interface CGRIDMap__Proto005PsBabyM1Gas {
    Limited: CGRIDClass__Proto005PsBabyM1Gas__Limited,
    Unaccounted: CGRIDClass__Proto005PsBabyM1Gas__Unaccounted
}
export type Proto005PsBabyM1Gas = { kind: CGRIDTag__Proto005PsBabyM1Gas.Limited, value: CGRIDMap__Proto005PsBabyM1Gas['Limited'] } | { kind: CGRIDTag__Proto005PsBabyM1Gas.Unaccounted, value: CGRIDMap__Proto005PsBabyM1Gas['Unaccounted'] };
export function proto005psbabym1gas_mkDecoder(): VariantDecoder<CGRIDTag__Proto005PsBabyM1Gas,Proto005PsBabyM1Gas> {
    function f(disc: CGRIDTag__Proto005PsBabyM1Gas) {
        switch (disc) {
            case CGRIDTag__Proto005PsBabyM1Gas.Limited: return CGRIDClass__Proto005PsBabyM1Gas__Limited.decode;
            case CGRIDTag__Proto005PsBabyM1Gas.Unaccounted: return CGRIDClass__Proto005PsBabyM1Gas__Unaccounted.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto005PsBabyM1Gas => Object.values(CGRIDTag__Proto005PsBabyM1Gas).includes(tagval);
    return f;
}
export class CGRIDClass__Proto005PsBabyM1Gas extends Box<Proto005PsBabyM1Gas> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto005PsBabyM1Gas>, Proto005PsBabyM1Gas>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1Gas {
        return new this(variant_decoder(width.Uint8)(proto005psbabym1gas_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto005_psbabym1_gas_encoder = (value: Proto005PsBabyM1Gas): OutputBytes => {
    return variant_encoder<KindOf<Proto005PsBabyM1Gas>, Proto005PsBabyM1Gas>(width.Uint8)(value);
}
export const proto005_psbabym1_gas_decoder = (p: Parser): Proto005PsBabyM1Gas => {
    return variant_decoder(width.Uint8)(proto005psbabym1gas_mkDecoder())(p);
}
