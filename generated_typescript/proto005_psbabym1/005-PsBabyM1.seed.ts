import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type Proto005PsBabyM1Seed = FixedBytes<32>;
export class CGRIDClass__Proto005PsBabyM1Seed extends Box<Proto005PsBabyM1Seed> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1Seed {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto005_psbabym1_seed_encoder = (value: Proto005PsBabyM1Seed): OutputBytes => {
    return value.encode();
}
export const proto005_psbabym1_seed_decoder = (p: Parser): Proto005PsBabyM1Seed => {
    return FixedBytes.decode<32>({len: 32})(p);
}
