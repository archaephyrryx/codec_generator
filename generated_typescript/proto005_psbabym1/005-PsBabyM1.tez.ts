import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto005PsBabyM1Tez = N;
export class CGRIDClass__Proto005PsBabyM1Tez extends Box<Proto005PsBabyM1Tez> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto005PsBabyM1Tez {
        return new this(N.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto005_psbabym1_tez_encoder = (value: Proto005PsBabyM1Tez): OutputBytes => {
    return value.encode();
}
export const proto005_psbabym1_tez_decoder = (p: Parser): Proto005PsBabyM1Tez => {
    return N.decode(p);
}
