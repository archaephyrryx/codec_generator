import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { Target } from '../../ts_runtime/target';
export type GroundBytes = Dynamic<Bytes,width.Uint30>;
export class CGRIDClass__GroundBytes extends Box<GroundBytes> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundBytes {
        return new this(Dynamic.decode(Bytes.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_bytes_encoder = (value: GroundBytes): OutputBytes => {
    return value.encode();
}
export const ground_bytes_decoder = (p: Parser): GroundBytes => {
    return Dynamic.decode(Bytes.decode, width.Uint30)(p);
}
