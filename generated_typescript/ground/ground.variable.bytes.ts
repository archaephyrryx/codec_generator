import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { Target } from '../../ts_runtime/target';
export type GroundVariableBytes = Bytes;
export class CGRIDClass__GroundVariableBytes extends Box<GroundVariableBytes> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundVariableBytes {
        return new this(Bytes.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_variable_bytes_encoder = (value: GroundVariableBytes): OutputBytes => {
    return value.encode();
}
export const ground_variable_bytes_decoder = (p: Parser): GroundVariableBytes => {
    return Bytes.decode(p);
}
