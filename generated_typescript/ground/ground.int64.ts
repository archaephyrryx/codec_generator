import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type GroundInt64 = Int64;
export class CGRIDClass__GroundInt64 extends Box<GroundInt64> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundInt64 {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_int64_encoder = (value: GroundInt64): OutputBytes => {
    return value.encode();
}
export const ground_int64_decoder = (p: Parser): GroundInt64 => {
    return Int64.decode(p);
}
