import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { Target } from '../../ts_runtime/target';
export type GroundBool = Bool;
export class CGRIDClass__GroundBool extends Box<GroundBool> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundBool {
        return new this(Bool.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_bool_encoder = (value: GroundBool): OutputBytes => {
    return value.encode();
}
export const ground_bool_decoder = (p: Parser): GroundBool => {
    return Bool.decode(p);
}
