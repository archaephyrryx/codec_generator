import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int31 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type GroundInt31 = Int31;
export class CGRIDClass__GroundInt31 extends Box<GroundInt31> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundInt31 {
        return new this(Int31.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_int31_encoder = (value: GroundInt31): OutputBytes => {
    return value.encode();
}
export const ground_int31_decoder = (p: Parser): GroundInt31 => {
    return Int31.decode(p);
}
