import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
export type GroundEmpty = Unit;
export class CGRIDClass__GroundEmpty extends Box<GroundEmpty> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundEmpty {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_empty_encoder = (value: GroundEmpty): OutputBytes => {
    return value.encode();
}
export const ground_empty_decoder = (p: Parser): GroundEmpty => {
    return Unit.decode(p);
}
