import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type GroundInt16 = Int16;
export class CGRIDClass__GroundInt16 extends Box<GroundInt16> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundInt16 {
        return new this(Int16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_int16_encoder = (value: GroundInt16): OutputBytes => {
    return value.encode();
}
export const ground_int16_decoder = (p: Parser): GroundInt16 => {
    return Int16.decode(p);
}
