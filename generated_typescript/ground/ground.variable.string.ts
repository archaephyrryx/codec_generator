import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
export type GroundVariableString = U8String;
export class CGRIDClass__GroundVariableString extends Box<GroundVariableString> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundVariableString {
        return new this(U8String.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_variable_string_encoder = (value: GroundVariableString): OutputBytes => {
    return value.encode();
}
export const ground_variable_string_decoder = (p: Parser): GroundVariableString => {
    return U8String.decode(p);
}
