import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type GroundUint8 = Uint8;
export class CGRIDClass__GroundUint8 extends Box<GroundUint8> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundUint8 {
        return new this(Uint8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_uint8_encoder = (value: GroundUint8): OutputBytes => {
    return value.encode();
}
export const ground_uint8_decoder = (p: Parser): GroundUint8 => {
    return Uint8.decode(p);
}
