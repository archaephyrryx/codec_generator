import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
export type GroundUnit = Unit;
export class CGRIDClass__GroundUnit extends Box<GroundUnit> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundUnit {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_unit_encoder = (value: GroundUnit): OutputBytes => {
    return value.encode();
}
export const ground_unit_decoder = (p: Parser): GroundUnit => {
    return Unit.decode(p);
}
