import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type GroundUint16 = Uint16;
export class CGRIDClass__GroundUint16 extends Box<GroundUint16> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundUint16 {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_uint16_encoder = (value: GroundUint16): OutputBytes => {
    return value.encode();
}
export const ground_uint16_decoder = (p: Parser): GroundUint16 => {
    return Uint16.decode(p);
}
