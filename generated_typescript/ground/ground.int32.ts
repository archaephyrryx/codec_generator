import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type GroundInt32 = Int32;
export class CGRIDClass__GroundInt32 extends Box<GroundInt32> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundInt32 {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_int32_encoder = (value: GroundInt32): OutputBytes => {
    return value.encode();
}
export const ground_int32_decoder = (p: Parser): GroundInt32 => {
    return Int32.decode(p);
}
