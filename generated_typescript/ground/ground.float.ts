import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Double } from '../../ts_runtime/float/double';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type GroundFloat = Double;
export class CGRIDClass__GroundFloat extends Box<GroundFloat> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__GroundFloat {
        return new this(Double.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const ground_float_encoder = (value: GroundFloat): OutputBytes => {
    return value.encode();
}
export const ground_float_decoder = (p: Parser): GroundFloat => {
    return Double.decode(p);
}
