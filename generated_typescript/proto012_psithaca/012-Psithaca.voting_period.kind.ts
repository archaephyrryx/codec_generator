import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto012PsithacaVotingPeriodKind__exploration generated for Proto012PsithacaVotingPeriodKind__exploration
export class CGRIDClass__Proto012PsithacaVotingPeriodKind__exploration extends Box<Proto012PsithacaVotingPeriodKind__exploration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriodKind__exploration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaVotingPeriodKind__Proposal generated for Proto012PsithacaVotingPeriodKind__Proposal
export class CGRIDClass__Proto012PsithacaVotingPeriodKind__Proposal extends Box<Proto012PsithacaVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaVotingPeriodKind__Promotion generated for Proto012PsithacaVotingPeriodKind__Promotion
export class CGRIDClass__Proto012PsithacaVotingPeriodKind__Promotion extends Box<Proto012PsithacaVotingPeriodKind__Promotion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriodKind__Promotion {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaVotingPeriodKind__Cooldown generated for Proto012PsithacaVotingPeriodKind__Cooldown
export class CGRIDClass__Proto012PsithacaVotingPeriodKind__Cooldown extends Box<Proto012PsithacaVotingPeriodKind__Cooldown> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriodKind__Cooldown {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaVotingPeriodKind__Adoption generated for Proto012PsithacaVotingPeriodKind__Adoption
export class CGRIDClass__Proto012PsithacaVotingPeriodKind__Adoption extends Box<Proto012PsithacaVotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto012PsithacaVotingPeriodKind__exploration = Unit;
export type Proto012PsithacaVotingPeriodKind__Proposal = Unit;
export type Proto012PsithacaVotingPeriodKind__Promotion = Unit;
export type Proto012PsithacaVotingPeriodKind__Cooldown = Unit;
export type Proto012PsithacaVotingPeriodKind__Adoption = Unit;
export enum CGRIDTag__Proto012PsithacaVotingPeriodKind{
    Proposal = 0,
    exploration = 1,
    Cooldown = 2,
    Promotion = 3,
    Adoption = 4
}
export interface CGRIDMap__Proto012PsithacaVotingPeriodKind {
    Proposal: CGRIDClass__Proto012PsithacaVotingPeriodKind__Proposal,
    exploration: CGRIDClass__Proto012PsithacaVotingPeriodKind__exploration,
    Cooldown: CGRIDClass__Proto012PsithacaVotingPeriodKind__Cooldown,
    Promotion: CGRIDClass__Proto012PsithacaVotingPeriodKind__Promotion,
    Adoption: CGRIDClass__Proto012PsithacaVotingPeriodKind__Adoption
}
export type Proto012PsithacaVotingPeriodKind = { kind: CGRIDTag__Proto012PsithacaVotingPeriodKind.Proposal, value: CGRIDMap__Proto012PsithacaVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto012PsithacaVotingPeriodKind.exploration, value: CGRIDMap__Proto012PsithacaVotingPeriodKind['exploration'] } | { kind: CGRIDTag__Proto012PsithacaVotingPeriodKind.Cooldown, value: CGRIDMap__Proto012PsithacaVotingPeriodKind['Cooldown'] } | { kind: CGRIDTag__Proto012PsithacaVotingPeriodKind.Promotion, value: CGRIDMap__Proto012PsithacaVotingPeriodKind['Promotion'] } | { kind: CGRIDTag__Proto012PsithacaVotingPeriodKind.Adoption, value: CGRIDMap__Proto012PsithacaVotingPeriodKind['Adoption'] };
export function proto012psithacavotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaVotingPeriodKind,Proto012PsithacaVotingPeriodKind> {
    function f(disc: CGRIDTag__Proto012PsithacaVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaVotingPeriodKind.Proposal: return CGRIDClass__Proto012PsithacaVotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto012PsithacaVotingPeriodKind.exploration: return CGRIDClass__Proto012PsithacaVotingPeriodKind__exploration.decode;
            case CGRIDTag__Proto012PsithacaVotingPeriodKind.Cooldown: return CGRIDClass__Proto012PsithacaVotingPeriodKind__Cooldown.decode;
            case CGRIDTag__Proto012PsithacaVotingPeriodKind.Promotion: return CGRIDClass__Proto012PsithacaVotingPeriodKind__Promotion.decode;
            case CGRIDTag__Proto012PsithacaVotingPeriodKind.Adoption: return CGRIDClass__Proto012PsithacaVotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaVotingPeriodKind => Object.values(CGRIDTag__Proto012PsithacaVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012PsithacaVotingPeriodKind extends Box<Proto012PsithacaVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaVotingPeriodKind>, Proto012PsithacaVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriodKind {
        return new this(variant_decoder(width.Uint8)(proto012psithacavotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto012_psithaca_voting_period_kind_encoder = (value: Proto012PsithacaVotingPeriodKind): OutputBytes => {
    return variant_encoder<KindOf<Proto012PsithacaVotingPeriodKind>, Proto012PsithacaVotingPeriodKind>(width.Uint8)(value);
}
export const proto012_psithaca_voting_period_kind_decoder = (p: Parser): Proto012PsithacaVotingPeriodKind => {
    return variant_decoder(width.Uint8)(proto012psithacavotingperiodkind_mkDecoder())(p);
}
