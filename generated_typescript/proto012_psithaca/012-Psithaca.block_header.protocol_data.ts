import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_payload_hash generated for Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_payload_hash extends Box<Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaBlock_headerAlphaSigned_contents_signature generated for Proto012PsithacaBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto012_PsithacaBlock_headerAlphaSigned_contents_signature extends Box<Proto012PsithacaBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto012PsithacaBlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
export type Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type Proto012PsithacaBlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto012PsithacaBlockHeaderProtocolData = { payload_hash: CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_escape_vote: Bool, signature: CGRIDClass__Proto012_PsithacaBlock_headerAlphaSigned_contents_signature };
export class CGRIDClass__Proto012PsithacaBlockHeaderProtocolData extends Box<Proto012PsithacaBlockHeaderProtocolData> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaBlockHeaderProtocolData {
        return new this(record_decoder<Proto012PsithacaBlockHeaderProtocolData>({payload_hash: CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_escape_vote: Bool.decode, signature: CGRIDClass__Proto012_PsithacaBlock_headerAlphaSigned_contents_signature.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_escape_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_escape_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export const proto012_psithaca_block_header_protocol_data_encoder = (value: Proto012PsithacaBlockHeaderProtocolData): OutputBytes => {
    return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote', 'signature']})(value);
}
export const proto012_psithaca_block_header_protocol_data_decoder = (p: Parser): Proto012PsithacaBlockHeaderProtocolData => {
    return record_decoder<Proto012PsithacaBlockHeaderProtocolData>({payload_hash: CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_escape_vote: Bool.decode, signature: CGRIDClass__Proto012_PsithacaBlock_headerAlphaSigned_contents_signature.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote', 'signature']})(p);
}
