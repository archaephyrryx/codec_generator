import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Transaction generated for Proto012PsithacaOperationAlphaInternalOperationRhs__Transaction
export class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Transaction extends Box<Proto012PsithacaOperationAlphaInternalOperationRhs__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Transaction {
        return new this(record_decoder<Proto012PsithacaOperationAlphaInternalOperationRhs__Transaction>({amount: N.decode, destination: CGRIDClass__Proto012_PsithacaContract_id.decode, parameters: Option.decode(CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Transaction_parameters.decode)}, {order: ['amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Set_deposits_limit generated for Proto012PsithacaOperationAlphaInternalOperationRhs__Set_deposits_limit
export class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Set_deposits_limit extends Box<Proto012PsithacaOperationAlphaInternalOperationRhs__Set_deposits_limit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['limit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Set_deposits_limit {
        return new this(record_decoder<Proto012PsithacaOperationAlphaInternalOperationRhs__Set_deposits_limit>({limit: Option.decode(N.decode)}, {order: ['limit']})(p));
    };
    get encodeLength(): number {
        return (this.value.limit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.limit.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Reveal generated for Proto012PsithacaOperationAlphaInternalOperationRhs__Reveal
export class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Reveal extends Box<Proto012PsithacaOperationAlphaInternalOperationRhs__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Reveal {
        return new this(record_decoder<Proto012PsithacaOperationAlphaInternalOperationRhs__Reveal>({public_key: CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Reveal_public_key.decode}, {order: ['public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Register_global_constant generated for Proto012PsithacaOperationAlphaInternalOperationRhs__Register_global_constant
export class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Register_global_constant extends Box<Proto012PsithacaOperationAlphaInternalOperationRhs__Register_global_constant> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Register_global_constant {
        return new this(record_decoder<Proto012PsithacaOperationAlphaInternalOperationRhs__Register_global_constant>({value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['value']})(p));
    };
    get encodeLength(): number {
        return (this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Origination generated for Proto012PsithacaOperationAlphaInternalOperationRhs__Origination
export class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Origination extends Box<Proto012PsithacaOperationAlphaInternalOperationRhs__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Origination {
        return new this(record_decoder<Proto012PsithacaOperationAlphaInternalOperationRhs__Origination>({balance: N.decode, delegate: Option.decode(CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Origination_delegate.decode), script: CGRIDClass__Proto012_PsithacaScriptedContracts.decode}, {order: ['balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Delegation generated for Proto012PsithacaOperationAlphaInternalOperationRhs__Delegation
export class CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Delegation extends Box<Proto012PsithacaOperationAlphaInternalOperationRhs__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Delegation {
        return new this(record_decoder<Proto012PsithacaOperationAlphaInternalOperationRhs__Delegation>({delegate: Option.decode(CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Delegation_delegate.decode)}, {order: ['delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint__set_delegate generated for Proto012PsithacaEntrypoint__set_delegate
export class CGRIDClass__Proto012PsithacaEntrypoint__set_delegate extends Box<Proto012PsithacaEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint__root generated for Proto012PsithacaEntrypoint__root
export class CGRIDClass__Proto012PsithacaEntrypoint__root extends Box<Proto012PsithacaEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint__remove_delegate generated for Proto012PsithacaEntrypoint__remove_delegate
export class CGRIDClass__Proto012PsithacaEntrypoint__remove_delegate extends Box<Proto012PsithacaEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint__named generated for Proto012PsithacaEntrypoint__named
export class CGRIDClass__Proto012PsithacaEntrypoint__named extends Box<Proto012PsithacaEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint___do generated for Proto012PsithacaEntrypoint___do
export class CGRIDClass__Proto012PsithacaEntrypoint___do extends Box<Proto012PsithacaEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint___default generated for Proto012PsithacaEntrypoint___default
export class CGRIDClass__Proto012PsithacaEntrypoint___default extends Box<Proto012PsithacaEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaContractId__Originated generated for Proto012PsithacaContractId__Originated
export class CGRIDClass__Proto012PsithacaContractId__Originated extends Box<Proto012PsithacaContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaContractId__Implicit generated for Proto012PsithacaContractId__Implicit
export class CGRIDClass__Proto012PsithacaContractId__Implicit extends Box<Proto012PsithacaContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaContractId__Implicit {
        return new this(record_decoder<Proto012PsithacaContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto012PsithacaOperationAlphaInternalOperationRhs__Transaction = { amount: N, destination: CGRIDClass__Proto012_PsithacaContract_id, parameters: Option<CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Transaction_parameters> };
export type Proto012PsithacaOperationAlphaInternalOperationRhs__Set_deposits_limit = { limit: Option<N> };
export type Proto012PsithacaOperationAlphaInternalOperationRhs__Reveal = { public_key: CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Reveal_public_key };
export type Proto012PsithacaOperationAlphaInternalOperationRhs__Register_global_constant = { value: Dynamic<Bytes,width.Uint30> };
export type Proto012PsithacaOperationAlphaInternalOperationRhs__Origination = { balance: N, delegate: Option<CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Origination_delegate>, script: CGRIDClass__Proto012_PsithacaScriptedContracts };
export type Proto012PsithacaOperationAlphaInternalOperationRhs__Delegation = { delegate: Option<CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Delegation_delegate> };
export type Proto012PsithacaEntrypoint__set_delegate = Unit;
export type Proto012PsithacaEntrypoint__root = Unit;
export type Proto012PsithacaEntrypoint__remove_delegate = Unit;
export type Proto012PsithacaEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto012PsithacaEntrypoint___do = Unit;
export type Proto012PsithacaEntrypoint___default = Unit;
export type Proto012PsithacaContractId__Originated = Padded<CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad,1>;
export type Proto012PsithacaContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaScriptedContracts generated for Proto012PsithacaScriptedContracts
export class CGRIDClass__Proto012_PsithacaScriptedContracts extends Box<Proto012PsithacaScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaScriptedContracts {
        return new this(record_decoder<Proto012PsithacaScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_rhs generated for Proto012PsithacaOperationAlphaInternalOperationRhs
export function proto012psithacaoperationalphainternaloperationrhs_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs,Proto012PsithacaOperationAlphaInternalOperationRhs> {
    function f(disc: CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Reveal: return CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Reveal.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Transaction: return CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Transaction.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Origination: return CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Origination.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Delegation: return CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Delegation.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Register_global_constant: return CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Register_global_constant.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Set_deposits_limit: return CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Set_deposits_limit.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs => Object.values(CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_rhs extends Box<Proto012PsithacaOperationAlphaInternalOperationRhs> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaOperationAlphaInternalOperationRhs>, Proto012PsithacaOperationAlphaInternalOperationRhs>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_rhs {
        return new this(variant_decoder(width.Uint8)(proto012psithacaoperationalphainternaloperationrhs_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Transaction_parameters generated for Proto012PsithacaOperationAlphaInternalOperationTransactionParameters
export class CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Transaction_parameters extends Box<Proto012PsithacaOperationAlphaInternalOperationTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Transaction_parameters {
        return new this(record_decoder<Proto012PsithacaOperationAlphaInternalOperationTransactionParameters>({entrypoint: CGRIDClass__Proto012_PsithacaEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Reveal_public_key generated for Proto012PsithacaOperationAlphaInternalOperationRevealPublicKey
export class CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Reveal_public_key extends Box<Proto012PsithacaOperationAlphaInternalOperationRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Reveal_public_key {
        return new this(record_decoder<Proto012PsithacaOperationAlphaInternalOperationRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Origination_delegate generated for Proto012PsithacaOperationAlphaInternalOperationOriginationDelegate
export class CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Origination_delegate extends Box<Proto012PsithacaOperationAlphaInternalOperationOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Origination_delegate {
        return new this(record_decoder<Proto012PsithacaOperationAlphaInternalOperationOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Delegation_delegate generated for Proto012PsithacaOperationAlphaInternalOperationDelegationDelegate
export class CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Delegation_delegate extends Box<Proto012PsithacaOperationAlphaInternalOperationDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_Delegation_delegate {
        return new this(record_decoder<Proto012PsithacaOperationAlphaInternalOperationDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaEntrypoint generated for Proto012PsithacaEntrypoint
export function proto012psithacaentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaEntrypoint,Proto012PsithacaEntrypoint> {
    function f(disc: CGRIDTag__Proto012PsithacaEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaEntrypoint._default: return CGRIDClass__Proto012PsithacaEntrypoint___default.decode;
            case CGRIDTag__Proto012PsithacaEntrypoint.root: return CGRIDClass__Proto012PsithacaEntrypoint__root.decode;
            case CGRIDTag__Proto012PsithacaEntrypoint._do: return CGRIDClass__Proto012PsithacaEntrypoint___do.decode;
            case CGRIDTag__Proto012PsithacaEntrypoint.set_delegate: return CGRIDClass__Proto012PsithacaEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto012PsithacaEntrypoint.remove_delegate: return CGRIDClass__Proto012PsithacaEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto012PsithacaEntrypoint.named: return CGRIDClass__Proto012PsithacaEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaEntrypoint => Object.values(CGRIDTag__Proto012PsithacaEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaEntrypoint extends Box<Proto012PsithacaEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaEntrypoint>, Proto012PsithacaEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto012psithacaentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad generated for Proto012PsithacaContractIdOriginatedDenestPad
export class CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad extends Box<Proto012PsithacaContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto012PsithacaContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaContract_id generated for Proto012PsithacaContractId
export function proto012psithacacontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaContractId,Proto012PsithacaContractId> {
    function f(disc: CGRIDTag__Proto012PsithacaContractId) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaContractId.Implicit: return CGRIDClass__Proto012PsithacaContractId__Implicit.decode;
            case CGRIDTag__Proto012PsithacaContractId.Originated: return CGRIDClass__Proto012PsithacaContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaContractId => Object.values(CGRIDTag__Proto012PsithacaContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaContract_id extends Box<Proto012PsithacaContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaContractId>, Proto012PsithacaContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaContract_id {
        return new this(variant_decoder(width.Uint8)(proto012psithacacontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type Proto012PsithacaScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export enum CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs{
    Reveal = 0,
    Transaction = 1,
    Origination = 2,
    Delegation = 3,
    Register_global_constant = 4,
    Set_deposits_limit = 5
}
export interface CGRIDMap__Proto012PsithacaOperationAlphaInternalOperationRhs {
    Reveal: CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Reveal,
    Transaction: CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Transaction,
    Origination: CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Origination,
    Delegation: CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Delegation,
    Register_global_constant: CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Register_global_constant,
    Set_deposits_limit: CGRIDClass__Proto012PsithacaOperationAlphaInternalOperationRhs__Set_deposits_limit
}
export type Proto012PsithacaOperationAlphaInternalOperationRhs = { kind: CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Reveal, value: CGRIDMap__Proto012PsithacaOperationAlphaInternalOperationRhs['Reveal'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Transaction, value: CGRIDMap__Proto012PsithacaOperationAlphaInternalOperationRhs['Transaction'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Origination, value: CGRIDMap__Proto012PsithacaOperationAlphaInternalOperationRhs['Origination'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Delegation, value: CGRIDMap__Proto012PsithacaOperationAlphaInternalOperationRhs['Delegation'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Register_global_constant, value: CGRIDMap__Proto012PsithacaOperationAlphaInternalOperationRhs['Register_global_constant'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaInternalOperationRhs.Set_deposits_limit, value: CGRIDMap__Proto012PsithacaOperationAlphaInternalOperationRhs['Set_deposits_limit'] };
export type Proto012PsithacaOperationAlphaInternalOperationTransactionParameters = { entrypoint: CGRIDClass__Proto012_PsithacaEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto012PsithacaOperationAlphaInternalOperationRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto012PsithacaOperationAlphaInternalOperationOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationAlphaInternalOperationDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto012PsithacaEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto012PsithacaEntrypoint {
    _default: CGRIDClass__Proto012PsithacaEntrypoint___default,
    root: CGRIDClass__Proto012PsithacaEntrypoint__root,
    _do: CGRIDClass__Proto012PsithacaEntrypoint___do,
    set_delegate: CGRIDClass__Proto012PsithacaEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto012PsithacaEntrypoint__remove_delegate,
    named: CGRIDClass__Proto012PsithacaEntrypoint__named
}
export type Proto012PsithacaEntrypoint = { kind: CGRIDTag__Proto012PsithacaEntrypoint._default, value: CGRIDMap__Proto012PsithacaEntrypoint['_default'] } | { kind: CGRIDTag__Proto012PsithacaEntrypoint.root, value: CGRIDMap__Proto012PsithacaEntrypoint['root'] } | { kind: CGRIDTag__Proto012PsithacaEntrypoint._do, value: CGRIDMap__Proto012PsithacaEntrypoint['_do'] } | { kind: CGRIDTag__Proto012PsithacaEntrypoint.set_delegate, value: CGRIDMap__Proto012PsithacaEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto012PsithacaEntrypoint.remove_delegate, value: CGRIDMap__Proto012PsithacaEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto012PsithacaEntrypoint.named, value: CGRIDMap__Proto012PsithacaEntrypoint['named'] };
export type Proto012PsithacaContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto012PsithacaContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto012PsithacaContractId {
    Implicit: CGRIDClass__Proto012PsithacaContractId__Implicit,
    Originated: CGRIDClass__Proto012PsithacaContractId__Originated
}
export type Proto012PsithacaContractId = { kind: CGRIDTag__Proto012PsithacaContractId.Implicit, value: CGRIDMap__Proto012PsithacaContractId['Implicit'] } | { kind: CGRIDTag__Proto012PsithacaContractId.Originated, value: CGRIDMap__Proto012PsithacaContractId['Originated'] };
export type Proto012PsithacaOperationInternal = { source: CGRIDClass__Proto012_PsithacaContract_id, nonce: Uint16, proto012_psithaca_operation_alpha_internal_operation_rhs: CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_rhs };
export class CGRIDClass__Proto012PsithacaOperationInternal extends Box<Proto012PsithacaOperationInternal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'nonce', 'proto012_psithaca_operation_alpha_internal_operation_rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationInternal {
        return new this(record_decoder<Proto012PsithacaOperationInternal>({source: CGRIDClass__Proto012_PsithacaContract_id.decode, nonce: Uint16.decode, proto012_psithaca_operation_alpha_internal_operation_rhs: CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto012_psithaca_operation_alpha_internal_operation_rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.nonce.encodeLength +  this.value.proto012_psithaca_operation_alpha_internal_operation_rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt) +  this.value.proto012_psithaca_operation_alpha_internal_operation_rhs.writeTarget(tgt));
    }
}
export const proto012_psithaca_operation_internal_encoder = (value: Proto012PsithacaOperationInternal): OutputBytes => {
    return record_encoder({order: ['source', 'nonce', 'proto012_psithaca_operation_alpha_internal_operation_rhs']})(value);
}
export const proto012_psithaca_operation_internal_decoder = (p: Parser): Proto012PsithacaOperationInternal => {
    return record_decoder<Proto012PsithacaOperationInternal>({source: CGRIDClass__Proto012_PsithacaContract_id.decode, nonce: Uint16.decode, proto012_psithaca_operation_alpha_internal_operation_rhs: CGRIDClass__Proto012_PsithacaOperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto012_psithaca_operation_alpha_internal_operation_rhs']})(p);
}
