import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int31 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto012PsithacaScriptLoc = Int31;
export class CGRIDClass__Proto012PsithacaScriptLoc extends Box<Proto012PsithacaScriptLoc> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaScriptLoc {
        return new this(Int31.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto012_psithaca_script_loc_encoder = (value: Proto012PsithacaScriptLoc): OutputBytes => {
    return value.encode();
}
export const proto012_psithaca_script_loc_decoder = (p: Parser): Proto012PsithacaScriptLoc => {
    return Int31.decode(p);
}
