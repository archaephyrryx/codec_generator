import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto012PsithacaTez = N;
export class CGRIDClass__Proto012PsithacaTez extends Box<Proto012PsithacaTez> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaTez {
        return new this(N.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto012_psithaca_tez_encoder = (value: Proto012PsithacaTez): OutputBytes => {
    return value.encode();
}
export const proto012_psithaca_tez_decoder = (p: Parser): Proto012PsithacaTez => {
    return N.decode(p);
}
