import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Nullable } from '../../ts_runtime/composite/opt/nullable';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { VPadded } from '../../ts_runtime/composite/vpadded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int64, Int8, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Transaction generated for Proto012PsithacaOperationAlphaContents__Transaction
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Transaction extends Box<Proto012PsithacaOperationAlphaContents__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Transaction {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Transaction>({source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Transaction_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: N.decode, destination: CGRIDClass__Proto012_PsithacaContract_id.decode, parameters: Option.decode(CGRIDClass__Proto012_PsithacaOperationAlphaContents_Transaction_parameters.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Set_deposits_limit generated for Proto012PsithacaOperationAlphaContents__Set_deposits_limit
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Set_deposits_limit extends Box<Proto012PsithacaOperationAlphaContents__Set_deposits_limit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'limit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Set_deposits_limit {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Set_deposits_limit>({source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Set_deposits_limit_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, limit: Option.decode(N.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'limit']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.limit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.limit.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Seed_nonce_revelation generated for Proto012PsithacaOperationAlphaContents__Seed_nonce_revelation
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Seed_nonce_revelation extends Box<Proto012PsithacaOperationAlphaContents__Seed_nonce_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Seed_nonce_revelation {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Seed_nonce_revelation>({level: Int32.decode, nonce: FixedBytes.decode<32>({len: 32})}, {order: ['level', 'nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Reveal generated for Proto012PsithacaOperationAlphaContents__Reveal
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Reveal extends Box<Proto012PsithacaOperationAlphaContents__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Reveal {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Reveal>({source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Reveal_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_key: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Reveal_public_key.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Register_global_constant generated for Proto012PsithacaOperationAlphaContents__Register_global_constant
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Register_global_constant extends Box<Proto012PsithacaOperationAlphaContents__Register_global_constant> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Register_global_constant {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Register_global_constant>({source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Register_global_constant_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Proposals generated for Proto012PsithacaOperationAlphaContents__Proposals
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Proposals extends Box<Proto012PsithacaOperationAlphaContents__Proposals> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposals']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Proposals {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Proposals>({source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Proposals_source.decode, period: Int32.decode, proposals: Dynamic.decode(Sequence.decode(CGRIDClass__Proto012_PsithacaOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'period', 'proposals']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposals.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposals.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Preendorsement generated for Proto012PsithacaOperationAlphaContents__Preendorsement
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Preendorsement extends Box<Proto012PsithacaOperationAlphaContents__Preendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Preendorsement {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Preendorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Preendorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Origination generated for Proto012PsithacaOperationAlphaContents__Origination
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Origination extends Box<Proto012PsithacaOperationAlphaContents__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Origination {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Origination>({source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, balance: N.decode, delegate: Option.decode(CGRIDClass__Proto012_PsithacaOperationAlphaContents_Origination_delegate.decode), script: CGRIDClass__Proto012_PsithacaScriptedContracts.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Failing_noop generated for Proto012PsithacaOperationAlphaContents__Failing_noop
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Failing_noop extends Box<Proto012PsithacaOperationAlphaContents__Failing_noop> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['arbitrary']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Failing_noop {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Failing_noop>({arbitrary: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['arbitrary']})(p));
    };
    get encodeLength(): number {
        return (this.value.arbitrary.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.arbitrary.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Endorsement generated for Proto012PsithacaOperationAlphaContents__Endorsement
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Endorsement extends Box<Proto012PsithacaOperationAlphaContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Endorsement {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Endorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Endorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_preendorsement_evidence generated for Proto012PsithacaOperationAlphaContents__Double_preendorsement_evidence
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_preendorsement_evidence extends Box<Proto012PsithacaOperationAlphaContents__Double_preendorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_preendorsement_evidence {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Double_preendorsement_evidence>({op1: Dynamic.decode(CGRIDClass__Proto012_PsithacaInlinedPreendorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__Proto012_PsithacaInlinedPreendorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_endorsement_evidence generated for Proto012PsithacaOperationAlphaContents__Double_endorsement_evidence
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_endorsement_evidence extends Box<Proto012PsithacaOperationAlphaContents__Double_endorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_endorsement_evidence {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Double_endorsement_evidence>({op1: Dynamic.decode(CGRIDClass__Proto012_PsithacaInlinedEndorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__Proto012_PsithacaInlinedEndorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_baking_evidence generated for Proto012PsithacaOperationAlphaContents__Double_baking_evidence
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_baking_evidence extends Box<Proto012PsithacaOperationAlphaContents__Double_baking_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bh1', 'bh2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_baking_evidence {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Double_baking_evidence>({bh1: Dynamic.decode(CGRIDClass__Proto012_PsithacaBlock_headerAlphaFull_header.decode, width.Uint30), bh2: Dynamic.decode(CGRIDClass__Proto012_PsithacaBlock_headerAlphaFull_header.decode, width.Uint30)}, {order: ['bh1', 'bh2']})(p));
    };
    get encodeLength(): number {
        return (this.value.bh1.encodeLength +  this.value.bh2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bh1.writeTarget(tgt) +  this.value.bh2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Delegation generated for Proto012PsithacaOperationAlphaContents__Delegation
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Delegation extends Box<Proto012PsithacaOperationAlphaContents__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Delegation {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Delegation>({source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Delegation_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, delegate: Option.decode(CGRIDClass__Proto012_PsithacaOperationAlphaContents_Delegation_delegate.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Ballot generated for Proto012PsithacaOperationAlphaContents__Ballot
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Ballot extends Box<Proto012PsithacaOperationAlphaContents__Ballot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposal', 'ballot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Ballot {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Ballot>({source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Ballot_source.decode, period: Int32.decode, proposal: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Ballot_proposal.decode, ballot: Int8.decode}, {order: ['source', 'period', 'proposal', 'ballot']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposal.encodeLength +  this.value.ballot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposal.writeTarget(tgt) +  this.value.ballot.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationAlphaContents__Activate_account generated for Proto012PsithacaOperationAlphaContents__Activate_account
export class CGRIDClass__Proto012PsithacaOperationAlphaContents__Activate_account extends Box<Proto012PsithacaOperationAlphaContents__Activate_account> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'secret']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationAlphaContents__Activate_account {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContents__Activate_account>({pkh: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Activate_account_pkh.decode, secret: FixedBytes.decode<20>({len: 20})}, {order: ['pkh', 'secret']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.secret.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.secret.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaInlinedPreendorsementContents__Preendorsement generated for Proto012PsithacaInlinedPreendorsementContents__Preendorsement
export class CGRIDClass__Proto012PsithacaInlinedPreendorsementContents__Preendorsement extends Box<Proto012PsithacaInlinedPreendorsementContents__Preendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaInlinedPreendorsementContents__Preendorsement {
        return new this(record_decoder<Proto012PsithacaInlinedPreendorsementContents__Preendorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto012_PsithacaInlinedPreendorsementContents_Preendorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaInlinedEndorsementMempoolContents__Endorsement generated for Proto012PsithacaInlinedEndorsementMempoolContents__Endorsement
export class CGRIDClass__Proto012PsithacaInlinedEndorsementMempoolContents__Endorsement extends Box<Proto012PsithacaInlinedEndorsementMempoolContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaInlinedEndorsementMempoolContents__Endorsement {
        return new this(record_decoder<Proto012PsithacaInlinedEndorsementMempoolContents__Endorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto012_PsithacaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint__set_delegate generated for Proto012PsithacaEntrypoint__set_delegate
export class CGRIDClass__Proto012PsithacaEntrypoint__set_delegate extends Box<Proto012PsithacaEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint__root generated for Proto012PsithacaEntrypoint__root
export class CGRIDClass__Proto012PsithacaEntrypoint__root extends Box<Proto012PsithacaEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint__remove_delegate generated for Proto012PsithacaEntrypoint__remove_delegate
export class CGRIDClass__Proto012PsithacaEntrypoint__remove_delegate extends Box<Proto012PsithacaEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint__named generated for Proto012PsithacaEntrypoint__named
export class CGRIDClass__Proto012PsithacaEntrypoint__named extends Box<Proto012PsithacaEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint___do generated for Proto012PsithacaEntrypoint___do
export class CGRIDClass__Proto012PsithacaEntrypoint___do extends Box<Proto012PsithacaEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaEntrypoint___default generated for Proto012PsithacaEntrypoint___default
export class CGRIDClass__Proto012PsithacaEntrypoint___default extends Box<Proto012PsithacaEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaContractId__Originated generated for Proto012PsithacaContractId__Originated
export class CGRIDClass__Proto012PsithacaContractId__Originated extends Box<Proto012PsithacaContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaContractId__Implicit generated for Proto012PsithacaContractId__Implicit
export class CGRIDClass__Proto012PsithacaContractId__Implicit extends Box<Proto012PsithacaContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaContractId__Implicit {
        return new this(record_decoder<Proto012PsithacaContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto012PsithacaOperationAlphaContents__Transaction = { source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Transaction_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: N, destination: CGRIDClass__Proto012_PsithacaContract_id, parameters: Option<CGRIDClass__Proto012_PsithacaOperationAlphaContents_Transaction_parameters> };
export type Proto012PsithacaOperationAlphaContents__Set_deposits_limit = { source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Set_deposits_limit_source, fee: N, counter: N, gas_limit: N, storage_limit: N, limit: Option<N> };
export type Proto012PsithacaOperationAlphaContents__Seed_nonce_revelation = { level: Int32, nonce: FixedBytes<32> };
export type Proto012PsithacaOperationAlphaContents__Reveal = { source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Reveal_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_key: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Reveal_public_key };
export type Proto012PsithacaOperationAlphaContents__Register_global_constant = { source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Register_global_constant_source, fee: N, counter: N, gas_limit: N, storage_limit: N, value: Dynamic<Bytes,width.Uint30> };
export type Proto012PsithacaOperationAlphaContents__Proposals = { source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Proposals_source, period: Int32, proposals: Dynamic<Sequence<CGRIDClass__Proto012_PsithacaOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq>,width.Uint30> };
export type Proto012PsithacaOperationAlphaContents__Preendorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Preendorsement_block_payload_hash };
export type Proto012PsithacaOperationAlphaContents__Origination = { source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, balance: N, delegate: Option<CGRIDClass__Proto012_PsithacaOperationAlphaContents_Origination_delegate>, script: CGRIDClass__Proto012_PsithacaScriptedContracts };
export type Proto012PsithacaOperationAlphaContents__Failing_noop = { arbitrary: Dynamic<U8String,width.Uint30> };
export type Proto012PsithacaOperationAlphaContents__Endorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Endorsement_block_payload_hash };
export type Proto012PsithacaOperationAlphaContents__Double_preendorsement_evidence = { op1: Dynamic<CGRIDClass__Proto012_PsithacaInlinedPreendorsement,width.Uint30>, op2: Dynamic<CGRIDClass__Proto012_PsithacaInlinedPreendorsement,width.Uint30> };
export type Proto012PsithacaOperationAlphaContents__Double_endorsement_evidence = { op1: Dynamic<CGRIDClass__Proto012_PsithacaInlinedEndorsement,width.Uint30>, op2: Dynamic<CGRIDClass__Proto012_PsithacaInlinedEndorsement,width.Uint30> };
export type Proto012PsithacaOperationAlphaContents__Double_baking_evidence = { bh1: Dynamic<CGRIDClass__Proto012_PsithacaBlock_headerAlphaFull_header,width.Uint30>, bh2: Dynamic<CGRIDClass__Proto012_PsithacaBlock_headerAlphaFull_header,width.Uint30> };
export type Proto012PsithacaOperationAlphaContents__Delegation = { source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Delegation_source, fee: N, counter: N, gas_limit: N, storage_limit: N, delegate: Option<CGRIDClass__Proto012_PsithacaOperationAlphaContents_Delegation_delegate> };
export type Proto012PsithacaOperationAlphaContents__Ballot = { source: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Ballot_source, period: Int32, proposal: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Ballot_proposal, ballot: Int8 };
export type Proto012PsithacaOperationAlphaContents__Activate_account = { pkh: CGRIDClass__Proto012_PsithacaOperationAlphaContents_Activate_account_pkh, secret: FixedBytes<20> };
export type Proto012PsithacaInlinedPreendorsementContents__Preendorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto012_PsithacaInlinedPreendorsementContents_Preendorsement_block_payload_hash };
export type Proto012PsithacaInlinedEndorsementMempoolContents__Endorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto012_PsithacaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash };
export type Proto012PsithacaEntrypoint__set_delegate = Unit;
export type Proto012PsithacaEntrypoint__root = Unit;
export type Proto012PsithacaEntrypoint__remove_delegate = Unit;
export type Proto012PsithacaEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto012PsithacaEntrypoint___do = Unit;
export type Proto012PsithacaEntrypoint___default = Unit;
export type Proto012PsithacaContractId__Originated = Padded<CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad,1>;
export type Proto012PsithacaContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaScriptedContracts generated for Proto012PsithacaScriptedContracts
export class CGRIDClass__Proto012_PsithacaScriptedContracts extends Box<Proto012PsithacaScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaScriptedContracts {
        return new this(record_decoder<Proto012PsithacaScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_and_signature_signature generated for Proto012PsithacaOperationAlphaContentsAndSignatureSignature
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_and_signature_signature extends Box<Proto012PsithacaOperationAlphaContentsAndSignatureSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_and_signature_signature {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsAndSignatureSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Transaction_source generated for Proto012PsithacaOperationAlphaContentsTransactionSource
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Transaction_source extends Box<Proto012PsithacaOperationAlphaContentsTransactionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Transaction_source {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsTransactionSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Transaction_parameters generated for Proto012PsithacaOperationAlphaContentsTransactionParameters
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Transaction_parameters extends Box<Proto012PsithacaOperationAlphaContentsTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Transaction_parameters {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsTransactionParameters>({entrypoint: CGRIDClass__Proto012_PsithacaEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Set_deposits_limit_source generated for Proto012PsithacaOperationAlphaContentsSetDepositsLimitSource
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Set_deposits_limit_source extends Box<Proto012PsithacaOperationAlphaContentsSetDepositsLimitSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Set_deposits_limit_source {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsSetDepositsLimitSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Reveal_source generated for Proto012PsithacaOperationAlphaContentsRevealSource
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Reveal_source extends Box<Proto012PsithacaOperationAlphaContentsRevealSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Reveal_source {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsRevealSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Reveal_public_key generated for Proto012PsithacaOperationAlphaContentsRevealPublicKey
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Reveal_public_key extends Box<Proto012PsithacaOperationAlphaContentsRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Reveal_public_key {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Register_global_constant_source generated for Proto012PsithacaOperationAlphaContentsRegisterGlobalConstantSource
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Register_global_constant_source extends Box<Proto012PsithacaOperationAlphaContentsRegisterGlobalConstantSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Register_global_constant_source {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsRegisterGlobalConstantSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Proposals_source generated for Proto012PsithacaOperationAlphaContentsProposalsSource
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Proposals_source extends Box<Proto012PsithacaOperationAlphaContentsProposalsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Proposals_source {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsProposalsSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq generated for Proto012PsithacaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq extends Box<Proto012PsithacaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Preendorsement_block_payload_hash generated for Proto012PsithacaOperationAlphaContentsPreendorsementBlockPayloadHash
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Preendorsement_block_payload_hash extends Box<Proto012PsithacaOperationAlphaContentsPreendorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Preendorsement_block_payload_hash {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsPreendorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Origination_source generated for Proto012PsithacaOperationAlphaContentsOriginationSource
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Origination_source extends Box<Proto012PsithacaOperationAlphaContentsOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Origination_source {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsOriginationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Origination_delegate generated for Proto012PsithacaOperationAlphaContentsOriginationDelegate
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Origination_delegate extends Box<Proto012PsithacaOperationAlphaContentsOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Origination_delegate {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Endorsement_block_payload_hash generated for Proto012PsithacaOperationAlphaContentsEndorsementBlockPayloadHash
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Endorsement_block_payload_hash extends Box<Proto012PsithacaOperationAlphaContentsEndorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Endorsement_block_payload_hash {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsEndorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Delegation_source generated for Proto012PsithacaOperationAlphaContentsDelegationSource
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Delegation_source extends Box<Proto012PsithacaOperationAlphaContentsDelegationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Delegation_source {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsDelegationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Delegation_delegate generated for Proto012PsithacaOperationAlphaContentsDelegationDelegate
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Delegation_delegate extends Box<Proto012PsithacaOperationAlphaContentsDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Delegation_delegate {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Ballot_source generated for Proto012PsithacaOperationAlphaContentsBallotSource
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Ballot_source extends Box<Proto012PsithacaOperationAlphaContentsBallotSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Ballot_source {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsBallotSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Ballot_proposal generated for Proto012PsithacaOperationAlphaContentsBallotProposal
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Ballot_proposal extends Box<Proto012PsithacaOperationAlphaContentsBallotProposal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Ballot_proposal {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsBallotProposal>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Activate_account_pkh generated for Proto012PsithacaOperationAlphaContentsActivateAccountPkh
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents_Activate_account_pkh extends Box<Proto012PsithacaOperationAlphaContentsActivateAccountPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents_Activate_account_pkh {
        return new this(record_decoder<Proto012PsithacaOperationAlphaContentsActivateAccountPkh>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperationAlphaContents generated for Proto012PsithacaOperationAlphaContents
export function proto012psithacaoperationalphacontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaOperationAlphaContents,Proto012PsithacaOperationAlphaContents> {
    function f(disc: CGRIDTag__Proto012PsithacaOperationAlphaContents) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Seed_nonce_revelation: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Seed_nonce_revelation.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Double_endorsement_evidence: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_endorsement_evidence.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Double_baking_evidence: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_baking_evidence.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Activate_account: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Activate_account.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Proposals: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Proposals.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Ballot: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Ballot.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Double_preendorsement_evidence: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_preendorsement_evidence.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Failing_noop: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Failing_noop.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Preendorsement: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Preendorsement.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Endorsement: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Endorsement.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Reveal: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Reveal.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Transaction: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Transaction.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Origination: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Origination.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Delegation: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Delegation.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Register_global_constant: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Register_global_constant.decode;
            case CGRIDTag__Proto012PsithacaOperationAlphaContents.Set_deposits_limit: return CGRIDClass__Proto012PsithacaOperationAlphaContents__Set_deposits_limit.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaOperationAlphaContents => Object.values(CGRIDTag__Proto012PsithacaOperationAlphaContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaOperationAlphaContents extends Box<Proto012PsithacaOperationAlphaContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaOperationAlphaContents>, Proto012PsithacaOperationAlphaContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperationAlphaContents {
        return new this(variant_decoder(width.Uint8)(proto012psithacaoperationalphacontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaInlinedPreendorsement_signature generated for Proto012PsithacaInlinedPreendorsementSignature
export class CGRIDClass__Proto012_PsithacaInlinedPreendorsement_signature extends Box<Proto012PsithacaInlinedPreendorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaInlinedPreendorsement_signature {
        return new this(record_decoder<Proto012PsithacaInlinedPreendorsementSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaInlinedPreendorsementContents_Preendorsement_block_payload_hash generated for Proto012PsithacaInlinedPreendorsementContentsPreendorsementBlockPayloadHash
export class CGRIDClass__Proto012_PsithacaInlinedPreendorsementContents_Preendorsement_block_payload_hash extends Box<Proto012PsithacaInlinedPreendorsementContentsPreendorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaInlinedPreendorsementContents_Preendorsement_block_payload_hash {
        return new this(record_decoder<Proto012PsithacaInlinedPreendorsementContentsPreendorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaInlinedPreendorsementContents generated for Proto012PsithacaInlinedPreendorsementContents
export function proto012psithacainlinedpreendorsementcontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaInlinedPreendorsementContents,Proto012PsithacaInlinedPreendorsementContents> {
    function f(disc: CGRIDTag__Proto012PsithacaInlinedPreendorsementContents) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaInlinedPreendorsementContents.Preendorsement: return CGRIDClass__Proto012PsithacaInlinedPreendorsementContents__Preendorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaInlinedPreendorsementContents => Object.values(CGRIDTag__Proto012PsithacaInlinedPreendorsementContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaInlinedPreendorsementContents extends Box<Proto012PsithacaInlinedPreendorsementContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaInlinedPreendorsementContents>, Proto012PsithacaInlinedPreendorsementContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaInlinedPreendorsementContents {
        return new this(variant_decoder(width.Uint8)(proto012psithacainlinedpreendorsementcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaInlinedPreendorsement generated for Proto012PsithacaInlinedPreendorsement
export class CGRIDClass__Proto012_PsithacaInlinedPreendorsement extends Box<Proto012PsithacaInlinedPreendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaInlinedPreendorsement {
        return new this(record_decoder<Proto012PsithacaInlinedPreendorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__Proto012_PsithacaInlinedPreendorsementContents.decode, signature: Nullable.decode(CGRIDClass__Proto012_PsithacaInlinedPreendorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaInlinedEndorsement_signature generated for Proto012PsithacaInlinedEndorsementSignature
export class CGRIDClass__Proto012_PsithacaInlinedEndorsement_signature extends Box<Proto012PsithacaInlinedEndorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaInlinedEndorsement_signature {
        return new this(record_decoder<Proto012PsithacaInlinedEndorsementSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash generated for Proto012PsithacaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash
export class CGRIDClass__Proto012_PsithacaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash extends Box<Proto012PsithacaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash {
        return new this(record_decoder<Proto012PsithacaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaInlinedEndorsement_mempoolContents generated for Proto012PsithacaInlinedEndorsementMempoolContents
export function proto012psithacainlinedendorsementmempoolcontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaInlinedEndorsementMempoolContents,Proto012PsithacaInlinedEndorsementMempoolContents> {
    function f(disc: CGRIDTag__Proto012PsithacaInlinedEndorsementMempoolContents) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaInlinedEndorsementMempoolContents.Endorsement: return CGRIDClass__Proto012PsithacaInlinedEndorsementMempoolContents__Endorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaInlinedEndorsementMempoolContents => Object.values(CGRIDTag__Proto012PsithacaInlinedEndorsementMempoolContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaInlinedEndorsement_mempoolContents extends Box<Proto012PsithacaInlinedEndorsementMempoolContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaInlinedEndorsementMempoolContents>, Proto012PsithacaInlinedEndorsementMempoolContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaInlinedEndorsement_mempoolContents {
        return new this(variant_decoder(width.Uint8)(proto012psithacainlinedendorsementmempoolcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaInlinedEndorsement generated for Proto012PsithacaInlinedEndorsement
export class CGRIDClass__Proto012_PsithacaInlinedEndorsement extends Box<Proto012PsithacaInlinedEndorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaInlinedEndorsement {
        return new this(record_decoder<Proto012PsithacaInlinedEndorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__Proto012_PsithacaInlinedEndorsement_mempoolContents.decode, signature: Nullable.decode(CGRIDClass__Proto012_PsithacaInlinedEndorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaEntrypoint generated for Proto012PsithacaEntrypoint
export function proto012psithacaentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaEntrypoint,Proto012PsithacaEntrypoint> {
    function f(disc: CGRIDTag__Proto012PsithacaEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaEntrypoint._default: return CGRIDClass__Proto012PsithacaEntrypoint___default.decode;
            case CGRIDTag__Proto012PsithacaEntrypoint.root: return CGRIDClass__Proto012PsithacaEntrypoint__root.decode;
            case CGRIDTag__Proto012PsithacaEntrypoint._do: return CGRIDClass__Proto012PsithacaEntrypoint___do.decode;
            case CGRIDTag__Proto012PsithacaEntrypoint.set_delegate: return CGRIDClass__Proto012PsithacaEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto012PsithacaEntrypoint.remove_delegate: return CGRIDClass__Proto012PsithacaEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto012PsithacaEntrypoint.named: return CGRIDClass__Proto012PsithacaEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaEntrypoint => Object.values(CGRIDTag__Proto012PsithacaEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaEntrypoint extends Box<Proto012PsithacaEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaEntrypoint>, Proto012PsithacaEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto012psithacaentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad generated for Proto012PsithacaContractIdOriginatedDenestPad
export class CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad extends Box<Proto012PsithacaContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto012PsithacaContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaContract_id generated for Proto012PsithacaContractId
export function proto012psithacacontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaContractId,Proto012PsithacaContractId> {
    function f(disc: CGRIDTag__Proto012PsithacaContractId) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaContractId.Implicit: return CGRIDClass__Proto012PsithacaContractId__Implicit.decode;
            case CGRIDTag__Proto012PsithacaContractId.Originated: return CGRIDClass__Proto012PsithacaContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaContractId => Object.values(CGRIDTag__Proto012PsithacaContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaContract_id extends Box<Proto012PsithacaContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaContractId>, Proto012PsithacaContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaContract_id {
        return new this(variant_decoder(width.Uint8)(proto012psithacacontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_payload_hash generated for Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_payload_hash extends Box<Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaBlock_headerAlphaSigned_contents_signature generated for Proto012PsithacaBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto012_PsithacaBlock_headerAlphaSigned_contents_signature extends Box<Proto012PsithacaBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto012PsithacaBlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaBlock_headerAlphaFull_header generated for Proto012PsithacaBlockHeaderAlphaFullHeader
export class CGRIDClass__Proto012_PsithacaBlock_headerAlphaFull_header extends Box<Proto012PsithacaBlockHeaderAlphaFullHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaBlock_headerAlphaFull_header {
        return new this(record_decoder<Proto012PsithacaBlockHeaderAlphaFullHeader>({level: Int32.decode, proto: Uint8.decode, predecessor: CGRIDClass__Block_headerShell_predecessor.decode, timestamp: Int64.decode, validation_pass: Uint8.decode, operations_hash: CGRIDClass__Block_headerShell_operations_hash.decode, fitness: Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30), context: CGRIDClass__Block_headerShell_context.decode, payload_hash: CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_escape_vote: Bool.decode, signature: CGRIDClass__Proto012_PsithacaBlock_headerAlphaSigned_contents_signature.decode}, {order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.proto.encodeLength +  this.value.predecessor.encodeLength +  this.value.timestamp.encodeLength +  this.value.validation_pass.encodeLength +  this.value.operations_hash.encodeLength +  this.value.fitness.encodeLength +  this.value.context.encodeLength +  this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_escape_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.proto.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.timestamp.writeTarget(tgt) +  this.value.validation_pass.writeTarget(tgt) +  this.value.operations_hash.writeTarget(tgt) +  this.value.fitness.writeTarget(tgt) +  this.value.context.writeTarget(tgt) +  this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_escape_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_predecessor generated for BlockHeaderShellPredecessor
export class CGRIDClass__Block_headerShell_predecessor extends Box<BlockHeaderShellPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_predecessor {
        return new this(record_decoder<BlockHeaderShellPredecessor>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_operations_hash generated for BlockHeaderShellOperationsHash
export class CGRIDClass__Block_headerShell_operations_hash extends Box<BlockHeaderShellOperationsHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_list_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_operations_hash {
        return new this(record_decoder<BlockHeaderShellOperationsHash>({operation_list_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_list_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_list_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_list_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_context generated for BlockHeaderShellContext
export class CGRIDClass__Block_headerShell_context extends Box<BlockHeaderShellContext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_context {
        return new this(record_decoder<BlockHeaderShellContext>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export type BlockHeaderShellPredecessor = { block_hash: FixedBytes<32> };
export type BlockHeaderShellOperationsHash = { operation_list_list_hash: FixedBytes<32> };
export type BlockHeaderShellContext = { context_hash: FixedBytes<32> };
export type Proto012PsithacaScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export type Proto012PsithacaOperationAlphaContentsAndSignatureSignature = { signature_v0: FixedBytes<64> };
export type Proto012PsithacaOperationAlphaContentsTransactionSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationAlphaContentsTransactionParameters = { entrypoint: CGRIDClass__Proto012_PsithacaEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto012PsithacaOperationAlphaContentsSetDepositsLimitSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationAlphaContentsRevealSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationAlphaContentsRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto012PsithacaOperationAlphaContentsRegisterGlobalConstantSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationAlphaContentsProposalsSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationAlphaContentsProposalsProposalsDenestDynDenestSeq = { protocol_hash: FixedBytes<32> };
export type Proto012PsithacaOperationAlphaContentsPreendorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export type Proto012PsithacaOperationAlphaContentsOriginationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationAlphaContentsOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationAlphaContentsEndorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export type Proto012PsithacaOperationAlphaContentsDelegationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationAlphaContentsDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationAlphaContentsBallotSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationAlphaContentsBallotProposal = { protocol_hash: FixedBytes<32> };
export type Proto012PsithacaOperationAlphaContentsActivateAccountPkh = { ed25519_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__Proto012PsithacaOperationAlphaContents{
    Seed_nonce_revelation = 1,
    Double_endorsement_evidence = 2,
    Double_baking_evidence = 3,
    Activate_account = 4,
    Proposals = 5,
    Ballot = 6,
    Double_preendorsement_evidence = 7,
    Failing_noop = 17,
    Preendorsement = 20,
    Endorsement = 21,
    Reveal = 107,
    Transaction = 108,
    Origination = 109,
    Delegation = 110,
    Register_global_constant = 111,
    Set_deposits_limit = 112
}
export interface CGRIDMap__Proto012PsithacaOperationAlphaContents {
    Seed_nonce_revelation: CGRIDClass__Proto012PsithacaOperationAlphaContents__Seed_nonce_revelation,
    Double_endorsement_evidence: CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_endorsement_evidence,
    Double_baking_evidence: CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_baking_evidence,
    Activate_account: CGRIDClass__Proto012PsithacaOperationAlphaContents__Activate_account,
    Proposals: CGRIDClass__Proto012PsithacaOperationAlphaContents__Proposals,
    Ballot: CGRIDClass__Proto012PsithacaOperationAlphaContents__Ballot,
    Double_preendorsement_evidence: CGRIDClass__Proto012PsithacaOperationAlphaContents__Double_preendorsement_evidence,
    Failing_noop: CGRIDClass__Proto012PsithacaOperationAlphaContents__Failing_noop,
    Preendorsement: CGRIDClass__Proto012PsithacaOperationAlphaContents__Preendorsement,
    Endorsement: CGRIDClass__Proto012PsithacaOperationAlphaContents__Endorsement,
    Reveal: CGRIDClass__Proto012PsithacaOperationAlphaContents__Reveal,
    Transaction: CGRIDClass__Proto012PsithacaOperationAlphaContents__Transaction,
    Origination: CGRIDClass__Proto012PsithacaOperationAlphaContents__Origination,
    Delegation: CGRIDClass__Proto012PsithacaOperationAlphaContents__Delegation,
    Register_global_constant: CGRIDClass__Proto012PsithacaOperationAlphaContents__Register_global_constant,
    Set_deposits_limit: CGRIDClass__Proto012PsithacaOperationAlphaContents__Set_deposits_limit
}
export type Proto012PsithacaOperationAlphaContents = { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Seed_nonce_revelation, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Seed_nonce_revelation'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Double_endorsement_evidence, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Double_endorsement_evidence'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Double_baking_evidence, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Double_baking_evidence'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Activate_account, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Activate_account'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Proposals, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Proposals'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Ballot, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Ballot'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Double_preendorsement_evidence, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Double_preendorsement_evidence'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Failing_noop, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Failing_noop'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Preendorsement, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Preendorsement'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Endorsement, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Endorsement'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Reveal, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Reveal'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Transaction, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Transaction'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Origination, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Origination'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Delegation, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Delegation'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Register_global_constant, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Register_global_constant'] } | { kind: CGRIDTag__Proto012PsithacaOperationAlphaContents.Set_deposits_limit, value: CGRIDMap__Proto012PsithacaOperationAlphaContents['Set_deposits_limit'] };
export type Proto012PsithacaInlinedPreendorsementSignature = { signature_v0: FixedBytes<64> };
export type Proto012PsithacaInlinedPreendorsementContentsPreendorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export enum CGRIDTag__Proto012PsithacaInlinedPreendorsementContents{
    Preendorsement = 20
}
export interface CGRIDMap__Proto012PsithacaInlinedPreendorsementContents {
    Preendorsement: CGRIDClass__Proto012PsithacaInlinedPreendorsementContents__Preendorsement
}
export type Proto012PsithacaInlinedPreendorsementContents = { kind: CGRIDTag__Proto012PsithacaInlinedPreendorsementContents.Preendorsement, value: CGRIDMap__Proto012PsithacaInlinedPreendorsementContents['Preendorsement'] };
export type Proto012PsithacaInlinedPreendorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__Proto012_PsithacaInlinedPreendorsementContents, signature: Nullable<CGRIDClass__Proto012_PsithacaInlinedPreendorsement_signature> };
export type Proto012PsithacaInlinedEndorsementSignature = { signature_v0: FixedBytes<64> };
export type Proto012PsithacaInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export enum CGRIDTag__Proto012PsithacaInlinedEndorsementMempoolContents{
    Endorsement = 21
}
export interface CGRIDMap__Proto012PsithacaInlinedEndorsementMempoolContents {
    Endorsement: CGRIDClass__Proto012PsithacaInlinedEndorsementMempoolContents__Endorsement
}
export type Proto012PsithacaInlinedEndorsementMempoolContents = { kind: CGRIDTag__Proto012PsithacaInlinedEndorsementMempoolContents.Endorsement, value: CGRIDMap__Proto012PsithacaInlinedEndorsementMempoolContents['Endorsement'] };
export type Proto012PsithacaInlinedEndorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__Proto012_PsithacaInlinedEndorsement_mempoolContents, signature: Nullable<CGRIDClass__Proto012_PsithacaInlinedEndorsement_signature> };
export enum CGRIDTag__Proto012PsithacaEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto012PsithacaEntrypoint {
    _default: CGRIDClass__Proto012PsithacaEntrypoint___default,
    root: CGRIDClass__Proto012PsithacaEntrypoint__root,
    _do: CGRIDClass__Proto012PsithacaEntrypoint___do,
    set_delegate: CGRIDClass__Proto012PsithacaEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto012PsithacaEntrypoint__remove_delegate,
    named: CGRIDClass__Proto012PsithacaEntrypoint__named
}
export type Proto012PsithacaEntrypoint = { kind: CGRIDTag__Proto012PsithacaEntrypoint._default, value: CGRIDMap__Proto012PsithacaEntrypoint['_default'] } | { kind: CGRIDTag__Proto012PsithacaEntrypoint.root, value: CGRIDMap__Proto012PsithacaEntrypoint['root'] } | { kind: CGRIDTag__Proto012PsithacaEntrypoint._do, value: CGRIDMap__Proto012PsithacaEntrypoint['_do'] } | { kind: CGRIDTag__Proto012PsithacaEntrypoint.set_delegate, value: CGRIDMap__Proto012PsithacaEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto012PsithacaEntrypoint.remove_delegate, value: CGRIDMap__Proto012PsithacaEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto012PsithacaEntrypoint.named, value: CGRIDMap__Proto012PsithacaEntrypoint['named'] };
export type Proto012PsithacaContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto012PsithacaContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto012PsithacaContractId {
    Implicit: CGRIDClass__Proto012PsithacaContractId__Implicit,
    Originated: CGRIDClass__Proto012PsithacaContractId__Originated
}
export type Proto012PsithacaContractId = { kind: CGRIDTag__Proto012PsithacaContractId.Implicit, value: CGRIDMap__Proto012PsithacaContractId['Implicit'] } | { kind: CGRIDTag__Proto012PsithacaContractId.Originated, value: CGRIDMap__Proto012PsithacaContractId['Originated'] };
export type Proto012PsithacaBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto012PsithacaBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type Proto012PsithacaBlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto012PsithacaBlockHeaderAlphaFullHeader = { level: Int32, proto: Uint8, predecessor: CGRIDClass__Block_headerShell_predecessor, timestamp: Int64, validation_pass: Uint8, operations_hash: CGRIDClass__Block_headerShell_operations_hash, fitness: Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>, context: CGRIDClass__Block_headerShell_context, payload_hash: CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto012_PsithacaBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_escape_vote: Bool, signature: CGRIDClass__Proto012_PsithacaBlock_headerAlphaSigned_contents_signature };
export type Proto012PsithacaOperation = { branch: CGRIDClass__OperationShell_header_branch, contents: VPadded<Sequence<CGRIDClass__Proto012_PsithacaOperationAlphaContents>,64>, signature: CGRIDClass__Proto012_PsithacaOperationAlphaContents_and_signature_signature };
export class CGRIDClass__Proto012PsithacaOperation extends Box<Proto012PsithacaOperation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'contents', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperation {
        return new this(record_decoder<Proto012PsithacaOperation>({branch: CGRIDClass__OperationShell_header_branch.decode, contents: VPadded.decode(Sequence.decode(CGRIDClass__Proto012_PsithacaOperationAlphaContents.decode), 64), signature: CGRIDClass__Proto012_PsithacaOperationAlphaContents_and_signature_signature.decode}, {order: ['branch', 'contents', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.contents.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.contents.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export const proto012_psithaca_operation_encoder = (value: Proto012PsithacaOperation): OutputBytes => {
    return record_encoder({order: ['branch', 'contents', 'signature']})(value);
}
export const proto012_psithaca_operation_decoder = (p: Parser): Proto012PsithacaOperation => {
    return record_decoder<Proto012PsithacaOperation>({branch: CGRIDClass__OperationShell_header_branch.decode, contents: VPadded.decode(Sequence.decode(CGRIDClass__Proto012_PsithacaOperationAlphaContents.decode), 64), signature: CGRIDClass__Proto012_PsithacaOperationAlphaContents_and_signature_signature.decode}, {order: ['branch', 'contents', 'signature']})(p);
}
