import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto012PsithacaVotingPeriodKind__exploration generated for Proto012PsithacaVotingPeriodKind__exploration
export class CGRIDClass__Proto012PsithacaVotingPeriodKind__exploration extends Box<Proto012PsithacaVotingPeriodKind__exploration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriodKind__exploration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaVotingPeriodKind__Proposal generated for Proto012PsithacaVotingPeriodKind__Proposal
export class CGRIDClass__Proto012PsithacaVotingPeriodKind__Proposal extends Box<Proto012PsithacaVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaVotingPeriodKind__Promotion generated for Proto012PsithacaVotingPeriodKind__Promotion
export class CGRIDClass__Proto012PsithacaVotingPeriodKind__Promotion extends Box<Proto012PsithacaVotingPeriodKind__Promotion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriodKind__Promotion {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaVotingPeriodKind__Cooldown generated for Proto012PsithacaVotingPeriodKind__Cooldown
export class CGRIDClass__Proto012PsithacaVotingPeriodKind__Cooldown extends Box<Proto012PsithacaVotingPeriodKind__Cooldown> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriodKind__Cooldown {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaVotingPeriodKind__Adoption generated for Proto012PsithacaVotingPeriodKind__Adoption
export class CGRIDClass__Proto012PsithacaVotingPeriodKind__Adoption extends Box<Proto012PsithacaVotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto012PsithacaVotingPeriodKind__exploration = Unit;
export type Proto012PsithacaVotingPeriodKind__Proposal = Unit;
export type Proto012PsithacaVotingPeriodKind__Promotion = Unit;
export type Proto012PsithacaVotingPeriodKind__Cooldown = Unit;
export type Proto012PsithacaVotingPeriodKind__Adoption = Unit;
// Class CGRIDClass__Proto012_PsithacaVoting_period_kind generated for Proto012PsithacaVotingPeriodKind
export function proto012psithacavotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaVotingPeriodKind,Proto012PsithacaVotingPeriodKind> {
    function f(disc: CGRIDTag__Proto012PsithacaVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaVotingPeriodKind.Proposal: return CGRIDClass__Proto012PsithacaVotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto012PsithacaVotingPeriodKind.exploration: return CGRIDClass__Proto012PsithacaVotingPeriodKind__exploration.decode;
            case CGRIDTag__Proto012PsithacaVotingPeriodKind.Cooldown: return CGRIDClass__Proto012PsithacaVotingPeriodKind__Cooldown.decode;
            case CGRIDTag__Proto012PsithacaVotingPeriodKind.Promotion: return CGRIDClass__Proto012PsithacaVotingPeriodKind__Promotion.decode;
            case CGRIDTag__Proto012PsithacaVotingPeriodKind.Adoption: return CGRIDClass__Proto012PsithacaVotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaVotingPeriodKind => Object.values(CGRIDTag__Proto012PsithacaVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaVoting_period_kind extends Box<Proto012PsithacaVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaVotingPeriodKind>, Proto012PsithacaVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaVoting_period_kind {
        return new this(variant_decoder(width.Uint8)(proto012psithacavotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__Proto012PsithacaVotingPeriodKind{
    Proposal = 0,
    exploration = 1,
    Cooldown = 2,
    Promotion = 3,
    Adoption = 4
}
export interface CGRIDMap__Proto012PsithacaVotingPeriodKind {
    Proposal: CGRIDClass__Proto012PsithacaVotingPeriodKind__Proposal,
    exploration: CGRIDClass__Proto012PsithacaVotingPeriodKind__exploration,
    Cooldown: CGRIDClass__Proto012PsithacaVotingPeriodKind__Cooldown,
    Promotion: CGRIDClass__Proto012PsithacaVotingPeriodKind__Promotion,
    Adoption: CGRIDClass__Proto012PsithacaVotingPeriodKind__Adoption
}
export type Proto012PsithacaVotingPeriodKind = { kind: CGRIDTag__Proto012PsithacaVotingPeriodKind.Proposal, value: CGRIDMap__Proto012PsithacaVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto012PsithacaVotingPeriodKind.exploration, value: CGRIDMap__Proto012PsithacaVotingPeriodKind['exploration'] } | { kind: CGRIDTag__Proto012PsithacaVotingPeriodKind.Cooldown, value: CGRIDMap__Proto012PsithacaVotingPeriodKind['Cooldown'] } | { kind: CGRIDTag__Proto012PsithacaVotingPeriodKind.Promotion, value: CGRIDMap__Proto012PsithacaVotingPeriodKind['Promotion'] } | { kind: CGRIDTag__Proto012PsithacaVotingPeriodKind.Adoption, value: CGRIDMap__Proto012PsithacaVotingPeriodKind['Adoption'] };
export type Proto012PsithacaVotingPeriod = { index: Int32, kind: CGRIDClass__Proto012_PsithacaVoting_period_kind, start_position: Int32 };
export class CGRIDClass__Proto012PsithacaVotingPeriod extends Box<Proto012PsithacaVotingPeriod> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['index', 'kind', 'start_position']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVotingPeriod {
        return new this(record_decoder<Proto012PsithacaVotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto012_PsithacaVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p));
    };
    get encodeLength(): number {
        return (this.value.index.encodeLength +  this.value.kind.encodeLength +  this.value.start_position.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.index.writeTarget(tgt) +  this.value.kind.writeTarget(tgt) +  this.value.start_position.writeTarget(tgt));
    }
}
export const proto012_psithaca_voting_period_encoder = (value: Proto012PsithacaVotingPeriod): OutputBytes => {
    return record_encoder({order: ['index', 'kind', 'start_position']})(value);
}
export const proto012_psithaca_voting_period_decoder = (p: Parser): Proto012PsithacaVotingPeriod => {
    return record_decoder<Proto012PsithacaVotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto012_PsithacaVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p);
}
