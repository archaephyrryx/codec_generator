import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int16, Int31, Int32, Int64, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaConstantsParametricDelegateSelection__Round_robin_over_delegates generated for Proto012PsithacaConstantsParametricDelegateSelection__Round_robin_over_delegates
export class CGRIDClass__Proto012PsithacaConstantsParametricDelegateSelection__Round_robin_over_delegates extends Box<Proto012PsithacaConstantsParametricDelegateSelection__Round_robin_over_delegates> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaConstantsParametricDelegateSelection__Round_robin_over_delegates {
        return new this(Dynamic.decode(Sequence.decode(Dynamic.decode(Sequence.decode(CGRIDClass__Proto012_PsithacaConstantsParametric_delegate_selection_Round_robin_over_delegates_denest_dyn_denest_seq_denest_dyn_denest_seq.decode), width.Uint30)), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaConstantsParametricDelegateSelection__Random_delegate_selection generated for Proto012PsithacaConstantsParametricDelegateSelection__Random_delegate_selection
export class CGRIDClass__Proto012PsithacaConstantsParametricDelegateSelection__Random_delegate_selection extends Box<Proto012PsithacaConstantsParametricDelegateSelection__Random_delegate_selection> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaConstantsParametricDelegateSelection__Random_delegate_selection {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type Proto012PsithacaConstantsParametricDelegateSelection__Round_robin_over_delegates = Dynamic<Sequence<Dynamic<Sequence<CGRIDClass__Proto012_PsithacaConstantsParametric_delegate_selection_Round_robin_over_delegates_denest_dyn_denest_seq_denest_dyn_denest_seq>,width.Uint30>>,width.Uint30>;
export type Proto012PsithacaConstantsParametricDelegateSelection__Random_delegate_selection = Unit;
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement generated for Proto012PsithacaConstantsParametricRatioOfFrozenDepositsSlashedPerDoubleEndorsement
export class CGRIDClass__Proto012_PsithacaConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement extends Box<Proto012PsithacaConstantsParametricRatioOfFrozenDepositsSlashedPerDoubleEndorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['numerator', 'denominator']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement {
        return new this(record_decoder<Proto012PsithacaConstantsParametricRatioOfFrozenDepositsSlashedPerDoubleEndorsement>({numerator: Uint16.decode, denominator: Uint16.decode}, {order: ['numerator', 'denominator']})(p));
    };
    get encodeLength(): number {
        return (this.value.numerator.encodeLength +  this.value.denominator.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.numerator.writeTarget(tgt) +  this.value.denominator.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaConstantsParametric_minimal_participation_ratio generated for Proto012PsithacaConstantsParametricMinimalParticipationRatio
export class CGRIDClass__Proto012_PsithacaConstantsParametric_minimal_participation_ratio extends Box<Proto012PsithacaConstantsParametricMinimalParticipationRatio> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['numerator', 'denominator']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaConstantsParametric_minimal_participation_ratio {
        return new this(record_decoder<Proto012PsithacaConstantsParametricMinimalParticipationRatio>({numerator: Uint16.decode, denominator: Uint16.decode}, {order: ['numerator', 'denominator']})(p));
    };
    get encodeLength(): number {
        return (this.value.numerator.encodeLength +  this.value.denominator.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.numerator.writeTarget(tgt) +  this.value.denominator.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaConstantsParametric_delegate_selection_Round_robin_over_delegates_denest_dyn_denest_seq_denest_dyn_denest_seq generated for Proto012PsithacaConstantsParametricDelegateSelectionRoundRobinOverDelegatesDenestDynDenestSeqDenestDynDenestSeq
export class CGRIDClass__Proto012_PsithacaConstantsParametric_delegate_selection_Round_robin_over_delegates_denest_dyn_denest_seq_denest_dyn_denest_seq extends Box<Proto012PsithacaConstantsParametricDelegateSelectionRoundRobinOverDelegatesDenestDynDenestSeqDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaConstantsParametric_delegate_selection_Round_robin_over_delegates_denest_dyn_denest_seq_denest_dyn_denest_seq {
        return new this(record_decoder<Proto012PsithacaConstantsParametricDelegateSelectionRoundRobinOverDelegatesDenestDynDenestSeqDenestDynDenestSeq>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaConstantsParametric_delegate_selection generated for Proto012PsithacaConstantsParametricDelegateSelection
export function proto012psithacaconstantsparametricdelegateselection_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaConstantsParametricDelegateSelection,Proto012PsithacaConstantsParametricDelegateSelection> {
    function f(disc: CGRIDTag__Proto012PsithacaConstantsParametricDelegateSelection) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaConstantsParametricDelegateSelection.Random_delegate_selection: return CGRIDClass__Proto012PsithacaConstantsParametricDelegateSelection__Random_delegate_selection.decode;
            case CGRIDTag__Proto012PsithacaConstantsParametricDelegateSelection.Round_robin_over_delegates: return CGRIDClass__Proto012PsithacaConstantsParametricDelegateSelection__Round_robin_over_delegates.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaConstantsParametricDelegateSelection => Object.values(CGRIDTag__Proto012PsithacaConstantsParametricDelegateSelection).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaConstantsParametric_delegate_selection extends Box<Proto012PsithacaConstantsParametricDelegateSelection> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaConstantsParametricDelegateSelection>, Proto012PsithacaConstantsParametricDelegateSelection>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaConstantsParametric_delegate_selection {
        return new this(variant_decoder(width.Uint8)(proto012psithacaconstantsparametricdelegateselection_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type Proto012PsithacaConstantsParametricRatioOfFrozenDepositsSlashedPerDoubleEndorsement = { numerator: Uint16, denominator: Uint16 };
export type Proto012PsithacaConstantsParametricMinimalParticipationRatio = { numerator: Uint16, denominator: Uint16 };
export type Proto012PsithacaConstantsParametricDelegateSelectionRoundRobinOverDelegatesDenestDynDenestSeqDenestDynDenestSeq = { signature_v0_public_key: CGRIDClass__Public_key };
export enum CGRIDTag__Proto012PsithacaConstantsParametricDelegateSelection{
    Random_delegate_selection = 0,
    Round_robin_over_delegates = 1
}
export interface CGRIDMap__Proto012PsithacaConstantsParametricDelegateSelection {
    Random_delegate_selection: CGRIDClass__Proto012PsithacaConstantsParametricDelegateSelection__Random_delegate_selection,
    Round_robin_over_delegates: CGRIDClass__Proto012PsithacaConstantsParametricDelegateSelection__Round_robin_over_delegates
}
export type Proto012PsithacaConstantsParametricDelegateSelection = { kind: CGRIDTag__Proto012PsithacaConstantsParametricDelegateSelection.Random_delegate_selection, value: CGRIDMap__Proto012PsithacaConstantsParametricDelegateSelection['Random_delegate_selection'] } | { kind: CGRIDTag__Proto012PsithacaConstantsParametricDelegateSelection.Round_robin_over_delegates, value: CGRIDMap__Proto012PsithacaConstantsParametricDelegateSelection['Round_robin_over_delegates'] };
export type Proto012PsithacaConstantsParametric = { preserved_cycles: Uint8, blocks_per_cycle: Int32, blocks_per_commitment: Int32, blocks_per_stake_snapshot: Int32, blocks_per_voting_period: Int32, hard_gas_limit_per_operation: Z, hard_gas_limit_per_block: Z, proof_of_work_threshold: Int64, tokens_per_roll: N, seed_nonce_revelation_tip: N, origination_size: Int31, baking_reward_fixed_portion: N, baking_reward_bonus_per_slot: N, endorsing_reward_per_slot: N, cost_per_byte: N, hard_storage_limit_per_operation: Z, quorum_min: Int32, quorum_max: Int32, min_proposal_quorum: Int32, liquidity_baking_subsidy: N, liquidity_baking_sunset_level: Int32, liquidity_baking_escape_ema_threshold: Int32, max_operations_time_to_live: Int16, minimal_block_delay: Int64, delay_increment_per_round: Int64, consensus_committee_size: Int31, consensus_threshold: Int31, minimal_participation_ratio: CGRIDClass__Proto012_PsithacaConstantsParametric_minimal_participation_ratio, max_slashing_period: Int31, frozen_deposits_percentage: Int31, double_baking_punishment: N, ratio_of_frozen_deposits_slashed_per_double_endorsement: CGRIDClass__Proto012_PsithacaConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement, delegate_selection: CGRIDClass__Proto012_PsithacaConstantsParametric_delegate_selection };
export class CGRIDClass__Proto012PsithacaConstantsParametric extends Box<Proto012PsithacaConstantsParametric> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'blocks_per_stake_snapshot', 'blocks_per_voting_period', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'tokens_per_roll', 'seed_nonce_revelation_tip', 'origination_size', 'baking_reward_fixed_portion', 'baking_reward_bonus_per_slot', 'endorsing_reward_per_slot', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'liquidity_baking_subsidy', 'liquidity_baking_sunset_level', 'liquidity_baking_escape_ema_threshold', 'max_operations_time_to_live', 'minimal_block_delay', 'delay_increment_per_round', 'consensus_committee_size', 'consensus_threshold', 'minimal_participation_ratio', 'max_slashing_period', 'frozen_deposits_percentage', 'double_baking_punishment', 'ratio_of_frozen_deposits_slashed_per_double_endorsement', 'delegate_selection']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaConstantsParametric {
        return new this(record_decoder<Proto012PsithacaConstantsParametric>({preserved_cycles: Uint8.decode, blocks_per_cycle: Int32.decode, blocks_per_commitment: Int32.decode, blocks_per_stake_snapshot: Int32.decode, blocks_per_voting_period: Int32.decode, hard_gas_limit_per_operation: Z.decode, hard_gas_limit_per_block: Z.decode, proof_of_work_threshold: Int64.decode, tokens_per_roll: N.decode, seed_nonce_revelation_tip: N.decode, origination_size: Int31.decode, baking_reward_fixed_portion: N.decode, baking_reward_bonus_per_slot: N.decode, endorsing_reward_per_slot: N.decode, cost_per_byte: N.decode, hard_storage_limit_per_operation: Z.decode, quorum_min: Int32.decode, quorum_max: Int32.decode, min_proposal_quorum: Int32.decode, liquidity_baking_subsidy: N.decode, liquidity_baking_sunset_level: Int32.decode, liquidity_baking_escape_ema_threshold: Int32.decode, max_operations_time_to_live: Int16.decode, minimal_block_delay: Int64.decode, delay_increment_per_round: Int64.decode, consensus_committee_size: Int31.decode, consensus_threshold: Int31.decode, minimal_participation_ratio: CGRIDClass__Proto012_PsithacaConstantsParametric_minimal_participation_ratio.decode, max_slashing_period: Int31.decode, frozen_deposits_percentage: Int31.decode, double_baking_punishment: N.decode, ratio_of_frozen_deposits_slashed_per_double_endorsement: CGRIDClass__Proto012_PsithacaConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement.decode, delegate_selection: CGRIDClass__Proto012_PsithacaConstantsParametric_delegate_selection.decode}, {order: ['preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'blocks_per_stake_snapshot', 'blocks_per_voting_period', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'tokens_per_roll', 'seed_nonce_revelation_tip', 'origination_size', 'baking_reward_fixed_portion', 'baking_reward_bonus_per_slot', 'endorsing_reward_per_slot', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'liquidity_baking_subsidy', 'liquidity_baking_sunset_level', 'liquidity_baking_escape_ema_threshold', 'max_operations_time_to_live', 'minimal_block_delay', 'delay_increment_per_round', 'consensus_committee_size', 'consensus_threshold', 'minimal_participation_ratio', 'max_slashing_period', 'frozen_deposits_percentage', 'double_baking_punishment', 'ratio_of_frozen_deposits_slashed_per_double_endorsement', 'delegate_selection']})(p));
    };
    get encodeLength(): number {
        return (this.value.preserved_cycles.encodeLength +  this.value.blocks_per_cycle.encodeLength +  this.value.blocks_per_commitment.encodeLength +  this.value.blocks_per_stake_snapshot.encodeLength +  this.value.blocks_per_voting_period.encodeLength +  this.value.hard_gas_limit_per_operation.encodeLength +  this.value.hard_gas_limit_per_block.encodeLength +  this.value.proof_of_work_threshold.encodeLength +  this.value.tokens_per_roll.encodeLength +  this.value.seed_nonce_revelation_tip.encodeLength +  this.value.origination_size.encodeLength +  this.value.baking_reward_fixed_portion.encodeLength +  this.value.baking_reward_bonus_per_slot.encodeLength +  this.value.endorsing_reward_per_slot.encodeLength +  this.value.cost_per_byte.encodeLength +  this.value.hard_storage_limit_per_operation.encodeLength +  this.value.quorum_min.encodeLength +  this.value.quorum_max.encodeLength +  this.value.min_proposal_quorum.encodeLength +  this.value.liquidity_baking_subsidy.encodeLength +  this.value.liquidity_baking_sunset_level.encodeLength +  this.value.liquidity_baking_escape_ema_threshold.encodeLength +  this.value.max_operations_time_to_live.encodeLength +  this.value.minimal_block_delay.encodeLength +  this.value.delay_increment_per_round.encodeLength +  this.value.consensus_committee_size.encodeLength +  this.value.consensus_threshold.encodeLength +  this.value.minimal_participation_ratio.encodeLength +  this.value.max_slashing_period.encodeLength +  this.value.frozen_deposits_percentage.encodeLength +  this.value.double_baking_punishment.encodeLength +  this.value.ratio_of_frozen_deposits_slashed_per_double_endorsement.encodeLength +  this.value.delegate_selection.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.preserved_cycles.writeTarget(tgt) +  this.value.blocks_per_cycle.writeTarget(tgt) +  this.value.blocks_per_commitment.writeTarget(tgt) +  this.value.blocks_per_stake_snapshot.writeTarget(tgt) +  this.value.blocks_per_voting_period.writeTarget(tgt) +  this.value.hard_gas_limit_per_operation.writeTarget(tgt) +  this.value.hard_gas_limit_per_block.writeTarget(tgt) +  this.value.proof_of_work_threshold.writeTarget(tgt) +  this.value.tokens_per_roll.writeTarget(tgt) +  this.value.seed_nonce_revelation_tip.writeTarget(tgt) +  this.value.origination_size.writeTarget(tgt) +  this.value.baking_reward_fixed_portion.writeTarget(tgt) +  this.value.baking_reward_bonus_per_slot.writeTarget(tgt) +  this.value.endorsing_reward_per_slot.writeTarget(tgt) +  this.value.cost_per_byte.writeTarget(tgt) +  this.value.hard_storage_limit_per_operation.writeTarget(tgt) +  this.value.quorum_min.writeTarget(tgt) +  this.value.quorum_max.writeTarget(tgt) +  this.value.min_proposal_quorum.writeTarget(tgt) +  this.value.liquidity_baking_subsidy.writeTarget(tgt) +  this.value.liquidity_baking_sunset_level.writeTarget(tgt) +  this.value.liquidity_baking_escape_ema_threshold.writeTarget(tgt) +  this.value.max_operations_time_to_live.writeTarget(tgt) +  this.value.minimal_block_delay.writeTarget(tgt) +  this.value.delay_increment_per_round.writeTarget(tgt) +  this.value.consensus_committee_size.writeTarget(tgt) +  this.value.consensus_threshold.writeTarget(tgt) +  this.value.minimal_participation_ratio.writeTarget(tgt) +  this.value.max_slashing_period.writeTarget(tgt) +  this.value.frozen_deposits_percentage.writeTarget(tgt) +  this.value.double_baking_punishment.writeTarget(tgt) +  this.value.ratio_of_frozen_deposits_slashed_per_double_endorsement.writeTarget(tgt) +  this.value.delegate_selection.writeTarget(tgt));
    }
}
export const proto012_psithaca_constants_parametric_encoder = (value: Proto012PsithacaConstantsParametric): OutputBytes => {
    return record_encoder({order: ['preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'blocks_per_stake_snapshot', 'blocks_per_voting_period', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'tokens_per_roll', 'seed_nonce_revelation_tip', 'origination_size', 'baking_reward_fixed_portion', 'baking_reward_bonus_per_slot', 'endorsing_reward_per_slot', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'liquidity_baking_subsidy', 'liquidity_baking_sunset_level', 'liquidity_baking_escape_ema_threshold', 'max_operations_time_to_live', 'minimal_block_delay', 'delay_increment_per_round', 'consensus_committee_size', 'consensus_threshold', 'minimal_participation_ratio', 'max_slashing_period', 'frozen_deposits_percentage', 'double_baking_punishment', 'ratio_of_frozen_deposits_slashed_per_double_endorsement', 'delegate_selection']})(value);
}
export const proto012_psithaca_constants_parametric_decoder = (p: Parser): Proto012PsithacaConstantsParametric => {
    return record_decoder<Proto012PsithacaConstantsParametric>({preserved_cycles: Uint8.decode, blocks_per_cycle: Int32.decode, blocks_per_commitment: Int32.decode, blocks_per_stake_snapshot: Int32.decode, blocks_per_voting_period: Int32.decode, hard_gas_limit_per_operation: Z.decode, hard_gas_limit_per_block: Z.decode, proof_of_work_threshold: Int64.decode, tokens_per_roll: N.decode, seed_nonce_revelation_tip: N.decode, origination_size: Int31.decode, baking_reward_fixed_portion: N.decode, baking_reward_bonus_per_slot: N.decode, endorsing_reward_per_slot: N.decode, cost_per_byte: N.decode, hard_storage_limit_per_operation: Z.decode, quorum_min: Int32.decode, quorum_max: Int32.decode, min_proposal_quorum: Int32.decode, liquidity_baking_subsidy: N.decode, liquidity_baking_sunset_level: Int32.decode, liquidity_baking_escape_ema_threshold: Int32.decode, max_operations_time_to_live: Int16.decode, minimal_block_delay: Int64.decode, delay_increment_per_round: Int64.decode, consensus_committee_size: Int31.decode, consensus_threshold: Int31.decode, minimal_participation_ratio: CGRIDClass__Proto012_PsithacaConstantsParametric_minimal_participation_ratio.decode, max_slashing_period: Int31.decode, frozen_deposits_percentage: Int31.decode, double_baking_punishment: N.decode, ratio_of_frozen_deposits_slashed_per_double_endorsement: CGRIDClass__Proto012_PsithacaConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement.decode, delegate_selection: CGRIDClass__Proto012_PsithacaConstantsParametric_delegate_selection.decode}, {order: ['preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'blocks_per_stake_snapshot', 'blocks_per_voting_period', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'tokens_per_roll', 'seed_nonce_revelation_tip', 'origination_size', 'baking_reward_fixed_portion', 'baking_reward_bonus_per_slot', 'endorsing_reward_per_slot', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'liquidity_baking_subsidy', 'liquidity_baking_sunset_level', 'liquidity_baking_escape_ema_threshold', 'max_operations_time_to_live', 'minimal_block_delay', 'delay_increment_per_round', 'consensus_committee_size', 'consensus_threshold', 'minimal_participation_ratio', 'max_slashing_period', 'frozen_deposits_percentage', 'double_baking_punishment', 'ratio_of_frozen_deposits_slashed_per_double_endorsement', 'delegate_selection']})(p);
}
