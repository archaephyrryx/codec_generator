import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto012PsithacaCycle = Int32;
export class CGRIDClass__Proto012PsithacaCycle extends Box<Proto012PsithacaCycle> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaCycle {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto012_psithaca_cycle_encoder = (value: Proto012PsithacaCycle): OutputBytes => {
    return value.encode();
}
export const proto012_psithaca_cycle_decoder = (p: Parser): Proto012PsithacaCycle => {
    return Int32.decode(p);
}
