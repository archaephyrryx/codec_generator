import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto012PsithacaVoteBallot = Int8;
export class CGRIDClass__Proto012PsithacaVoteBallot extends Box<Proto012PsithacaVoteBallot> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaVoteBallot {
        return new this(Int8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto012_psithaca_vote_ballot_encoder = (value: Proto012PsithacaVoteBallot): OutputBytes => {
    return value.encode();
}
export const proto012_psithaca_vote_ballot_decoder = (p: Parser): Proto012PsithacaVoteBallot => {
    return Int8.decode(p);
}
