import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto012PsithacaGas__Unaccounted generated for Proto012PsithacaGas__Unaccounted
export class CGRIDClass__Proto012PsithacaGas__Unaccounted extends Box<Proto012PsithacaGas__Unaccounted> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaGas__Unaccounted {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaGas__Limited generated for Proto012PsithacaGas__Limited
export class CGRIDClass__Proto012PsithacaGas__Limited extends Box<Proto012PsithacaGas__Limited> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaGas__Limited {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto012PsithacaGas__Unaccounted = Unit;
export type Proto012PsithacaGas__Limited = Z;
export enum CGRIDTag__Proto012PsithacaGas{
    Limited = 0,
    Unaccounted = 1
}
export interface CGRIDMap__Proto012PsithacaGas {
    Limited: CGRIDClass__Proto012PsithacaGas__Limited,
    Unaccounted: CGRIDClass__Proto012PsithacaGas__Unaccounted
}
export type Proto012PsithacaGas = { kind: CGRIDTag__Proto012PsithacaGas.Limited, value: CGRIDMap__Proto012PsithacaGas['Limited'] } | { kind: CGRIDTag__Proto012PsithacaGas.Unaccounted, value: CGRIDMap__Proto012PsithacaGas['Unaccounted'] };
export function proto012psithacagas_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaGas,Proto012PsithacaGas> {
    function f(disc: CGRIDTag__Proto012PsithacaGas) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaGas.Limited: return CGRIDClass__Proto012PsithacaGas__Limited.decode;
            case CGRIDTag__Proto012PsithacaGas.Unaccounted: return CGRIDClass__Proto012PsithacaGas__Unaccounted.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaGas => Object.values(CGRIDTag__Proto012PsithacaGas).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012PsithacaGas extends Box<Proto012PsithacaGas> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaGas>, Proto012PsithacaGas>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaGas {
        return new this(variant_decoder(width.Uint8)(proto012psithacagas_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto012_psithaca_gas_encoder = (value: Proto012PsithacaGas): OutputBytes => {
    return variant_encoder<KindOf<Proto012PsithacaGas>, Proto012PsithacaGas>(width.Uint8)(value);
}
export const proto012_psithaca_gas_decoder = (p: Parser): Proto012PsithacaGas => {
    return variant_decoder(width.Uint8)(proto012psithacagas_mkDecoder())(p);
}
