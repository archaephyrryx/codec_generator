import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto012PsithacaFitnessLockedRound__Some generated for Proto012PsithacaFitnessLockedRound__Some
export class CGRIDClass__Proto012PsithacaFitnessLockedRound__Some extends Box<Proto012PsithacaFitnessLockedRound__Some> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaFitnessLockedRound__Some {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaFitnessLockedRound__None generated for Proto012PsithacaFitnessLockedRound__None
export class CGRIDClass__Proto012PsithacaFitnessLockedRound__None extends Box<Proto012PsithacaFitnessLockedRound__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaFitnessLockedRound__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto012PsithacaFitnessLockedRound__Some = Int32;
export type Proto012PsithacaFitnessLockedRound__None = Unit;
// Class CGRIDClass__Proto012_PsithacaFitness_locked_round generated for Proto012PsithacaFitnessLockedRound
export function proto012psithacafitnesslockedround_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaFitnessLockedRound,Proto012PsithacaFitnessLockedRound> {
    function f(disc: CGRIDTag__Proto012PsithacaFitnessLockedRound) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaFitnessLockedRound.None: return CGRIDClass__Proto012PsithacaFitnessLockedRound__None.decode;
            case CGRIDTag__Proto012PsithacaFitnessLockedRound.Some: return CGRIDClass__Proto012PsithacaFitnessLockedRound__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaFitnessLockedRound => Object.values(CGRIDTag__Proto012PsithacaFitnessLockedRound).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaFitness_locked_round extends Box<Proto012PsithacaFitnessLockedRound> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaFitnessLockedRound>, Proto012PsithacaFitnessLockedRound>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaFitness_locked_round {
        return new this(variant_decoder(width.Uint8)(proto012psithacafitnesslockedround_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__Proto012PsithacaFitnessLockedRound{
    None = 0,
    Some = 1
}
export interface CGRIDMap__Proto012PsithacaFitnessLockedRound {
    None: CGRIDClass__Proto012PsithacaFitnessLockedRound__None,
    Some: CGRIDClass__Proto012PsithacaFitnessLockedRound__Some
}
export type Proto012PsithacaFitnessLockedRound = { kind: CGRIDTag__Proto012PsithacaFitnessLockedRound.None, value: CGRIDMap__Proto012PsithacaFitnessLockedRound['None'] } | { kind: CGRIDTag__Proto012PsithacaFitnessLockedRound.Some, value: CGRIDMap__Proto012PsithacaFitnessLockedRound['Some'] };
export type Proto012PsithacaFitness = { level: Int32, locked_round: CGRIDClass__Proto012_PsithacaFitness_locked_round, predecessor_round: Int32, round: Int32 };
export class CGRIDClass__Proto012PsithacaFitness extends Box<Proto012PsithacaFitness> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaFitness {
        return new this(record_decoder<Proto012PsithacaFitness>({level: Int32.decode, locked_round: CGRIDClass__Proto012_PsithacaFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.locked_round.encodeLength +  this.value.predecessor_round.encodeLength +  this.value.round.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.locked_round.writeTarget(tgt) +  this.value.predecessor_round.writeTarget(tgt) +  this.value.round.writeTarget(tgt));
    }
}
export const proto012_psithaca_fitness_encoder = (value: Proto012PsithacaFitness): OutputBytes => {
    return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(value);
}
export const proto012_psithaca_fitness_decoder = (p: Parser): Proto012PsithacaFitness => {
    return record_decoder<Proto012PsithacaFitness>({level: Int32.decode, locked_round: CGRIDClass__Proto012_PsithacaFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p);
}
