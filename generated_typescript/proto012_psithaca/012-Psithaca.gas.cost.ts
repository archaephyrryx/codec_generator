import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
export type Proto012PsithacaGasCost = Z;
export class CGRIDClass__Proto012PsithacaGasCost extends Box<Proto012PsithacaGasCost> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaGasCost {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto012_psithaca_gas_cost_encoder = (value: Proto012PsithacaGasCost): OutputBytes => {
    return value.encode();
}
export const proto012_psithaca_gas_cost_decoder = (p: Parser): Proto012PsithacaGasCost => {
    return Z.decode(p);
}
