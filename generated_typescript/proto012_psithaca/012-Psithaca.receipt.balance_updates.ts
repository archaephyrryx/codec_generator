import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Subsidy generated for Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Subsidy
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Subsidy extends Box<Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Subsidy> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Subsidy {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Simulation generated for Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Simulation
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Simulation extends Box<Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Simulation> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Simulation {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration generated for Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration extends Box<Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Block_application generated for Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Block_application
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Block_application extends Box<Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Block_application> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Block_application {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Storage_fees generated for Proto012PsithacaOperationMetadataAlphaBalance__Storage_fees
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Storage_fees extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Storage_fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Storage_fees {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Storage_fees>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Nonce_revelation_rewards generated for Proto012PsithacaOperationMetadataAlphaBalance__Nonce_revelation_rewards
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Nonce_revelation_rewards extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Nonce_revelation_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Nonce_revelation_rewards {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Nonce_revelation_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Minted generated for Proto012PsithacaOperationMetadataAlphaBalance__Minted
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Minted extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Minted> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Minted {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Minted>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Lost_endorsing_rewards generated for Proto012PsithacaOperationMetadataAlphaBalance__Lost_endorsing_rewards
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Lost_endorsing_rewards extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Lost_endorsing_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'participation', 'revelation', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Lost_endorsing_rewards {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Lost_endorsing_rewards>({category: Unit.decode, delegate: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate.decode, participation: Bool.decode, revelation: Bool.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'participation', 'revelation', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.participation.encodeLength +  this.value.revelation.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.participation.writeTarget(tgt) +  this.value.revelation.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Liquidity_baking_subsidies generated for Proto012PsithacaOperationMetadataAlphaBalance__Liquidity_baking_subsidies
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Liquidity_baking_subsidies extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Liquidity_baking_subsidies> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Liquidity_baking_subsidies {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Liquidity_baking_subsidies>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_rewards generated for Proto012PsithacaOperationMetadataAlphaBalance__Legacy_rewards
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_rewards extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Legacy_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_rewards {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Legacy_rewards>({category: Unit.decode, delegate: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_rewards_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_fees generated for Proto012PsithacaOperationMetadataAlphaBalance__Legacy_fees
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_fees extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Legacy_fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_fees {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Legacy_fees>({category: Unit.decode, delegate: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_fees_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_deposits generated for Proto012PsithacaOperationMetadataAlphaBalance__Legacy_deposits
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_deposits extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Legacy_deposits> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_deposits {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Legacy_deposits>({category: Unit.decode, delegate: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_deposits_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Invoice generated for Proto012PsithacaOperationMetadataAlphaBalance__Invoice
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Invoice extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Invoice> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Invoice {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Invoice>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Initial_commitments generated for Proto012PsithacaOperationMetadataAlphaBalance__Initial_commitments
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Initial_commitments extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Initial_commitments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Initial_commitments {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Initial_commitments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Endorsing_rewards generated for Proto012PsithacaOperationMetadataAlphaBalance__Endorsing_rewards
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Endorsing_rewards extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Endorsing_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Endorsing_rewards {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Endorsing_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_punishments generated for Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_punishments
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_punishments extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_punishments {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_evidence_rewards generated for Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_evidence_rewards
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_evidence_rewards extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_evidence_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_evidence_rewards {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_evidence_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Deposits generated for Proto012PsithacaOperationMetadataAlphaBalance__Deposits
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Deposits extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Deposits> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Deposits {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Deposits>({category: Unit.decode, delegate: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Deposits_delegate.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Contract generated for Proto012PsithacaOperationMetadataAlphaBalance__Contract
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Contract extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Contract> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Contract {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Contract>({contract: CGRIDClass__Proto012_PsithacaContract_id.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['contract', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Commitments generated for Proto012PsithacaOperationMetadataAlphaBalance__Commitments
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Commitments extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Commitments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'committer', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Commitments {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Commitments>({category: Unit.decode, committer: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Commitments_committer.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'committer', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.committer.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.committer.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Burned generated for Proto012PsithacaOperationMetadataAlphaBalance__Burned
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Burned extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Burned> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Burned {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Burned>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Bootstrap generated for Proto012PsithacaOperationMetadataAlphaBalance__Bootstrap
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Bootstrap extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Bootstrap> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Bootstrap {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Bootstrap>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Block_fees generated for Proto012PsithacaOperationMetadataAlphaBalance__Block_fees
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Block_fees extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Block_fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Block_fees {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Block_fees>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Baking_rewards generated for Proto012PsithacaOperationMetadataAlphaBalance__Baking_rewards
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Baking_rewards extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Baking_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Baking_rewards {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Baking_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Baking_bonuses generated for Proto012PsithacaOperationMetadataAlphaBalance__Baking_bonuses
export class CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Baking_bonuses extends Box<Proto012PsithacaOperationMetadataAlphaBalance__Baking_bonuses> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Baking_bonuses {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalance__Baking_bonuses>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012PsithacaContractId__Originated generated for Proto012PsithacaContractId__Originated
export class CGRIDClass__Proto012PsithacaContractId__Originated extends Box<Proto012PsithacaContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto012PsithacaContractId__Implicit generated for Proto012PsithacaContractId__Implicit
export class CGRIDClass__Proto012PsithacaContractId__Implicit extends Box<Proto012PsithacaContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaContractId__Implicit {
        return new this(record_decoder<Proto012PsithacaContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Subsidy = Unit;
export type Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Simulation = Unit;
export type Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration = Unit;
export type Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Block_application = Unit;
export type Proto012PsithacaOperationMetadataAlphaBalance__Storage_fees = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Nonce_revelation_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Minted = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Lost_endorsing_rewards = { category: Unit, delegate: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate, participation: Bool, revelation: Bool, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Liquidity_baking_subsidies = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Legacy_rewards = { category: Unit, delegate: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_rewards_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Legacy_fees = { category: Unit, delegate: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_fees_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Legacy_deposits = { category: Unit, delegate: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_deposits_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Invoice = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Initial_commitments = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Endorsing_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_punishments = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_evidence_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Deposits = { category: Unit, delegate: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Deposits_delegate, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Contract = { contract: CGRIDClass__Proto012_PsithacaContract_id, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Commitments = { category: Unit, committer: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Commitments_committer, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Burned = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Bootstrap = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Block_fees = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Baking_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaOperationMetadataAlphaBalance__Baking_bonuses = { category: Unit, change: Int64, origin: CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin };
export type Proto012PsithacaContractId__Originated = Padded<CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad,1>;
export type Proto012PsithacaContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin generated for Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin
export function proto012psithacaoperationmetadataalphaupdateoriginorigin_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin,Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin> {
    function f(disc: CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin.Block_application: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Block_application.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin.Subsidy: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Subsidy.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin.Simulation: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Simulation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin => Object.values(CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin extends Box<Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin>, Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperation_metadataAlphaUpdate_origin_origin {
        return new this(variant_decoder(width.Uint8)(proto012psithacaoperationmetadataalphaupdateoriginorigin_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate generated for Proto012PsithacaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate
export class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate extends Box<Proto012PsithacaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_rewards_delegate generated for Proto012PsithacaOperationMetadataAlphaBalanceLegacyRewardsDelegate
export class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_rewards_delegate extends Box<Proto012PsithacaOperationMetadataAlphaBalanceLegacyRewardsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_rewards_delegate {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalanceLegacyRewardsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_fees_delegate generated for Proto012PsithacaOperationMetadataAlphaBalanceLegacyFeesDelegate
export class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_fees_delegate extends Box<Proto012PsithacaOperationMetadataAlphaBalanceLegacyFeesDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_fees_delegate {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalanceLegacyFeesDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_deposits_delegate generated for Proto012PsithacaOperationMetadataAlphaBalanceLegacyDepositsDelegate
export class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_deposits_delegate extends Box<Proto012PsithacaOperationMetadataAlphaBalanceLegacyDepositsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Legacy_deposits_delegate {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalanceLegacyDepositsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Deposits_delegate generated for Proto012PsithacaOperationMetadataAlphaBalanceDepositsDelegate
export class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Deposits_delegate extends Box<Proto012PsithacaOperationMetadataAlphaBalanceDepositsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Deposits_delegate {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalanceDepositsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Commitments_committer generated for Proto012PsithacaOperationMetadataAlphaBalanceCommitmentsCommitter
export class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Commitments_committer extends Box<Proto012PsithacaOperationMetadataAlphaBalanceCommitmentsCommitter> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['blinded_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance_Commitments_committer {
        return new this(record_decoder<Proto012PsithacaOperationMetadataAlphaBalanceCommitmentsCommitter>({blinded_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['blinded_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.blinded_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.blinded_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance generated for Proto012PsithacaOperationMetadataAlphaBalance
export function proto012psithacaoperationmetadataalphabalance_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance,Proto012PsithacaOperationMetadataAlphaBalance> {
    function f(disc: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Contract: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Contract.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Legacy_rewards: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_rewards.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Block_fees: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Block_fees.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Legacy_deposits: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_deposits.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Deposits: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Deposits.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Nonce_revelation_rewards: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Nonce_revelation_rewards.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Double_signing_evidence_rewards: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_evidence_rewards.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Endorsing_rewards: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Endorsing_rewards.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Baking_rewards: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Baking_rewards.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Baking_bonuses: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Baking_bonuses.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Legacy_fees: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_fees.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Storage_fees: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Storage_fees.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Double_signing_punishments: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_punishments.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Lost_endorsing_rewards: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Lost_endorsing_rewards.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Liquidity_baking_subsidies: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Liquidity_baking_subsidies.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Burned: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Burned.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Commitments: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Commitments.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Bootstrap: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Bootstrap.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Invoice: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Invoice.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Initial_commitments: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Initial_commitments.decode;
            case CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Minted: return CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Minted.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance => Object.values(CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance extends Box<Proto012PsithacaOperationMetadataAlphaBalance> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaOperationMetadataAlphaBalance>, Proto012PsithacaOperationMetadataAlphaBalance>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance {
        return new this(variant_decoder(width.Uint8)(proto012psithacaoperationmetadataalphabalance_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad generated for Proto012PsithacaContractIdOriginatedDenestPad
export class CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad extends Box<Proto012PsithacaContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto012PsithacaContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto012_PsithacaContract_id generated for Proto012PsithacaContractId
export function proto012psithacacontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto012PsithacaContractId,Proto012PsithacaContractId> {
    function f(disc: CGRIDTag__Proto012PsithacaContractId) {
        switch (disc) {
            case CGRIDTag__Proto012PsithacaContractId.Implicit: return CGRIDClass__Proto012PsithacaContractId__Implicit.decode;
            case CGRIDTag__Proto012PsithacaContractId.Originated: return CGRIDClass__Proto012PsithacaContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto012PsithacaContractId => Object.values(CGRIDTag__Proto012PsithacaContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto012_PsithacaContract_id extends Box<Proto012PsithacaContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto012PsithacaContractId>, Proto012PsithacaContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto012_PsithacaContract_id {
        return new this(variant_decoder(width.Uint8)(proto012psithacacontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin{
    Block_application = 0,
    Protocol_migration = 1,
    Subsidy = 2,
    Simulation = 3
}
export interface CGRIDMap__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin {
    Block_application: CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Block_application,
    Protocol_migration: CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration,
    Subsidy: CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Subsidy,
    Simulation: CGRIDClass__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin__Simulation
}
export type Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin = { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin.Block_application, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin['Block_application'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin['Protocol_migration'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin.Subsidy, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin['Subsidy'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin.Simulation, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaUpdateOriginOrigin['Simulation'] };
export type Proto012PsithacaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationMetadataAlphaBalanceLegacyRewardsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationMetadataAlphaBalanceLegacyFeesDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationMetadataAlphaBalanceLegacyDepositsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationMetadataAlphaBalanceDepositsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto012PsithacaOperationMetadataAlphaBalanceCommitmentsCommitter = { blinded_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance{
    Contract = 0,
    Legacy_rewards = 1,
    Block_fees = 2,
    Legacy_deposits = 3,
    Deposits = 4,
    Nonce_revelation_rewards = 5,
    Double_signing_evidence_rewards = 6,
    Endorsing_rewards = 7,
    Baking_rewards = 8,
    Baking_bonuses = 9,
    Legacy_fees = 10,
    Storage_fees = 11,
    Double_signing_punishments = 12,
    Lost_endorsing_rewards = 13,
    Liquidity_baking_subsidies = 14,
    Burned = 15,
    Commitments = 16,
    Bootstrap = 17,
    Invoice = 18,
    Initial_commitments = 19,
    Minted = 20
}
export interface CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance {
    Contract: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Contract,
    Legacy_rewards: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_rewards,
    Block_fees: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Block_fees,
    Legacy_deposits: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_deposits,
    Deposits: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Deposits,
    Nonce_revelation_rewards: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Nonce_revelation_rewards,
    Double_signing_evidence_rewards: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_evidence_rewards,
    Endorsing_rewards: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Endorsing_rewards,
    Baking_rewards: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Baking_rewards,
    Baking_bonuses: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Baking_bonuses,
    Legacy_fees: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Legacy_fees,
    Storage_fees: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Storage_fees,
    Double_signing_punishments: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Double_signing_punishments,
    Lost_endorsing_rewards: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Lost_endorsing_rewards,
    Liquidity_baking_subsidies: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Liquidity_baking_subsidies,
    Burned: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Burned,
    Commitments: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Commitments,
    Bootstrap: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Bootstrap,
    Invoice: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Invoice,
    Initial_commitments: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Initial_commitments,
    Minted: CGRIDClass__Proto012PsithacaOperationMetadataAlphaBalance__Minted
}
export type Proto012PsithacaOperationMetadataAlphaBalance = { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Contract, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Contract'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Legacy_rewards, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Legacy_rewards'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Block_fees, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Block_fees'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Legacy_deposits, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Legacy_deposits'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Deposits, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Deposits'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Nonce_revelation_rewards, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Nonce_revelation_rewards'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Double_signing_evidence_rewards, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Double_signing_evidence_rewards'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Endorsing_rewards, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Endorsing_rewards'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Baking_rewards, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Baking_rewards'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Baking_bonuses, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Baking_bonuses'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Legacy_fees, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Legacy_fees'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Storage_fees, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Storage_fees'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Double_signing_punishments, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Double_signing_punishments'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Lost_endorsing_rewards, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Lost_endorsing_rewards'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Liquidity_baking_subsidies, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Liquidity_baking_subsidies'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Burned, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Burned'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Commitments, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Commitments'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Bootstrap, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Bootstrap'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Invoice, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Invoice'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Initial_commitments, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Initial_commitments'] } | { kind: CGRIDTag__Proto012PsithacaOperationMetadataAlphaBalance.Minted, value: CGRIDMap__Proto012PsithacaOperationMetadataAlphaBalance['Minted'] };
export type Proto012PsithacaContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto012PsithacaContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto012PsithacaContractId {
    Implicit: CGRIDClass__Proto012PsithacaContractId__Implicit,
    Originated: CGRIDClass__Proto012PsithacaContractId__Originated
}
export type Proto012PsithacaContractId = { kind: CGRIDTag__Proto012PsithacaContractId.Implicit, value: CGRIDMap__Proto012PsithacaContractId['Implicit'] } | { kind: CGRIDTag__Proto012PsithacaContractId.Originated, value: CGRIDMap__Proto012PsithacaContractId['Originated'] };
export type Proto012PsithacaReceiptBalanceUpdates = Dynamic<Sequence<CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance>,width.Uint30>;
export class CGRIDClass__Proto012PsithacaReceiptBalanceUpdates extends Box<Proto012PsithacaReceiptBalanceUpdates> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto012PsithacaReceiptBalanceUpdates {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto012_psithaca_receipt_balance_updates_encoder = (value: Proto012PsithacaReceiptBalanceUpdates): OutputBytes => {
    return value.encode();
}
export const proto012_psithaca_receipt_balance_updates_decoder = (p: Parser): Proto012PsithacaReceiptBalanceUpdates => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto012_PsithacaOperation_metadataAlphaBalance.decode), width.Uint30)(p);
}
