import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto006PsCARTHAVotingPeriod = Int32;
export class CGRIDClass__Proto006PsCARTHAVotingPeriod extends Box<Proto006PsCARTHAVotingPeriod> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAVotingPeriod {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto006_pscartha_voting_period_encoder = (value: Proto006PsCARTHAVotingPeriod): OutputBytes => {
    return value.encode();
}
export const proto006_pscartha_voting_period_decoder = (p: Parser): Proto006PsCARTHAVotingPeriod => {
    return Int32.decode(p);
}
