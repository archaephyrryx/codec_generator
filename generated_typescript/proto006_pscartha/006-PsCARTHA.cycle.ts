import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto006PsCARTHACycle = Int32;
export class CGRIDClass__Proto006PsCARTHACycle extends Box<Proto006PsCARTHACycle> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHACycle {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto006_pscartha_cycle_encoder = (value: Proto006PsCARTHACycle): OutputBytes => {
    return value.encode();
}
export const proto006_pscartha_cycle_decoder = (p: Parser): Proto006PsCARTHACycle => {
    return Int32.decode(p);
}
