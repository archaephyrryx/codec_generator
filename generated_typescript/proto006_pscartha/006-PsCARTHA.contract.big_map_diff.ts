import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__update generated for Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__update
export class CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__update extends Box<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'big_map', 'key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__update {
        return new this(record_decoder<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__update>({action: Unit.decode, big_map: Z.decode, key_hash: CGRIDClass__Proto006_PsCARTHAContractBig_map_diff_denest_dyn_denest_seq_update_key_hash.decode, key: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode)}, {order: ['action', 'big_map', 'key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.big_map.encodeLength +  this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.big_map.writeTarget(tgt) +  this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__remove generated for Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__remove
export class CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__remove extends Box<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__remove> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'big_map']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__remove {
        return new this(record_decoder<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__remove>({action: Unit.decode, big_map: Z.decode}, {order: ['action', 'big_map']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.big_map.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.big_map.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__copy generated for Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__copy
export class CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__copy extends Box<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__copy> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'source_big_map', 'destination_big_map']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__copy {
        return new this(record_decoder<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__copy>({action: Unit.decode, source_big_map: Z.decode, destination_big_map: Z.decode}, {order: ['action', 'source_big_map', 'destination_big_map']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.source_big_map.encodeLength +  this.value.destination_big_map.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.source_big_map.writeTarget(tgt) +  this.value.destination_big_map.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__alloc generated for Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__alloc
export class CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__alloc extends Box<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__alloc> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'big_map', 'key_type', 'value_type']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__alloc {
        return new this(record_decoder<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__alloc>({action: Unit.decode, big_map: Z.decode, key_type: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode, value_type: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode}, {order: ['action', 'big_map', 'key_type', 'value_type']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.big_map.encodeLength +  this.value.key_type.encodeLength +  this.value.value_type.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.big_map.writeTarget(tgt) +  this.value.key_type.writeTarget(tgt) +  this.value.value_type.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__String generated for MichelineProto006PsCARTHAMichelsonV1Expression__String
export class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__String extends Box<MichelineProto006PsCARTHAMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__String {
        return new this(record_decoder<MichelineProto006PsCARTHAMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Sequence generated for MichelineProto006PsCARTHAMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Sequence extends Box<MichelineProto006PsCARTHAMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__generic generated for MichelineProto006PsCARTHAMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__generic extends Box<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Int generated for MichelineProto006PsCARTHAMichelsonV1Expression__Int
export class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Int extends Box<MichelineProto006PsCARTHAMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineProto006PsCARTHAMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Bytes generated for MichelineProto006PsCARTHAMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Bytes extends Box<MichelineProto006PsCARTHAMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineProto006PsCARTHAMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
export type Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__update = { action: Unit, big_map: Z, key_hash: CGRIDClass__Proto006_PsCARTHAContractBig_map_diff_denest_dyn_denest_seq_update_key_hash, key: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression> };
export type Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__remove = { action: Unit, big_map: Z };
export type Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__copy = { action: Unit, source_big_map: Z, destination_big_map: Z };
export type Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__alloc = { action: Unit, big_map: Z, key_type: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression, value_type: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression };
export type MichelineProto006PsCARTHAMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineProto006PsCARTHAMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression>,width.Uint30>;
export type MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives };
export type MichelineProto006PsCARTHAMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression, arg2: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression, arg2: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression };
export type MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives, arg: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives, arg: CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression };
export type MichelineProto006PsCARTHAMichelsonV1Expression__Int = { int: Z };
export type MichelineProto006PsCARTHAMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
// Class CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives generated for Proto006PsCARTHAMichelsonV1Primitives
export class CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives extends Box<Proto006PsCARTHAMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<Proto006PsCARTHAMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is Proto006PsCARTHAMichelsonV1Primitives => (Object.values(Proto006PsCARTHAMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006_PsCARTHAContractBig_map_diff_denest_dyn_denest_seq_update_key_hash generated for Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeqUpdateKeyHash
export class CGRIDClass__Proto006_PsCARTHAContractBig_map_diff_denest_dyn_denest_seq_update_key_hash extends Box<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeqUpdateKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAContractBig_map_diff_denest_dyn_denest_seq_update_key_hash {
        return new this(record_decoder<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeqUpdateKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAContractBig_map_diff_denest_dyn_denest_seq generated for Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq
export function proto006pscarthacontractbigmapdiffdenestdyndenestseq_mkDecoder(): VariantDecoder<CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq,Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq> {
    function f(disc: CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq) {
        switch (disc) {
            case CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq.update: return CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__update.decode;
            case CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq.remove: return CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__remove.decode;
            case CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq.copy: return CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__copy.decode;
            case CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq.alloc: return CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__alloc.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq => Object.values(CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq).includes(tagval);
    return f;
}
export class CGRIDClass__Proto006_PsCARTHAContractBig_map_diff_denest_dyn_denest_seq extends Box<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq>, Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAContractBig_map_diff_denest_dyn_denest_seq {
        return new this(variant_decoder(width.Uint8)(proto006pscarthacontractbigmapdiffdenestdyndenestseq_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression generated for MichelineProto006PsCARTHAMichelsonV1Expression
export function michelineproto006pscarthamichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression,MichelineProto006PsCARTHAMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Int: return CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.String: return CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Sequence: return CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Bytes: return CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression => Object.values(CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression extends Box<MichelineProto006PsCARTHAMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineProto006PsCARTHAMichelsonV1Expression>, MichelineProto006PsCARTHAMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto006_PsCARTHAMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelineproto006pscarthamichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression {
    Int: CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineProto006PsCARTHAMichelsonV1Expression__Bytes
}
export type MichelineProto006PsCARTHAMichelsonV1Expression = { kind: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Int, value: CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.String, value: CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineProto006PsCARTHAMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineProto006PsCARTHAMichelsonV1Expression['Bytes'] };
export enum Proto006PsCARTHAMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117
}
export type Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeqUpdateKeyHash = { script_expr: FixedBytes<32> };
export enum CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq{
    update = 0,
    remove = 1,
    copy = 2,
    alloc = 3
}
export interface CGRIDMap__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq {
    update: CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__update,
    remove: CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__remove,
    copy: CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__copy,
    alloc: CGRIDClass__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq__alloc
}
export type Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq = { kind: CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq.update, value: CGRIDMap__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq['update'] } | { kind: CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq.remove, value: CGRIDMap__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq['remove'] } | { kind: CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq.copy, value: CGRIDMap__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq['copy'] } | { kind: CGRIDTag__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq.alloc, value: CGRIDMap__Proto006PsCARTHAContractBigMapDiffDenestDynDenestSeq['alloc'] };
export type Proto006PsCARTHAContractBigMapDiff = Dynamic<Sequence<CGRIDClass__Proto006_PsCARTHAContractBig_map_diff_denest_dyn_denest_seq>,width.Uint30>;
export class CGRIDClass__Proto006PsCARTHAContractBigMapDiff extends Box<Proto006PsCARTHAContractBigMapDiff> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAContractBigMapDiff {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto006_PsCARTHAContractBig_map_diff_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto006_pscartha_contract_big_map_diff_encoder = (value: Proto006PsCARTHAContractBigMapDiff): OutputBytes => {
    return value.encode();
}
export const proto006_pscartha_contract_big_map_diff_decoder = (p: Parser): Proto006PsCARTHAContractBigMapDiff => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto006_PsCARTHAContractBig_map_diff_denest_dyn_denest_seq.decode), width.Uint30)(p);
}
