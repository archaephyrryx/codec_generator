import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
export type Proto006PsCARTHAErrors = Dynamic<U8String,width.Uint30>;
export class CGRIDClass__Proto006PsCARTHAErrors extends Box<Proto006PsCARTHAErrors> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAErrors {
        return new this(Dynamic.decode(U8String.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto006_pscartha_errors_encoder = (value: Proto006PsCARTHAErrors): OutputBytes => {
    return value.encode();
}
export const proto006_pscartha_errors_decoder = (p: Parser): Proto006PsCARTHAErrors => {
    return Dynamic.decode(U8String.decode, width.Uint30)(p);
}
