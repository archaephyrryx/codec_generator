import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__Proto006_PsCARTHADelegateFrozen_balance_by_cycles_denest_dyn_denest_seq generated for Proto006PsCARTHADelegateFrozenBalanceByCyclesDenestDynDenestSeq
export class CGRIDClass__Proto006_PsCARTHADelegateFrozen_balance_by_cycles_denest_dyn_denest_seq extends Box<Proto006PsCARTHADelegateFrozenBalanceByCyclesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle', 'deposit', 'fees', 'rewards']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHADelegateFrozen_balance_by_cycles_denest_dyn_denest_seq {
        return new this(record_decoder<Proto006PsCARTHADelegateFrozenBalanceByCyclesDenestDynDenestSeq>({cycle: Int32.decode, deposit: N.decode, fees: N.decode, rewards: N.decode}, {order: ['cycle', 'deposit', 'fees', 'rewards']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle.encodeLength +  this.value.deposit.encodeLength +  this.value.fees.encodeLength +  this.value.rewards.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle.writeTarget(tgt) +  this.value.deposit.writeTarget(tgt) +  this.value.fees.writeTarget(tgt) +  this.value.rewards.writeTarget(tgt));
    }
}
export type Proto006PsCARTHADelegateFrozenBalanceByCyclesDenestDynDenestSeq = { cycle: Int32, deposit: N, fees: N, rewards: N };
export type Proto006PsCARTHADelegateFrozenBalanceByCycles = Dynamic<Sequence<CGRIDClass__Proto006_PsCARTHADelegateFrozen_balance_by_cycles_denest_dyn_denest_seq>,width.Uint30>;
export class CGRIDClass__Proto006PsCARTHADelegateFrozenBalanceByCycles extends Box<Proto006PsCARTHADelegateFrozenBalanceByCycles> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHADelegateFrozenBalanceByCycles {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto006_PsCARTHADelegateFrozen_balance_by_cycles_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto006_pscartha_delegate_frozen_balance_by_cycles_encoder = (value: Proto006PsCARTHADelegateFrozenBalanceByCycles): OutputBytes => {
    return value.encode();
}
export const proto006_pscartha_delegate_frozen_balance_by_cycles_decoder = (p: Parser): Proto006PsCARTHADelegateFrozenBalanceByCycles => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto006_PsCARTHADelegateFrozen_balance_by_cycles_denest_dyn_denest_seq.decode), width.Uint30)(p);
}
