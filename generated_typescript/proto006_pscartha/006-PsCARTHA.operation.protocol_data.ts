import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Nullable } from '../../ts_runtime/composite/opt/nullable';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { VPadded } from '../../ts_runtime/composite/vpadded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int64, Int8, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Transaction generated for Proto006PsCARTHAOperationAlphaContents__Transaction
export class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Transaction extends Box<Proto006PsCARTHAOperationAlphaContents__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Transaction {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContents__Transaction>({source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Transaction_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: N.decode, destination: CGRIDClass__Proto006_PsCARTHAContract_id.decode, parameters: Option.decode(CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Transaction_parameters.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Seed_nonce_revelation generated for Proto006PsCARTHAOperationAlphaContents__Seed_nonce_revelation
export class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Seed_nonce_revelation extends Box<Proto006PsCARTHAOperationAlphaContents__Seed_nonce_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Seed_nonce_revelation {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContents__Seed_nonce_revelation>({level: Int32.decode, nonce: FixedBytes.decode<32>({len: 32})}, {order: ['level', 'nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Reveal generated for Proto006PsCARTHAOperationAlphaContents__Reveal
export class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Reveal extends Box<Proto006PsCARTHAOperationAlphaContents__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Reveal {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContents__Reveal>({source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Reveal_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_key: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Reveal_public_key.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Proposals generated for Proto006PsCARTHAOperationAlphaContents__Proposals
export class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Proposals extends Box<Proto006PsCARTHAOperationAlphaContents__Proposals> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposals']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Proposals {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContents__Proposals>({source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Proposals_source.decode, period: Int32.decode, proposals: Dynamic.decode(Sequence.decode(CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'period', 'proposals']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposals.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposals.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Origination generated for Proto006PsCARTHAOperationAlphaContents__Origination
export class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Origination extends Box<Proto006PsCARTHAOperationAlphaContents__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Origination {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContents__Origination>({source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, balance: N.decode, delegate: Option.decode(CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Origination_delegate.decode), script: CGRIDClass__Proto006_PsCARTHAScriptedContracts.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Endorsement generated for Proto006PsCARTHAOperationAlphaContents__Endorsement
export class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Endorsement extends Box<Proto006PsCARTHAOperationAlphaContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Endorsement {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContents__Endorsement>({level: Int32.decode}, {order: ['level']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Double_endorsement_evidence generated for Proto006PsCARTHAOperationAlphaContents__Double_endorsement_evidence
export class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Double_endorsement_evidence extends Box<Proto006PsCARTHAOperationAlphaContents__Double_endorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Double_endorsement_evidence {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContents__Double_endorsement_evidence>({op1: Dynamic.decode(CGRIDClass__Proto006_PsCARTHAInlinedEndorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__Proto006_PsCARTHAInlinedEndorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Double_baking_evidence generated for Proto006PsCARTHAOperationAlphaContents__Double_baking_evidence
export class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Double_baking_evidence extends Box<Proto006PsCARTHAOperationAlphaContents__Double_baking_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bh1', 'bh2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Double_baking_evidence {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContents__Double_baking_evidence>({bh1: Dynamic.decode(CGRIDClass__Proto006_PsCARTHABlock_headerAlphaFull_header.decode, width.Uint30), bh2: Dynamic.decode(CGRIDClass__Proto006_PsCARTHABlock_headerAlphaFull_header.decode, width.Uint30)}, {order: ['bh1', 'bh2']})(p));
    };
    get encodeLength(): number {
        return (this.value.bh1.encodeLength +  this.value.bh2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bh1.writeTarget(tgt) +  this.value.bh2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Delegation generated for Proto006PsCARTHAOperationAlphaContents__Delegation
export class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Delegation extends Box<Proto006PsCARTHAOperationAlphaContents__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Delegation {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContents__Delegation>({source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Delegation_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, delegate: Option.decode(CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Delegation_delegate.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Ballot generated for Proto006PsCARTHAOperationAlphaContents__Ballot
export class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Ballot extends Box<Proto006PsCARTHAOperationAlphaContents__Ballot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposal', 'ballot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Ballot {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContents__Ballot>({source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Ballot_source.decode, period: Int32.decode, proposal: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Ballot_proposal.decode, ballot: Int8.decode}, {order: ['source', 'period', 'proposal', 'ballot']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposal.encodeLength +  this.value.ballot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposal.writeTarget(tgt) +  this.value.ballot.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Activate_account generated for Proto006PsCARTHAOperationAlphaContents__Activate_account
export class CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Activate_account extends Box<Proto006PsCARTHAOperationAlphaContents__Activate_account> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'secret']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Activate_account {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContents__Activate_account>({pkh: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Activate_account_pkh.decode, secret: FixedBytes.decode<20>({len: 20})}, {order: ['pkh', 'secret']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.secret.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.secret.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAInlinedEndorsementContents__Endorsement generated for Proto006PsCARTHAInlinedEndorsementContents__Endorsement
export class CGRIDClass__Proto006PsCARTHAInlinedEndorsementContents__Endorsement extends Box<Proto006PsCARTHAInlinedEndorsementContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAInlinedEndorsementContents__Endorsement {
        return new this(record_decoder<Proto006PsCARTHAInlinedEndorsementContents__Endorsement>({level: Int32.decode}, {order: ['level']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006PsCARTHAEntrypoint__set_delegate generated for Proto006PsCARTHAEntrypoint__set_delegate
export class CGRIDClass__Proto006PsCARTHAEntrypoint__set_delegate extends Box<Proto006PsCARTHAEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006PsCARTHAEntrypoint__root generated for Proto006PsCARTHAEntrypoint__root
export class CGRIDClass__Proto006PsCARTHAEntrypoint__root extends Box<Proto006PsCARTHAEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006PsCARTHAEntrypoint__remove_delegate generated for Proto006PsCARTHAEntrypoint__remove_delegate
export class CGRIDClass__Proto006PsCARTHAEntrypoint__remove_delegate extends Box<Proto006PsCARTHAEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006PsCARTHAEntrypoint__named generated for Proto006PsCARTHAEntrypoint__named
export class CGRIDClass__Proto006PsCARTHAEntrypoint__named extends Box<Proto006PsCARTHAEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006PsCARTHAEntrypoint___do generated for Proto006PsCARTHAEntrypoint___do
export class CGRIDClass__Proto006PsCARTHAEntrypoint___do extends Box<Proto006PsCARTHAEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006PsCARTHAEntrypoint___default generated for Proto006PsCARTHAEntrypoint___default
export class CGRIDClass__Proto006PsCARTHAEntrypoint___default extends Box<Proto006PsCARTHAEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006PsCARTHAContractId__Originated generated for Proto006PsCARTHAContractId__Originated
export class CGRIDClass__Proto006PsCARTHAContractId__Originated extends Box<Proto006PsCARTHAContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto006_PsCARTHAContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006PsCARTHAContractId__Implicit generated for Proto006PsCARTHAContractId__Implicit
export class CGRIDClass__Proto006PsCARTHAContractId__Implicit extends Box<Proto006PsCARTHAContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAContractId__Implicit {
        return new this(record_decoder<Proto006PsCARTHAContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto006PsCARTHAOperationAlphaContents__Transaction = { source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Transaction_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: N, destination: CGRIDClass__Proto006_PsCARTHAContract_id, parameters: Option<CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Transaction_parameters> };
export type Proto006PsCARTHAOperationAlphaContents__Seed_nonce_revelation = { level: Int32, nonce: FixedBytes<32> };
export type Proto006PsCARTHAOperationAlphaContents__Reveal = { source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Reveal_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_key: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Reveal_public_key };
export type Proto006PsCARTHAOperationAlphaContents__Proposals = { source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Proposals_source, period: Int32, proposals: Dynamic<Sequence<CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq>,width.Uint30> };
export type Proto006PsCARTHAOperationAlphaContents__Origination = { source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, balance: N, delegate: Option<CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Origination_delegate>, script: CGRIDClass__Proto006_PsCARTHAScriptedContracts };
export type Proto006PsCARTHAOperationAlphaContents__Endorsement = { level: Int32 };
export type Proto006PsCARTHAOperationAlphaContents__Double_endorsement_evidence = { op1: Dynamic<CGRIDClass__Proto006_PsCARTHAInlinedEndorsement,width.Uint30>, op2: Dynamic<CGRIDClass__Proto006_PsCARTHAInlinedEndorsement,width.Uint30> };
export type Proto006PsCARTHAOperationAlphaContents__Double_baking_evidence = { bh1: Dynamic<CGRIDClass__Proto006_PsCARTHABlock_headerAlphaFull_header,width.Uint30>, bh2: Dynamic<CGRIDClass__Proto006_PsCARTHABlock_headerAlphaFull_header,width.Uint30> };
export type Proto006PsCARTHAOperationAlphaContents__Delegation = { source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Delegation_source, fee: N, counter: N, gas_limit: N, storage_limit: N, delegate: Option<CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Delegation_delegate> };
export type Proto006PsCARTHAOperationAlphaContents__Ballot = { source: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Ballot_source, period: Int32, proposal: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Ballot_proposal, ballot: Int8 };
export type Proto006PsCARTHAOperationAlphaContents__Activate_account = { pkh: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Activate_account_pkh, secret: FixedBytes<20> };
export type Proto006PsCARTHAInlinedEndorsementContents__Endorsement = { level: Int32 };
export type Proto006PsCARTHAEntrypoint__set_delegate = Unit;
export type Proto006PsCARTHAEntrypoint__root = Unit;
export type Proto006PsCARTHAEntrypoint__remove_delegate = Unit;
export type Proto006PsCARTHAEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto006PsCARTHAEntrypoint___do = Unit;
export type Proto006PsCARTHAEntrypoint___default = Unit;
export type Proto006PsCARTHAContractId__Originated = Padded<CGRIDClass__Proto006_PsCARTHAContract_id_Originated_denest_pad,1>;
export type Proto006PsCARTHAContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAScriptedContracts generated for Proto006PsCARTHAScriptedContracts
export class CGRIDClass__Proto006_PsCARTHAScriptedContracts extends Box<Proto006PsCARTHAScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAScriptedContracts {
        return new this(record_decoder<Proto006PsCARTHAScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_and_signature_signature generated for Proto006PsCARTHAOperationAlphaContentsAndSignatureSignature
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_and_signature_signature extends Box<Proto006PsCARTHAOperationAlphaContentsAndSignatureSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_and_signature_signature {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsAndSignatureSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Transaction_source generated for Proto006PsCARTHAOperationAlphaContentsTransactionSource
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Transaction_source extends Box<Proto006PsCARTHAOperationAlphaContentsTransactionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Transaction_source {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsTransactionSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Transaction_parameters generated for Proto006PsCARTHAOperationAlphaContentsTransactionParameters
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Transaction_parameters extends Box<Proto006PsCARTHAOperationAlphaContentsTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Transaction_parameters {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsTransactionParameters>({entrypoint: CGRIDClass__Proto006_PsCARTHAEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Reveal_source generated for Proto006PsCARTHAOperationAlphaContentsRevealSource
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Reveal_source extends Box<Proto006PsCARTHAOperationAlphaContentsRevealSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Reveal_source {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsRevealSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Reveal_public_key generated for Proto006PsCARTHAOperationAlphaContentsRevealPublicKey
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Reveal_public_key extends Box<Proto006PsCARTHAOperationAlphaContentsRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Reveal_public_key {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Proposals_source generated for Proto006PsCARTHAOperationAlphaContentsProposalsSource
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Proposals_source extends Box<Proto006PsCARTHAOperationAlphaContentsProposalsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Proposals_source {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsProposalsSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq generated for Proto006PsCARTHAOperationAlphaContentsProposalsProposalsDenestDynDenestSeq
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq extends Box<Proto006PsCARTHAOperationAlphaContentsProposalsProposalsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Origination_source generated for Proto006PsCARTHAOperationAlphaContentsOriginationSource
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Origination_source extends Box<Proto006PsCARTHAOperationAlphaContentsOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Origination_source {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsOriginationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Origination_delegate generated for Proto006PsCARTHAOperationAlphaContentsOriginationDelegate
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Origination_delegate extends Box<Proto006PsCARTHAOperationAlphaContentsOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Origination_delegate {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Delegation_source generated for Proto006PsCARTHAOperationAlphaContentsDelegationSource
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Delegation_source extends Box<Proto006PsCARTHAOperationAlphaContentsDelegationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Delegation_source {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsDelegationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Delegation_delegate generated for Proto006PsCARTHAOperationAlphaContentsDelegationDelegate
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Delegation_delegate extends Box<Proto006PsCARTHAOperationAlphaContentsDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Delegation_delegate {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Ballot_source generated for Proto006PsCARTHAOperationAlphaContentsBallotSource
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Ballot_source extends Box<Proto006PsCARTHAOperationAlphaContentsBallotSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Ballot_source {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsBallotSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Ballot_proposal generated for Proto006PsCARTHAOperationAlphaContentsBallotProposal
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Ballot_proposal extends Box<Proto006PsCARTHAOperationAlphaContentsBallotProposal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Ballot_proposal {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsBallotProposal>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Activate_account_pkh generated for Proto006PsCARTHAOperationAlphaContentsActivateAccountPkh
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Activate_account_pkh extends Box<Proto006PsCARTHAOperationAlphaContentsActivateAccountPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_Activate_account_pkh {
        return new this(record_decoder<Proto006PsCARTHAOperationAlphaContentsActivateAccountPkh>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents generated for Proto006PsCARTHAOperationAlphaContents
export function proto006pscarthaoperationalphacontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto006PsCARTHAOperationAlphaContents,Proto006PsCARTHAOperationAlphaContents> {
    function f(disc: CGRIDTag__Proto006PsCARTHAOperationAlphaContents) {
        switch (disc) {
            case CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Endorsement: return CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Endorsement.decode;
            case CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Seed_nonce_revelation: return CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Seed_nonce_revelation.decode;
            case CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Double_endorsement_evidence: return CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Double_endorsement_evidence.decode;
            case CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Double_baking_evidence: return CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Double_baking_evidence.decode;
            case CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Activate_account: return CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Activate_account.decode;
            case CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Proposals: return CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Proposals.decode;
            case CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Ballot: return CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Ballot.decode;
            case CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Reveal: return CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Reveal.decode;
            case CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Transaction: return CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Transaction.decode;
            case CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Origination: return CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Origination.decode;
            case CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Delegation: return CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Delegation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto006PsCARTHAOperationAlphaContents => Object.values(CGRIDTag__Proto006PsCARTHAOperationAlphaContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto006_PsCARTHAOperationAlphaContents extends Box<Proto006PsCARTHAOperationAlphaContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto006PsCARTHAOperationAlphaContents>, Proto006PsCARTHAOperationAlphaContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAOperationAlphaContents {
        return new this(variant_decoder(width.Uint8)(proto006pscarthaoperationalphacontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAInlinedEndorsement_signature generated for Proto006PsCARTHAInlinedEndorsementSignature
export class CGRIDClass__Proto006_PsCARTHAInlinedEndorsement_signature extends Box<Proto006PsCARTHAInlinedEndorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAInlinedEndorsement_signature {
        return new this(record_decoder<Proto006PsCARTHAInlinedEndorsementSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAInlinedEndorsementContents generated for Proto006PsCARTHAInlinedEndorsementContents
export function proto006pscarthainlinedendorsementcontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto006PsCARTHAInlinedEndorsementContents,Proto006PsCARTHAInlinedEndorsementContents> {
    function f(disc: CGRIDTag__Proto006PsCARTHAInlinedEndorsementContents) {
        switch (disc) {
            case CGRIDTag__Proto006PsCARTHAInlinedEndorsementContents.Endorsement: return CGRIDClass__Proto006PsCARTHAInlinedEndorsementContents__Endorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto006PsCARTHAInlinedEndorsementContents => Object.values(CGRIDTag__Proto006PsCARTHAInlinedEndorsementContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto006_PsCARTHAInlinedEndorsementContents extends Box<Proto006PsCARTHAInlinedEndorsementContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto006PsCARTHAInlinedEndorsementContents>, Proto006PsCARTHAInlinedEndorsementContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAInlinedEndorsementContents {
        return new this(variant_decoder(width.Uint8)(proto006pscarthainlinedendorsementcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAInlinedEndorsement generated for Proto006PsCARTHAInlinedEndorsement
export class CGRIDClass__Proto006_PsCARTHAInlinedEndorsement extends Box<Proto006PsCARTHAInlinedEndorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAInlinedEndorsement {
        return new this(record_decoder<Proto006PsCARTHAInlinedEndorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__Proto006_PsCARTHAInlinedEndorsementContents.decode, signature: Nullable.decode(CGRIDClass__Proto006_PsCARTHAInlinedEndorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAEntrypoint generated for Proto006PsCARTHAEntrypoint
export function proto006pscarthaentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto006PsCARTHAEntrypoint,Proto006PsCARTHAEntrypoint> {
    function f(disc: CGRIDTag__Proto006PsCARTHAEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto006PsCARTHAEntrypoint._default: return CGRIDClass__Proto006PsCARTHAEntrypoint___default.decode;
            case CGRIDTag__Proto006PsCARTHAEntrypoint.root: return CGRIDClass__Proto006PsCARTHAEntrypoint__root.decode;
            case CGRIDTag__Proto006PsCARTHAEntrypoint._do: return CGRIDClass__Proto006PsCARTHAEntrypoint___do.decode;
            case CGRIDTag__Proto006PsCARTHAEntrypoint.set_delegate: return CGRIDClass__Proto006PsCARTHAEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto006PsCARTHAEntrypoint.remove_delegate: return CGRIDClass__Proto006PsCARTHAEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto006PsCARTHAEntrypoint.named: return CGRIDClass__Proto006PsCARTHAEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto006PsCARTHAEntrypoint => Object.values(CGRIDTag__Proto006PsCARTHAEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto006_PsCARTHAEntrypoint extends Box<Proto006PsCARTHAEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto006PsCARTHAEntrypoint>, Proto006PsCARTHAEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto006pscarthaentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAContract_id_Originated_denest_pad generated for Proto006PsCARTHAContractIdOriginatedDenestPad
export class CGRIDClass__Proto006_PsCARTHAContract_id_Originated_denest_pad extends Box<Proto006PsCARTHAContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto006PsCARTHAContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHAContract_id generated for Proto006PsCARTHAContractId
export function proto006pscarthacontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto006PsCARTHAContractId,Proto006PsCARTHAContractId> {
    function f(disc: CGRIDTag__Proto006PsCARTHAContractId) {
        switch (disc) {
            case CGRIDTag__Proto006PsCARTHAContractId.Implicit: return CGRIDClass__Proto006PsCARTHAContractId__Implicit.decode;
            case CGRIDTag__Proto006PsCARTHAContractId.Originated: return CGRIDClass__Proto006PsCARTHAContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto006PsCARTHAContractId => Object.values(CGRIDTag__Proto006PsCARTHAContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto006_PsCARTHAContract_id extends Box<Proto006PsCARTHAContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto006PsCARTHAContractId>, Proto006PsCARTHAContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHAContract_id {
        return new this(variant_decoder(width.Uint8)(proto006pscarthacontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHABlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto006_PsCARTHABlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHABlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHABlock_headerAlphaSigned_contents_signature generated for Proto006PsCARTHABlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto006_PsCARTHABlock_headerAlphaSigned_contents_signature extends Box<Proto006PsCARTHABlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHABlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto006PsCARTHABlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto006_PsCARTHABlock_headerAlphaFull_header generated for Proto006PsCARTHABlockHeaderAlphaFullHeader
export class CGRIDClass__Proto006_PsCARTHABlock_headerAlphaFull_header extends Box<Proto006PsCARTHABlockHeaderAlphaFullHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006_PsCARTHABlock_headerAlphaFull_header {
        return new this(record_decoder<Proto006PsCARTHABlockHeaderAlphaFullHeader>({level: Int32.decode, proto: Uint8.decode, predecessor: CGRIDClass__Block_headerShell_predecessor.decode, timestamp: Int64.decode, validation_pass: Uint8.decode, operations_hash: CGRIDClass__Block_headerShell_operations_hash.decode, fitness: Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30), context: CGRIDClass__Block_headerShell_context.decode, priority: Uint16.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto006_PsCARTHABlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), signature: CGRIDClass__Proto006_PsCARTHABlock_headerAlphaSigned_contents_signature.decode}, {order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.proto.encodeLength +  this.value.predecessor.encodeLength +  this.value.timestamp.encodeLength +  this.value.validation_pass.encodeLength +  this.value.operations_hash.encodeLength +  this.value.fitness.encodeLength +  this.value.context.encodeLength +  this.value.priority.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.proto.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.timestamp.writeTarget(tgt) +  this.value.validation_pass.writeTarget(tgt) +  this.value.operations_hash.writeTarget(tgt) +  this.value.fitness.writeTarget(tgt) +  this.value.context.writeTarget(tgt) +  this.value.priority.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_predecessor generated for BlockHeaderShellPredecessor
export class CGRIDClass__Block_headerShell_predecessor extends Box<BlockHeaderShellPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_predecessor {
        return new this(record_decoder<BlockHeaderShellPredecessor>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_operations_hash generated for BlockHeaderShellOperationsHash
export class CGRIDClass__Block_headerShell_operations_hash extends Box<BlockHeaderShellOperationsHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_list_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_operations_hash {
        return new this(record_decoder<BlockHeaderShellOperationsHash>({operation_list_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_list_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_list_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_list_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_context generated for BlockHeaderShellContext
export class CGRIDClass__Block_headerShell_context extends Box<BlockHeaderShellContext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_context {
        return new this(record_decoder<BlockHeaderShellContext>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export type BlockHeaderShellPredecessor = { block_hash: FixedBytes<32> };
export type BlockHeaderShellOperationsHash = { operation_list_list_hash: FixedBytes<32> };
export type BlockHeaderShellContext = { context_hash: FixedBytes<32> };
export type Proto006PsCARTHAScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export type Proto006PsCARTHAOperationAlphaContentsAndSignatureSignature = { signature_v0: FixedBytes<64> };
export type Proto006PsCARTHAOperationAlphaContentsTransactionSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto006PsCARTHAOperationAlphaContentsTransactionParameters = { entrypoint: CGRIDClass__Proto006_PsCARTHAEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto006PsCARTHAOperationAlphaContentsRevealSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto006PsCARTHAOperationAlphaContentsRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto006PsCARTHAOperationAlphaContentsProposalsSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto006PsCARTHAOperationAlphaContentsProposalsProposalsDenestDynDenestSeq = { protocol_hash: FixedBytes<32> };
export type Proto006PsCARTHAOperationAlphaContentsOriginationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto006PsCARTHAOperationAlphaContentsOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto006PsCARTHAOperationAlphaContentsDelegationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto006PsCARTHAOperationAlphaContentsDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto006PsCARTHAOperationAlphaContentsBallotSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto006PsCARTHAOperationAlphaContentsBallotProposal = { protocol_hash: FixedBytes<32> };
export type Proto006PsCARTHAOperationAlphaContentsActivateAccountPkh = { ed25519_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__Proto006PsCARTHAOperationAlphaContents{
    Endorsement = 0,
    Seed_nonce_revelation = 1,
    Double_endorsement_evidence = 2,
    Double_baking_evidence = 3,
    Activate_account = 4,
    Proposals = 5,
    Ballot = 6,
    Reveal = 107,
    Transaction = 108,
    Origination = 109,
    Delegation = 110
}
export interface CGRIDMap__Proto006PsCARTHAOperationAlphaContents {
    Endorsement: CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Endorsement,
    Seed_nonce_revelation: CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Seed_nonce_revelation,
    Double_endorsement_evidence: CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Double_endorsement_evidence,
    Double_baking_evidence: CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Double_baking_evidence,
    Activate_account: CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Activate_account,
    Proposals: CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Proposals,
    Ballot: CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Ballot,
    Reveal: CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Reveal,
    Transaction: CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Transaction,
    Origination: CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Origination,
    Delegation: CGRIDClass__Proto006PsCARTHAOperationAlphaContents__Delegation
}
export type Proto006PsCARTHAOperationAlphaContents = { kind: CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Endorsement, value: CGRIDMap__Proto006PsCARTHAOperationAlphaContents['Endorsement'] } | { kind: CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Seed_nonce_revelation, value: CGRIDMap__Proto006PsCARTHAOperationAlphaContents['Seed_nonce_revelation'] } | { kind: CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Double_endorsement_evidence, value: CGRIDMap__Proto006PsCARTHAOperationAlphaContents['Double_endorsement_evidence'] } | { kind: CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Double_baking_evidence, value: CGRIDMap__Proto006PsCARTHAOperationAlphaContents['Double_baking_evidence'] } | { kind: CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Activate_account, value: CGRIDMap__Proto006PsCARTHAOperationAlphaContents['Activate_account'] } | { kind: CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Proposals, value: CGRIDMap__Proto006PsCARTHAOperationAlphaContents['Proposals'] } | { kind: CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Ballot, value: CGRIDMap__Proto006PsCARTHAOperationAlphaContents['Ballot'] } | { kind: CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Reveal, value: CGRIDMap__Proto006PsCARTHAOperationAlphaContents['Reveal'] } | { kind: CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Transaction, value: CGRIDMap__Proto006PsCARTHAOperationAlphaContents['Transaction'] } | { kind: CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Origination, value: CGRIDMap__Proto006PsCARTHAOperationAlphaContents['Origination'] } | { kind: CGRIDTag__Proto006PsCARTHAOperationAlphaContents.Delegation, value: CGRIDMap__Proto006PsCARTHAOperationAlphaContents['Delegation'] };
export type Proto006PsCARTHAInlinedEndorsementSignature = { signature_v0: FixedBytes<64> };
export enum CGRIDTag__Proto006PsCARTHAInlinedEndorsementContents{
    Endorsement = 0
}
export interface CGRIDMap__Proto006PsCARTHAInlinedEndorsementContents {
    Endorsement: CGRIDClass__Proto006PsCARTHAInlinedEndorsementContents__Endorsement
}
export type Proto006PsCARTHAInlinedEndorsementContents = { kind: CGRIDTag__Proto006PsCARTHAInlinedEndorsementContents.Endorsement, value: CGRIDMap__Proto006PsCARTHAInlinedEndorsementContents['Endorsement'] };
export type Proto006PsCARTHAInlinedEndorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__Proto006_PsCARTHAInlinedEndorsementContents, signature: Nullable<CGRIDClass__Proto006_PsCARTHAInlinedEndorsement_signature> };
export enum CGRIDTag__Proto006PsCARTHAEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto006PsCARTHAEntrypoint {
    _default: CGRIDClass__Proto006PsCARTHAEntrypoint___default,
    root: CGRIDClass__Proto006PsCARTHAEntrypoint__root,
    _do: CGRIDClass__Proto006PsCARTHAEntrypoint___do,
    set_delegate: CGRIDClass__Proto006PsCARTHAEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto006PsCARTHAEntrypoint__remove_delegate,
    named: CGRIDClass__Proto006PsCARTHAEntrypoint__named
}
export type Proto006PsCARTHAEntrypoint = { kind: CGRIDTag__Proto006PsCARTHAEntrypoint._default, value: CGRIDMap__Proto006PsCARTHAEntrypoint['_default'] } | { kind: CGRIDTag__Proto006PsCARTHAEntrypoint.root, value: CGRIDMap__Proto006PsCARTHAEntrypoint['root'] } | { kind: CGRIDTag__Proto006PsCARTHAEntrypoint._do, value: CGRIDMap__Proto006PsCARTHAEntrypoint['_do'] } | { kind: CGRIDTag__Proto006PsCARTHAEntrypoint.set_delegate, value: CGRIDMap__Proto006PsCARTHAEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto006PsCARTHAEntrypoint.remove_delegate, value: CGRIDMap__Proto006PsCARTHAEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto006PsCARTHAEntrypoint.named, value: CGRIDMap__Proto006PsCARTHAEntrypoint['named'] };
export type Proto006PsCARTHAContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto006PsCARTHAContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto006PsCARTHAContractId {
    Implicit: CGRIDClass__Proto006PsCARTHAContractId__Implicit,
    Originated: CGRIDClass__Proto006PsCARTHAContractId__Originated
}
export type Proto006PsCARTHAContractId = { kind: CGRIDTag__Proto006PsCARTHAContractId.Implicit, value: CGRIDMap__Proto006PsCARTHAContractId['Implicit'] } | { kind: CGRIDTag__Proto006PsCARTHAContractId.Originated, value: CGRIDMap__Proto006PsCARTHAContractId['Originated'] };
export type Proto006PsCARTHABlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto006PsCARTHABlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto006PsCARTHABlockHeaderAlphaFullHeader = { level: Int32, proto: Uint8, predecessor: CGRIDClass__Block_headerShell_predecessor, timestamp: Int64, validation_pass: Uint8, operations_hash: CGRIDClass__Block_headerShell_operations_hash, fitness: Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>, context: CGRIDClass__Block_headerShell_context, priority: Uint16, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto006_PsCARTHABlock_headerAlphaUnsigned_contents_seed_nonce_hash>, signature: CGRIDClass__Proto006_PsCARTHABlock_headerAlphaSigned_contents_signature };
export type Proto006PsCARTHAOperationProtocolData = { contents: VPadded<Sequence<CGRIDClass__Proto006_PsCARTHAOperationAlphaContents>,64>, signature: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_and_signature_signature };
export class CGRIDClass__Proto006PsCARTHAOperationProtocolData extends Box<Proto006PsCARTHAOperationProtocolData> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contents', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAOperationProtocolData {
        return new this(record_decoder<Proto006PsCARTHAOperationProtocolData>({contents: VPadded.decode(Sequence.decode(CGRIDClass__Proto006_PsCARTHAOperationAlphaContents.decode), 64), signature: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_and_signature_signature.decode}, {order: ['contents', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.contents.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contents.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export const proto006_pscartha_operation_protocol_data_encoder = (value: Proto006PsCARTHAOperationProtocolData): OutputBytes => {
    return record_encoder({order: ['contents', 'signature']})(value);
}
export const proto006_pscartha_operation_protocol_data_decoder = (p: Parser): Proto006PsCARTHAOperationProtocolData => {
    return record_decoder<Proto006PsCARTHAOperationProtocolData>({contents: VPadded.decode(Sequence.decode(CGRIDClass__Proto006_PsCARTHAOperationAlphaContents.decode), 64), signature: CGRIDClass__Proto006_PsCARTHAOperationAlphaContents_and_signature_signature.decode}, {order: ['contents', 'signature']})(p);
}
