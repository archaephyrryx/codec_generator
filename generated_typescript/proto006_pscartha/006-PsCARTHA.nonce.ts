import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type Proto006PsCARTHANonce = FixedBytes<32>;
export class CGRIDClass__Proto006PsCARTHANonce extends Box<Proto006PsCARTHANonce> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHANonce {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto006_pscartha_nonce_encoder = (value: Proto006PsCARTHANonce): OutputBytes => {
    return value.encode();
}
export const proto006_pscartha_nonce_decoder = (p: Parser): Proto006PsCARTHANonce => {
    return FixedBytes.decode<32>({len: 32})(p);
}
