import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto006PsCARTHATez = N;
export class CGRIDClass__Proto006PsCARTHATez extends Box<Proto006PsCARTHATez> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHATez {
        return new this(N.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto006_pscartha_tez_encoder = (value: Proto006PsCARTHATez): OutputBytes => {
    return value.encode();
}
export const proto006_pscartha_tez_decoder = (p: Parser): Proto006PsCARTHATez => {
    return N.decode(p);
}
