import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto006PsCARTHAGas__Unaccounted generated for Proto006PsCARTHAGas__Unaccounted
export class CGRIDClass__Proto006PsCARTHAGas__Unaccounted extends Box<Proto006PsCARTHAGas__Unaccounted> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAGas__Unaccounted {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006PsCARTHAGas__Limited generated for Proto006PsCARTHAGas__Limited
export class CGRIDClass__Proto006PsCARTHAGas__Limited extends Box<Proto006PsCARTHAGas__Limited> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAGas__Limited {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto006PsCARTHAGas__Unaccounted = Unit;
export type Proto006PsCARTHAGas__Limited = Z;
export enum CGRIDTag__Proto006PsCARTHAGas{
    Limited = 0,
    Unaccounted = 1
}
export interface CGRIDMap__Proto006PsCARTHAGas {
    Limited: CGRIDClass__Proto006PsCARTHAGas__Limited,
    Unaccounted: CGRIDClass__Proto006PsCARTHAGas__Unaccounted
}
export type Proto006PsCARTHAGas = { kind: CGRIDTag__Proto006PsCARTHAGas.Limited, value: CGRIDMap__Proto006PsCARTHAGas['Limited'] } | { kind: CGRIDTag__Proto006PsCARTHAGas.Unaccounted, value: CGRIDMap__Proto006PsCARTHAGas['Unaccounted'] };
export function proto006pscarthagas_mkDecoder(): VariantDecoder<CGRIDTag__Proto006PsCARTHAGas,Proto006PsCARTHAGas> {
    function f(disc: CGRIDTag__Proto006PsCARTHAGas) {
        switch (disc) {
            case CGRIDTag__Proto006PsCARTHAGas.Limited: return CGRIDClass__Proto006PsCARTHAGas__Limited.decode;
            case CGRIDTag__Proto006PsCARTHAGas.Unaccounted: return CGRIDClass__Proto006PsCARTHAGas__Unaccounted.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto006PsCARTHAGas => Object.values(CGRIDTag__Proto006PsCARTHAGas).includes(tagval);
    return f;
}
export class CGRIDClass__Proto006PsCARTHAGas extends Box<Proto006PsCARTHAGas> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto006PsCARTHAGas>, Proto006PsCARTHAGas>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAGas {
        return new this(variant_decoder(width.Uint8)(proto006pscarthagas_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto006_pscartha_gas_encoder = (value: Proto006PsCARTHAGas): OutputBytes => {
    return variant_encoder<KindOf<Proto006PsCARTHAGas>, Proto006PsCARTHAGas>(width.Uint8)(value);
}
export const proto006_pscartha_gas_decoder = (p: Parser): Proto006PsCARTHAGas => {
    return variant_decoder(width.Uint8)(proto006pscarthagas_mkDecoder())(p);
}
