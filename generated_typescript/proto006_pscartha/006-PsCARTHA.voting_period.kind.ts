import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Testing_vote generated for Proto006PsCARTHAVotingPeriodKind__Testing_vote
export class CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Testing_vote extends Box<Proto006PsCARTHAVotingPeriodKind__Testing_vote> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Testing_vote {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Testing generated for Proto006PsCARTHAVotingPeriodKind__Testing
export class CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Testing extends Box<Proto006PsCARTHAVotingPeriodKind__Testing> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Testing {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Proposal generated for Proto006PsCARTHAVotingPeriodKind__Proposal
export class CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Proposal extends Box<Proto006PsCARTHAVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Promotion_vote generated for Proto006PsCARTHAVotingPeriodKind__Promotion_vote
export class CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Promotion_vote extends Box<Proto006PsCARTHAVotingPeriodKind__Promotion_vote> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Promotion_vote {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto006PsCARTHAVotingPeriodKind__Testing_vote = Unit;
export type Proto006PsCARTHAVotingPeriodKind__Testing = Unit;
export type Proto006PsCARTHAVotingPeriodKind__Proposal = Unit;
export type Proto006PsCARTHAVotingPeriodKind__Promotion_vote = Unit;
export enum CGRIDTag__Proto006PsCARTHAVotingPeriodKind{
    Proposal = 0,
    Testing_vote = 1,
    Testing = 2,
    Promotion_vote = 3
}
export interface CGRIDMap__Proto006PsCARTHAVotingPeriodKind {
    Proposal: CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Proposal,
    Testing_vote: CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Testing_vote,
    Testing: CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Testing,
    Promotion_vote: CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Promotion_vote
}
export type Proto006PsCARTHAVotingPeriodKind = { kind: CGRIDTag__Proto006PsCARTHAVotingPeriodKind.Proposal, value: CGRIDMap__Proto006PsCARTHAVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto006PsCARTHAVotingPeriodKind.Testing_vote, value: CGRIDMap__Proto006PsCARTHAVotingPeriodKind['Testing_vote'] } | { kind: CGRIDTag__Proto006PsCARTHAVotingPeriodKind.Testing, value: CGRIDMap__Proto006PsCARTHAVotingPeriodKind['Testing'] } | { kind: CGRIDTag__Proto006PsCARTHAVotingPeriodKind.Promotion_vote, value: CGRIDMap__Proto006PsCARTHAVotingPeriodKind['Promotion_vote'] };
export function proto006pscarthavotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto006PsCARTHAVotingPeriodKind,Proto006PsCARTHAVotingPeriodKind> {
    function f(disc: CGRIDTag__Proto006PsCARTHAVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto006PsCARTHAVotingPeriodKind.Proposal: return CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto006PsCARTHAVotingPeriodKind.Testing_vote: return CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Testing_vote.decode;
            case CGRIDTag__Proto006PsCARTHAVotingPeriodKind.Testing: return CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Testing.decode;
            case CGRIDTag__Proto006PsCARTHAVotingPeriodKind.Promotion_vote: return CGRIDClass__Proto006PsCARTHAVotingPeriodKind__Promotion_vote.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto006PsCARTHAVotingPeriodKind => Object.values(CGRIDTag__Proto006PsCARTHAVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto006PsCARTHAVotingPeriodKind extends Box<Proto006PsCARTHAVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto006PsCARTHAVotingPeriodKind>, Proto006PsCARTHAVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHAVotingPeriodKind {
        return new this(variant_decoder(width.Uint8)(proto006pscarthavotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto006_pscartha_voting_period_kind_encoder = (value: Proto006PsCARTHAVotingPeriodKind): OutputBytes => {
    return variant_encoder<KindOf<Proto006PsCARTHAVotingPeriodKind>, Proto006PsCARTHAVotingPeriodKind>(width.Uint8)(value);
}
export const proto006_pscartha_voting_period_kind_decoder = (p: Parser): Proto006PsCARTHAVotingPeriodKind => {
    return variant_decoder(width.Uint8)(proto006pscarthavotingperiodkind_mkDecoder())(p);
}
