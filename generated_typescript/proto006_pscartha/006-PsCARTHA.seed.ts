import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type Proto006PsCARTHASeed = FixedBytes<32>;
export class CGRIDClass__Proto006PsCARTHASeed extends Box<Proto006PsCARTHASeed> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto006PsCARTHASeed {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto006_pscartha_seed_encoder = (value: Proto006PsCARTHASeed): OutputBytes => {
    return value.encode();
}
export const proto006_pscartha_seed_decoder = (p: Parser): Proto006PsCARTHASeed => {
    return FixedBytes.decode<32>({len: 32})(p);
}
