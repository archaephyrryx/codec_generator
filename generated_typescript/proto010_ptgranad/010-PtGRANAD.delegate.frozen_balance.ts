import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto010PtGRANADDelegateFrozenBalance = { deposits: N, fees: N, rewards: N };
export class CGRIDClass__Proto010PtGRANADDelegateFrozenBalance extends Box<Proto010PtGRANADDelegateFrozenBalance> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['deposits', 'fees', 'rewards']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADDelegateFrozenBalance {
        return new this(record_decoder<Proto010PtGRANADDelegateFrozenBalance>({deposits: N.decode, fees: N.decode, rewards: N.decode}, {order: ['deposits', 'fees', 'rewards']})(p));
    };
    get encodeLength(): number {
        return (this.value.deposits.encodeLength +  this.value.fees.encodeLength +  this.value.rewards.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.deposits.writeTarget(tgt) +  this.value.fees.writeTarget(tgt) +  this.value.rewards.writeTarget(tgt));
    }
}
export const proto010_ptgranad_delegate_frozen_balance_encoder = (value: Proto010PtGRANADDelegateFrozenBalance): OutputBytes => {
    return record_encoder({order: ['deposits', 'fees', 'rewards']})(value);
}
export const proto010_ptgranad_delegate_frozen_balance_decoder = (p: Parser): Proto010PtGRANADDelegateFrozenBalance => {
    return record_decoder<Proto010PtGRANADDelegateFrozenBalance>({deposits: N.decode, fees: N.decode, rewards: N.decode}, {order: ['deposits', 'fees', 'rewards']})(p);
}
