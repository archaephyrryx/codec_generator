import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto010PtGRANADRawLevel = Int32;
export class CGRIDClass__Proto010PtGRANADRawLevel extends Box<Proto010PtGRANADRawLevel> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADRawLevel {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto010_ptgranad_raw_level_encoder = (value: Proto010PtGRANADRawLevel): OutputBytes => {
    return value.encode();
}
export const proto010_ptgranad_raw_level_decoder = (p: Parser): Proto010PtGRANADRawLevel => {
    return Int32.decode(p);
}
