import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int31, Int32, Int64, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_unknown generated for Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_unknown
export class CGRIDClass__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_unknown extends Box<Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_unknown> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_unknown {
        return new this(tuple_decoder<Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_unknown>(CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq_Public_key_known_Public_key_unknown_index0.decode, N.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_known generated for Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_known
export class CGRIDClass__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_known extends Box<Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_known> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_known {
        return new this(tuple_decoder<Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_known>(CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq_Public_key_known_index0.decode, N.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_unknown = [CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq_Public_key_known_Public_key_unknown_index0, N];
export type Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_known = [CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq_Public_key_known_index0, N];
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADScriptedContracts generated for Proto010PtGRANADScriptedContracts
export class CGRIDClass__Proto010_PtGRANADScriptedContracts extends Box<Proto010PtGRANADScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADScriptedContracts {
        return new this(record_decoder<Proto010PtGRANADScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADParameters_commitments_denest_dyn_denest_seq_index0 generated for Proto010PtGRANADParametersCommitmentsDenestDynDenestSeqIndex0
export class CGRIDClass__Proto010_PtGRANADParameters_commitments_denest_dyn_denest_seq_index0 extends Box<Proto010PtGRANADParametersCommitmentsDenestDynDenestSeqIndex0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['blinded_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADParameters_commitments_denest_dyn_denest_seq_index0 {
        return new this(record_decoder<Proto010PtGRANADParametersCommitmentsDenestDynDenestSeqIndex0>({blinded_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['blinded_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.blinded_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.blinded_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADParameters_commitments_denest_dyn_denest_seq generated for Proto010PtGRANADParametersCommitmentsDenestDynDenestSeq
export class CGRIDClass__Proto010_PtGRANADParameters_commitments_denest_dyn_denest_seq extends Box<Proto010PtGRANADParametersCommitmentsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADParameters_commitments_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto010PtGRANADParametersCommitmentsDenestDynDenestSeq>(CGRIDClass__Proto010_PtGRANADParameters_commitments_denest_dyn_denest_seq_index0.decode, N.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADParameters_bootstrap_contracts_denest_dyn_denest_seq_delegate generated for Proto010PtGRANADParametersBootstrapContractsDenestDynDenestSeqDelegate
export class CGRIDClass__Proto010_PtGRANADParameters_bootstrap_contracts_denest_dyn_denest_seq_delegate extends Box<Proto010PtGRANADParametersBootstrapContractsDenestDynDenestSeqDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADParameters_bootstrap_contracts_denest_dyn_denest_seq_delegate {
        return new this(record_decoder<Proto010PtGRANADParametersBootstrapContractsDenestDynDenestSeqDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADParameters_bootstrap_contracts_denest_dyn_denest_seq generated for Proto010PtGRANADParametersBootstrapContractsDenestDynDenestSeq
export class CGRIDClass__Proto010_PtGRANADParameters_bootstrap_contracts_denest_dyn_denest_seq extends Box<Proto010PtGRANADParametersBootstrapContractsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['delegate', 'amount', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADParameters_bootstrap_contracts_denest_dyn_denest_seq {
        return new this(record_decoder<Proto010PtGRANADParametersBootstrapContractsDenestDynDenestSeq>({delegate: CGRIDClass__Proto010_PtGRANADParameters_bootstrap_contracts_denest_dyn_denest_seq_delegate.decode, amount: N.decode, script: CGRIDClass__Proto010_PtGRANADScriptedContracts.decode}, {order: ['delegate', 'amount', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.delegate.encodeLength +  this.value.amount.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.delegate.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq_Public_key_known_index0 generated for Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeqPublicKeyKnownIndex0
export class CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq_Public_key_known_index0 extends Box<Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeqPublicKeyKnownIndex0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq_Public_key_known_index0 {
        return new this(record_decoder<Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeqPublicKeyKnownIndex0>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq_Public_key_known_Public_key_unknown_index0 generated for Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeqPublicKeyKnownPublicKeyUnknownIndex0
export class CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq_Public_key_known_Public_key_unknown_index0 extends Box<Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeqPublicKeyKnownPublicKeyUnknownIndex0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq_Public_key_known_Public_key_unknown_index0 {
        return new this(record_decoder<Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeqPublicKeyKnownPublicKeyUnknownIndex0>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq generated for Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq
export function proto010ptgranadparametersbootstrapaccountsdenestdyndenestseq_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq,Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq> {
    function f(disc: CGRIDTag__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq.Public_key_known: return CGRIDClass__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_known.decode;
            case CGRIDTag__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq.Public_key_unknown: return CGRIDClass__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_unknown.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq => Object.values(CGRIDTag__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq extends Box<Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq>, Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadparametersbootstrapaccountsdenestdyndenestseq_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type Proto010PtGRANADScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export type Proto010PtGRANADParametersCommitmentsDenestDynDenestSeqIndex0 = { blinded_public_key_hash: FixedBytes<20> };
export type Proto010PtGRANADParametersCommitmentsDenestDynDenestSeq = [CGRIDClass__Proto010_PtGRANADParameters_commitments_denest_dyn_denest_seq_index0, N];
export type Proto010PtGRANADParametersBootstrapContractsDenestDynDenestSeqDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADParametersBootstrapContractsDenestDynDenestSeq = { delegate: CGRIDClass__Proto010_PtGRANADParameters_bootstrap_contracts_denest_dyn_denest_seq_delegate, amount: N, script: CGRIDClass__Proto010_PtGRANADScriptedContracts };
export type Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeqPublicKeyKnownIndex0 = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeqPublicKeyKnownPublicKeyUnknownIndex0 = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq{
    Public_key_known = 0,
    Public_key_unknown = 1
}
export interface CGRIDMap__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq {
    Public_key_known: CGRIDClass__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_known,
    Public_key_unknown: CGRIDClass__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq__Public_key_unknown
}
export type Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq = { kind: CGRIDTag__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq.Public_key_known, value: CGRIDMap__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq['Public_key_known'] } | { kind: CGRIDTag__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq.Public_key_unknown, value: CGRIDMap__Proto010PtGRANADParametersBootstrapAccountsDenestDynDenestSeq['Public_key_unknown'] };
export type Proto010PtGRANADParameters = { bootstrap_accounts: Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq>,width.Uint30>, bootstrap_contracts: Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADParameters_bootstrap_contracts_denest_dyn_denest_seq>,width.Uint30>, commitments: Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADParameters_commitments_denest_dyn_denest_seq>,width.Uint30>, security_deposit_ramp_up_cycles: Option<Int31>, no_reward_cycles: Option<Int31>, preserved_cycles: Uint8, blocks_per_cycle: Int32, blocks_per_commitment: Int32, blocks_per_roll_snapshot: Int32, blocks_per_voting_period: Int32, time_between_blocks: Dynamic<Sequence<Int64>,width.Uint30>, endorsers_per_block: Uint16, hard_gas_limit_per_operation: Z, hard_gas_limit_per_block: Z, proof_of_work_threshold: Int64, tokens_per_roll: N, michelson_maximum_type_size: Uint16, seed_nonce_revelation_tip: N, origination_size: Int31, block_security_deposit: N, endorsement_security_deposit: N, baking_reward_per_endorsement: Dynamic<Sequence<N>,width.Uint30>, endorsement_reward: Dynamic<Sequence<N>,width.Uint30>, cost_per_byte: N, hard_storage_limit_per_operation: Z, quorum_min: Int32, quorum_max: Int32, min_proposal_quorum: Int32, initial_endorsers: Uint16, delay_per_missing_endorsement: Int64, minimal_block_delay: Int64, liquidity_baking_subsidy: N, liquidity_baking_sunset_level: Int32, liquidity_baking_escape_ema_threshold: Int32 };
export class CGRIDClass__Proto010PtGRANADParameters extends Box<Proto010PtGRANADParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bootstrap_accounts', 'bootstrap_contracts', 'commitments', 'security_deposit_ramp_up_cycles', 'no_reward_cycles', 'preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'blocks_per_roll_snapshot', 'blocks_per_voting_period', 'time_between_blocks', 'endorsers_per_block', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'tokens_per_roll', 'michelson_maximum_type_size', 'seed_nonce_revelation_tip', 'origination_size', 'block_security_deposit', 'endorsement_security_deposit', 'baking_reward_per_endorsement', 'endorsement_reward', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'initial_endorsers', 'delay_per_missing_endorsement', 'minimal_block_delay', 'liquidity_baking_subsidy', 'liquidity_baking_sunset_level', 'liquidity_baking_escape_ema_threshold']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADParameters {
        return new this(record_decoder<Proto010PtGRANADParameters>({bootstrap_accounts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq.decode), width.Uint30), bootstrap_contracts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADParameters_bootstrap_contracts_denest_dyn_denest_seq.decode), width.Uint30), commitments: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADParameters_commitments_denest_dyn_denest_seq.decode), width.Uint30), security_deposit_ramp_up_cycles: Option.decode(Int31.decode), no_reward_cycles: Option.decode(Int31.decode), preserved_cycles: Uint8.decode, blocks_per_cycle: Int32.decode, blocks_per_commitment: Int32.decode, blocks_per_roll_snapshot: Int32.decode, blocks_per_voting_period: Int32.decode, time_between_blocks: Dynamic.decode(Sequence.decode(Int64.decode), width.Uint30), endorsers_per_block: Uint16.decode, hard_gas_limit_per_operation: Z.decode, hard_gas_limit_per_block: Z.decode, proof_of_work_threshold: Int64.decode, tokens_per_roll: N.decode, michelson_maximum_type_size: Uint16.decode, seed_nonce_revelation_tip: N.decode, origination_size: Int31.decode, block_security_deposit: N.decode, endorsement_security_deposit: N.decode, baking_reward_per_endorsement: Dynamic.decode(Sequence.decode(N.decode), width.Uint30), endorsement_reward: Dynamic.decode(Sequence.decode(N.decode), width.Uint30), cost_per_byte: N.decode, hard_storage_limit_per_operation: Z.decode, quorum_min: Int32.decode, quorum_max: Int32.decode, min_proposal_quorum: Int32.decode, initial_endorsers: Uint16.decode, delay_per_missing_endorsement: Int64.decode, minimal_block_delay: Int64.decode, liquidity_baking_subsidy: N.decode, liquidity_baking_sunset_level: Int32.decode, liquidity_baking_escape_ema_threshold: Int32.decode}, {order: ['bootstrap_accounts', 'bootstrap_contracts', 'commitments', 'security_deposit_ramp_up_cycles', 'no_reward_cycles', 'preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'blocks_per_roll_snapshot', 'blocks_per_voting_period', 'time_between_blocks', 'endorsers_per_block', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'tokens_per_roll', 'michelson_maximum_type_size', 'seed_nonce_revelation_tip', 'origination_size', 'block_security_deposit', 'endorsement_security_deposit', 'baking_reward_per_endorsement', 'endorsement_reward', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'initial_endorsers', 'delay_per_missing_endorsement', 'minimal_block_delay', 'liquidity_baking_subsidy', 'liquidity_baking_sunset_level', 'liquidity_baking_escape_ema_threshold']})(p));
    };
    get encodeLength(): number {
        return (this.value.bootstrap_accounts.encodeLength +  this.value.bootstrap_contracts.encodeLength +  this.value.commitments.encodeLength +  this.value.security_deposit_ramp_up_cycles.encodeLength +  this.value.no_reward_cycles.encodeLength +  this.value.preserved_cycles.encodeLength +  this.value.blocks_per_cycle.encodeLength +  this.value.blocks_per_commitment.encodeLength +  this.value.blocks_per_roll_snapshot.encodeLength +  this.value.blocks_per_voting_period.encodeLength +  this.value.time_between_blocks.encodeLength +  this.value.endorsers_per_block.encodeLength +  this.value.hard_gas_limit_per_operation.encodeLength +  this.value.hard_gas_limit_per_block.encodeLength +  this.value.proof_of_work_threshold.encodeLength +  this.value.tokens_per_roll.encodeLength +  this.value.michelson_maximum_type_size.encodeLength +  this.value.seed_nonce_revelation_tip.encodeLength +  this.value.origination_size.encodeLength +  this.value.block_security_deposit.encodeLength +  this.value.endorsement_security_deposit.encodeLength +  this.value.baking_reward_per_endorsement.encodeLength +  this.value.endorsement_reward.encodeLength +  this.value.cost_per_byte.encodeLength +  this.value.hard_storage_limit_per_operation.encodeLength +  this.value.quorum_min.encodeLength +  this.value.quorum_max.encodeLength +  this.value.min_proposal_quorum.encodeLength +  this.value.initial_endorsers.encodeLength +  this.value.delay_per_missing_endorsement.encodeLength +  this.value.minimal_block_delay.encodeLength +  this.value.liquidity_baking_subsidy.encodeLength +  this.value.liquidity_baking_sunset_level.encodeLength +  this.value.liquidity_baking_escape_ema_threshold.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bootstrap_accounts.writeTarget(tgt) +  this.value.bootstrap_contracts.writeTarget(tgt) +  this.value.commitments.writeTarget(tgt) +  this.value.security_deposit_ramp_up_cycles.writeTarget(tgt) +  this.value.no_reward_cycles.writeTarget(tgt) +  this.value.preserved_cycles.writeTarget(tgt) +  this.value.blocks_per_cycle.writeTarget(tgt) +  this.value.blocks_per_commitment.writeTarget(tgt) +  this.value.blocks_per_roll_snapshot.writeTarget(tgt) +  this.value.blocks_per_voting_period.writeTarget(tgt) +  this.value.time_between_blocks.writeTarget(tgt) +  this.value.endorsers_per_block.writeTarget(tgt) +  this.value.hard_gas_limit_per_operation.writeTarget(tgt) +  this.value.hard_gas_limit_per_block.writeTarget(tgt) +  this.value.proof_of_work_threshold.writeTarget(tgt) +  this.value.tokens_per_roll.writeTarget(tgt) +  this.value.michelson_maximum_type_size.writeTarget(tgt) +  this.value.seed_nonce_revelation_tip.writeTarget(tgt) +  this.value.origination_size.writeTarget(tgt) +  this.value.block_security_deposit.writeTarget(tgt) +  this.value.endorsement_security_deposit.writeTarget(tgt) +  this.value.baking_reward_per_endorsement.writeTarget(tgt) +  this.value.endorsement_reward.writeTarget(tgt) +  this.value.cost_per_byte.writeTarget(tgt) +  this.value.hard_storage_limit_per_operation.writeTarget(tgt) +  this.value.quorum_min.writeTarget(tgt) +  this.value.quorum_max.writeTarget(tgt) +  this.value.min_proposal_quorum.writeTarget(tgt) +  this.value.initial_endorsers.writeTarget(tgt) +  this.value.delay_per_missing_endorsement.writeTarget(tgt) +  this.value.minimal_block_delay.writeTarget(tgt) +  this.value.liquidity_baking_subsidy.writeTarget(tgt) +  this.value.liquidity_baking_sunset_level.writeTarget(tgt) +  this.value.liquidity_baking_escape_ema_threshold.writeTarget(tgt));
    }
}
export const proto010_ptgranad_parameters_encoder = (value: Proto010PtGRANADParameters): OutputBytes => {
    return record_encoder({order: ['bootstrap_accounts', 'bootstrap_contracts', 'commitments', 'security_deposit_ramp_up_cycles', 'no_reward_cycles', 'preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'blocks_per_roll_snapshot', 'blocks_per_voting_period', 'time_between_blocks', 'endorsers_per_block', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'tokens_per_roll', 'michelson_maximum_type_size', 'seed_nonce_revelation_tip', 'origination_size', 'block_security_deposit', 'endorsement_security_deposit', 'baking_reward_per_endorsement', 'endorsement_reward', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'initial_endorsers', 'delay_per_missing_endorsement', 'minimal_block_delay', 'liquidity_baking_subsidy', 'liquidity_baking_sunset_level', 'liquidity_baking_escape_ema_threshold']})(value);
}
export const proto010_ptgranad_parameters_decoder = (p: Parser): Proto010PtGRANADParameters => {
    return record_decoder<Proto010PtGRANADParameters>({bootstrap_accounts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADParameters_bootstrap_accounts_denest_dyn_denest_seq.decode), width.Uint30), bootstrap_contracts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADParameters_bootstrap_contracts_denest_dyn_denest_seq.decode), width.Uint30), commitments: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADParameters_commitments_denest_dyn_denest_seq.decode), width.Uint30), security_deposit_ramp_up_cycles: Option.decode(Int31.decode), no_reward_cycles: Option.decode(Int31.decode), preserved_cycles: Uint8.decode, blocks_per_cycle: Int32.decode, blocks_per_commitment: Int32.decode, blocks_per_roll_snapshot: Int32.decode, blocks_per_voting_period: Int32.decode, time_between_blocks: Dynamic.decode(Sequence.decode(Int64.decode), width.Uint30), endorsers_per_block: Uint16.decode, hard_gas_limit_per_operation: Z.decode, hard_gas_limit_per_block: Z.decode, proof_of_work_threshold: Int64.decode, tokens_per_roll: N.decode, michelson_maximum_type_size: Uint16.decode, seed_nonce_revelation_tip: N.decode, origination_size: Int31.decode, block_security_deposit: N.decode, endorsement_security_deposit: N.decode, baking_reward_per_endorsement: Dynamic.decode(Sequence.decode(N.decode), width.Uint30), endorsement_reward: Dynamic.decode(Sequence.decode(N.decode), width.Uint30), cost_per_byte: N.decode, hard_storage_limit_per_operation: Z.decode, quorum_min: Int32.decode, quorum_max: Int32.decode, min_proposal_quorum: Int32.decode, initial_endorsers: Uint16.decode, delay_per_missing_endorsement: Int64.decode, minimal_block_delay: Int64.decode, liquidity_baking_subsidy: N.decode, liquidity_baking_sunset_level: Int32.decode, liquidity_baking_escape_ema_threshold: Int32.decode}, {order: ['bootstrap_accounts', 'bootstrap_contracts', 'commitments', 'security_deposit_ramp_up_cycles', 'no_reward_cycles', 'preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'blocks_per_roll_snapshot', 'blocks_per_voting_period', 'time_between_blocks', 'endorsers_per_block', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'tokens_per_roll', 'michelson_maximum_type_size', 'seed_nonce_revelation_tip', 'origination_size', 'block_security_deposit', 'endorsement_security_deposit', 'baking_reward_per_endorsement', 'endorsement_reward', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'initial_endorsers', 'delay_per_missing_endorsement', 'minimal_block_delay', 'liquidity_baking_subsidy', 'liquidity_baking_sunset_level', 'liquidity_baking_escape_ema_threshold']})(p);
}
