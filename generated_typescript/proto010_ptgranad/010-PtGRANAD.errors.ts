import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
export type Proto010PtGRANADErrors = Dynamic<U8String,width.Uint30>;
export class CGRIDClass__Proto010PtGRANADErrors extends Box<Proto010PtGRANADErrors> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADErrors {
        return new this(Dynamic.decode(U8String.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto010_ptgranad_errors_encoder = (value: Proto010PtGRANADErrors): OutputBytes => {
    return value.encode();
}
export const proto010_ptgranad_errors_decoder = (p: Parser): Proto010PtGRANADErrors => {
    return Dynamic.decode(U8String.decode, width.Uint30)(p);
}
