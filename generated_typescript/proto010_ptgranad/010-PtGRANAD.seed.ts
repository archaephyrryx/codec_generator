import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type Proto010PtGRANADSeed = FixedBytes<32>;
export class CGRIDClass__Proto010PtGRANADSeed extends Box<Proto010PtGRANADSeed> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADSeed {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto010_ptgranad_seed_encoder = (value: Proto010PtGRANADSeed): OutputBytes => {
    return value.encode();
}
export const proto010_ptgranad_seed_decoder = (p: Parser): Proto010PtGRANADSeed => {
    return FixedBytes.decode<32>({len: 32})(p);
}
