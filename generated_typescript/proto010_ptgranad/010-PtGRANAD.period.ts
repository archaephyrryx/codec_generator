import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto010PtGRANADPeriod = Int64;
export class CGRIDClass__Proto010PtGRANADPeriod extends Box<Proto010PtGRANADPeriod> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADPeriod {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto010_ptgranad_period_encoder = (value: Proto010PtGRANADPeriod): OutputBytes => {
    return value.encode();
}
export const proto010_ptgranad_period_decoder = (p: Parser): Proto010PtGRANADPeriod => {
    return Int64.decode(p);
}
