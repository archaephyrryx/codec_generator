import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto010_PtGRANADBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto010_PtGRANADBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
export type Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto010PtGRANADBlockHeaderContents = { priority: Uint16, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto010_PtGRANADBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_escape_vote: Bool };
export class CGRIDClass__Proto010PtGRANADBlockHeaderContents extends Box<Proto010PtGRANADBlockHeaderContents> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADBlockHeaderContents {
        return new this(record_decoder<Proto010PtGRANADBlockHeaderContents>({priority: Uint16.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto010_PtGRANADBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_escape_vote: Bool.decode}, {order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote']})(p));
    };
    get encodeLength(): number {
        return (this.value.priority.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_escape_vote.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.priority.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_escape_vote.writeTarget(tgt));
    }
}
export const proto010_ptgranad_block_header_contents_encoder = (value: Proto010PtGRANADBlockHeaderContents): OutputBytes => {
    return record_encoder({order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote']})(value);
}
export const proto010_ptgranad_block_header_contents_decoder = (p: Parser): Proto010PtGRANADBlockHeaderContents => {
    return record_decoder<Proto010PtGRANADBlockHeaderContents>({priority: Uint16.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto010_PtGRANADBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_escape_vote: Bool.decode}, {order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote']})(p);
}
