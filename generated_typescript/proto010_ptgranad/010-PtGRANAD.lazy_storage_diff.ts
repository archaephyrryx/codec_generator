import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__sapling_state generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__sapling_state
export class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__sapling_state extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__sapling_state> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['id', 'diff']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__sapling_state {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__sapling_state>({id: Z.decode, diff: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff.decode}, {order: ['id', 'diff']})(p));
    };
    get encodeLength(): number {
        return (this.value.id.encodeLength +  this.value.diff.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.id.writeTarget(tgt) +  this.value.diff.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__big_map generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__big_map
export class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__big_map extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__big_map> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['id', 'diff']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__big_map {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__big_map>({id: Z.decode, diff: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff.decode}, {order: ['id', 'diff']})(p));
    };
    get encodeLength(): number {
        return (this.value.id.encodeLength +  this.value.diff.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.id.writeTarget(tgt) +  this.value.diff.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update
export class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update>({action: Unit.decode, updates: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates.decode}, {order: ['action', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove
export class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove>({action: Unit.decode}, {order: ['action']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy
export class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'source', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy>({action: Unit.decode, source: Z.decode, updates: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates.decode}, {order: ['action', 'source', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.source.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.source.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc
export class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates', 'memo_size']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc>({action: Unit.decode, updates: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates.decode, memo_size: Uint16.decode}, {order: ['action', 'updates', 'memo_size']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength +  this.value.memo_size.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt) +  this.value.memo_size.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__update generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__update
export class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__update extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__update {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__update>({action: Unit.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['action', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove
export class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove>({action: Unit.decode}, {order: ['action']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy
export class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'source', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy>({action: Unit.decode, source: Z.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['action', 'source', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.source.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.source.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc
export class CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates', 'key_type', 'value_type']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc>({action: Unit.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq.decode), width.Uint30), key_type: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode, value_type: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode}, {order: ['action', 'updates', 'key_type', 'value_type']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength +  this.value.key_type.encodeLength +  this.value.value_type.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt) +  this.value.key_type.writeTarget(tgt) +  this.value.value_type.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__String generated for MichelineProto010PtGRANADMichelsonV1Expression__String
export class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__String extends Box<MichelineProto010PtGRANADMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__String {
        return new this(record_decoder<MichelineProto010PtGRANADMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Sequence generated for MichelineProto010PtGRANADMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Sequence extends Box<MichelineProto010PtGRANADMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__generic generated for MichelineProto010PtGRANADMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__generic extends Box<MichelineProto010PtGRANADMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineProto010PtGRANADMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Int generated for MichelineProto010PtGRANADMichelsonV1Expression__Int
export class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Int extends Box<MichelineProto010PtGRANADMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineProto010PtGRANADMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Bytes generated for MichelineProto010PtGRANADMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Bytes extends Box<MichelineProto010PtGRANADMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineProto010PtGRANADMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__sapling_state = { id: Z, diff: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__big_map = { id: Z, diff: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update = { action: Unit, updates: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove = { action: Unit };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy = { action: Unit, source: Z, updates: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc = { action: Unit, updates: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates, memo_size: Uint16 };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__update = { action: Unit, updates: Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq>,width.Uint30> };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove = { action: Unit };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy = { action: Unit, source: Z, updates: Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq>,width.Uint30> };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc = { action: Unit, updates: Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq>,width.Uint30>, key_type: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression, value_type: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression };
export type MichelineProto010PtGRANADMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineProto010PtGRANADMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression>,width.Uint30>;
export type MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives };
export type MichelineProto010PtGRANADMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression, arg2: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression, arg2: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression };
export type MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives, arg: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives, arg: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression };
export type MichelineProto010PtGRANADMichelsonV1Expression__Int = { int: Z };
export type MichelineProto010PtGRANADMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
// Class CGRIDClass__SaplingTransactionCiphertext generated for SaplingTransactionCiphertext
export class CGRIDClass__SaplingTransactionCiphertext extends Box<SaplingTransactionCiphertext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionCiphertext {
        return new this(record_decoder<SaplingTransactionCiphertext>({cv: FixedBytes.decode<32>({len: 32}), epk: FixedBytes.decode<32>({len: 32}), payload_enc: Dynamic.decode(Bytes.decode, width.Uint30), nonce_enc: FixedBytes.decode<24>({len: 24}), payload_out: FixedBytes.decode<80>({len: 80}), nonce_out: FixedBytes.decode<24>({len: 24})}, {order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(p));
    };
    get encodeLength(): number {
        return (this.value.cv.encodeLength +  this.value.epk.encodeLength +  this.value.payload_enc.encodeLength +  this.value.nonce_enc.encodeLength +  this.value.payload_out.encodeLength +  this.value.nonce_out.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cv.writeTarget(tgt) +  this.value.epk.writeTarget(tgt) +  this.value.payload_enc.writeTarget(tgt) +  this.value.nonce_enc.writeTarget(tgt) +  this.value.payload_out.writeTarget(tgt) +  this.value.nonce_out.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives generated for Proto010PtGRANADMichelsonV1Primitives
export class CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives extends Box<Proto010PtGRANADMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<Proto010PtGRANADMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is Proto010PtGRANADMichelsonV1Primitives => (Object.values(Proto010PtGRANADMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff
export function proto010ptgranadlazystoragediffdenestdyndenestseqsaplingstatediff_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff,Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff> {
    function f(disc: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.update: return CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update.decode;
            case CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.remove: return CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove.decode;
            case CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.copy: return CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy.decode;
            case CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.alloc: return CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff => Object.values(CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff>, Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadlazystoragediffdenestdyndenestseqsaplingstatediff_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff
export function proto010ptgranadlazystoragediffdenestdyndenestseqbigmapdiff_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff,Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff> {
    function f(disc: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff.update: return CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__update.decode;
            case CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff.remove: return CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove.decode;
            case CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff.copy: return CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy.decode;
            case CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff.alloc: return CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff => Object.values(CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff>, Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadlazystoragediffdenestdyndenestseqbigmapdiff_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq generated for Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq
export function proto010ptgranadlazystoragediffdenestdyndenestseq_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq,Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq> {
    function f(disc: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq.big_map: return CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__big_map.decode;
            case CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq.sapling_state: return CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__sapling_state.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq => Object.values(CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq extends Box<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq>, Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadlazystoragediffdenestdyndenestseq_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression generated for MichelineProto010PtGRANADMichelsonV1Expression
export function michelineproto010ptgranadmichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression,MichelineProto010PtGRANADMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Int: return CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.String: return CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Sequence: return CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Bytes: return CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression => Object.values(CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression extends Box<MichelineProto010PtGRANADMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineProto010PtGRANADMichelsonV1Expression>, MichelineProto010PtGRANADMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelineproto010ptgranadmichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export type SaplingTransactionCiphertext = { cv: FixedBytes<32>, epk: FixedBytes<32>, payload_enc: Dynamic<Bytes,width.Uint30>, nonce_enc: FixedBytes<24>, payload_out: FixedBytes<80>, nonce_out: FixedBytes<24> };
export enum CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression {
    Int: CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineProto010PtGRANADMichelsonV1Expression__Bytes
}
export type MichelineProto010PtGRANADMichelsonV1Expression = { kind: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Int, value: CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.String, value: CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineProto010PtGRANADMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineProto010PtGRANADMichelsonV1Expression['Bytes'] };
export enum Proto010PtGRANADMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117,
    LEVEL = 118,
    SELF_ADDRESS = 119,
    never = 120,
    NEVER = 121,
    UNPAIR = 122,
    VOTING_POWER = 123,
    TOTAL_VOTING_POWER = 124,
    KECCAK = 125,
    SHA3 = 126,
    PAIRING_CHECK = 127,
    bls12_381_g1 = 128,
    bls12_381_g2 = 129,
    bls12_381_fr = 130,
    sapling_state = 131,
    sapling_transaction = 132,
    SAPLING_EMPTY_STATE = 133,
    SAPLING_VERIFY_UPDATE = 134,
    ticket = 135,
    TICKET = 136,
    READ_TICKET = 137,
    SPLIT_TICKET = 138,
    JOIN_TICKETS = 139,
    GET_AND_UPDATE = 140
}
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export enum CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff{
    update = 0,
    remove = 1,
    copy = 2,
    alloc = 3
}
export interface CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff {
    update: CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update,
    remove: CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove,
    copy: CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy,
    alloc: CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc
}
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff = { kind: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.update, value: CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['update'] } | { kind: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.remove, value: CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['remove'] } | { kind: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.copy, value: CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['copy'] } | { kind: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.alloc, value: CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['alloc'] };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression> };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression> };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto010_PtGRANADMichelson_v1Expression> };
export enum CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff{
    update = 0,
    remove = 1,
    copy = 2,
    alloc = 3
}
export interface CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff {
    update: CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__update,
    remove: CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove,
    copy: CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy,
    alloc: CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc
}
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff = { kind: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff.update, value: CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff['update'] } | { kind: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff.remove, value: CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff['remove'] } | { kind: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff.copy, value: CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff['copy'] } | { kind: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff.alloc, value: CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeqBigMapDiff['alloc'] };
export enum CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq{
    big_map = 0,
    sapling_state = 1
}
export interface CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq {
    big_map: CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__big_map,
    sapling_state: CGRIDClass__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq__sapling_state
}
export type Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq = { kind: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq.big_map, value: CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq['big_map'] } | { kind: CGRIDTag__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq.sapling_state, value: CGRIDMap__Proto010PtGRANADLazyStorageDiffDenestDynDenestSeq['sapling_state'] };
export type Proto010PtGRANADLazyStorageDiff = Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq>,width.Uint30>;
export class CGRIDClass__Proto010PtGRANADLazyStorageDiff extends Box<Proto010PtGRANADLazyStorageDiff> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADLazyStorageDiff {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto010_ptgranad_lazy_storage_diff_encoder = (value: Proto010PtGRANADLazyStorageDiff): OutputBytes => {
    return value.encode();
}
export const proto010_ptgranad_lazy_storage_diff_decoder = (p: Parser): Proto010PtGRANADLazyStorageDiff => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADLazy_storage_diff_denest_dyn_denest_seq.decode), width.Uint30)(p);
}
