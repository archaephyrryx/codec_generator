import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto010PtGRANADVotingPeriodKind__exploration generated for Proto010PtGRANADVotingPeriodKind__exploration
export class CGRIDClass__Proto010PtGRANADVotingPeriodKind__exploration extends Box<Proto010PtGRANADVotingPeriodKind__exploration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADVotingPeriodKind__exploration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADVotingPeriodKind__Proposal generated for Proto010PtGRANADVotingPeriodKind__Proposal
export class CGRIDClass__Proto010PtGRANADVotingPeriodKind__Proposal extends Box<Proto010PtGRANADVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADVotingPeriodKind__Promotion generated for Proto010PtGRANADVotingPeriodKind__Promotion
export class CGRIDClass__Proto010PtGRANADVotingPeriodKind__Promotion extends Box<Proto010PtGRANADVotingPeriodKind__Promotion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADVotingPeriodKind__Promotion {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADVotingPeriodKind__Cooldown generated for Proto010PtGRANADVotingPeriodKind__Cooldown
export class CGRIDClass__Proto010PtGRANADVotingPeriodKind__Cooldown extends Box<Proto010PtGRANADVotingPeriodKind__Cooldown> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADVotingPeriodKind__Cooldown {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADVotingPeriodKind__Adoption generated for Proto010PtGRANADVotingPeriodKind__Adoption
export class CGRIDClass__Proto010PtGRANADVotingPeriodKind__Adoption extends Box<Proto010PtGRANADVotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADVotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto010PtGRANADVotingPeriodKind__exploration = Unit;
export type Proto010PtGRANADVotingPeriodKind__Proposal = Unit;
export type Proto010PtGRANADVotingPeriodKind__Promotion = Unit;
export type Proto010PtGRANADVotingPeriodKind__Cooldown = Unit;
export type Proto010PtGRANADVotingPeriodKind__Adoption = Unit;
export enum CGRIDTag__Proto010PtGRANADVotingPeriodKind{
    Proposal = 0,
    exploration = 1,
    Cooldown = 2,
    Promotion = 3,
    Adoption = 4
}
export interface CGRIDMap__Proto010PtGRANADVotingPeriodKind {
    Proposal: CGRIDClass__Proto010PtGRANADVotingPeriodKind__Proposal,
    exploration: CGRIDClass__Proto010PtGRANADVotingPeriodKind__exploration,
    Cooldown: CGRIDClass__Proto010PtGRANADVotingPeriodKind__Cooldown,
    Promotion: CGRIDClass__Proto010PtGRANADVotingPeriodKind__Promotion,
    Adoption: CGRIDClass__Proto010PtGRANADVotingPeriodKind__Adoption
}
export type Proto010PtGRANADVotingPeriodKind = { kind: CGRIDTag__Proto010PtGRANADVotingPeriodKind.Proposal, value: CGRIDMap__Proto010PtGRANADVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto010PtGRANADVotingPeriodKind.exploration, value: CGRIDMap__Proto010PtGRANADVotingPeriodKind['exploration'] } | { kind: CGRIDTag__Proto010PtGRANADVotingPeriodKind.Cooldown, value: CGRIDMap__Proto010PtGRANADVotingPeriodKind['Cooldown'] } | { kind: CGRIDTag__Proto010PtGRANADVotingPeriodKind.Promotion, value: CGRIDMap__Proto010PtGRANADVotingPeriodKind['Promotion'] } | { kind: CGRIDTag__Proto010PtGRANADVotingPeriodKind.Adoption, value: CGRIDMap__Proto010PtGRANADVotingPeriodKind['Adoption'] };
export function proto010ptgranadvotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADVotingPeriodKind,Proto010PtGRANADVotingPeriodKind> {
    function f(disc: CGRIDTag__Proto010PtGRANADVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADVotingPeriodKind.Proposal: return CGRIDClass__Proto010PtGRANADVotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto010PtGRANADVotingPeriodKind.exploration: return CGRIDClass__Proto010PtGRANADVotingPeriodKind__exploration.decode;
            case CGRIDTag__Proto010PtGRANADVotingPeriodKind.Cooldown: return CGRIDClass__Proto010PtGRANADVotingPeriodKind__Cooldown.decode;
            case CGRIDTag__Proto010PtGRANADVotingPeriodKind.Promotion: return CGRIDClass__Proto010PtGRANADVotingPeriodKind__Promotion.decode;
            case CGRIDTag__Proto010PtGRANADVotingPeriodKind.Adoption: return CGRIDClass__Proto010PtGRANADVotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADVotingPeriodKind => Object.values(CGRIDTag__Proto010PtGRANADVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010PtGRANADVotingPeriodKind extends Box<Proto010PtGRANADVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADVotingPeriodKind>, Proto010PtGRANADVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADVotingPeriodKind {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadvotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto010_ptgranad_voting_period_kind_encoder = (value: Proto010PtGRANADVotingPeriodKind): OutputBytes => {
    return variant_encoder<KindOf<Proto010PtGRANADVotingPeriodKind>, Proto010PtGRANADVotingPeriodKind>(width.Uint8)(value);
}
export const proto010_ptgranad_voting_period_kind_decoder = (p: Parser): Proto010PtGRANADVotingPeriodKind => {
    return variant_decoder(width.Uint8)(proto010ptgranadvotingperiodkind_mkDecoder())(p);
}
