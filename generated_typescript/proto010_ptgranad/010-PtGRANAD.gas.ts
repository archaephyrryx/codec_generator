import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto010PtGRANADGas__Unaccounted generated for Proto010PtGRANADGas__Unaccounted
export class CGRIDClass__Proto010PtGRANADGas__Unaccounted extends Box<Proto010PtGRANADGas__Unaccounted> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADGas__Unaccounted {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADGas__Limited generated for Proto010PtGRANADGas__Limited
export class CGRIDClass__Proto010PtGRANADGas__Limited extends Box<Proto010PtGRANADGas__Limited> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADGas__Limited {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto010PtGRANADGas__Unaccounted = Unit;
export type Proto010PtGRANADGas__Limited = Z;
export enum CGRIDTag__Proto010PtGRANADGas{
    Limited = 0,
    Unaccounted = 1
}
export interface CGRIDMap__Proto010PtGRANADGas {
    Limited: CGRIDClass__Proto010PtGRANADGas__Limited,
    Unaccounted: CGRIDClass__Proto010PtGRANADGas__Unaccounted
}
export type Proto010PtGRANADGas = { kind: CGRIDTag__Proto010PtGRANADGas.Limited, value: CGRIDMap__Proto010PtGRANADGas['Limited'] } | { kind: CGRIDTag__Proto010PtGRANADGas.Unaccounted, value: CGRIDMap__Proto010PtGRANADGas['Unaccounted'] };
export function proto010ptgranadgas_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADGas,Proto010PtGRANADGas> {
    function f(disc: CGRIDTag__Proto010PtGRANADGas) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADGas.Limited: return CGRIDClass__Proto010PtGRANADGas__Limited.decode;
            case CGRIDTag__Proto010PtGRANADGas.Unaccounted: return CGRIDClass__Proto010PtGRANADGas__Unaccounted.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADGas => Object.values(CGRIDTag__Proto010PtGRANADGas).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010PtGRANADGas extends Box<Proto010PtGRANADGas> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADGas>, Proto010PtGRANADGas>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADGas {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadgas_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto010_ptgranad_gas_encoder = (value: Proto010PtGRANADGas): OutputBytes => {
    return variant_encoder<KindOf<Proto010PtGRANADGas>, Proto010PtGRANADGas>(width.Uint8)(value);
}
export const proto010_ptgranad_gas_decoder = (p: Parser): Proto010PtGRANADGas => {
    return variant_decoder(width.Uint8)(proto010ptgranadgas_mkDecoder())(p);
}
