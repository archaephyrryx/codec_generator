import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Subsidy generated for Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Subsidy
export class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Subsidy extends Box<Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Subsidy> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Subsidy {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration generated for Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration
export class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration extends Box<Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Block_application generated for Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Block_application
export class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Block_application extends Box<Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Block_application> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Block_application {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Rewards generated for Proto010PtGRANADOperationMetadataAlphaBalance__Rewards
export class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Rewards extends Box<Proto010PtGRANADOperationMetadataAlphaBalance__Rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Rewards {
        return new this(record_decoder<Proto010PtGRANADOperationMetadataAlphaBalance__Rewards>({category: Unit.decode, delegate: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Rewards_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Fees generated for Proto010PtGRANADOperationMetadataAlphaBalance__Fees
export class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Fees extends Box<Proto010PtGRANADOperationMetadataAlphaBalance__Fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Fees {
        return new this(record_decoder<Proto010PtGRANADOperationMetadataAlphaBalance__Fees>({category: Unit.decode, delegate: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Fees_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Deposits generated for Proto010PtGRANADOperationMetadataAlphaBalance__Deposits
export class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Deposits extends Box<Proto010PtGRANADOperationMetadataAlphaBalance__Deposits> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Deposits {
        return new this(record_decoder<Proto010PtGRANADOperationMetadataAlphaBalance__Deposits>({category: Unit.decode, delegate: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Deposits_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Contract generated for Proto010PtGRANADOperationMetadataAlphaBalance__Contract
export class CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Contract extends Box<Proto010PtGRANADOperationMetadataAlphaBalance__Contract> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Contract {
        return new this(record_decoder<Proto010PtGRANADOperationMetadataAlphaBalance__Contract>({contract: CGRIDClass__Proto010_PtGRANADContract_id.decode, change: Int64.decode, origin: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['contract', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADContractId__Originated generated for Proto010PtGRANADContractId__Originated
export class CGRIDClass__Proto010PtGRANADContractId__Originated extends Box<Proto010PtGRANADContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADContractId__Implicit generated for Proto010PtGRANADContractId__Implicit
export class CGRIDClass__Proto010PtGRANADContractId__Implicit extends Box<Proto010PtGRANADContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADContractId__Implicit {
        return new this(record_decoder<Proto010PtGRANADContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Subsidy = Unit;
export type Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration = Unit;
export type Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Block_application = Unit;
export type Proto010PtGRANADOperationMetadataAlphaBalance__Rewards = { category: Unit, delegate: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Rewards_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaUpdate_origin_origin };
export type Proto010PtGRANADOperationMetadataAlphaBalance__Fees = { category: Unit, delegate: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Fees_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaUpdate_origin_origin };
export type Proto010PtGRANADOperationMetadataAlphaBalance__Deposits = { category: Unit, delegate: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Deposits_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaUpdate_origin_origin };
export type Proto010PtGRANADOperationMetadataAlphaBalance__Contract = { contract: CGRIDClass__Proto010_PtGRANADContract_id, change: Int64, origin: CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaUpdate_origin_origin };
export type Proto010PtGRANADContractId__Originated = Padded<CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad,1>;
export type Proto010PtGRANADContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaUpdate_origin_origin generated for Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin
export function proto010ptgranadoperationmetadataalphaupdateoriginorigin_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin,Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin> {
    function f(disc: CGRIDTag__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin.Block_application: return CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Block_application.decode;
            case CGRIDTag__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration: return CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration.decode;
            case CGRIDTag__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin.Subsidy: return CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Subsidy.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin => Object.values(CGRIDTag__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaUpdate_origin_origin extends Box<Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin>, Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaUpdate_origin_origin {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadoperationmetadataalphaupdateoriginorigin_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Rewards_delegate generated for Proto010PtGRANADOperationMetadataAlphaBalanceRewardsDelegate
export class CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Rewards_delegate extends Box<Proto010PtGRANADOperationMetadataAlphaBalanceRewardsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Rewards_delegate {
        return new this(record_decoder<Proto010PtGRANADOperationMetadataAlphaBalanceRewardsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Fees_delegate generated for Proto010PtGRANADOperationMetadataAlphaBalanceFeesDelegate
export class CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Fees_delegate extends Box<Proto010PtGRANADOperationMetadataAlphaBalanceFeesDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Fees_delegate {
        return new this(record_decoder<Proto010PtGRANADOperationMetadataAlphaBalanceFeesDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Deposits_delegate generated for Proto010PtGRANADOperationMetadataAlphaBalanceDepositsDelegate
export class CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Deposits_delegate extends Box<Proto010PtGRANADOperationMetadataAlphaBalanceDepositsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance_Deposits_delegate {
        return new this(record_decoder<Proto010PtGRANADOperationMetadataAlphaBalanceDepositsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance generated for Proto010PtGRANADOperationMetadataAlphaBalance
export function proto010ptgranadoperationmetadataalphabalance_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance,Proto010PtGRANADOperationMetadataAlphaBalance> {
    function f(disc: CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance.Contract: return CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Contract.decode;
            case CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance.Rewards: return CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Rewards.decode;
            case CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance.Fees: return CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Fees.decode;
            case CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance.Deposits: return CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Deposits.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance => Object.values(CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance extends Box<Proto010PtGRANADOperationMetadataAlphaBalance> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADOperationMetadataAlphaBalance>, Proto010PtGRANADOperationMetadataAlphaBalance>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadoperationmetadataalphabalance_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad generated for Proto010PtGRANADContractIdOriginatedDenestPad
export class CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad extends Box<Proto010PtGRANADContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto010PtGRANADContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADContract_id generated for Proto010PtGRANADContractId
export function proto010ptgranadcontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADContractId,Proto010PtGRANADContractId> {
    function f(disc: CGRIDTag__Proto010PtGRANADContractId) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADContractId.Implicit: return CGRIDClass__Proto010PtGRANADContractId__Implicit.decode;
            case CGRIDTag__Proto010PtGRANADContractId.Originated: return CGRIDClass__Proto010PtGRANADContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADContractId => Object.values(CGRIDTag__Proto010PtGRANADContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADContract_id extends Box<Proto010PtGRANADContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADContractId>, Proto010PtGRANADContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADContract_id {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadcontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin{
    Block_application = 0,
    Protocol_migration = 1,
    Subsidy = 2
}
export interface CGRIDMap__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin {
    Block_application: CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Block_application,
    Protocol_migration: CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration,
    Subsidy: CGRIDClass__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin__Subsidy
}
export type Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin = { kind: CGRIDTag__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin.Block_application, value: CGRIDMap__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin['Block_application'] } | { kind: CGRIDTag__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration, value: CGRIDMap__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin['Protocol_migration'] } | { kind: CGRIDTag__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin.Subsidy, value: CGRIDMap__Proto010PtGRANADOperationMetadataAlphaUpdateOriginOrigin['Subsidy'] };
export type Proto010PtGRANADOperationMetadataAlphaBalanceRewardsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADOperationMetadataAlphaBalanceFeesDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADOperationMetadataAlphaBalanceDepositsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance{
    Contract = 0,
    Rewards = 1,
    Fees = 2,
    Deposits = 3
}
export interface CGRIDMap__Proto010PtGRANADOperationMetadataAlphaBalance {
    Contract: CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Contract,
    Rewards: CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Rewards,
    Fees: CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Fees,
    Deposits: CGRIDClass__Proto010PtGRANADOperationMetadataAlphaBalance__Deposits
}
export type Proto010PtGRANADOperationMetadataAlphaBalance = { kind: CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance.Contract, value: CGRIDMap__Proto010PtGRANADOperationMetadataAlphaBalance['Contract'] } | { kind: CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance.Rewards, value: CGRIDMap__Proto010PtGRANADOperationMetadataAlphaBalance['Rewards'] } | { kind: CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance.Fees, value: CGRIDMap__Proto010PtGRANADOperationMetadataAlphaBalance['Fees'] } | { kind: CGRIDTag__Proto010PtGRANADOperationMetadataAlphaBalance.Deposits, value: CGRIDMap__Proto010PtGRANADOperationMetadataAlphaBalance['Deposits'] };
export type Proto010PtGRANADContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto010PtGRANADContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto010PtGRANADContractId {
    Implicit: CGRIDClass__Proto010PtGRANADContractId__Implicit,
    Originated: CGRIDClass__Proto010PtGRANADContractId__Originated
}
export type Proto010PtGRANADContractId = { kind: CGRIDTag__Proto010PtGRANADContractId.Implicit, value: CGRIDMap__Proto010PtGRANADContractId['Implicit'] } | { kind: CGRIDTag__Proto010PtGRANADContractId.Originated, value: CGRIDMap__Proto010PtGRANADContractId['Originated'] };
export type Proto010PtGRANADReceiptBalanceUpdates = Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance>,width.Uint30>;
export class CGRIDClass__Proto010PtGRANADReceiptBalanceUpdates extends Box<Proto010PtGRANADReceiptBalanceUpdates> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADReceiptBalanceUpdates {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto010_ptgranad_receipt_balance_updates_encoder = (value: Proto010PtGRANADReceiptBalanceUpdates): OutputBytes => {
    return value.encode();
}
export const proto010_ptgranad_receipt_balance_updates_decoder = (p: Parser): Proto010PtGRANADReceiptBalanceUpdates => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADOperation_metadataAlphaBalance.decode), width.Uint30)(p);
}
