import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Transaction generated for Proto010PtGRANADOperationAlphaInternalOperationRhs__Transaction
export class CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Transaction extends Box<Proto010PtGRANADOperationAlphaInternalOperationRhs__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Transaction {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaInternalOperationRhs__Transaction>({amount: N.decode, destination: CGRIDClass__Proto010_PtGRANADContract_id.decode, parameters: Option.decode(CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Transaction_parameters.decode)}, {order: ['amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Reveal generated for Proto010PtGRANADOperationAlphaInternalOperationRhs__Reveal
export class CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Reveal extends Box<Proto010PtGRANADOperationAlphaInternalOperationRhs__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Reveal {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaInternalOperationRhs__Reveal>({public_key: CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Reveal_public_key.decode}, {order: ['public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Origination generated for Proto010PtGRANADOperationAlphaInternalOperationRhs__Origination
export class CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Origination extends Box<Proto010PtGRANADOperationAlphaInternalOperationRhs__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Origination {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaInternalOperationRhs__Origination>({balance: N.decode, delegate: Option.decode(CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Origination_delegate.decode), script: CGRIDClass__Proto010_PtGRANADScriptedContracts.decode}, {order: ['balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Delegation generated for Proto010PtGRANADOperationAlphaInternalOperationRhs__Delegation
export class CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Delegation extends Box<Proto010PtGRANADOperationAlphaInternalOperationRhs__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Delegation {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaInternalOperationRhs__Delegation>({delegate: Option.decode(CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Delegation_delegate.decode)}, {order: ['delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint__set_delegate generated for Proto010PtGRANADEntrypoint__set_delegate
export class CGRIDClass__Proto010PtGRANADEntrypoint__set_delegate extends Box<Proto010PtGRANADEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint__root generated for Proto010PtGRANADEntrypoint__root
export class CGRIDClass__Proto010PtGRANADEntrypoint__root extends Box<Proto010PtGRANADEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint__remove_delegate generated for Proto010PtGRANADEntrypoint__remove_delegate
export class CGRIDClass__Proto010PtGRANADEntrypoint__remove_delegate extends Box<Proto010PtGRANADEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint__named generated for Proto010PtGRANADEntrypoint__named
export class CGRIDClass__Proto010PtGRANADEntrypoint__named extends Box<Proto010PtGRANADEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint___do generated for Proto010PtGRANADEntrypoint___do
export class CGRIDClass__Proto010PtGRANADEntrypoint___do extends Box<Proto010PtGRANADEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint___default generated for Proto010PtGRANADEntrypoint___default
export class CGRIDClass__Proto010PtGRANADEntrypoint___default extends Box<Proto010PtGRANADEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADContractId__Originated generated for Proto010PtGRANADContractId__Originated
export class CGRIDClass__Proto010PtGRANADContractId__Originated extends Box<Proto010PtGRANADContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADContractId__Implicit generated for Proto010PtGRANADContractId__Implicit
export class CGRIDClass__Proto010PtGRANADContractId__Implicit extends Box<Proto010PtGRANADContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADContractId__Implicit {
        return new this(record_decoder<Proto010PtGRANADContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto010PtGRANADOperationAlphaInternalOperationRhs__Transaction = { amount: N, destination: CGRIDClass__Proto010_PtGRANADContract_id, parameters: Option<CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Transaction_parameters> };
export type Proto010PtGRANADOperationAlphaInternalOperationRhs__Reveal = { public_key: CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Reveal_public_key };
export type Proto010PtGRANADOperationAlphaInternalOperationRhs__Origination = { balance: N, delegate: Option<CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Origination_delegate>, script: CGRIDClass__Proto010_PtGRANADScriptedContracts };
export type Proto010PtGRANADOperationAlphaInternalOperationRhs__Delegation = { delegate: Option<CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Delegation_delegate> };
export type Proto010PtGRANADEntrypoint__set_delegate = Unit;
export type Proto010PtGRANADEntrypoint__root = Unit;
export type Proto010PtGRANADEntrypoint__remove_delegate = Unit;
export type Proto010PtGRANADEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto010PtGRANADEntrypoint___do = Unit;
export type Proto010PtGRANADEntrypoint___default = Unit;
export type Proto010PtGRANADContractId__Originated = Padded<CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad,1>;
export type Proto010PtGRANADContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADScriptedContracts generated for Proto010PtGRANADScriptedContracts
export class CGRIDClass__Proto010_PtGRANADScriptedContracts extends Box<Proto010PtGRANADScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADScriptedContracts {
        return new this(record_decoder<Proto010PtGRANADScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_rhs generated for Proto010PtGRANADOperationAlphaInternalOperationRhs
export function proto010ptgranadoperationalphainternaloperationrhs_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs,Proto010PtGRANADOperationAlphaInternalOperationRhs> {
    function f(disc: CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs.Reveal: return CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Reveal.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs.Transaction: return CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Transaction.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs.Origination: return CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Origination.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs.Delegation: return CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Delegation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs => Object.values(CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_rhs extends Box<Proto010PtGRANADOperationAlphaInternalOperationRhs> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADOperationAlphaInternalOperationRhs>, Proto010PtGRANADOperationAlphaInternalOperationRhs>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_rhs {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadoperationalphainternaloperationrhs_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Transaction_parameters generated for Proto010PtGRANADOperationAlphaInternalOperationTransactionParameters
export class CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Transaction_parameters extends Box<Proto010PtGRANADOperationAlphaInternalOperationTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Transaction_parameters {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaInternalOperationTransactionParameters>({entrypoint: CGRIDClass__Proto010_PtGRANADEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Reveal_public_key generated for Proto010PtGRANADOperationAlphaInternalOperationRevealPublicKey
export class CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Reveal_public_key extends Box<Proto010PtGRANADOperationAlphaInternalOperationRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Reveal_public_key {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaInternalOperationRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Origination_delegate generated for Proto010PtGRANADOperationAlphaInternalOperationOriginationDelegate
export class CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Origination_delegate extends Box<Proto010PtGRANADOperationAlphaInternalOperationOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Origination_delegate {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaInternalOperationOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Delegation_delegate generated for Proto010PtGRANADOperationAlphaInternalOperationDelegationDelegate
export class CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Delegation_delegate extends Box<Proto010PtGRANADOperationAlphaInternalOperationDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_Delegation_delegate {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaInternalOperationDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADEntrypoint generated for Proto010PtGRANADEntrypoint
export function proto010ptgranadentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADEntrypoint,Proto010PtGRANADEntrypoint> {
    function f(disc: CGRIDTag__Proto010PtGRANADEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADEntrypoint._default: return CGRIDClass__Proto010PtGRANADEntrypoint___default.decode;
            case CGRIDTag__Proto010PtGRANADEntrypoint.root: return CGRIDClass__Proto010PtGRANADEntrypoint__root.decode;
            case CGRIDTag__Proto010PtGRANADEntrypoint._do: return CGRIDClass__Proto010PtGRANADEntrypoint___do.decode;
            case CGRIDTag__Proto010PtGRANADEntrypoint.set_delegate: return CGRIDClass__Proto010PtGRANADEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto010PtGRANADEntrypoint.remove_delegate: return CGRIDClass__Proto010PtGRANADEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto010PtGRANADEntrypoint.named: return CGRIDClass__Proto010PtGRANADEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADEntrypoint => Object.values(CGRIDTag__Proto010PtGRANADEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADEntrypoint extends Box<Proto010PtGRANADEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADEntrypoint>, Proto010PtGRANADEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad generated for Proto010PtGRANADContractIdOriginatedDenestPad
export class CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad extends Box<Proto010PtGRANADContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto010PtGRANADContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADContract_id generated for Proto010PtGRANADContractId
export function proto010ptgranadcontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADContractId,Proto010PtGRANADContractId> {
    function f(disc: CGRIDTag__Proto010PtGRANADContractId) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADContractId.Implicit: return CGRIDClass__Proto010PtGRANADContractId__Implicit.decode;
            case CGRIDTag__Proto010PtGRANADContractId.Originated: return CGRIDClass__Proto010PtGRANADContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADContractId => Object.values(CGRIDTag__Proto010PtGRANADContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADContract_id extends Box<Proto010PtGRANADContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADContractId>, Proto010PtGRANADContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADContract_id {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadcontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type Proto010PtGRANADScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export enum CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs{
    Reveal = 0,
    Transaction = 1,
    Origination = 2,
    Delegation = 3
}
export interface CGRIDMap__Proto010PtGRANADOperationAlphaInternalOperationRhs {
    Reveal: CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Reveal,
    Transaction: CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Transaction,
    Origination: CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Origination,
    Delegation: CGRIDClass__Proto010PtGRANADOperationAlphaInternalOperationRhs__Delegation
}
export type Proto010PtGRANADOperationAlphaInternalOperationRhs = { kind: CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs.Reveal, value: CGRIDMap__Proto010PtGRANADOperationAlphaInternalOperationRhs['Reveal'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs.Transaction, value: CGRIDMap__Proto010PtGRANADOperationAlphaInternalOperationRhs['Transaction'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs.Origination, value: CGRIDMap__Proto010PtGRANADOperationAlphaInternalOperationRhs['Origination'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaInternalOperationRhs.Delegation, value: CGRIDMap__Proto010PtGRANADOperationAlphaInternalOperationRhs['Delegation'] };
export type Proto010PtGRANADOperationAlphaInternalOperationTransactionParameters = { entrypoint: CGRIDClass__Proto010_PtGRANADEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto010PtGRANADOperationAlphaInternalOperationRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto010PtGRANADOperationAlphaInternalOperationOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADOperationAlphaInternalOperationDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto010PtGRANADEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto010PtGRANADEntrypoint {
    _default: CGRIDClass__Proto010PtGRANADEntrypoint___default,
    root: CGRIDClass__Proto010PtGRANADEntrypoint__root,
    _do: CGRIDClass__Proto010PtGRANADEntrypoint___do,
    set_delegate: CGRIDClass__Proto010PtGRANADEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto010PtGRANADEntrypoint__remove_delegate,
    named: CGRIDClass__Proto010PtGRANADEntrypoint__named
}
export type Proto010PtGRANADEntrypoint = { kind: CGRIDTag__Proto010PtGRANADEntrypoint._default, value: CGRIDMap__Proto010PtGRANADEntrypoint['_default'] } | { kind: CGRIDTag__Proto010PtGRANADEntrypoint.root, value: CGRIDMap__Proto010PtGRANADEntrypoint['root'] } | { kind: CGRIDTag__Proto010PtGRANADEntrypoint._do, value: CGRIDMap__Proto010PtGRANADEntrypoint['_do'] } | { kind: CGRIDTag__Proto010PtGRANADEntrypoint.set_delegate, value: CGRIDMap__Proto010PtGRANADEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto010PtGRANADEntrypoint.remove_delegate, value: CGRIDMap__Proto010PtGRANADEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto010PtGRANADEntrypoint.named, value: CGRIDMap__Proto010PtGRANADEntrypoint['named'] };
export type Proto010PtGRANADContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto010PtGRANADContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto010PtGRANADContractId {
    Implicit: CGRIDClass__Proto010PtGRANADContractId__Implicit,
    Originated: CGRIDClass__Proto010PtGRANADContractId__Originated
}
export type Proto010PtGRANADContractId = { kind: CGRIDTag__Proto010PtGRANADContractId.Implicit, value: CGRIDMap__Proto010PtGRANADContractId['Implicit'] } | { kind: CGRIDTag__Proto010PtGRANADContractId.Originated, value: CGRIDMap__Proto010PtGRANADContractId['Originated'] };
export type Proto010PtGRANADOperationInternal = { source: CGRIDClass__Proto010_PtGRANADContract_id, nonce: Uint16, proto010_ptgranad_operation_alpha_internal_operation_rhs: CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_rhs };
export class CGRIDClass__Proto010PtGRANADOperationInternal extends Box<Proto010PtGRANADOperationInternal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'nonce', 'proto010_ptgranad_operation_alpha_internal_operation_rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationInternal {
        return new this(record_decoder<Proto010PtGRANADOperationInternal>({source: CGRIDClass__Proto010_PtGRANADContract_id.decode, nonce: Uint16.decode, proto010_ptgranad_operation_alpha_internal_operation_rhs: CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto010_ptgranad_operation_alpha_internal_operation_rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.nonce.encodeLength +  this.value.proto010_ptgranad_operation_alpha_internal_operation_rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt) +  this.value.proto010_ptgranad_operation_alpha_internal_operation_rhs.writeTarget(tgt));
    }
}
export const proto010_ptgranad_operation_internal_encoder = (value: Proto010PtGRANADOperationInternal): OutputBytes => {
    return record_encoder({order: ['source', 'nonce', 'proto010_ptgranad_operation_alpha_internal_operation_rhs']})(value);
}
export const proto010_ptgranad_operation_internal_decoder = (p: Parser): Proto010PtGRANADOperationInternal => {
    return record_decoder<Proto010PtGRANADOperationInternal>({source: CGRIDClass__Proto010_PtGRANADContract_id.decode, nonce: Uint16.decode, proto010_ptgranad_operation_alpha_internal_operation_rhs: CGRIDClass__Proto010_PtGRANADOperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto010_ptgranad_operation_alpha_internal_operation_rhs']})(p);
}
