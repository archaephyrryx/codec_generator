import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Nullable } from '../../ts_runtime/composite/opt/nullable';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int64, Int8, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Transaction generated for Proto010PtGRANADOperationAlphaContents__Transaction
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Transaction extends Box<Proto010PtGRANADOperationAlphaContents__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Transaction {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Transaction>({source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Transaction_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: N.decode, destination: CGRIDClass__Proto010_PtGRANADContract_id.decode, parameters: Option.decode(CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Transaction_parameters.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Seed_nonce_revelation generated for Proto010PtGRANADOperationAlphaContents__Seed_nonce_revelation
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Seed_nonce_revelation extends Box<Proto010PtGRANADOperationAlphaContents__Seed_nonce_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Seed_nonce_revelation {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Seed_nonce_revelation>({level: Int32.decode, nonce: FixedBytes.decode<32>({len: 32})}, {order: ['level', 'nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Reveal generated for Proto010PtGRANADOperationAlphaContents__Reveal
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Reveal extends Box<Proto010PtGRANADOperationAlphaContents__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Reveal {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Reveal>({source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Reveal_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_key: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Reveal_public_key.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Proposals generated for Proto010PtGRANADOperationAlphaContents__Proposals
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Proposals extends Box<Proto010PtGRANADOperationAlphaContents__Proposals> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposals']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Proposals {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Proposals>({source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Proposals_source.decode, period: Int32.decode, proposals: Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'period', 'proposals']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposals.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposals.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Origination generated for Proto010PtGRANADOperationAlphaContents__Origination
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Origination extends Box<Proto010PtGRANADOperationAlphaContents__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Origination {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Origination>({source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, balance: N.decode, delegate: Option.decode(CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Origination_delegate.decode), script: CGRIDClass__Proto010_PtGRANADScriptedContracts.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Failing_noop generated for Proto010PtGRANADOperationAlphaContents__Failing_noop
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Failing_noop extends Box<Proto010PtGRANADOperationAlphaContents__Failing_noop> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['arbitrary']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Failing_noop {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Failing_noop>({arbitrary: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['arbitrary']})(p));
    };
    get encodeLength(): number {
        return (this.value.arbitrary.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.arbitrary.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Endorsement_with_slot generated for Proto010PtGRANADOperationAlphaContents__Endorsement_with_slot
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Endorsement_with_slot extends Box<Proto010PtGRANADOperationAlphaContents__Endorsement_with_slot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['endorsement', 'slot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Endorsement_with_slot {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Endorsement_with_slot>({endorsement: Dynamic.decode(CGRIDClass__Proto010_PtGRANADInlinedEndorsement.decode, width.Uint30), slot: Uint16.decode}, {order: ['endorsement', 'slot']})(p));
    };
    get encodeLength(): number {
        return (this.value.endorsement.encodeLength +  this.value.slot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.endorsement.writeTarget(tgt) +  this.value.slot.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Endorsement generated for Proto010PtGRANADOperationAlphaContents__Endorsement
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Endorsement extends Box<Proto010PtGRANADOperationAlphaContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Endorsement {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Endorsement>({level: Int32.decode}, {order: ['level']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Double_endorsement_evidence generated for Proto010PtGRANADOperationAlphaContents__Double_endorsement_evidence
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Double_endorsement_evidence extends Box<Proto010PtGRANADOperationAlphaContents__Double_endorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2', 'slot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Double_endorsement_evidence {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Double_endorsement_evidence>({op1: Dynamic.decode(CGRIDClass__Proto010_PtGRANADInlinedEndorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__Proto010_PtGRANADInlinedEndorsement.decode, width.Uint30), slot: Uint16.decode}, {order: ['op1', 'op2', 'slot']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength +  this.value.slot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt) +  this.value.slot.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Double_baking_evidence generated for Proto010PtGRANADOperationAlphaContents__Double_baking_evidence
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Double_baking_evidence extends Box<Proto010PtGRANADOperationAlphaContents__Double_baking_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bh1', 'bh2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Double_baking_evidence {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Double_baking_evidence>({bh1: Dynamic.decode(CGRIDClass__Proto010_PtGRANADBlock_headerAlphaFull_header.decode, width.Uint30), bh2: Dynamic.decode(CGRIDClass__Proto010_PtGRANADBlock_headerAlphaFull_header.decode, width.Uint30)}, {order: ['bh1', 'bh2']})(p));
    };
    get encodeLength(): number {
        return (this.value.bh1.encodeLength +  this.value.bh2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bh1.writeTarget(tgt) +  this.value.bh2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Delegation generated for Proto010PtGRANADOperationAlphaContents__Delegation
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Delegation extends Box<Proto010PtGRANADOperationAlphaContents__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Delegation {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Delegation>({source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Delegation_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, delegate: Option.decode(CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Delegation_delegate.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Ballot generated for Proto010PtGRANADOperationAlphaContents__Ballot
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Ballot extends Box<Proto010PtGRANADOperationAlphaContents__Ballot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposal', 'ballot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Ballot {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Ballot>({source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Ballot_source.decode, period: Int32.decode, proposal: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Ballot_proposal.decode, ballot: Int8.decode}, {order: ['source', 'period', 'proposal', 'ballot']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposal.encodeLength +  this.value.ballot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposal.writeTarget(tgt) +  this.value.ballot.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Activate_account generated for Proto010PtGRANADOperationAlphaContents__Activate_account
export class CGRIDClass__Proto010PtGRANADOperationAlphaContents__Activate_account extends Box<Proto010PtGRANADOperationAlphaContents__Activate_account> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'secret']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationAlphaContents__Activate_account {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContents__Activate_account>({pkh: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Activate_account_pkh.decode, secret: FixedBytes.decode<20>({len: 20})}, {order: ['pkh', 'secret']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.secret.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.secret.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADInlinedEndorsementContents__Endorsement generated for Proto010PtGRANADInlinedEndorsementContents__Endorsement
export class CGRIDClass__Proto010PtGRANADInlinedEndorsementContents__Endorsement extends Box<Proto010PtGRANADInlinedEndorsementContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADInlinedEndorsementContents__Endorsement {
        return new this(record_decoder<Proto010PtGRANADInlinedEndorsementContents__Endorsement>({level: Int32.decode}, {order: ['level']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint__set_delegate generated for Proto010PtGRANADEntrypoint__set_delegate
export class CGRIDClass__Proto010PtGRANADEntrypoint__set_delegate extends Box<Proto010PtGRANADEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint__root generated for Proto010PtGRANADEntrypoint__root
export class CGRIDClass__Proto010PtGRANADEntrypoint__root extends Box<Proto010PtGRANADEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint__remove_delegate generated for Proto010PtGRANADEntrypoint__remove_delegate
export class CGRIDClass__Proto010PtGRANADEntrypoint__remove_delegate extends Box<Proto010PtGRANADEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint__named generated for Proto010PtGRANADEntrypoint__named
export class CGRIDClass__Proto010PtGRANADEntrypoint__named extends Box<Proto010PtGRANADEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint___do generated for Proto010PtGRANADEntrypoint___do
export class CGRIDClass__Proto010PtGRANADEntrypoint___do extends Box<Proto010PtGRANADEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADEntrypoint___default generated for Proto010PtGRANADEntrypoint___default
export class CGRIDClass__Proto010PtGRANADEntrypoint___default extends Box<Proto010PtGRANADEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADContractId__Originated generated for Proto010PtGRANADContractId__Originated
export class CGRIDClass__Proto010PtGRANADContractId__Originated extends Box<Proto010PtGRANADContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto010PtGRANADContractId__Implicit generated for Proto010PtGRANADContractId__Implicit
export class CGRIDClass__Proto010PtGRANADContractId__Implicit extends Box<Proto010PtGRANADContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADContractId__Implicit {
        return new this(record_decoder<Proto010PtGRANADContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto010PtGRANADOperationAlphaContents__Transaction = { source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Transaction_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: N, destination: CGRIDClass__Proto010_PtGRANADContract_id, parameters: Option<CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Transaction_parameters> };
export type Proto010PtGRANADOperationAlphaContents__Seed_nonce_revelation = { level: Int32, nonce: FixedBytes<32> };
export type Proto010PtGRANADOperationAlphaContents__Reveal = { source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Reveal_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_key: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Reveal_public_key };
export type Proto010PtGRANADOperationAlphaContents__Proposals = { source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Proposals_source, period: Int32, proposals: Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq>,width.Uint30> };
export type Proto010PtGRANADOperationAlphaContents__Origination = { source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, balance: N, delegate: Option<CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Origination_delegate>, script: CGRIDClass__Proto010_PtGRANADScriptedContracts };
export type Proto010PtGRANADOperationAlphaContents__Failing_noop = { arbitrary: Dynamic<U8String,width.Uint30> };
export type Proto010PtGRANADOperationAlphaContents__Endorsement_with_slot = { endorsement: Dynamic<CGRIDClass__Proto010_PtGRANADInlinedEndorsement,width.Uint30>, slot: Uint16 };
export type Proto010PtGRANADOperationAlphaContents__Endorsement = { level: Int32 };
export type Proto010PtGRANADOperationAlphaContents__Double_endorsement_evidence = { op1: Dynamic<CGRIDClass__Proto010_PtGRANADInlinedEndorsement,width.Uint30>, op2: Dynamic<CGRIDClass__Proto010_PtGRANADInlinedEndorsement,width.Uint30>, slot: Uint16 };
export type Proto010PtGRANADOperationAlphaContents__Double_baking_evidence = { bh1: Dynamic<CGRIDClass__Proto010_PtGRANADBlock_headerAlphaFull_header,width.Uint30>, bh2: Dynamic<CGRIDClass__Proto010_PtGRANADBlock_headerAlphaFull_header,width.Uint30> };
export type Proto010PtGRANADOperationAlphaContents__Delegation = { source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Delegation_source, fee: N, counter: N, gas_limit: N, storage_limit: N, delegate: Option<CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Delegation_delegate> };
export type Proto010PtGRANADOperationAlphaContents__Ballot = { source: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Ballot_source, period: Int32, proposal: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Ballot_proposal, ballot: Int8 };
export type Proto010PtGRANADOperationAlphaContents__Activate_account = { pkh: CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Activate_account_pkh, secret: FixedBytes<20> };
export type Proto010PtGRANADInlinedEndorsementContents__Endorsement = { level: Int32 };
export type Proto010PtGRANADEntrypoint__set_delegate = Unit;
export type Proto010PtGRANADEntrypoint__root = Unit;
export type Proto010PtGRANADEntrypoint__remove_delegate = Unit;
export type Proto010PtGRANADEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto010PtGRANADEntrypoint___do = Unit;
export type Proto010PtGRANADEntrypoint___default = Unit;
export type Proto010PtGRANADContractId__Originated = Padded<CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad,1>;
export type Proto010PtGRANADContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADScriptedContracts generated for Proto010PtGRANADScriptedContracts
export class CGRIDClass__Proto010_PtGRANADScriptedContracts extends Box<Proto010PtGRANADScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADScriptedContracts {
        return new this(record_decoder<Proto010PtGRANADScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Transaction_source generated for Proto010PtGRANADOperationAlphaContentsTransactionSource
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Transaction_source extends Box<Proto010PtGRANADOperationAlphaContentsTransactionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Transaction_source {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsTransactionSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Transaction_parameters generated for Proto010PtGRANADOperationAlphaContentsTransactionParameters
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Transaction_parameters extends Box<Proto010PtGRANADOperationAlphaContentsTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Transaction_parameters {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsTransactionParameters>({entrypoint: CGRIDClass__Proto010_PtGRANADEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Reveal_source generated for Proto010PtGRANADOperationAlphaContentsRevealSource
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Reveal_source extends Box<Proto010PtGRANADOperationAlphaContentsRevealSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Reveal_source {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsRevealSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Reveal_public_key generated for Proto010PtGRANADOperationAlphaContentsRevealPublicKey
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Reveal_public_key extends Box<Proto010PtGRANADOperationAlphaContentsRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Reveal_public_key {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Proposals_source generated for Proto010PtGRANADOperationAlphaContentsProposalsSource
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Proposals_source extends Box<Proto010PtGRANADOperationAlphaContentsProposalsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Proposals_source {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsProposalsSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq generated for Proto010PtGRANADOperationAlphaContentsProposalsProposalsDenestDynDenestSeq
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq extends Box<Proto010PtGRANADOperationAlphaContentsProposalsProposalsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Origination_source generated for Proto010PtGRANADOperationAlphaContentsOriginationSource
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Origination_source extends Box<Proto010PtGRANADOperationAlphaContentsOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Origination_source {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsOriginationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Origination_delegate generated for Proto010PtGRANADOperationAlphaContentsOriginationDelegate
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Origination_delegate extends Box<Proto010PtGRANADOperationAlphaContentsOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Origination_delegate {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Delegation_source generated for Proto010PtGRANADOperationAlphaContentsDelegationSource
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Delegation_source extends Box<Proto010PtGRANADOperationAlphaContentsDelegationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Delegation_source {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsDelegationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Delegation_delegate generated for Proto010PtGRANADOperationAlphaContentsDelegationDelegate
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Delegation_delegate extends Box<Proto010PtGRANADOperationAlphaContentsDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Delegation_delegate {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Ballot_source generated for Proto010PtGRANADOperationAlphaContentsBallotSource
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Ballot_source extends Box<Proto010PtGRANADOperationAlphaContentsBallotSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Ballot_source {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsBallotSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Ballot_proposal generated for Proto010PtGRANADOperationAlphaContentsBallotProposal
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Ballot_proposal extends Box<Proto010PtGRANADOperationAlphaContentsBallotProposal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Ballot_proposal {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsBallotProposal>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Activate_account_pkh generated for Proto010PtGRANADOperationAlphaContentsActivateAccountPkh
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Activate_account_pkh extends Box<Proto010PtGRANADOperationAlphaContentsActivateAccountPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents_Activate_account_pkh {
        return new this(record_decoder<Proto010PtGRANADOperationAlphaContentsActivateAccountPkh>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADOperationAlphaContents generated for Proto010PtGRANADOperationAlphaContents
export function proto010ptgranadoperationalphacontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADOperationAlphaContents,Proto010PtGRANADOperationAlphaContents> {
    function f(disc: CGRIDTag__Proto010PtGRANADOperationAlphaContents) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Endorsement: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Endorsement.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Seed_nonce_revelation: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Seed_nonce_revelation.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Double_endorsement_evidence: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Double_endorsement_evidence.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Double_baking_evidence: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Double_baking_evidence.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Activate_account: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Activate_account.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Proposals: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Proposals.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Ballot: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Ballot.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Endorsement_with_slot: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Endorsement_with_slot.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Failing_noop: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Failing_noop.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Reveal: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Reveal.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Transaction: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Transaction.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Origination: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Origination.decode;
            case CGRIDTag__Proto010PtGRANADOperationAlphaContents.Delegation: return CGRIDClass__Proto010PtGRANADOperationAlphaContents__Delegation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADOperationAlphaContents => Object.values(CGRIDTag__Proto010PtGRANADOperationAlphaContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADOperationAlphaContents extends Box<Proto010PtGRANADOperationAlphaContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADOperationAlphaContents>, Proto010PtGRANADOperationAlphaContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADOperationAlphaContents {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadoperationalphacontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADInlinedEndorsement_signature generated for Proto010PtGRANADInlinedEndorsementSignature
export class CGRIDClass__Proto010_PtGRANADInlinedEndorsement_signature extends Box<Proto010PtGRANADInlinedEndorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADInlinedEndorsement_signature {
        return new this(record_decoder<Proto010PtGRANADInlinedEndorsementSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADInlinedEndorsementContents generated for Proto010PtGRANADInlinedEndorsementContents
export function proto010ptgranadinlinedendorsementcontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADInlinedEndorsementContents,Proto010PtGRANADInlinedEndorsementContents> {
    function f(disc: CGRIDTag__Proto010PtGRANADInlinedEndorsementContents) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADInlinedEndorsementContents.Endorsement: return CGRIDClass__Proto010PtGRANADInlinedEndorsementContents__Endorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADInlinedEndorsementContents => Object.values(CGRIDTag__Proto010PtGRANADInlinedEndorsementContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADInlinedEndorsementContents extends Box<Proto010PtGRANADInlinedEndorsementContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADInlinedEndorsementContents>, Proto010PtGRANADInlinedEndorsementContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADInlinedEndorsementContents {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadinlinedendorsementcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADInlinedEndorsement generated for Proto010PtGRANADInlinedEndorsement
export class CGRIDClass__Proto010_PtGRANADInlinedEndorsement extends Box<Proto010PtGRANADInlinedEndorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADInlinedEndorsement {
        return new this(record_decoder<Proto010PtGRANADInlinedEndorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__Proto010_PtGRANADInlinedEndorsementContents.decode, signature: Nullable.decode(CGRIDClass__Proto010_PtGRANADInlinedEndorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADEntrypoint generated for Proto010PtGRANADEntrypoint
export function proto010ptgranadentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADEntrypoint,Proto010PtGRANADEntrypoint> {
    function f(disc: CGRIDTag__Proto010PtGRANADEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADEntrypoint._default: return CGRIDClass__Proto010PtGRANADEntrypoint___default.decode;
            case CGRIDTag__Proto010PtGRANADEntrypoint.root: return CGRIDClass__Proto010PtGRANADEntrypoint__root.decode;
            case CGRIDTag__Proto010PtGRANADEntrypoint._do: return CGRIDClass__Proto010PtGRANADEntrypoint___do.decode;
            case CGRIDTag__Proto010PtGRANADEntrypoint.set_delegate: return CGRIDClass__Proto010PtGRANADEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto010PtGRANADEntrypoint.remove_delegate: return CGRIDClass__Proto010PtGRANADEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto010PtGRANADEntrypoint.named: return CGRIDClass__Proto010PtGRANADEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADEntrypoint => Object.values(CGRIDTag__Proto010PtGRANADEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADEntrypoint extends Box<Proto010PtGRANADEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADEntrypoint>, Proto010PtGRANADEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad generated for Proto010PtGRANADContractIdOriginatedDenestPad
export class CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad extends Box<Proto010PtGRANADContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto010PtGRANADContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADContract_id generated for Proto010PtGRANADContractId
export function proto010ptgranadcontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto010PtGRANADContractId,Proto010PtGRANADContractId> {
    function f(disc: CGRIDTag__Proto010PtGRANADContractId) {
        switch (disc) {
            case CGRIDTag__Proto010PtGRANADContractId.Implicit: return CGRIDClass__Proto010PtGRANADContractId__Implicit.decode;
            case CGRIDTag__Proto010PtGRANADContractId.Originated: return CGRIDClass__Proto010PtGRANADContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto010PtGRANADContractId => Object.values(CGRIDTag__Proto010PtGRANADContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto010_PtGRANADContract_id extends Box<Proto010PtGRANADContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto010PtGRANADContractId>, Proto010PtGRANADContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADContract_id {
        return new this(variant_decoder(width.Uint8)(proto010ptgranadcontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto010_PtGRANADBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADBlock_headerAlphaSigned_contents_signature generated for Proto010PtGRANADBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto010_PtGRANADBlock_headerAlphaSigned_contents_signature extends Box<Proto010PtGRANADBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto010PtGRANADBlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto010_PtGRANADBlock_headerAlphaFull_header generated for Proto010PtGRANADBlockHeaderAlphaFullHeader
export class CGRIDClass__Proto010_PtGRANADBlock_headerAlphaFull_header extends Box<Proto010PtGRANADBlockHeaderAlphaFullHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADBlock_headerAlphaFull_header {
        return new this(record_decoder<Proto010PtGRANADBlockHeaderAlphaFullHeader>({level: Int32.decode, proto: Uint8.decode, predecessor: CGRIDClass__Block_headerShell_predecessor.decode, timestamp: Int64.decode, validation_pass: Uint8.decode, operations_hash: CGRIDClass__Block_headerShell_operations_hash.decode, fitness: Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30), context: CGRIDClass__Block_headerShell_context.decode, priority: Uint16.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto010_PtGRANADBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_escape_vote: Bool.decode, signature: CGRIDClass__Proto010_PtGRANADBlock_headerAlphaSigned_contents_signature.decode}, {order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.proto.encodeLength +  this.value.predecessor.encodeLength +  this.value.timestamp.encodeLength +  this.value.validation_pass.encodeLength +  this.value.operations_hash.encodeLength +  this.value.fitness.encodeLength +  this.value.context.encodeLength +  this.value.priority.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_escape_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.proto.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.timestamp.writeTarget(tgt) +  this.value.validation_pass.writeTarget(tgt) +  this.value.operations_hash.writeTarget(tgt) +  this.value.fitness.writeTarget(tgt) +  this.value.context.writeTarget(tgt) +  this.value.priority.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_escape_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_predecessor generated for BlockHeaderShellPredecessor
export class CGRIDClass__Block_headerShell_predecessor extends Box<BlockHeaderShellPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_predecessor {
        return new this(record_decoder<BlockHeaderShellPredecessor>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_operations_hash generated for BlockHeaderShellOperationsHash
export class CGRIDClass__Block_headerShell_operations_hash extends Box<BlockHeaderShellOperationsHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_list_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_operations_hash {
        return new this(record_decoder<BlockHeaderShellOperationsHash>({operation_list_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_list_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_list_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_list_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_context generated for BlockHeaderShellContext
export class CGRIDClass__Block_headerShell_context extends Box<BlockHeaderShellContext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_context {
        return new this(record_decoder<BlockHeaderShellContext>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export type BlockHeaderShellPredecessor = { block_hash: FixedBytes<32> };
export type BlockHeaderShellOperationsHash = { operation_list_list_hash: FixedBytes<32> };
export type BlockHeaderShellContext = { context_hash: FixedBytes<32> };
export type Proto010PtGRANADScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export type Proto010PtGRANADOperationAlphaContentsTransactionSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADOperationAlphaContentsTransactionParameters = { entrypoint: CGRIDClass__Proto010_PtGRANADEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto010PtGRANADOperationAlphaContentsRevealSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADOperationAlphaContentsRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto010PtGRANADOperationAlphaContentsProposalsSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADOperationAlphaContentsProposalsProposalsDenestDynDenestSeq = { protocol_hash: FixedBytes<32> };
export type Proto010PtGRANADOperationAlphaContentsOriginationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADOperationAlphaContentsOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADOperationAlphaContentsDelegationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADOperationAlphaContentsDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADOperationAlphaContentsBallotSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto010PtGRANADOperationAlphaContentsBallotProposal = { protocol_hash: FixedBytes<32> };
export type Proto010PtGRANADOperationAlphaContentsActivateAccountPkh = { ed25519_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__Proto010PtGRANADOperationAlphaContents{
    Endorsement = 0,
    Seed_nonce_revelation = 1,
    Double_endorsement_evidence = 2,
    Double_baking_evidence = 3,
    Activate_account = 4,
    Proposals = 5,
    Ballot = 6,
    Endorsement_with_slot = 10,
    Failing_noop = 17,
    Reveal = 107,
    Transaction = 108,
    Origination = 109,
    Delegation = 110
}
export interface CGRIDMap__Proto010PtGRANADOperationAlphaContents {
    Endorsement: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Endorsement,
    Seed_nonce_revelation: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Seed_nonce_revelation,
    Double_endorsement_evidence: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Double_endorsement_evidence,
    Double_baking_evidence: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Double_baking_evidence,
    Activate_account: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Activate_account,
    Proposals: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Proposals,
    Ballot: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Ballot,
    Endorsement_with_slot: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Endorsement_with_slot,
    Failing_noop: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Failing_noop,
    Reveal: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Reveal,
    Transaction: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Transaction,
    Origination: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Origination,
    Delegation: CGRIDClass__Proto010PtGRANADOperationAlphaContents__Delegation
}
export type Proto010PtGRANADOperationAlphaContents = { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Endorsement, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Endorsement'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Seed_nonce_revelation, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Seed_nonce_revelation'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Double_endorsement_evidence, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Double_endorsement_evidence'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Double_baking_evidence, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Double_baking_evidence'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Activate_account, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Activate_account'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Proposals, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Proposals'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Ballot, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Ballot'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Endorsement_with_slot, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Endorsement_with_slot'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Failing_noop, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Failing_noop'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Reveal, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Reveal'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Transaction, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Transaction'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Origination, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Origination'] } | { kind: CGRIDTag__Proto010PtGRANADOperationAlphaContents.Delegation, value: CGRIDMap__Proto010PtGRANADOperationAlphaContents['Delegation'] };
export type Proto010PtGRANADInlinedEndorsementSignature = { signature_v0: FixedBytes<64> };
export enum CGRIDTag__Proto010PtGRANADInlinedEndorsementContents{
    Endorsement = 0
}
export interface CGRIDMap__Proto010PtGRANADInlinedEndorsementContents {
    Endorsement: CGRIDClass__Proto010PtGRANADInlinedEndorsementContents__Endorsement
}
export type Proto010PtGRANADInlinedEndorsementContents = { kind: CGRIDTag__Proto010PtGRANADInlinedEndorsementContents.Endorsement, value: CGRIDMap__Proto010PtGRANADInlinedEndorsementContents['Endorsement'] };
export type Proto010PtGRANADInlinedEndorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__Proto010_PtGRANADInlinedEndorsementContents, signature: Nullable<CGRIDClass__Proto010_PtGRANADInlinedEndorsement_signature> };
export enum CGRIDTag__Proto010PtGRANADEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto010PtGRANADEntrypoint {
    _default: CGRIDClass__Proto010PtGRANADEntrypoint___default,
    root: CGRIDClass__Proto010PtGRANADEntrypoint__root,
    _do: CGRIDClass__Proto010PtGRANADEntrypoint___do,
    set_delegate: CGRIDClass__Proto010PtGRANADEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto010PtGRANADEntrypoint__remove_delegate,
    named: CGRIDClass__Proto010PtGRANADEntrypoint__named
}
export type Proto010PtGRANADEntrypoint = { kind: CGRIDTag__Proto010PtGRANADEntrypoint._default, value: CGRIDMap__Proto010PtGRANADEntrypoint['_default'] } | { kind: CGRIDTag__Proto010PtGRANADEntrypoint.root, value: CGRIDMap__Proto010PtGRANADEntrypoint['root'] } | { kind: CGRIDTag__Proto010PtGRANADEntrypoint._do, value: CGRIDMap__Proto010PtGRANADEntrypoint['_do'] } | { kind: CGRIDTag__Proto010PtGRANADEntrypoint.set_delegate, value: CGRIDMap__Proto010PtGRANADEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto010PtGRANADEntrypoint.remove_delegate, value: CGRIDMap__Proto010PtGRANADEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto010PtGRANADEntrypoint.named, value: CGRIDMap__Proto010PtGRANADEntrypoint['named'] };
export type Proto010PtGRANADContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto010PtGRANADContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto010PtGRANADContractId {
    Implicit: CGRIDClass__Proto010PtGRANADContractId__Implicit,
    Originated: CGRIDClass__Proto010PtGRANADContractId__Originated
}
export type Proto010PtGRANADContractId = { kind: CGRIDTag__Proto010PtGRANADContractId.Implicit, value: CGRIDMap__Proto010PtGRANADContractId['Implicit'] } | { kind: CGRIDTag__Proto010PtGRANADContractId.Originated, value: CGRIDMap__Proto010PtGRANADContractId['Originated'] };
export type Proto010PtGRANADBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto010PtGRANADBlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto010PtGRANADBlockHeaderAlphaFullHeader = { level: Int32, proto: Uint8, predecessor: CGRIDClass__Block_headerShell_predecessor, timestamp: Int64, validation_pass: Uint8, operations_hash: CGRIDClass__Block_headerShell_operations_hash, fitness: Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>, context: CGRIDClass__Block_headerShell_context, priority: Uint16, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto010_PtGRANADBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_escape_vote: Bool, signature: CGRIDClass__Proto010_PtGRANADBlock_headerAlphaSigned_contents_signature };
export type Proto010PtGRANADOperationContentsList = Sequence<CGRIDClass__Proto010_PtGRANADOperationAlphaContents>;
export class CGRIDClass__Proto010PtGRANADOperationContentsList extends Box<Proto010PtGRANADOperationContentsList> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADOperationContentsList {
        return new this(Sequence.decode(CGRIDClass__Proto010_PtGRANADOperationAlphaContents.decode)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto010_ptgranad_operation_contents_list_encoder = (value: Proto010PtGRANADOperationContentsList): OutputBytes => {
    return value.encode();
}
export const proto010_ptgranad_operation_contents_list_decoder = (p: Parser): Proto010PtGRANADOperationContentsList => {
    return Sequence.decode(CGRIDClass__Proto010_PtGRANADOperationAlphaContents.decode)(p);
}
