import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__Proto010_PtGRANADDelegateFrozen_balance_by_cycles_denest_dyn_denest_seq generated for Proto010PtGRANADDelegateFrozenBalanceByCyclesDenestDynDenestSeq
export class CGRIDClass__Proto010_PtGRANADDelegateFrozen_balance_by_cycles_denest_dyn_denest_seq extends Box<Proto010PtGRANADDelegateFrozenBalanceByCyclesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle', 'deposits', 'fees', 'rewards']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto010_PtGRANADDelegateFrozen_balance_by_cycles_denest_dyn_denest_seq {
        return new this(record_decoder<Proto010PtGRANADDelegateFrozenBalanceByCyclesDenestDynDenestSeq>({cycle: Int32.decode, deposits: N.decode, fees: N.decode, rewards: N.decode}, {order: ['cycle', 'deposits', 'fees', 'rewards']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle.encodeLength +  this.value.deposits.encodeLength +  this.value.fees.encodeLength +  this.value.rewards.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle.writeTarget(tgt) +  this.value.deposits.writeTarget(tgt) +  this.value.fees.writeTarget(tgt) +  this.value.rewards.writeTarget(tgt));
    }
}
export type Proto010PtGRANADDelegateFrozenBalanceByCyclesDenestDynDenestSeq = { cycle: Int32, deposits: N, fees: N, rewards: N };
export type Proto010PtGRANADDelegateFrozenBalanceByCycles = Dynamic<Sequence<CGRIDClass__Proto010_PtGRANADDelegateFrozen_balance_by_cycles_denest_dyn_denest_seq>,width.Uint30>;
export class CGRIDClass__Proto010PtGRANADDelegateFrozenBalanceByCycles extends Box<Proto010PtGRANADDelegateFrozenBalanceByCycles> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADDelegateFrozenBalanceByCycles {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADDelegateFrozen_balance_by_cycles_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto010_ptgranad_delegate_frozen_balance_by_cycles_encoder = (value: Proto010PtGRANADDelegateFrozenBalanceByCycles): OutputBytes => {
    return value.encode();
}
export const proto010_ptgranad_delegate_frozen_balance_by_cycles_decoder = (p: Parser): Proto010PtGRANADDelegateFrozenBalanceByCycles => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto010_PtGRANADDelegateFrozen_balance_by_cycles_denest_dyn_denest_seq.decode), width.Uint30)(p);
}
