import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto010PtGRANADCycle = Int32;
export class CGRIDClass__Proto010PtGRANADCycle extends Box<Proto010PtGRANADCycle> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADCycle {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto010_ptgranad_cycle_encoder = (value: Proto010PtGRANADCycle): OutputBytes => {
    return value.encode();
}
export const proto010_ptgranad_cycle_decoder = (p: Parser): Proto010PtGRANADCycle => {
    return Int32.decode(p);
}
