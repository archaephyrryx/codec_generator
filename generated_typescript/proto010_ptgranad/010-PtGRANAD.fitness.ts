import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { Target } from '../../ts_runtime/target';
export type Proto010PtGRANADFitness = Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>;
export class CGRIDClass__Proto010PtGRANADFitness extends Box<Proto010PtGRANADFitness> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto010PtGRANADFitness {
        return new this(Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto010_ptgranad_fitness_encoder = (value: Proto010PtGRANADFitness): OutputBytes => {
    return value.encode();
}
export const proto010_ptgranad_fitness_decoder = (p: Parser): Proto010PtGRANADFitness => {
    return Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30)(p);
}
