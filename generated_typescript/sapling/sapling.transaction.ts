import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { SequenceBounded } from '../../ts_runtime/composite/seq/sequence.bounded';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__SaplingTransactionOutput generated for SaplingTransactionOutput
export class CGRIDClass__SaplingTransactionOutput extends Box<SaplingTransactionOutput> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cm', 'proof_o', 'ciphertext']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionOutput {
        return new this(record_decoder<SaplingTransactionOutput>({cm: FixedBytes.decode<32>({len: 32}), proof_o: FixedBytes.decode<192>({len: 192}), ciphertext: CGRIDClass__SaplingTransactionCiphertext.decode}, {order: ['cm', 'proof_o', 'ciphertext']})(p));
    };
    get encodeLength(): number {
        return (this.value.cm.encodeLength +  this.value.proof_o.encodeLength +  this.value.ciphertext.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cm.writeTarget(tgt) +  this.value.proof_o.writeTarget(tgt) +  this.value.ciphertext.writeTarget(tgt));
    }
}
// Class CGRIDClass__SaplingTransactionInput generated for SaplingTransactionInput
export class CGRIDClass__SaplingTransactionInput extends Box<SaplingTransactionInput> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cv', 'nf', 'rk', 'proof_i', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionInput {
        return new this(record_decoder<SaplingTransactionInput>({cv: FixedBytes.decode<32>({len: 32}), nf: FixedBytes.decode<32>({len: 32}), rk: FixedBytes.decode<32>({len: 32}), proof_i: FixedBytes.decode<192>({len: 192}), signature: FixedBytes.decode<64>({len: 64})}, {order: ['cv', 'nf', 'rk', 'proof_i', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.cv.encodeLength +  this.value.nf.encodeLength +  this.value.rk.encodeLength +  this.value.proof_i.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cv.writeTarget(tgt) +  this.value.nf.writeTarget(tgt) +  this.value.rk.writeTarget(tgt) +  this.value.proof_i.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__SaplingTransactionCiphertext generated for SaplingTransactionCiphertext
export class CGRIDClass__SaplingTransactionCiphertext extends Box<SaplingTransactionCiphertext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionCiphertext {
        return new this(record_decoder<SaplingTransactionCiphertext>({cv: FixedBytes.decode<32>({len: 32}), epk: FixedBytes.decode<32>({len: 32}), payload_enc: Dynamic.decode(Bytes.decode, width.Uint30), nonce_enc: FixedBytes.decode<24>({len: 24}), payload_out: FixedBytes.decode<80>({len: 80}), nonce_out: FixedBytes.decode<24>({len: 24})}, {order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(p));
    };
    get encodeLength(): number {
        return (this.value.cv.encodeLength +  this.value.epk.encodeLength +  this.value.payload_enc.encodeLength +  this.value.nonce_enc.encodeLength +  this.value.payload_out.encodeLength +  this.value.nonce_out.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cv.writeTarget(tgt) +  this.value.epk.writeTarget(tgt) +  this.value.payload_enc.writeTarget(tgt) +  this.value.nonce_enc.writeTarget(tgt) +  this.value.payload_out.writeTarget(tgt) +  this.value.nonce_out.writeTarget(tgt));
    }
}
export type SaplingTransactionOutput = { cm: FixedBytes<32>, proof_o: FixedBytes<192>, ciphertext: CGRIDClass__SaplingTransactionCiphertext };
export type SaplingTransactionInput = { cv: FixedBytes<32>, nf: FixedBytes<32>, rk: FixedBytes<32>, proof_i: FixedBytes<192>, signature: FixedBytes<64> };
export type SaplingTransactionCiphertext = { cv: FixedBytes<32>, epk: FixedBytes<32>, payload_enc: Dynamic<Bytes,width.Uint30>, nonce_enc: FixedBytes<24>, payload_out: FixedBytes<80>, nonce_out: FixedBytes<24> };
export type SaplingTransaction = { inputs: Dynamic<SequenceBounded<CGRIDClass__SaplingTransactionInput,5208>,width.Uint30>, outputs: Dynamic<SequenceBounded<CGRIDClass__SaplingTransactionOutput,2019>,width.Uint30>, binding_sig: FixedBytes<64>, balance: Int64, root: FixedBytes<32>, bound_data: Dynamic<U8String,width.Uint30> };
export class CGRIDClass__SaplingTransaction extends Box<SaplingTransaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['inputs', 'outputs', 'binding_sig', 'balance', 'root', 'bound_data']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransaction {
        return new this(record_decoder<SaplingTransaction>({inputs: Dynamic.decode(SequenceBounded.decode(CGRIDClass__SaplingTransactionInput.decode, 5208, None), width.Uint30), outputs: Dynamic.decode(SequenceBounded.decode(CGRIDClass__SaplingTransactionOutput.decode, 2019, None), width.Uint30), binding_sig: FixedBytes.decode<64>({len: 64}), balance: Int64.decode, root: FixedBytes.decode<32>({len: 32}), bound_data: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['inputs', 'outputs', 'binding_sig', 'balance', 'root', 'bound_data']})(p));
    };
    get encodeLength(): number {
        return (this.value.inputs.encodeLength +  this.value.outputs.encodeLength +  this.value.binding_sig.encodeLength +  this.value.balance.encodeLength +  this.value.root.encodeLength +  this.value.bound_data.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.inputs.writeTarget(tgt) +  this.value.outputs.writeTarget(tgt) +  this.value.binding_sig.writeTarget(tgt) +  this.value.balance.writeTarget(tgt) +  this.value.root.writeTarget(tgt) +  this.value.bound_data.writeTarget(tgt));
    }
}
export const sapling_transaction_encoder = (value: SaplingTransaction): OutputBytes => {
    return record_encoder({order: ['inputs', 'outputs', 'binding_sig', 'balance', 'root', 'bound_data']})(value);
}
export const sapling_transaction_decoder = (p: Parser): SaplingTransaction => {
    return record_decoder<SaplingTransaction>({inputs: Dynamic.decode(SequenceBounded.decode(CGRIDClass__SaplingTransactionInput.decode, 5208, None), width.Uint30), outputs: Dynamic.decode(SequenceBounded.decode(CGRIDClass__SaplingTransactionOutput.decode, 2019, None), width.Uint30), binding_sig: FixedBytes.decode<64>({len: 64}), balance: Int64.decode, root: FixedBytes.decode<32>({len: 32}), bound_data: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['inputs', 'outputs', 'binding_sig', 'balance', 'root', 'bound_data']})(p);
}
