import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type SaplingTransactionNullifier = FixedBytes<32>;
export class CGRIDClass__SaplingTransactionNullifier extends Box<SaplingTransactionNullifier> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionNullifier {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const sapling_transaction_nullifier_encoder = (value: SaplingTransactionNullifier): OutputBytes => {
    return value.encode();
}
export const sapling_transaction_nullifier_decoder = (p: Parser): SaplingTransactionNullifier => {
    return FixedBytes.decode<32>({len: 32})(p);
}
