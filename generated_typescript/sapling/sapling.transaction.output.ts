import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__SaplingTransactionCiphertext generated for SaplingTransactionCiphertext
export class CGRIDClass__SaplingTransactionCiphertext extends Box<SaplingTransactionCiphertext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionCiphertext {
        return new this(record_decoder<SaplingTransactionCiphertext>({cv: FixedBytes.decode<32>({len: 32}), epk: FixedBytes.decode<32>({len: 32}), payload_enc: Dynamic.decode(Bytes.decode, width.Uint30), nonce_enc: FixedBytes.decode<24>({len: 24}), payload_out: FixedBytes.decode<80>({len: 80}), nonce_out: FixedBytes.decode<24>({len: 24})}, {order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(p));
    };
    get encodeLength(): number {
        return (this.value.cv.encodeLength +  this.value.epk.encodeLength +  this.value.payload_enc.encodeLength +  this.value.nonce_enc.encodeLength +  this.value.payload_out.encodeLength +  this.value.nonce_out.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cv.writeTarget(tgt) +  this.value.epk.writeTarget(tgt) +  this.value.payload_enc.writeTarget(tgt) +  this.value.nonce_enc.writeTarget(tgt) +  this.value.payload_out.writeTarget(tgt) +  this.value.nonce_out.writeTarget(tgt));
    }
}
export type SaplingTransactionCiphertext = { cv: FixedBytes<32>, epk: FixedBytes<32>, payload_enc: Dynamic<Bytes,width.Uint30>, nonce_enc: FixedBytes<24>, payload_out: FixedBytes<80>, nonce_out: FixedBytes<24> };
export type SaplingTransactionOutput = { cm: FixedBytes<32>, proof_o: FixedBytes<192>, ciphertext: CGRIDClass__SaplingTransactionCiphertext };
export class CGRIDClass__SaplingTransactionOutput extends Box<SaplingTransactionOutput> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cm', 'proof_o', 'ciphertext']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionOutput {
        return new this(record_decoder<SaplingTransactionOutput>({cm: FixedBytes.decode<32>({len: 32}), proof_o: FixedBytes.decode<192>({len: 192}), ciphertext: CGRIDClass__SaplingTransactionCiphertext.decode}, {order: ['cm', 'proof_o', 'ciphertext']})(p));
    };
    get encodeLength(): number {
        return (this.value.cm.encodeLength +  this.value.proof_o.encodeLength +  this.value.ciphertext.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cm.writeTarget(tgt) +  this.value.proof_o.writeTarget(tgt) +  this.value.ciphertext.writeTarget(tgt));
    }
}
export const sapling_transaction_output_encoder = (value: SaplingTransactionOutput): OutputBytes => {
    return record_encoder({order: ['cm', 'proof_o', 'ciphertext']})(value);
}
export const sapling_transaction_output_decoder = (p: Parser): SaplingTransactionOutput => {
    return record_decoder<SaplingTransactionOutput>({cm: FixedBytes.decode<32>({len: 32}), proof_o: FixedBytes.decode<192>({len: 192}), ciphertext: CGRIDClass__SaplingTransactionCiphertext.decode}, {order: ['cm', 'proof_o', 'ciphertext']})(p);
}
