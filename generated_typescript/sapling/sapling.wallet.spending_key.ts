import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__SaplingWalletExpanded_spending_key generated for SaplingWalletExpandedSpendingKey
export class CGRIDClass__SaplingWalletExpanded_spending_key extends Box<SaplingWalletExpandedSpendingKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ask', 'nsk', 'ovk']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingWalletExpanded_spending_key {
        return new this(record_decoder<SaplingWalletExpandedSpendingKey>({ask: FixedBytes.decode<32>({len: 32}), nsk: FixedBytes.decode<32>({len: 32}), ovk: FixedBytes.decode<32>({len: 32})}, {order: ['ask', 'nsk', 'ovk']})(p));
    };
    get encodeLength(): number {
        return (this.value.ask.encodeLength +  this.value.nsk.encodeLength +  this.value.ovk.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ask.writeTarget(tgt) +  this.value.nsk.writeTarget(tgt) +  this.value.ovk.writeTarget(tgt));
    }
}
export type SaplingWalletExpandedSpendingKey = { ask: FixedBytes<32>, nsk: FixedBytes<32>, ovk: FixedBytes<32> };
export type SaplingWalletSpendingKey = { depth: FixedBytes<1>, parent_fvk_tag: FixedBytes<4>, child_index: FixedBytes<4>, chain_code: FixedBytes<32>, expsk: CGRIDClass__SaplingWalletExpanded_spending_key, dk: FixedBytes<32> };
export class CGRIDClass__SaplingWalletSpendingKey extends Box<SaplingWalletSpendingKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['depth', 'parent_fvk_tag', 'child_index', 'chain_code', 'expsk', 'dk']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingWalletSpendingKey {
        return new this(record_decoder<SaplingWalletSpendingKey>({depth: FixedBytes.decode<1>({len: 1}), parent_fvk_tag: FixedBytes.decode<4>({len: 4}), child_index: FixedBytes.decode<4>({len: 4}), chain_code: FixedBytes.decode<32>({len: 32}), expsk: CGRIDClass__SaplingWalletExpanded_spending_key.decode, dk: FixedBytes.decode<32>({len: 32})}, {order: ['depth', 'parent_fvk_tag', 'child_index', 'chain_code', 'expsk', 'dk']})(p));
    };
    get encodeLength(): number {
        return (this.value.depth.encodeLength +  this.value.parent_fvk_tag.encodeLength +  this.value.child_index.encodeLength +  this.value.chain_code.encodeLength +  this.value.expsk.encodeLength +  this.value.dk.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.depth.writeTarget(tgt) +  this.value.parent_fvk_tag.writeTarget(tgt) +  this.value.child_index.writeTarget(tgt) +  this.value.chain_code.writeTarget(tgt) +  this.value.expsk.writeTarget(tgt) +  this.value.dk.writeTarget(tgt));
    }
}
export const sapling_wallet_spending_key_encoder = (value: SaplingWalletSpendingKey): OutputBytes => {
    return record_encoder({order: ['depth', 'parent_fvk_tag', 'child_index', 'chain_code', 'expsk', 'dk']})(value);
}
export const sapling_wallet_spending_key_decoder = (p: Parser): SaplingWalletSpendingKey => {
    return record_decoder<SaplingWalletSpendingKey>({depth: FixedBytes.decode<1>({len: 1}), parent_fvk_tag: FixedBytes.decode<4>({len: 4}), child_index: FixedBytes.decode<4>({len: 4}), chain_code: FixedBytes.decode<32>({len: 32}), expsk: CGRIDClass__SaplingWalletExpanded_spending_key.decode, dk: FixedBytes.decode<32>({len: 32})}, {order: ['depth', 'parent_fvk_tag', 'child_index', 'chain_code', 'expsk', 'dk']})(p);
}
