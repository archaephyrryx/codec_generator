import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type SaplingTransactionPlaintext = { diversifier: FixedBytes<11>, amount: Int64, rcm: FixedBytes<32>, memo: Dynamic<Bytes,width.Uint30> };
export class CGRIDClass__SaplingTransactionPlaintext extends Box<SaplingTransactionPlaintext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['diversifier', 'amount', 'rcm', 'memo']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionPlaintext {
        return new this(record_decoder<SaplingTransactionPlaintext>({diversifier: FixedBytes.decode<11>({len: 11}), amount: Int64.decode, rcm: FixedBytes.decode<32>({len: 32}), memo: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['diversifier', 'amount', 'rcm', 'memo']})(p));
    };
    get encodeLength(): number {
        return (this.value.diversifier.encodeLength +  this.value.amount.encodeLength +  this.value.rcm.encodeLength +  this.value.memo.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.diversifier.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.rcm.writeTarget(tgt) +  this.value.memo.writeTarget(tgt));
    }
}
export const sapling_transaction_plaintext_encoder = (value: SaplingTransactionPlaintext): OutputBytes => {
    return record_encoder({order: ['diversifier', 'amount', 'rcm', 'memo']})(value);
}
export const sapling_transaction_plaintext_decoder = (p: Parser): SaplingTransactionPlaintext => {
    return record_decoder<SaplingTransactionPlaintext>({diversifier: FixedBytes.decode<11>({len: 11}), amount: Int64.decode, rcm: FixedBytes.decode<32>({len: 32}), memo: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['diversifier', 'amount', 'rcm', 'memo']})(p);
}
