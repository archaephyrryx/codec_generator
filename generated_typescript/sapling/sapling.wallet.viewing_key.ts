import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__SaplingWalletFull_viewing_key generated for SaplingWalletFullViewingKey
export class CGRIDClass__SaplingWalletFull_viewing_key extends Box<SaplingWalletFullViewingKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ak', 'nk', 'ovk']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingWalletFull_viewing_key {
        return new this(record_decoder<SaplingWalletFullViewingKey>({ak: FixedBytes.decode<32>({len: 32}), nk: FixedBytes.decode<32>({len: 32}), ovk: FixedBytes.decode<32>({len: 32})}, {order: ['ak', 'nk', 'ovk']})(p));
    };
    get encodeLength(): number {
        return (this.value.ak.encodeLength +  this.value.nk.encodeLength +  this.value.ovk.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ak.writeTarget(tgt) +  this.value.nk.writeTarget(tgt) +  this.value.ovk.writeTarget(tgt));
    }
}
export type SaplingWalletFullViewingKey = { ak: FixedBytes<32>, nk: FixedBytes<32>, ovk: FixedBytes<32> };
export type SaplingWalletViewingKey = { depth: FixedBytes<1>, parent_fvk_tag: FixedBytes<4>, child_index: FixedBytes<4>, chain_code: FixedBytes<32>, expsk: CGRIDClass__SaplingWalletFull_viewing_key, dk: FixedBytes<32> };
export class CGRIDClass__SaplingWalletViewingKey extends Box<SaplingWalletViewingKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['depth', 'parent_fvk_tag', 'child_index', 'chain_code', 'expsk', 'dk']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingWalletViewingKey {
        return new this(record_decoder<SaplingWalletViewingKey>({depth: FixedBytes.decode<1>({len: 1}), parent_fvk_tag: FixedBytes.decode<4>({len: 4}), child_index: FixedBytes.decode<4>({len: 4}), chain_code: FixedBytes.decode<32>({len: 32}), expsk: CGRIDClass__SaplingWalletFull_viewing_key.decode, dk: FixedBytes.decode<32>({len: 32})}, {order: ['depth', 'parent_fvk_tag', 'child_index', 'chain_code', 'expsk', 'dk']})(p));
    };
    get encodeLength(): number {
        return (this.value.depth.encodeLength +  this.value.parent_fvk_tag.encodeLength +  this.value.child_index.encodeLength +  this.value.chain_code.encodeLength +  this.value.expsk.encodeLength +  this.value.dk.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.depth.writeTarget(tgt) +  this.value.parent_fvk_tag.writeTarget(tgt) +  this.value.child_index.writeTarget(tgt) +  this.value.chain_code.writeTarget(tgt) +  this.value.expsk.writeTarget(tgt) +  this.value.dk.writeTarget(tgt));
    }
}
export const sapling_wallet_viewing_key_encoder = (value: SaplingWalletViewingKey): OutputBytes => {
    return record_encoder({order: ['depth', 'parent_fvk_tag', 'child_index', 'chain_code', 'expsk', 'dk']})(value);
}
export const sapling_wallet_viewing_key_decoder = (p: Parser): SaplingWalletViewingKey => {
    return record_decoder<SaplingWalletViewingKey>({depth: FixedBytes.decode<1>({len: 1}), parent_fvk_tag: FixedBytes.decode<4>({len: 4}), child_index: FixedBytes.decode<4>({len: 4}), chain_code: FixedBytes.decode<32>({len: 32}), expsk: CGRIDClass__SaplingWalletFull_viewing_key.decode, dk: FixedBytes.decode<32>({len: 32})}, {order: ['depth', 'parent_fvk_tag', 'child_index', 'chain_code', 'expsk', 'dk']})(p);
}
