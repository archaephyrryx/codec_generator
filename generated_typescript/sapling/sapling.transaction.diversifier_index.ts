import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type SaplingTransactionDiversifierIndex = Int64;
export class CGRIDClass__SaplingTransactionDiversifierIndex extends Box<SaplingTransactionDiversifierIndex> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionDiversifierIndex {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const sapling_transaction_diversifier_index_encoder = (value: SaplingTransactionDiversifierIndex): OutputBytes => {
    return value.encode();
}
export const sapling_transaction_diversifier_index_decoder = (p: Parser): SaplingTransactionDiversifierIndex => {
    return Int64.decode(p);
}
