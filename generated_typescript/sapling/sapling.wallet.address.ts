import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type SaplingWalletAddress = { diversifier: FixedBytes<11>, pkd: FixedBytes<32> };
export class CGRIDClass__SaplingWalletAddress extends Box<SaplingWalletAddress> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['diversifier', 'pkd']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingWalletAddress {
        return new this(record_decoder<SaplingWalletAddress>({diversifier: FixedBytes.decode<11>({len: 11}), pkd: FixedBytes.decode<32>({len: 32})}, {order: ['diversifier', 'pkd']})(p));
    };
    get encodeLength(): number {
        return (this.value.diversifier.encodeLength +  this.value.pkd.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.diversifier.writeTarget(tgt) +  this.value.pkd.writeTarget(tgt));
    }
}
export const sapling_wallet_address_encoder = (value: SaplingWalletAddress): OutputBytes => {
    return record_encoder({order: ['diversifier', 'pkd']})(value);
}
export const sapling_wallet_address_decoder = (p: Parser): SaplingWalletAddress => {
    return record_decoder<SaplingWalletAddress>({diversifier: FixedBytes.decode<11>({len: 11}), pkd: FixedBytes.decode<32>({len: 32})}, {order: ['diversifier', 'pkd']})(p);
}
