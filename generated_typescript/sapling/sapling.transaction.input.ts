import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type SaplingTransactionInput = { cv: FixedBytes<32>, nf: FixedBytes<32>, rk: FixedBytes<32>, proof_i: FixedBytes<192>, signature: FixedBytes<64> };
export class CGRIDClass__SaplingTransactionInput extends Box<SaplingTransactionInput> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cv', 'nf', 'rk', 'proof_i', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionInput {
        return new this(record_decoder<SaplingTransactionInput>({cv: FixedBytes.decode<32>({len: 32}), nf: FixedBytes.decode<32>({len: 32}), rk: FixedBytes.decode<32>({len: 32}), proof_i: FixedBytes.decode<192>({len: 192}), signature: FixedBytes.decode<64>({len: 64})}, {order: ['cv', 'nf', 'rk', 'proof_i', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.cv.encodeLength +  this.value.nf.encodeLength +  this.value.rk.encodeLength +  this.value.proof_i.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cv.writeTarget(tgt) +  this.value.nf.writeTarget(tgt) +  this.value.rk.writeTarget(tgt) +  this.value.proof_i.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export const sapling_transaction_input_encoder = (value: SaplingTransactionInput): OutputBytes => {
    return record_encoder({order: ['cv', 'nf', 'rk', 'proof_i', 'signature']})(value);
}
export const sapling_transaction_input_decoder = (p: Parser): SaplingTransactionInput => {
    return record_decoder<SaplingTransactionInput>({cv: FixedBytes.decode<32>({len: 32}), nf: FixedBytes.decode<32>({len: 32}), rk: FixedBytes.decode<32>({len: 32}), proof_i: FixedBytes.decode<192>({len: 192}), signature: FixedBytes.decode<64>({len: 64})}, {order: ['cv', 'nf', 'rk', 'proof_i', 'signature']})(p);
}
