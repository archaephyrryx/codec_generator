import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type SaplingTransactionBindingSig = FixedBytes<64>;
export class CGRIDClass__SaplingTransactionBindingSig extends Box<SaplingTransactionBindingSig> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionBindingSig {
        return new this(FixedBytes.decode<64>({len: 64})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const sapling_transaction_binding_sig_encoder = (value: SaplingTransactionBindingSig): OutputBytes => {
    return value.encode();
}
export const sapling_transaction_binding_sig_decoder = (p: Parser): SaplingTransactionBindingSig => {
    return FixedBytes.decode<64>({len: 64})(p);
}
