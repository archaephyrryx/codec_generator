import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type Proto011PtHangz2Seed = FixedBytes<32>;
export class CGRIDClass__Proto011PtHangz2Seed extends Box<Proto011PtHangz2Seed> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Seed {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto011_pthangz2_seed_encoder = (value: Proto011PtHangz2Seed): OutputBytes => {
    return value.encode();
}
export const proto011_pthangz2_seed_decoder = (p: Parser): Proto011PtHangz2Seed => {
    return FixedBytes.decode<32>({len: 32})(p);
}
