import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto011PtHangz2Period = Int64;
export class CGRIDClass__Proto011PtHangz2Period extends Box<Proto011PtHangz2Period> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Period {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto011_pthangz2_period_encoder = (value: Proto011PtHangz2Period): OutputBytes => {
    return value.encode();
}
export const proto011_pthangz2_period_decoder = (p: Parser): Proto011PtHangz2Period => {
    return Int64.decode(p);
}
