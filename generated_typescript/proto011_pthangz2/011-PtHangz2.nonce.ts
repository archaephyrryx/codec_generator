import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type Proto011PtHangz2Nonce = FixedBytes<32>;
export class CGRIDClass__Proto011PtHangz2Nonce extends Box<Proto011PtHangz2Nonce> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Nonce {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto011_pthangz2_nonce_encoder = (value: Proto011PtHangz2Nonce): OutputBytes => {
    return value.encode();
}
export const proto011_pthangz2_nonce_decoder = (p: Parser): Proto011PtHangz2Nonce => {
    return FixedBytes.decode<32>({len: 32})(p);
}
