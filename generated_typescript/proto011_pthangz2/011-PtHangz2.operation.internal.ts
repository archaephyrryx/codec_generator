import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Transaction generated for Proto011PtHangz2OperationAlphaInternalOperationRhs__Transaction
export class CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Transaction extends Box<Proto011PtHangz2OperationAlphaInternalOperationRhs__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Transaction {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaInternalOperationRhs__Transaction>({amount: N.decode, destination: CGRIDClass__Proto011_PtHangz2Contract_id.decode, parameters: Option.decode(CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Transaction_parameters.decode)}, {order: ['amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Reveal generated for Proto011PtHangz2OperationAlphaInternalOperationRhs__Reveal
export class CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Reveal extends Box<Proto011PtHangz2OperationAlphaInternalOperationRhs__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Reveal {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaInternalOperationRhs__Reveal>({public_key: CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Reveal_public_key.decode}, {order: ['public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Register_global_constant generated for Proto011PtHangz2OperationAlphaInternalOperationRhs__Register_global_constant
export class CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Register_global_constant extends Box<Proto011PtHangz2OperationAlphaInternalOperationRhs__Register_global_constant> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Register_global_constant {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaInternalOperationRhs__Register_global_constant>({value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['value']})(p));
    };
    get encodeLength(): number {
        return (this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Origination generated for Proto011PtHangz2OperationAlphaInternalOperationRhs__Origination
export class CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Origination extends Box<Proto011PtHangz2OperationAlphaInternalOperationRhs__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Origination {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaInternalOperationRhs__Origination>({balance: N.decode, delegate: Option.decode(CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Origination_delegate.decode), script: CGRIDClass__Proto011_PtHangz2ScriptedContracts.decode}, {order: ['balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Delegation generated for Proto011PtHangz2OperationAlphaInternalOperationRhs__Delegation
export class CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Delegation extends Box<Proto011PtHangz2OperationAlphaInternalOperationRhs__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Delegation {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaInternalOperationRhs__Delegation>({delegate: Option.decode(CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Delegation_delegate.decode)}, {order: ['delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint__set_delegate generated for Proto011PtHangz2Entrypoint__set_delegate
export class CGRIDClass__Proto011PtHangz2Entrypoint__set_delegate extends Box<Proto011PtHangz2Entrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint__root generated for Proto011PtHangz2Entrypoint__root
export class CGRIDClass__Proto011PtHangz2Entrypoint__root extends Box<Proto011PtHangz2Entrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint__remove_delegate generated for Proto011PtHangz2Entrypoint__remove_delegate
export class CGRIDClass__Proto011PtHangz2Entrypoint__remove_delegate extends Box<Proto011PtHangz2Entrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint__named generated for Proto011PtHangz2Entrypoint__named
export class CGRIDClass__Proto011PtHangz2Entrypoint__named extends Box<Proto011PtHangz2Entrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint___do generated for Proto011PtHangz2Entrypoint___do
export class CGRIDClass__Proto011PtHangz2Entrypoint___do extends Box<Proto011PtHangz2Entrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint___default generated for Proto011PtHangz2Entrypoint___default
export class CGRIDClass__Proto011PtHangz2Entrypoint___default extends Box<Proto011PtHangz2Entrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2ContractId__Originated generated for Proto011PtHangz2ContractId__Originated
export class CGRIDClass__Proto011PtHangz2ContractId__Originated extends Box<Proto011PtHangz2ContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2ContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2ContractId__Implicit generated for Proto011PtHangz2ContractId__Implicit
export class CGRIDClass__Proto011PtHangz2ContractId__Implicit extends Box<Proto011PtHangz2ContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2ContractId__Implicit {
        return new this(record_decoder<Proto011PtHangz2ContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto011PtHangz2OperationAlphaInternalOperationRhs__Transaction = { amount: N, destination: CGRIDClass__Proto011_PtHangz2Contract_id, parameters: Option<CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Transaction_parameters> };
export type Proto011PtHangz2OperationAlphaInternalOperationRhs__Reveal = { public_key: CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Reveal_public_key };
export type Proto011PtHangz2OperationAlphaInternalOperationRhs__Register_global_constant = { value: Dynamic<Bytes,width.Uint30> };
export type Proto011PtHangz2OperationAlphaInternalOperationRhs__Origination = { balance: N, delegate: Option<CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Origination_delegate>, script: CGRIDClass__Proto011_PtHangz2ScriptedContracts };
export type Proto011PtHangz2OperationAlphaInternalOperationRhs__Delegation = { delegate: Option<CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Delegation_delegate> };
export type Proto011PtHangz2Entrypoint__set_delegate = Unit;
export type Proto011PtHangz2Entrypoint__root = Unit;
export type Proto011PtHangz2Entrypoint__remove_delegate = Unit;
export type Proto011PtHangz2Entrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto011PtHangz2Entrypoint___do = Unit;
export type Proto011PtHangz2Entrypoint___default = Unit;
export type Proto011PtHangz2ContractId__Originated = Padded<CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad,1>;
export type Proto011PtHangz2ContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2ScriptedContracts generated for Proto011PtHangz2ScriptedContracts
export class CGRIDClass__Proto011_PtHangz2ScriptedContracts extends Box<Proto011PtHangz2ScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2ScriptedContracts {
        return new this(record_decoder<Proto011PtHangz2ScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_rhs generated for Proto011PtHangz2OperationAlphaInternalOperationRhs
export function proto011pthangz2operationalphainternaloperationrhs_mkDecoder(): VariantDecoder<CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs,Proto011PtHangz2OperationAlphaInternalOperationRhs> {
    function f(disc: CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs) {
        switch (disc) {
            case CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs.Reveal: return CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Reveal.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs.Transaction: return CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Transaction.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs.Origination: return CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Origination.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs.Delegation: return CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Delegation.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs.Register_global_constant: return CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Register_global_constant.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs => Object.values(CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs).includes(tagval);
    return f;
}
export class CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_rhs extends Box<Proto011PtHangz2OperationAlphaInternalOperationRhs> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto011PtHangz2OperationAlphaInternalOperationRhs>, Proto011PtHangz2OperationAlphaInternalOperationRhs>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_rhs {
        return new this(variant_decoder(width.Uint8)(proto011pthangz2operationalphainternaloperationrhs_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Transaction_parameters generated for Proto011PtHangz2OperationAlphaInternalOperationTransactionParameters
export class CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Transaction_parameters extends Box<Proto011PtHangz2OperationAlphaInternalOperationTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Transaction_parameters {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaInternalOperationTransactionParameters>({entrypoint: CGRIDClass__Proto011_PtHangz2Entrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Reveal_public_key generated for Proto011PtHangz2OperationAlphaInternalOperationRevealPublicKey
export class CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Reveal_public_key extends Box<Proto011PtHangz2OperationAlphaInternalOperationRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Reveal_public_key {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaInternalOperationRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Origination_delegate generated for Proto011PtHangz2OperationAlphaInternalOperationOriginationDelegate
export class CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Origination_delegate extends Box<Proto011PtHangz2OperationAlphaInternalOperationOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Origination_delegate {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaInternalOperationOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Delegation_delegate generated for Proto011PtHangz2OperationAlphaInternalOperationDelegationDelegate
export class CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Delegation_delegate extends Box<Proto011PtHangz2OperationAlphaInternalOperationDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_Delegation_delegate {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaInternalOperationDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Entrypoint generated for Proto011PtHangz2Entrypoint
export function proto011pthangz2entrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto011PtHangz2Entrypoint,Proto011PtHangz2Entrypoint> {
    function f(disc: CGRIDTag__Proto011PtHangz2Entrypoint) {
        switch (disc) {
            case CGRIDTag__Proto011PtHangz2Entrypoint._default: return CGRIDClass__Proto011PtHangz2Entrypoint___default.decode;
            case CGRIDTag__Proto011PtHangz2Entrypoint.root: return CGRIDClass__Proto011PtHangz2Entrypoint__root.decode;
            case CGRIDTag__Proto011PtHangz2Entrypoint._do: return CGRIDClass__Proto011PtHangz2Entrypoint___do.decode;
            case CGRIDTag__Proto011PtHangz2Entrypoint.set_delegate: return CGRIDClass__Proto011PtHangz2Entrypoint__set_delegate.decode;
            case CGRIDTag__Proto011PtHangz2Entrypoint.remove_delegate: return CGRIDClass__Proto011PtHangz2Entrypoint__remove_delegate.decode;
            case CGRIDTag__Proto011PtHangz2Entrypoint.named: return CGRIDClass__Proto011PtHangz2Entrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto011PtHangz2Entrypoint => Object.values(CGRIDTag__Proto011PtHangz2Entrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto011_PtHangz2Entrypoint extends Box<Proto011PtHangz2Entrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto011PtHangz2Entrypoint>, Proto011PtHangz2Entrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Entrypoint {
        return new this(variant_decoder(width.Uint8)(proto011pthangz2entrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad generated for Proto011PtHangz2ContractIdOriginatedDenestPad
export class CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad extends Box<Proto011PtHangz2ContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad {
        return new this(record_decoder<Proto011PtHangz2ContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Contract_id generated for Proto011PtHangz2ContractId
export function proto011pthangz2contractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto011PtHangz2ContractId,Proto011PtHangz2ContractId> {
    function f(disc: CGRIDTag__Proto011PtHangz2ContractId) {
        switch (disc) {
            case CGRIDTag__Proto011PtHangz2ContractId.Implicit: return CGRIDClass__Proto011PtHangz2ContractId__Implicit.decode;
            case CGRIDTag__Proto011PtHangz2ContractId.Originated: return CGRIDClass__Proto011PtHangz2ContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto011PtHangz2ContractId => Object.values(CGRIDTag__Proto011PtHangz2ContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto011_PtHangz2Contract_id extends Box<Proto011PtHangz2ContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto011PtHangz2ContractId>, Proto011PtHangz2ContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Contract_id {
        return new this(variant_decoder(width.Uint8)(proto011pthangz2contractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type Proto011PtHangz2ScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export enum CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs{
    Reveal = 0,
    Transaction = 1,
    Origination = 2,
    Delegation = 3,
    Register_global_constant = 4
}
export interface CGRIDMap__Proto011PtHangz2OperationAlphaInternalOperationRhs {
    Reveal: CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Reveal,
    Transaction: CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Transaction,
    Origination: CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Origination,
    Delegation: CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Delegation,
    Register_global_constant: CGRIDClass__Proto011PtHangz2OperationAlphaInternalOperationRhs__Register_global_constant
}
export type Proto011PtHangz2OperationAlphaInternalOperationRhs = { kind: CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs.Reveal, value: CGRIDMap__Proto011PtHangz2OperationAlphaInternalOperationRhs['Reveal'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs.Transaction, value: CGRIDMap__Proto011PtHangz2OperationAlphaInternalOperationRhs['Transaction'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs.Origination, value: CGRIDMap__Proto011PtHangz2OperationAlphaInternalOperationRhs['Origination'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs.Delegation, value: CGRIDMap__Proto011PtHangz2OperationAlphaInternalOperationRhs['Delegation'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaInternalOperationRhs.Register_global_constant, value: CGRIDMap__Proto011PtHangz2OperationAlphaInternalOperationRhs['Register_global_constant'] };
export type Proto011PtHangz2OperationAlphaInternalOperationTransactionParameters = { entrypoint: CGRIDClass__Proto011_PtHangz2Entrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto011PtHangz2OperationAlphaInternalOperationRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto011PtHangz2OperationAlphaInternalOperationOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationAlphaInternalOperationDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto011PtHangz2Entrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto011PtHangz2Entrypoint {
    _default: CGRIDClass__Proto011PtHangz2Entrypoint___default,
    root: CGRIDClass__Proto011PtHangz2Entrypoint__root,
    _do: CGRIDClass__Proto011PtHangz2Entrypoint___do,
    set_delegate: CGRIDClass__Proto011PtHangz2Entrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto011PtHangz2Entrypoint__remove_delegate,
    named: CGRIDClass__Proto011PtHangz2Entrypoint__named
}
export type Proto011PtHangz2Entrypoint = { kind: CGRIDTag__Proto011PtHangz2Entrypoint._default, value: CGRIDMap__Proto011PtHangz2Entrypoint['_default'] } | { kind: CGRIDTag__Proto011PtHangz2Entrypoint.root, value: CGRIDMap__Proto011PtHangz2Entrypoint['root'] } | { kind: CGRIDTag__Proto011PtHangz2Entrypoint._do, value: CGRIDMap__Proto011PtHangz2Entrypoint['_do'] } | { kind: CGRIDTag__Proto011PtHangz2Entrypoint.set_delegate, value: CGRIDMap__Proto011PtHangz2Entrypoint['set_delegate'] } | { kind: CGRIDTag__Proto011PtHangz2Entrypoint.remove_delegate, value: CGRIDMap__Proto011PtHangz2Entrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto011PtHangz2Entrypoint.named, value: CGRIDMap__Proto011PtHangz2Entrypoint['named'] };
export type Proto011PtHangz2ContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto011PtHangz2ContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto011PtHangz2ContractId {
    Implicit: CGRIDClass__Proto011PtHangz2ContractId__Implicit,
    Originated: CGRIDClass__Proto011PtHangz2ContractId__Originated
}
export type Proto011PtHangz2ContractId = { kind: CGRIDTag__Proto011PtHangz2ContractId.Implicit, value: CGRIDMap__Proto011PtHangz2ContractId['Implicit'] } | { kind: CGRIDTag__Proto011PtHangz2ContractId.Originated, value: CGRIDMap__Proto011PtHangz2ContractId['Originated'] };
export type Proto011PtHangz2OperationInternal = { source: CGRIDClass__Proto011_PtHangz2Contract_id, nonce: Uint16, proto011_pthangz2_operation_alpha_internal_operation_rhs: CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_rhs };
export class CGRIDClass__Proto011PtHangz2OperationInternal extends Box<Proto011PtHangz2OperationInternal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'nonce', 'proto011_pthangz2_operation_alpha_internal_operation_rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationInternal {
        return new this(record_decoder<Proto011PtHangz2OperationInternal>({source: CGRIDClass__Proto011_PtHangz2Contract_id.decode, nonce: Uint16.decode, proto011_pthangz2_operation_alpha_internal_operation_rhs: CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto011_pthangz2_operation_alpha_internal_operation_rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.nonce.encodeLength +  this.value.proto011_pthangz2_operation_alpha_internal_operation_rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt) +  this.value.proto011_pthangz2_operation_alpha_internal_operation_rhs.writeTarget(tgt));
    }
}
export const proto011_pthangz2_operation_internal_encoder = (value: Proto011PtHangz2OperationInternal): OutputBytes => {
    return record_encoder({order: ['source', 'nonce', 'proto011_pthangz2_operation_alpha_internal_operation_rhs']})(value);
}
export const proto011_pthangz2_operation_internal_decoder = (p: Parser): Proto011PtHangz2OperationInternal => {
    return record_decoder<Proto011PtHangz2OperationInternal>({source: CGRIDClass__Proto011_PtHangz2Contract_id.decode, nonce: Uint16.decode, proto011_pthangz2_operation_alpha_internal_operation_rhs: CGRIDClass__Proto011_PtHangz2OperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto011_pthangz2_operation_alpha_internal_operation_rhs']})(p);
}
