import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Subsidy generated for Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Subsidy
export class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Subsidy extends Box<Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Subsidy> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Subsidy {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Protocol_migration generated for Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Protocol_migration
export class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Protocol_migration extends Box<Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Protocol_migration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Protocol_migration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Block_application generated for Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Block_application
export class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Block_application extends Box<Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Block_application> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Block_application {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Rewards generated for Proto011PtHangz2OperationMetadataAlphaBalance__Rewards
export class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Rewards extends Box<Proto011PtHangz2OperationMetadataAlphaBalance__Rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Rewards {
        return new this(record_decoder<Proto011PtHangz2OperationMetadataAlphaBalance__Rewards>({category: Unit.decode, delegate: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Rewards_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Fees generated for Proto011PtHangz2OperationMetadataAlphaBalance__Fees
export class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Fees extends Box<Proto011PtHangz2OperationMetadataAlphaBalance__Fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Fees {
        return new this(record_decoder<Proto011PtHangz2OperationMetadataAlphaBalance__Fees>({category: Unit.decode, delegate: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Fees_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Deposits generated for Proto011PtHangz2OperationMetadataAlphaBalance__Deposits
export class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Deposits extends Box<Proto011PtHangz2OperationMetadataAlphaBalance__Deposits> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Deposits {
        return new this(record_decoder<Proto011PtHangz2OperationMetadataAlphaBalance__Deposits>({category: Unit.decode, delegate: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Deposits_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Contract generated for Proto011PtHangz2OperationMetadataAlphaBalance__Contract
export class CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Contract extends Box<Proto011PtHangz2OperationMetadataAlphaBalance__Contract> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Contract {
        return new this(record_decoder<Proto011PtHangz2OperationMetadataAlphaBalance__Contract>({contract: CGRIDClass__Proto011_PtHangz2Contract_id.decode, change: Int64.decode, origin: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaUpdate_origin_origin.decode}, {order: ['contract', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2ContractId__Originated generated for Proto011PtHangz2ContractId__Originated
export class CGRIDClass__Proto011PtHangz2ContractId__Originated extends Box<Proto011PtHangz2ContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2ContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2ContractId__Implicit generated for Proto011PtHangz2ContractId__Implicit
export class CGRIDClass__Proto011PtHangz2ContractId__Implicit extends Box<Proto011PtHangz2ContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2ContractId__Implicit {
        return new this(record_decoder<Proto011PtHangz2ContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Subsidy = Unit;
export type Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Protocol_migration = Unit;
export type Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Block_application = Unit;
export type Proto011PtHangz2OperationMetadataAlphaBalance__Rewards = { category: Unit, delegate: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Rewards_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaUpdate_origin_origin };
export type Proto011PtHangz2OperationMetadataAlphaBalance__Fees = { category: Unit, delegate: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Fees_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaUpdate_origin_origin };
export type Proto011PtHangz2OperationMetadataAlphaBalance__Deposits = { category: Unit, delegate: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Deposits_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaUpdate_origin_origin };
export type Proto011PtHangz2OperationMetadataAlphaBalance__Contract = { contract: CGRIDClass__Proto011_PtHangz2Contract_id, change: Int64, origin: CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaUpdate_origin_origin };
export type Proto011PtHangz2ContractId__Originated = Padded<CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad,1>;
export type Proto011PtHangz2ContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaUpdate_origin_origin generated for Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin
export function proto011pthangz2operationmetadataalphaupdateoriginorigin_mkDecoder(): VariantDecoder<CGRIDTag__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin,Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin> {
    function f(disc: CGRIDTag__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin) {
        switch (disc) {
            case CGRIDTag__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin.Block_application: return CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Block_application.decode;
            case CGRIDTag__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin.Protocol_migration: return CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Protocol_migration.decode;
            case CGRIDTag__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin.Subsidy: return CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Subsidy.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin => Object.values(CGRIDTag__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin).includes(tagval);
    return f;
}
export class CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaUpdate_origin_origin extends Box<Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin>, Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaUpdate_origin_origin {
        return new this(variant_decoder(width.Uint8)(proto011pthangz2operationmetadataalphaupdateoriginorigin_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Rewards_delegate generated for Proto011PtHangz2OperationMetadataAlphaBalanceRewardsDelegate
export class CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Rewards_delegate extends Box<Proto011PtHangz2OperationMetadataAlphaBalanceRewardsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Rewards_delegate {
        return new this(record_decoder<Proto011PtHangz2OperationMetadataAlphaBalanceRewardsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Fees_delegate generated for Proto011PtHangz2OperationMetadataAlphaBalanceFeesDelegate
export class CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Fees_delegate extends Box<Proto011PtHangz2OperationMetadataAlphaBalanceFeesDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Fees_delegate {
        return new this(record_decoder<Proto011PtHangz2OperationMetadataAlphaBalanceFeesDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Deposits_delegate generated for Proto011PtHangz2OperationMetadataAlphaBalanceDepositsDelegate
export class CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Deposits_delegate extends Box<Proto011PtHangz2OperationMetadataAlphaBalanceDepositsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance_Deposits_delegate {
        return new this(record_decoder<Proto011PtHangz2OperationMetadataAlphaBalanceDepositsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance generated for Proto011PtHangz2OperationMetadataAlphaBalance
export function proto011pthangz2operationmetadataalphabalance_mkDecoder(): VariantDecoder<CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance,Proto011PtHangz2OperationMetadataAlphaBalance> {
    function f(disc: CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance) {
        switch (disc) {
            case CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance.Contract: return CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Contract.decode;
            case CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance.Rewards: return CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Rewards.decode;
            case CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance.Fees: return CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Fees.decode;
            case CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance.Deposits: return CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Deposits.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance => Object.values(CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance).includes(tagval);
    return f;
}
export class CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance extends Box<Proto011PtHangz2OperationMetadataAlphaBalance> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto011PtHangz2OperationMetadataAlphaBalance>, Proto011PtHangz2OperationMetadataAlphaBalance>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance {
        return new this(variant_decoder(width.Uint8)(proto011pthangz2operationmetadataalphabalance_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad generated for Proto011PtHangz2ContractIdOriginatedDenestPad
export class CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad extends Box<Proto011PtHangz2ContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad {
        return new this(record_decoder<Proto011PtHangz2ContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Contract_id generated for Proto011PtHangz2ContractId
export function proto011pthangz2contractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto011PtHangz2ContractId,Proto011PtHangz2ContractId> {
    function f(disc: CGRIDTag__Proto011PtHangz2ContractId) {
        switch (disc) {
            case CGRIDTag__Proto011PtHangz2ContractId.Implicit: return CGRIDClass__Proto011PtHangz2ContractId__Implicit.decode;
            case CGRIDTag__Proto011PtHangz2ContractId.Originated: return CGRIDClass__Proto011PtHangz2ContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto011PtHangz2ContractId => Object.values(CGRIDTag__Proto011PtHangz2ContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto011_PtHangz2Contract_id extends Box<Proto011PtHangz2ContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto011PtHangz2ContractId>, Proto011PtHangz2ContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Contract_id {
        return new this(variant_decoder(width.Uint8)(proto011pthangz2contractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin{
    Block_application = 0,
    Protocol_migration = 1,
    Subsidy = 2
}
export interface CGRIDMap__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin {
    Block_application: CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Block_application,
    Protocol_migration: CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Protocol_migration,
    Subsidy: CGRIDClass__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin__Subsidy
}
export type Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin = { kind: CGRIDTag__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin.Block_application, value: CGRIDMap__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin['Block_application'] } | { kind: CGRIDTag__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin.Protocol_migration, value: CGRIDMap__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin['Protocol_migration'] } | { kind: CGRIDTag__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin.Subsidy, value: CGRIDMap__Proto011PtHangz2OperationMetadataAlphaUpdateOriginOrigin['Subsidy'] };
export type Proto011PtHangz2OperationMetadataAlphaBalanceRewardsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationMetadataAlphaBalanceFeesDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationMetadataAlphaBalanceDepositsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance{
    Contract = 0,
    Rewards = 1,
    Fees = 2,
    Deposits = 3
}
export interface CGRIDMap__Proto011PtHangz2OperationMetadataAlphaBalance {
    Contract: CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Contract,
    Rewards: CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Rewards,
    Fees: CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Fees,
    Deposits: CGRIDClass__Proto011PtHangz2OperationMetadataAlphaBalance__Deposits
}
export type Proto011PtHangz2OperationMetadataAlphaBalance = { kind: CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance.Contract, value: CGRIDMap__Proto011PtHangz2OperationMetadataAlphaBalance['Contract'] } | { kind: CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance.Rewards, value: CGRIDMap__Proto011PtHangz2OperationMetadataAlphaBalance['Rewards'] } | { kind: CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance.Fees, value: CGRIDMap__Proto011PtHangz2OperationMetadataAlphaBalance['Fees'] } | { kind: CGRIDTag__Proto011PtHangz2OperationMetadataAlphaBalance.Deposits, value: CGRIDMap__Proto011PtHangz2OperationMetadataAlphaBalance['Deposits'] };
export type Proto011PtHangz2ContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto011PtHangz2ContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto011PtHangz2ContractId {
    Implicit: CGRIDClass__Proto011PtHangz2ContractId__Implicit,
    Originated: CGRIDClass__Proto011PtHangz2ContractId__Originated
}
export type Proto011PtHangz2ContractId = { kind: CGRIDTag__Proto011PtHangz2ContractId.Implicit, value: CGRIDMap__Proto011PtHangz2ContractId['Implicit'] } | { kind: CGRIDTag__Proto011PtHangz2ContractId.Originated, value: CGRIDMap__Proto011PtHangz2ContractId['Originated'] };
export type Proto011PtHangz2ReceiptBalanceUpdates = Dynamic<Sequence<CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance>,width.Uint30>;
export class CGRIDClass__Proto011PtHangz2ReceiptBalanceUpdates extends Box<Proto011PtHangz2ReceiptBalanceUpdates> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2ReceiptBalanceUpdates {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto011_pthangz2_receipt_balance_updates_encoder = (value: Proto011PtHangz2ReceiptBalanceUpdates): OutputBytes => {
    return value.encode();
}
export const proto011_pthangz2_receipt_balance_updates_decoder = (p: Parser): Proto011PtHangz2ReceiptBalanceUpdates => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto011_PtHangz2Operation_metadataAlphaBalance.decode), width.Uint30)(p);
}
