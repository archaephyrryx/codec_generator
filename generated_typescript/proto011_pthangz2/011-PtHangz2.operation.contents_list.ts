import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Nullable } from '../../ts_runtime/composite/opt/nullable';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int64, Int8, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Transaction generated for Proto011PtHangz2OperationAlphaContents__Transaction
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Transaction extends Box<Proto011PtHangz2OperationAlphaContents__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Transaction {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Transaction>({source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Transaction_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: N.decode, destination: CGRIDClass__Proto011_PtHangz2Contract_id.decode, parameters: Option.decode(CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Transaction_parameters.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Seed_nonce_revelation generated for Proto011PtHangz2OperationAlphaContents__Seed_nonce_revelation
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Seed_nonce_revelation extends Box<Proto011PtHangz2OperationAlphaContents__Seed_nonce_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Seed_nonce_revelation {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Seed_nonce_revelation>({level: Int32.decode, nonce: FixedBytes.decode<32>({len: 32})}, {order: ['level', 'nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Reveal generated for Proto011PtHangz2OperationAlphaContents__Reveal
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Reveal extends Box<Proto011PtHangz2OperationAlphaContents__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Reveal {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Reveal>({source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Reveal_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_key: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Reveal_public_key.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Register_global_constant generated for Proto011PtHangz2OperationAlphaContents__Register_global_constant
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Register_global_constant extends Box<Proto011PtHangz2OperationAlphaContents__Register_global_constant> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Register_global_constant {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Register_global_constant>({source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Register_global_constant_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Proposals generated for Proto011PtHangz2OperationAlphaContents__Proposals
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Proposals extends Box<Proto011PtHangz2OperationAlphaContents__Proposals> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposals']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Proposals {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Proposals>({source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Proposals_source.decode, period: Int32.decode, proposals: Dynamic.decode(Sequence.decode(CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'period', 'proposals']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposals.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposals.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Origination generated for Proto011PtHangz2OperationAlphaContents__Origination
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Origination extends Box<Proto011PtHangz2OperationAlphaContents__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Origination {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Origination>({source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, balance: N.decode, delegate: Option.decode(CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Origination_delegate.decode), script: CGRIDClass__Proto011_PtHangz2ScriptedContracts.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Failing_noop generated for Proto011PtHangz2OperationAlphaContents__Failing_noop
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Failing_noop extends Box<Proto011PtHangz2OperationAlphaContents__Failing_noop> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['arbitrary']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Failing_noop {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Failing_noop>({arbitrary: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['arbitrary']})(p));
    };
    get encodeLength(): number {
        return (this.value.arbitrary.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.arbitrary.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Endorsement_with_slot generated for Proto011PtHangz2OperationAlphaContents__Endorsement_with_slot
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Endorsement_with_slot extends Box<Proto011PtHangz2OperationAlphaContents__Endorsement_with_slot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['endorsement', 'slot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Endorsement_with_slot {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Endorsement_with_slot>({endorsement: Dynamic.decode(CGRIDClass__Proto011_PtHangz2InlinedEndorsement.decode, width.Uint30), slot: Uint16.decode}, {order: ['endorsement', 'slot']})(p));
    };
    get encodeLength(): number {
        return (this.value.endorsement.encodeLength +  this.value.slot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.endorsement.writeTarget(tgt) +  this.value.slot.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Endorsement generated for Proto011PtHangz2OperationAlphaContents__Endorsement
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Endorsement extends Box<Proto011PtHangz2OperationAlphaContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Endorsement {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Endorsement>({level: Int32.decode}, {order: ['level']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Double_endorsement_evidence generated for Proto011PtHangz2OperationAlphaContents__Double_endorsement_evidence
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Double_endorsement_evidence extends Box<Proto011PtHangz2OperationAlphaContents__Double_endorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2', 'slot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Double_endorsement_evidence {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Double_endorsement_evidence>({op1: Dynamic.decode(CGRIDClass__Proto011_PtHangz2InlinedEndorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__Proto011_PtHangz2InlinedEndorsement.decode, width.Uint30), slot: Uint16.decode}, {order: ['op1', 'op2', 'slot']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength +  this.value.slot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt) +  this.value.slot.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Double_baking_evidence generated for Proto011PtHangz2OperationAlphaContents__Double_baking_evidence
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Double_baking_evidence extends Box<Proto011PtHangz2OperationAlphaContents__Double_baking_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bh1', 'bh2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Double_baking_evidence {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Double_baking_evidence>({bh1: Dynamic.decode(CGRIDClass__Proto011_PtHangz2Block_headerAlphaFull_header.decode, width.Uint30), bh2: Dynamic.decode(CGRIDClass__Proto011_PtHangz2Block_headerAlphaFull_header.decode, width.Uint30)}, {order: ['bh1', 'bh2']})(p));
    };
    get encodeLength(): number {
        return (this.value.bh1.encodeLength +  this.value.bh2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bh1.writeTarget(tgt) +  this.value.bh2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Delegation generated for Proto011PtHangz2OperationAlphaContents__Delegation
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Delegation extends Box<Proto011PtHangz2OperationAlphaContents__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Delegation {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Delegation>({source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Delegation_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, delegate: Option.decode(CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Delegation_delegate.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Ballot generated for Proto011PtHangz2OperationAlphaContents__Ballot
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Ballot extends Box<Proto011PtHangz2OperationAlphaContents__Ballot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposal', 'ballot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Ballot {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Ballot>({source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Ballot_source.decode, period: Int32.decode, proposal: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Ballot_proposal.decode, ballot: Int8.decode}, {order: ['source', 'period', 'proposal', 'ballot']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposal.encodeLength +  this.value.ballot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposal.writeTarget(tgt) +  this.value.ballot.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Activate_account generated for Proto011PtHangz2OperationAlphaContents__Activate_account
export class CGRIDClass__Proto011PtHangz2OperationAlphaContents__Activate_account extends Box<Proto011PtHangz2OperationAlphaContents__Activate_account> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'secret']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationAlphaContents__Activate_account {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContents__Activate_account>({pkh: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Activate_account_pkh.decode, secret: FixedBytes.decode<20>({len: 20})}, {order: ['pkh', 'secret']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.secret.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.secret.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2InlinedEndorsementContents__Endorsement generated for Proto011PtHangz2InlinedEndorsementContents__Endorsement
export class CGRIDClass__Proto011PtHangz2InlinedEndorsementContents__Endorsement extends Box<Proto011PtHangz2InlinedEndorsementContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2InlinedEndorsementContents__Endorsement {
        return new this(record_decoder<Proto011PtHangz2InlinedEndorsementContents__Endorsement>({level: Int32.decode}, {order: ['level']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint__set_delegate generated for Proto011PtHangz2Entrypoint__set_delegate
export class CGRIDClass__Proto011PtHangz2Entrypoint__set_delegate extends Box<Proto011PtHangz2Entrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint__root generated for Proto011PtHangz2Entrypoint__root
export class CGRIDClass__Proto011PtHangz2Entrypoint__root extends Box<Proto011PtHangz2Entrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint__remove_delegate generated for Proto011PtHangz2Entrypoint__remove_delegate
export class CGRIDClass__Proto011PtHangz2Entrypoint__remove_delegate extends Box<Proto011PtHangz2Entrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint__named generated for Proto011PtHangz2Entrypoint__named
export class CGRIDClass__Proto011PtHangz2Entrypoint__named extends Box<Proto011PtHangz2Entrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint___do generated for Proto011PtHangz2Entrypoint___do
export class CGRIDClass__Proto011PtHangz2Entrypoint___do extends Box<Proto011PtHangz2Entrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2Entrypoint___default generated for Proto011PtHangz2Entrypoint___default
export class CGRIDClass__Proto011PtHangz2Entrypoint___default extends Box<Proto011PtHangz2Entrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Entrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2ContractId__Originated generated for Proto011PtHangz2ContractId__Originated
export class CGRIDClass__Proto011PtHangz2ContractId__Originated extends Box<Proto011PtHangz2ContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2ContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2ContractId__Implicit generated for Proto011PtHangz2ContractId__Implicit
export class CGRIDClass__Proto011PtHangz2ContractId__Implicit extends Box<Proto011PtHangz2ContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2ContractId__Implicit {
        return new this(record_decoder<Proto011PtHangz2ContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto011PtHangz2OperationAlphaContents__Transaction = { source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Transaction_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: N, destination: CGRIDClass__Proto011_PtHangz2Contract_id, parameters: Option<CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Transaction_parameters> };
export type Proto011PtHangz2OperationAlphaContents__Seed_nonce_revelation = { level: Int32, nonce: FixedBytes<32> };
export type Proto011PtHangz2OperationAlphaContents__Reveal = { source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Reveal_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_key: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Reveal_public_key };
export type Proto011PtHangz2OperationAlphaContents__Register_global_constant = { source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Register_global_constant_source, fee: N, counter: N, gas_limit: N, storage_limit: N, value: Dynamic<Bytes,width.Uint30> };
export type Proto011PtHangz2OperationAlphaContents__Proposals = { source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Proposals_source, period: Int32, proposals: Dynamic<Sequence<CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq>,width.Uint30> };
export type Proto011PtHangz2OperationAlphaContents__Origination = { source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, balance: N, delegate: Option<CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Origination_delegate>, script: CGRIDClass__Proto011_PtHangz2ScriptedContracts };
export type Proto011PtHangz2OperationAlphaContents__Failing_noop = { arbitrary: Dynamic<U8String,width.Uint30> };
export type Proto011PtHangz2OperationAlphaContents__Endorsement_with_slot = { endorsement: Dynamic<CGRIDClass__Proto011_PtHangz2InlinedEndorsement,width.Uint30>, slot: Uint16 };
export type Proto011PtHangz2OperationAlphaContents__Endorsement = { level: Int32 };
export type Proto011PtHangz2OperationAlphaContents__Double_endorsement_evidence = { op1: Dynamic<CGRIDClass__Proto011_PtHangz2InlinedEndorsement,width.Uint30>, op2: Dynamic<CGRIDClass__Proto011_PtHangz2InlinedEndorsement,width.Uint30>, slot: Uint16 };
export type Proto011PtHangz2OperationAlphaContents__Double_baking_evidence = { bh1: Dynamic<CGRIDClass__Proto011_PtHangz2Block_headerAlphaFull_header,width.Uint30>, bh2: Dynamic<CGRIDClass__Proto011_PtHangz2Block_headerAlphaFull_header,width.Uint30> };
export type Proto011PtHangz2OperationAlphaContents__Delegation = { source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Delegation_source, fee: N, counter: N, gas_limit: N, storage_limit: N, delegate: Option<CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Delegation_delegate> };
export type Proto011PtHangz2OperationAlphaContents__Ballot = { source: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Ballot_source, period: Int32, proposal: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Ballot_proposal, ballot: Int8 };
export type Proto011PtHangz2OperationAlphaContents__Activate_account = { pkh: CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Activate_account_pkh, secret: FixedBytes<20> };
export type Proto011PtHangz2InlinedEndorsementContents__Endorsement = { level: Int32 };
export type Proto011PtHangz2Entrypoint__set_delegate = Unit;
export type Proto011PtHangz2Entrypoint__root = Unit;
export type Proto011PtHangz2Entrypoint__remove_delegate = Unit;
export type Proto011PtHangz2Entrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto011PtHangz2Entrypoint___do = Unit;
export type Proto011PtHangz2Entrypoint___default = Unit;
export type Proto011PtHangz2ContractId__Originated = Padded<CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad,1>;
export type Proto011PtHangz2ContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2ScriptedContracts generated for Proto011PtHangz2ScriptedContracts
export class CGRIDClass__Proto011_PtHangz2ScriptedContracts extends Box<Proto011PtHangz2ScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2ScriptedContracts {
        return new this(record_decoder<Proto011PtHangz2ScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Transaction_source generated for Proto011PtHangz2OperationAlphaContentsTransactionSource
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Transaction_source extends Box<Proto011PtHangz2OperationAlphaContentsTransactionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Transaction_source {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsTransactionSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Transaction_parameters generated for Proto011PtHangz2OperationAlphaContentsTransactionParameters
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Transaction_parameters extends Box<Proto011PtHangz2OperationAlphaContentsTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Transaction_parameters {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsTransactionParameters>({entrypoint: CGRIDClass__Proto011_PtHangz2Entrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Reveal_source generated for Proto011PtHangz2OperationAlphaContentsRevealSource
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Reveal_source extends Box<Proto011PtHangz2OperationAlphaContentsRevealSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Reveal_source {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsRevealSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Reveal_public_key generated for Proto011PtHangz2OperationAlphaContentsRevealPublicKey
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Reveal_public_key extends Box<Proto011PtHangz2OperationAlphaContentsRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Reveal_public_key {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Register_global_constant_source generated for Proto011PtHangz2OperationAlphaContentsRegisterGlobalConstantSource
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Register_global_constant_source extends Box<Proto011PtHangz2OperationAlphaContentsRegisterGlobalConstantSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Register_global_constant_source {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsRegisterGlobalConstantSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Proposals_source generated for Proto011PtHangz2OperationAlphaContentsProposalsSource
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Proposals_source extends Box<Proto011PtHangz2OperationAlphaContentsProposalsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Proposals_source {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsProposalsSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq generated for Proto011PtHangz2OperationAlphaContentsProposalsProposalsDenestDynDenestSeq
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq extends Box<Proto011PtHangz2OperationAlphaContentsProposalsProposalsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsProposalsProposalsDenestDynDenestSeq>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Origination_source generated for Proto011PtHangz2OperationAlphaContentsOriginationSource
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Origination_source extends Box<Proto011PtHangz2OperationAlphaContentsOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Origination_source {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsOriginationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Origination_delegate generated for Proto011PtHangz2OperationAlphaContentsOriginationDelegate
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Origination_delegate extends Box<Proto011PtHangz2OperationAlphaContentsOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Origination_delegate {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Delegation_source generated for Proto011PtHangz2OperationAlphaContentsDelegationSource
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Delegation_source extends Box<Proto011PtHangz2OperationAlphaContentsDelegationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Delegation_source {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsDelegationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Delegation_delegate generated for Proto011PtHangz2OperationAlphaContentsDelegationDelegate
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Delegation_delegate extends Box<Proto011PtHangz2OperationAlphaContentsDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Delegation_delegate {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Ballot_source generated for Proto011PtHangz2OperationAlphaContentsBallotSource
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Ballot_source extends Box<Proto011PtHangz2OperationAlphaContentsBallotSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Ballot_source {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsBallotSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Ballot_proposal generated for Proto011PtHangz2OperationAlphaContentsBallotProposal
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Ballot_proposal extends Box<Proto011PtHangz2OperationAlphaContentsBallotProposal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Ballot_proposal {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsBallotProposal>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Activate_account_pkh generated for Proto011PtHangz2OperationAlphaContentsActivateAccountPkh
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Activate_account_pkh extends Box<Proto011PtHangz2OperationAlphaContentsActivateAccountPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents_Activate_account_pkh {
        return new this(record_decoder<Proto011PtHangz2OperationAlphaContentsActivateAccountPkh>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2OperationAlphaContents generated for Proto011PtHangz2OperationAlphaContents
export function proto011pthangz2operationalphacontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto011PtHangz2OperationAlphaContents,Proto011PtHangz2OperationAlphaContents> {
    function f(disc: CGRIDTag__Proto011PtHangz2OperationAlphaContents) {
        switch (disc) {
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Endorsement: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Endorsement.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Seed_nonce_revelation: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Seed_nonce_revelation.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Double_endorsement_evidence: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Double_endorsement_evidence.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Double_baking_evidence: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Double_baking_evidence.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Activate_account: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Activate_account.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Proposals: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Proposals.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Ballot: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Ballot.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Endorsement_with_slot: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Endorsement_with_slot.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Failing_noop: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Failing_noop.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Reveal: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Reveal.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Transaction: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Transaction.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Origination: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Origination.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Delegation: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Delegation.decode;
            case CGRIDTag__Proto011PtHangz2OperationAlphaContents.Register_global_constant: return CGRIDClass__Proto011PtHangz2OperationAlphaContents__Register_global_constant.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto011PtHangz2OperationAlphaContents => Object.values(CGRIDTag__Proto011PtHangz2OperationAlphaContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto011_PtHangz2OperationAlphaContents extends Box<Proto011PtHangz2OperationAlphaContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto011PtHangz2OperationAlphaContents>, Proto011PtHangz2OperationAlphaContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2OperationAlphaContents {
        return new this(variant_decoder(width.Uint8)(proto011pthangz2operationalphacontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2InlinedEndorsement_signature generated for Proto011PtHangz2InlinedEndorsementSignature
export class CGRIDClass__Proto011_PtHangz2InlinedEndorsement_signature extends Box<Proto011PtHangz2InlinedEndorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2InlinedEndorsement_signature {
        return new this(record_decoder<Proto011PtHangz2InlinedEndorsementSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2InlinedEndorsementContents generated for Proto011PtHangz2InlinedEndorsementContents
export function proto011pthangz2inlinedendorsementcontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto011PtHangz2InlinedEndorsementContents,Proto011PtHangz2InlinedEndorsementContents> {
    function f(disc: CGRIDTag__Proto011PtHangz2InlinedEndorsementContents) {
        switch (disc) {
            case CGRIDTag__Proto011PtHangz2InlinedEndorsementContents.Endorsement: return CGRIDClass__Proto011PtHangz2InlinedEndorsementContents__Endorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto011PtHangz2InlinedEndorsementContents => Object.values(CGRIDTag__Proto011PtHangz2InlinedEndorsementContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto011_PtHangz2InlinedEndorsementContents extends Box<Proto011PtHangz2InlinedEndorsementContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto011PtHangz2InlinedEndorsementContents>, Proto011PtHangz2InlinedEndorsementContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2InlinedEndorsementContents {
        return new this(variant_decoder(width.Uint8)(proto011pthangz2inlinedendorsementcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2InlinedEndorsement generated for Proto011PtHangz2InlinedEndorsement
export class CGRIDClass__Proto011_PtHangz2InlinedEndorsement extends Box<Proto011PtHangz2InlinedEndorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2InlinedEndorsement {
        return new this(record_decoder<Proto011PtHangz2InlinedEndorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__Proto011_PtHangz2InlinedEndorsementContents.decode, signature: Nullable.decode(CGRIDClass__Proto011_PtHangz2InlinedEndorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Entrypoint generated for Proto011PtHangz2Entrypoint
export function proto011pthangz2entrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto011PtHangz2Entrypoint,Proto011PtHangz2Entrypoint> {
    function f(disc: CGRIDTag__Proto011PtHangz2Entrypoint) {
        switch (disc) {
            case CGRIDTag__Proto011PtHangz2Entrypoint._default: return CGRIDClass__Proto011PtHangz2Entrypoint___default.decode;
            case CGRIDTag__Proto011PtHangz2Entrypoint.root: return CGRIDClass__Proto011PtHangz2Entrypoint__root.decode;
            case CGRIDTag__Proto011PtHangz2Entrypoint._do: return CGRIDClass__Proto011PtHangz2Entrypoint___do.decode;
            case CGRIDTag__Proto011PtHangz2Entrypoint.set_delegate: return CGRIDClass__Proto011PtHangz2Entrypoint__set_delegate.decode;
            case CGRIDTag__Proto011PtHangz2Entrypoint.remove_delegate: return CGRIDClass__Proto011PtHangz2Entrypoint__remove_delegate.decode;
            case CGRIDTag__Proto011PtHangz2Entrypoint.named: return CGRIDClass__Proto011PtHangz2Entrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto011PtHangz2Entrypoint => Object.values(CGRIDTag__Proto011PtHangz2Entrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto011_PtHangz2Entrypoint extends Box<Proto011PtHangz2Entrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto011PtHangz2Entrypoint>, Proto011PtHangz2Entrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Entrypoint {
        return new this(variant_decoder(width.Uint8)(proto011pthangz2entrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad generated for Proto011PtHangz2ContractIdOriginatedDenestPad
export class CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad extends Box<Proto011PtHangz2ContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Contract_id_Originated_denest_pad {
        return new this(record_decoder<Proto011PtHangz2ContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Contract_id generated for Proto011PtHangz2ContractId
export function proto011pthangz2contractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto011PtHangz2ContractId,Proto011PtHangz2ContractId> {
    function f(disc: CGRIDTag__Proto011PtHangz2ContractId) {
        switch (disc) {
            case CGRIDTag__Proto011PtHangz2ContractId.Implicit: return CGRIDClass__Proto011PtHangz2ContractId__Implicit.decode;
            case CGRIDTag__Proto011PtHangz2ContractId.Originated: return CGRIDClass__Proto011PtHangz2ContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto011PtHangz2ContractId => Object.values(CGRIDTag__Proto011PtHangz2ContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto011_PtHangz2Contract_id extends Box<Proto011PtHangz2ContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto011PtHangz2ContractId>, Proto011PtHangz2ContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Contract_id {
        return new this(variant_decoder(width.Uint8)(proto011pthangz2contractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Block_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto011PtHangz2BlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto011_PtHangz2Block_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto011PtHangz2BlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Block_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto011PtHangz2BlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Block_headerAlphaSigned_contents_signature generated for Proto011PtHangz2BlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto011_PtHangz2Block_headerAlphaSigned_contents_signature extends Box<Proto011PtHangz2BlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Block_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto011PtHangz2BlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto011_PtHangz2Block_headerAlphaFull_header generated for Proto011PtHangz2BlockHeaderAlphaFullHeader
export class CGRIDClass__Proto011_PtHangz2Block_headerAlphaFull_header extends Box<Proto011PtHangz2BlockHeaderAlphaFullHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Block_headerAlphaFull_header {
        return new this(record_decoder<Proto011PtHangz2BlockHeaderAlphaFullHeader>({level: Int32.decode, proto: Uint8.decode, predecessor: CGRIDClass__Block_headerShell_predecessor.decode, timestamp: Int64.decode, validation_pass: Uint8.decode, operations_hash: CGRIDClass__Block_headerShell_operations_hash.decode, fitness: Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30), context: CGRIDClass__Block_headerShell_context.decode, priority: Uint16.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto011_PtHangz2Block_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_escape_vote: Bool.decode, signature: CGRIDClass__Proto011_PtHangz2Block_headerAlphaSigned_contents_signature.decode}, {order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_escape_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.proto.encodeLength +  this.value.predecessor.encodeLength +  this.value.timestamp.encodeLength +  this.value.validation_pass.encodeLength +  this.value.operations_hash.encodeLength +  this.value.fitness.encodeLength +  this.value.context.encodeLength +  this.value.priority.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_escape_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.proto.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.timestamp.writeTarget(tgt) +  this.value.validation_pass.writeTarget(tgt) +  this.value.operations_hash.writeTarget(tgt) +  this.value.fitness.writeTarget(tgt) +  this.value.context.writeTarget(tgt) +  this.value.priority.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_escape_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_predecessor generated for BlockHeaderShellPredecessor
export class CGRIDClass__Block_headerShell_predecessor extends Box<BlockHeaderShellPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_predecessor {
        return new this(record_decoder<BlockHeaderShellPredecessor>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_operations_hash generated for BlockHeaderShellOperationsHash
export class CGRIDClass__Block_headerShell_operations_hash extends Box<BlockHeaderShellOperationsHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_list_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_operations_hash {
        return new this(record_decoder<BlockHeaderShellOperationsHash>({operation_list_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_list_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_list_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_list_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_context generated for BlockHeaderShellContext
export class CGRIDClass__Block_headerShell_context extends Box<BlockHeaderShellContext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_context {
        return new this(record_decoder<BlockHeaderShellContext>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export type BlockHeaderShellPredecessor = { block_hash: FixedBytes<32> };
export type BlockHeaderShellOperationsHash = { operation_list_list_hash: FixedBytes<32> };
export type BlockHeaderShellContext = { context_hash: FixedBytes<32> };
export type Proto011PtHangz2ScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export type Proto011PtHangz2OperationAlphaContentsTransactionSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationAlphaContentsTransactionParameters = { entrypoint: CGRIDClass__Proto011_PtHangz2Entrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto011PtHangz2OperationAlphaContentsRevealSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationAlphaContentsRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto011PtHangz2OperationAlphaContentsRegisterGlobalConstantSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationAlphaContentsProposalsSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationAlphaContentsProposalsProposalsDenestDynDenestSeq = { protocol_hash: FixedBytes<32> };
export type Proto011PtHangz2OperationAlphaContentsOriginationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationAlphaContentsOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationAlphaContentsDelegationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationAlphaContentsDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationAlphaContentsBallotSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto011PtHangz2OperationAlphaContentsBallotProposal = { protocol_hash: FixedBytes<32> };
export type Proto011PtHangz2OperationAlphaContentsActivateAccountPkh = { ed25519_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__Proto011PtHangz2OperationAlphaContents{
    Endorsement = 0,
    Seed_nonce_revelation = 1,
    Double_endorsement_evidence = 2,
    Double_baking_evidence = 3,
    Activate_account = 4,
    Proposals = 5,
    Ballot = 6,
    Endorsement_with_slot = 10,
    Failing_noop = 17,
    Reveal = 107,
    Transaction = 108,
    Origination = 109,
    Delegation = 110,
    Register_global_constant = 111
}
export interface CGRIDMap__Proto011PtHangz2OperationAlphaContents {
    Endorsement: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Endorsement,
    Seed_nonce_revelation: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Seed_nonce_revelation,
    Double_endorsement_evidence: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Double_endorsement_evidence,
    Double_baking_evidence: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Double_baking_evidence,
    Activate_account: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Activate_account,
    Proposals: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Proposals,
    Ballot: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Ballot,
    Endorsement_with_slot: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Endorsement_with_slot,
    Failing_noop: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Failing_noop,
    Reveal: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Reveal,
    Transaction: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Transaction,
    Origination: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Origination,
    Delegation: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Delegation,
    Register_global_constant: CGRIDClass__Proto011PtHangz2OperationAlphaContents__Register_global_constant
}
export type Proto011PtHangz2OperationAlphaContents = { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Endorsement, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Endorsement'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Seed_nonce_revelation, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Seed_nonce_revelation'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Double_endorsement_evidence, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Double_endorsement_evidence'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Double_baking_evidence, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Double_baking_evidence'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Activate_account, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Activate_account'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Proposals, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Proposals'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Ballot, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Ballot'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Endorsement_with_slot, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Endorsement_with_slot'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Failing_noop, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Failing_noop'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Reveal, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Reveal'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Transaction, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Transaction'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Origination, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Origination'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Delegation, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Delegation'] } | { kind: CGRIDTag__Proto011PtHangz2OperationAlphaContents.Register_global_constant, value: CGRIDMap__Proto011PtHangz2OperationAlphaContents['Register_global_constant'] };
export type Proto011PtHangz2InlinedEndorsementSignature = { signature_v0: FixedBytes<64> };
export enum CGRIDTag__Proto011PtHangz2InlinedEndorsementContents{
    Endorsement = 0
}
export interface CGRIDMap__Proto011PtHangz2InlinedEndorsementContents {
    Endorsement: CGRIDClass__Proto011PtHangz2InlinedEndorsementContents__Endorsement
}
export type Proto011PtHangz2InlinedEndorsementContents = { kind: CGRIDTag__Proto011PtHangz2InlinedEndorsementContents.Endorsement, value: CGRIDMap__Proto011PtHangz2InlinedEndorsementContents['Endorsement'] };
export type Proto011PtHangz2InlinedEndorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__Proto011_PtHangz2InlinedEndorsementContents, signature: Nullable<CGRIDClass__Proto011_PtHangz2InlinedEndorsement_signature> };
export enum CGRIDTag__Proto011PtHangz2Entrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto011PtHangz2Entrypoint {
    _default: CGRIDClass__Proto011PtHangz2Entrypoint___default,
    root: CGRIDClass__Proto011PtHangz2Entrypoint__root,
    _do: CGRIDClass__Proto011PtHangz2Entrypoint___do,
    set_delegate: CGRIDClass__Proto011PtHangz2Entrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto011PtHangz2Entrypoint__remove_delegate,
    named: CGRIDClass__Proto011PtHangz2Entrypoint__named
}
export type Proto011PtHangz2Entrypoint = { kind: CGRIDTag__Proto011PtHangz2Entrypoint._default, value: CGRIDMap__Proto011PtHangz2Entrypoint['_default'] } | { kind: CGRIDTag__Proto011PtHangz2Entrypoint.root, value: CGRIDMap__Proto011PtHangz2Entrypoint['root'] } | { kind: CGRIDTag__Proto011PtHangz2Entrypoint._do, value: CGRIDMap__Proto011PtHangz2Entrypoint['_do'] } | { kind: CGRIDTag__Proto011PtHangz2Entrypoint.set_delegate, value: CGRIDMap__Proto011PtHangz2Entrypoint['set_delegate'] } | { kind: CGRIDTag__Proto011PtHangz2Entrypoint.remove_delegate, value: CGRIDMap__Proto011PtHangz2Entrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto011PtHangz2Entrypoint.named, value: CGRIDMap__Proto011PtHangz2Entrypoint['named'] };
export type Proto011PtHangz2ContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto011PtHangz2ContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto011PtHangz2ContractId {
    Implicit: CGRIDClass__Proto011PtHangz2ContractId__Implicit,
    Originated: CGRIDClass__Proto011PtHangz2ContractId__Originated
}
export type Proto011PtHangz2ContractId = { kind: CGRIDTag__Proto011PtHangz2ContractId.Implicit, value: CGRIDMap__Proto011PtHangz2ContractId['Implicit'] } | { kind: CGRIDTag__Proto011PtHangz2ContractId.Originated, value: CGRIDMap__Proto011PtHangz2ContractId['Originated'] };
export type Proto011PtHangz2BlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto011PtHangz2BlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto011PtHangz2BlockHeaderAlphaFullHeader = { level: Int32, proto: Uint8, predecessor: CGRIDClass__Block_headerShell_predecessor, timestamp: Int64, validation_pass: Uint8, operations_hash: CGRIDClass__Block_headerShell_operations_hash, fitness: Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>, context: CGRIDClass__Block_headerShell_context, priority: Uint16, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto011_PtHangz2Block_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_escape_vote: Bool, signature: CGRIDClass__Proto011_PtHangz2Block_headerAlphaSigned_contents_signature };
export type Proto011PtHangz2OperationContentsList = Sequence<CGRIDClass__Proto011_PtHangz2OperationAlphaContents>;
export class CGRIDClass__Proto011PtHangz2OperationContentsList extends Box<Proto011PtHangz2OperationContentsList> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2OperationContentsList {
        return new this(Sequence.decode(CGRIDClass__Proto011_PtHangz2OperationAlphaContents.decode)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto011_pthangz2_operation_contents_list_encoder = (value: Proto011PtHangz2OperationContentsList): OutputBytes => {
    return value.encode();
}
export const proto011_pthangz2_operation_contents_list_decoder = (p: Parser): Proto011PtHangz2OperationContentsList => {
    return Sequence.decode(CGRIDClass__Proto011_PtHangz2OperationAlphaContents.decode)(p);
}
