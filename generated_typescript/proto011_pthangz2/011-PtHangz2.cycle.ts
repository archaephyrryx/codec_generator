import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto011PtHangz2Cycle = Int32;
export class CGRIDClass__Proto011PtHangz2Cycle extends Box<Proto011PtHangz2Cycle> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Cycle {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto011_pthangz2_cycle_encoder = (value: Proto011PtHangz2Cycle): OutputBytes => {
    return value.encode();
}
export const proto011_pthangz2_cycle_decoder = (p: Parser): Proto011PtHangz2Cycle => {
    return Int32.decode(p);
}
