import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto011PtHangz2VotingPeriodKind__exploration generated for Proto011PtHangz2VotingPeriodKind__exploration
export class CGRIDClass__Proto011PtHangz2VotingPeriodKind__exploration extends Box<Proto011PtHangz2VotingPeriodKind__exploration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2VotingPeriodKind__exploration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2VotingPeriodKind__Proposal generated for Proto011PtHangz2VotingPeriodKind__Proposal
export class CGRIDClass__Proto011PtHangz2VotingPeriodKind__Proposal extends Box<Proto011PtHangz2VotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2VotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2VotingPeriodKind__Promotion generated for Proto011PtHangz2VotingPeriodKind__Promotion
export class CGRIDClass__Proto011PtHangz2VotingPeriodKind__Promotion extends Box<Proto011PtHangz2VotingPeriodKind__Promotion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2VotingPeriodKind__Promotion {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2VotingPeriodKind__Cooldown generated for Proto011PtHangz2VotingPeriodKind__Cooldown
export class CGRIDClass__Proto011PtHangz2VotingPeriodKind__Cooldown extends Box<Proto011PtHangz2VotingPeriodKind__Cooldown> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2VotingPeriodKind__Cooldown {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto011PtHangz2VotingPeriodKind__Adoption generated for Proto011PtHangz2VotingPeriodKind__Adoption
export class CGRIDClass__Proto011PtHangz2VotingPeriodKind__Adoption extends Box<Proto011PtHangz2VotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2VotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto011PtHangz2VotingPeriodKind__exploration = Unit;
export type Proto011PtHangz2VotingPeriodKind__Proposal = Unit;
export type Proto011PtHangz2VotingPeriodKind__Promotion = Unit;
export type Proto011PtHangz2VotingPeriodKind__Cooldown = Unit;
export type Proto011PtHangz2VotingPeriodKind__Adoption = Unit;
// Class CGRIDClass__Proto011_PtHangz2Voting_period_kind generated for Proto011PtHangz2VotingPeriodKind
export function proto011pthangz2votingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto011PtHangz2VotingPeriodKind,Proto011PtHangz2VotingPeriodKind> {
    function f(disc: CGRIDTag__Proto011PtHangz2VotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto011PtHangz2VotingPeriodKind.Proposal: return CGRIDClass__Proto011PtHangz2VotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto011PtHangz2VotingPeriodKind.exploration: return CGRIDClass__Proto011PtHangz2VotingPeriodKind__exploration.decode;
            case CGRIDTag__Proto011PtHangz2VotingPeriodKind.Cooldown: return CGRIDClass__Proto011PtHangz2VotingPeriodKind__Cooldown.decode;
            case CGRIDTag__Proto011PtHangz2VotingPeriodKind.Promotion: return CGRIDClass__Proto011PtHangz2VotingPeriodKind__Promotion.decode;
            case CGRIDTag__Proto011PtHangz2VotingPeriodKind.Adoption: return CGRIDClass__Proto011PtHangz2VotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto011PtHangz2VotingPeriodKind => Object.values(CGRIDTag__Proto011PtHangz2VotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto011_PtHangz2Voting_period_kind extends Box<Proto011PtHangz2VotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto011PtHangz2VotingPeriodKind>, Proto011PtHangz2VotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2Voting_period_kind {
        return new this(variant_decoder(width.Uint8)(proto011pthangz2votingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__Proto011PtHangz2VotingPeriodKind{
    Proposal = 0,
    exploration = 1,
    Cooldown = 2,
    Promotion = 3,
    Adoption = 4
}
export interface CGRIDMap__Proto011PtHangz2VotingPeriodKind {
    Proposal: CGRIDClass__Proto011PtHangz2VotingPeriodKind__Proposal,
    exploration: CGRIDClass__Proto011PtHangz2VotingPeriodKind__exploration,
    Cooldown: CGRIDClass__Proto011PtHangz2VotingPeriodKind__Cooldown,
    Promotion: CGRIDClass__Proto011PtHangz2VotingPeriodKind__Promotion,
    Adoption: CGRIDClass__Proto011PtHangz2VotingPeriodKind__Adoption
}
export type Proto011PtHangz2VotingPeriodKind = { kind: CGRIDTag__Proto011PtHangz2VotingPeriodKind.Proposal, value: CGRIDMap__Proto011PtHangz2VotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto011PtHangz2VotingPeriodKind.exploration, value: CGRIDMap__Proto011PtHangz2VotingPeriodKind['exploration'] } | { kind: CGRIDTag__Proto011PtHangz2VotingPeriodKind.Cooldown, value: CGRIDMap__Proto011PtHangz2VotingPeriodKind['Cooldown'] } | { kind: CGRIDTag__Proto011PtHangz2VotingPeriodKind.Promotion, value: CGRIDMap__Proto011PtHangz2VotingPeriodKind['Promotion'] } | { kind: CGRIDTag__Proto011PtHangz2VotingPeriodKind.Adoption, value: CGRIDMap__Proto011PtHangz2VotingPeriodKind['Adoption'] };
export type Proto011PtHangz2VotingPeriod = { index: Int32, kind: CGRIDClass__Proto011_PtHangz2Voting_period_kind, start_position: Int32 };
export class CGRIDClass__Proto011PtHangz2VotingPeriod extends Box<Proto011PtHangz2VotingPeriod> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['index', 'kind', 'start_position']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2VotingPeriod {
        return new this(record_decoder<Proto011PtHangz2VotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto011_PtHangz2Voting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p));
    };
    get encodeLength(): number {
        return (this.value.index.encodeLength +  this.value.kind.encodeLength +  this.value.start_position.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.index.writeTarget(tgt) +  this.value.kind.writeTarget(tgt) +  this.value.start_position.writeTarget(tgt));
    }
}
export const proto011_pthangz2_voting_period_encoder = (value: Proto011PtHangz2VotingPeriod): OutputBytes => {
    return record_encoder({order: ['index', 'kind', 'start_position']})(value);
}
export const proto011_pthangz2_voting_period_decoder = (p: Parser): Proto011PtHangz2VotingPeriod => {
    return record_decoder<Proto011PtHangz2VotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto011_PtHangz2Voting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p);
}
