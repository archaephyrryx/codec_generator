import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__Proto011_PtHangz2DelegateFrozen_balance_by_cycles_denest_dyn_denest_seq generated for Proto011PtHangz2DelegateFrozenBalanceByCyclesDenestDynDenestSeq
export class CGRIDClass__Proto011_PtHangz2DelegateFrozen_balance_by_cycles_denest_dyn_denest_seq extends Box<Proto011PtHangz2DelegateFrozenBalanceByCyclesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle', 'deposits', 'fees', 'rewards']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto011_PtHangz2DelegateFrozen_balance_by_cycles_denest_dyn_denest_seq {
        return new this(record_decoder<Proto011PtHangz2DelegateFrozenBalanceByCyclesDenestDynDenestSeq>({cycle: Int32.decode, deposits: N.decode, fees: N.decode, rewards: N.decode}, {order: ['cycle', 'deposits', 'fees', 'rewards']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle.encodeLength +  this.value.deposits.encodeLength +  this.value.fees.encodeLength +  this.value.rewards.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle.writeTarget(tgt) +  this.value.deposits.writeTarget(tgt) +  this.value.fees.writeTarget(tgt) +  this.value.rewards.writeTarget(tgt));
    }
}
export type Proto011PtHangz2DelegateFrozenBalanceByCyclesDenestDynDenestSeq = { cycle: Int32, deposits: N, fees: N, rewards: N };
export type Proto011PtHangz2DelegateFrozenBalanceByCycles = Dynamic<Sequence<CGRIDClass__Proto011_PtHangz2DelegateFrozen_balance_by_cycles_denest_dyn_denest_seq>,width.Uint30>;
export class CGRIDClass__Proto011PtHangz2DelegateFrozenBalanceByCycles extends Box<Proto011PtHangz2DelegateFrozenBalanceByCycles> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2DelegateFrozenBalanceByCycles {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto011_PtHangz2DelegateFrozen_balance_by_cycles_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto011_pthangz2_delegate_frozen_balance_by_cycles_encoder = (value: Proto011PtHangz2DelegateFrozenBalanceByCycles): OutputBytes => {
    return value.encode();
}
export const proto011_pthangz2_delegate_frozen_balance_by_cycles_decoder = (p: Parser): Proto011PtHangz2DelegateFrozenBalanceByCycles => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto011_PtHangz2DelegateFrozen_balance_by_cycles_denest_dyn_denest_seq.decode), width.Uint30)(p);
}
