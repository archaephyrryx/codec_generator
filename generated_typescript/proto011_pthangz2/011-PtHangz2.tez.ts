import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto011PtHangz2Tez = N;
export class CGRIDClass__Proto011PtHangz2Tez extends Box<Proto011PtHangz2Tez> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Tez {
        return new this(N.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto011_pthangz2_tez_encoder = (value: Proto011PtHangz2Tez): OutputBytes => {
    return value.encode();
}
export const proto011_pthangz2_tez_decoder = (p: Parser): Proto011PtHangz2Tez => {
    return N.decode(p);
}
