import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { Target } from '../../ts_runtime/target';
export type Proto011PtHangz2Fitness = Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>;
export class CGRIDClass__Proto011PtHangz2Fitness extends Box<Proto011PtHangz2Fitness> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2Fitness {
        return new this(Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto011_pthangz2_fitness_encoder = (value: Proto011PtHangz2Fitness): OutputBytes => {
    return value.encode();
}
export const proto011_pthangz2_fitness_decoder = (p: Parser): Proto011PtHangz2Fitness => {
    return Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30)(p);
}
