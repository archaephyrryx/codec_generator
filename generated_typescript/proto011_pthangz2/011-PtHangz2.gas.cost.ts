import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
export type Proto011PtHangz2GasCost = Z;
export class CGRIDClass__Proto011PtHangz2GasCost extends Box<Proto011PtHangz2GasCost> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto011PtHangz2GasCost {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto011_pthangz2_gas_cost_encoder = (value: Proto011PtHangz2GasCost): OutputBytes => {
    return value.encode();
}
export const proto011_pthangz2_gas_cost_decoder = (p: Parser): Proto011PtHangz2GasCost => {
    return Z.decode(p);
}
