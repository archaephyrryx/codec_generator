import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
export type Proto009PsFLorenErrors = Dynamic<U8String,width.Uint30>;
export class CGRIDClass__Proto009PsFLorenErrors extends Box<Proto009PsFLorenErrors> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenErrors {
        return new this(Dynamic.decode(U8String.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto009_psfloren_errors_encoder = (value: Proto009PsFLorenErrors): OutputBytes => {
    return value.encode();
}
export const proto009_psfloren_errors_decoder = (p: Parser): Proto009PsFLorenErrors => {
    return Dynamic.decode(U8String.decode, width.Uint30)(p);
}
