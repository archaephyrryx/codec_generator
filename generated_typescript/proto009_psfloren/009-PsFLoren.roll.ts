import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto009PsFLorenRoll = Int32;
export class CGRIDClass__Proto009PsFLorenRoll extends Box<Proto009PsFLorenRoll> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenRoll {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto009_psfloren_roll_encoder = (value: Proto009PsFLorenRoll): OutputBytes => {
    return value.encode();
}
export const proto009_psfloren_roll_decoder = (p: Parser): Proto009PsFLorenRoll => {
    return Int32.decode(p);
}
