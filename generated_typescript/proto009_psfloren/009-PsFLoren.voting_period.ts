import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto009PsFLorenVotingPeriodKind__exploration generated for Proto009PsFLorenVotingPeriodKind__exploration
export class CGRIDClass__Proto009PsFLorenVotingPeriodKind__exploration extends Box<Proto009PsFLorenVotingPeriodKind__exploration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenVotingPeriodKind__exploration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenVotingPeriodKind__Proposal generated for Proto009PsFLorenVotingPeriodKind__Proposal
export class CGRIDClass__Proto009PsFLorenVotingPeriodKind__Proposal extends Box<Proto009PsFLorenVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenVotingPeriodKind__Promotion generated for Proto009PsFLorenVotingPeriodKind__Promotion
export class CGRIDClass__Proto009PsFLorenVotingPeriodKind__Promotion extends Box<Proto009PsFLorenVotingPeriodKind__Promotion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenVotingPeriodKind__Promotion {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenVotingPeriodKind__Cooldown generated for Proto009PsFLorenVotingPeriodKind__Cooldown
export class CGRIDClass__Proto009PsFLorenVotingPeriodKind__Cooldown extends Box<Proto009PsFLorenVotingPeriodKind__Cooldown> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenVotingPeriodKind__Cooldown {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenVotingPeriodKind__Adoption generated for Proto009PsFLorenVotingPeriodKind__Adoption
export class CGRIDClass__Proto009PsFLorenVotingPeriodKind__Adoption extends Box<Proto009PsFLorenVotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenVotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto009PsFLorenVotingPeriodKind__exploration = Unit;
export type Proto009PsFLorenVotingPeriodKind__Proposal = Unit;
export type Proto009PsFLorenVotingPeriodKind__Promotion = Unit;
export type Proto009PsFLorenVotingPeriodKind__Cooldown = Unit;
export type Proto009PsFLorenVotingPeriodKind__Adoption = Unit;
// Class CGRIDClass__Proto009_PsFLorenVoting_period_kind generated for Proto009PsFLorenVotingPeriodKind
export function proto009psflorenvotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto009PsFLorenVotingPeriodKind,Proto009PsFLorenVotingPeriodKind> {
    function f(disc: CGRIDTag__Proto009PsFLorenVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto009PsFLorenVotingPeriodKind.Proposal: return CGRIDClass__Proto009PsFLorenVotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto009PsFLorenVotingPeriodKind.exploration: return CGRIDClass__Proto009PsFLorenVotingPeriodKind__exploration.decode;
            case CGRIDTag__Proto009PsFLorenVotingPeriodKind.Cooldown: return CGRIDClass__Proto009PsFLorenVotingPeriodKind__Cooldown.decode;
            case CGRIDTag__Proto009PsFLorenVotingPeriodKind.Promotion: return CGRIDClass__Proto009PsFLorenVotingPeriodKind__Promotion.decode;
            case CGRIDTag__Proto009PsFLorenVotingPeriodKind.Adoption: return CGRIDClass__Proto009PsFLorenVotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto009PsFLorenVotingPeriodKind => Object.values(CGRIDTag__Proto009PsFLorenVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto009_PsFLorenVoting_period_kind extends Box<Proto009PsFLorenVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto009PsFLorenVotingPeriodKind>, Proto009PsFLorenVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenVoting_period_kind {
        return new this(variant_decoder(width.Uint8)(proto009psflorenvotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__Proto009PsFLorenVotingPeriodKind{
    Proposal = 0,
    exploration = 1,
    Cooldown = 2,
    Promotion = 3,
    Adoption = 4
}
export interface CGRIDMap__Proto009PsFLorenVotingPeriodKind {
    Proposal: CGRIDClass__Proto009PsFLorenVotingPeriodKind__Proposal,
    exploration: CGRIDClass__Proto009PsFLorenVotingPeriodKind__exploration,
    Cooldown: CGRIDClass__Proto009PsFLorenVotingPeriodKind__Cooldown,
    Promotion: CGRIDClass__Proto009PsFLorenVotingPeriodKind__Promotion,
    Adoption: CGRIDClass__Proto009PsFLorenVotingPeriodKind__Adoption
}
export type Proto009PsFLorenVotingPeriodKind = { kind: CGRIDTag__Proto009PsFLorenVotingPeriodKind.Proposal, value: CGRIDMap__Proto009PsFLorenVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto009PsFLorenVotingPeriodKind.exploration, value: CGRIDMap__Proto009PsFLorenVotingPeriodKind['exploration'] } | { kind: CGRIDTag__Proto009PsFLorenVotingPeriodKind.Cooldown, value: CGRIDMap__Proto009PsFLorenVotingPeriodKind['Cooldown'] } | { kind: CGRIDTag__Proto009PsFLorenVotingPeriodKind.Promotion, value: CGRIDMap__Proto009PsFLorenVotingPeriodKind['Promotion'] } | { kind: CGRIDTag__Proto009PsFLorenVotingPeriodKind.Adoption, value: CGRIDMap__Proto009PsFLorenVotingPeriodKind['Adoption'] };
export type Proto009PsFLorenVotingPeriod = { index: Int32, kind: CGRIDClass__Proto009_PsFLorenVoting_period_kind, start_position: Int32 };
export class CGRIDClass__Proto009PsFLorenVotingPeriod extends Box<Proto009PsFLorenVotingPeriod> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['index', 'kind', 'start_position']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenVotingPeriod {
        return new this(record_decoder<Proto009PsFLorenVotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto009_PsFLorenVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p));
    };
    get encodeLength(): number {
        return (this.value.index.encodeLength +  this.value.kind.encodeLength +  this.value.start_position.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.index.writeTarget(tgt) +  this.value.kind.writeTarget(tgt) +  this.value.start_position.writeTarget(tgt));
    }
}
export const proto009_psfloren_voting_period_encoder = (value: Proto009PsFLorenVotingPeriod): OutputBytes => {
    return record_encoder({order: ['index', 'kind', 'start_position']})(value);
}
export const proto009_psfloren_voting_period_decoder = (p: Parser): Proto009PsFLorenVotingPeriod => {
    return record_decoder<Proto009PsFLorenVotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto009_PsFLorenVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p);
}
