import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto009PsFLorenCycle = Int32;
export class CGRIDClass__Proto009PsFLorenCycle extends Box<Proto009PsFLorenCycle> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenCycle {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto009_psfloren_cycle_encoder = (value: Proto009PsFLorenCycle): OutputBytes => {
    return value.encode();
}
export const proto009_psfloren_cycle_decoder = (p: Parser): Proto009PsFLorenCycle => {
    return Int32.decode(p);
}
