import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Transaction generated for Proto009PsFLorenOperationAlphaInternalOperationRhs__Transaction
export class CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Transaction extends Box<Proto009PsFLorenOperationAlphaInternalOperationRhs__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Transaction {
        return new this(record_decoder<Proto009PsFLorenOperationAlphaInternalOperationRhs__Transaction>({amount: N.decode, destination: CGRIDClass__Proto009_PsFLorenContract_id.decode, parameters: Option.decode(CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Transaction_parameters.decode)}, {order: ['amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Reveal generated for Proto009PsFLorenOperationAlphaInternalOperationRhs__Reveal
export class CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Reveal extends Box<Proto009PsFLorenOperationAlphaInternalOperationRhs__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Reveal {
        return new this(record_decoder<Proto009PsFLorenOperationAlphaInternalOperationRhs__Reveal>({public_key: CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Reveal_public_key.decode}, {order: ['public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Origination generated for Proto009PsFLorenOperationAlphaInternalOperationRhs__Origination
export class CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Origination extends Box<Proto009PsFLorenOperationAlphaInternalOperationRhs__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Origination {
        return new this(record_decoder<Proto009PsFLorenOperationAlphaInternalOperationRhs__Origination>({balance: N.decode, delegate: Option.decode(CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Origination_delegate.decode), script: CGRIDClass__Proto009_PsFLorenScriptedContracts.decode}, {order: ['balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Delegation generated for Proto009PsFLorenOperationAlphaInternalOperationRhs__Delegation
export class CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Delegation extends Box<Proto009PsFLorenOperationAlphaInternalOperationRhs__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Delegation {
        return new this(record_decoder<Proto009PsFLorenOperationAlphaInternalOperationRhs__Delegation>({delegate: Option.decode(CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Delegation_delegate.decode)}, {order: ['delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009PsFLorenEntrypoint__set_delegate generated for Proto009PsFLorenEntrypoint__set_delegate
export class CGRIDClass__Proto009PsFLorenEntrypoint__set_delegate extends Box<Proto009PsFLorenEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenEntrypoint__root generated for Proto009PsFLorenEntrypoint__root
export class CGRIDClass__Proto009PsFLorenEntrypoint__root extends Box<Proto009PsFLorenEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenEntrypoint__remove_delegate generated for Proto009PsFLorenEntrypoint__remove_delegate
export class CGRIDClass__Proto009PsFLorenEntrypoint__remove_delegate extends Box<Proto009PsFLorenEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenEntrypoint__named generated for Proto009PsFLorenEntrypoint__named
export class CGRIDClass__Proto009PsFLorenEntrypoint__named extends Box<Proto009PsFLorenEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenEntrypoint___do generated for Proto009PsFLorenEntrypoint___do
export class CGRIDClass__Proto009PsFLorenEntrypoint___do extends Box<Proto009PsFLorenEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenEntrypoint___default generated for Proto009PsFLorenEntrypoint___default
export class CGRIDClass__Proto009PsFLorenEntrypoint___default extends Box<Proto009PsFLorenEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenContractId__Originated generated for Proto009PsFLorenContractId__Originated
export class CGRIDClass__Proto009PsFLorenContractId__Originated extends Box<Proto009PsFLorenContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto009_PsFLorenContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenContractId__Implicit generated for Proto009PsFLorenContractId__Implicit
export class CGRIDClass__Proto009PsFLorenContractId__Implicit extends Box<Proto009PsFLorenContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenContractId__Implicit {
        return new this(record_decoder<Proto009PsFLorenContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto009PsFLorenOperationAlphaInternalOperationRhs__Transaction = { amount: N, destination: CGRIDClass__Proto009_PsFLorenContract_id, parameters: Option<CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Transaction_parameters> };
export type Proto009PsFLorenOperationAlphaInternalOperationRhs__Reveal = { public_key: CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Reveal_public_key };
export type Proto009PsFLorenOperationAlphaInternalOperationRhs__Origination = { balance: N, delegate: Option<CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Origination_delegate>, script: CGRIDClass__Proto009_PsFLorenScriptedContracts };
export type Proto009PsFLorenOperationAlphaInternalOperationRhs__Delegation = { delegate: Option<CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Delegation_delegate> };
export type Proto009PsFLorenEntrypoint__set_delegate = Unit;
export type Proto009PsFLorenEntrypoint__root = Unit;
export type Proto009PsFLorenEntrypoint__remove_delegate = Unit;
export type Proto009PsFLorenEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto009PsFLorenEntrypoint___do = Unit;
export type Proto009PsFLorenEntrypoint___default = Unit;
export type Proto009PsFLorenContractId__Originated = Padded<CGRIDClass__Proto009_PsFLorenContract_id_Originated_denest_pad,1>;
export type Proto009PsFLorenContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenScriptedContracts generated for Proto009PsFLorenScriptedContracts
export class CGRIDClass__Proto009_PsFLorenScriptedContracts extends Box<Proto009PsFLorenScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenScriptedContracts {
        return new this(record_decoder<Proto009PsFLorenScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_rhs generated for Proto009PsFLorenOperationAlphaInternalOperationRhs
export function proto009psflorenoperationalphainternaloperationrhs_mkDecoder(): VariantDecoder<CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs,Proto009PsFLorenOperationAlphaInternalOperationRhs> {
    function f(disc: CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs) {
        switch (disc) {
            case CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs.Reveal: return CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Reveal.decode;
            case CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs.Transaction: return CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Transaction.decode;
            case CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs.Origination: return CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Origination.decode;
            case CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs.Delegation: return CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Delegation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs => Object.values(CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs).includes(tagval);
    return f;
}
export class CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_rhs extends Box<Proto009PsFLorenOperationAlphaInternalOperationRhs> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto009PsFLorenOperationAlphaInternalOperationRhs>, Proto009PsFLorenOperationAlphaInternalOperationRhs>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_rhs {
        return new this(variant_decoder(width.Uint8)(proto009psflorenoperationalphainternaloperationrhs_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Transaction_parameters generated for Proto009PsFLorenOperationAlphaInternalOperationTransactionParameters
export class CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Transaction_parameters extends Box<Proto009PsFLorenOperationAlphaInternalOperationTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Transaction_parameters {
        return new this(record_decoder<Proto009PsFLorenOperationAlphaInternalOperationTransactionParameters>({entrypoint: CGRIDClass__Proto009_PsFLorenEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Reveal_public_key generated for Proto009PsFLorenOperationAlphaInternalOperationRevealPublicKey
export class CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Reveal_public_key extends Box<Proto009PsFLorenOperationAlphaInternalOperationRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Reveal_public_key {
        return new this(record_decoder<Proto009PsFLorenOperationAlphaInternalOperationRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Origination_delegate generated for Proto009PsFLorenOperationAlphaInternalOperationOriginationDelegate
export class CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Origination_delegate extends Box<Proto009PsFLorenOperationAlphaInternalOperationOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Origination_delegate {
        return new this(record_decoder<Proto009PsFLorenOperationAlphaInternalOperationOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Delegation_delegate generated for Proto009PsFLorenOperationAlphaInternalOperationDelegationDelegate
export class CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Delegation_delegate extends Box<Proto009PsFLorenOperationAlphaInternalOperationDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_Delegation_delegate {
        return new this(record_decoder<Proto009PsFLorenOperationAlphaInternalOperationDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenEntrypoint generated for Proto009PsFLorenEntrypoint
export function proto009psflorenentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto009PsFLorenEntrypoint,Proto009PsFLorenEntrypoint> {
    function f(disc: CGRIDTag__Proto009PsFLorenEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto009PsFLorenEntrypoint._default: return CGRIDClass__Proto009PsFLorenEntrypoint___default.decode;
            case CGRIDTag__Proto009PsFLorenEntrypoint.root: return CGRIDClass__Proto009PsFLorenEntrypoint__root.decode;
            case CGRIDTag__Proto009PsFLorenEntrypoint._do: return CGRIDClass__Proto009PsFLorenEntrypoint___do.decode;
            case CGRIDTag__Proto009PsFLorenEntrypoint.set_delegate: return CGRIDClass__Proto009PsFLorenEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto009PsFLorenEntrypoint.remove_delegate: return CGRIDClass__Proto009PsFLorenEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto009PsFLorenEntrypoint.named: return CGRIDClass__Proto009PsFLorenEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto009PsFLorenEntrypoint => Object.values(CGRIDTag__Proto009PsFLorenEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto009_PsFLorenEntrypoint extends Box<Proto009PsFLorenEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto009PsFLorenEntrypoint>, Proto009PsFLorenEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto009psflorenentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenContract_id_Originated_denest_pad generated for Proto009PsFLorenContractIdOriginatedDenestPad
export class CGRIDClass__Proto009_PsFLorenContract_id_Originated_denest_pad extends Box<Proto009PsFLorenContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto009PsFLorenContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenContract_id generated for Proto009PsFLorenContractId
export function proto009psflorencontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto009PsFLorenContractId,Proto009PsFLorenContractId> {
    function f(disc: CGRIDTag__Proto009PsFLorenContractId) {
        switch (disc) {
            case CGRIDTag__Proto009PsFLorenContractId.Implicit: return CGRIDClass__Proto009PsFLorenContractId__Implicit.decode;
            case CGRIDTag__Proto009PsFLorenContractId.Originated: return CGRIDClass__Proto009PsFLorenContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto009PsFLorenContractId => Object.values(CGRIDTag__Proto009PsFLorenContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto009_PsFLorenContract_id extends Box<Proto009PsFLorenContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto009PsFLorenContractId>, Proto009PsFLorenContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenContract_id {
        return new this(variant_decoder(width.Uint8)(proto009psflorencontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type Proto009PsFLorenScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export enum CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs{
    Reveal = 0,
    Transaction = 1,
    Origination = 2,
    Delegation = 3
}
export interface CGRIDMap__Proto009PsFLorenOperationAlphaInternalOperationRhs {
    Reveal: CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Reveal,
    Transaction: CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Transaction,
    Origination: CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Origination,
    Delegation: CGRIDClass__Proto009PsFLorenOperationAlphaInternalOperationRhs__Delegation
}
export type Proto009PsFLorenOperationAlphaInternalOperationRhs = { kind: CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs.Reveal, value: CGRIDMap__Proto009PsFLorenOperationAlphaInternalOperationRhs['Reveal'] } | { kind: CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs.Transaction, value: CGRIDMap__Proto009PsFLorenOperationAlphaInternalOperationRhs['Transaction'] } | { kind: CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs.Origination, value: CGRIDMap__Proto009PsFLorenOperationAlphaInternalOperationRhs['Origination'] } | { kind: CGRIDTag__Proto009PsFLorenOperationAlphaInternalOperationRhs.Delegation, value: CGRIDMap__Proto009PsFLorenOperationAlphaInternalOperationRhs['Delegation'] };
export type Proto009PsFLorenOperationAlphaInternalOperationTransactionParameters = { entrypoint: CGRIDClass__Proto009_PsFLorenEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto009PsFLorenOperationAlphaInternalOperationRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto009PsFLorenOperationAlphaInternalOperationOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto009PsFLorenOperationAlphaInternalOperationDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto009PsFLorenEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto009PsFLorenEntrypoint {
    _default: CGRIDClass__Proto009PsFLorenEntrypoint___default,
    root: CGRIDClass__Proto009PsFLorenEntrypoint__root,
    _do: CGRIDClass__Proto009PsFLorenEntrypoint___do,
    set_delegate: CGRIDClass__Proto009PsFLorenEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto009PsFLorenEntrypoint__remove_delegate,
    named: CGRIDClass__Proto009PsFLorenEntrypoint__named
}
export type Proto009PsFLorenEntrypoint = { kind: CGRIDTag__Proto009PsFLorenEntrypoint._default, value: CGRIDMap__Proto009PsFLorenEntrypoint['_default'] } | { kind: CGRIDTag__Proto009PsFLorenEntrypoint.root, value: CGRIDMap__Proto009PsFLorenEntrypoint['root'] } | { kind: CGRIDTag__Proto009PsFLorenEntrypoint._do, value: CGRIDMap__Proto009PsFLorenEntrypoint['_do'] } | { kind: CGRIDTag__Proto009PsFLorenEntrypoint.set_delegate, value: CGRIDMap__Proto009PsFLorenEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto009PsFLorenEntrypoint.remove_delegate, value: CGRIDMap__Proto009PsFLorenEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto009PsFLorenEntrypoint.named, value: CGRIDMap__Proto009PsFLorenEntrypoint['named'] };
export type Proto009PsFLorenContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto009PsFLorenContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto009PsFLorenContractId {
    Implicit: CGRIDClass__Proto009PsFLorenContractId__Implicit,
    Originated: CGRIDClass__Proto009PsFLorenContractId__Originated
}
export type Proto009PsFLorenContractId = { kind: CGRIDTag__Proto009PsFLorenContractId.Implicit, value: CGRIDMap__Proto009PsFLorenContractId['Implicit'] } | { kind: CGRIDTag__Proto009PsFLorenContractId.Originated, value: CGRIDMap__Proto009PsFLorenContractId['Originated'] };
export type Proto009PsFLorenOperationInternal = { source: CGRIDClass__Proto009_PsFLorenContract_id, nonce: Uint16, proto009_psfloren_operation_alpha_internal_operation_rhs: CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_rhs };
export class CGRIDClass__Proto009PsFLorenOperationInternal extends Box<Proto009PsFLorenOperationInternal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'nonce', 'proto009_psfloren_operation_alpha_internal_operation_rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenOperationInternal {
        return new this(record_decoder<Proto009PsFLorenOperationInternal>({source: CGRIDClass__Proto009_PsFLorenContract_id.decode, nonce: Uint16.decode, proto009_psfloren_operation_alpha_internal_operation_rhs: CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto009_psfloren_operation_alpha_internal_operation_rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.nonce.encodeLength +  this.value.proto009_psfloren_operation_alpha_internal_operation_rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt) +  this.value.proto009_psfloren_operation_alpha_internal_operation_rhs.writeTarget(tgt));
    }
}
export const proto009_psfloren_operation_internal_encoder = (value: Proto009PsFLorenOperationInternal): OutputBytes => {
    return record_encoder({order: ['source', 'nonce', 'proto009_psfloren_operation_alpha_internal_operation_rhs']})(value);
}
export const proto009_psfloren_operation_internal_decoder = (p: Parser): Proto009PsFLorenOperationInternal => {
    return record_decoder<Proto009PsFLorenOperationInternal>({source: CGRIDClass__Proto009_PsFLorenContract_id.decode, nonce: Uint16.decode, proto009_psfloren_operation_alpha_internal_operation_rhs: CGRIDClass__Proto009_PsFLorenOperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto009_psfloren_operation_alpha_internal_operation_rhs']})(p);
}
