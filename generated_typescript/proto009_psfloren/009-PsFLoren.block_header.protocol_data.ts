import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto009_PsFLorenBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto009_PsFLorenBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenBlock_headerAlphaSigned_contents_signature generated for Proto009PsFLorenBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto009_PsFLorenBlock_headerAlphaSigned_contents_signature extends Box<Proto009PsFLorenBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto009PsFLorenBlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
export type Proto009PsFLorenBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto009PsFLorenBlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto009PsFLorenBlockHeaderProtocolData = { priority: Uint16, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto009_PsFLorenBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, signature: CGRIDClass__Proto009_PsFLorenBlock_headerAlphaSigned_contents_signature };
export class CGRIDClass__Proto009PsFLorenBlockHeaderProtocolData extends Box<Proto009PsFLorenBlockHeaderProtocolData> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenBlockHeaderProtocolData {
        return new this(record_decoder<Proto009PsFLorenBlockHeaderProtocolData>({priority: Uint16.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto009_PsFLorenBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), signature: CGRIDClass__Proto009_PsFLorenBlock_headerAlphaSigned_contents_signature.decode}, {order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.priority.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.priority.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export const proto009_psfloren_block_header_protocol_data_encoder = (value: Proto009PsFLorenBlockHeaderProtocolData): OutputBytes => {
    return record_encoder({order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(value);
}
export const proto009_psfloren_block_header_protocol_data_decoder = (p: Parser): Proto009PsFLorenBlockHeaderProtocolData => {
    return record_decoder<Proto009PsFLorenBlockHeaderProtocolData>({priority: Uint16.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto009_PsFLorenBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), signature: CGRIDClass__Proto009_PsFLorenBlock_headerAlphaSigned_contents_signature.decode}, {order: ['priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(p);
}
