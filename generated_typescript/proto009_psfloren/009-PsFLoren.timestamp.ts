import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto009PsFLorenTimestamp = Int64;
export class CGRIDClass__Proto009PsFLorenTimestamp extends Box<Proto009PsFLorenTimestamp> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenTimestamp {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto009_psfloren_timestamp_encoder = (value: Proto009PsFLorenTimestamp): OutputBytes => {
    return value.encode();
}
export const proto009_psfloren_timestamp_decoder = (p: Parser): Proto009PsFLorenTimestamp => {
    return Int64.decode(p);
}
