import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration generated for Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration
export class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration extends Box<Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Block_application generated for Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Block_application
export class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Block_application extends Box<Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Block_application> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Block_application {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Rewards generated for Proto009PsFLorenOperationMetadataAlphaBalance__Rewards
export class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Rewards extends Box<Proto009PsFLorenOperationMetadataAlphaBalance__Rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Rewards {
        return new this(record_decoder<Proto009PsFLorenOperationMetadataAlphaBalance__Rewards>({category: Unit.decode, delegate: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Rewards_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Fees generated for Proto009PsFLorenOperationMetadataAlphaBalance__Fees
export class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Fees extends Box<Proto009PsFLorenOperationMetadataAlphaBalance__Fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Fees {
        return new this(record_decoder<Proto009PsFLorenOperationMetadataAlphaBalance__Fees>({category: Unit.decode, delegate: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Fees_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Deposits generated for Proto009PsFLorenOperationMetadataAlphaBalance__Deposits
export class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Deposits extends Box<Proto009PsFLorenOperationMetadataAlphaBalance__Deposits> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Deposits {
        return new this(record_decoder<Proto009PsFLorenOperationMetadataAlphaBalance__Deposits>({category: Unit.decode, delegate: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Deposits_delegate.decode, cycle: Int32.decode, change: Int64.decode, origin: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'cycle', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Contract generated for Proto009PsFLorenOperationMetadataAlphaBalance__Contract
export class CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Contract extends Box<Proto009PsFLorenOperationMetadataAlphaBalance__Contract> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Contract {
        return new this(record_decoder<Proto009PsFLorenOperationMetadataAlphaBalance__Contract>({contract: CGRIDClass__Proto009_PsFLorenContract_id.decode, change: Int64.decode, origin: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['contract', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009PsFLorenContractId__Originated generated for Proto009PsFLorenContractId__Originated
export class CGRIDClass__Proto009PsFLorenContractId__Originated extends Box<Proto009PsFLorenContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto009_PsFLorenContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenContractId__Implicit generated for Proto009PsFLorenContractId__Implicit
export class CGRIDClass__Proto009PsFLorenContractId__Implicit extends Box<Proto009PsFLorenContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenContractId__Implicit {
        return new this(record_decoder<Proto009PsFLorenContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration = Unit;
export type Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Block_application = Unit;
export type Proto009PsFLorenOperationMetadataAlphaBalance__Rewards = { category: Unit, delegate: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Rewards_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaUpdate_origin_origin };
export type Proto009PsFLorenOperationMetadataAlphaBalance__Fees = { category: Unit, delegate: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Fees_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaUpdate_origin_origin };
export type Proto009PsFLorenOperationMetadataAlphaBalance__Deposits = { category: Unit, delegate: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Deposits_delegate, cycle: Int32, change: Int64, origin: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaUpdate_origin_origin };
export type Proto009PsFLorenOperationMetadataAlphaBalance__Contract = { contract: CGRIDClass__Proto009_PsFLorenContract_id, change: Int64, origin: CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaUpdate_origin_origin };
export type Proto009PsFLorenContractId__Originated = Padded<CGRIDClass__Proto009_PsFLorenContract_id_Originated_denest_pad,1>;
export type Proto009PsFLorenContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaUpdate_origin_origin generated for Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin
export function proto009psflorenoperationmetadataalphaupdateoriginorigin_mkDecoder(): VariantDecoder<CGRIDTag__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin,Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin> {
    function f(disc: CGRIDTag__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin) {
        switch (disc) {
            case CGRIDTag__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin.Block_application: return CGRIDClass__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Block_application.decode;
            case CGRIDTag__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration: return CGRIDClass__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin => Object.values(CGRIDTag__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin).includes(tagval);
    return f;
}
export class CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaUpdate_origin_origin extends Box<Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin>, Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaUpdate_origin_origin {
        return new this(variant_decoder(width.Uint8)(proto009psflorenoperationmetadataalphaupdateoriginorigin_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Rewards_delegate generated for Proto009PsFLorenOperationMetadataAlphaBalanceRewardsDelegate
export class CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Rewards_delegate extends Box<Proto009PsFLorenOperationMetadataAlphaBalanceRewardsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Rewards_delegate {
        return new this(record_decoder<Proto009PsFLorenOperationMetadataAlphaBalanceRewardsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Fees_delegate generated for Proto009PsFLorenOperationMetadataAlphaBalanceFeesDelegate
export class CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Fees_delegate extends Box<Proto009PsFLorenOperationMetadataAlphaBalanceFeesDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Fees_delegate {
        return new this(record_decoder<Proto009PsFLorenOperationMetadataAlphaBalanceFeesDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Deposits_delegate generated for Proto009PsFLorenOperationMetadataAlphaBalanceDepositsDelegate
export class CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Deposits_delegate extends Box<Proto009PsFLorenOperationMetadataAlphaBalanceDepositsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance_Deposits_delegate {
        return new this(record_decoder<Proto009PsFLorenOperationMetadataAlphaBalanceDepositsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance generated for Proto009PsFLorenOperationMetadataAlphaBalance
export function proto009psflorenoperationmetadataalphabalance_mkDecoder(): VariantDecoder<CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance,Proto009PsFLorenOperationMetadataAlphaBalance> {
    function f(disc: CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance) {
        switch (disc) {
            case CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance.Contract: return CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Contract.decode;
            case CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance.Rewards: return CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Rewards.decode;
            case CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance.Fees: return CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Fees.decode;
            case CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance.Deposits: return CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Deposits.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance => Object.values(CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance).includes(tagval);
    return f;
}
export class CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance extends Box<Proto009PsFLorenOperationMetadataAlphaBalance> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto009PsFLorenOperationMetadataAlphaBalance>, Proto009PsFLorenOperationMetadataAlphaBalance>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance {
        return new this(variant_decoder(width.Uint8)(proto009psflorenoperationmetadataalphabalance_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenContract_id_Originated_denest_pad generated for Proto009PsFLorenContractIdOriginatedDenestPad
export class CGRIDClass__Proto009_PsFLorenContract_id_Originated_denest_pad extends Box<Proto009PsFLorenContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto009PsFLorenContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto009_PsFLorenContract_id generated for Proto009PsFLorenContractId
export function proto009psflorencontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto009PsFLorenContractId,Proto009PsFLorenContractId> {
    function f(disc: CGRIDTag__Proto009PsFLorenContractId) {
        switch (disc) {
            case CGRIDTag__Proto009PsFLorenContractId.Implicit: return CGRIDClass__Proto009PsFLorenContractId__Implicit.decode;
            case CGRIDTag__Proto009PsFLorenContractId.Originated: return CGRIDClass__Proto009PsFLorenContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto009PsFLorenContractId => Object.values(CGRIDTag__Proto009PsFLorenContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto009_PsFLorenContract_id extends Box<Proto009PsFLorenContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto009PsFLorenContractId>, Proto009PsFLorenContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009_PsFLorenContract_id {
        return new this(variant_decoder(width.Uint8)(proto009psflorencontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin{
    Block_application = 0,
    Protocol_migration = 1
}
export interface CGRIDMap__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin {
    Block_application: CGRIDClass__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Block_application,
    Protocol_migration: CGRIDClass__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration
}
export type Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin = { kind: CGRIDTag__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin.Block_application, value: CGRIDMap__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin['Block_application'] } | { kind: CGRIDTag__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration, value: CGRIDMap__Proto009PsFLorenOperationMetadataAlphaUpdateOriginOrigin['Protocol_migration'] };
export type Proto009PsFLorenOperationMetadataAlphaBalanceRewardsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto009PsFLorenOperationMetadataAlphaBalanceFeesDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto009PsFLorenOperationMetadataAlphaBalanceDepositsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance{
    Contract = 0,
    Rewards = 1,
    Fees = 2,
    Deposits = 3
}
export interface CGRIDMap__Proto009PsFLorenOperationMetadataAlphaBalance {
    Contract: CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Contract,
    Rewards: CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Rewards,
    Fees: CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Fees,
    Deposits: CGRIDClass__Proto009PsFLorenOperationMetadataAlphaBalance__Deposits
}
export type Proto009PsFLorenOperationMetadataAlphaBalance = { kind: CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance.Contract, value: CGRIDMap__Proto009PsFLorenOperationMetadataAlphaBalance['Contract'] } | { kind: CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance.Rewards, value: CGRIDMap__Proto009PsFLorenOperationMetadataAlphaBalance['Rewards'] } | { kind: CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance.Fees, value: CGRIDMap__Proto009PsFLorenOperationMetadataAlphaBalance['Fees'] } | { kind: CGRIDTag__Proto009PsFLorenOperationMetadataAlphaBalance.Deposits, value: CGRIDMap__Proto009PsFLorenOperationMetadataAlphaBalance['Deposits'] };
export type Proto009PsFLorenContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto009PsFLorenContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto009PsFLorenContractId {
    Implicit: CGRIDClass__Proto009PsFLorenContractId__Implicit,
    Originated: CGRIDClass__Proto009PsFLorenContractId__Originated
}
export type Proto009PsFLorenContractId = { kind: CGRIDTag__Proto009PsFLorenContractId.Implicit, value: CGRIDMap__Proto009PsFLorenContractId['Implicit'] } | { kind: CGRIDTag__Proto009PsFLorenContractId.Originated, value: CGRIDMap__Proto009PsFLorenContractId['Originated'] };
export type Proto009PsFLorenReceiptBalanceUpdates = Dynamic<Sequence<CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance>,width.Uint30>;
export class CGRIDClass__Proto009PsFLorenReceiptBalanceUpdates extends Box<Proto009PsFLorenReceiptBalanceUpdates> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenReceiptBalanceUpdates {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto009_psfloren_receipt_balance_updates_encoder = (value: Proto009PsFLorenReceiptBalanceUpdates): OutputBytes => {
    return value.encode();
}
export const proto009_psfloren_receipt_balance_updates_decoder = (p: Parser): Proto009PsFLorenReceiptBalanceUpdates => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto009_PsFLorenOperation_metadataAlphaBalance.decode), width.Uint30)(p);
}
