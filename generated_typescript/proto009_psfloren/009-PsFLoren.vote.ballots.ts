import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto009PsFLorenVoteBallots = { yay: Int32, nay: Int32, pass: Int32 };
export class CGRIDClass__Proto009PsFLorenVoteBallots extends Box<Proto009PsFLorenVoteBallots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['yay', 'nay', 'pass']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenVoteBallots {
        return new this(record_decoder<Proto009PsFLorenVoteBallots>({yay: Int32.decode, nay: Int32.decode, pass: Int32.decode}, {order: ['yay', 'nay', 'pass']})(p));
    };
    get encodeLength(): number {
        return (this.value.yay.encodeLength +  this.value.nay.encodeLength +  this.value.pass.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.yay.writeTarget(tgt) +  this.value.nay.writeTarget(tgt) +  this.value.pass.writeTarget(tgt));
    }
}
export const proto009_psfloren_vote_ballots_encoder = (value: Proto009PsFLorenVoteBallots): OutputBytes => {
    return record_encoder({order: ['yay', 'nay', 'pass']})(value);
}
export const proto009_psfloren_vote_ballots_decoder = (p: Parser): Proto009PsFLorenVoteBallots => {
    return record_decoder<Proto009PsFLorenVoteBallots>({yay: Int32.decode, nay: Int32.decode, pass: Int32.decode}, {order: ['yay', 'nay', 'pass']})(p);
}
