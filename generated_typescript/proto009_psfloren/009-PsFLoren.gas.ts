import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto009PsFLorenGas__Unaccounted generated for Proto009PsFLorenGas__Unaccounted
export class CGRIDClass__Proto009PsFLorenGas__Unaccounted extends Box<Proto009PsFLorenGas__Unaccounted> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenGas__Unaccounted {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto009PsFLorenGas__Limited generated for Proto009PsFLorenGas__Limited
export class CGRIDClass__Proto009PsFLorenGas__Limited extends Box<Proto009PsFLorenGas__Limited> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenGas__Limited {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto009PsFLorenGas__Unaccounted = Unit;
export type Proto009PsFLorenGas__Limited = Z;
export enum CGRIDTag__Proto009PsFLorenGas{
    Limited = 0,
    Unaccounted = 1
}
export interface CGRIDMap__Proto009PsFLorenGas {
    Limited: CGRIDClass__Proto009PsFLorenGas__Limited,
    Unaccounted: CGRIDClass__Proto009PsFLorenGas__Unaccounted
}
export type Proto009PsFLorenGas = { kind: CGRIDTag__Proto009PsFLorenGas.Limited, value: CGRIDMap__Proto009PsFLorenGas['Limited'] } | { kind: CGRIDTag__Proto009PsFLorenGas.Unaccounted, value: CGRIDMap__Proto009PsFLorenGas['Unaccounted'] };
export function proto009psflorengas_mkDecoder(): VariantDecoder<CGRIDTag__Proto009PsFLorenGas,Proto009PsFLorenGas> {
    function f(disc: CGRIDTag__Proto009PsFLorenGas) {
        switch (disc) {
            case CGRIDTag__Proto009PsFLorenGas.Limited: return CGRIDClass__Proto009PsFLorenGas__Limited.decode;
            case CGRIDTag__Proto009PsFLorenGas.Unaccounted: return CGRIDClass__Proto009PsFLorenGas__Unaccounted.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto009PsFLorenGas => Object.values(CGRIDTag__Proto009PsFLorenGas).includes(tagval);
    return f;
}
export class CGRIDClass__Proto009PsFLorenGas extends Box<Proto009PsFLorenGas> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto009PsFLorenGas>, Proto009PsFLorenGas>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenGas {
        return new this(variant_decoder(width.Uint8)(proto009psflorengas_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto009_psfloren_gas_encoder = (value: Proto009PsFLorenGas): OutputBytes => {
    return variant_encoder<KindOf<Proto009PsFLorenGas>, Proto009PsFLorenGas>(width.Uint8)(value);
}
export const proto009_psfloren_gas_decoder = (p: Parser): Proto009PsFLorenGas => {
    return variant_decoder(width.Uint8)(proto009psflorengas_mkDecoder())(p);
}
