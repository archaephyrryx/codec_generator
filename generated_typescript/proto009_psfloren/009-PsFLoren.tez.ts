import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto009PsFLorenTez = N;
export class CGRIDClass__Proto009PsFLorenTez extends Box<Proto009PsFLorenTez> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto009PsFLorenTez {
        return new this(N.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto009_psfloren_tez_encoder = (value: Proto009PsFLorenTez): OutputBytes => {
    return value.encode();
}
export const proto009_psfloren_tez_decoder = (p: Parser): Proto009PsFLorenTez => {
    return N.decode(p);
}
