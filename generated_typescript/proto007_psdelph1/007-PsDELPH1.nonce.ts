import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type Proto007PsDELPH1Nonce = FixedBytes<32>;
export class CGRIDClass__Proto007PsDELPH1Nonce extends Box<Proto007PsDELPH1Nonce> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1Nonce {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto007_psdelph1_nonce_encoder = (value: Proto007PsDELPH1Nonce): OutputBytes => {
    return value.encode();
}
export const proto007_psdelph1_nonce_decoder = (p: Parser): Proto007PsDELPH1Nonce => {
    return FixedBytes.decode<32>({len: 32})(p);
}
