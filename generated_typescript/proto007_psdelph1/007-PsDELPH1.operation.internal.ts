import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Transaction generated for Proto007PsDELPH1OperationAlphaInternalOperationRhs__Transaction
export class CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Transaction extends Box<Proto007PsDELPH1OperationAlphaInternalOperationRhs__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Transaction {
        return new this(record_decoder<Proto007PsDELPH1OperationAlphaInternalOperationRhs__Transaction>({amount: N.decode, destination: CGRIDClass__Proto007_PsDELPH1Contract_id.decode, parameters: Option.decode(CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Transaction_parameters.decode)}, {order: ['amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Reveal generated for Proto007PsDELPH1OperationAlphaInternalOperationRhs__Reveal
export class CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Reveal extends Box<Proto007PsDELPH1OperationAlphaInternalOperationRhs__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Reveal {
        return new this(record_decoder<Proto007PsDELPH1OperationAlphaInternalOperationRhs__Reveal>({public_key: CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Reveal_public_key.decode}, {order: ['public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Origination generated for Proto007PsDELPH1OperationAlphaInternalOperationRhs__Origination
export class CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Origination extends Box<Proto007PsDELPH1OperationAlphaInternalOperationRhs__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Origination {
        return new this(record_decoder<Proto007PsDELPH1OperationAlphaInternalOperationRhs__Origination>({balance: N.decode, delegate: Option.decode(CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Origination_delegate.decode), script: CGRIDClass__Proto007_PsDELPH1ScriptedContracts.decode}, {order: ['balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Delegation generated for Proto007PsDELPH1OperationAlphaInternalOperationRhs__Delegation
export class CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Delegation extends Box<Proto007PsDELPH1OperationAlphaInternalOperationRhs__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Delegation {
        return new this(record_decoder<Proto007PsDELPH1OperationAlphaInternalOperationRhs__Delegation>({delegate: Option.decode(CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Delegation_delegate.decode)}, {order: ['delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007PsDELPH1Entrypoint__set_delegate generated for Proto007PsDELPH1Entrypoint__set_delegate
export class CGRIDClass__Proto007PsDELPH1Entrypoint__set_delegate extends Box<Proto007PsDELPH1Entrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1Entrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto007PsDELPH1Entrypoint__root generated for Proto007PsDELPH1Entrypoint__root
export class CGRIDClass__Proto007PsDELPH1Entrypoint__root extends Box<Proto007PsDELPH1Entrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1Entrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto007PsDELPH1Entrypoint__remove_delegate generated for Proto007PsDELPH1Entrypoint__remove_delegate
export class CGRIDClass__Proto007PsDELPH1Entrypoint__remove_delegate extends Box<Proto007PsDELPH1Entrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1Entrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto007PsDELPH1Entrypoint__named generated for Proto007PsDELPH1Entrypoint__named
export class CGRIDClass__Proto007PsDELPH1Entrypoint__named extends Box<Proto007PsDELPH1Entrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1Entrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto007PsDELPH1Entrypoint___do generated for Proto007PsDELPH1Entrypoint___do
export class CGRIDClass__Proto007PsDELPH1Entrypoint___do extends Box<Proto007PsDELPH1Entrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1Entrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto007PsDELPH1Entrypoint___default generated for Proto007PsDELPH1Entrypoint___default
export class CGRIDClass__Proto007PsDELPH1Entrypoint___default extends Box<Proto007PsDELPH1Entrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1Entrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto007PsDELPH1ContractId__Originated generated for Proto007PsDELPH1ContractId__Originated
export class CGRIDClass__Proto007PsDELPH1ContractId__Originated extends Box<Proto007PsDELPH1ContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1ContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto007_PsDELPH1Contract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto007PsDELPH1ContractId__Implicit generated for Proto007PsDELPH1ContractId__Implicit
export class CGRIDClass__Proto007PsDELPH1ContractId__Implicit extends Box<Proto007PsDELPH1ContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1ContractId__Implicit {
        return new this(record_decoder<Proto007PsDELPH1ContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto007PsDELPH1OperationAlphaInternalOperationRhs__Transaction = { amount: N, destination: CGRIDClass__Proto007_PsDELPH1Contract_id, parameters: Option<CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Transaction_parameters> };
export type Proto007PsDELPH1OperationAlphaInternalOperationRhs__Reveal = { public_key: CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Reveal_public_key };
export type Proto007PsDELPH1OperationAlphaInternalOperationRhs__Origination = { balance: N, delegate: Option<CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Origination_delegate>, script: CGRIDClass__Proto007_PsDELPH1ScriptedContracts };
export type Proto007PsDELPH1OperationAlphaInternalOperationRhs__Delegation = { delegate: Option<CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Delegation_delegate> };
export type Proto007PsDELPH1Entrypoint__set_delegate = Unit;
export type Proto007PsDELPH1Entrypoint__root = Unit;
export type Proto007PsDELPH1Entrypoint__remove_delegate = Unit;
export type Proto007PsDELPH1Entrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto007PsDELPH1Entrypoint___do = Unit;
export type Proto007PsDELPH1Entrypoint___default = Unit;
export type Proto007PsDELPH1ContractId__Originated = Padded<CGRIDClass__Proto007_PsDELPH1Contract_id_Originated_denest_pad,1>;
export type Proto007PsDELPH1ContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007_PsDELPH1ScriptedContracts generated for Proto007PsDELPH1ScriptedContracts
export class CGRIDClass__Proto007_PsDELPH1ScriptedContracts extends Box<Proto007PsDELPH1ScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007_PsDELPH1ScriptedContracts {
        return new this(record_decoder<Proto007PsDELPH1ScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_rhs generated for Proto007PsDELPH1OperationAlphaInternalOperationRhs
export function proto007psdelph1operationalphainternaloperationrhs_mkDecoder(): VariantDecoder<CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs,Proto007PsDELPH1OperationAlphaInternalOperationRhs> {
    function f(disc: CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs) {
        switch (disc) {
            case CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs.Reveal: return CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Reveal.decode;
            case CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs.Transaction: return CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Transaction.decode;
            case CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs.Origination: return CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Origination.decode;
            case CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs.Delegation: return CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Delegation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs => Object.values(CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs).includes(tagval);
    return f;
}
export class CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_rhs extends Box<Proto007PsDELPH1OperationAlphaInternalOperationRhs> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto007PsDELPH1OperationAlphaInternalOperationRhs>, Proto007PsDELPH1OperationAlphaInternalOperationRhs>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_rhs {
        return new this(variant_decoder(width.Uint8)(proto007psdelph1operationalphainternaloperationrhs_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Transaction_parameters generated for Proto007PsDELPH1OperationAlphaInternalOperationTransactionParameters
export class CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Transaction_parameters extends Box<Proto007PsDELPH1OperationAlphaInternalOperationTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Transaction_parameters {
        return new this(record_decoder<Proto007PsDELPH1OperationAlphaInternalOperationTransactionParameters>({entrypoint: CGRIDClass__Proto007_PsDELPH1Entrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Reveal_public_key generated for Proto007PsDELPH1OperationAlphaInternalOperationRevealPublicKey
export class CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Reveal_public_key extends Box<Proto007PsDELPH1OperationAlphaInternalOperationRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Reveal_public_key {
        return new this(record_decoder<Proto007PsDELPH1OperationAlphaInternalOperationRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Origination_delegate generated for Proto007PsDELPH1OperationAlphaInternalOperationOriginationDelegate
export class CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Origination_delegate extends Box<Proto007PsDELPH1OperationAlphaInternalOperationOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Origination_delegate {
        return new this(record_decoder<Proto007PsDELPH1OperationAlphaInternalOperationOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Delegation_delegate generated for Proto007PsDELPH1OperationAlphaInternalOperationDelegationDelegate
export class CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Delegation_delegate extends Box<Proto007PsDELPH1OperationAlphaInternalOperationDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_Delegation_delegate {
        return new this(record_decoder<Proto007PsDELPH1OperationAlphaInternalOperationDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007_PsDELPH1Entrypoint generated for Proto007PsDELPH1Entrypoint
export function proto007psdelph1entrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto007PsDELPH1Entrypoint,Proto007PsDELPH1Entrypoint> {
    function f(disc: CGRIDTag__Proto007PsDELPH1Entrypoint) {
        switch (disc) {
            case CGRIDTag__Proto007PsDELPH1Entrypoint._default: return CGRIDClass__Proto007PsDELPH1Entrypoint___default.decode;
            case CGRIDTag__Proto007PsDELPH1Entrypoint.root: return CGRIDClass__Proto007PsDELPH1Entrypoint__root.decode;
            case CGRIDTag__Proto007PsDELPH1Entrypoint._do: return CGRIDClass__Proto007PsDELPH1Entrypoint___do.decode;
            case CGRIDTag__Proto007PsDELPH1Entrypoint.set_delegate: return CGRIDClass__Proto007PsDELPH1Entrypoint__set_delegate.decode;
            case CGRIDTag__Proto007PsDELPH1Entrypoint.remove_delegate: return CGRIDClass__Proto007PsDELPH1Entrypoint__remove_delegate.decode;
            case CGRIDTag__Proto007PsDELPH1Entrypoint.named: return CGRIDClass__Proto007PsDELPH1Entrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto007PsDELPH1Entrypoint => Object.values(CGRIDTag__Proto007PsDELPH1Entrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto007_PsDELPH1Entrypoint extends Box<Proto007PsDELPH1Entrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto007PsDELPH1Entrypoint>, Proto007PsDELPH1Entrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007_PsDELPH1Entrypoint {
        return new this(variant_decoder(width.Uint8)(proto007psdelph1entrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007_PsDELPH1Contract_id_Originated_denest_pad generated for Proto007PsDELPH1ContractIdOriginatedDenestPad
export class CGRIDClass__Proto007_PsDELPH1Contract_id_Originated_denest_pad extends Box<Proto007PsDELPH1ContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007_PsDELPH1Contract_id_Originated_denest_pad {
        return new this(record_decoder<Proto007PsDELPH1ContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto007_PsDELPH1Contract_id generated for Proto007PsDELPH1ContractId
export function proto007psdelph1contractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto007PsDELPH1ContractId,Proto007PsDELPH1ContractId> {
    function f(disc: CGRIDTag__Proto007PsDELPH1ContractId) {
        switch (disc) {
            case CGRIDTag__Proto007PsDELPH1ContractId.Implicit: return CGRIDClass__Proto007PsDELPH1ContractId__Implicit.decode;
            case CGRIDTag__Proto007PsDELPH1ContractId.Originated: return CGRIDClass__Proto007PsDELPH1ContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto007PsDELPH1ContractId => Object.values(CGRIDTag__Proto007PsDELPH1ContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto007_PsDELPH1Contract_id extends Box<Proto007PsDELPH1ContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto007PsDELPH1ContractId>, Proto007PsDELPH1ContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007_PsDELPH1Contract_id {
        return new this(variant_decoder(width.Uint8)(proto007psdelph1contractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type Proto007PsDELPH1ScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export enum CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs{
    Reveal = 0,
    Transaction = 1,
    Origination = 2,
    Delegation = 3
}
export interface CGRIDMap__Proto007PsDELPH1OperationAlphaInternalOperationRhs {
    Reveal: CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Reveal,
    Transaction: CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Transaction,
    Origination: CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Origination,
    Delegation: CGRIDClass__Proto007PsDELPH1OperationAlphaInternalOperationRhs__Delegation
}
export type Proto007PsDELPH1OperationAlphaInternalOperationRhs = { kind: CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs.Reveal, value: CGRIDMap__Proto007PsDELPH1OperationAlphaInternalOperationRhs['Reveal'] } | { kind: CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs.Transaction, value: CGRIDMap__Proto007PsDELPH1OperationAlphaInternalOperationRhs['Transaction'] } | { kind: CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs.Origination, value: CGRIDMap__Proto007PsDELPH1OperationAlphaInternalOperationRhs['Origination'] } | { kind: CGRIDTag__Proto007PsDELPH1OperationAlphaInternalOperationRhs.Delegation, value: CGRIDMap__Proto007PsDELPH1OperationAlphaInternalOperationRhs['Delegation'] };
export type Proto007PsDELPH1OperationAlphaInternalOperationTransactionParameters = { entrypoint: CGRIDClass__Proto007_PsDELPH1Entrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto007PsDELPH1OperationAlphaInternalOperationRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto007PsDELPH1OperationAlphaInternalOperationOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto007PsDELPH1OperationAlphaInternalOperationDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto007PsDELPH1Entrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto007PsDELPH1Entrypoint {
    _default: CGRIDClass__Proto007PsDELPH1Entrypoint___default,
    root: CGRIDClass__Proto007PsDELPH1Entrypoint__root,
    _do: CGRIDClass__Proto007PsDELPH1Entrypoint___do,
    set_delegate: CGRIDClass__Proto007PsDELPH1Entrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto007PsDELPH1Entrypoint__remove_delegate,
    named: CGRIDClass__Proto007PsDELPH1Entrypoint__named
}
export type Proto007PsDELPH1Entrypoint = { kind: CGRIDTag__Proto007PsDELPH1Entrypoint._default, value: CGRIDMap__Proto007PsDELPH1Entrypoint['_default'] } | { kind: CGRIDTag__Proto007PsDELPH1Entrypoint.root, value: CGRIDMap__Proto007PsDELPH1Entrypoint['root'] } | { kind: CGRIDTag__Proto007PsDELPH1Entrypoint._do, value: CGRIDMap__Proto007PsDELPH1Entrypoint['_do'] } | { kind: CGRIDTag__Proto007PsDELPH1Entrypoint.set_delegate, value: CGRIDMap__Proto007PsDELPH1Entrypoint['set_delegate'] } | { kind: CGRIDTag__Proto007PsDELPH1Entrypoint.remove_delegate, value: CGRIDMap__Proto007PsDELPH1Entrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto007PsDELPH1Entrypoint.named, value: CGRIDMap__Proto007PsDELPH1Entrypoint['named'] };
export type Proto007PsDELPH1ContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto007PsDELPH1ContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto007PsDELPH1ContractId {
    Implicit: CGRIDClass__Proto007PsDELPH1ContractId__Implicit,
    Originated: CGRIDClass__Proto007PsDELPH1ContractId__Originated
}
export type Proto007PsDELPH1ContractId = { kind: CGRIDTag__Proto007PsDELPH1ContractId.Implicit, value: CGRIDMap__Proto007PsDELPH1ContractId['Implicit'] } | { kind: CGRIDTag__Proto007PsDELPH1ContractId.Originated, value: CGRIDMap__Proto007PsDELPH1ContractId['Originated'] };
export type Proto007PsDELPH1OperationInternal = { source: CGRIDClass__Proto007_PsDELPH1Contract_id, nonce: Uint16, proto007_psdelph1_operation_alpha_internal_operation_rhs: CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_rhs };
export class CGRIDClass__Proto007PsDELPH1OperationInternal extends Box<Proto007PsDELPH1OperationInternal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'nonce', 'proto007_psdelph1_operation_alpha_internal_operation_rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1OperationInternal {
        return new this(record_decoder<Proto007PsDELPH1OperationInternal>({source: CGRIDClass__Proto007_PsDELPH1Contract_id.decode, nonce: Uint16.decode, proto007_psdelph1_operation_alpha_internal_operation_rhs: CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto007_psdelph1_operation_alpha_internal_operation_rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.nonce.encodeLength +  this.value.proto007_psdelph1_operation_alpha_internal_operation_rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt) +  this.value.proto007_psdelph1_operation_alpha_internal_operation_rhs.writeTarget(tgt));
    }
}
export const proto007_psdelph1_operation_internal_encoder = (value: Proto007PsDELPH1OperationInternal): OutputBytes => {
    return record_encoder({order: ['source', 'nonce', 'proto007_psdelph1_operation_alpha_internal_operation_rhs']})(value);
}
export const proto007_psdelph1_operation_internal_decoder = (p: Parser): Proto007PsDELPH1OperationInternal => {
    return record_decoder<Proto007PsDELPH1OperationInternal>({source: CGRIDClass__Proto007_PsDELPH1Contract_id.decode, nonce: Uint16.decode, proto007_psdelph1_operation_alpha_internal_operation_rhs: CGRIDClass__Proto007_PsDELPH1OperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto007_psdelph1_operation_alpha_internal_operation_rhs']})(p);
}
