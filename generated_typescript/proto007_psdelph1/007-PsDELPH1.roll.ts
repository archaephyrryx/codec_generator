import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto007PsDELPH1Roll = Int32;
export class CGRIDClass__Proto007PsDELPH1Roll extends Box<Proto007PsDELPH1Roll> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1Roll {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto007_psdelph1_roll_encoder = (value: Proto007PsDELPH1Roll): OutputBytes => {
    return value.encode();
}
export const proto007_psdelph1_roll_decoder = (p: Parser): Proto007PsDELPH1Roll => {
    return Int32.decode(p);
}
