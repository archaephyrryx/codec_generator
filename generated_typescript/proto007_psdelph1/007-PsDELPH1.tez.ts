import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto007PsDELPH1Tez = N;
export class CGRIDClass__Proto007PsDELPH1Tez extends Box<Proto007PsDELPH1Tez> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1Tez {
        return new this(N.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto007_psdelph1_tez_encoder = (value: Proto007PsDELPH1Tez): OutputBytes => {
    return value.encode();
}
export const proto007_psdelph1_tez_decoder = (p: Parser): Proto007PsDELPH1Tez => {
    return N.decode(p);
}
