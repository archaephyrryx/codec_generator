import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
export type Proto007PsDELPH1GasCost = Z;
export class CGRIDClass__Proto007PsDELPH1GasCost extends Box<Proto007PsDELPH1GasCost> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1GasCost {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto007_psdelph1_gas_cost_encoder = (value: Proto007PsDELPH1GasCost): OutputBytes => {
    return value.encode();
}
export const proto007_psdelph1_gas_cost_decoder = (p: Parser): Proto007PsDELPH1GasCost => {
    return Z.decode(p);
}
