import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { Target } from '../../ts_runtime/target';
export type Proto007PsDELPH1Fitness = Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>;
export class CGRIDClass__Proto007PsDELPH1Fitness extends Box<Proto007PsDELPH1Fitness> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto007PsDELPH1Fitness {
        return new this(Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto007_psdelph1_fitness_encoder = (value: Proto007PsDELPH1Fitness): OutputBytes => {
    return value.encode();
}
export const proto007_psdelph1_fitness_decoder = (p: Parser): Proto007PsDELPH1Fitness => {
    return Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30)(p);
}
