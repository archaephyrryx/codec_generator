import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
export type Proto014PtKathmaGasCost = Z;
export class CGRIDClass__Proto014PtKathmaGasCost extends Box<Proto014PtKathmaGasCost> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaGasCost {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto014_ptkathma_gas_cost_encoder = (value: Proto014PtKathmaGasCost): OutputBytes => {
    return value.encode();
}
export const proto014_ptkathma_gas_cost_decoder = (p: Parser): Proto014PtKathmaGasCost => {
    return Z.decode(p);
}
