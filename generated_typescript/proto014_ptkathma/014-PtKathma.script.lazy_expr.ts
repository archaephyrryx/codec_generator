import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { Target } from '../../ts_runtime/target';
export type Proto014PtKathmaScriptLazyExpr = Dynamic<Bytes,width.Uint30>;
export class CGRIDClass__Proto014PtKathmaScriptLazyExpr extends Box<Proto014PtKathmaScriptLazyExpr> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaScriptLazyExpr {
        return new this(Dynamic.decode(Bytes.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto014_ptkathma_script_lazy_expr_encoder = (value: Proto014PtKathmaScriptLazyExpr): OutputBytes => {
    return value.encode();
}
export const proto014_ptkathma_script_lazy_expr_decoder = (p: Parser): Proto014PtKathmaScriptLazyExpr => {
    return Dynamic.decode(Bytes.decode, width.Uint30)(p);
}
