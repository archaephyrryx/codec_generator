import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto014PtKathmaTez = N;
export class CGRIDClass__Proto014PtKathmaTez extends Box<Proto014PtKathmaTez> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaTez {
        return new this(N.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto014_ptkathma_tez_encoder = (value: Proto014PtKathmaTez): OutputBytes => {
    return value.encode();
}
export const proto014_ptkathma_tez_decoder = (p: Parser): Proto014PtKathmaTez => {
    return N.decode(p);
}
