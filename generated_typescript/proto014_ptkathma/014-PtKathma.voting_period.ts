import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto014PtKathmaVotingPeriodKind__exploration generated for Proto014PtKathmaVotingPeriodKind__exploration
export class CGRIDClass__Proto014PtKathmaVotingPeriodKind__exploration extends Box<Proto014PtKathmaVotingPeriodKind__exploration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaVotingPeriodKind__exploration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaVotingPeriodKind__Proposal generated for Proto014PtKathmaVotingPeriodKind__Proposal
export class CGRIDClass__Proto014PtKathmaVotingPeriodKind__Proposal extends Box<Proto014PtKathmaVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaVotingPeriodKind__Promotion generated for Proto014PtKathmaVotingPeriodKind__Promotion
export class CGRIDClass__Proto014PtKathmaVotingPeriodKind__Promotion extends Box<Proto014PtKathmaVotingPeriodKind__Promotion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaVotingPeriodKind__Promotion {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaVotingPeriodKind__Cooldown generated for Proto014PtKathmaVotingPeriodKind__Cooldown
export class CGRIDClass__Proto014PtKathmaVotingPeriodKind__Cooldown extends Box<Proto014PtKathmaVotingPeriodKind__Cooldown> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaVotingPeriodKind__Cooldown {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaVotingPeriodKind__Adoption generated for Proto014PtKathmaVotingPeriodKind__Adoption
export class CGRIDClass__Proto014PtKathmaVotingPeriodKind__Adoption extends Box<Proto014PtKathmaVotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaVotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto014PtKathmaVotingPeriodKind__exploration = Unit;
export type Proto014PtKathmaVotingPeriodKind__Proposal = Unit;
export type Proto014PtKathmaVotingPeriodKind__Promotion = Unit;
export type Proto014PtKathmaVotingPeriodKind__Cooldown = Unit;
export type Proto014PtKathmaVotingPeriodKind__Adoption = Unit;
// Class CGRIDClass__Proto014_PtKathmaVoting_period_kind generated for Proto014PtKathmaVotingPeriodKind
export function proto014ptkathmavotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaVotingPeriodKind,Proto014PtKathmaVotingPeriodKind> {
    function f(disc: CGRIDTag__Proto014PtKathmaVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaVotingPeriodKind.Proposal: return CGRIDClass__Proto014PtKathmaVotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto014PtKathmaVotingPeriodKind.exploration: return CGRIDClass__Proto014PtKathmaVotingPeriodKind__exploration.decode;
            case CGRIDTag__Proto014PtKathmaVotingPeriodKind.Cooldown: return CGRIDClass__Proto014PtKathmaVotingPeriodKind__Cooldown.decode;
            case CGRIDTag__Proto014PtKathmaVotingPeriodKind.Promotion: return CGRIDClass__Proto014PtKathmaVotingPeriodKind__Promotion.decode;
            case CGRIDTag__Proto014PtKathmaVotingPeriodKind.Adoption: return CGRIDClass__Proto014PtKathmaVotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaVotingPeriodKind => Object.values(CGRIDTag__Proto014PtKathmaVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaVoting_period_kind extends Box<Proto014PtKathmaVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaVotingPeriodKind>, Proto014PtKathmaVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaVoting_period_kind {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmavotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__Proto014PtKathmaVotingPeriodKind{
    Proposal = 0,
    exploration = 1,
    Cooldown = 2,
    Promotion = 3,
    Adoption = 4
}
export interface CGRIDMap__Proto014PtKathmaVotingPeriodKind {
    Proposal: CGRIDClass__Proto014PtKathmaVotingPeriodKind__Proposal,
    exploration: CGRIDClass__Proto014PtKathmaVotingPeriodKind__exploration,
    Cooldown: CGRIDClass__Proto014PtKathmaVotingPeriodKind__Cooldown,
    Promotion: CGRIDClass__Proto014PtKathmaVotingPeriodKind__Promotion,
    Adoption: CGRIDClass__Proto014PtKathmaVotingPeriodKind__Adoption
}
export type Proto014PtKathmaVotingPeriodKind = { kind: CGRIDTag__Proto014PtKathmaVotingPeriodKind.Proposal, value: CGRIDMap__Proto014PtKathmaVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto014PtKathmaVotingPeriodKind.exploration, value: CGRIDMap__Proto014PtKathmaVotingPeriodKind['exploration'] } | { kind: CGRIDTag__Proto014PtKathmaVotingPeriodKind.Cooldown, value: CGRIDMap__Proto014PtKathmaVotingPeriodKind['Cooldown'] } | { kind: CGRIDTag__Proto014PtKathmaVotingPeriodKind.Promotion, value: CGRIDMap__Proto014PtKathmaVotingPeriodKind['Promotion'] } | { kind: CGRIDTag__Proto014PtKathmaVotingPeriodKind.Adoption, value: CGRIDMap__Proto014PtKathmaVotingPeriodKind['Adoption'] };
export type Proto014PtKathmaVotingPeriod = { index: Int32, kind: CGRIDClass__Proto014_PtKathmaVoting_period_kind, start_position: Int32 };
export class CGRIDClass__Proto014PtKathmaVotingPeriod extends Box<Proto014PtKathmaVotingPeriod> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['index', 'kind', 'start_position']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaVotingPeriod {
        return new this(record_decoder<Proto014PtKathmaVotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto014_PtKathmaVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p));
    };
    get encodeLength(): number {
        return (this.value.index.encodeLength +  this.value.kind.encodeLength +  this.value.start_position.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.index.writeTarget(tgt) +  this.value.kind.writeTarget(tgt) +  this.value.start_position.writeTarget(tgt));
    }
}
export const proto014_ptkathma_voting_period_encoder = (value: Proto014PtKathmaVotingPeriod): OutputBytes => {
    return record_encoder({order: ['index', 'kind', 'start_position']})(value);
}
export const proto014_ptkathma_voting_period_decoder = (p: Parser): Proto014PtKathmaVotingPeriod => {
    return record_decoder<Proto014PtKathmaVotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto014_PtKathmaVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p);
}
