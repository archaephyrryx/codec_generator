import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto014PtKathmaFitnessLockedRound__Some generated for Proto014PtKathmaFitnessLockedRound__Some
export class CGRIDClass__Proto014PtKathmaFitnessLockedRound__Some extends Box<Proto014PtKathmaFitnessLockedRound__Some> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaFitnessLockedRound__Some {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaFitnessLockedRound__None generated for Proto014PtKathmaFitnessLockedRound__None
export class CGRIDClass__Proto014PtKathmaFitnessLockedRound__None extends Box<Proto014PtKathmaFitnessLockedRound__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaFitnessLockedRound__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto014PtKathmaFitnessLockedRound__Some = Int32;
export type Proto014PtKathmaFitnessLockedRound__None = Unit;
// Class CGRIDClass__Proto014_PtKathmaFitness_locked_round generated for Proto014PtKathmaFitnessLockedRound
export function proto014ptkathmafitnesslockedround_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaFitnessLockedRound,Proto014PtKathmaFitnessLockedRound> {
    function f(disc: CGRIDTag__Proto014PtKathmaFitnessLockedRound) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaFitnessLockedRound.None: return CGRIDClass__Proto014PtKathmaFitnessLockedRound__None.decode;
            case CGRIDTag__Proto014PtKathmaFitnessLockedRound.Some: return CGRIDClass__Proto014PtKathmaFitnessLockedRound__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaFitnessLockedRound => Object.values(CGRIDTag__Proto014PtKathmaFitnessLockedRound).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaFitness_locked_round extends Box<Proto014PtKathmaFitnessLockedRound> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaFitnessLockedRound>, Proto014PtKathmaFitnessLockedRound>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaFitness_locked_round {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmafitnesslockedround_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__Proto014PtKathmaFitnessLockedRound{
    None = 0,
    Some = 1
}
export interface CGRIDMap__Proto014PtKathmaFitnessLockedRound {
    None: CGRIDClass__Proto014PtKathmaFitnessLockedRound__None,
    Some: CGRIDClass__Proto014PtKathmaFitnessLockedRound__Some
}
export type Proto014PtKathmaFitnessLockedRound = { kind: CGRIDTag__Proto014PtKathmaFitnessLockedRound.None, value: CGRIDMap__Proto014PtKathmaFitnessLockedRound['None'] } | { kind: CGRIDTag__Proto014PtKathmaFitnessLockedRound.Some, value: CGRIDMap__Proto014PtKathmaFitnessLockedRound['Some'] };
export type Proto014PtKathmaFitness = { level: Int32, locked_round: CGRIDClass__Proto014_PtKathmaFitness_locked_round, predecessor_round: Int32, round: Int32 };
export class CGRIDClass__Proto014PtKathmaFitness extends Box<Proto014PtKathmaFitness> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaFitness {
        return new this(record_decoder<Proto014PtKathmaFitness>({level: Int32.decode, locked_round: CGRIDClass__Proto014_PtKathmaFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.locked_round.encodeLength +  this.value.predecessor_round.encodeLength +  this.value.round.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.locked_round.writeTarget(tgt) +  this.value.predecessor_round.writeTarget(tgt) +  this.value.round.writeTarget(tgt));
    }
}
export const proto014_ptkathma_fitness_encoder = (value: Proto014PtKathmaFitness): OutputBytes => {
    return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(value);
}
export const proto014_ptkathma_fitness_decoder = (p: Parser): Proto014PtKathmaFitness => {
    return record_decoder<Proto014PtKathmaFitness>({level: Int32.decode, locked_round: CGRIDClass__Proto014_PtKathmaFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p);
}
