import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto014PtKathmaTimestamp = Int64;
export class CGRIDClass__Proto014PtKathmaTimestamp extends Box<Proto014PtKathmaTimestamp> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaTimestamp {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto014_ptkathma_timestamp_encoder = (value: Proto014PtKathmaTimestamp): OutputBytes => {
    return value.encode();
}
export const proto014_ptkathma_timestamp_decoder = (p: Parser): Proto014PtKathmaTimestamp => {
    return Int64.decode(p);
}
