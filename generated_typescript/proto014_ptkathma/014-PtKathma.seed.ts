import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type Proto014PtKathmaSeed = FixedBytes<32>;
export class CGRIDClass__Proto014PtKathmaSeed extends Box<Proto014PtKathmaSeed> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaSeed {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto014_ptkathma_seed_encoder = (value: Proto014PtKathmaSeed): OutputBytes => {
    return value.encode();
}
export const proto014_ptkathma_seed_decoder = (p: Parser): Proto014PtKathmaSeed => {
    return FixedBytes.decode<32>({len: 32})(p);
}
