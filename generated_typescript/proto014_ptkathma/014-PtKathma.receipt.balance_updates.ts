import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Subsidy generated for Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Subsidy
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Subsidy extends Box<Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Subsidy> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Subsidy {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Simulation generated for Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Simulation
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Simulation extends Box<Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Simulation> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Simulation {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration generated for Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration extends Box<Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Block_application generated for Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Block_application
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Block_application extends Box<Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Block_application> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Block_application {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards generated for Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments generated for Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Storage_fees generated for Proto014PtKathmaOperationMetadataAlphaBalance__Storage_fees
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Storage_fees extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Storage_fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Storage_fees {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Storage_fees>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Sc_rollup_refutation_punishments generated for Proto014PtKathmaOperationMetadataAlphaBalance__Sc_rollup_refutation_punishments
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Sc_rollup_refutation_punishments extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Sc_rollup_refutation_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Sc_rollup_refutation_punishments {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Sc_rollup_refutation_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Nonce_revelation_rewards generated for Proto014PtKathmaOperationMetadataAlphaBalance__Nonce_revelation_rewards
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Nonce_revelation_rewards extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Nonce_revelation_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Nonce_revelation_rewards {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Nonce_revelation_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Minted generated for Proto014PtKathmaOperationMetadataAlphaBalance__Minted
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Minted extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Minted> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Minted {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Minted>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Lost_endorsing_rewards generated for Proto014PtKathmaOperationMetadataAlphaBalance__Lost_endorsing_rewards
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Lost_endorsing_rewards extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Lost_endorsing_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'participation', 'revelation', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Lost_endorsing_rewards {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Lost_endorsing_rewards>({category: Unit.decode, delegate: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate.decode, participation: Bool.decode, revelation: Bool.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'participation', 'revelation', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.participation.encodeLength +  this.value.revelation.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.participation.writeTarget(tgt) +  this.value.revelation.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Liquidity_baking_subsidies generated for Proto014PtKathmaOperationMetadataAlphaBalance__Liquidity_baking_subsidies
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Liquidity_baking_subsidies extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Liquidity_baking_subsidies> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Liquidity_baking_subsidies {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Liquidity_baking_subsidies>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Invoice generated for Proto014PtKathmaOperationMetadataAlphaBalance__Invoice
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Invoice extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Invoice> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Invoice {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Invoice>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Initial_commitments generated for Proto014PtKathmaOperationMetadataAlphaBalance__Initial_commitments
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Initial_commitments extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Initial_commitments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Initial_commitments {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Initial_commitments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Frozen_bonds generated for Proto014PtKathmaOperationMetadataAlphaBalance__Frozen_bonds
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Frozen_bonds extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Frozen_bonds> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'contract', 'bond_id', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Frozen_bonds {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Frozen_bonds>({category: Unit.decode, contract: CGRIDClass__Proto014_PtKathmaContract_id.decode, bond_id: CGRIDClass__Proto014_PtKathmaBond_id.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'contract', 'bond_id', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.contract.encodeLength +  this.value.bond_id.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.contract.writeTarget(tgt) +  this.value.bond_id.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Endorsing_rewards generated for Proto014PtKathmaOperationMetadataAlphaBalance__Endorsing_rewards
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Endorsing_rewards extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Endorsing_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Endorsing_rewards {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Endorsing_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_punishments generated for Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_punishments
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_punishments extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_punishments {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_evidence_rewards generated for Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_evidence_rewards
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_evidence_rewards extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_evidence_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_evidence_rewards {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_evidence_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Deposits generated for Proto014PtKathmaOperationMetadataAlphaBalance__Deposits
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Deposits extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Deposits> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Deposits {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Deposits>({category: Unit.decode, delegate: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Deposits_delegate.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Contract generated for Proto014PtKathmaOperationMetadataAlphaBalance__Contract
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Contract extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Contract> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Contract {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Contract>({contract: CGRIDClass__Proto014_PtKathmaContract_id.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['contract', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Commitments generated for Proto014PtKathmaOperationMetadataAlphaBalance__Commitments
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Commitments extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Commitments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'committer', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Commitments {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Commitments>({category: Unit.decode, committer: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Commitments_committer.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'committer', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.committer.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.committer.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Burned generated for Proto014PtKathmaOperationMetadataAlphaBalance__Burned
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Burned extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Burned> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Burned {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Burned>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Bootstrap generated for Proto014PtKathmaOperationMetadataAlphaBalance__Bootstrap
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Bootstrap extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Bootstrap> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Bootstrap {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Bootstrap>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Block_fees generated for Proto014PtKathmaOperationMetadataAlphaBalance__Block_fees
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Block_fees extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Block_fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Block_fees {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Block_fees>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Baking_rewards generated for Proto014PtKathmaOperationMetadataAlphaBalance__Baking_rewards
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Baking_rewards extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Baking_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Baking_rewards {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Baking_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Baking_bonuses generated for Proto014PtKathmaOperationMetadataAlphaBalance__Baking_bonuses
export class CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Baking_bonuses extends Box<Proto014PtKathmaOperationMetadataAlphaBalance__Baking_bonuses> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Baking_bonuses {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalance__Baking_bonuses>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaContractId__Originated generated for Proto014PtKathmaContractId__Originated
export class CGRIDClass__Proto014PtKathmaContractId__Originated extends Box<Proto014PtKathmaContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto014_PtKathmaContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaContractId__Implicit generated for Proto014PtKathmaContractId__Implicit
export class CGRIDClass__Proto014PtKathmaContractId__Implicit extends Box<Proto014PtKathmaContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaContractId__Implicit {
        return new this(record_decoder<Proto014PtKathmaContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaBondId__Tx_rollup_bond_id generated for Proto014PtKathmaBondId__Tx_rollup_bond_id
export class CGRIDClass__Proto014PtKathmaBondId__Tx_rollup_bond_id extends Box<Proto014PtKathmaBondId__Tx_rollup_bond_id> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['tx_rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaBondId__Tx_rollup_bond_id {
        return new this(record_decoder<Proto014PtKathmaBondId__Tx_rollup_bond_id>({tx_rollup: CGRIDClass__Proto014_PtKathmaTx_rollup_id.decode}, {order: ['tx_rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.tx_rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.tx_rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaBondId__Sc_rollup_bond_id generated for Proto014PtKathmaBondId__Sc_rollup_bond_id
export class CGRIDClass__Proto014PtKathmaBondId__Sc_rollup_bond_id extends Box<Proto014PtKathmaBondId__Sc_rollup_bond_id> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['sc_rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaBondId__Sc_rollup_bond_id {
        return new this(record_decoder<Proto014PtKathmaBondId__Sc_rollup_bond_id>({sc_rollup: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['sc_rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.sc_rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.sc_rollup.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Subsidy = Unit;
export type Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Simulation = Unit;
export type Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration = Unit;
export type Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Block_application = Unit;
export type Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Storage_fees = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Sc_rollup_refutation_punishments = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Nonce_revelation_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Minted = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Lost_endorsing_rewards = { category: Unit, delegate: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate, participation: Bool, revelation: Bool, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Liquidity_baking_subsidies = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Invoice = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Initial_commitments = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Frozen_bonds = { category: Unit, contract: CGRIDClass__Proto014_PtKathmaContract_id, bond_id: CGRIDClass__Proto014_PtKathmaBond_id, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Endorsing_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_punishments = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_evidence_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Deposits = { category: Unit, delegate: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Deposits_delegate, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Contract = { contract: CGRIDClass__Proto014_PtKathmaContract_id, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Commitments = { category: Unit, committer: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Commitments_committer, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Burned = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Bootstrap = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Block_fees = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Baking_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaOperationMetadataAlphaBalance__Baking_bonuses = { category: Unit, change: Int64, origin: CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin };
export type Proto014PtKathmaContractId__Originated = Padded<CGRIDClass__Proto014_PtKathmaContract_id_Originated_denest_pad,1>;
export type Proto014PtKathmaContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto014PtKathmaBondId__Tx_rollup_bond_id = { tx_rollup: CGRIDClass__Proto014_PtKathmaTx_rollup_id };
export type Proto014PtKathmaBondId__Sc_rollup_bond_id = { sc_rollup: Dynamic<U8String,width.Uint30> };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaTx_rollup_id generated for Proto014PtKathmaTxRollupId
export class CGRIDClass__Proto014_PtKathmaTx_rollup_id extends Box<Proto014PtKathmaTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaTx_rollup_id {
        return new this(record_decoder<Proto014PtKathmaTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin generated for Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin
export function proto014ptkathmaoperationmetadataalphaupdateoriginorigin_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin,Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin> {
    function f(disc: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin.Block_application: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Block_application.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin.Subsidy: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Subsidy.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin.Simulation: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Simulation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin => Object.values(CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin extends Box<Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin>, Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaUpdate_origin_origin {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmaoperationmetadataalphaupdateoriginorigin_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate generated for Proto014PtKathmaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate
export class CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate extends Box<Proto014PtKathmaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Deposits_delegate generated for Proto014PtKathmaOperationMetadataAlphaBalanceDepositsDelegate
export class CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Deposits_delegate extends Box<Proto014PtKathmaOperationMetadataAlphaBalanceDepositsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Deposits_delegate {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalanceDepositsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Commitments_committer generated for Proto014PtKathmaOperationMetadataAlphaBalanceCommitmentsCommitter
export class CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Commitments_committer extends Box<Proto014PtKathmaOperationMetadataAlphaBalanceCommitmentsCommitter> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['blinded_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance_Commitments_committer {
        return new this(record_decoder<Proto014PtKathmaOperationMetadataAlphaBalanceCommitmentsCommitter>({blinded_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['blinded_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.blinded_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.blinded_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance generated for Proto014PtKathmaOperationMetadataAlphaBalance
export function proto014ptkathmaoperationmetadataalphabalance_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance,Proto014PtKathmaOperationMetadataAlphaBalance> {
    function f(disc: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Contract: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Contract.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Block_fees: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Block_fees.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Deposits: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Deposits.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Nonce_revelation_rewards: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Nonce_revelation_rewards.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Double_signing_evidence_rewards: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_evidence_rewards.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Endorsing_rewards: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Endorsing_rewards.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Baking_rewards: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Baking_rewards.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Baking_bonuses: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Baking_bonuses.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Storage_fees: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Storage_fees.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Double_signing_punishments: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_punishments.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Lost_endorsing_rewards: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Lost_endorsing_rewards.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Liquidity_baking_subsidies: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Liquidity_baking_subsidies.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Burned: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Burned.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Commitments: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Commitments.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Bootstrap: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Bootstrap.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Invoice: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Invoice.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Initial_commitments: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Initial_commitments.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Minted: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Minted.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Frozen_bonds: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Frozen_bonds.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Tx_rollup_rejection_rewards: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Tx_rollup_rejection_punishments: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments.decode;
            case CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Sc_rollup_refutation_punishments: return CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Sc_rollup_refutation_punishments.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance => Object.values(CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance extends Box<Proto014PtKathmaOperationMetadataAlphaBalance> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaOperationMetadataAlphaBalance>, Proto014PtKathmaOperationMetadataAlphaBalance>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmaoperationmetadataalphabalance_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaContract_id_Originated_denest_pad generated for Proto014PtKathmaContractIdOriginatedDenestPad
export class CGRIDClass__Proto014_PtKathmaContract_id_Originated_denest_pad extends Box<Proto014PtKathmaContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto014PtKathmaContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaContract_id generated for Proto014PtKathmaContractId
export function proto014ptkathmacontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaContractId,Proto014PtKathmaContractId> {
    function f(disc: CGRIDTag__Proto014PtKathmaContractId) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaContractId.Implicit: return CGRIDClass__Proto014PtKathmaContractId__Implicit.decode;
            case CGRIDTag__Proto014PtKathmaContractId.Originated: return CGRIDClass__Proto014PtKathmaContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaContractId => Object.values(CGRIDTag__Proto014PtKathmaContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaContract_id extends Box<Proto014PtKathmaContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaContractId>, Proto014PtKathmaContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaContract_id {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmacontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaBond_id generated for Proto014PtKathmaBondId
export function proto014ptkathmabondid_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaBondId,Proto014PtKathmaBondId> {
    function f(disc: CGRIDTag__Proto014PtKathmaBondId) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaBondId.Tx_rollup_bond_id: return CGRIDClass__Proto014PtKathmaBondId__Tx_rollup_bond_id.decode;
            case CGRIDTag__Proto014PtKathmaBondId.Sc_rollup_bond_id: return CGRIDClass__Proto014PtKathmaBondId__Sc_rollup_bond_id.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaBondId => Object.values(CGRIDTag__Proto014PtKathmaBondId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaBond_id extends Box<Proto014PtKathmaBondId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaBondId>, Proto014PtKathmaBondId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaBond_id {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmabondid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export type Proto014PtKathmaTxRollupId = { rollup_hash: FixedBytes<20> };
export enum CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin{
    Block_application = 0,
    Protocol_migration = 1,
    Subsidy = 2,
    Simulation = 3
}
export interface CGRIDMap__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin {
    Block_application: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Block_application,
    Protocol_migration: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration,
    Subsidy: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Subsidy,
    Simulation: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin__Simulation
}
export type Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin = { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin.Block_application, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin['Block_application'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin['Protocol_migration'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin.Subsidy, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin['Subsidy'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin.Simulation, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaUpdateOriginOrigin['Simulation'] };
export type Proto014PtKathmaOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto014PtKathmaOperationMetadataAlphaBalanceDepositsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto014PtKathmaOperationMetadataAlphaBalanceCommitmentsCommitter = { blinded_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance{
    Contract = 0,
    Block_fees = 2,
    Deposits = 4,
    Nonce_revelation_rewards = 5,
    Double_signing_evidence_rewards = 6,
    Endorsing_rewards = 7,
    Baking_rewards = 8,
    Baking_bonuses = 9,
    Storage_fees = 11,
    Double_signing_punishments = 12,
    Lost_endorsing_rewards = 13,
    Liquidity_baking_subsidies = 14,
    Burned = 15,
    Commitments = 16,
    Bootstrap = 17,
    Invoice = 18,
    Initial_commitments = 19,
    Minted = 20,
    Frozen_bonds = 21,
    Tx_rollup_rejection_rewards = 22,
    Tx_rollup_rejection_punishments = 23,
    Sc_rollup_refutation_punishments = 24
}
export interface CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance {
    Contract: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Contract,
    Block_fees: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Block_fees,
    Deposits: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Deposits,
    Nonce_revelation_rewards: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Nonce_revelation_rewards,
    Double_signing_evidence_rewards: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_evidence_rewards,
    Endorsing_rewards: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Endorsing_rewards,
    Baking_rewards: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Baking_rewards,
    Baking_bonuses: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Baking_bonuses,
    Storage_fees: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Storage_fees,
    Double_signing_punishments: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Double_signing_punishments,
    Lost_endorsing_rewards: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Lost_endorsing_rewards,
    Liquidity_baking_subsidies: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Liquidity_baking_subsidies,
    Burned: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Burned,
    Commitments: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Commitments,
    Bootstrap: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Bootstrap,
    Invoice: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Invoice,
    Initial_commitments: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Initial_commitments,
    Minted: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Minted,
    Frozen_bonds: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Frozen_bonds,
    Tx_rollup_rejection_rewards: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards,
    Tx_rollup_rejection_punishments: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments,
    Sc_rollup_refutation_punishments: CGRIDClass__Proto014PtKathmaOperationMetadataAlphaBalance__Sc_rollup_refutation_punishments
}
export type Proto014PtKathmaOperationMetadataAlphaBalance = { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Contract, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Contract'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Block_fees, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Block_fees'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Deposits, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Deposits'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Nonce_revelation_rewards, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Nonce_revelation_rewards'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Double_signing_evidence_rewards, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Double_signing_evidence_rewards'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Endorsing_rewards, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Endorsing_rewards'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Baking_rewards, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Baking_rewards'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Baking_bonuses, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Baking_bonuses'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Storage_fees, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Storage_fees'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Double_signing_punishments, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Double_signing_punishments'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Lost_endorsing_rewards, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Lost_endorsing_rewards'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Liquidity_baking_subsidies, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Liquidity_baking_subsidies'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Burned, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Burned'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Commitments, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Commitments'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Bootstrap, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Bootstrap'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Invoice, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Invoice'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Initial_commitments, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Initial_commitments'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Minted, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Minted'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Frozen_bonds, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Frozen_bonds'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Tx_rollup_rejection_rewards, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Tx_rollup_rejection_rewards'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Tx_rollup_rejection_punishments, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Tx_rollup_rejection_punishments'] } | { kind: CGRIDTag__Proto014PtKathmaOperationMetadataAlphaBalance.Sc_rollup_refutation_punishments, value: CGRIDMap__Proto014PtKathmaOperationMetadataAlphaBalance['Sc_rollup_refutation_punishments'] };
export type Proto014PtKathmaContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto014PtKathmaContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto014PtKathmaContractId {
    Implicit: CGRIDClass__Proto014PtKathmaContractId__Implicit,
    Originated: CGRIDClass__Proto014PtKathmaContractId__Originated
}
export type Proto014PtKathmaContractId = { kind: CGRIDTag__Proto014PtKathmaContractId.Implicit, value: CGRIDMap__Proto014PtKathmaContractId['Implicit'] } | { kind: CGRIDTag__Proto014PtKathmaContractId.Originated, value: CGRIDMap__Proto014PtKathmaContractId['Originated'] };
export enum CGRIDTag__Proto014PtKathmaBondId{
    Tx_rollup_bond_id = 0,
    Sc_rollup_bond_id = 1
}
export interface CGRIDMap__Proto014PtKathmaBondId {
    Tx_rollup_bond_id: CGRIDClass__Proto014PtKathmaBondId__Tx_rollup_bond_id,
    Sc_rollup_bond_id: CGRIDClass__Proto014PtKathmaBondId__Sc_rollup_bond_id
}
export type Proto014PtKathmaBondId = { kind: CGRIDTag__Proto014PtKathmaBondId.Tx_rollup_bond_id, value: CGRIDMap__Proto014PtKathmaBondId['Tx_rollup_bond_id'] } | { kind: CGRIDTag__Proto014PtKathmaBondId.Sc_rollup_bond_id, value: CGRIDMap__Proto014PtKathmaBondId['Sc_rollup_bond_id'] };
export type Proto014PtKathmaReceiptBalanceUpdates = Dynamic<Sequence<CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance>,width.Uint30>;
export class CGRIDClass__Proto014PtKathmaReceiptBalanceUpdates extends Box<Proto014PtKathmaReceiptBalanceUpdates> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaReceiptBalanceUpdates {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto014_ptkathma_receipt_balance_updates_encoder = (value: Proto014PtKathmaReceiptBalanceUpdates): OutputBytes => {
    return value.encode();
}
export const proto014_ptkathma_receipt_balance_updates_decoder = (p: Parser): Proto014PtKathmaReceiptBalanceUpdates => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto014_PtKathmaOperation_metadataAlphaBalance.decode), width.Uint30)(p);
}
