import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export type Proto014PtKathmaOperationRaw = { branch: CGRIDClass__OperationShell_header_branch, data: Bytes };
export class CGRIDClass__Proto014PtKathmaOperationRaw extends Box<Proto014PtKathmaOperationRaw> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'data']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationRaw {
        return new this(record_decoder<Proto014PtKathmaOperationRaw>({branch: CGRIDClass__OperationShell_header_branch.decode, data: Bytes.decode}, {order: ['branch', 'data']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.data.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.data.writeTarget(tgt));
    }
}
export const proto014_ptkathma_operation_raw_encoder = (value: Proto014PtKathmaOperationRaw): OutputBytes => {
    return record_encoder({order: ['branch', 'data']})(value);
}
export const proto014_ptkathma_operation_raw_decoder = (p: Parser): Proto014PtKathmaOperationRaw => {
    return record_decoder<Proto014PtKathmaOperationRaw>({branch: CGRIDClass__OperationShell_header_branch.decode, data: Bytes.decode}, {order: ['branch', 'data']})(p);
}
