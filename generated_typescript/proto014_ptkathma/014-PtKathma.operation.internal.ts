import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaTransactionDestination__Tx_rollup generated for Proto014PtKathmaTransactionDestination__Tx_rollup
export class CGRIDClass__Proto014PtKathmaTransactionDestination__Tx_rollup extends Box<Proto014PtKathmaTransactionDestination__Tx_rollup> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaTransactionDestination__Tx_rollup {
        return new this(Padded.decode(CGRIDClass__Proto014_PtKathmaTx_rollup_id.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaTransactionDestination__Sc_rollup generated for Proto014PtKathmaTransactionDestination__Sc_rollup
export class CGRIDClass__Proto014PtKathmaTransactionDestination__Sc_rollup extends Box<Proto014PtKathmaTransactionDestination__Sc_rollup> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaTransactionDestination__Sc_rollup {
        return new this(Padded.decode(CGRIDClass__Proto014_PtKathmaTransaction_destination_Sc_rollup_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaTransactionDestination__Originated generated for Proto014PtKathmaTransactionDestination__Originated
export class CGRIDClass__Proto014PtKathmaTransactionDestination__Originated extends Box<Proto014PtKathmaTransactionDestination__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaTransactionDestination__Originated {
        return new this(Padded.decode(CGRIDClass__Proto014_PtKathmaTransaction_destination_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaTransactionDestination__Implicit generated for Proto014PtKathmaTransactionDestination__Implicit
export class CGRIDClass__Proto014PtKathmaTransactionDestination__Implicit extends Box<Proto014PtKathmaTransactionDestination__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaTransactionDestination__Implicit {
        return new this(record_decoder<Proto014PtKathmaTransactionDestination__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaEntrypoint__set_delegate generated for Proto014PtKathmaEntrypoint__set_delegate
export class CGRIDClass__Proto014PtKathmaEntrypoint__set_delegate extends Box<Proto014PtKathmaEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaEntrypoint__root generated for Proto014PtKathmaEntrypoint__root
export class CGRIDClass__Proto014PtKathmaEntrypoint__root extends Box<Proto014PtKathmaEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaEntrypoint__remove_delegate generated for Proto014PtKathmaEntrypoint__remove_delegate
export class CGRIDClass__Proto014PtKathmaEntrypoint__remove_delegate extends Box<Proto014PtKathmaEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaEntrypoint__named generated for Proto014PtKathmaEntrypoint__named
export class CGRIDClass__Proto014PtKathmaEntrypoint__named extends Box<Proto014PtKathmaEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaEntrypoint___do generated for Proto014PtKathmaEntrypoint___do
export class CGRIDClass__Proto014PtKathmaEntrypoint___do extends Box<Proto014PtKathmaEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaEntrypoint___default generated for Proto014PtKathmaEntrypoint___default
export class CGRIDClass__Proto014PtKathmaEntrypoint___default extends Box<Proto014PtKathmaEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaContractId__Originated generated for Proto014PtKathmaContractId__Originated
export class CGRIDClass__Proto014PtKathmaContractId__Originated extends Box<Proto014PtKathmaContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto014_PtKathmaContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014PtKathmaContractId__Implicit generated for Proto014PtKathmaContractId__Implicit
export class CGRIDClass__Proto014PtKathmaContractId__Implicit extends Box<Proto014PtKathmaContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaContractId__Implicit {
        return new this(record_decoder<Proto014PtKathmaContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Transaction generated for Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Transaction
export class CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Transaction extends Box<Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Transaction {
        return new this(record_decoder<Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Transaction>({amount: N.decode, destination: CGRIDClass__Proto014_PtKathmaTransaction_destination.decode, parameters: Option.decode(CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Transaction_parameters.decode)}, {order: ['amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Origination generated for Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Origination
export class CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Origination extends Box<Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Origination {
        return new this(record_decoder<Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Origination>({balance: N.decode, delegate: Option.decode(CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Origination_delegate.decode), script: CGRIDClass__Proto014_PtKathmaScriptedContracts.decode}, {order: ['balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Event generated for Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Event
export class CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Event extends Box<Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Event> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_type', 'tag', 'payload']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Event {
        return new this(record_decoder<Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Event>({_type: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, tag: Option.decode(CGRIDClass__Proto014_PtKathmaEntrypoint.decode), payload: Option.decode(CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode)}, {order: ['_type', 'tag', 'payload']})(p));
    };
    get encodeLength(): number {
        return (this.value._type.encodeLength +  this.value.tag.encodeLength +  this.value.payload.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._type.writeTarget(tgt) +  this.value.tag.writeTarget(tgt) +  this.value.payload.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Delegation generated for Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Delegation
export class CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Delegation extends Box<Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Delegation {
        return new this(record_decoder<Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Delegation>({delegate: Option.decode(CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Delegation_delegate.decode)}, {order: ['delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__String generated for MichelineProto014PtKathmaMichelsonV1Expression__String
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__String extends Box<MichelineProto014PtKathmaMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__String {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Sequence generated for MichelineProto014PtKathmaMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Sequence extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Int generated for MichelineProto014PtKathmaMichelsonV1Expression__Int
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Int extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Bytes generated for MichelineProto014PtKathmaMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Bytes extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto014PtKathmaTransactionDestination__Tx_rollup = Padded<CGRIDClass__Proto014_PtKathmaTx_rollup_id,1>;
export type Proto014PtKathmaTransactionDestination__Sc_rollup = Padded<CGRIDClass__Proto014_PtKathmaTransaction_destination_Sc_rollup_denest_pad,1>;
export type Proto014PtKathmaTransactionDestination__Originated = Padded<CGRIDClass__Proto014_PtKathmaTransaction_destination_Originated_denest_pad,1>;
export type Proto014PtKathmaTransactionDestination__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto014PtKathmaEntrypoint__set_delegate = Unit;
export type Proto014PtKathmaEntrypoint__root = Unit;
export type Proto014PtKathmaEntrypoint__remove_delegate = Unit;
export type Proto014PtKathmaEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto014PtKathmaEntrypoint___do = Unit;
export type Proto014PtKathmaEntrypoint___default = Unit;
export type Proto014PtKathmaContractId__Originated = Padded<CGRIDClass__Proto014_PtKathmaContract_id_Originated_denest_pad,1>;
export type Proto014PtKathmaContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Transaction = { amount: N, destination: CGRIDClass__Proto014_PtKathmaTransaction_destination, parameters: Option<CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Transaction_parameters> };
export type Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Origination = { balance: N, delegate: Option<CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Origination_delegate>, script: CGRIDClass__Proto014_PtKathmaScriptedContracts };
export type Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Event = { _type: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, tag: Option<CGRIDClass__Proto014_PtKathmaEntrypoint>, payload: Option<CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression> };
export type Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Delegation = { delegate: Option<CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Delegation_delegate> };
export type MichelineProto014PtKathmaMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineProto014PtKathmaMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression>,width.Uint30>;
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, arg2: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, arg2: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, arg: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, arg: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression };
export type MichelineProto014PtKathmaMichelsonV1Expression__Int = { int: Z };
export type MichelineProto014PtKathmaMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaTx_rollup_id generated for Proto014PtKathmaTxRollupId
export class CGRIDClass__Proto014_PtKathmaTx_rollup_id extends Box<Proto014PtKathmaTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaTx_rollup_id {
        return new this(record_decoder<Proto014PtKathmaTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaTransaction_destination_Sc_rollup_denest_pad generated for Proto014PtKathmaTransactionDestinationScRollupDenestPad
export class CGRIDClass__Proto014_PtKathmaTransaction_destination_Sc_rollup_denest_pad extends Box<Proto014PtKathmaTransactionDestinationScRollupDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['sc_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaTransaction_destination_Sc_rollup_denest_pad {
        return new this(record_decoder<Proto014PtKathmaTransactionDestinationScRollupDenestPad>({sc_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['sc_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.sc_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.sc_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaTransaction_destination_Originated_denest_pad generated for Proto014PtKathmaTransactionDestinationOriginatedDenestPad
export class CGRIDClass__Proto014_PtKathmaTransaction_destination_Originated_denest_pad extends Box<Proto014PtKathmaTransactionDestinationOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaTransaction_destination_Originated_denest_pad {
        return new this(record_decoder<Proto014PtKathmaTransactionDestinationOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaTransaction_destination generated for Proto014PtKathmaTransactionDestination
export function proto014ptkathmatransactiondestination_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaTransactionDestination,Proto014PtKathmaTransactionDestination> {
    function f(disc: CGRIDTag__Proto014PtKathmaTransactionDestination) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaTransactionDestination.Implicit: return CGRIDClass__Proto014PtKathmaTransactionDestination__Implicit.decode;
            case CGRIDTag__Proto014PtKathmaTransactionDestination.Originated: return CGRIDClass__Proto014PtKathmaTransactionDestination__Originated.decode;
            case CGRIDTag__Proto014PtKathmaTransactionDestination.Tx_rollup: return CGRIDClass__Proto014PtKathmaTransactionDestination__Tx_rollup.decode;
            case CGRIDTag__Proto014PtKathmaTransactionDestination.Sc_rollup: return CGRIDClass__Proto014PtKathmaTransactionDestination__Sc_rollup.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaTransactionDestination => Object.values(CGRIDTag__Proto014PtKathmaTransactionDestination).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaTransaction_destination extends Box<Proto014PtKathmaTransactionDestination> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaTransactionDestination>, Proto014PtKathmaTransactionDestination>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaTransaction_destination {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmatransactiondestination_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaScriptedContracts generated for Proto014PtKathmaScriptedContracts
export class CGRIDClass__Proto014_PtKathmaScriptedContracts extends Box<Proto014PtKathmaScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaScriptedContracts {
        return new this(record_decoder<Proto014PtKathmaScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives generated for Proto014PtKathmaMichelsonV1Primitives
export class CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives extends Box<Proto014PtKathmaMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<Proto014PtKathmaMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is Proto014PtKathmaMichelsonV1Primitives => (Object.values(Proto014PtKathmaMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014_PtKathmaEntrypoint generated for Proto014PtKathmaEntrypoint
export function proto014ptkathmaentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaEntrypoint,Proto014PtKathmaEntrypoint> {
    function f(disc: CGRIDTag__Proto014PtKathmaEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaEntrypoint._default: return CGRIDClass__Proto014PtKathmaEntrypoint___default.decode;
            case CGRIDTag__Proto014PtKathmaEntrypoint.root: return CGRIDClass__Proto014PtKathmaEntrypoint__root.decode;
            case CGRIDTag__Proto014PtKathmaEntrypoint._do: return CGRIDClass__Proto014PtKathmaEntrypoint___do.decode;
            case CGRIDTag__Proto014PtKathmaEntrypoint.set_delegate: return CGRIDClass__Proto014PtKathmaEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto014PtKathmaEntrypoint.remove_delegate: return CGRIDClass__Proto014PtKathmaEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto014PtKathmaEntrypoint.named: return CGRIDClass__Proto014PtKathmaEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaEntrypoint => Object.values(CGRIDTag__Proto014PtKathmaEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaEntrypoint extends Box<Proto014PtKathmaEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaEntrypoint>, Proto014PtKathmaEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmaentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaContract_id_Originated_denest_pad generated for Proto014PtKathmaContractIdOriginatedDenestPad
export class CGRIDClass__Proto014_PtKathmaContract_id_Originated_denest_pad extends Box<Proto014PtKathmaContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto014PtKathmaContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaContract_id generated for Proto014PtKathmaContractId
export function proto014ptkathmacontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaContractId,Proto014PtKathmaContractId> {
    function f(disc: CGRIDTag__Proto014PtKathmaContractId) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaContractId.Implicit: return CGRIDClass__Proto014PtKathmaContractId__Implicit.decode;
            case CGRIDTag__Proto014PtKathmaContractId.Originated: return CGRIDClass__Proto014PtKathmaContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaContractId => Object.values(CGRIDTag__Proto014PtKathmaContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaContract_id extends Box<Proto014PtKathmaContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaContractId>, Proto014PtKathmaContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaContract_id {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmacontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_rhs generated for Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs
export function proto014ptkathmaapplyinternalresultsalphaoperationresultrhs_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs,Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs> {
    function f(disc: CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs.Transaction: return CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Transaction.decode;
            case CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs.Origination: return CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Origination.decode;
            case CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs.Delegation: return CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Delegation.decode;
            case CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs.Event: return CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Event.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs => Object.values(CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_rhs extends Box<Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs>, Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_rhs {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmaapplyinternalresultsalphaoperationresultrhs_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Transaction_parameters generated for Proto014PtKathmaApplyInternalResultsAlphaOperationResultTransactionParameters
export class CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Transaction_parameters extends Box<Proto014PtKathmaApplyInternalResultsAlphaOperationResultTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Transaction_parameters {
        return new this(record_decoder<Proto014PtKathmaApplyInternalResultsAlphaOperationResultTransactionParameters>({entrypoint: CGRIDClass__Proto014_PtKathmaEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Origination_delegate generated for Proto014PtKathmaApplyInternalResultsAlphaOperationResultOriginationDelegate
export class CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Origination_delegate extends Box<Proto014PtKathmaApplyInternalResultsAlphaOperationResultOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Origination_delegate {
        return new this(record_decoder<Proto014PtKathmaApplyInternalResultsAlphaOperationResultOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Delegation_delegate generated for Proto014PtKathmaApplyInternalResultsAlphaOperationResultDelegationDelegate
export class CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Delegation_delegate extends Box<Proto014PtKathmaApplyInternalResultsAlphaOperationResultDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_Delegation_delegate {
        return new this(record_decoder<Proto014PtKathmaApplyInternalResultsAlphaOperationResultDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression generated for MichelineProto014PtKathmaMichelsonV1Expression
export function michelineproto014ptkathmamichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression,MichelineProto014PtKathmaMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Int: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.String: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Sequence: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Bytes: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression => Object.values(CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression extends Box<MichelineProto014PtKathmaMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineProto014PtKathmaMichelsonV1Expression>, MichelineProto014PtKathmaMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelineproto014ptkathmamichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression {
    Int: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Bytes
}
export type MichelineProto014PtKathmaMichelsonV1Expression = { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Int, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.String, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Bytes'] };
export type Proto014PtKathmaTxRollupId = { rollup_hash: FixedBytes<20> };
export type Proto014PtKathmaTransactionDestinationScRollupDenestPad = { sc_rollup_hash: FixedBytes<20> };
export type Proto014PtKathmaTransactionDestinationOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto014PtKathmaTransactionDestination{
    Implicit = 0,
    Originated = 1,
    Tx_rollup = 2,
    Sc_rollup = 3
}
export interface CGRIDMap__Proto014PtKathmaTransactionDestination {
    Implicit: CGRIDClass__Proto014PtKathmaTransactionDestination__Implicit,
    Originated: CGRIDClass__Proto014PtKathmaTransactionDestination__Originated,
    Tx_rollup: CGRIDClass__Proto014PtKathmaTransactionDestination__Tx_rollup,
    Sc_rollup: CGRIDClass__Proto014PtKathmaTransactionDestination__Sc_rollup
}
export type Proto014PtKathmaTransactionDestination = { kind: CGRIDTag__Proto014PtKathmaTransactionDestination.Implicit, value: CGRIDMap__Proto014PtKathmaTransactionDestination['Implicit'] } | { kind: CGRIDTag__Proto014PtKathmaTransactionDestination.Originated, value: CGRIDMap__Proto014PtKathmaTransactionDestination['Originated'] } | { kind: CGRIDTag__Proto014PtKathmaTransactionDestination.Tx_rollup, value: CGRIDMap__Proto014PtKathmaTransactionDestination['Tx_rollup'] } | { kind: CGRIDTag__Proto014PtKathmaTransactionDestination.Sc_rollup, value: CGRIDMap__Proto014PtKathmaTransactionDestination['Sc_rollup'] };
export type Proto014PtKathmaScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export enum Proto014PtKathmaMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117,
    LEVEL = 118,
    SELF_ADDRESS = 119,
    never = 120,
    NEVER = 121,
    UNPAIR = 122,
    VOTING_POWER = 123,
    TOTAL_VOTING_POWER = 124,
    KECCAK = 125,
    SHA3 = 126,
    PAIRING_CHECK = 127,
    bls12_381_g1 = 128,
    bls12_381_g2 = 129,
    bls12_381_fr = 130,
    sapling_state = 131,
    sapling_transaction_deprecated = 132,
    SAPLING_EMPTY_STATE = 133,
    SAPLING_VERIFY_UPDATE = 134,
    ticket = 135,
    TICKET = 136,
    READ_TICKET = 137,
    SPLIT_TICKET = 138,
    JOIN_TICKETS = 139,
    GET_AND_UPDATE = 140,
    chest = 141,
    chest_key = 142,
    OPEN_CHEST = 143,
    VIEW = 144,
    view = 145,
    constant = 146,
    SUB_MUTEZ = 147,
    tx_rollup_l2_address = 148,
    MIN_BLOCK_TIME = 149,
    sapling_transaction = 150,
    EMIT = 151
}
export enum CGRIDTag__Proto014PtKathmaEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto014PtKathmaEntrypoint {
    _default: CGRIDClass__Proto014PtKathmaEntrypoint___default,
    root: CGRIDClass__Proto014PtKathmaEntrypoint__root,
    _do: CGRIDClass__Proto014PtKathmaEntrypoint___do,
    set_delegate: CGRIDClass__Proto014PtKathmaEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto014PtKathmaEntrypoint__remove_delegate,
    named: CGRIDClass__Proto014PtKathmaEntrypoint__named
}
export type Proto014PtKathmaEntrypoint = { kind: CGRIDTag__Proto014PtKathmaEntrypoint._default, value: CGRIDMap__Proto014PtKathmaEntrypoint['_default'] } | { kind: CGRIDTag__Proto014PtKathmaEntrypoint.root, value: CGRIDMap__Proto014PtKathmaEntrypoint['root'] } | { kind: CGRIDTag__Proto014PtKathmaEntrypoint._do, value: CGRIDMap__Proto014PtKathmaEntrypoint['_do'] } | { kind: CGRIDTag__Proto014PtKathmaEntrypoint.set_delegate, value: CGRIDMap__Proto014PtKathmaEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto014PtKathmaEntrypoint.remove_delegate, value: CGRIDMap__Proto014PtKathmaEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto014PtKathmaEntrypoint.named, value: CGRIDMap__Proto014PtKathmaEntrypoint['named'] };
export type Proto014PtKathmaContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto014PtKathmaContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto014PtKathmaContractId {
    Implicit: CGRIDClass__Proto014PtKathmaContractId__Implicit,
    Originated: CGRIDClass__Proto014PtKathmaContractId__Originated
}
export type Proto014PtKathmaContractId = { kind: CGRIDTag__Proto014PtKathmaContractId.Implicit, value: CGRIDMap__Proto014PtKathmaContractId['Implicit'] } | { kind: CGRIDTag__Proto014PtKathmaContractId.Originated, value: CGRIDMap__Proto014PtKathmaContractId['Originated'] };
export enum CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs{
    Transaction = 1,
    Origination = 2,
    Delegation = 3,
    Event = 4
}
export interface CGRIDMap__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs {
    Transaction: CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Transaction,
    Origination: CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Origination,
    Delegation: CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Delegation,
    Event: CGRIDClass__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs__Event
}
export type Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs = { kind: CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs.Transaction, value: CGRIDMap__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs['Transaction'] } | { kind: CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs.Origination, value: CGRIDMap__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs['Origination'] } | { kind: CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs.Delegation, value: CGRIDMap__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs['Delegation'] } | { kind: CGRIDTag__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs.Event, value: CGRIDMap__Proto014PtKathmaApplyInternalResultsAlphaOperationResultRhs['Event'] };
export type Proto014PtKathmaApplyInternalResultsAlphaOperationResultTransactionParameters = { entrypoint: CGRIDClass__Proto014_PtKathmaEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto014PtKathmaApplyInternalResultsAlphaOperationResultOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto014PtKathmaApplyInternalResultsAlphaOperationResultDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto014PtKathmaOperationInternal = { source: CGRIDClass__Proto014_PtKathmaContract_id, nonce: Uint16, proto014_ptkathma_apply_internal_results_alpha_operation_result_rhs: CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_rhs };
export class CGRIDClass__Proto014PtKathmaOperationInternal extends Box<Proto014PtKathmaOperationInternal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'nonce', 'proto014_ptkathma_apply_internal_results_alpha_operation_result_rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaOperationInternal {
        return new this(record_decoder<Proto014PtKathmaOperationInternal>({source: CGRIDClass__Proto014_PtKathmaContract_id.decode, nonce: Uint16.decode, proto014_ptkathma_apply_internal_results_alpha_operation_result_rhs: CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_rhs.decode}, {order: ['source', 'nonce', 'proto014_ptkathma_apply_internal_results_alpha_operation_result_rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.nonce.encodeLength +  this.value.proto014_ptkathma_apply_internal_results_alpha_operation_result_rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt) +  this.value.proto014_ptkathma_apply_internal_results_alpha_operation_result_rhs.writeTarget(tgt));
    }
}
export const proto014_ptkathma_operation_internal_encoder = (value: Proto014PtKathmaOperationInternal): OutputBytes => {
    return record_encoder({order: ['source', 'nonce', 'proto014_ptkathma_apply_internal_results_alpha_operation_result_rhs']})(value);
}
export const proto014_ptkathma_operation_internal_decoder = (p: Parser): Proto014PtKathmaOperationInternal => {
    return record_decoder<Proto014PtKathmaOperationInternal>({source: CGRIDClass__Proto014_PtKathmaContract_id.decode, nonce: Uint16.decode, proto014_ptkathma_apply_internal_results_alpha_operation_result_rhs: CGRIDClass__Proto014_PtKathmaApply_internal_resultsAlphaOperation_result_rhs.decode}, {order: ['source', 'nonce', 'proto014_ptkathma_apply_internal_results_alpha_operation_result_rhs']})(p);
}
