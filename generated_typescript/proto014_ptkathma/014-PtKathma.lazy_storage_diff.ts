import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__sapling_state generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__sapling_state
export class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__sapling_state extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__sapling_state> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['id', 'diff']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__sapling_state {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__sapling_state>({id: Z.decode, diff: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff.decode}, {order: ['id', 'diff']})(p));
    };
    get encodeLength(): number {
        return (this.value.id.encodeLength +  this.value.diff.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.id.writeTarget(tgt) +  this.value.diff.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__big_map generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__big_map
export class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__big_map extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__big_map> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['id', 'diff']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__big_map {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__big_map>({id: Z.decode, diff: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff.decode}, {order: ['id', 'diff']})(p));
    };
    get encodeLength(): number {
        return (this.value.id.encodeLength +  this.value.diff.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.id.writeTarget(tgt) +  this.value.diff.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update
export class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update>({action: Unit.decode, updates: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates.decode}, {order: ['action', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove
export class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove>({action: Unit.decode}, {order: ['action']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy
export class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'source', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy>({action: Unit.decode, source: Z.decode, updates: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates.decode}, {order: ['action', 'source', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.source.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.source.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc
export class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates', 'memo_size']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc>({action: Unit.decode, updates: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates.decode, memo_size: Uint16.decode}, {order: ['action', 'updates', 'memo_size']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength +  this.value.memo_size.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt) +  this.value.memo_size.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__update generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__update
export class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__update extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__update {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__update>({action: Unit.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['action', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove
export class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove>({action: Unit.decode}, {order: ['action']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy
export class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'source', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy>({action: Unit.decode, source: Z.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['action', 'source', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.source.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.source.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc
export class CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates', 'key_type', 'value_type']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc>({action: Unit.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq.decode), width.Uint30), key_type: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, value_type: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode}, {order: ['action', 'updates', 'key_type', 'value_type']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength +  this.value.key_type.encodeLength +  this.value.value_type.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt) +  this.value.key_type.writeTarget(tgt) +  this.value.value_type.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__String generated for MichelineProto014PtKathmaMichelsonV1Expression__String
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__String extends Box<MichelineProto014PtKathmaMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__String {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Sequence generated for MichelineProto014PtKathmaMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Sequence extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Int generated for MichelineProto014PtKathmaMichelsonV1Expression__Int
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Int extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Bytes generated for MichelineProto014PtKathmaMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Bytes extends Box<MichelineProto014PtKathmaMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineProto014PtKathmaMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__sapling_state = { id: Z, diff: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__big_map = { id: Z, diff: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update = { action: Unit, updates: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove = { action: Unit };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy = { action: Unit, source: Z, updates: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc = { action: Unit, updates: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates, memo_size: Uint16 };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__update = { action: Unit, updates: Dynamic<Sequence<CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq>,width.Uint30> };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove = { action: Unit };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy = { action: Unit, source: Z, updates: Dynamic<Sequence<CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq>,width.Uint30> };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc = { action: Unit, updates: Dynamic<Sequence<CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq>,width.Uint30>, key_type: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, value_type: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression };
export type MichelineProto014PtKathmaMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineProto014PtKathmaMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression>,width.Uint30>;
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, arg2: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, arg2: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, arg: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives, arg: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression };
export type MichelineProto014PtKathmaMichelsonV1Expression__Int = { int: Z };
export type MichelineProto014PtKathmaMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
// Class CGRIDClass__SaplingTransactionCiphertext generated for SaplingTransactionCiphertext
export class CGRIDClass__SaplingTransactionCiphertext extends Box<SaplingTransactionCiphertext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionCiphertext {
        return new this(record_decoder<SaplingTransactionCiphertext>({cv: FixedBytes.decode<32>({len: 32}), epk: FixedBytes.decode<32>({len: 32}), payload_enc: Dynamic.decode(Bytes.decode, width.Uint30), nonce_enc: FixedBytes.decode<24>({len: 24}), payload_out: FixedBytes.decode<80>({len: 80}), nonce_out: FixedBytes.decode<24>({len: 24})}, {order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(p));
    };
    get encodeLength(): number {
        return (this.value.cv.encodeLength +  this.value.epk.encodeLength +  this.value.payload_enc.encodeLength +  this.value.nonce_enc.encodeLength +  this.value.payload_out.encodeLength +  this.value.nonce_out.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cv.writeTarget(tgt) +  this.value.epk.writeTarget(tgt) +  this.value.payload_enc.writeTarget(tgt) +  this.value.nonce_enc.writeTarget(tgt) +  this.value.payload_out.writeTarget(tgt) +  this.value.nonce_out.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives generated for Proto014PtKathmaMichelsonV1Primitives
export class CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives extends Box<Proto014PtKathmaMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<Proto014PtKathmaMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is Proto014PtKathmaMichelsonV1Primitives => (Object.values(Proto014PtKathmaMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff
export function proto014ptkathmalazystoragediffdenestdyndenestseqsaplingstatediff_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff,Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff> {
    function f(disc: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.update: return CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update.decode;
            case CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.remove: return CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove.decode;
            case CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.copy: return CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy.decode;
            case CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.alloc: return CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff => Object.values(CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff>, Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmalazystoragediffdenestdyndenestseqsaplingstatediff_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff
export function proto014ptkathmalazystoragediffdenestdyndenestseqbigmapdiff_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff,Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff> {
    function f(disc: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff.update: return CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__update.decode;
            case CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff.remove: return CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove.decode;
            case CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff.copy: return CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy.decode;
            case CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff.alloc: return CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff => Object.values(CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff>, Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmalazystoragediffdenestdyndenestseqbigmapdiff_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq generated for Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq
export function proto014ptkathmalazystoragediffdenestdyndenestseq_mkDecoder(): VariantDecoder<CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq,Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq> {
    function f(disc: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq) {
        switch (disc) {
            case CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq.big_map: return CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__big_map.decode;
            case CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq.sapling_state: return CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__sapling_state.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq => Object.values(CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq).includes(tagval);
    return f;
}
export class CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq extends Box<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>, Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq {
        return new this(variant_decoder(width.Uint8)(proto014ptkathmalazystoragediffdenestdyndenestseq_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression generated for MichelineProto014PtKathmaMichelsonV1Expression
export function michelineproto014ptkathmamichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression,MichelineProto014PtKathmaMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Int: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.String: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Sequence: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Bytes: return CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression => Object.values(CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression extends Box<MichelineProto014PtKathmaMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineProto014PtKathmaMichelsonV1Expression>, MichelineProto014PtKathmaMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelineproto014ptkathmamichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export type SaplingTransactionCiphertext = { cv: FixedBytes<32>, epk: FixedBytes<32>, payload_enc: Dynamic<Bytes,width.Uint30>, nonce_enc: FixedBytes<24>, payload_out: FixedBytes<80>, nonce_out: FixedBytes<24> };
export enum CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression {
    Int: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineProto014PtKathmaMichelsonV1Expression__Bytes
}
export type MichelineProto014PtKathmaMichelsonV1Expression = { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Int, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.String, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineProto014PtKathmaMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineProto014PtKathmaMichelsonV1Expression['Bytes'] };
export enum Proto014PtKathmaMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117,
    LEVEL = 118,
    SELF_ADDRESS = 119,
    never = 120,
    NEVER = 121,
    UNPAIR = 122,
    VOTING_POWER = 123,
    TOTAL_VOTING_POWER = 124,
    KECCAK = 125,
    SHA3 = 126,
    PAIRING_CHECK = 127,
    bls12_381_g1 = 128,
    bls12_381_g2 = 129,
    bls12_381_fr = 130,
    sapling_state = 131,
    sapling_transaction_deprecated = 132,
    SAPLING_EMPTY_STATE = 133,
    SAPLING_VERIFY_UPDATE = 134,
    ticket = 135,
    TICKET = 136,
    READ_TICKET = 137,
    SPLIT_TICKET = 138,
    JOIN_TICKETS = 139,
    GET_AND_UPDATE = 140,
    chest = 141,
    chest_key = 142,
    OPEN_CHEST = 143,
    VIEW = 144,
    view = 145,
    constant = 146,
    SUB_MUTEZ = 147,
    tx_rollup_l2_address = 148,
    MIN_BLOCK_TIME = 149,
    sapling_transaction = 150,
    EMIT = 151
}
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export enum CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff{
    update = 0,
    remove = 1,
    copy = 2,
    alloc = 3
}
export interface CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff {
    update: CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update,
    remove: CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove,
    copy: CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy,
    alloc: CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc
}
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff = { kind: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.update, value: CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['update'] } | { kind: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.remove, value: CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['remove'] } | { kind: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.copy, value: CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['copy'] } | { kind: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.alloc, value: CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['alloc'] };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression> };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression> };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto014_PtKathmaMichelson_v1Expression> };
export enum CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff{
    update = 0,
    remove = 1,
    copy = 2,
    alloc = 3
}
export interface CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff {
    update: CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__update,
    remove: CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove,
    copy: CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy,
    alloc: CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc
}
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff = { kind: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff.update, value: CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff['update'] } | { kind: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff.remove, value: CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff['remove'] } | { kind: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff.copy, value: CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff['copy'] } | { kind: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff.alloc, value: CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeqBigMapDiff['alloc'] };
export enum CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq{
    big_map = 0,
    sapling_state = 1
}
export interface CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq {
    big_map: CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__big_map,
    sapling_state: CGRIDClass__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq__sapling_state
}
export type Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq = { kind: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq.big_map, value: CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq['big_map'] } | { kind: CGRIDTag__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq.sapling_state, value: CGRIDMap__Proto014PtKathmaLazyStorageDiffDenestDynDenestSeq['sapling_state'] };
export type Proto014PtKathmaLazyStorageDiff = Dynamic<Sequence<CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq>,width.Uint30>;
export class CGRIDClass__Proto014PtKathmaLazyStorageDiff extends Box<Proto014PtKathmaLazyStorageDiff> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto014PtKathmaLazyStorageDiff {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto014_ptkathma_lazy_storage_diff_encoder = (value: Proto014PtKathmaLazyStorageDiff): OutputBytes => {
    return value.encode();
}
export const proto014_ptkathma_lazy_storage_diff_decoder = (p: Parser): Proto014PtKathmaLazyStorageDiff => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto014_PtKathmaLazy_storage_diff_denest_dyn_denest_seq.decode), width.Uint30)(p);
}
