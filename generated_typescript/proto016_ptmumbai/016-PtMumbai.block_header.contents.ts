import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash generated for Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash extends Box<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
export type Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type Proto016PtMumbaiBlockHeaderContents = { payload_hash: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_toggle_vote: Int8 };
export class CGRIDClass__Proto016PtMumbaiBlockHeaderContents extends Box<Proto016PtMumbaiBlockHeaderContents> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiBlockHeaderContents {
        return new this(record_decoder<Proto016PtMumbaiBlockHeaderContents>({payload_hash: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote']})(p));
    };
    get encodeLength(): number {
        return (this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_toggle_vote.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_toggle_vote.writeTarget(tgt));
    }
}
export const proto016_ptmumbai_block_header_contents_encoder = (value: Proto016PtMumbaiBlockHeaderContents): OutputBytes => {
    return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote']})(value);
}
export const proto016_ptmumbai_block_header_contents_decoder = (p: Parser): Proto016PtMumbaiBlockHeaderContents => {
    return record_decoder<Proto016PtMumbaiBlockHeaderContents>({payload_hash: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote']})(p);
}
