import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int31 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto016PtMumbaiScriptLoc = Int31;
export class CGRIDClass__Proto016PtMumbaiScriptLoc extends Box<Proto016PtMumbaiScriptLoc> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiScriptLoc {
        return new this(Int31.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto016_ptmumbai_script_loc_encoder = (value: Proto016PtMumbaiScriptLoc): OutputBytes => {
    return value.encode();
}
export const proto016_ptmumbai_script_loc_decoder = (p: Parser): Proto016PtMumbaiScriptLoc => {
    return Int31.decode(p);
}
