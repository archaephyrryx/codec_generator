import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Bls generated for PublicKeyHash__Bls
export class CGRIDClass__PublicKeyHash__Bls extends Box<PublicKeyHash__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Bls {
        return new this(record_decoder<PublicKeyHash__Bls>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Subsidy generated for Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Subsidy
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Subsidy extends Box<Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Subsidy> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Subsidy {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Simulation generated for Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Simulation
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Simulation extends Box<Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Simulation> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Simulation {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration generated for Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration extends Box<Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Block_application generated for Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Block_application
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Block_application extends Box<Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Block_application> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Block_application {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Storage_fees generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Storage_fees
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Storage_fees extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Storage_fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Storage_fees {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Storage_fees>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Nonce_revelation_rewards generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Nonce_revelation_rewards
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Nonce_revelation_rewards extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Nonce_revelation_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Nonce_revelation_rewards {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Nonce_revelation_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Minted generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Minted
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Minted extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Minted> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Minted {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Minted>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Lost_endorsing_rewards generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Lost_endorsing_rewards
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Lost_endorsing_rewards extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Lost_endorsing_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'participation', 'revelation', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Lost_endorsing_rewards {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Lost_endorsing_rewards>({category: Unit.decode, delegate: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate.decode, participation: Bool.decode, revelation: Bool.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'participation', 'revelation', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.participation.encodeLength +  this.value.revelation.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.participation.writeTarget(tgt) +  this.value.revelation.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Liquidity_baking_subsidies generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Liquidity_baking_subsidies
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Liquidity_baking_subsidies extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Liquidity_baking_subsidies> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Liquidity_baking_subsidies {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Liquidity_baking_subsidies>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Invoice generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Invoice
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Invoice extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Invoice> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Invoice {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Invoice>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Initial_commitments generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Initial_commitments
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Initial_commitments extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Initial_commitments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Initial_commitments {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Initial_commitments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Frozen_bonds generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Frozen_bonds
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Frozen_bonds extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Frozen_bonds> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'contract', 'bond_id', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Frozen_bonds {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Frozen_bonds>({category: Unit.decode, contract: CGRIDClass__Proto016_PtMumbaiContract_id.decode, bond_id: CGRIDClass__Proto016_PtMumbaiBond_id.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'contract', 'bond_id', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.contract.encodeLength +  this.value.bond_id.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.contract.writeTarget(tgt) +  this.value.bond_id.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Endorsing_rewards generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Endorsing_rewards
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Endorsing_rewards extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Endorsing_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Endorsing_rewards {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Endorsing_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_punishments generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_punishments
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_punishments extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_punishments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_punishments {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_punishments>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_evidence_rewards generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_evidence_rewards
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_evidence_rewards extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_evidence_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_evidence_rewards {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_evidence_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Deposits generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Deposits
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Deposits extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Deposits> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Deposits {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Deposits>({category: Unit.decode, delegate: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Deposits_delegate.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'delegate', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Contract generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Contract
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Contract extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Contract> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Contract {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Contract>({contract: CGRIDClass__Proto016_PtMumbaiContract_id.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['contract', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Commitments generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Commitments
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Commitments extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Commitments> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'committer', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Commitments {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Commitments>({category: Unit.decode, committer: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Commitments_committer.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'committer', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.committer.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.committer.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Burned generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Burned
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Burned extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Burned> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Burned {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Burned>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Bootstrap generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Bootstrap
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Bootstrap extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Bootstrap> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Bootstrap {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Bootstrap>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Block_fees generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Block_fees
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Block_fees extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Block_fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Block_fees {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Block_fees>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_rewards generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_rewards
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_rewards extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_rewards {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_rewards>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_bonuses generated for Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_bonuses
export class CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_bonuses extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_bonuses> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'change', 'origin']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_bonuses {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_bonuses>({category: Unit.decode, change: Int64.decode, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin.decode}, {order: ['category', 'change', 'origin']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.change.encodeLength +  this.value.origin.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.change.writeTarget(tgt) +  this.value.origin.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiContractId__Originated generated for Proto016PtMumbaiContractId__Originated
export class CGRIDClass__Proto016PtMumbaiContractId__Originated extends Box<Proto016PtMumbaiContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiContractId__Implicit generated for Proto016PtMumbaiContractId__Implicit
export class CGRIDClass__Proto016PtMumbaiContractId__Implicit extends Box<Proto016PtMumbaiContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiContractId__Implicit {
        return new this(record_decoder<Proto016PtMumbaiContractId__Implicit>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiBondId__Tx_rollup_bond_id generated for Proto016PtMumbaiBondId__Tx_rollup_bond_id
export class CGRIDClass__Proto016PtMumbaiBondId__Tx_rollup_bond_id extends Box<Proto016PtMumbaiBondId__Tx_rollup_bond_id> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['tx_rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiBondId__Tx_rollup_bond_id {
        return new this(record_decoder<Proto016PtMumbaiBondId__Tx_rollup_bond_id>({tx_rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id.decode}, {order: ['tx_rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.tx_rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.tx_rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiBondId__Smart_rollup_bond_id generated for Proto016PtMumbaiBondId__Smart_rollup_bond_id
export class CGRIDClass__Proto016PtMumbaiBondId__Smart_rollup_bond_id extends Box<Proto016PtMumbaiBondId__Smart_rollup_bond_id> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiBondId__Smart_rollup_bond_id {
        return new this(record_decoder<Proto016PtMumbaiBondId__Smart_rollup_bond_id>({smart_rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address.decode}, {order: ['smart_rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Bls = { bls12_381_public_key_hash: FixedBytes<20> };
export type Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Subsidy = Unit;
export type Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Simulation = Unit;
export type Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration = Unit;
export type Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Block_application = Unit;
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Storage_fees = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Nonce_revelation_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Minted = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Lost_endorsing_rewards = { category: Unit, delegate: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate, participation: Bool, revelation: Bool, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Liquidity_baking_subsidies = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Invoice = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Initial_commitments = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Frozen_bonds = { category: Unit, contract: CGRIDClass__Proto016_PtMumbaiContract_id, bond_id: CGRIDClass__Proto016_PtMumbaiBond_id, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Endorsing_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_punishments = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_evidence_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Deposits = { category: Unit, delegate: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Deposits_delegate, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Contract = { contract: CGRIDClass__Proto016_PtMumbaiContract_id, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Commitments = { category: Unit, committer: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Commitments_committer, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Burned = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Bootstrap = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Block_fees = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_rewards = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_bonuses = { category: Unit, change: Int64, origin: CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin };
export type Proto016PtMumbaiContractId__Originated = Padded<CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad,1>;
export type Proto016PtMumbaiContractId__Implicit = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiBondId__Tx_rollup_bond_id = { tx_rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id };
export type Proto016PtMumbaiBondId__Smart_rollup_bond_id = { smart_rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
            case CGRIDTag__PublicKeyHash.Bls: return CGRIDClass__PublicKeyHash__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiTx_rollup_id generated for Proto016PtMumbaiTxRollupId
export class CGRIDClass__Proto016_PtMumbaiTx_rollup_id extends Box<Proto016PtMumbaiTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiTx_rollup_id {
        return new this(record_decoder<Proto016PtMumbaiTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiSmart_rollup_address generated for Proto016PtMumbaiSmartRollupAddress
export class CGRIDClass__Proto016_PtMumbaiSmart_rollup_address extends Box<Proto016PtMumbaiSmartRollupAddress> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiSmart_rollup_address {
        return new this(record_decoder<Proto016PtMumbaiSmartRollupAddress>({smart_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['smart_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin generated for Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin
export function proto016ptmumbaioperationmetadataalphaupdateoriginorigin_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin,Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin.Block_application: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Block_application.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin.Subsidy: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Subsidy.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin.Simulation: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Simulation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin => Object.values(CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin extends Box<Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin>, Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaUpdate_origin_origin {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationmetadataalphaupdateoriginorigin_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate generated for Proto016PtMumbaiOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate
export class CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate extends Box<Proto016PtMumbaiOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Lost_endorsing_rewards_delegate {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Deposits_delegate generated for Proto016PtMumbaiOperationMetadataAlphaBalanceDepositsDelegate
export class CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Deposits_delegate extends Box<Proto016PtMumbaiOperationMetadataAlphaBalanceDepositsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Deposits_delegate {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalanceDepositsDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Commitments_committer generated for Proto016PtMumbaiOperationMetadataAlphaBalanceCommitmentsCommitter
export class CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Commitments_committer extends Box<Proto016PtMumbaiOperationMetadataAlphaBalanceCommitmentsCommitter> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['blinded_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance_Commitments_committer {
        return new this(record_decoder<Proto016PtMumbaiOperationMetadataAlphaBalanceCommitmentsCommitter>({blinded_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['blinded_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.blinded_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.blinded_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance generated for Proto016PtMumbaiOperationMetadataAlphaBalance
export function proto016ptmumbaioperationmetadataalphabalance_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance,Proto016PtMumbaiOperationMetadataAlphaBalance> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Contract: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Contract.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Block_fees: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Block_fees.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Deposits: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Deposits.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Nonce_revelation_rewards: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Nonce_revelation_rewards.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Double_signing_evidence_rewards: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_evidence_rewards.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Endorsing_rewards: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Endorsing_rewards.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Baking_rewards: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_rewards.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Baking_bonuses: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_bonuses.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Storage_fees: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Storage_fees.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Double_signing_punishments: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_punishments.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Lost_endorsing_rewards: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Lost_endorsing_rewards.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Liquidity_baking_subsidies: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Liquidity_baking_subsidies.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Burned: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Burned.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Commitments: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Commitments.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Bootstrap: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Bootstrap.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Invoice: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Invoice.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Initial_commitments: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Initial_commitments.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Minted: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Minted.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Frozen_bonds: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Frozen_bonds.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Tx_rollup_rejection_rewards: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Tx_rollup_rejection_punishments: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Smart_rollup_refutation_punishments: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments.decode;
            case CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Smart_rollup_refutation_rewards: return CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance => Object.values(CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance extends Box<Proto016PtMumbaiOperationMetadataAlphaBalance> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationMetadataAlphaBalance>, Proto016PtMumbaiOperationMetadataAlphaBalance>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationmetadataalphabalance_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad generated for Proto016PtMumbaiContractIdOriginatedDenestPad
export class CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad extends Box<Proto016PtMumbaiContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto016PtMumbaiContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiContract_id generated for Proto016PtMumbaiContractId
export function proto016ptmumbaicontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiContractId,Proto016PtMumbaiContractId> {
    function f(disc: CGRIDTag__Proto016PtMumbaiContractId) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiContractId.Implicit: return CGRIDClass__Proto016PtMumbaiContractId__Implicit.decode;
            case CGRIDTag__Proto016PtMumbaiContractId.Originated: return CGRIDClass__Proto016PtMumbaiContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiContractId => Object.values(CGRIDTag__Proto016PtMumbaiContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiContract_id extends Box<Proto016PtMumbaiContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiContractId>, Proto016PtMumbaiContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiContract_id {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaicontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiBond_id generated for Proto016PtMumbaiBondId
export function proto016ptmumbaibondid_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiBondId,Proto016PtMumbaiBondId> {
    function f(disc: CGRIDTag__Proto016PtMumbaiBondId) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiBondId.Tx_rollup_bond_id: return CGRIDClass__Proto016PtMumbaiBondId__Tx_rollup_bond_id.decode;
            case CGRIDTag__Proto016PtMumbaiBondId.Smart_rollup_bond_id: return CGRIDClass__Proto016PtMumbaiBondId__Smart_rollup_bond_id.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiBondId => Object.values(CGRIDTag__Proto016PtMumbaiBondId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiBond_id extends Box<Proto016PtMumbaiBondId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiBondId>, Proto016PtMumbaiBondId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiBond_id {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaibondid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256,
    Bls: CGRIDClass__PublicKeyHash__Bls
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] } | { kind: CGRIDTag__PublicKeyHash.Bls, value: CGRIDMap__PublicKeyHash['Bls'] };
export type Proto016PtMumbaiTxRollupId = { rollup_hash: FixedBytes<20> };
export type Proto016PtMumbaiSmartRollupAddress = { smart_rollup_hash: FixedBytes<20> };
export enum CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin{
    Block_application = 0,
    Protocol_migration = 1,
    Subsidy = 2,
    Simulation = 3
}
export interface CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin {
    Block_application: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Block_application,
    Protocol_migration: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Protocol_migration,
    Subsidy: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Subsidy,
    Simulation: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin__Simulation
}
export type Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin = { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin.Block_application, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin['Block_application'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin.Protocol_migration, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin['Protocol_migration'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin.Subsidy, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin['Subsidy'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin.Simulation, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaUpdateOriginOrigin['Simulation'] };
export type Proto016PtMumbaiOperationMetadataAlphaBalanceLostEndorsingRewardsDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationMetadataAlphaBalanceDepositsDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationMetadataAlphaBalanceCommitmentsCommitter = { blinded_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance{
    Contract = 0,
    Block_fees = 2,
    Deposits = 4,
    Nonce_revelation_rewards = 5,
    Double_signing_evidence_rewards = 6,
    Endorsing_rewards = 7,
    Baking_rewards = 8,
    Baking_bonuses = 9,
    Storage_fees = 11,
    Double_signing_punishments = 12,
    Lost_endorsing_rewards = 13,
    Liquidity_baking_subsidies = 14,
    Burned = 15,
    Commitments = 16,
    Bootstrap = 17,
    Invoice = 18,
    Initial_commitments = 19,
    Minted = 20,
    Frozen_bonds = 21,
    Tx_rollup_rejection_rewards = 22,
    Tx_rollup_rejection_punishments = 23,
    Smart_rollup_refutation_punishments = 24,
    Smart_rollup_refutation_rewards = 25
}
export interface CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance {
    Contract: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Contract,
    Block_fees: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Block_fees,
    Deposits: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Deposits,
    Nonce_revelation_rewards: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Nonce_revelation_rewards,
    Double_signing_evidence_rewards: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_evidence_rewards,
    Endorsing_rewards: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Endorsing_rewards,
    Baking_rewards: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_rewards,
    Baking_bonuses: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Baking_bonuses,
    Storage_fees: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Storage_fees,
    Double_signing_punishments: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Double_signing_punishments,
    Lost_endorsing_rewards: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Lost_endorsing_rewards,
    Liquidity_baking_subsidies: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Liquidity_baking_subsidies,
    Burned: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Burned,
    Commitments: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Commitments,
    Bootstrap: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Bootstrap,
    Invoice: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Invoice,
    Initial_commitments: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Initial_commitments,
    Minted: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Minted,
    Frozen_bonds: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Frozen_bonds,
    Tx_rollup_rejection_rewards: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_rewards,
    Tx_rollup_rejection_punishments: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Tx_rollup_rejection_punishments,
    Smart_rollup_refutation_punishments: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_punishments,
    Smart_rollup_refutation_rewards: CGRIDClass__Proto016PtMumbaiOperationMetadataAlphaBalance__Smart_rollup_refutation_rewards
}
export type Proto016PtMumbaiOperationMetadataAlphaBalance = { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Contract, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Contract'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Block_fees, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Block_fees'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Deposits, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Deposits'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Nonce_revelation_rewards, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Nonce_revelation_rewards'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Double_signing_evidence_rewards, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Double_signing_evidence_rewards'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Endorsing_rewards, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Endorsing_rewards'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Baking_rewards, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Baking_rewards'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Baking_bonuses, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Baking_bonuses'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Storage_fees, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Storage_fees'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Double_signing_punishments, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Double_signing_punishments'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Lost_endorsing_rewards, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Lost_endorsing_rewards'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Liquidity_baking_subsidies, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Liquidity_baking_subsidies'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Burned, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Burned'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Commitments, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Commitments'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Bootstrap, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Bootstrap'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Invoice, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Invoice'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Initial_commitments, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Initial_commitments'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Minted, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Minted'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Frozen_bonds, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Frozen_bonds'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Tx_rollup_rejection_rewards, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Tx_rollup_rejection_rewards'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Tx_rollup_rejection_punishments, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Tx_rollup_rejection_punishments'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Smart_rollup_refutation_punishments, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Smart_rollup_refutation_punishments'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationMetadataAlphaBalance.Smart_rollup_refutation_rewards, value: CGRIDMap__Proto016PtMumbaiOperationMetadataAlphaBalance['Smart_rollup_refutation_rewards'] };
export type Proto016PtMumbaiContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto016PtMumbaiContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto016PtMumbaiContractId {
    Implicit: CGRIDClass__Proto016PtMumbaiContractId__Implicit,
    Originated: CGRIDClass__Proto016PtMumbaiContractId__Originated
}
export type Proto016PtMumbaiContractId = { kind: CGRIDTag__Proto016PtMumbaiContractId.Implicit, value: CGRIDMap__Proto016PtMumbaiContractId['Implicit'] } | { kind: CGRIDTag__Proto016PtMumbaiContractId.Originated, value: CGRIDMap__Proto016PtMumbaiContractId['Originated'] };
export enum CGRIDTag__Proto016PtMumbaiBondId{
    Tx_rollup_bond_id = 0,
    Smart_rollup_bond_id = 1
}
export interface CGRIDMap__Proto016PtMumbaiBondId {
    Tx_rollup_bond_id: CGRIDClass__Proto016PtMumbaiBondId__Tx_rollup_bond_id,
    Smart_rollup_bond_id: CGRIDClass__Proto016PtMumbaiBondId__Smart_rollup_bond_id
}
export type Proto016PtMumbaiBondId = { kind: CGRIDTag__Proto016PtMumbaiBondId.Tx_rollup_bond_id, value: CGRIDMap__Proto016PtMumbaiBondId['Tx_rollup_bond_id'] } | { kind: CGRIDTag__Proto016PtMumbaiBondId.Smart_rollup_bond_id, value: CGRIDMap__Proto016PtMumbaiBondId['Smart_rollup_bond_id'] };
export type Proto016PtMumbaiReceiptBalanceUpdates = Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance>,width.Uint30>;
export class CGRIDClass__Proto016PtMumbaiReceiptBalanceUpdates extends Box<Proto016PtMumbaiReceiptBalanceUpdates> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiReceiptBalanceUpdates {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto016_ptmumbai_receipt_balance_updates_encoder = (value: Proto016PtMumbaiReceiptBalanceUpdates): OutputBytes => {
    return value.encode();
}
export const proto016_ptmumbai_receipt_balance_updates_decoder = (p: Parser): Proto016PtMumbaiReceiptBalanceUpdates => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperation_metadataAlphaBalance.decode), width.Uint30)(p);
}
