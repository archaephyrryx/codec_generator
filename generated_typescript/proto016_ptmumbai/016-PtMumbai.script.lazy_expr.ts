import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { Target } from '../../ts_runtime/target';
export type Proto016PtMumbaiScriptLazyExpr = Dynamic<Bytes,width.Uint30>;
export class CGRIDClass__Proto016PtMumbaiScriptLazyExpr extends Box<Proto016PtMumbaiScriptLazyExpr> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiScriptLazyExpr {
        return new this(Dynamic.decode(Bytes.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto016_ptmumbai_script_lazy_expr_encoder = (value: Proto016PtMumbaiScriptLazyExpr): OutputBytes => {
    return value.encode();
}
export const proto016_ptmumbai_script_lazy_expr_decoder = (p: Parser): Proto016PtMumbaiScriptLazyExpr => {
    return Dynamic.decode(Bytes.decode, width.Uint30)(p);
}
