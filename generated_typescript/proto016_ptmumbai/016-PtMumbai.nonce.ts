import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
export type Proto016PtMumbaiNonce = FixedBytes<32>;
export class CGRIDClass__Proto016PtMumbaiNonce extends Box<Proto016PtMumbaiNonce> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiNonce {
        return new this(FixedBytes.decode<32>({len: 32})(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto016_ptmumbai_nonce_encoder = (value: Proto016PtMumbaiNonce): OutputBytes => {
    return value.encode();
}
export const proto016_ptmumbai_nonce_decoder = (p: Parser): Proto016PtMumbaiNonce => {
    return FixedBytes.decode<32>({len: 32})(p);
}
