import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash generated for Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash extends Box<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaSigned_contents_signature generated for Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaSigned_contents_signature extends Box<Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
export type Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature = { signature_v1: Bytes };
export type Proto016PtMumbaiBlockHeaderProtocolData = { payload_hash: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_toggle_vote: Int8, signature: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaSigned_contents_signature };
export class CGRIDClass__Proto016PtMumbaiBlockHeaderProtocolData extends Box<Proto016PtMumbaiBlockHeaderProtocolData> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiBlockHeaderProtocolData {
        return new this(record_decoder<Proto016PtMumbaiBlockHeaderProtocolData>({payload_hash: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaSigned_contents_signature.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_toggle_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_toggle_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export const proto016_ptmumbai_block_header_protocol_data_encoder = (value: Proto016PtMumbaiBlockHeaderProtocolData): OutputBytes => {
    return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(value);
}
export const proto016_ptmumbai_block_header_protocol_data_decoder = (p: Parser): Proto016PtMumbaiBlockHeaderProtocolData => {
    return record_decoder<Proto016PtMumbaiBlockHeaderProtocolData>({payload_hash: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaSigned_contents_signature.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p);
}
