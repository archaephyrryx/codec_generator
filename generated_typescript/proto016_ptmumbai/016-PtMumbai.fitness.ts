import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto016PtMumbaiFitnessLockedRound__Some generated for Proto016PtMumbaiFitnessLockedRound__Some
export class CGRIDClass__Proto016PtMumbaiFitnessLockedRound__Some extends Box<Proto016PtMumbaiFitnessLockedRound__Some> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiFitnessLockedRound__Some {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiFitnessLockedRound__None generated for Proto016PtMumbaiFitnessLockedRound__None
export class CGRIDClass__Proto016PtMumbaiFitnessLockedRound__None extends Box<Proto016PtMumbaiFitnessLockedRound__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiFitnessLockedRound__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto016PtMumbaiFitnessLockedRound__Some = Int32;
export type Proto016PtMumbaiFitnessLockedRound__None = Unit;
// Class CGRIDClass__Proto016_PtMumbaiFitness_locked_round generated for Proto016PtMumbaiFitnessLockedRound
export function proto016ptmumbaifitnesslockedround_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiFitnessLockedRound,Proto016PtMumbaiFitnessLockedRound> {
    function f(disc: CGRIDTag__Proto016PtMumbaiFitnessLockedRound) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiFitnessLockedRound.None: return CGRIDClass__Proto016PtMumbaiFitnessLockedRound__None.decode;
            case CGRIDTag__Proto016PtMumbaiFitnessLockedRound.Some: return CGRIDClass__Proto016PtMumbaiFitnessLockedRound__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiFitnessLockedRound => Object.values(CGRIDTag__Proto016PtMumbaiFitnessLockedRound).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiFitness_locked_round extends Box<Proto016PtMumbaiFitnessLockedRound> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiFitnessLockedRound>, Proto016PtMumbaiFitnessLockedRound>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiFitness_locked_round {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaifitnesslockedround_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__Proto016PtMumbaiFitnessLockedRound{
    None = 0,
    Some = 1
}
export interface CGRIDMap__Proto016PtMumbaiFitnessLockedRound {
    None: CGRIDClass__Proto016PtMumbaiFitnessLockedRound__None,
    Some: CGRIDClass__Proto016PtMumbaiFitnessLockedRound__Some
}
export type Proto016PtMumbaiFitnessLockedRound = { kind: CGRIDTag__Proto016PtMumbaiFitnessLockedRound.None, value: CGRIDMap__Proto016PtMumbaiFitnessLockedRound['None'] } | { kind: CGRIDTag__Proto016PtMumbaiFitnessLockedRound.Some, value: CGRIDMap__Proto016PtMumbaiFitnessLockedRound['Some'] };
export type Proto016PtMumbaiFitness = { level: Int32, locked_round: CGRIDClass__Proto016_PtMumbaiFitness_locked_round, predecessor_round: Int32, round: Int32 };
export class CGRIDClass__Proto016PtMumbaiFitness extends Box<Proto016PtMumbaiFitness> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiFitness {
        return new this(record_decoder<Proto016PtMumbaiFitness>({level: Int32.decode, locked_round: CGRIDClass__Proto016_PtMumbaiFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.locked_round.encodeLength +  this.value.predecessor_round.encodeLength +  this.value.round.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.locked_round.writeTarget(tgt) +  this.value.predecessor_round.writeTarget(tgt) +  this.value.round.writeTarget(tgt));
    }
}
export const proto016_ptmumbai_fitness_encoder = (value: Proto016PtMumbaiFitness): OutputBytes => {
    return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(value);
}
export const proto016_ptmumbai_fitness_decoder = (p: Parser): Proto016PtMumbaiFitness => {
    return record_decoder<Proto016PtMumbaiFitness>({level: Int32.decode, locked_round: CGRIDClass__Proto016_PtMumbaiFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p);
}
