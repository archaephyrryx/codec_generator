import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int16, Int31, Int32, Int64, Int8, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Bls generated for PublicKeyHash__Bls
export class CGRIDClass__PublicKeyHash__Bls extends Box<PublicKeyHash__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Bls {
        return new this(record_decoder<PublicKeyHash__Bls>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Bls = { bls12_381_public_key_hash: FixedBytes<20> };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
            case CGRIDTag__PublicKeyHash.Bls: return CGRIDClass__PublicKeyHash__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiConstantsParametric_testnet_dictator generated for Proto016PtMumbaiConstantsParametricTestnetDictator
export class CGRIDClass__Proto016_PtMumbaiConstantsParametric_testnet_dictator extends Box<Proto016PtMumbaiConstantsParametricTestnetDictator> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiConstantsParametric_testnet_dictator {
        return new this(record_decoder<Proto016PtMumbaiConstantsParametricTestnetDictator>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement generated for Proto016PtMumbaiConstantsParametricRatioOfFrozenDepositsSlashedPerDoubleEndorsement
export class CGRIDClass__Proto016_PtMumbaiConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement extends Box<Proto016PtMumbaiConstantsParametricRatioOfFrozenDepositsSlashedPerDoubleEndorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['numerator', 'denominator']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement {
        return new this(record_decoder<Proto016PtMumbaiConstantsParametricRatioOfFrozenDepositsSlashedPerDoubleEndorsement>({numerator: Uint16.decode, denominator: Uint16.decode}, {order: ['numerator', 'denominator']})(p));
    };
    get encodeLength(): number {
        return (this.value.numerator.encodeLength +  this.value.denominator.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.numerator.writeTarget(tgt) +  this.value.denominator.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiConstantsParametric_minimal_participation_ratio generated for Proto016PtMumbaiConstantsParametricMinimalParticipationRatio
export class CGRIDClass__Proto016_PtMumbaiConstantsParametric_minimal_participation_ratio extends Box<Proto016PtMumbaiConstantsParametricMinimalParticipationRatio> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['numerator', 'denominator']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiConstantsParametric_minimal_participation_ratio {
        return new this(record_decoder<Proto016PtMumbaiConstantsParametricMinimalParticipationRatio>({numerator: Uint16.decode, denominator: Uint16.decode}, {order: ['numerator', 'denominator']})(p));
    };
    get encodeLength(): number {
        return (this.value.numerator.encodeLength +  this.value.denominator.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.numerator.writeTarget(tgt) +  this.value.denominator.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiConstantsParametric_initial_seed generated for Proto016PtMumbaiConstantsParametricInitialSeed
export class CGRIDClass__Proto016_PtMumbaiConstantsParametric_initial_seed extends Box<Proto016PtMumbaiConstantsParametricInitialSeed> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['random']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiConstantsParametric_initial_seed {
        return new this(record_decoder<Proto016PtMumbaiConstantsParametricInitialSeed>({random: FixedBytes.decode<32>({len: 32})}, {order: ['random']})(p));
    };
    get encodeLength(): number {
        return (this.value.random.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.random.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiConstantsParametric_dal_parametric generated for Proto016PtMumbaiConstantsParametricDalParametric
export class CGRIDClass__Proto016_PtMumbaiConstantsParametric_dal_parametric extends Box<Proto016PtMumbaiConstantsParametricDalParametric> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['feature_enable', 'number_of_slots', 'attestation_lag', 'availability_threshold', 'redundancy_factor', 'page_size', 'slot_size', 'number_of_shards']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiConstantsParametric_dal_parametric {
        return new this(record_decoder<Proto016PtMumbaiConstantsParametricDalParametric>({feature_enable: Bool.decode, number_of_slots: Int16.decode, attestation_lag: Int16.decode, availability_threshold: Int16.decode, redundancy_factor: Uint8.decode, page_size: Uint16.decode, slot_size: Int31.decode, number_of_shards: Uint16.decode}, {order: ['feature_enable', 'number_of_slots', 'attestation_lag', 'availability_threshold', 'redundancy_factor', 'page_size', 'slot_size', 'number_of_shards']})(p));
    };
    get encodeLength(): number {
        return (this.value.feature_enable.encodeLength +  this.value.number_of_slots.encodeLength +  this.value.attestation_lag.encodeLength +  this.value.availability_threshold.encodeLength +  this.value.redundancy_factor.encodeLength +  this.value.page_size.encodeLength +  this.value.slot_size.encodeLength +  this.value.number_of_shards.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.feature_enable.writeTarget(tgt) +  this.value.number_of_slots.writeTarget(tgt) +  this.value.attestation_lag.writeTarget(tgt) +  this.value.availability_threshold.writeTarget(tgt) +  this.value.redundancy_factor.writeTarget(tgt) +  this.value.page_size.writeTarget(tgt) +  this.value.slot_size.writeTarget(tgt) +  this.value.number_of_shards.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256,
    Bls: CGRIDClass__PublicKeyHash__Bls
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] } | { kind: CGRIDTag__PublicKeyHash.Bls, value: CGRIDMap__PublicKeyHash['Bls'] };
export type Proto016PtMumbaiConstantsParametricTestnetDictator = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiConstantsParametricRatioOfFrozenDepositsSlashedPerDoubleEndorsement = { numerator: Uint16, denominator: Uint16 };
export type Proto016PtMumbaiConstantsParametricMinimalParticipationRatio = { numerator: Uint16, denominator: Uint16 };
export type Proto016PtMumbaiConstantsParametricInitialSeed = { random: FixedBytes<32> };
export type Proto016PtMumbaiConstantsParametricDalParametric = { feature_enable: Bool, number_of_slots: Int16, attestation_lag: Int16, availability_threshold: Int16, redundancy_factor: Uint8, page_size: Uint16, slot_size: Int31, number_of_shards: Uint16 };
export type Proto016PtMumbaiConstantsParametric = { preserved_cycles: Uint8, blocks_per_cycle: Int32, blocks_per_commitment: Int32, nonce_revelation_threshold: Int32, blocks_per_stake_snapshot: Int32, cycles_per_voting_period: Int32, hard_gas_limit_per_operation: Z, hard_gas_limit_per_block: Z, proof_of_work_threshold: Int64, minimal_stake: N, vdf_difficulty: Int64, seed_nonce_revelation_tip: N, origination_size: Int31, baking_reward_fixed_portion: N, baking_reward_bonus_per_slot: N, endorsing_reward_per_slot: N, cost_per_byte: N, hard_storage_limit_per_operation: Z, quorum_min: Int32, quorum_max: Int32, min_proposal_quorum: Int32, liquidity_baking_subsidy: N, liquidity_baking_toggle_ema_threshold: Int32, max_operations_time_to_live: Int16, minimal_block_delay: Int64, delay_increment_per_round: Int64, consensus_committee_size: Int31, consensus_threshold: Int31, minimal_participation_ratio: CGRIDClass__Proto016_PtMumbaiConstantsParametric_minimal_participation_ratio, max_slashing_period: Int31, frozen_deposits_percentage: Int31, double_baking_punishment: N, ratio_of_frozen_deposits_slashed_per_double_endorsement: CGRIDClass__Proto016_PtMumbaiConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement, testnet_dictator: Option<CGRIDClass__Proto016_PtMumbaiConstantsParametric_testnet_dictator>, initial_seed: Option<CGRIDClass__Proto016_PtMumbaiConstantsParametric_initial_seed>, cache_script_size: Int31, cache_stake_distribution_cycles: Int8, cache_sampler_state_cycles: Int8, tx_rollup_enable: Bool, tx_rollup_origination_size: Int31, tx_rollup_hard_size_limit_per_inbox: Int31, tx_rollup_hard_size_limit_per_message: Int31, tx_rollup_max_withdrawals_per_batch: Int31, tx_rollup_commitment_bond: N, tx_rollup_finality_period: Int31, tx_rollup_withdraw_period: Int31, tx_rollup_max_inboxes_count: Int31, tx_rollup_max_messages_per_inbox: Int31, tx_rollup_max_commitments_count: Int31, tx_rollup_cost_per_byte_ema_factor: Int31, tx_rollup_max_ticket_payload_size: Int31, tx_rollup_rejection_max_proof_size: Int31, tx_rollup_sunset_level: Int32, dal_parametric: CGRIDClass__Proto016_PtMumbaiConstantsParametric_dal_parametric, smart_rollup_enable: Bool, smart_rollup_arith_pvm_enable: Bool, smart_rollup_origination_size: Int31, smart_rollup_challenge_window_in_blocks: Int31, smart_rollup_stake_amount: N, smart_rollup_commitment_period_in_blocks: Int31, smart_rollup_max_lookahead_in_blocks: Int32, smart_rollup_max_active_outbox_levels: Int32, smart_rollup_max_outbox_messages_per_level: Int31, smart_rollup_number_of_sections_in_dissection: Uint8, smart_rollup_timeout_period_in_blocks: Int31, smart_rollup_max_number_of_cemented_commitments: Int31, smart_rollup_max_number_of_parallel_games: Int31, zk_rollup_enable: Bool, zk_rollup_origination_size: Int31, zk_rollup_min_pending_to_process: Int31 };
export class CGRIDClass__Proto016PtMumbaiConstantsParametric extends Box<Proto016PtMumbaiConstantsParametric> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'nonce_revelation_threshold', 'blocks_per_stake_snapshot', 'cycles_per_voting_period', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'minimal_stake', 'vdf_difficulty', 'seed_nonce_revelation_tip', 'origination_size', 'baking_reward_fixed_portion', 'baking_reward_bonus_per_slot', 'endorsing_reward_per_slot', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'liquidity_baking_subsidy', 'liquidity_baking_toggle_ema_threshold', 'max_operations_time_to_live', 'minimal_block_delay', 'delay_increment_per_round', 'consensus_committee_size', 'consensus_threshold', 'minimal_participation_ratio', 'max_slashing_period', 'frozen_deposits_percentage', 'double_baking_punishment', 'ratio_of_frozen_deposits_slashed_per_double_endorsement', 'testnet_dictator', 'initial_seed', 'cache_script_size', 'cache_stake_distribution_cycles', 'cache_sampler_state_cycles', 'tx_rollup_enable', 'tx_rollup_origination_size', 'tx_rollup_hard_size_limit_per_inbox', 'tx_rollup_hard_size_limit_per_message', 'tx_rollup_max_withdrawals_per_batch', 'tx_rollup_commitment_bond', 'tx_rollup_finality_period', 'tx_rollup_withdraw_period', 'tx_rollup_max_inboxes_count', 'tx_rollup_max_messages_per_inbox', 'tx_rollup_max_commitments_count', 'tx_rollup_cost_per_byte_ema_factor', 'tx_rollup_max_ticket_payload_size', 'tx_rollup_rejection_max_proof_size', 'tx_rollup_sunset_level', 'dal_parametric', 'smart_rollup_enable', 'smart_rollup_arith_pvm_enable', 'smart_rollup_origination_size', 'smart_rollup_challenge_window_in_blocks', 'smart_rollup_stake_amount', 'smart_rollup_commitment_period_in_blocks', 'smart_rollup_max_lookahead_in_blocks', 'smart_rollup_max_active_outbox_levels', 'smart_rollup_max_outbox_messages_per_level', 'smart_rollup_number_of_sections_in_dissection', 'smart_rollup_timeout_period_in_blocks', 'smart_rollup_max_number_of_cemented_commitments', 'smart_rollup_max_number_of_parallel_games', 'zk_rollup_enable', 'zk_rollup_origination_size', 'zk_rollup_min_pending_to_process']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiConstantsParametric {
        return new this(record_decoder<Proto016PtMumbaiConstantsParametric>({preserved_cycles: Uint8.decode, blocks_per_cycle: Int32.decode, blocks_per_commitment: Int32.decode, nonce_revelation_threshold: Int32.decode, blocks_per_stake_snapshot: Int32.decode, cycles_per_voting_period: Int32.decode, hard_gas_limit_per_operation: Z.decode, hard_gas_limit_per_block: Z.decode, proof_of_work_threshold: Int64.decode, minimal_stake: N.decode, vdf_difficulty: Int64.decode, seed_nonce_revelation_tip: N.decode, origination_size: Int31.decode, baking_reward_fixed_portion: N.decode, baking_reward_bonus_per_slot: N.decode, endorsing_reward_per_slot: N.decode, cost_per_byte: N.decode, hard_storage_limit_per_operation: Z.decode, quorum_min: Int32.decode, quorum_max: Int32.decode, min_proposal_quorum: Int32.decode, liquidity_baking_subsidy: N.decode, liquidity_baking_toggle_ema_threshold: Int32.decode, max_operations_time_to_live: Int16.decode, minimal_block_delay: Int64.decode, delay_increment_per_round: Int64.decode, consensus_committee_size: Int31.decode, consensus_threshold: Int31.decode, minimal_participation_ratio: CGRIDClass__Proto016_PtMumbaiConstantsParametric_minimal_participation_ratio.decode, max_slashing_period: Int31.decode, frozen_deposits_percentage: Int31.decode, double_baking_punishment: N.decode, ratio_of_frozen_deposits_slashed_per_double_endorsement: CGRIDClass__Proto016_PtMumbaiConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement.decode, testnet_dictator: Option.decode(CGRIDClass__Proto016_PtMumbaiConstantsParametric_testnet_dictator.decode), initial_seed: Option.decode(CGRIDClass__Proto016_PtMumbaiConstantsParametric_initial_seed.decode), cache_script_size: Int31.decode, cache_stake_distribution_cycles: Int8.decode, cache_sampler_state_cycles: Int8.decode, tx_rollup_enable: Bool.decode, tx_rollup_origination_size: Int31.decode, tx_rollup_hard_size_limit_per_inbox: Int31.decode, tx_rollup_hard_size_limit_per_message: Int31.decode, tx_rollup_max_withdrawals_per_batch: Int31.decode, tx_rollup_commitment_bond: N.decode, tx_rollup_finality_period: Int31.decode, tx_rollup_withdraw_period: Int31.decode, tx_rollup_max_inboxes_count: Int31.decode, tx_rollup_max_messages_per_inbox: Int31.decode, tx_rollup_max_commitments_count: Int31.decode, tx_rollup_cost_per_byte_ema_factor: Int31.decode, tx_rollup_max_ticket_payload_size: Int31.decode, tx_rollup_rejection_max_proof_size: Int31.decode, tx_rollup_sunset_level: Int32.decode, dal_parametric: CGRIDClass__Proto016_PtMumbaiConstantsParametric_dal_parametric.decode, smart_rollup_enable: Bool.decode, smart_rollup_arith_pvm_enable: Bool.decode, smart_rollup_origination_size: Int31.decode, smart_rollup_challenge_window_in_blocks: Int31.decode, smart_rollup_stake_amount: N.decode, smart_rollup_commitment_period_in_blocks: Int31.decode, smart_rollup_max_lookahead_in_blocks: Int32.decode, smart_rollup_max_active_outbox_levels: Int32.decode, smart_rollup_max_outbox_messages_per_level: Int31.decode, smart_rollup_number_of_sections_in_dissection: Uint8.decode, smart_rollup_timeout_period_in_blocks: Int31.decode, smart_rollup_max_number_of_cemented_commitments: Int31.decode, smart_rollup_max_number_of_parallel_games: Int31.decode, zk_rollup_enable: Bool.decode, zk_rollup_origination_size: Int31.decode, zk_rollup_min_pending_to_process: Int31.decode}, {order: ['preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'nonce_revelation_threshold', 'blocks_per_stake_snapshot', 'cycles_per_voting_period', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'minimal_stake', 'vdf_difficulty', 'seed_nonce_revelation_tip', 'origination_size', 'baking_reward_fixed_portion', 'baking_reward_bonus_per_slot', 'endorsing_reward_per_slot', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'liquidity_baking_subsidy', 'liquidity_baking_toggle_ema_threshold', 'max_operations_time_to_live', 'minimal_block_delay', 'delay_increment_per_round', 'consensus_committee_size', 'consensus_threshold', 'minimal_participation_ratio', 'max_slashing_period', 'frozen_deposits_percentage', 'double_baking_punishment', 'ratio_of_frozen_deposits_slashed_per_double_endorsement', 'testnet_dictator', 'initial_seed', 'cache_script_size', 'cache_stake_distribution_cycles', 'cache_sampler_state_cycles', 'tx_rollup_enable', 'tx_rollup_origination_size', 'tx_rollup_hard_size_limit_per_inbox', 'tx_rollup_hard_size_limit_per_message', 'tx_rollup_max_withdrawals_per_batch', 'tx_rollup_commitment_bond', 'tx_rollup_finality_period', 'tx_rollup_withdraw_period', 'tx_rollup_max_inboxes_count', 'tx_rollup_max_messages_per_inbox', 'tx_rollup_max_commitments_count', 'tx_rollup_cost_per_byte_ema_factor', 'tx_rollup_max_ticket_payload_size', 'tx_rollup_rejection_max_proof_size', 'tx_rollup_sunset_level', 'dal_parametric', 'smart_rollup_enable', 'smart_rollup_arith_pvm_enable', 'smart_rollup_origination_size', 'smart_rollup_challenge_window_in_blocks', 'smart_rollup_stake_amount', 'smart_rollup_commitment_period_in_blocks', 'smart_rollup_max_lookahead_in_blocks', 'smart_rollup_max_active_outbox_levels', 'smart_rollup_max_outbox_messages_per_level', 'smart_rollup_number_of_sections_in_dissection', 'smart_rollup_timeout_period_in_blocks', 'smart_rollup_max_number_of_cemented_commitments', 'smart_rollup_max_number_of_parallel_games', 'zk_rollup_enable', 'zk_rollup_origination_size', 'zk_rollup_min_pending_to_process']})(p));
    };
    get encodeLength(): number {
        return (this.value.preserved_cycles.encodeLength +  this.value.blocks_per_cycle.encodeLength +  this.value.blocks_per_commitment.encodeLength +  this.value.nonce_revelation_threshold.encodeLength +  this.value.blocks_per_stake_snapshot.encodeLength +  this.value.cycles_per_voting_period.encodeLength +  this.value.hard_gas_limit_per_operation.encodeLength +  this.value.hard_gas_limit_per_block.encodeLength +  this.value.proof_of_work_threshold.encodeLength +  this.value.minimal_stake.encodeLength +  this.value.vdf_difficulty.encodeLength +  this.value.seed_nonce_revelation_tip.encodeLength +  this.value.origination_size.encodeLength +  this.value.baking_reward_fixed_portion.encodeLength +  this.value.baking_reward_bonus_per_slot.encodeLength +  this.value.endorsing_reward_per_slot.encodeLength +  this.value.cost_per_byte.encodeLength +  this.value.hard_storage_limit_per_operation.encodeLength +  this.value.quorum_min.encodeLength +  this.value.quorum_max.encodeLength +  this.value.min_proposal_quorum.encodeLength +  this.value.liquidity_baking_subsidy.encodeLength +  this.value.liquidity_baking_toggle_ema_threshold.encodeLength +  this.value.max_operations_time_to_live.encodeLength +  this.value.minimal_block_delay.encodeLength +  this.value.delay_increment_per_round.encodeLength +  this.value.consensus_committee_size.encodeLength +  this.value.consensus_threshold.encodeLength +  this.value.minimal_participation_ratio.encodeLength +  this.value.max_slashing_period.encodeLength +  this.value.frozen_deposits_percentage.encodeLength +  this.value.double_baking_punishment.encodeLength +  this.value.ratio_of_frozen_deposits_slashed_per_double_endorsement.encodeLength +  this.value.testnet_dictator.encodeLength +  this.value.initial_seed.encodeLength +  this.value.cache_script_size.encodeLength +  this.value.cache_stake_distribution_cycles.encodeLength +  this.value.cache_sampler_state_cycles.encodeLength +  this.value.tx_rollup_enable.encodeLength +  this.value.tx_rollup_origination_size.encodeLength +  this.value.tx_rollup_hard_size_limit_per_inbox.encodeLength +  this.value.tx_rollup_hard_size_limit_per_message.encodeLength +  this.value.tx_rollup_max_withdrawals_per_batch.encodeLength +  this.value.tx_rollup_commitment_bond.encodeLength +  this.value.tx_rollup_finality_period.encodeLength +  this.value.tx_rollup_withdraw_period.encodeLength +  this.value.tx_rollup_max_inboxes_count.encodeLength +  this.value.tx_rollup_max_messages_per_inbox.encodeLength +  this.value.tx_rollup_max_commitments_count.encodeLength +  this.value.tx_rollup_cost_per_byte_ema_factor.encodeLength +  this.value.tx_rollup_max_ticket_payload_size.encodeLength +  this.value.tx_rollup_rejection_max_proof_size.encodeLength +  this.value.tx_rollup_sunset_level.encodeLength +  this.value.dal_parametric.encodeLength +  this.value.smart_rollup_enable.encodeLength +  this.value.smart_rollup_arith_pvm_enable.encodeLength +  this.value.smart_rollup_origination_size.encodeLength +  this.value.smart_rollup_challenge_window_in_blocks.encodeLength +  this.value.smart_rollup_stake_amount.encodeLength +  this.value.smart_rollup_commitment_period_in_blocks.encodeLength +  this.value.smart_rollup_max_lookahead_in_blocks.encodeLength +  this.value.smart_rollup_max_active_outbox_levels.encodeLength +  this.value.smart_rollup_max_outbox_messages_per_level.encodeLength +  this.value.smart_rollup_number_of_sections_in_dissection.encodeLength +  this.value.smart_rollup_timeout_period_in_blocks.encodeLength +  this.value.smart_rollup_max_number_of_cemented_commitments.encodeLength +  this.value.smart_rollup_max_number_of_parallel_games.encodeLength +  this.value.zk_rollup_enable.encodeLength +  this.value.zk_rollup_origination_size.encodeLength +  this.value.zk_rollup_min_pending_to_process.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.preserved_cycles.writeTarget(tgt) +  this.value.blocks_per_cycle.writeTarget(tgt) +  this.value.blocks_per_commitment.writeTarget(tgt) +  this.value.nonce_revelation_threshold.writeTarget(tgt) +  this.value.blocks_per_stake_snapshot.writeTarget(tgt) +  this.value.cycles_per_voting_period.writeTarget(tgt) +  this.value.hard_gas_limit_per_operation.writeTarget(tgt) +  this.value.hard_gas_limit_per_block.writeTarget(tgt) +  this.value.proof_of_work_threshold.writeTarget(tgt) +  this.value.minimal_stake.writeTarget(tgt) +  this.value.vdf_difficulty.writeTarget(tgt) +  this.value.seed_nonce_revelation_tip.writeTarget(tgt) +  this.value.origination_size.writeTarget(tgt) +  this.value.baking_reward_fixed_portion.writeTarget(tgt) +  this.value.baking_reward_bonus_per_slot.writeTarget(tgt) +  this.value.endorsing_reward_per_slot.writeTarget(tgt) +  this.value.cost_per_byte.writeTarget(tgt) +  this.value.hard_storage_limit_per_operation.writeTarget(tgt) +  this.value.quorum_min.writeTarget(tgt) +  this.value.quorum_max.writeTarget(tgt) +  this.value.min_proposal_quorum.writeTarget(tgt) +  this.value.liquidity_baking_subsidy.writeTarget(tgt) +  this.value.liquidity_baking_toggle_ema_threshold.writeTarget(tgt) +  this.value.max_operations_time_to_live.writeTarget(tgt) +  this.value.minimal_block_delay.writeTarget(tgt) +  this.value.delay_increment_per_round.writeTarget(tgt) +  this.value.consensus_committee_size.writeTarget(tgt) +  this.value.consensus_threshold.writeTarget(tgt) +  this.value.minimal_participation_ratio.writeTarget(tgt) +  this.value.max_slashing_period.writeTarget(tgt) +  this.value.frozen_deposits_percentage.writeTarget(tgt) +  this.value.double_baking_punishment.writeTarget(tgt) +  this.value.ratio_of_frozen_deposits_slashed_per_double_endorsement.writeTarget(tgt) +  this.value.testnet_dictator.writeTarget(tgt) +  this.value.initial_seed.writeTarget(tgt) +  this.value.cache_script_size.writeTarget(tgt) +  this.value.cache_stake_distribution_cycles.writeTarget(tgt) +  this.value.cache_sampler_state_cycles.writeTarget(tgt) +  this.value.tx_rollup_enable.writeTarget(tgt) +  this.value.tx_rollup_origination_size.writeTarget(tgt) +  this.value.tx_rollup_hard_size_limit_per_inbox.writeTarget(tgt) +  this.value.tx_rollup_hard_size_limit_per_message.writeTarget(tgt) +  this.value.tx_rollup_max_withdrawals_per_batch.writeTarget(tgt) +  this.value.tx_rollup_commitment_bond.writeTarget(tgt) +  this.value.tx_rollup_finality_period.writeTarget(tgt) +  this.value.tx_rollup_withdraw_period.writeTarget(tgt) +  this.value.tx_rollup_max_inboxes_count.writeTarget(tgt) +  this.value.tx_rollup_max_messages_per_inbox.writeTarget(tgt) +  this.value.tx_rollup_max_commitments_count.writeTarget(tgt) +  this.value.tx_rollup_cost_per_byte_ema_factor.writeTarget(tgt) +  this.value.tx_rollup_max_ticket_payload_size.writeTarget(tgt) +  this.value.tx_rollup_rejection_max_proof_size.writeTarget(tgt) +  this.value.tx_rollup_sunset_level.writeTarget(tgt) +  this.value.dal_parametric.writeTarget(tgt) +  this.value.smart_rollup_enable.writeTarget(tgt) +  this.value.smart_rollup_arith_pvm_enable.writeTarget(tgt) +  this.value.smart_rollup_origination_size.writeTarget(tgt) +  this.value.smart_rollup_challenge_window_in_blocks.writeTarget(tgt) +  this.value.smart_rollup_stake_amount.writeTarget(tgt) +  this.value.smart_rollup_commitment_period_in_blocks.writeTarget(tgt) +  this.value.smart_rollup_max_lookahead_in_blocks.writeTarget(tgt) +  this.value.smart_rollup_max_active_outbox_levels.writeTarget(tgt) +  this.value.smart_rollup_max_outbox_messages_per_level.writeTarget(tgt) +  this.value.smart_rollup_number_of_sections_in_dissection.writeTarget(tgt) +  this.value.smart_rollup_timeout_period_in_blocks.writeTarget(tgt) +  this.value.smart_rollup_max_number_of_cemented_commitments.writeTarget(tgt) +  this.value.smart_rollup_max_number_of_parallel_games.writeTarget(tgt) +  this.value.zk_rollup_enable.writeTarget(tgt) +  this.value.zk_rollup_origination_size.writeTarget(tgt) +  this.value.zk_rollup_min_pending_to_process.writeTarget(tgt));
    }
}
export const proto016_ptmumbai_constants_parametric_encoder = (value: Proto016PtMumbaiConstantsParametric): OutputBytes => {
    return record_encoder({order: ['preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'nonce_revelation_threshold', 'blocks_per_stake_snapshot', 'cycles_per_voting_period', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'minimal_stake', 'vdf_difficulty', 'seed_nonce_revelation_tip', 'origination_size', 'baking_reward_fixed_portion', 'baking_reward_bonus_per_slot', 'endorsing_reward_per_slot', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'liquidity_baking_subsidy', 'liquidity_baking_toggle_ema_threshold', 'max_operations_time_to_live', 'minimal_block_delay', 'delay_increment_per_round', 'consensus_committee_size', 'consensus_threshold', 'minimal_participation_ratio', 'max_slashing_period', 'frozen_deposits_percentage', 'double_baking_punishment', 'ratio_of_frozen_deposits_slashed_per_double_endorsement', 'testnet_dictator', 'initial_seed', 'cache_script_size', 'cache_stake_distribution_cycles', 'cache_sampler_state_cycles', 'tx_rollup_enable', 'tx_rollup_origination_size', 'tx_rollup_hard_size_limit_per_inbox', 'tx_rollup_hard_size_limit_per_message', 'tx_rollup_max_withdrawals_per_batch', 'tx_rollup_commitment_bond', 'tx_rollup_finality_period', 'tx_rollup_withdraw_period', 'tx_rollup_max_inboxes_count', 'tx_rollup_max_messages_per_inbox', 'tx_rollup_max_commitments_count', 'tx_rollup_cost_per_byte_ema_factor', 'tx_rollup_max_ticket_payload_size', 'tx_rollup_rejection_max_proof_size', 'tx_rollup_sunset_level', 'dal_parametric', 'smart_rollup_enable', 'smart_rollup_arith_pvm_enable', 'smart_rollup_origination_size', 'smart_rollup_challenge_window_in_blocks', 'smart_rollup_stake_amount', 'smart_rollup_commitment_period_in_blocks', 'smart_rollup_max_lookahead_in_blocks', 'smart_rollup_max_active_outbox_levels', 'smart_rollup_max_outbox_messages_per_level', 'smart_rollup_number_of_sections_in_dissection', 'smart_rollup_timeout_period_in_blocks', 'smart_rollup_max_number_of_cemented_commitments', 'smart_rollup_max_number_of_parallel_games', 'zk_rollup_enable', 'zk_rollup_origination_size', 'zk_rollup_min_pending_to_process']})(value);
}
export const proto016_ptmumbai_constants_parametric_decoder = (p: Parser): Proto016PtMumbaiConstantsParametric => {
    return record_decoder<Proto016PtMumbaiConstantsParametric>({preserved_cycles: Uint8.decode, blocks_per_cycle: Int32.decode, blocks_per_commitment: Int32.decode, nonce_revelation_threshold: Int32.decode, blocks_per_stake_snapshot: Int32.decode, cycles_per_voting_period: Int32.decode, hard_gas_limit_per_operation: Z.decode, hard_gas_limit_per_block: Z.decode, proof_of_work_threshold: Int64.decode, minimal_stake: N.decode, vdf_difficulty: Int64.decode, seed_nonce_revelation_tip: N.decode, origination_size: Int31.decode, baking_reward_fixed_portion: N.decode, baking_reward_bonus_per_slot: N.decode, endorsing_reward_per_slot: N.decode, cost_per_byte: N.decode, hard_storage_limit_per_operation: Z.decode, quorum_min: Int32.decode, quorum_max: Int32.decode, min_proposal_quorum: Int32.decode, liquidity_baking_subsidy: N.decode, liquidity_baking_toggle_ema_threshold: Int32.decode, max_operations_time_to_live: Int16.decode, minimal_block_delay: Int64.decode, delay_increment_per_round: Int64.decode, consensus_committee_size: Int31.decode, consensus_threshold: Int31.decode, minimal_participation_ratio: CGRIDClass__Proto016_PtMumbaiConstantsParametric_minimal_participation_ratio.decode, max_slashing_period: Int31.decode, frozen_deposits_percentage: Int31.decode, double_baking_punishment: N.decode, ratio_of_frozen_deposits_slashed_per_double_endorsement: CGRIDClass__Proto016_PtMumbaiConstantsParametric_ratio_of_frozen_deposits_slashed_per_double_endorsement.decode, testnet_dictator: Option.decode(CGRIDClass__Proto016_PtMumbaiConstantsParametric_testnet_dictator.decode), initial_seed: Option.decode(CGRIDClass__Proto016_PtMumbaiConstantsParametric_initial_seed.decode), cache_script_size: Int31.decode, cache_stake_distribution_cycles: Int8.decode, cache_sampler_state_cycles: Int8.decode, tx_rollup_enable: Bool.decode, tx_rollup_origination_size: Int31.decode, tx_rollup_hard_size_limit_per_inbox: Int31.decode, tx_rollup_hard_size_limit_per_message: Int31.decode, tx_rollup_max_withdrawals_per_batch: Int31.decode, tx_rollup_commitment_bond: N.decode, tx_rollup_finality_period: Int31.decode, tx_rollup_withdraw_period: Int31.decode, tx_rollup_max_inboxes_count: Int31.decode, tx_rollup_max_messages_per_inbox: Int31.decode, tx_rollup_max_commitments_count: Int31.decode, tx_rollup_cost_per_byte_ema_factor: Int31.decode, tx_rollup_max_ticket_payload_size: Int31.decode, tx_rollup_rejection_max_proof_size: Int31.decode, tx_rollup_sunset_level: Int32.decode, dal_parametric: CGRIDClass__Proto016_PtMumbaiConstantsParametric_dal_parametric.decode, smart_rollup_enable: Bool.decode, smart_rollup_arith_pvm_enable: Bool.decode, smart_rollup_origination_size: Int31.decode, smart_rollup_challenge_window_in_blocks: Int31.decode, smart_rollup_stake_amount: N.decode, smart_rollup_commitment_period_in_blocks: Int31.decode, smart_rollup_max_lookahead_in_blocks: Int32.decode, smart_rollup_max_active_outbox_levels: Int32.decode, smart_rollup_max_outbox_messages_per_level: Int31.decode, smart_rollup_number_of_sections_in_dissection: Uint8.decode, smart_rollup_timeout_period_in_blocks: Int31.decode, smart_rollup_max_number_of_cemented_commitments: Int31.decode, smart_rollup_max_number_of_parallel_games: Int31.decode, zk_rollup_enable: Bool.decode, zk_rollup_origination_size: Int31.decode, zk_rollup_min_pending_to_process: Int31.decode}, {order: ['preserved_cycles', 'blocks_per_cycle', 'blocks_per_commitment', 'nonce_revelation_threshold', 'blocks_per_stake_snapshot', 'cycles_per_voting_period', 'hard_gas_limit_per_operation', 'hard_gas_limit_per_block', 'proof_of_work_threshold', 'minimal_stake', 'vdf_difficulty', 'seed_nonce_revelation_tip', 'origination_size', 'baking_reward_fixed_portion', 'baking_reward_bonus_per_slot', 'endorsing_reward_per_slot', 'cost_per_byte', 'hard_storage_limit_per_operation', 'quorum_min', 'quorum_max', 'min_proposal_quorum', 'liquidity_baking_subsidy', 'liquidity_baking_toggle_ema_threshold', 'max_operations_time_to_live', 'minimal_block_delay', 'delay_increment_per_round', 'consensus_committee_size', 'consensus_threshold', 'minimal_participation_ratio', 'max_slashing_period', 'frozen_deposits_percentage', 'double_baking_punishment', 'ratio_of_frozen_deposits_slashed_per_double_endorsement', 'testnet_dictator', 'initial_seed', 'cache_script_size', 'cache_stake_distribution_cycles', 'cache_sampler_state_cycles', 'tx_rollup_enable', 'tx_rollup_origination_size', 'tx_rollup_hard_size_limit_per_inbox', 'tx_rollup_hard_size_limit_per_message', 'tx_rollup_max_withdrawals_per_batch', 'tx_rollup_commitment_bond', 'tx_rollup_finality_period', 'tx_rollup_withdraw_period', 'tx_rollup_max_inboxes_count', 'tx_rollup_max_messages_per_inbox', 'tx_rollup_max_commitments_count', 'tx_rollup_cost_per_byte_ema_factor', 'tx_rollup_max_ticket_payload_size', 'tx_rollup_rejection_max_proof_size', 'tx_rollup_sunset_level', 'dal_parametric', 'smart_rollup_enable', 'smart_rollup_arith_pvm_enable', 'smart_rollup_origination_size', 'smart_rollup_challenge_window_in_blocks', 'smart_rollup_stake_amount', 'smart_rollup_commitment_period_in_blocks', 'smart_rollup_max_lookahead_in_blocks', 'smart_rollup_max_active_outbox_levels', 'smart_rollup_max_outbox_messages_per_level', 'smart_rollup_number_of_sections_in_dissection', 'smart_rollup_timeout_period_in_blocks', 'smart_rollup_max_number_of_cemented_commitments', 'smart_rollup_max_number_of_parallel_games', 'zk_rollup_enable', 'zk_rollup_origination_size', 'zk_rollup_min_pending_to_process']})(p);
}
