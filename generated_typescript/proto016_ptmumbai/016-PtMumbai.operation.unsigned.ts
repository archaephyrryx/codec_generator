import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Nullable } from '../../ts_runtime/composite/opt/nullable';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { SequenceBounded } from '../../ts_runtime/composite/seq/sequence.bounded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int16, Int31, Int32, Int64, Int8, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Bls generated for PublicKey__Bls
export class CGRIDClass__PublicKey__Bls extends Box<PublicKey__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Bls {
        return new this(record_decoder<PublicKey__Bls>({bls12_381_public_key: FixedBytes.decode<48>({len: 48})}, {order: ['bls12_381_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Bls generated for PublicKeyHash__Bls
export class CGRIDClass__PublicKeyHash__Bls extends Box<PublicKeyHash__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Bls {
        return new this(record_decoder<PublicKeyHash__Bls>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_update generated for Proto016PtMumbaiOperationAlphaContents__Zk_rollup_update
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_update extends Box<Proto016PtMumbaiOperationAlphaContents__Zk_rollup_update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'update']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_update {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Zk_rollup_update>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, zk_rollup: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_zk_rollup.decode, update: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'update']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.zk_rollup.encodeLength +  this.value.update.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.zk_rollup.writeTarget(tgt) +  this.value.update.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_publish generated for Proto016PtMumbaiOperationAlphaContents__Zk_rollup_publish
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_publish extends Box<Proto016PtMumbaiOperationAlphaContents__Zk_rollup_publish> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'op']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_publish {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Zk_rollup_publish>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, zk_rollup: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_zk_rollup.decode, op: Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'zk_rollup', 'op']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.zk_rollup.encodeLength +  this.value.op.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.zk_rollup.writeTarget(tgt) +  this.value.op.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_origination generated for Proto016PtMumbaiOperationAlphaContents__Zk_rollup_origination
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_origination extends Box<Proto016PtMumbaiOperationAlphaContents__Zk_rollup_origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_parameters', 'circuits_info', 'init_state', 'nb_ops']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_origination {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Zk_rollup_origination>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_parameters: Dynamic.decode(Bytes.decode, width.Uint30), circuits_info: Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq.decode), width.Uint30), init_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30), nb_ops: Int31.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_parameters', 'circuits_info', 'init_state', 'nb_ops']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_parameters.encodeLength +  this.value.circuits_info.encodeLength +  this.value.init_state.encodeLength +  this.value.nb_ops.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_parameters.writeTarget(tgt) +  this.value.circuits_info.writeTarget(tgt) +  this.value.init_state.writeTarget(tgt) +  this.value.nb_ops.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Vdf_revelation generated for Proto016PtMumbaiOperationAlphaContents__Vdf_revelation
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Vdf_revelation extends Box<Proto016PtMumbaiOperationAlphaContents__Vdf_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['solution']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Vdf_revelation {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Vdf_revelation>({solution: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Vdf_revelation_solution.decode}, {order: ['solution']})(p));
    };
    get encodeLength(): number {
        return (this.value.solution.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.solution.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Update_consensus_key generated for Proto016PtMumbaiOperationAlphaContents__Update_consensus_key
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Update_consensus_key extends Box<Proto016PtMumbaiOperationAlphaContents__Update_consensus_key> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pk']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Update_consensus_key {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Update_consensus_key>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Update_consensus_key_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, pk: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Update_consensus_key_pk.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pk']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.pk.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.pk.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_submit_batch generated for Proto016PtMumbaiOperationAlphaContents__Tx_rollup_submit_batch
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_submit_batch extends Box<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_submit_batch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'content', 'burn_limit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_submit_batch {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_submit_batch>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_submit_batch_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id.decode, content: Dynamic.decode(U8String.decode, width.Uint30), burn_limit: Option.decode(N.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'content', 'burn_limit']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.content.encodeLength +  this.value.burn_limit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.content.writeTarget(tgt) +  this.value.burn_limit.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_return_bond generated for Proto016PtMumbaiOperationAlphaContents__Tx_rollup_return_bond
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_return_bond extends Box<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_return_bond> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_return_bond {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_return_bond>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_return_bond_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_remove_commitment generated for Proto016PtMumbaiOperationAlphaContents__Tx_rollup_remove_commitment
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_remove_commitment extends Box<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_remove_commitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_remove_commitment {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_remove_commitment>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_remove_commitment_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_rejection generated for Proto016PtMumbaiOperationAlphaContents__Tx_rollup_rejection
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_rejection extends Box<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_rejection> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'level', 'message', 'message_position', 'message_path', 'message_result_hash', 'message_result_path', 'previous_message_result', 'previous_message_result_path', 'proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_rejection {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_rejection>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id.decode, level: Int32.decode, message: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message.decode, message_position: N.decode, message_path: Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq.decode), width.Uint30), message_result_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_result_hash.decode, message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), previous_message_result: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result.decode, previous_message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), proof: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'level', 'message', 'message_position', 'message_path', 'message_result_hash', 'message_result_path', 'previous_message_result', 'previous_message_result_path', 'proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.level.encodeLength +  this.value.message.encodeLength +  this.value.message_position.encodeLength +  this.value.message_path.encodeLength +  this.value.message_result_hash.encodeLength +  this.value.message_result_path.encodeLength +  this.value.previous_message_result.encodeLength +  this.value.previous_message_result_path.encodeLength +  this.value.proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.message.writeTarget(tgt) +  this.value.message_position.writeTarget(tgt) +  this.value.message_path.writeTarget(tgt) +  this.value.message_result_hash.writeTarget(tgt) +  this.value.message_result_path.writeTarget(tgt) +  this.value.previous_message_result.writeTarget(tgt) +  this.value.previous_message_result_path.writeTarget(tgt) +  this.value.proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_origination generated for Proto016PtMumbaiOperationAlphaContents__Tx_rollup_origination
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_origination extends Box<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup_origination']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_origination {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_origination>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, tx_rollup_origination: Unit.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup_origination']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.tx_rollup_origination.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.tx_rollup_origination.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_finalize_commitment generated for Proto016PtMumbaiOperationAlphaContents__Tx_rollup_finalize_commitment
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_finalize_commitment extends Box<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_finalize_commitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_finalize_commitment {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_finalize_commitment>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_finalize_commitment_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_dispatch_tickets generated for Proto016PtMumbaiOperationAlphaContents__Tx_rollup_dispatch_tickets
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_dispatch_tickets extends Box<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_dispatch_tickets> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup', 'level', 'context_hash', 'message_index', 'message_result_path', 'tickets_info']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_dispatch_tickets {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_dispatch_tickets>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, tx_rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id.decode, level: Int32.decode, context_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash.decode, message_index: Int31.decode, message_result_path: Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq.decode), width.Uint30), tickets_info: Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'tx_rollup', 'level', 'context_hash', 'message_index', 'message_result_path', 'tickets_info']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.tx_rollup.encodeLength +  this.value.level.encodeLength +  this.value.context_hash.encodeLength +  this.value.message_index.encodeLength +  this.value.message_result_path.encodeLength +  this.value.tickets_info.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.tx_rollup.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.context_hash.writeTarget(tgt) +  this.value.message_index.writeTarget(tgt) +  this.value.message_result_path.writeTarget(tgt) +  this.value.tickets_info.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_commit generated for Proto016PtMumbaiOperationAlphaContents__Tx_rollup_commit
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_commit extends Box<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_commit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_commit {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Tx_rollup_commit>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id.decode, commitment: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Transfer_ticket generated for Proto016PtMumbaiOperationAlphaContents__Transfer_ticket
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Transfer_ticket extends Box<Proto016PtMumbaiOperationAlphaContents__Transfer_ticket> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'ticket_contents', 'ticket_ty', 'ticket_ticketer', 'ticket_amount', 'destination', 'entrypoint']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Transfer_ticket {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Transfer_ticket>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transfer_ticket_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, ticket_contents: Dynamic.decode(Bytes.decode, width.Uint30), ticket_ty: Dynamic.decode(Bytes.decode, width.Uint30), ticket_ticketer: CGRIDClass__Proto016_PtMumbaiContract_id.decode, ticket_amount: N.decode, destination: CGRIDClass__Proto016_PtMumbaiContract_id.decode, entrypoint: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'ticket_contents', 'ticket_ty', 'ticket_ticketer', 'ticket_amount', 'destination', 'entrypoint']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.ticket_contents.encodeLength +  this.value.ticket_ty.encodeLength +  this.value.ticket_ticketer.encodeLength +  this.value.ticket_amount.encodeLength +  this.value.destination.encodeLength +  this.value.entrypoint.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.ticket_contents.writeTarget(tgt) +  this.value.ticket_ty.writeTarget(tgt) +  this.value.ticket_ticketer.writeTarget(tgt) +  this.value.ticket_amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.entrypoint.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Transaction generated for Proto016PtMumbaiOperationAlphaContents__Transaction
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Transaction extends Box<Proto016PtMumbaiOperationAlphaContents__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Transaction {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Transaction>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transaction_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: N.decode, destination: CGRIDClass__Proto016_PtMumbaiContract_id.decode, parameters: Option.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transaction_parameters.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_timeout generated for Proto016PtMumbaiOperationAlphaContents__Smart_rollup_timeout
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_timeout extends Box<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_timeout> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'stakers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_timeout {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_timeout>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address.decode, stakers: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'stakers']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.stakers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.stakers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_refute generated for Proto016PtMumbaiOperationAlphaContents__Smart_rollup_refute
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_refute extends Box<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_refute> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'opponent', 'refutation']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_refute {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_refute>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address.decode, opponent: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_opponent.decode, refutation: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'opponent', 'refutation']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.opponent.encodeLength +  this.value.refutation.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.opponent.writeTarget(tgt) +  this.value.refutation.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_recover_bond generated for Proto016PtMumbaiOperationAlphaContents__Smart_rollup_recover_bond
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_recover_bond extends Box<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_recover_bond> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'staker']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_recover_bond {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_recover_bond>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_rollup.decode, staker: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_staker.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'staker']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.staker.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.staker.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_publish generated for Proto016PtMumbaiOperationAlphaContents__Smart_rollup_publish
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_publish extends Box<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_publish> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_publish {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_publish>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address.decode, commitment: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_originate generated for Proto016PtMumbaiOperationAlphaContents__Smart_rollup_originate
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_originate extends Box<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_originate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pvm_kind', 'kernel', 'origination_proof', 'parameters_ty']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_originate {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_originate>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_originate_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, pvm_kind: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_originate_pvm_kind.decode, kernel: Dynamic.decode(U8String.decode, width.Uint30), origination_proof: Dynamic.decode(U8String.decode, width.Uint30), parameters_ty: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'pvm_kind', 'kernel', 'origination_proof', 'parameters_ty']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.pvm_kind.encodeLength +  this.value.kernel.encodeLength +  this.value.origination_proof.encodeLength +  this.value.parameters_ty.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.pvm_kind.writeTarget(tgt) +  this.value.kernel.writeTarget(tgt) +  this.value.origination_proof.writeTarget(tgt) +  this.value.parameters_ty.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_execute_outbox_message generated for Proto016PtMumbaiOperationAlphaContents__Smart_rollup_execute_outbox_message
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_execute_outbox_message extends Box<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_execute_outbox_message> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'cemented_commitment', 'output_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_execute_outbox_message {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_execute_outbox_message>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_execute_outbox_message_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address.decode, cemented_commitment: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_execute_outbox_message_cemented_commitment.decode, output_proof: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'cemented_commitment', 'output_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.cemented_commitment.encodeLength +  this.value.output_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.cemented_commitment.writeTarget(tgt) +  this.value.output_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_cement generated for Proto016PtMumbaiOperationAlphaContents__Smart_rollup_cement
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_cement extends Box<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_cement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_cement {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_cement>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_cement_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address.decode, commitment: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_cement_commitment.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'rollup', 'commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.rollup.encodeLength +  this.value.commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.rollup.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_add_messages generated for Proto016PtMumbaiOperationAlphaContents__Smart_rollup_add_messages
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_add_messages extends Box<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_add_messages> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'message']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_add_messages {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Smart_rollup_add_messages>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_add_messages_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, message: Dynamic.decode(Sequence.decode(Dynamic.decode(U8String.decode, width.Uint30)), width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'message']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.message.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.message.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Set_deposits_limit generated for Proto016PtMumbaiOperationAlphaContents__Set_deposits_limit
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Set_deposits_limit extends Box<Proto016PtMumbaiOperationAlphaContents__Set_deposits_limit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'limit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Set_deposits_limit {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Set_deposits_limit>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Set_deposits_limit_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, limit: Option.decode(N.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'limit']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.limit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.limit.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Seed_nonce_revelation generated for Proto016PtMumbaiOperationAlphaContents__Seed_nonce_revelation
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Seed_nonce_revelation extends Box<Proto016PtMumbaiOperationAlphaContents__Seed_nonce_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Seed_nonce_revelation {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Seed_nonce_revelation>({level: Int32.decode, nonce: FixedBytes.decode<32>({len: 32})}, {order: ['level', 'nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Reveal generated for Proto016PtMumbaiOperationAlphaContents__Reveal
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Reveal extends Box<Proto016PtMumbaiOperationAlphaContents__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Reveal {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Reveal>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Reveal_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_key: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Reveal_public_key.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Register_global_constant generated for Proto016PtMumbaiOperationAlphaContents__Register_global_constant
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Register_global_constant extends Box<Proto016PtMumbaiOperationAlphaContents__Register_global_constant> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Register_global_constant {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Register_global_constant>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Register_global_constant_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Proposals generated for Proto016PtMumbaiOperationAlphaContents__Proposals
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Proposals extends Box<Proto016PtMumbaiOperationAlphaContents__Proposals> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposals']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Proposals {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Proposals>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Proposals_source.decode, period: Int32.decode, proposals: Dynamic.decode(SequenceBounded.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq.decode, 20, None), width.Uint30)}, {order: ['source', 'period', 'proposals']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposals.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposals.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Preendorsement generated for Proto016PtMumbaiOperationAlphaContents__Preendorsement
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Preendorsement extends Box<Proto016PtMumbaiOperationAlphaContents__Preendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Preendorsement {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Preendorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Preendorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Origination generated for Proto016PtMumbaiOperationAlphaContents__Origination
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Origination extends Box<Proto016PtMumbaiOperationAlphaContents__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Origination {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Origination>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, balance: N.decode, delegate: Option.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Origination_delegate.decode), script: CGRIDClass__Proto016_PtMumbaiScriptedContracts.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Increase_paid_storage generated for Proto016PtMumbaiOperationAlphaContents__Increase_paid_storage
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Increase_paid_storage extends Box<Proto016PtMumbaiOperationAlphaContents__Increase_paid_storage> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Increase_paid_storage {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Increase_paid_storage>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Increase_paid_storage_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: Z.decode, destination: CGRIDClass__Proto016_PtMumbaiContract_idOriginated.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Failing_noop generated for Proto016PtMumbaiOperationAlphaContents__Failing_noop
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Failing_noop extends Box<Proto016PtMumbaiOperationAlphaContents__Failing_noop> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['arbitrary']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Failing_noop {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Failing_noop>({arbitrary: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['arbitrary']})(p));
    };
    get encodeLength(): number {
        return (this.value.arbitrary.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.arbitrary.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Endorsement generated for Proto016PtMumbaiOperationAlphaContents__Endorsement
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Endorsement extends Box<Proto016PtMumbaiOperationAlphaContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Endorsement {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Endorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Endorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Drain_delegate generated for Proto016PtMumbaiOperationAlphaContents__Drain_delegate
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Drain_delegate extends Box<Proto016PtMumbaiOperationAlphaContents__Drain_delegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['consensus_key', 'delegate', 'destination']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Drain_delegate {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Drain_delegate>({consensus_key: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_consensus_key.decode, delegate: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_delegate.decode, destination: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_destination.decode}, {order: ['consensus_key', 'delegate', 'destination']})(p));
    };
    get encodeLength(): number {
        return (this.value.consensus_key.encodeLength +  this.value.delegate.encodeLength +  this.value.destination.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.consensus_key.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.destination.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_preendorsement_evidence generated for Proto016PtMumbaiOperationAlphaContents__Double_preendorsement_evidence
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_preendorsement_evidence extends Box<Proto016PtMumbaiOperationAlphaContents__Double_preendorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_preendorsement_evidence {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Double_preendorsement_evidence>({op1: Dynamic.decode(CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_endorsement_evidence generated for Proto016PtMumbaiOperationAlphaContents__Double_endorsement_evidence
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_endorsement_evidence extends Box<Proto016PtMumbaiOperationAlphaContents__Double_endorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_endorsement_evidence {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Double_endorsement_evidence>({op1: Dynamic.decode(CGRIDClass__Proto016_PtMumbaiInlinedEndorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__Proto016_PtMumbaiInlinedEndorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_baking_evidence generated for Proto016PtMumbaiOperationAlphaContents__Double_baking_evidence
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_baking_evidence extends Box<Proto016PtMumbaiOperationAlphaContents__Double_baking_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bh1', 'bh2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_baking_evidence {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Double_baking_evidence>({bh1: Dynamic.decode(CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaFull_header.decode, width.Uint30), bh2: Dynamic.decode(CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaFull_header.decode, width.Uint30)}, {order: ['bh1', 'bh2']})(p));
    };
    get encodeLength(): number {
        return (this.value.bh1.encodeLength +  this.value.bh2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bh1.writeTarget(tgt) +  this.value.bh2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Delegation generated for Proto016PtMumbaiOperationAlphaContents__Delegation
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Delegation extends Box<Proto016PtMumbaiOperationAlphaContents__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Delegation {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Delegation>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Delegation_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, delegate: Option.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Delegation_delegate.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Dal_publish_slot_header generated for Proto016PtMumbaiOperationAlphaContents__Dal_publish_slot_header
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Dal_publish_slot_header extends Box<Proto016PtMumbaiOperationAlphaContents__Dal_publish_slot_header> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'slot_header']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Dal_publish_slot_header {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Dal_publish_slot_header>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, slot_header: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_slot_header.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'slot_header']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.slot_header.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.slot_header.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Dal_attestation generated for Proto016PtMumbaiOperationAlphaContents__Dal_attestation
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Dal_attestation extends Box<Proto016PtMumbaiOperationAlphaContents__Dal_attestation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['attestor', 'attestation', 'level']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Dal_attestation {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Dal_attestation>({attestor: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_attestation_attestor.decode, attestation: Z.decode, level: Int32.decode}, {order: ['attestor', 'attestation', 'level']})(p));
    };
    get encodeLength(): number {
        return (this.value.attestor.encodeLength +  this.value.attestation.encodeLength +  this.value.level.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.attestor.writeTarget(tgt) +  this.value.attestation.writeTarget(tgt) +  this.value.level.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Ballot generated for Proto016PtMumbaiOperationAlphaContents__Ballot
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Ballot extends Box<Proto016PtMumbaiOperationAlphaContents__Ballot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposal', 'ballot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Ballot {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Ballot>({source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Ballot_source.decode, period: Int32.decode, proposal: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Ballot_proposal.decode, ballot: Int8.decode}, {order: ['source', 'period', 'proposal', 'ballot']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposal.encodeLength +  this.value.ballot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposal.writeTarget(tgt) +  this.value.ballot.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Activate_account generated for Proto016PtMumbaiOperationAlphaContents__Activate_account
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Activate_account extends Box<Proto016PtMumbaiOperationAlphaContents__Activate_account> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'secret']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Activate_account {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContents__Activate_account>({pkh: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Activate_account_pkh.decode, secret: FixedBytes.decode<20>({len: 20})}, {order: ['pkh', 'secret']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.secret.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.secret.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some generated for Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contents', 'ty', 'ticketer']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some>({contents: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode, ty: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode, ticketer: CGRIDClass__Proto016_PtMumbaiContract_id.decode}, {order: ['contents', 'ty', 'ticketer']})(p));
    };
    get encodeLength(): number {
        return (this.value.contents.encodeLength +  this.value.ty.encodeLength +  this.value.ticketer.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contents.writeTarget(tgt) +  this.value.ty.writeTarget(tgt) +  this.value.ticketer.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None generated for Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public generated for Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_public']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public>({_public: Unit.decode}, {order: ['_public']})(p));
    };
    get encodeLength(): number {
        return (this.value._public.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._public.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private generated for Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_private']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private>({_private: Unit.decode}, {order: ['_private']})(p));
    };
    get encodeLength(): number {
        return (this.value._private.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._private.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee generated for Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['fee']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee>({fee: Unit.decode}, {order: ['fee']})(p));
    };
    get encodeLength(): number {
        return (this.value.fee.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.fee.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Deposit generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Deposit
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Deposit extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Deposit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['deposit']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Deposit {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Deposit>({deposit: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit.decode}, {order: ['deposit']})(p));
    };
    get encodeLength(): number {
        return (this.value.deposit.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.deposit.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Batch generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Batch
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Batch extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Batch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['batch']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Batch {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Batch>({batch: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['batch']})(p));
    };
    get encodeLength(): number {
        return (this.value.batch.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.batch.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 {
        return new this(Uint8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 generated for Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 generated for Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 generated for Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 generated for Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 {
        return new this(Uint8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some generated for Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some>({commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None generated for Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Start generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Start
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Start extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Start> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['refutation_kind', 'player_commitment_hash', 'opponent_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Start {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Start>({refutation_kind: Unit.decode, player_commitment_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Start_player_commitment_hash.decode, opponent_commitment_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Start_opponent_commitment_hash.decode}, {order: ['refutation_kind', 'player_commitment_hash', 'opponent_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.refutation_kind.encodeLength +  this.value.player_commitment_hash.encodeLength +  this.value.opponent_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.refutation_kind.writeTarget(tgt) +  this.value.player_commitment_hash.writeTarget(tgt) +  this.value.opponent_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Move generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Move
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Move extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Move> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['refutation_kind', 'choice', 'step']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Move {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Move>({refutation_kind: Unit.decode, choice: N.decode, step: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step.decode}, {order: ['refutation_kind', 'choice', 'step']})(p));
    };
    get encodeLength(): number {
        return (this.value.refutation_kind.encodeLength +  this.value.choice.encodeLength +  this.value.step.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.refutation_kind.writeTarget(tgt) +  this.value.choice.writeTarget(tgt) +  this.value.step.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pvm_step', 'input_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof>({pvm_step: Dynamic.decode(U8String.decode, width.Uint30), input_proof: Option.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof.decode)}, {order: ['pvm_step', 'input_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.pvm_step.encodeLength +  this.value.input_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pvm_step.writeTarget(tgt) +  this.value.input_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['input_proof_kind', 'reveal_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof>({input_proof_kind: Unit.decode, reveal_proof: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof.decode}, {order: ['input_proof_kind', 'reveal_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.input_proof_kind.encodeLength +  this.value.reveal_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.input_proof_kind.writeTarget(tgt) +  this.value.reveal_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['input_proof_kind', 'level', 'message_counter', 'serialized_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof>({input_proof_kind: Unit.decode, level: Int32.decode, message_counter: N.decode, serialized_proof: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['input_proof_kind', 'level', 'message_counter', 'serialized_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.input_proof_kind.encodeLength +  this.value.level.encodeLength +  this.value.message_counter.encodeLength +  this.value.serialized_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.input_proof_kind.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.message_counter.writeTarget(tgt) +  this.value.serialized_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['input_proof_kind']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input>({input_proof_kind: Unit.decode}, {order: ['input_proof_kind']})(p));
    };
    get encodeLength(): number {
        return (this.value.input_proof_kind.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.input_proof_kind.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['reveal_proof_kind', 'raw_data']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof>({reveal_proof_kind: Unit.decode, raw_data: Dynamic.decode(U8String.decode, width.Uint16)}, {order: ['reveal_proof_kind', 'raw_data']})(p));
    };
    get encodeLength(): number {
        return (this.value.reveal_proof_kind.encodeLength +  this.value.raw_data.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.reveal_proof_kind.writeTarget(tgt) +  this.value.raw_data.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['reveal_proof_kind']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof>({reveal_proof_kind: Unit.decode}, {order: ['reveal_proof_kind']})(p));
    };
    get encodeLength(): number {
        return (this.value.reveal_proof_kind.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.reveal_proof_kind.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof
export class CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['reveal_proof_kind', 'dal_page_id', 'dal_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof>({reveal_proof_kind: Unit.decode, dal_page_id: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id.decode, dal_proof: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['reveal_proof_kind', 'dal_page_id', 'dal_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.reveal_proof_kind.encodeLength +  this.value.dal_page_id.encodeLength +  this.value.dal_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.reveal_proof_kind.writeTarget(tgt) +  this.value.dal_page_id.writeTarget(tgt) +  this.value.dal_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiInlinedPreendorsementContents__Preendorsement generated for Proto016PtMumbaiInlinedPreendorsementContents__Preendorsement
export class CGRIDClass__Proto016PtMumbaiInlinedPreendorsementContents__Preendorsement extends Box<Proto016PtMumbaiInlinedPreendorsementContents__Preendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiInlinedPreendorsementContents__Preendorsement {
        return new this(record_decoder<Proto016PtMumbaiInlinedPreendorsementContents__Preendorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto016_PtMumbaiInlinedPreendorsementContents_Preendorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiInlinedEndorsementMempoolContents__Endorsement generated for Proto016PtMumbaiInlinedEndorsementMempoolContents__Endorsement
export class CGRIDClass__Proto016PtMumbaiInlinedEndorsementMempoolContents__Endorsement extends Box<Proto016PtMumbaiInlinedEndorsementMempoolContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['slot', 'level', 'round', 'block_payload_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiInlinedEndorsementMempoolContents__Endorsement {
        return new this(record_decoder<Proto016PtMumbaiInlinedEndorsementMempoolContents__Endorsement>({slot: Uint16.decode, level: Int32.decode, round: Int32.decode, block_payload_hash: CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash.decode}, {order: ['slot', 'level', 'round', 'block_payload_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.slot.encodeLength +  this.value.level.encodeLength +  this.value.round.encodeLength +  this.value.block_payload_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.slot.writeTarget(tgt) +  this.value.level.writeTarget(tgt) +  this.value.round.writeTarget(tgt) +  this.value.block_payload_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint__set_delegate generated for Proto016PtMumbaiEntrypoint__set_delegate
export class CGRIDClass__Proto016PtMumbaiEntrypoint__set_delegate extends Box<Proto016PtMumbaiEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint__root generated for Proto016PtMumbaiEntrypoint__root
export class CGRIDClass__Proto016PtMumbaiEntrypoint__root extends Box<Proto016PtMumbaiEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint__remove_delegate generated for Proto016PtMumbaiEntrypoint__remove_delegate
export class CGRIDClass__Proto016PtMumbaiEntrypoint__remove_delegate extends Box<Proto016PtMumbaiEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint__named generated for Proto016PtMumbaiEntrypoint__named
export class CGRIDClass__Proto016PtMumbaiEntrypoint__named extends Box<Proto016PtMumbaiEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint__deposit generated for Proto016PtMumbaiEntrypoint__deposit
export class CGRIDClass__Proto016PtMumbaiEntrypoint__deposit extends Box<Proto016PtMumbaiEntrypoint__deposit> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint__deposit {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint___do generated for Proto016PtMumbaiEntrypoint___do
export class CGRIDClass__Proto016PtMumbaiEntrypoint___do extends Box<Proto016PtMumbaiEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint___default generated for Proto016PtMumbaiEntrypoint___default
export class CGRIDClass__Proto016PtMumbaiEntrypoint___default extends Box<Proto016PtMumbaiEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiContractId__Originated generated for Proto016PtMumbaiContractId__Originated
export class CGRIDClass__Proto016PtMumbaiContractId__Originated extends Box<Proto016PtMumbaiContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiContractId__Implicit generated for Proto016PtMumbaiContractId__Implicit
export class CGRIDClass__Proto016PtMumbaiContractId__Implicit extends Box<Proto016PtMumbaiContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiContractId__Implicit {
        return new this(record_decoder<Proto016PtMumbaiContractId__Implicit>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiContractIdOriginated__Originated generated for Proto016PtMumbaiContractIdOriginated__Originated
export class CGRIDClass__Proto016PtMumbaiContractIdOriginated__Originated extends Box<Proto016PtMumbaiContractIdOriginated__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiContractIdOriginated__Originated {
        return new this(Padded.decode(CGRIDClass__Proto016_PtMumbaiContract_idOriginated_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__String generated for MichelineProto016PtMumbaiMichelsonV1Expression__String
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__String extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__String {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Sequence generated for MichelineProto016PtMumbaiMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Sequence extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Int generated for MichelineProto016PtMumbaiMichelsonV1Expression__Int
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Int extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Bytes generated for MichelineProto016PtMumbaiMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Bytes extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKey__Bls = { bls12_381_public_key: FixedBytes<48> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Bls = { bls12_381_public_key_hash: FixedBytes<20> };
export type Proto016PtMumbaiOperationAlphaContents__Zk_rollup_update = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_source, fee: N, counter: N, gas_limit: N, storage_limit: N, zk_rollup: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_zk_rollup, update: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update };
export type Proto016PtMumbaiOperationAlphaContents__Zk_rollup_publish = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_source, fee: N, counter: N, gas_limit: N, storage_limit: N, zk_rollup: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_zk_rollup, op: Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq>,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Zk_rollup_origination = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_parameters: Dynamic<Bytes,width.Uint30>, circuits_info: Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq>,width.Uint30>, init_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30>, nb_ops: Int31 };
export type Proto016PtMumbaiOperationAlphaContents__Vdf_revelation = { solution: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Vdf_revelation_solution };
export type Proto016PtMumbaiOperationAlphaContents__Update_consensus_key = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Update_consensus_key_source, fee: N, counter: N, gas_limit: N, storage_limit: N, pk: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Update_consensus_key_pk };
export type Proto016PtMumbaiOperationAlphaContents__Tx_rollup_submit_batch = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_submit_batch_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id, content: Dynamic<U8String,width.Uint30>, burn_limit: Option<N> };
export type Proto016PtMumbaiOperationAlphaContents__Tx_rollup_return_bond = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_return_bond_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id };
export type Proto016PtMumbaiOperationAlphaContents__Tx_rollup_remove_commitment = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_remove_commitment_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id };
export type Proto016PtMumbaiOperationAlphaContents__Tx_rollup_rejection = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id, level: Int32, message: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message, message_position: N, message_path: Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq>,width.Uint30>, message_result_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_result_hash, message_result_path: Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq>,width.Uint30>, previous_message_result: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result, previous_message_result_path: Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq>,width.Uint30>, proof: Dynamic<U8String,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Tx_rollup_origination = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, tx_rollup_origination: Unit };
export type Proto016PtMumbaiOperationAlphaContents__Tx_rollup_finalize_commitment = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_finalize_commitment_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id };
export type Proto016PtMumbaiOperationAlphaContents__Tx_rollup_dispatch_tickets = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_source, fee: N, counter: N, gas_limit: N, storage_limit: N, tx_rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id, level: Int32, context_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash, message_index: Int31, message_result_path: Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq>,width.Uint30>, tickets_info: Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq>,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Tx_rollup_commit = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiTx_rollup_id, commitment: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment };
export type Proto016PtMumbaiOperationAlphaContents__Transfer_ticket = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transfer_ticket_source, fee: N, counter: N, gas_limit: N, storage_limit: N, ticket_contents: Dynamic<Bytes,width.Uint30>, ticket_ty: Dynamic<Bytes,width.Uint30>, ticket_ticketer: CGRIDClass__Proto016_PtMumbaiContract_id, ticket_amount: N, destination: CGRIDClass__Proto016_PtMumbaiContract_id, entrypoint: Dynamic<U8String,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Transaction = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transaction_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: N, destination: CGRIDClass__Proto016_PtMumbaiContract_id, parameters: Option<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transaction_parameters> };
export type Proto016PtMumbaiOperationAlphaContents__Smart_rollup_timeout = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address, stakers: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers };
export type Proto016PtMumbaiOperationAlphaContents__Smart_rollup_refute = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address, opponent: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_opponent, refutation: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation };
export type Proto016PtMumbaiOperationAlphaContents__Smart_rollup_recover_bond = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_rollup, staker: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_staker };
export type Proto016PtMumbaiOperationAlphaContents__Smart_rollup_publish = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address, commitment: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment };
export type Proto016PtMumbaiOperationAlphaContents__Smart_rollup_originate = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_originate_source, fee: N, counter: N, gas_limit: N, storage_limit: N, pvm_kind: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_originate_pvm_kind, kernel: Dynamic<U8String,width.Uint30>, origination_proof: Dynamic<U8String,width.Uint30>, parameters_ty: Dynamic<Bytes,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Smart_rollup_execute_outbox_message = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_execute_outbox_message_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address, cemented_commitment: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_execute_outbox_message_cemented_commitment, output_proof: Dynamic<U8String,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Smart_rollup_cement = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_cement_source, fee: N, counter: N, gas_limit: N, storage_limit: N, rollup: CGRIDClass__Proto016_PtMumbaiSmart_rollup_address, commitment: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_cement_commitment };
export type Proto016PtMumbaiOperationAlphaContents__Smart_rollup_add_messages = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_add_messages_source, fee: N, counter: N, gas_limit: N, storage_limit: N, message: Dynamic<Sequence<Dynamic<U8String,width.Uint30>>,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Set_deposits_limit = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Set_deposits_limit_source, fee: N, counter: N, gas_limit: N, storage_limit: N, limit: Option<N> };
export type Proto016PtMumbaiOperationAlphaContents__Seed_nonce_revelation = { level: Int32, nonce: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContents__Reveal = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Reveal_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_key: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Reveal_public_key };
export type Proto016PtMumbaiOperationAlphaContents__Register_global_constant = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Register_global_constant_source, fee: N, counter: N, gas_limit: N, storage_limit: N, value: Dynamic<Bytes,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Proposals = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Proposals_source, period: Int32, proposals: Dynamic<SequenceBounded<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq,20>,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Preendorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Preendorsement_block_payload_hash };
export type Proto016PtMumbaiOperationAlphaContents__Origination = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, balance: N, delegate: Option<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Origination_delegate>, script: CGRIDClass__Proto016_PtMumbaiScriptedContracts };
export type Proto016PtMumbaiOperationAlphaContents__Increase_paid_storage = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Increase_paid_storage_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: Z, destination: CGRIDClass__Proto016_PtMumbaiContract_idOriginated };
export type Proto016PtMumbaiOperationAlphaContents__Failing_noop = { arbitrary: Dynamic<U8String,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Endorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Endorsement_block_payload_hash };
export type Proto016PtMumbaiOperationAlphaContents__Drain_delegate = { consensus_key: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_consensus_key, delegate: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_delegate, destination: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_destination };
export type Proto016PtMumbaiOperationAlphaContents__Double_preendorsement_evidence = { op1: Dynamic<CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement,width.Uint30>, op2: Dynamic<CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Double_endorsement_evidence = { op1: Dynamic<CGRIDClass__Proto016_PtMumbaiInlinedEndorsement,width.Uint30>, op2: Dynamic<CGRIDClass__Proto016_PtMumbaiInlinedEndorsement,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Double_baking_evidence = { bh1: Dynamic<CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaFull_header,width.Uint30>, bh2: Dynamic<CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaFull_header,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContents__Delegation = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Delegation_source, fee: N, counter: N, gas_limit: N, storage_limit: N, delegate: Option<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Delegation_delegate> };
export type Proto016PtMumbaiOperationAlphaContents__Dal_publish_slot_header = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_source, fee: N, counter: N, gas_limit: N, storage_limit: N, slot_header: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_slot_header };
export type Proto016PtMumbaiOperationAlphaContents__Dal_attestation = { attestor: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_attestation_attestor, attestation: Z, level: Int32 };
export type Proto016PtMumbaiOperationAlphaContents__Ballot = { source: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Ballot_source, period: Int32, proposal: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Ballot_proposal, ballot: Int8 };
export type Proto016PtMumbaiOperationAlphaContents__Activate_account = { pkh: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Activate_account_pkh, secret: FixedBytes<20> };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some = { contents: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression, ty: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression, ticketer: CGRIDClass__Proto016_PtMumbaiContract_id };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None = Unit;
export type Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public = { _public: Unit };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private = { _private: Unit };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee = { fee: Unit };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Deposit = { deposit: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Batch = { batch: Dynamic<U8String,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3 = Int64;
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2 = Int32;
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1 = Uint16;
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0 = Uint8;
export type Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3 = Int64;
export type Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2 = Int32;
export type Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1 = Uint16;
export type Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0 = Uint8;
export type Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some = { commitment_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None = Unit;
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Start = { refutation_kind: Unit, player_commitment_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Start_player_commitment_hash, opponent_commitment_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Start_opponent_commitment_hash };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Move = { refutation_kind: Unit, choice: N, step: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof = { pvm_step: Dynamic<U8String,width.Uint30>, input_proof: Option<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection = Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq>,width.Uint30>;
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof = { input_proof_kind: Unit, reveal_proof: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof = { input_proof_kind: Unit, level: Int32, message_counter: N, serialized_proof: Dynamic<U8String,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input = { input_proof_kind: Unit };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof = { reveal_proof_kind: Unit, raw_data: Dynamic<U8String,width.Uint16> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof = { reveal_proof_kind: Unit };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof = { reveal_proof_kind: Unit, dal_page_id: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id, dal_proof: Dynamic<Bytes,width.Uint30> };
export type Proto016PtMumbaiInlinedPreendorsementContents__Preendorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto016_PtMumbaiInlinedPreendorsementContents_Preendorsement_block_payload_hash };
export type Proto016PtMumbaiInlinedEndorsementMempoolContents__Endorsement = { slot: Uint16, level: Int32, round: Int32, block_payload_hash: CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash };
export type Proto016PtMumbaiEntrypoint__set_delegate = Unit;
export type Proto016PtMumbaiEntrypoint__root = Unit;
export type Proto016PtMumbaiEntrypoint__remove_delegate = Unit;
export type Proto016PtMumbaiEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto016PtMumbaiEntrypoint__deposit = Unit;
export type Proto016PtMumbaiEntrypoint___do = Unit;
export type Proto016PtMumbaiEntrypoint___default = Unit;
export type Proto016PtMumbaiContractId__Originated = Padded<CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad,1>;
export type Proto016PtMumbaiContractId__Implicit = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiContractIdOriginated__Originated = Padded<CGRIDClass__Proto016_PtMumbaiContract_idOriginated_Originated_denest_pad,1>;
export type MichelineProto016PtMumbaiMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression>,width.Uint30>;
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression, arg2: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression, arg2: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, arg: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, arg: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Int = { int: Z };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
            case CGRIDTag__PublicKeyHash.Bls: return CGRIDClass__PublicKeyHash__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
            case CGRIDTag__PublicKey.Bls: return CGRIDClass__PublicKey__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiTx_rollup_id generated for Proto016PtMumbaiTxRollupId
export class CGRIDClass__Proto016_PtMumbaiTx_rollup_id extends Box<Proto016PtMumbaiTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiTx_rollup_id {
        return new this(record_decoder<Proto016PtMumbaiTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiSmart_rollup_address generated for Proto016PtMumbaiSmartRollupAddress
export class CGRIDClass__Proto016_PtMumbaiSmart_rollup_address extends Box<Proto016PtMumbaiSmartRollupAddress> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiSmart_rollup_address {
        return new this(record_decoder<Proto016PtMumbaiSmartRollupAddress>({smart_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['smart_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiScriptedContracts generated for Proto016PtMumbaiScriptedContracts
export class CGRIDClass__Proto016_PtMumbaiScriptedContracts extends Box<Proto016PtMumbaiScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiScriptedContracts {
        return new this(record_decoder<Proto016PtMumbaiScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_zk_rollup generated for Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateZkRollup
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_zk_rollup extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateZkRollup> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['zk_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_zk_rollup {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateZkRollup>({zk_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['zk_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.zk_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.zk_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1 generated for Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1 extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['new_state', 'fee']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1 {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1>({new_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30), fee: FixedBytes.decode<32>({len: 32})}, {order: ['new_state', 'fee']})(p));
    };
    get encodeLength(): number {
        return (this.value.new_state.encodeLength +  this.value.fee.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.new_state.writeTarget(tgt) +  this.value.fee.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq>(Dynamic.decode(U8String.decode, width.Uint30), CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1 generated for Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1 extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['new_state', 'fee', 'exit_validity']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1 {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1>({new_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30), fee: FixedBytes.decode<32>({len: 32}), exit_validity: Bool.decode}, {order: ['new_state', 'fee', 'exit_validity']})(p));
    };
    get encodeLength(): number {
        return (this.value.new_state.encodeLength +  this.value.fee.encodeLength +  this.value.exit_validity.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.new_state.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.exit_validity.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq>(Dynamic.decode(U8String.decode, width.Uint30), CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_fee_pi generated for Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdateFeePi
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_fee_pi extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdateFeePi> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['new_state']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_fee_pi {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdateFeePi>({new_state: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['new_state']})(p));
    };
    get encodeLength(): number {
        return (this.value.new_state.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.new_state.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update generated for Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdate
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pending_pis', 'private_pis', 'fee_pi', 'proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdate>({pending_pis: Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq.decode), width.Uint30), private_pis: Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq.decode), width.Uint30), fee_pi: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_fee_pi.decode, proof: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['pending_pis', 'private_pis', 'fee_pi', 'proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.pending_pis.encodeLength +  this.value.private_pis.encodeLength +  this.value.fee_pi.encodeLength +  this.value.proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pending_pis.writeTarget(tgt) +  this.value.private_pis.writeTarget(tgt) +  this.value.fee_pi.writeTarget(tgt) +  this.value.proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_source generated for Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_source extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_zk_rollup generated for Proto016PtMumbaiOperationAlphaContentsZkRollupPublishZkRollup
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_zk_rollup extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishZkRollup> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['zk_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_zk_rollup {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishZkRollup>({zk_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['zk_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.zk_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.zk_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_source generated for Proto016PtMumbaiOperationAlphaContentsZkRollupPublishSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_source extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index1 generated for Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1
export function proto016ptmumbaioperationalphacontentszkrolluppublishopdenestdyndenestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1,Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1.None: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1.Some: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1 => Object.values(CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index1 extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1>, Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationalphacontentszkrolluppublishopdenestdyndenestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id generated for Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['zk_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId>({zk_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['zk_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.zk_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.zk_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id generated for Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price generated for Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['id', 'amount']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price>({id: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id.decode, amount: Z.decode}, {order: ['id', 'amount']})(p));
    };
    get encodeLength(): number {
        return (this.value.id.encodeLength +  this.value.amount.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.id.writeTarget(tgt) +  this.value.amount.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst generated for Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0 generated for Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0 extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op_code', 'price', 'l1_dst', 'rollup_id', 'payload']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0 {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0>({op_code: Int31.decode, price: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price.decode, l1_dst: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst.decode, rollup_id: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id.decode, payload: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['op_code', 'price', 'l1_dst', 'rollup_id', 'payload']})(p));
    };
    get encodeLength(): number {
        return (this.value.op_code.encodeLength +  this.value.price.encodeLength +  this.value.l1_dst.encodeLength +  this.value.rollup_id.encodeLength +  this.value.payload.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op_code.writeTarget(tgt) +  this.value.price.writeTarget(tgt) +  this.value.l1_dst.writeTarget(tgt) +  this.value.rollup_id.writeTarget(tgt) +  this.value.payload.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq>(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0.decode, CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_source generated for Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_source extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1 generated for Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1
export function proto016ptmumbaioperationalphacontentszkrolluporiginationcircuitsinfodenestdyndenestseqindex1_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1,Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Public: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Private: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Fee: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1 => Object.values(CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1 extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1>, Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1 {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationalphacontentszkrolluporiginationcircuitsinfodenestdyndenestseqindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq>(Dynamic.decode(U8String.decode, width.Uint30), CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Vdf_revelation_solution generated for Proto016PtMumbaiOperationAlphaContentsVdfRevelationSolution
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Vdf_revelation_solution extends Box<Proto016PtMumbaiOperationAlphaContentsVdfRevelationSolution> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Vdf_revelation_solution {
        return new this(tuple_decoder<Proto016PtMumbaiOperationAlphaContentsVdfRevelationSolution>(FixedBytes.decode<100>({len: 100}), FixedBytes.decode<100>({len: 100}))(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Update_consensus_key_source generated for Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeySource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Update_consensus_key_source extends Box<Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeySource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Update_consensus_key_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeySource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Update_consensus_key_pk generated for Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeyPk
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Update_consensus_key_pk extends Box<Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeyPk> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Update_consensus_key_pk {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeyPk>({signature_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_submit_batch_source generated for Proto016PtMumbaiOperationAlphaContentsTxRollupSubmitBatchSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_submit_batch_source extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupSubmitBatchSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_submit_batch_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupSubmitBatchSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_return_bond_source generated for Proto016PtMumbaiOperationAlphaContentsTxRollupReturnBondSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_return_bond_source extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupReturnBondSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_return_bond_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupReturnBondSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_remove_commitment_source generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRemoveCommitmentSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_remove_commitment_source extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRemoveCommitmentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_remove_commitment_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRemoveCommitmentSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_source generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_source extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['withdraw_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash>({withdraw_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['withdraw_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.withdraw_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.withdraw_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResult
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResult> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash', 'withdraw_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResult>({context_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash.decode, withdraw_list_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash.decode}, {order: ['context_hash', 'withdraw_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength +  this.value.withdraw_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt) +  this.value.withdraw_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_result_hash generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultHash
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_result_hash extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_result_hash {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultHash>({message_result_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['inbox_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_path_denest_dyn_denest_seq {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq>({inbox_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['inbox_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.inbox_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.inbox_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount
export function proto016ptmumbaioperationalphacontentstxrolluprejectionmessagedepositdepositamount_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount,Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_0: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_1: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_2: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_3: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount => Object.values(CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount>, Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationalphacontentstxrolluprejectionmessagedepositdepositamount_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDeposit
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDeposit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['sender', 'destination', 'ticket_hash', 'amount']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDeposit>({sender: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender.decode, destination: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination.decode, ticket_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash.decode, amount: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount.decode}, {order: ['sender', 'destination', 'ticket_hash', 'amount']})(p));
    };
    get encodeLength(): number {
        return (this.value.sender.encodeLength +  this.value.destination.encodeLength +  this.value.ticket_hash.encodeLength +  this.value.amount.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.sender.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.ticket_hash.writeTarget(tgt) +  this.value.amount.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message generated for Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage
export function proto016ptmumbaioperationalphacontentstxrolluprejectionmessage_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage,Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage.Batch: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Batch.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage.Deposit: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Deposit.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage => Object.values(CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage>, Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationalphacontentstxrolluprejectionmessage_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_origination_source generated for Proto016PtMumbaiOperationAlphaContentsTxRollupOriginationSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_origination_source extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_origination_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupOriginationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_finalize_commitment_source generated for Proto016PtMumbaiOperationAlphaContentsTxRollupFinalizeCommitmentSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_finalize_commitment_source extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupFinalizeCommitmentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_finalize_commitment_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupFinalizeCommitmentSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer generated for Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount generated for Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount
export function proto016ptmumbaioperationalphacontentstxrollupdispatchticketsticketsinfodenestdyndenestseqamount_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount,Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_0: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_1: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_2: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_3: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount => Object.values(CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount>, Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationalphacontentstxrollupdispatchticketsticketsinfodenestdyndenestseqamount_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contents', 'ty', 'ticketer', 'amount', 'claimer']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq>({contents: Dynamic.decode(Bytes.decode, width.Uint30), ty: Dynamic.decode(Bytes.decode, width.Uint30), ticketer: CGRIDClass__Proto016_PtMumbaiContract_id.decode, amount: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount.decode, claimer: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer.decode}, {order: ['contents', 'ty', 'ticketer', 'amount', 'claimer']})(p));
    };
    get encodeLength(): number {
        return (this.value.contents.encodeLength +  this.value.ty.encodeLength +  this.value.ticketer.encodeLength +  this.value.amount.encodeLength +  this.value.claimer.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contents.writeTarget(tgt) +  this.value.ty.writeTarget(tgt) +  this.value.ticketer.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.claimer.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_source generated for Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_source extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_message_result_path_denest_dyn_denest_seq {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq>({message_result_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash generated for Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsContextHash
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsContextHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_context_hash {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsContextHash>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_source generated for Proto016PtMumbaiOperationAlphaContentsTxRollupCommitSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_source extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_predecessor generated for Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor
export function proto016ptmumbaioperationalphacontentstxrollupcommitcommitmentpredecessor_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor,Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor.None: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor.Some: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor => Object.values(CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_predecessor extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor>, Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_predecessor {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationalphacontentstxrollupcommitcommitmentpredecessor_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['message_result_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq>({message_result_hash: FixedBytes.decode<32>({len: 32})}, {order: ['message_result_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.message_result_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.message_result_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root generated for Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['inbox_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot>({inbox_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['inbox_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.inbox_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.inbox_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment generated for Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitment
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment extends Box<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'messages', 'predecessor', 'inbox_merkle_root']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitment>({level: Int32.decode, messages: Dynamic.decode(Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq.decode), width.Uint30), predecessor: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_predecessor.decode, inbox_merkle_root: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root.decode}, {order: ['level', 'messages', 'predecessor', 'inbox_merkle_root']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.messages.encodeLength +  this.value.predecessor.encodeLength +  this.value.inbox_merkle_root.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.messages.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.inbox_merkle_root.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transfer_ticket_source generated for Proto016PtMumbaiOperationAlphaContentsTransferTicketSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transfer_ticket_source extends Box<Proto016PtMumbaiOperationAlphaContentsTransferTicketSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transfer_ticket_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTransferTicketSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transaction_source generated for Proto016PtMumbaiOperationAlphaContentsTransactionSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transaction_source extends Box<Proto016PtMumbaiOperationAlphaContentsTransactionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transaction_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTransactionSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transaction_parameters generated for Proto016PtMumbaiOperationAlphaContentsTransactionParameters
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transaction_parameters extends Box<Proto016PtMumbaiOperationAlphaContentsTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Transaction_parameters {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsTransactionParameters>({entrypoint: CGRIDClass__Proto016_PtMumbaiEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers_bob generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersBob
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers_bob extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersBob> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers_bob {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersBob>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers_alice generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersAlice
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers_alice extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersAlice> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers_alice {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersAlice>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakers
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakers> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['alice', 'bob']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakers>({alice: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers_alice.decode, bob: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers_bob.decode}, {order: ['alice', 'bob']})(p));
    };
    get encodeLength(): number {
        return (this.value.alice.encodeLength +  this.value.bob.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.alice.writeTarget(tgt) +  this.value.bob.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_source generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_source extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_source generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_source extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Start_player_commitment_hash generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Start_player_commitment_hash extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Start_player_commitment_hash {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Start_opponent_commitment_hash generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Start_opponent_commitment_hash extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Start_opponent_commitment_hash {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['published_level', 'slot_index', 'page_index']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof_dal_page_proof_dal_page_id {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId>({published_level: Int32.decode, slot_index: Uint8.decode, page_index: Int16.decode}, {order: ['published_level', 'slot_index', 'page_index']})(p));
    };
    get encodeLength(): number {
        return (this.value.published_level.encodeLength +  this.value.slot_index.encodeLength +  this.value.page_index.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.published_level.writeTarget(tgt) +  this.value.slot_index.writeTarget(tgt) +  this.value.page_index.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof
export function proto016ptmumbaioperationalphacontentssmartrolluprefuterefutationmovestepproofinputproofrevealproofrevealproof_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof,Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.raw_data_proof: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.metadata_proof: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.dal_page_proof: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof => Object.values(CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof>, Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof_reveal_proof_reveal_proof {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationalphacontentssmartrolluprefuterefutationmovestepproofinputproofrevealproofrevealproof_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof
export function proto016ptmumbaioperationalphacontentssmartrolluprefuterefutationmovestepproofinputproof_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof,Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.inbox_proof: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.reveal_proof: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.first_input: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof => Object.values(CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof>, Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Proof_input_proof {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationalphacontentssmartrolluprefuterefutationmovestepproofinputproof_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_state_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState>({smart_rollup_state_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_state_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_state_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_state_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['state', 'tick']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq>({state: Option.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state.decode), tick: N.decode}, {order: ['state', 'tick']})(p));
    };
    get encodeLength(): number {
        return (this.value.state.encodeLength +  this.value.tick.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.state.writeTarget(tgt) +  this.value.tick.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep
export function proto016ptmumbaioperationalphacontentssmartrolluprefuterefutationmovestep_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep,Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep.Dissection: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep.Proof: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep => Object.values(CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep>, Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationalphacontentssmartrolluprefuterefutationmovestep_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation
export function proto016ptmumbaioperationalphacontentssmartrolluprefuterefutation_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation,Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation.Start: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Start.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation.Move: return CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Move.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation => Object.values(CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation>, Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationalphacontentssmartrolluprefuterefutation_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_opponent generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteOpponent
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_opponent extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteOpponent> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_opponent {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteOpponent>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_staker generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondStaker
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_staker extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondStaker> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_staker {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondStaker>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_source generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_source extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_rollup generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondRollup
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_rollup extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondRollup> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_recover_bond_rollup {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondRollup>({smart_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['smart_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_source generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_source extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment_predecessor generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentPredecessor
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment_predecessor extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment_predecessor {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentPredecessor>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment_compressed_state generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentCompressedState
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment_compressed_state extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentCompressedState> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_state_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment_compressed_state {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentCompressedState>({smart_rollup_state_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_state_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_state_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_state_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitment
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['compressed_state', 'inbox_level', 'predecessor', 'number_of_ticks']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitment>({compressed_state: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment_compressed_state.decode, inbox_level: Int32.decode, predecessor: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment_predecessor.decode, number_of_ticks: Int64.decode}, {order: ['compressed_state', 'inbox_level', 'predecessor', 'number_of_ticks']})(p));
    };
    get encodeLength(): number {
        return (this.value.compressed_state.encodeLength +  this.value.inbox_level.encodeLength +  this.value.predecessor.encodeLength +  this.value.number_of_ticks.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.compressed_state.writeTarget(tgt) +  this.value.inbox_level.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.number_of_ticks.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_originate_source generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginateSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_originate_source extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginateSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_originate_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginateSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_originate_pvm_kind generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginatePvmKind
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_originate_pvm_kind extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginatePvmKind> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginatePvmKind>(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_originate_pvm_kind {
        return new this(enum_decoder(width.Uint8)((x): x is Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginatePvmKind => (Object.values(Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginatePvmKind).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_execute_outbox_message_source generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_execute_outbox_message_source extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_execute_outbox_message_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_execute_outbox_message_cemented_commitment generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_execute_outbox_message_cemented_commitment extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_execute_outbox_message_cemented_commitment {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_cement_source generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupCementSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_cement_source extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupCementSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_cement_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupCementSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_cement_commitment generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupCementCommitment
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_cement_commitment extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupCementCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_commitment_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_cement_commitment {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupCementCommitment>({smart_rollup_commitment_hash: FixedBytes.decode<32>({len: 32})}, {order: ['smart_rollup_commitment_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_commitment_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_commitment_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_add_messages_source generated for Proto016PtMumbaiOperationAlphaContentsSmartRollupAddMessagesSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_add_messages_source extends Box<Proto016PtMumbaiOperationAlphaContentsSmartRollupAddMessagesSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_add_messages_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSmartRollupAddMessagesSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Set_deposits_limit_source generated for Proto016PtMumbaiOperationAlphaContentsSetDepositsLimitSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Set_deposits_limit_source extends Box<Proto016PtMumbaiOperationAlphaContentsSetDepositsLimitSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Set_deposits_limit_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsSetDepositsLimitSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Reveal_source generated for Proto016PtMumbaiOperationAlphaContentsRevealSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Reveal_source extends Box<Proto016PtMumbaiOperationAlphaContentsRevealSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Reveal_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsRevealSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Reveal_public_key generated for Proto016PtMumbaiOperationAlphaContentsRevealPublicKey
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Reveal_public_key extends Box<Proto016PtMumbaiOperationAlphaContentsRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Reveal_public_key {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsRevealPublicKey>({signature_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Register_global_constant_source generated for Proto016PtMumbaiOperationAlphaContentsRegisterGlobalConstantSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Register_global_constant_source extends Box<Proto016PtMumbaiOperationAlphaContentsRegisterGlobalConstantSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Register_global_constant_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsRegisterGlobalConstantSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Proposals_source generated for Proto016PtMumbaiOperationAlphaContentsProposalsSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Proposals_source extends Box<Proto016PtMumbaiOperationAlphaContentsProposalsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Proposals_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsProposalsSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq generated for Proto016PtMumbaiOperationAlphaContentsProposalsProposalsDenestDynDenestSeq
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq extends Box<Proto016PtMumbaiOperationAlphaContentsProposalsProposalsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Preendorsement_block_payload_hash generated for Proto016PtMumbaiOperationAlphaContentsPreendorsementBlockPayloadHash
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Preendorsement_block_payload_hash extends Box<Proto016PtMumbaiOperationAlphaContentsPreendorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Preendorsement_block_payload_hash {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsPreendorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Origination_source generated for Proto016PtMumbaiOperationAlphaContentsOriginationSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Origination_source extends Box<Proto016PtMumbaiOperationAlphaContentsOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Origination_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsOriginationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Origination_delegate generated for Proto016PtMumbaiOperationAlphaContentsOriginationDelegate
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Origination_delegate extends Box<Proto016PtMumbaiOperationAlphaContentsOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Origination_delegate {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsOriginationDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Increase_paid_storage_source generated for Proto016PtMumbaiOperationAlphaContentsIncreasePaidStorageSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Increase_paid_storage_source extends Box<Proto016PtMumbaiOperationAlphaContentsIncreasePaidStorageSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Increase_paid_storage_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsIncreasePaidStorageSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Endorsement_block_payload_hash generated for Proto016PtMumbaiOperationAlphaContentsEndorsementBlockPayloadHash
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Endorsement_block_payload_hash extends Box<Proto016PtMumbaiOperationAlphaContentsEndorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Endorsement_block_payload_hash {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsEndorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_destination generated for Proto016PtMumbaiOperationAlphaContentsDrainDelegateDestination
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_destination extends Box<Proto016PtMumbaiOperationAlphaContentsDrainDelegateDestination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_destination {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsDrainDelegateDestination>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_delegate generated for Proto016PtMumbaiOperationAlphaContentsDrainDelegateDelegate
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_delegate extends Box<Proto016PtMumbaiOperationAlphaContentsDrainDelegateDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_delegate {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsDrainDelegateDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_consensus_key generated for Proto016PtMumbaiOperationAlphaContentsDrainDelegateConsensusKey
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_consensus_key extends Box<Proto016PtMumbaiOperationAlphaContentsDrainDelegateConsensusKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Drain_delegate_consensus_key {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsDrainDelegateConsensusKey>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Delegation_source generated for Proto016PtMumbaiOperationAlphaContentsDelegationSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Delegation_source extends Box<Proto016PtMumbaiOperationAlphaContentsDelegationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Delegation_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsDelegationSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Delegation_delegate generated for Proto016PtMumbaiOperationAlphaContentsDelegationDelegate
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Delegation_delegate extends Box<Proto016PtMumbaiOperationAlphaContentsDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Delegation_delegate {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsDelegationDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_source generated for Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_source extends Box<Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_slot_header_commitment generated for Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_slot_header_commitment extends Box<Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['dal_commitment']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_slot_header_commitment {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment>({dal_commitment: FixedBytes.decode<48>({len: 48})}, {order: ['dal_commitment']})(p));
    };
    get encodeLength(): number {
        return (this.value.dal_commitment.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.dal_commitment.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_slot_header generated for Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeader
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_slot_header extends Box<Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'index', 'commitment', 'commitment_proof']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_slot_header {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeader>({level: Int32.decode, index: Uint8.decode, commitment: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_slot_header_commitment.decode, commitment_proof: FixedBytes.decode<48>({len: 48})}, {order: ['level', 'index', 'commitment', 'commitment_proof']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.index.encodeLength +  this.value.commitment.encodeLength +  this.value.commitment_proof.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.index.writeTarget(tgt) +  this.value.commitment.writeTarget(tgt) +  this.value.commitment_proof.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_attestation_attestor generated for Proto016PtMumbaiOperationAlphaContentsDalAttestationAttestor
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_attestation_attestor extends Box<Proto016PtMumbaiOperationAlphaContentsDalAttestationAttestor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_attestation_attestor {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsDalAttestationAttestor>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Ballot_source generated for Proto016PtMumbaiOperationAlphaContentsBallotSource
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Ballot_source extends Box<Proto016PtMumbaiOperationAlphaContentsBallotSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Ballot_source {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsBallotSource>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Ballot_proposal generated for Proto016PtMumbaiOperationAlphaContentsBallotProposal
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Ballot_proposal extends Box<Proto016PtMumbaiOperationAlphaContentsBallotProposal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Ballot_proposal {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsBallotProposal>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Activate_account_pkh generated for Proto016PtMumbaiOperationAlphaContentsActivateAccountPkh
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Activate_account_pkh extends Box<Proto016PtMumbaiOperationAlphaContentsActivateAccountPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Activate_account_pkh {
        return new this(record_decoder<Proto016PtMumbaiOperationAlphaContentsActivateAccountPkh>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents generated for Proto016PtMumbaiOperationAlphaContents
export function proto016ptmumbaioperationalphacontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiOperationAlphaContents,Proto016PtMumbaiOperationAlphaContents> {
    function f(disc: CGRIDTag__Proto016PtMumbaiOperationAlphaContents) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Seed_nonce_revelation: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Seed_nonce_revelation.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Double_endorsement_evidence: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_endorsement_evidence.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Double_baking_evidence: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_baking_evidence.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Activate_account: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Activate_account.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Proposals: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Proposals.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Ballot: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Ballot.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Double_preendorsement_evidence: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_preendorsement_evidence.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Vdf_revelation: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Vdf_revelation.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Drain_delegate: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Drain_delegate.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Failing_noop: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Failing_noop.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Preendorsement: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Preendorsement.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Endorsement: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Endorsement.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Dal_attestation: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Dal_attestation.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Reveal: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Reveal.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Transaction: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Transaction.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Origination: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Origination.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Delegation: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Delegation.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Register_global_constant: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Register_global_constant.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Set_deposits_limit: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Set_deposits_limit.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Increase_paid_storage: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Increase_paid_storage.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Update_consensus_key: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Update_consensus_key.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_origination: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_origination.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_submit_batch: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_submit_batch.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_commit: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_commit.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_return_bond: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_return_bond.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_finalize_commitment: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_finalize_commitment.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_remove_commitment: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_remove_commitment.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_rejection: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_rejection.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_dispatch_tickets: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_dispatch_tickets.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Transfer_ticket: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Transfer_ticket.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_originate: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_originate.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_add_messages: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_add_messages.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_cement: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_cement.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_publish: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_publish.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_refute: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_refute.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_timeout: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_timeout.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_execute_outbox_message: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_execute_outbox_message.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_recover_bond: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_recover_bond.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Dal_publish_slot_header: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Dal_publish_slot_header.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Zk_rollup_origination: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_origination.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Zk_rollup_publish: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_publish.decode;
            case CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Zk_rollup_update: return CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_update.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiOperationAlphaContents => Object.values(CGRIDTag__Proto016PtMumbaiOperationAlphaContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiOperationAlphaContents extends Box<Proto016PtMumbaiOperationAlphaContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiOperationAlphaContents>, Proto016PtMumbaiOperationAlphaContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiOperationAlphaContents {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaioperationalphacontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives generated for Proto016PtMumbaiMichelsonV1Primitives
export class CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives extends Box<Proto016PtMumbaiMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<Proto016PtMumbaiMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is Proto016PtMumbaiMichelsonV1Primitives => (Object.values(Proto016PtMumbaiMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement_signature generated for Proto016PtMumbaiInlinedPreendorsementSignature
export class CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement_signature extends Box<Proto016PtMumbaiInlinedPreendorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement_signature {
        return new this(record_decoder<Proto016PtMumbaiInlinedPreendorsementSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiInlinedPreendorsementContents_Preendorsement_block_payload_hash generated for Proto016PtMumbaiInlinedPreendorsementContentsPreendorsementBlockPayloadHash
export class CGRIDClass__Proto016_PtMumbaiInlinedPreendorsementContents_Preendorsement_block_payload_hash extends Box<Proto016PtMumbaiInlinedPreendorsementContentsPreendorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiInlinedPreendorsementContents_Preendorsement_block_payload_hash {
        return new this(record_decoder<Proto016PtMumbaiInlinedPreendorsementContentsPreendorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiInlinedPreendorsementContents generated for Proto016PtMumbaiInlinedPreendorsementContents
export function proto016ptmumbaiinlinedpreendorsementcontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiInlinedPreendorsementContents,Proto016PtMumbaiInlinedPreendorsementContents> {
    function f(disc: CGRIDTag__Proto016PtMumbaiInlinedPreendorsementContents) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiInlinedPreendorsementContents.Preendorsement: return CGRIDClass__Proto016PtMumbaiInlinedPreendorsementContents__Preendorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiInlinedPreendorsementContents => Object.values(CGRIDTag__Proto016PtMumbaiInlinedPreendorsementContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiInlinedPreendorsementContents extends Box<Proto016PtMumbaiInlinedPreendorsementContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiInlinedPreendorsementContents>, Proto016PtMumbaiInlinedPreendorsementContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiInlinedPreendorsementContents {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaiinlinedpreendorsementcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement generated for Proto016PtMumbaiInlinedPreendorsement
export class CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement extends Box<Proto016PtMumbaiInlinedPreendorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement {
        return new this(record_decoder<Proto016PtMumbaiInlinedPreendorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__Proto016_PtMumbaiInlinedPreendorsementContents.decode, signature: Nullable.decode(CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_signature generated for Proto016PtMumbaiInlinedEndorsementSignature
export class CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_signature extends Box<Proto016PtMumbaiInlinedEndorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_signature {
        return new this(record_decoder<Proto016PtMumbaiInlinedEndorsementSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash generated for Proto016PtMumbaiInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash
export class CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash extends Box<Proto016PtMumbaiInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_mempoolContents_Endorsement_block_payload_hash {
        return new this(record_decoder<Proto016PtMumbaiInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_mempoolContents generated for Proto016PtMumbaiInlinedEndorsementMempoolContents
export function proto016ptmumbaiinlinedendorsementmempoolcontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiInlinedEndorsementMempoolContents,Proto016PtMumbaiInlinedEndorsementMempoolContents> {
    function f(disc: CGRIDTag__Proto016PtMumbaiInlinedEndorsementMempoolContents) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiInlinedEndorsementMempoolContents.Endorsement: return CGRIDClass__Proto016PtMumbaiInlinedEndorsementMempoolContents__Endorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiInlinedEndorsementMempoolContents => Object.values(CGRIDTag__Proto016PtMumbaiInlinedEndorsementMempoolContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_mempoolContents extends Box<Proto016PtMumbaiInlinedEndorsementMempoolContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiInlinedEndorsementMempoolContents>, Proto016PtMumbaiInlinedEndorsementMempoolContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_mempoolContents {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaiinlinedendorsementmempoolcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiInlinedEndorsement generated for Proto016PtMumbaiInlinedEndorsement
export class CGRIDClass__Proto016_PtMumbaiInlinedEndorsement extends Box<Proto016PtMumbaiInlinedEndorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiInlinedEndorsement {
        return new this(record_decoder<Proto016PtMumbaiInlinedEndorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_mempoolContents.decode, signature: Nullable.decode(CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiEntrypoint generated for Proto016PtMumbaiEntrypoint
export function proto016ptmumbaientrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiEntrypoint,Proto016PtMumbaiEntrypoint> {
    function f(disc: CGRIDTag__Proto016PtMumbaiEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiEntrypoint._default: return CGRIDClass__Proto016PtMumbaiEntrypoint___default.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint.root: return CGRIDClass__Proto016PtMumbaiEntrypoint__root.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint._do: return CGRIDClass__Proto016PtMumbaiEntrypoint___do.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint.set_delegate: return CGRIDClass__Proto016PtMumbaiEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint.remove_delegate: return CGRIDClass__Proto016PtMumbaiEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint.deposit: return CGRIDClass__Proto016PtMumbaiEntrypoint__deposit.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint.named: return CGRIDClass__Proto016PtMumbaiEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiEntrypoint => Object.values(CGRIDTag__Proto016PtMumbaiEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiEntrypoint extends Box<Proto016PtMumbaiEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiEntrypoint>, Proto016PtMumbaiEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaientrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad generated for Proto016PtMumbaiContractIdOriginatedDenestPad
export class CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad extends Box<Proto016PtMumbaiContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto016PtMumbaiContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiContract_idOriginated_Originated_denest_pad generated for Proto016PtMumbaiContractIdOriginatedOriginatedDenestPad
export class CGRIDClass__Proto016_PtMumbaiContract_idOriginated_Originated_denest_pad extends Box<Proto016PtMumbaiContractIdOriginatedOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiContract_idOriginated_Originated_denest_pad {
        return new this(record_decoder<Proto016PtMumbaiContractIdOriginatedOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiContract_idOriginated generated for Proto016PtMumbaiContractIdOriginated
export function proto016ptmumbaicontractidoriginated_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiContractIdOriginated,Proto016PtMumbaiContractIdOriginated> {
    function f(disc: CGRIDTag__Proto016PtMumbaiContractIdOriginated) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiContractIdOriginated.Originated: return CGRIDClass__Proto016PtMumbaiContractIdOriginated__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiContractIdOriginated => Object.values(CGRIDTag__Proto016PtMumbaiContractIdOriginated).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiContract_idOriginated extends Box<Proto016PtMumbaiContractIdOriginated> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiContractIdOriginated>, Proto016PtMumbaiContractIdOriginated>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiContract_idOriginated {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaicontractidoriginated_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiContract_id generated for Proto016PtMumbaiContractId
export function proto016ptmumbaicontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiContractId,Proto016PtMumbaiContractId> {
    function f(disc: CGRIDTag__Proto016PtMumbaiContractId) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiContractId.Implicit: return CGRIDClass__Proto016PtMumbaiContractId__Implicit.decode;
            case CGRIDTag__Proto016PtMumbaiContractId.Originated: return CGRIDClass__Proto016PtMumbaiContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiContractId => Object.values(CGRIDTag__Proto016PtMumbaiContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiContract_id extends Box<Proto016PtMumbaiContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiContractId>, Proto016PtMumbaiContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiContract_id {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaicontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash generated for Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash extends Box<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaSigned_contents_signature generated for Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaSigned_contents_signature extends Box<Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaFull_header generated for Proto016PtMumbaiBlockHeaderAlphaFullHeader
export class CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaFull_header extends Box<Proto016PtMumbaiBlockHeaderAlphaFullHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaFull_header {
        return new this(record_decoder<Proto016PtMumbaiBlockHeaderAlphaFullHeader>({level: Int32.decode, proto: Uint8.decode, predecessor: CGRIDClass__Block_headerShell_predecessor.decode, timestamp: Int64.decode, validation_pass: Uint8.decode, operations_hash: CGRIDClass__Block_headerShell_operations_hash.decode, fitness: Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30), context: CGRIDClass__Block_headerShell_context.decode, payload_hash: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaSigned_contents_signature.decode}, {order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.proto.encodeLength +  this.value.predecessor.encodeLength +  this.value.timestamp.encodeLength +  this.value.validation_pass.encodeLength +  this.value.operations_hash.encodeLength +  this.value.fitness.encodeLength +  this.value.context.encodeLength +  this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_toggle_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.proto.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.timestamp.writeTarget(tgt) +  this.value.validation_pass.writeTarget(tgt) +  this.value.operations_hash.writeTarget(tgt) +  this.value.fitness.writeTarget(tgt) +  this.value.context.writeTarget(tgt) +  this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_toggle_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression generated for MichelineProto016PtMumbaiMichelsonV1Expression
export function michelineproto016ptmumbaimichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression,MichelineProto016PtMumbaiMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Int: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.String: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Sequence: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Bytes: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression => Object.values(CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression extends Box<MichelineProto016PtMumbaiMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineProto016PtMumbaiMichelsonV1Expression>, MichelineProto016PtMumbaiMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelineproto016ptmumbaimichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_predecessor generated for BlockHeaderShellPredecessor
export class CGRIDClass__Block_headerShell_predecessor extends Box<BlockHeaderShellPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_predecessor {
        return new this(record_decoder<BlockHeaderShellPredecessor>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_operations_hash generated for BlockHeaderShellOperationsHash
export class CGRIDClass__Block_headerShell_operations_hash extends Box<BlockHeaderShellOperationsHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_list_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_operations_hash {
        return new this(record_decoder<BlockHeaderShellOperationsHash>({operation_list_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_list_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_list_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_list_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_context generated for BlockHeaderShellContext
export class CGRIDClass__Block_headerShell_context extends Box<BlockHeaderShellContext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_context {
        return new this(record_decoder<BlockHeaderShellContext>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256,
    Bls: CGRIDClass__PublicKeyHash__Bls
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] } | { kind: CGRIDTag__PublicKeyHash.Bls, value: CGRIDMap__PublicKeyHash['Bls'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256,
    Bls: CGRIDClass__PublicKey__Bls
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] } | { kind: CGRIDTag__PublicKey.Bls, value: CGRIDMap__PublicKey['Bls'] };
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export enum CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression {
    Int: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Bytes
}
export type MichelineProto016PtMumbaiMichelsonV1Expression = { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Int, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.String, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Bytes'] };
export type BlockHeaderShellPredecessor = { block_hash: FixedBytes<32> };
export type BlockHeaderShellOperationsHash = { operation_list_list_hash: FixedBytes<32> };
export type BlockHeaderShellContext = { context_hash: FixedBytes<32> };
export type Proto016PtMumbaiTxRollupId = { rollup_hash: FixedBytes<20> };
export type Proto016PtMumbaiSmartRollupAddress = { smart_rollup_hash: FixedBytes<20> };
export type Proto016PtMumbaiScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateZkRollup = { zk_rollup_hash: FixedBytes<20> };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeqIndex1 = { new_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30>, fee: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePrivatePisDenestDynDenestSeq = [Dynamic<U8String,width.Uint30>, CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq_index1];
export type Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeqIndex1 = { new_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30>, fee: FixedBytes<32>, exit_validity: Bool };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdatePendingPisDenestDynDenestSeq = [Dynamic<U8String,width.Uint30>, CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq_index1];
export type Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdateFeePi = { new_state: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateUpdate = { pending_pis: Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_pending_pis_denest_dyn_denest_seq>,width.Uint30>, private_pis: Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_private_pis_denest_dyn_denest_seq>,width.Uint30>, fee_pi: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_update_update_fee_pi, proof: Dynamic<Bytes,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupUpdateSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupPublishZkRollup = { zk_rollup_hash: FixedBytes<20> };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupPublishSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1{
    None = 0,
    Some = 1
}
export interface CGRIDMap__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1 {
    None: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__None,
    Some: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1__Some
}
export type Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1 = { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1.None, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1['None'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1.Some, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex1['Some'] };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0RollupId = { zk_rollup_hash: FixedBytes<20> };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0PriceId = { script_expr: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0Price = { id: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price_id, amount: Z };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0Index0L1Dst = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeqIndex0 = { op_code: Int31, price: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_price, l1_dst: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_l1_dst, rollup_id: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0_index0_rollup_id, payload: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupPublishOpDenestDynDenestSeq = [CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index0, CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_publish_op_denest_dyn_denest_seq_index1];
export type Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1{
    Public = 0,
    Private = 1,
    Fee = 2
}
export interface CGRIDMap__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1 {
    Public: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Public,
    Private: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Private,
    Fee: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1__Fee
}
export type Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1 = { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Public, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1['Public'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Private, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1['Private'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1.Fee, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeqIndex1['Fee'] };
export type Proto016PtMumbaiOperationAlphaContentsZkRollupOriginationCircuitsInfoDenestDynDenestSeq = [Dynamic<U8String,width.Uint30>, CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Zk_rollup_origination_circuits_info_denest_dyn_denest_seq_index1];
export type Proto016PtMumbaiOperationAlphaContentsVdfRevelationSolution = [FixedBytes<100>, FixedBytes<100>];
export type Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeySource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsUpdateConsensusKeyPk = { signature_public_key: CGRIDClass__Public_key };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupSubmitBatchSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupReturnBondSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRemoveCommitmentSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultWithdrawListHash = { withdraw_list_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResultContextHash = { context_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionPreviousMessageResult = { context_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_context_hash, withdraw_list_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_previous_message_result_withdraw_list_hash };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageResultHash = { message_result_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessagePathDenestDynDenestSeq = { inbox_list_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositTicketHash = { script_expr: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositSender = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositDestination = { bls12_381_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3
}
export interface CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount {
    case_0: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_0,
    case_1: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_1,
    case_2: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_2,
    case_3: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount__case_3
}
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount = { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_0, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_0'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_1, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_1'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_2, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_2'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount.case_3, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDepositAmount['case_3'] };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessageDepositDeposit = { sender: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_sender, destination: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_destination, ticket_hash: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_ticket_hash, amount: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_rejection_message_Deposit_deposit_amount };
export enum CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage{
    Batch = 0,
    Deposit = 1
}
export interface CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage {
    Batch: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Batch,
    Deposit: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage__Deposit
}
export type Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage = { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage.Batch, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage['Batch'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage.Deposit, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupRejectionMessage['Deposit'] };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupOriginationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupFinalizeCommitmentSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqClaimer = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount{
    case_0 = 0,
    case_1 = 1,
    case_2 = 2,
    case_3 = 3
}
export interface CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount {
    case_0: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_0,
    case_1: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_1,
    case_2: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_2,
    case_3: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount__case_3
}
export type Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount = { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_0, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_0'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_1, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_1'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_2, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_2'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount.case_3, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeqAmount['case_3'] };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsTicketsInfoDenestDynDenestSeq = { contents: Dynamic<Bytes,width.Uint30>, ty: Dynamic<Bytes,width.Uint30>, ticketer: CGRIDClass__Proto016_PtMumbaiContract_id, amount: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_amount, claimer: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_dispatch_tickets_tickets_info_denest_dyn_denest_seq_claimer };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsMessageResultPathDenestDynDenestSeq = { message_result_list_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupDispatchTicketsContextHash = { context_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupCommitSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor{
    None = 0,
    Some = 1
}
export interface CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor {
    None: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__None,
    Some: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor__Some
}
export type Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor = { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor.None, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor['None'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor.Some, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentPredecessor['Some'] };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentMessagesDenestDynDenestSeq = { message_result_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitmentInboxMerkleRoot = { inbox_list_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsTxRollupCommitCommitment = { level: Int32, messages: Dynamic<Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_messages_denest_dyn_denest_seq>,width.Uint30>, predecessor: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_predecessor, inbox_merkle_root: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Tx_rollup_commit_commitment_inbox_merkle_root };
export type Proto016PtMumbaiOperationAlphaContentsTransferTicketSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsTransactionSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsTransactionParameters = { entrypoint: CGRIDClass__Proto016_PtMumbaiEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersBob = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakersAlice = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutStakers = { alice: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers_alice, bob: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_timeout_stakers_bob };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupTimeoutSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartPlayerCommitmentHash = { smart_rollup_commitment_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationStartOpponentCommitmentHash = { smart_rollup_commitment_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProofDalPageProofDalPageId = { published_level: Int32, slot_index: Uint8, page_index: Int16 };
export enum CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof{
    raw_data_proof = 0,
    metadata_proof = 1,
    dal_page_proof = 2
}
export interface CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof {
    raw_data_proof: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__raw_data_proof,
    metadata_proof: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__metadata_proof,
    dal_page_proof: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof__dal_page_proof
}
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof = { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.raw_data_proof, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof['raw_data_proof'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.metadata_proof, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof['metadata_proof'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof.dal_page_proof, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProofRevealProofRevealProof['dal_page_proof'] };
export enum CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof{
    inbox_proof = 0,
    reveal_proof = 1,
    first_input = 2
}
export interface CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof {
    inbox_proof: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__inbox_proof,
    reveal_proof: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__reveal_proof,
    first_input: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof__first_input
}
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof = { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.inbox_proof, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof['inbox_proof'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.reveal_proof, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof['reveal_proof'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof.first_input, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepProofInputProof['first_input'] };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeqState = { smart_rollup_state_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStepDissectionDenestDynDenestSeq = { state: Option<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_refute_refutation_Move_step_Dissection_denest_dyn_denest_seq_state>, tick: N };
export enum CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep{
    Dissection = 0,
    Proof = 1
}
export interface CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep {
    Dissection: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Dissection,
    Proof: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep__Proof
}
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep = { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep.Dissection, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep['Dissection'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep.Proof, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutationMoveStep['Proof'] };
export enum CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation{
    Start = 0,
    Move = 1
}
export interface CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation {
    Start: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Start,
    Move: CGRIDClass__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation__Move
}
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation = { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation.Start, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation['Start'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation.Move, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteRefutation['Move'] };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRefuteOpponent = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondStaker = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupRecoverBondRollup = { smart_rollup_hash: FixedBytes<20> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentPredecessor = { smart_rollup_commitment_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitmentCompressedState = { smart_rollup_state_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupPublishCommitment = { compressed_state: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment_compressed_state, inbox_level: Int32, predecessor: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Smart_rollup_publish_commitment_predecessor, number_of_ticks: Int64 };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginateSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum Proto016PtMumbaiOperationAlphaContentsSmartRollupOriginatePvmKind{
    arith = 0,
    wasm_2_0_0 = 1
}
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupExecuteOutboxMessageCementedCommitment = { smart_rollup_commitment_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupCementSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupCementCommitment = { smart_rollup_commitment_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsSmartRollupAddMessagesSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsSetDepositsLimitSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsRevealSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsRevealPublicKey = { signature_public_key: CGRIDClass__Public_key };
export type Proto016PtMumbaiOperationAlphaContentsRegisterGlobalConstantSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsProposalsSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsProposalsProposalsDenestDynDenestSeq = { protocol_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsPreendorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsOriginationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsOriginationDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsIncreasePaidStorageSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsEndorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsDrainDelegateDestination = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsDrainDelegateDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsDrainDelegateConsensusKey = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsDelegationSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsDelegationDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeaderCommitment = { dal_commitment: FixedBytes<48> };
export type Proto016PtMumbaiOperationAlphaContentsDalPublishSlotHeaderSlotHeader = { level: Int32, index: Uint8, commitment: CGRIDClass__Proto016_PtMumbaiOperationAlphaContents_Dal_publish_slot_header_slot_header_commitment, commitment_proof: FixedBytes<48> };
export type Proto016PtMumbaiOperationAlphaContentsDalAttestationAttestor = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsBallotSource = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationAlphaContentsBallotProposal = { protocol_hash: FixedBytes<32> };
export type Proto016PtMumbaiOperationAlphaContentsActivateAccountPkh = { ed25519_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__Proto016PtMumbaiOperationAlphaContents{
    Seed_nonce_revelation = 1,
    Double_endorsement_evidence = 2,
    Double_baking_evidence = 3,
    Activate_account = 4,
    Proposals = 5,
    Ballot = 6,
    Double_preendorsement_evidence = 7,
    Vdf_revelation = 8,
    Drain_delegate = 9,
    Failing_noop = 17,
    Preendorsement = 20,
    Endorsement = 21,
    Dal_attestation = 22,
    Reveal = 107,
    Transaction = 108,
    Origination = 109,
    Delegation = 110,
    Register_global_constant = 111,
    Set_deposits_limit = 112,
    Increase_paid_storage = 113,
    Update_consensus_key = 114,
    Tx_rollup_origination = 150,
    Tx_rollup_submit_batch = 151,
    Tx_rollup_commit = 152,
    Tx_rollup_return_bond = 153,
    Tx_rollup_finalize_commitment = 154,
    Tx_rollup_remove_commitment = 155,
    Tx_rollup_rejection = 156,
    Tx_rollup_dispatch_tickets = 157,
    Transfer_ticket = 158,
    Smart_rollup_originate = 200,
    Smart_rollup_add_messages = 201,
    Smart_rollup_cement = 202,
    Smart_rollup_publish = 203,
    Smart_rollup_refute = 204,
    Smart_rollup_timeout = 205,
    Smart_rollup_execute_outbox_message = 206,
    Smart_rollup_recover_bond = 207,
    Dal_publish_slot_header = 230,
    Zk_rollup_origination = 250,
    Zk_rollup_publish = 251,
    Zk_rollup_update = 252
}
export interface CGRIDMap__Proto016PtMumbaiOperationAlphaContents {
    Seed_nonce_revelation: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Seed_nonce_revelation,
    Double_endorsement_evidence: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_endorsement_evidence,
    Double_baking_evidence: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_baking_evidence,
    Activate_account: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Activate_account,
    Proposals: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Proposals,
    Ballot: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Ballot,
    Double_preendorsement_evidence: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Double_preendorsement_evidence,
    Vdf_revelation: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Vdf_revelation,
    Drain_delegate: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Drain_delegate,
    Failing_noop: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Failing_noop,
    Preendorsement: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Preendorsement,
    Endorsement: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Endorsement,
    Dal_attestation: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Dal_attestation,
    Reveal: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Reveal,
    Transaction: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Transaction,
    Origination: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Origination,
    Delegation: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Delegation,
    Register_global_constant: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Register_global_constant,
    Set_deposits_limit: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Set_deposits_limit,
    Increase_paid_storage: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Increase_paid_storage,
    Update_consensus_key: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Update_consensus_key,
    Tx_rollup_origination: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_origination,
    Tx_rollup_submit_batch: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_submit_batch,
    Tx_rollup_commit: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_commit,
    Tx_rollup_return_bond: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_return_bond,
    Tx_rollup_finalize_commitment: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_finalize_commitment,
    Tx_rollup_remove_commitment: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_remove_commitment,
    Tx_rollup_rejection: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_rejection,
    Tx_rollup_dispatch_tickets: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Tx_rollup_dispatch_tickets,
    Transfer_ticket: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Transfer_ticket,
    Smart_rollup_originate: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_originate,
    Smart_rollup_add_messages: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_add_messages,
    Smart_rollup_cement: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_cement,
    Smart_rollup_publish: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_publish,
    Smart_rollup_refute: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_refute,
    Smart_rollup_timeout: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_timeout,
    Smart_rollup_execute_outbox_message: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_execute_outbox_message,
    Smart_rollup_recover_bond: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Smart_rollup_recover_bond,
    Dal_publish_slot_header: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Dal_publish_slot_header,
    Zk_rollup_origination: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_origination,
    Zk_rollup_publish: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_publish,
    Zk_rollup_update: CGRIDClass__Proto016PtMumbaiOperationAlphaContents__Zk_rollup_update
}
export type Proto016PtMumbaiOperationAlphaContents = { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Seed_nonce_revelation, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Seed_nonce_revelation'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Double_endorsement_evidence, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Double_endorsement_evidence'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Double_baking_evidence, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Double_baking_evidence'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Activate_account, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Activate_account'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Proposals, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Proposals'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Ballot, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Ballot'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Double_preendorsement_evidence, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Double_preendorsement_evidence'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Vdf_revelation, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Vdf_revelation'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Drain_delegate, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Drain_delegate'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Failing_noop, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Failing_noop'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Preendorsement, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Preendorsement'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Endorsement, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Endorsement'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Dal_attestation, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Dal_attestation'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Reveal, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Reveal'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Transaction, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Transaction'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Origination, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Origination'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Delegation, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Delegation'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Register_global_constant, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Register_global_constant'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Set_deposits_limit, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Set_deposits_limit'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Increase_paid_storage, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Increase_paid_storage'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Update_consensus_key, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Update_consensus_key'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_origination, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Tx_rollup_origination'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_submit_batch, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Tx_rollup_submit_batch'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_commit, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Tx_rollup_commit'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_return_bond, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Tx_rollup_return_bond'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_finalize_commitment, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Tx_rollup_finalize_commitment'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_remove_commitment, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Tx_rollup_remove_commitment'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_rejection, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Tx_rollup_rejection'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Tx_rollup_dispatch_tickets, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Tx_rollup_dispatch_tickets'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Transfer_ticket, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Transfer_ticket'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_originate, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Smart_rollup_originate'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_add_messages, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Smart_rollup_add_messages'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_cement, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Smart_rollup_cement'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_publish, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Smart_rollup_publish'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_refute, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Smart_rollup_refute'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_timeout, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Smart_rollup_timeout'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_execute_outbox_message, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Smart_rollup_execute_outbox_message'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Smart_rollup_recover_bond, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Smart_rollup_recover_bond'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Dal_publish_slot_header, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Dal_publish_slot_header'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Zk_rollup_origination, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Zk_rollup_origination'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Zk_rollup_publish, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Zk_rollup_publish'] } | { kind: CGRIDTag__Proto016PtMumbaiOperationAlphaContents.Zk_rollup_update, value: CGRIDMap__Proto016PtMumbaiOperationAlphaContents['Zk_rollup_update'] };
export enum Proto016PtMumbaiMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117,
    LEVEL = 118,
    SELF_ADDRESS = 119,
    never = 120,
    NEVER = 121,
    UNPAIR = 122,
    VOTING_POWER = 123,
    TOTAL_VOTING_POWER = 124,
    KECCAK = 125,
    SHA3 = 126,
    PAIRING_CHECK = 127,
    bls12_381_g1 = 128,
    bls12_381_g2 = 129,
    bls12_381_fr = 130,
    sapling_state = 131,
    sapling_transaction_deprecated = 132,
    SAPLING_EMPTY_STATE = 133,
    SAPLING_VERIFY_UPDATE = 134,
    ticket = 135,
    TICKET_DEPRECATED = 136,
    READ_TICKET = 137,
    SPLIT_TICKET = 138,
    JOIN_TICKETS = 139,
    GET_AND_UPDATE = 140,
    chest = 141,
    chest_key = 142,
    OPEN_CHEST = 143,
    VIEW = 144,
    view = 145,
    constant = 146,
    SUB_MUTEZ = 147,
    tx_rollup_l2_address = 148,
    MIN_BLOCK_TIME = 149,
    sapling_transaction = 150,
    EMIT = 151,
    Lambda_rec = 152,
    LAMBDA_REC = 153,
    TICKET = 154,
    BYTES = 155,
    NAT = 156
}
export type Proto016PtMumbaiInlinedPreendorsementSignature = { signature_v1: Bytes };
export type Proto016PtMumbaiInlinedPreendorsementContentsPreendorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export enum CGRIDTag__Proto016PtMumbaiInlinedPreendorsementContents{
    Preendorsement = 20
}
export interface CGRIDMap__Proto016PtMumbaiInlinedPreendorsementContents {
    Preendorsement: CGRIDClass__Proto016PtMumbaiInlinedPreendorsementContents__Preendorsement
}
export type Proto016PtMumbaiInlinedPreendorsementContents = { kind: CGRIDTag__Proto016PtMumbaiInlinedPreendorsementContents.Preendorsement, value: CGRIDMap__Proto016PtMumbaiInlinedPreendorsementContents['Preendorsement'] };
export type Proto016PtMumbaiInlinedPreendorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__Proto016_PtMumbaiInlinedPreendorsementContents, signature: Nullable<CGRIDClass__Proto016_PtMumbaiInlinedPreendorsement_signature> };
export type Proto016PtMumbaiInlinedEndorsementSignature = { signature_v1: Bytes };
export type Proto016PtMumbaiInlinedEndorsementMempoolContentsEndorsementBlockPayloadHash = { value_hash: FixedBytes<32> };
export enum CGRIDTag__Proto016PtMumbaiInlinedEndorsementMempoolContents{
    Endorsement = 21
}
export interface CGRIDMap__Proto016PtMumbaiInlinedEndorsementMempoolContents {
    Endorsement: CGRIDClass__Proto016PtMumbaiInlinedEndorsementMempoolContents__Endorsement
}
export type Proto016PtMumbaiInlinedEndorsementMempoolContents = { kind: CGRIDTag__Proto016PtMumbaiInlinedEndorsementMempoolContents.Endorsement, value: CGRIDMap__Proto016PtMumbaiInlinedEndorsementMempoolContents['Endorsement'] };
export type Proto016PtMumbaiInlinedEndorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_mempoolContents, signature: Nullable<CGRIDClass__Proto016_PtMumbaiInlinedEndorsement_signature> };
export enum CGRIDTag__Proto016PtMumbaiEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    deposit = 5,
    named = 255
}
export interface CGRIDMap__Proto016PtMumbaiEntrypoint {
    _default: CGRIDClass__Proto016PtMumbaiEntrypoint___default,
    root: CGRIDClass__Proto016PtMumbaiEntrypoint__root,
    _do: CGRIDClass__Proto016PtMumbaiEntrypoint___do,
    set_delegate: CGRIDClass__Proto016PtMumbaiEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto016PtMumbaiEntrypoint__remove_delegate,
    deposit: CGRIDClass__Proto016PtMumbaiEntrypoint__deposit,
    named: CGRIDClass__Proto016PtMumbaiEntrypoint__named
}
export type Proto016PtMumbaiEntrypoint = { kind: CGRIDTag__Proto016PtMumbaiEntrypoint._default, value: CGRIDMap__Proto016PtMumbaiEntrypoint['_default'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint.root, value: CGRIDMap__Proto016PtMumbaiEntrypoint['root'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint._do, value: CGRIDMap__Proto016PtMumbaiEntrypoint['_do'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint.set_delegate, value: CGRIDMap__Proto016PtMumbaiEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint.remove_delegate, value: CGRIDMap__Proto016PtMumbaiEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint.deposit, value: CGRIDMap__Proto016PtMumbaiEntrypoint['deposit'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint.named, value: CGRIDMap__Proto016PtMumbaiEntrypoint['named'] };
export type Proto016PtMumbaiContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export type Proto016PtMumbaiContractIdOriginatedOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto016PtMumbaiContractIdOriginated{
    Originated = 1
}
export interface CGRIDMap__Proto016PtMumbaiContractIdOriginated {
    Originated: CGRIDClass__Proto016PtMumbaiContractIdOriginated__Originated
}
export type Proto016PtMumbaiContractIdOriginated = { kind: CGRIDTag__Proto016PtMumbaiContractIdOriginated.Originated, value: CGRIDMap__Proto016PtMumbaiContractIdOriginated['Originated'] };
export enum CGRIDTag__Proto016PtMumbaiContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto016PtMumbaiContractId {
    Implicit: CGRIDClass__Proto016PtMumbaiContractId__Implicit,
    Originated: CGRIDClass__Proto016PtMumbaiContractId__Originated
}
export type Proto016PtMumbaiContractId = { kind: CGRIDTag__Proto016PtMumbaiContractId.Implicit, value: CGRIDMap__Proto016PtMumbaiContractId['Implicit'] } | { kind: CGRIDTag__Proto016PtMumbaiContractId.Originated, value: CGRIDMap__Proto016PtMumbaiContractId['Originated'] };
export type Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto016PtMumbaiBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type Proto016PtMumbaiBlockHeaderAlphaSignedContentsSignature = { signature_v1: Bytes };
export type Proto016PtMumbaiBlockHeaderAlphaFullHeader = { level: Int32, proto: Uint8, predecessor: CGRIDClass__Block_headerShell_predecessor, timestamp: Int64, validation_pass: Uint8, operations_hash: CGRIDClass__Block_headerShell_operations_hash, fitness: Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>, context: CGRIDClass__Block_headerShell_context, payload_hash: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_toggle_vote: Int8, signature: CGRIDClass__Proto016_PtMumbaiBlock_headerAlphaSigned_contents_signature };
export type Proto016PtMumbaiOperationUnsigned = { branch: CGRIDClass__OperationShell_header_branch, contents: Sequence<CGRIDClass__Proto016_PtMumbaiOperationAlphaContents> };
export class CGRIDClass__Proto016PtMumbaiOperationUnsigned extends Box<Proto016PtMumbaiOperationUnsigned> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'contents']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationUnsigned {
        return new this(record_decoder<Proto016PtMumbaiOperationUnsigned>({branch: CGRIDClass__OperationShell_header_branch.decode, contents: Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents.decode)}, {order: ['branch', 'contents']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.contents.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.contents.writeTarget(tgt));
    }
}
export const proto016_ptmumbai_operation_unsigned_encoder = (value: Proto016PtMumbaiOperationUnsigned): OutputBytes => {
    return record_encoder({order: ['branch', 'contents']})(value);
}
export const proto016_ptmumbai_operation_unsigned_decoder = (p: Parser): Proto016PtMumbaiOperationUnsigned => {
    return record_decoder<Proto016PtMumbaiOperationUnsigned>({branch: CGRIDClass__OperationShell_header_branch.decode, contents: Sequence.decode(CGRIDClass__Proto016_PtMumbaiOperationAlphaContents.decode)}, {order: ['branch', 'contents']})(p);
}
