import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto016PtMumbaiTimestamp = Int64;
export class CGRIDClass__Proto016PtMumbaiTimestamp extends Box<Proto016PtMumbaiTimestamp> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiTimestamp {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto016_ptmumbai_timestamp_encoder = (value: Proto016PtMumbaiTimestamp): OutputBytes => {
    return value.encode();
}
export const proto016_ptmumbai_timestamp_decoder = (p: Parser): Proto016PtMumbaiTimestamp => {
    return Int64.decode(p);
}
