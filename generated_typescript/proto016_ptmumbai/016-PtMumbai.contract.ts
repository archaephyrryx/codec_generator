import { Codec } from '../../ts_runtime/codec';
import { Padded } from '../../ts_runtime/composite/padded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Bls generated for PublicKeyHash__Bls
export class CGRIDClass__PublicKeyHash__Bls extends Box<PublicKeyHash__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Bls {
        return new this(record_decoder<PublicKeyHash__Bls>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Bls = { bls12_381_public_key_hash: FixedBytes<20> };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
            case CGRIDTag__PublicKeyHash.Bls: return CGRIDClass__PublicKeyHash__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad generated for Proto016PtMumbaiContractIdOriginatedDenestPad
export class CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad extends Box<Proto016PtMumbaiContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto016PtMumbaiContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiContract__Originated generated for Proto016PtMumbaiContract__Originated
export class CGRIDClass__Proto016PtMumbaiContract__Originated extends Box<Proto016PtMumbaiContract__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiContract__Originated {
        return new this(Padded.decode(CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiContract__Implicit generated for Proto016PtMumbaiContract__Implicit
export class CGRIDClass__Proto016PtMumbaiContract__Implicit extends Box<Proto016PtMumbaiContract__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiContract__Implicit {
        return new this(record_decoder<Proto016PtMumbaiContract__Implicit>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256,
    Bls: CGRIDClass__PublicKeyHash__Bls
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] } | { kind: CGRIDTag__PublicKeyHash.Bls, value: CGRIDMap__PublicKeyHash['Bls'] };
export type Proto016PtMumbaiContract__Originated = Padded<CGRIDClass__Proto016_PtMumbaiContract_id_Originated_denest_pad,1>;
export type Proto016PtMumbaiContract__Implicit = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto016PtMumbaiContract{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto016PtMumbaiContract {
    Implicit: CGRIDClass__Proto016PtMumbaiContract__Implicit,
    Originated: CGRIDClass__Proto016PtMumbaiContract__Originated
}
export type Proto016PtMumbaiContract = { kind: CGRIDTag__Proto016PtMumbaiContract.Implicit, value: CGRIDMap__Proto016PtMumbaiContract['Implicit'] } | { kind: CGRIDTag__Proto016PtMumbaiContract.Originated, value: CGRIDMap__Proto016PtMumbaiContract['Originated'] };
export function proto016ptmumbaicontract_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiContract,Proto016PtMumbaiContract> {
    function f(disc: CGRIDTag__Proto016PtMumbaiContract) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiContract.Implicit: return CGRIDClass__Proto016PtMumbaiContract__Implicit.decode;
            case CGRIDTag__Proto016PtMumbaiContract.Originated: return CGRIDClass__Proto016PtMumbaiContract__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiContract => Object.values(CGRIDTag__Proto016PtMumbaiContract).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016PtMumbaiContract extends Box<Proto016PtMumbaiContract> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiContract>, Proto016PtMumbaiContract>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiContract {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaicontract_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto016_ptmumbai_contract_encoder = (value: Proto016PtMumbaiContract): OutputBytes => {
    return variant_encoder<KindOf<Proto016PtMumbaiContract>, Proto016PtMumbaiContract>(width.Uint8)(value);
}
export const proto016_ptmumbai_contract_decoder = (p: Parser): Proto016PtMumbaiContract => {
    return variant_decoder(width.Uint8)(proto016ptmumbaicontract_mkDecoder())(p);
}
