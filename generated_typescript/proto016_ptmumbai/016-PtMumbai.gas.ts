import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto016PtMumbaiGas__Unaccounted generated for Proto016PtMumbaiGas__Unaccounted
export class CGRIDClass__Proto016PtMumbaiGas__Unaccounted extends Box<Proto016PtMumbaiGas__Unaccounted> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiGas__Unaccounted {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiGas__Limited generated for Proto016PtMumbaiGas__Limited
export class CGRIDClass__Proto016PtMumbaiGas__Limited extends Box<Proto016PtMumbaiGas__Limited> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiGas__Limited {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto016PtMumbaiGas__Unaccounted = Unit;
export type Proto016PtMumbaiGas__Limited = Z;
export enum CGRIDTag__Proto016PtMumbaiGas{
    Limited = 0,
    Unaccounted = 1
}
export interface CGRIDMap__Proto016PtMumbaiGas {
    Limited: CGRIDClass__Proto016PtMumbaiGas__Limited,
    Unaccounted: CGRIDClass__Proto016PtMumbaiGas__Unaccounted
}
export type Proto016PtMumbaiGas = { kind: CGRIDTag__Proto016PtMumbaiGas.Limited, value: CGRIDMap__Proto016PtMumbaiGas['Limited'] } | { kind: CGRIDTag__Proto016PtMumbaiGas.Unaccounted, value: CGRIDMap__Proto016PtMumbaiGas['Unaccounted'] };
export function proto016ptmumbaigas_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiGas,Proto016PtMumbaiGas> {
    function f(disc: CGRIDTag__Proto016PtMumbaiGas) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiGas.Limited: return CGRIDClass__Proto016PtMumbaiGas__Limited.decode;
            case CGRIDTag__Proto016PtMumbaiGas.Unaccounted: return CGRIDClass__Proto016PtMumbaiGas__Unaccounted.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiGas => Object.values(CGRIDTag__Proto016PtMumbaiGas).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016PtMumbaiGas extends Box<Proto016PtMumbaiGas> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiGas>, Proto016PtMumbaiGas>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiGas {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaigas_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto016_ptmumbai_gas_encoder = (value: Proto016PtMumbaiGas): OutputBytes => {
    return variant_encoder<KindOf<Proto016PtMumbaiGas>, Proto016PtMumbaiGas>(width.Uint8)(value);
}
export const proto016_ptmumbai_gas_decoder = (p: Parser): Proto016PtMumbaiGas => {
    return variant_decoder(width.Uint8)(proto016ptmumbaigas_mkDecoder())(p);
}
