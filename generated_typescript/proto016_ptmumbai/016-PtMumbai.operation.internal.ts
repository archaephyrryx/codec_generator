import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Bls generated for PublicKeyHash__Bls
export class CGRIDClass__PublicKeyHash__Bls extends Box<PublicKeyHash__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Bls {
        return new this(record_decoder<PublicKeyHash__Bls>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiTransactionDestination__Zk_rollup generated for Proto016PtMumbaiTransactionDestination__Zk_rollup
export class CGRIDClass__Proto016PtMumbaiTransactionDestination__Zk_rollup extends Box<Proto016PtMumbaiTransactionDestination__Zk_rollup> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiTransactionDestination__Zk_rollup {
        return new this(Padded.decode(CGRIDClass__Proto016_PtMumbaiTransaction_destination_Zk_rollup_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiTransactionDestination__Tx_rollup generated for Proto016PtMumbaiTransactionDestination__Tx_rollup
export class CGRIDClass__Proto016PtMumbaiTransactionDestination__Tx_rollup extends Box<Proto016PtMumbaiTransactionDestination__Tx_rollup> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiTransactionDestination__Tx_rollup {
        return new this(Padded.decode(CGRIDClass__Proto016_PtMumbaiTx_rollup_id.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiTransactionDestination__Smart_rollup generated for Proto016PtMumbaiTransactionDestination__Smart_rollup
export class CGRIDClass__Proto016PtMumbaiTransactionDestination__Smart_rollup extends Box<Proto016PtMumbaiTransactionDestination__Smart_rollup> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiTransactionDestination__Smart_rollup {
        return new this(Padded.decode(CGRIDClass__Proto016_PtMumbaiTransaction_destination_Smart_rollup_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiTransactionDestination__Originated generated for Proto016PtMumbaiTransactionDestination__Originated
export class CGRIDClass__Proto016PtMumbaiTransactionDestination__Originated extends Box<Proto016PtMumbaiTransactionDestination__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiTransactionDestination__Originated {
        return new this(Padded.decode(CGRIDClass__Proto016_PtMumbaiTransaction_destination_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiTransactionDestination__Implicit generated for Proto016PtMumbaiTransactionDestination__Implicit
export class CGRIDClass__Proto016PtMumbaiTransactionDestination__Implicit extends Box<Proto016PtMumbaiTransactionDestination__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiTransactionDestination__Implicit {
        return new this(record_decoder<Proto016PtMumbaiTransactionDestination__Implicit>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint__set_delegate generated for Proto016PtMumbaiEntrypoint__set_delegate
export class CGRIDClass__Proto016PtMumbaiEntrypoint__set_delegate extends Box<Proto016PtMumbaiEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint__root generated for Proto016PtMumbaiEntrypoint__root
export class CGRIDClass__Proto016PtMumbaiEntrypoint__root extends Box<Proto016PtMumbaiEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint__remove_delegate generated for Proto016PtMumbaiEntrypoint__remove_delegate
export class CGRIDClass__Proto016PtMumbaiEntrypoint__remove_delegate extends Box<Proto016PtMumbaiEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint__named generated for Proto016PtMumbaiEntrypoint__named
export class CGRIDClass__Proto016PtMumbaiEntrypoint__named extends Box<Proto016PtMumbaiEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint__deposit generated for Proto016PtMumbaiEntrypoint__deposit
export class CGRIDClass__Proto016PtMumbaiEntrypoint__deposit extends Box<Proto016PtMumbaiEntrypoint__deposit> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint__deposit {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint___do generated for Proto016PtMumbaiEntrypoint___do
export class CGRIDClass__Proto016PtMumbaiEntrypoint___do extends Box<Proto016PtMumbaiEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiEntrypoint___default generated for Proto016PtMumbaiEntrypoint___default
export class CGRIDClass__Proto016PtMumbaiEntrypoint___default extends Box<Proto016PtMumbaiEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Transaction generated for Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Transaction
export class CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Transaction extends Box<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Transaction {
        return new this(record_decoder<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Transaction>({amount: N.decode, destination: CGRIDClass__Proto016_PtMumbaiTransaction_destination.decode, parameters: Option.decode(CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Transaction_parameters.decode)}, {order: ['amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Origination generated for Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Origination
export class CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Origination extends Box<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Origination {
        return new this(record_decoder<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Origination>({balance: N.decode, delegate: Option.decode(CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Origination_delegate.decode), script: CGRIDClass__Proto016_PtMumbaiScriptedContracts.decode}, {order: ['balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Event generated for Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Event
export class CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Event extends Box<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Event> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_type', 'tag', 'payload']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Event {
        return new this(record_decoder<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Event>({_type: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode, tag: Option.decode(CGRIDClass__Proto016_PtMumbaiEntrypoint.decode), payload: Option.decode(CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode)}, {order: ['_type', 'tag', 'payload']})(p));
    };
    get encodeLength(): number {
        return (this.value._type.encodeLength +  this.value.tag.encodeLength +  this.value.payload.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._type.writeTarget(tgt) +  this.value.tag.writeTarget(tgt) +  this.value.payload.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Delegation generated for Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Delegation
export class CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Delegation extends Box<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Delegation {
        return new this(record_decoder<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Delegation>({delegate: Option.decode(CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Delegation_delegate.decode)}, {order: ['delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__String generated for MichelineProto016PtMumbaiMichelsonV1Expression__String
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__String extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__String {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Sequence generated for MichelineProto016PtMumbaiMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Sequence extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Int generated for MichelineProto016PtMumbaiMichelsonV1Expression__Int
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Int extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Bytes generated for MichelineProto016PtMumbaiMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Bytes extends Box<MichelineProto016PtMumbaiMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineProto016PtMumbaiMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Bls = { bls12_381_public_key_hash: FixedBytes<20> };
export type Proto016PtMumbaiTransactionDestination__Zk_rollup = Padded<CGRIDClass__Proto016_PtMumbaiTransaction_destination_Zk_rollup_denest_pad,1>;
export type Proto016PtMumbaiTransactionDestination__Tx_rollup = Padded<CGRIDClass__Proto016_PtMumbaiTx_rollup_id,1>;
export type Proto016PtMumbaiTransactionDestination__Smart_rollup = Padded<CGRIDClass__Proto016_PtMumbaiTransaction_destination_Smart_rollup_denest_pad,1>;
export type Proto016PtMumbaiTransactionDestination__Originated = Padded<CGRIDClass__Proto016_PtMumbaiTransaction_destination_Originated_denest_pad,1>;
export type Proto016PtMumbaiTransactionDestination__Implicit = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiEntrypoint__set_delegate = Unit;
export type Proto016PtMumbaiEntrypoint__root = Unit;
export type Proto016PtMumbaiEntrypoint__remove_delegate = Unit;
export type Proto016PtMumbaiEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto016PtMumbaiEntrypoint__deposit = Unit;
export type Proto016PtMumbaiEntrypoint___do = Unit;
export type Proto016PtMumbaiEntrypoint___default = Unit;
export type Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Transaction = { amount: N, destination: CGRIDClass__Proto016_PtMumbaiTransaction_destination, parameters: Option<CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Transaction_parameters> };
export type Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Origination = { balance: N, delegate: Option<CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Origination_delegate>, script: CGRIDClass__Proto016_PtMumbaiScriptedContracts };
export type Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Event = { _type: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression, tag: Option<CGRIDClass__Proto016_PtMumbaiEntrypoint>, payload: Option<CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression> };
export type Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Delegation = { delegate: Option<CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Delegation_delegate> };
export type MichelineProto016PtMumbaiMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression>,width.Uint30>;
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression, arg2: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression, arg2: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, arg: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives, arg: CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Int = { int: Z };
export type MichelineProto016PtMumbaiMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
            case CGRIDTag__PublicKeyHash.Bls: return CGRIDClass__PublicKeyHash__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiTx_rollup_id generated for Proto016PtMumbaiTxRollupId
export class CGRIDClass__Proto016_PtMumbaiTx_rollup_id extends Box<Proto016PtMumbaiTxRollupId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiTx_rollup_id {
        return new this(record_decoder<Proto016PtMumbaiTxRollupId>({rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiTransaction_destination_Zk_rollup_denest_pad generated for Proto016PtMumbaiTransactionDestinationZkRollupDenestPad
export class CGRIDClass__Proto016_PtMumbaiTransaction_destination_Zk_rollup_denest_pad extends Box<Proto016PtMumbaiTransactionDestinationZkRollupDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['zk_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiTransaction_destination_Zk_rollup_denest_pad {
        return new this(record_decoder<Proto016PtMumbaiTransactionDestinationZkRollupDenestPad>({zk_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['zk_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.zk_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.zk_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiTransaction_destination_Smart_rollup_denest_pad generated for Proto016PtMumbaiTransactionDestinationSmartRollupDenestPad
export class CGRIDClass__Proto016_PtMumbaiTransaction_destination_Smart_rollup_denest_pad extends Box<Proto016PtMumbaiTransactionDestinationSmartRollupDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['smart_rollup_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiTransaction_destination_Smart_rollup_denest_pad {
        return new this(record_decoder<Proto016PtMumbaiTransactionDestinationSmartRollupDenestPad>({smart_rollup_hash: FixedBytes.decode<20>({len: 20})}, {order: ['smart_rollup_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.smart_rollup_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.smart_rollup_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiTransaction_destination_Originated_denest_pad generated for Proto016PtMumbaiTransactionDestinationOriginatedDenestPad
export class CGRIDClass__Proto016_PtMumbaiTransaction_destination_Originated_denest_pad extends Box<Proto016PtMumbaiTransactionDestinationOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiTransaction_destination_Originated_denest_pad {
        return new this(record_decoder<Proto016PtMumbaiTransactionDestinationOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiTransaction_destination generated for Proto016PtMumbaiTransactionDestination
export function proto016ptmumbaitransactiondestination_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiTransactionDestination,Proto016PtMumbaiTransactionDestination> {
    function f(disc: CGRIDTag__Proto016PtMumbaiTransactionDestination) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiTransactionDestination.Implicit: return CGRIDClass__Proto016PtMumbaiTransactionDestination__Implicit.decode;
            case CGRIDTag__Proto016PtMumbaiTransactionDestination.Originated: return CGRIDClass__Proto016PtMumbaiTransactionDestination__Originated.decode;
            case CGRIDTag__Proto016PtMumbaiTransactionDestination.Tx_rollup: return CGRIDClass__Proto016PtMumbaiTransactionDestination__Tx_rollup.decode;
            case CGRIDTag__Proto016PtMumbaiTransactionDestination.Smart_rollup: return CGRIDClass__Proto016PtMumbaiTransactionDestination__Smart_rollup.decode;
            case CGRIDTag__Proto016PtMumbaiTransactionDestination.Zk_rollup: return CGRIDClass__Proto016PtMumbaiTransactionDestination__Zk_rollup.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiTransactionDestination => Object.values(CGRIDTag__Proto016PtMumbaiTransactionDestination).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiTransaction_destination extends Box<Proto016PtMumbaiTransactionDestination> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiTransactionDestination>, Proto016PtMumbaiTransactionDestination>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiTransaction_destination {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaitransactiondestination_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiScriptedContracts generated for Proto016PtMumbaiScriptedContracts
export class CGRIDClass__Proto016_PtMumbaiScriptedContracts extends Box<Proto016PtMumbaiScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiScriptedContracts {
        return new this(record_decoder<Proto016PtMumbaiScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives generated for Proto016PtMumbaiMichelsonV1Primitives
export class CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives extends Box<Proto016PtMumbaiMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<Proto016PtMumbaiMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is Proto016PtMumbaiMichelsonV1Primitives => (Object.values(Proto016PtMumbaiMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016_PtMumbaiEntrypoint generated for Proto016PtMumbaiEntrypoint
export function proto016ptmumbaientrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiEntrypoint,Proto016PtMumbaiEntrypoint> {
    function f(disc: CGRIDTag__Proto016PtMumbaiEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiEntrypoint._default: return CGRIDClass__Proto016PtMumbaiEntrypoint___default.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint.root: return CGRIDClass__Proto016PtMumbaiEntrypoint__root.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint._do: return CGRIDClass__Proto016PtMumbaiEntrypoint___do.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint.set_delegate: return CGRIDClass__Proto016PtMumbaiEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint.remove_delegate: return CGRIDClass__Proto016PtMumbaiEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint.deposit: return CGRIDClass__Proto016PtMumbaiEntrypoint__deposit.decode;
            case CGRIDTag__Proto016PtMumbaiEntrypoint.named: return CGRIDClass__Proto016PtMumbaiEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiEntrypoint => Object.values(CGRIDTag__Proto016PtMumbaiEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiEntrypoint extends Box<Proto016PtMumbaiEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiEntrypoint>, Proto016PtMumbaiEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaientrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_rhs generated for Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs
export function proto016ptmumbaiapplyinternalresultsalphaoperationresultrhs_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs,Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs> {
    function f(disc: CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs.Transaction: return CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Transaction.decode;
            case CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs.Origination: return CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Origination.decode;
            case CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs.Delegation: return CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Delegation.decode;
            case CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs.Event: return CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Event.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs => Object.values(CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_rhs extends Box<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs>, Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_rhs {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaiapplyinternalresultsalphaoperationresultrhs_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Transaction_parameters generated for Proto016PtMumbaiApplyInternalResultsAlphaOperationResultTransactionParameters
export class CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Transaction_parameters extends Box<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Transaction_parameters {
        return new this(record_decoder<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultTransactionParameters>({entrypoint: CGRIDClass__Proto016_PtMumbaiEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Origination_delegate generated for Proto016PtMumbaiApplyInternalResultsAlphaOperationResultOriginationDelegate
export class CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Origination_delegate extends Box<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Origination_delegate {
        return new this(record_decoder<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultOriginationDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Delegation_delegate generated for Proto016PtMumbaiApplyInternalResultsAlphaOperationResultDelegationDelegate
export class CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Delegation_delegate extends Box<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_Delegation_delegate {
        return new this(record_decoder<Proto016PtMumbaiApplyInternalResultsAlphaOperationResultDelegationDelegate>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression generated for MichelineProto016PtMumbaiMichelsonV1Expression
export function michelineproto016ptmumbaimichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression,MichelineProto016PtMumbaiMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Int: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.String: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Sequence: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Bytes: return CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression => Object.values(CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression extends Box<MichelineProto016PtMumbaiMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineProto016PtMumbaiMichelsonV1Expression>, MichelineProto016PtMumbaiMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto016_PtMumbaiMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelineproto016ptmumbaimichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256,
    Bls: CGRIDClass__PublicKeyHash__Bls
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] } | { kind: CGRIDTag__PublicKeyHash.Bls, value: CGRIDMap__PublicKeyHash['Bls'] };
export enum CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression {
    Int: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineProto016PtMumbaiMichelsonV1Expression__Bytes
}
export type MichelineProto016PtMumbaiMichelsonV1Expression = { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Int, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.String, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineProto016PtMumbaiMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineProto016PtMumbaiMichelsonV1Expression['Bytes'] };
export type Proto016PtMumbaiTxRollupId = { rollup_hash: FixedBytes<20> };
export type Proto016PtMumbaiTransactionDestinationZkRollupDenestPad = { zk_rollup_hash: FixedBytes<20> };
export type Proto016PtMumbaiTransactionDestinationSmartRollupDenestPad = { smart_rollup_hash: FixedBytes<20> };
export type Proto016PtMumbaiTransactionDestinationOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto016PtMumbaiTransactionDestination{
    Implicit = 0,
    Originated = 1,
    Tx_rollup = 2,
    Smart_rollup = 3,
    Zk_rollup = 4
}
export interface CGRIDMap__Proto016PtMumbaiTransactionDestination {
    Implicit: CGRIDClass__Proto016PtMumbaiTransactionDestination__Implicit,
    Originated: CGRIDClass__Proto016PtMumbaiTransactionDestination__Originated,
    Tx_rollup: CGRIDClass__Proto016PtMumbaiTransactionDestination__Tx_rollup,
    Smart_rollup: CGRIDClass__Proto016PtMumbaiTransactionDestination__Smart_rollup,
    Zk_rollup: CGRIDClass__Proto016PtMumbaiTransactionDestination__Zk_rollup
}
export type Proto016PtMumbaiTransactionDestination = { kind: CGRIDTag__Proto016PtMumbaiTransactionDestination.Implicit, value: CGRIDMap__Proto016PtMumbaiTransactionDestination['Implicit'] } | { kind: CGRIDTag__Proto016PtMumbaiTransactionDestination.Originated, value: CGRIDMap__Proto016PtMumbaiTransactionDestination['Originated'] } | { kind: CGRIDTag__Proto016PtMumbaiTransactionDestination.Tx_rollup, value: CGRIDMap__Proto016PtMumbaiTransactionDestination['Tx_rollup'] } | { kind: CGRIDTag__Proto016PtMumbaiTransactionDestination.Smart_rollup, value: CGRIDMap__Proto016PtMumbaiTransactionDestination['Smart_rollup'] } | { kind: CGRIDTag__Proto016PtMumbaiTransactionDestination.Zk_rollup, value: CGRIDMap__Proto016PtMumbaiTransactionDestination['Zk_rollup'] };
export type Proto016PtMumbaiScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export enum Proto016PtMumbaiMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117,
    LEVEL = 118,
    SELF_ADDRESS = 119,
    never = 120,
    NEVER = 121,
    UNPAIR = 122,
    VOTING_POWER = 123,
    TOTAL_VOTING_POWER = 124,
    KECCAK = 125,
    SHA3 = 126,
    PAIRING_CHECK = 127,
    bls12_381_g1 = 128,
    bls12_381_g2 = 129,
    bls12_381_fr = 130,
    sapling_state = 131,
    sapling_transaction_deprecated = 132,
    SAPLING_EMPTY_STATE = 133,
    SAPLING_VERIFY_UPDATE = 134,
    ticket = 135,
    TICKET_DEPRECATED = 136,
    READ_TICKET = 137,
    SPLIT_TICKET = 138,
    JOIN_TICKETS = 139,
    GET_AND_UPDATE = 140,
    chest = 141,
    chest_key = 142,
    OPEN_CHEST = 143,
    VIEW = 144,
    view = 145,
    constant = 146,
    SUB_MUTEZ = 147,
    tx_rollup_l2_address = 148,
    MIN_BLOCK_TIME = 149,
    sapling_transaction = 150,
    EMIT = 151,
    Lambda_rec = 152,
    LAMBDA_REC = 153,
    TICKET = 154,
    BYTES = 155,
    NAT = 156
}
export enum CGRIDTag__Proto016PtMumbaiEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    deposit = 5,
    named = 255
}
export interface CGRIDMap__Proto016PtMumbaiEntrypoint {
    _default: CGRIDClass__Proto016PtMumbaiEntrypoint___default,
    root: CGRIDClass__Proto016PtMumbaiEntrypoint__root,
    _do: CGRIDClass__Proto016PtMumbaiEntrypoint___do,
    set_delegate: CGRIDClass__Proto016PtMumbaiEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto016PtMumbaiEntrypoint__remove_delegate,
    deposit: CGRIDClass__Proto016PtMumbaiEntrypoint__deposit,
    named: CGRIDClass__Proto016PtMumbaiEntrypoint__named
}
export type Proto016PtMumbaiEntrypoint = { kind: CGRIDTag__Proto016PtMumbaiEntrypoint._default, value: CGRIDMap__Proto016PtMumbaiEntrypoint['_default'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint.root, value: CGRIDMap__Proto016PtMumbaiEntrypoint['root'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint._do, value: CGRIDMap__Proto016PtMumbaiEntrypoint['_do'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint.set_delegate, value: CGRIDMap__Proto016PtMumbaiEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint.remove_delegate, value: CGRIDMap__Proto016PtMumbaiEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint.deposit, value: CGRIDMap__Proto016PtMumbaiEntrypoint['deposit'] } | { kind: CGRIDTag__Proto016PtMumbaiEntrypoint.named, value: CGRIDMap__Proto016PtMumbaiEntrypoint['named'] };
export enum CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs{
    Transaction = 1,
    Origination = 2,
    Delegation = 3,
    Event = 4
}
export interface CGRIDMap__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs {
    Transaction: CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Transaction,
    Origination: CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Origination,
    Delegation: CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Delegation,
    Event: CGRIDClass__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs__Event
}
export type Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs = { kind: CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs.Transaction, value: CGRIDMap__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs['Transaction'] } | { kind: CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs.Origination, value: CGRIDMap__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs['Origination'] } | { kind: CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs.Delegation, value: CGRIDMap__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs['Delegation'] } | { kind: CGRIDTag__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs.Event, value: CGRIDMap__Proto016PtMumbaiApplyInternalResultsAlphaOperationResultRhs['Event'] };
export type Proto016PtMumbaiApplyInternalResultsAlphaOperationResultTransactionParameters = { entrypoint: CGRIDClass__Proto016_PtMumbaiEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto016PtMumbaiApplyInternalResultsAlphaOperationResultOriginationDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiApplyInternalResultsAlphaOperationResultDelegationDelegate = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto016PtMumbaiOperationInternal = { source: CGRIDClass__Proto016_PtMumbaiTransaction_destination, nonce: Uint16, proto016_ptmumbai_apply_internal_results_alpha_operation_result_rhs: CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_rhs };
export class CGRIDClass__Proto016PtMumbaiOperationInternal extends Box<Proto016PtMumbaiOperationInternal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'nonce', 'proto016_ptmumbai_apply_internal_results_alpha_operation_result_rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiOperationInternal {
        return new this(record_decoder<Proto016PtMumbaiOperationInternal>({source: CGRIDClass__Proto016_PtMumbaiTransaction_destination.decode, nonce: Uint16.decode, proto016_ptmumbai_apply_internal_results_alpha_operation_result_rhs: CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_rhs.decode}, {order: ['source', 'nonce', 'proto016_ptmumbai_apply_internal_results_alpha_operation_result_rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.nonce.encodeLength +  this.value.proto016_ptmumbai_apply_internal_results_alpha_operation_result_rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt) +  this.value.proto016_ptmumbai_apply_internal_results_alpha_operation_result_rhs.writeTarget(tgt));
    }
}
export const proto016_ptmumbai_operation_internal_encoder = (value: Proto016PtMumbaiOperationInternal): OutputBytes => {
    return record_encoder({order: ['source', 'nonce', 'proto016_ptmumbai_apply_internal_results_alpha_operation_result_rhs']})(value);
}
export const proto016_ptmumbai_operation_internal_decoder = (p: Parser): Proto016PtMumbaiOperationInternal => {
    return record_decoder<Proto016PtMumbaiOperationInternal>({source: CGRIDClass__Proto016_PtMumbaiTransaction_destination.decode, nonce: Uint16.decode, proto016_ptmumbai_apply_internal_results_alpha_operation_result_rhs: CGRIDClass__Proto016_PtMumbaiApply_internal_resultsAlphaOperation_result_rhs.decode}, {order: ['source', 'nonce', 'proto016_ptmumbai_apply_internal_results_alpha_operation_result_rhs']})(p);
}
