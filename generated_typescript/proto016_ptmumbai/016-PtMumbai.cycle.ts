import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto016PtMumbaiCycle = Int32;
export class CGRIDClass__Proto016PtMumbaiCycle extends Box<Proto016PtMumbaiCycle> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiCycle {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto016_ptmumbai_cycle_encoder = (value: Proto016PtMumbaiCycle): OutputBytes => {
    return value.encode();
}
export const proto016_ptmumbai_cycle_decoder = (p: Parser): Proto016PtMumbaiCycle => {
    return Int32.decode(p);
}
