import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto016PtMumbaiTez = N;
export class CGRIDClass__Proto016PtMumbaiTez extends Box<Proto016PtMumbaiTez> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiTez {
        return new this(N.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto016_ptmumbai_tez_encoder = (value: Proto016PtMumbaiTez): OutputBytes => {
    return value.encode();
}
export const proto016_ptmumbai_tez_decoder = (p: Parser): Proto016PtMumbaiTez => {
    return N.decode(p);
}
