import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto016PtMumbaiVotingPeriodKind__exploration generated for Proto016PtMumbaiVotingPeriodKind__exploration
export class CGRIDClass__Proto016PtMumbaiVotingPeriodKind__exploration extends Box<Proto016PtMumbaiVotingPeriodKind__exploration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiVotingPeriodKind__exploration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Proposal generated for Proto016PtMumbaiVotingPeriodKind__Proposal
export class CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Proposal extends Box<Proto016PtMumbaiVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Promotion generated for Proto016PtMumbaiVotingPeriodKind__Promotion
export class CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Promotion extends Box<Proto016PtMumbaiVotingPeriodKind__Promotion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Promotion {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Cooldown generated for Proto016PtMumbaiVotingPeriodKind__Cooldown
export class CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Cooldown extends Box<Proto016PtMumbaiVotingPeriodKind__Cooldown> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Cooldown {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Adoption generated for Proto016PtMumbaiVotingPeriodKind__Adoption
export class CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Adoption extends Box<Proto016PtMumbaiVotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto016PtMumbaiVotingPeriodKind__exploration = Unit;
export type Proto016PtMumbaiVotingPeriodKind__Proposal = Unit;
export type Proto016PtMumbaiVotingPeriodKind__Promotion = Unit;
export type Proto016PtMumbaiVotingPeriodKind__Cooldown = Unit;
export type Proto016PtMumbaiVotingPeriodKind__Adoption = Unit;
// Class CGRIDClass__Proto016_PtMumbaiVoting_period_kind generated for Proto016PtMumbaiVotingPeriodKind
export function proto016ptmumbaivotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto016PtMumbaiVotingPeriodKind,Proto016PtMumbaiVotingPeriodKind> {
    function f(disc: CGRIDTag__Proto016PtMumbaiVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto016PtMumbaiVotingPeriodKind.Proposal: return CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto016PtMumbaiVotingPeriodKind.exploration: return CGRIDClass__Proto016PtMumbaiVotingPeriodKind__exploration.decode;
            case CGRIDTag__Proto016PtMumbaiVotingPeriodKind.Cooldown: return CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Cooldown.decode;
            case CGRIDTag__Proto016PtMumbaiVotingPeriodKind.Promotion: return CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Promotion.decode;
            case CGRIDTag__Proto016PtMumbaiVotingPeriodKind.Adoption: return CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto016PtMumbaiVotingPeriodKind => Object.values(CGRIDTag__Proto016PtMumbaiVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto016_PtMumbaiVoting_period_kind extends Box<Proto016PtMumbaiVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto016PtMumbaiVotingPeriodKind>, Proto016PtMumbaiVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016_PtMumbaiVoting_period_kind {
        return new this(variant_decoder(width.Uint8)(proto016ptmumbaivotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__Proto016PtMumbaiVotingPeriodKind{
    Proposal = 0,
    exploration = 1,
    Cooldown = 2,
    Promotion = 3,
    Adoption = 4
}
export interface CGRIDMap__Proto016PtMumbaiVotingPeriodKind {
    Proposal: CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Proposal,
    exploration: CGRIDClass__Proto016PtMumbaiVotingPeriodKind__exploration,
    Cooldown: CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Cooldown,
    Promotion: CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Promotion,
    Adoption: CGRIDClass__Proto016PtMumbaiVotingPeriodKind__Adoption
}
export type Proto016PtMumbaiVotingPeriodKind = { kind: CGRIDTag__Proto016PtMumbaiVotingPeriodKind.Proposal, value: CGRIDMap__Proto016PtMumbaiVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto016PtMumbaiVotingPeriodKind.exploration, value: CGRIDMap__Proto016PtMumbaiVotingPeriodKind['exploration'] } | { kind: CGRIDTag__Proto016PtMumbaiVotingPeriodKind.Cooldown, value: CGRIDMap__Proto016PtMumbaiVotingPeriodKind['Cooldown'] } | { kind: CGRIDTag__Proto016PtMumbaiVotingPeriodKind.Promotion, value: CGRIDMap__Proto016PtMumbaiVotingPeriodKind['Promotion'] } | { kind: CGRIDTag__Proto016PtMumbaiVotingPeriodKind.Adoption, value: CGRIDMap__Proto016PtMumbaiVotingPeriodKind['Adoption'] };
export type Proto016PtMumbaiVotingPeriod = { index: Int32, kind: CGRIDClass__Proto016_PtMumbaiVoting_period_kind, start_position: Int32 };
export class CGRIDClass__Proto016PtMumbaiVotingPeriod extends Box<Proto016PtMumbaiVotingPeriod> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['index', 'kind', 'start_position']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto016PtMumbaiVotingPeriod {
        return new this(record_decoder<Proto016PtMumbaiVotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto016_PtMumbaiVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p));
    };
    get encodeLength(): number {
        return (this.value.index.encodeLength +  this.value.kind.encodeLength +  this.value.start_position.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.index.writeTarget(tgt) +  this.value.kind.writeTarget(tgt) +  this.value.start_position.writeTarget(tgt));
    }
}
export const proto016_ptmumbai_voting_period_encoder = (value: Proto016PtMumbaiVotingPeriod): OutputBytes => {
    return record_encoder({order: ['index', 'kind', 'start_position']})(value);
}
export const proto016_ptmumbai_voting_period_decoder = (p: Parser): Proto016PtMumbaiVotingPeriod => {
    return record_decoder<Proto016PtMumbaiVotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto016_PtMumbaiVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p);
}
