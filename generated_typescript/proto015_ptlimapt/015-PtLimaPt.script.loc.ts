import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int31 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto015PtLimaPtScriptLoc = Int31;
export class CGRIDClass__Proto015PtLimaPtScriptLoc extends Box<Proto015PtLimaPtScriptLoc> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtScriptLoc {
        return new this(Int31.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto015_ptlimapt_script_loc_encoder = (value: Proto015PtLimaPtScriptLoc): OutputBytes => {
    return value.encode();
}
export const proto015_ptlimapt_script_loc_decoder = (p: Parser): Proto015PtLimaPtScriptLoc => {
    return Int31.decode(p);
}
