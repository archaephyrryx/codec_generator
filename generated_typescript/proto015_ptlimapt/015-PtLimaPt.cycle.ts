import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto015PtLimaPtCycle = Int32;
export class CGRIDClass__Proto015PtLimaPtCycle extends Box<Proto015PtLimaPtCycle> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtCycle {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto015_ptlimapt_cycle_encoder = (value: Proto015PtLimaPtCycle): OutputBytes => {
    return value.encode();
}
export const proto015_ptlimapt_cycle_decoder = (p: Parser): Proto015PtLimaPtCycle => {
    return Int32.decode(p);
}
