import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__sapling_state generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__sapling_state
export class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__sapling_state extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__sapling_state> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['id', 'diff']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__sapling_state {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__sapling_state>({id: Z.decode, diff: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff.decode}, {order: ['id', 'diff']})(p));
    };
    get encodeLength(): number {
        return (this.value.id.encodeLength +  this.value.diff.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.id.writeTarget(tgt) +  this.value.diff.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__big_map generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__big_map
export class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__big_map extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__big_map> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['id', 'diff']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__big_map {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__big_map>({id: Z.decode, diff: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff.decode}, {order: ['id', 'diff']})(p));
    };
    get encodeLength(): number {
        return (this.value.id.encodeLength +  this.value.diff.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.id.writeTarget(tgt) +  this.value.diff.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update
export class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update>({action: Unit.decode, updates: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates.decode}, {order: ['action', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove
export class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove>({action: Unit.decode}, {order: ['action']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy
export class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'source', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy>({action: Unit.decode, source: Z.decode, updates: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates.decode}, {order: ['action', 'source', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.source.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.source.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc
export class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates', 'memo_size']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc>({action: Unit.decode, updates: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates.decode, memo_size: Uint16.decode}, {order: ['action', 'updates', 'memo_size']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength +  this.value.memo_size.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt) +  this.value.memo_size.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__update generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__update
export class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__update extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__update> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__update {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__update>({action: Unit.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['action', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove
export class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove>({action: Unit.decode}, {order: ['action']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy
export class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'source', 'updates']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy>({action: Unit.decode, source: Z.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['action', 'source', 'updates']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.source.encodeLength +  this.value.updates.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.source.writeTarget(tgt) +  this.value.updates.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc
export class CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['action', 'updates', 'key_type', 'value_type']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc>({action: Unit.decode, updates: Dynamic.decode(Sequence.decode(CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq.decode), width.Uint30), key_type: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode, value_type: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode}, {order: ['action', 'updates', 'key_type', 'value_type']})(p));
    };
    get encodeLength(): number {
        return (this.value.action.encodeLength +  this.value.updates.encodeLength +  this.value.key_type.encodeLength +  this.value.value_type.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.action.writeTarget(tgt) +  this.value.updates.writeTarget(tgt) +  this.value.key_type.writeTarget(tgt) +  this.value.value_type.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__String generated for MichelineProto015PtLimaPtMichelsonV1Expression__String
export class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__String extends Box<MichelineProto015PtLimaPtMichelsonV1Expression__String> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['_string']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__String {
        return new this(record_decoder<MichelineProto015PtLimaPtMichelsonV1Expression__String>({_string: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['_string']})(p));
    };
    get encodeLength(): number {
        return (this.value._string.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value._string.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Sequence generated for MichelineProto015PtLimaPtMichelsonV1Expression__Sequence
export class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Sequence extends Box<MichelineProto015PtLimaPtMichelsonV1Expression__Sequence> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Sequence {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__some_annots generated for MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__some_annots
export class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__some_annots extends Box<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__some_annots {
        return new this(record_decoder<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__some_annots>({prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__no_annots generated for MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__no_annots
export class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__no_annots extends Box<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__no_annots {
        return new this(record_decoder<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__no_annots>({prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives.decode}, {order: ['prim']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__generic generated for MichelineProto015PtLimaPtMichelsonV1Expression__Prim__generic
export class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__generic extends Box<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__generic> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'args', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__generic {
        return new this(record_decoder<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__generic>({prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives.decode, args: Dynamic.decode(Sequence.decode(CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode), width.Uint30), annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'args', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.args.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.args.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__some_annots generated for MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__some_annots
export class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__some_annots extends Box<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__some_annots {
        return new this(record_decoder<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__some_annots>({prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg1', 'arg2', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__no_annots generated for MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__no_annots
export class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__no_annots extends Box<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg1', 'arg2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__no_annots {
        return new this(record_decoder<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__no_annots>({prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives.decode, arg1: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode, arg2: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode}, {order: ['prim', 'arg1', 'arg2']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg1.encodeLength +  this.value.arg2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg1.writeTarget(tgt) +  this.value.arg2.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__some_annots generated for MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__some_annots
export class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__some_annots extends Box<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__some_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg', 'annots']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__some_annots {
        return new this(record_decoder<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__some_annots>({prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode, annots: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['prim', 'arg', 'annots']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength +  this.value.annots.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt) +  this.value.annots.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__no_annots generated for MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__no_annots
export class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__no_annots extends Box<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__no_annots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['prim', 'arg']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__no_annots {
        return new this(record_decoder<MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__no_annots>({prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives.decode, arg: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode}, {order: ['prim', 'arg']})(p));
    };
    get encodeLength(): number {
        return (this.value.prim.encodeLength +  this.value.arg.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.prim.writeTarget(tgt) +  this.value.arg.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Int generated for MichelineProto015PtLimaPtMichelsonV1Expression__Int
export class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Int extends Box<MichelineProto015PtLimaPtMichelsonV1Expression__Int> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['int']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Int {
        return new this(record_decoder<MichelineProto015PtLimaPtMichelsonV1Expression__Int>({int: Z.decode}, {order: ['int']})(p));
    };
    get encodeLength(): number {
        return (this.value.int.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.int.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Bytes generated for MichelineProto015PtLimaPtMichelsonV1Expression__Bytes
export class CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Bytes extends Box<MichelineProto015PtLimaPtMichelsonV1Expression__Bytes> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bytes']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Bytes {
        return new this(record_decoder<MichelineProto015PtLimaPtMichelsonV1Expression__Bytes>({bytes: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['bytes']})(p));
    };
    get encodeLength(): number {
        return (this.value.bytes.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bytes.writeTarget(tgt));
    }
}
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__sapling_state = { id: Z, diff: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__big_map = { id: Z, diff: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update = { action: Unit, updates: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove = { action: Unit };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy = { action: Unit, source: Z, updates: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc = { action: Unit, updates: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates, memo_size: Uint16 };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__update = { action: Unit, updates: Dynamic<Sequence<CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq>,width.Uint30> };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove = { action: Unit };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy = { action: Unit, source: Z, updates: Dynamic<Sequence<CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq>,width.Uint30> };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc = { action: Unit, updates: Dynamic<Sequence<CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq>,width.Uint30>, key_type: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression, value_type: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression };
export type MichelineProto015PtLimaPtMichelsonV1Expression__String = { _string: Dynamic<U8String,width.Uint30> };
export type MichelineProto015PtLimaPtMichelsonV1Expression__Sequence = Dynamic<Sequence<CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression>,width.Uint30>;
export type MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__some_annots = { prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__no_annots = { prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives };
export type MichelineProto015PtLimaPtMichelsonV1Expression__Prim__generic = { prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives, args: Dynamic<Sequence<CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression>,width.Uint30>, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__some_annots = { prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression, arg2: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__no_annots = { prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives, arg1: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression, arg2: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression };
export type MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__some_annots = { prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives, arg: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression, annots: Dynamic<U8String,width.Uint30> };
export type MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__no_annots = { prim: CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives, arg: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression };
export type MichelineProto015PtLimaPtMichelsonV1Expression__Int = { int: Z };
export type MichelineProto015PtLimaPtMichelsonV1Expression__Bytes = { bytes: Dynamic<Bytes,width.Uint30> };
// Class CGRIDClass__SaplingTransactionCiphertext generated for SaplingTransactionCiphertext
export class CGRIDClass__SaplingTransactionCiphertext extends Box<SaplingTransactionCiphertext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SaplingTransactionCiphertext {
        return new this(record_decoder<SaplingTransactionCiphertext>({cv: FixedBytes.decode<32>({len: 32}), epk: FixedBytes.decode<32>({len: 32}), payload_enc: Dynamic.decode(Bytes.decode, width.Uint30), nonce_enc: FixedBytes.decode<24>({len: 24}), payload_out: FixedBytes.decode<80>({len: 80}), nonce_out: FixedBytes.decode<24>({len: 24})}, {order: ['cv', 'epk', 'payload_enc', 'nonce_enc', 'payload_out', 'nonce_out']})(p));
    };
    get encodeLength(): number {
        return (this.value.cv.encodeLength +  this.value.epk.encodeLength +  this.value.payload_enc.encodeLength +  this.value.nonce_enc.encodeLength +  this.value.payload_out.encodeLength +  this.value.nonce_out.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cv.writeTarget(tgt) +  this.value.epk.writeTarget(tgt) +  this.value.payload_enc.writeTarget(tgt) +  this.value.nonce_enc.writeTarget(tgt) +  this.value.payload_out.writeTarget(tgt) +  this.value.nonce_out.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives generated for Proto015PtLimaPtMichelsonV1Primitives
export class CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives extends Box<Proto015PtLimaPtMichelsonV1Primitives> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<Proto015PtLimaPtMichelsonV1Primitives>(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtMichelsonV1Primitives {
        return new this(enum_decoder(width.Uint8)((x): x is Proto015PtLimaPtMichelsonV1Primitives => (Object.values(Proto015PtLimaPtMichelsonV1Primitives).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq {
        return new this(tuple_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq>(FixedBytes.decode<32>({len: 32}), CGRIDClass__SaplingTransactionCiphertext.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['commitments_and_ciphertexts', 'nullifiers']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates>({commitments_and_ciphertexts: Dynamic.decode(Sequence.decode(CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq.decode), width.Uint30), nullifiers: Dynamic.decode(Sequence.decode(FixedBytes.decode<32>({len: 32})), width.Uint30)}, {order: ['commitments_and_ciphertexts', 'nullifiers']})(p));
    };
    get encodeLength(): number {
        return (this.value.commitments_and_ciphertexts.encodeLength +  this.value.nullifiers.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.commitments_and_ciphertexts.writeTarget(tgt) +  this.value.nullifiers.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff
export function proto015ptlimaptlazystoragediffdenestdyndenestseqsaplingstatediff_mkDecoder(): VariantDecoder<CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff,Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff> {
    function f(disc: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff) {
        switch (disc) {
            case CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.update: return CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update.decode;
            case CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.remove: return CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove.decode;
            case CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.copy: return CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy.decode;
            case CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.alloc: return CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff => Object.values(CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff).includes(tagval);
    return f;
}
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff>, Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff {
        return new this(variant_decoder(width.Uint8)(proto015ptlimaptlazystoragediffdenestdyndenestseqsaplingstatediff_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['script_expr']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash>({script_expr: FixedBytes.decode<32>({len: 32})}, {order: ['script_expr']})(p));
    };
    get encodeLength(): number {
        return (this.value.script_expr.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.script_expr.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['key_hash', 'key', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq {
        return new this(record_decoder<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq>({key_hash: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash.decode, key: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode, value: Option.decode(CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression.decode)}, {order: ['key_hash', 'key', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.key_hash.encodeLength +  this.value.key.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.key_hash.writeTarget(tgt) +  this.value.key.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff
export function proto015ptlimaptlazystoragediffdenestdyndenestseqbigmapdiff_mkDecoder(): VariantDecoder<CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff,Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff> {
    function f(disc: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff) {
        switch (disc) {
            case CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff.update: return CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__update.decode;
            case CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff.remove: return CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove.decode;
            case CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff.copy: return CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy.decode;
            case CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff.alloc: return CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff => Object.values(CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff).includes(tagval);
    return f;
}
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff>, Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff {
        return new this(variant_decoder(width.Uint8)(proto015ptlimaptlazystoragediffdenestdyndenestseqbigmapdiff_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq generated for Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq
export function proto015ptlimaptlazystoragediffdenestdyndenestseq_mkDecoder(): VariantDecoder<CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq,Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq> {
    function f(disc: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq) {
        switch (disc) {
            case CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq.big_map: return CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__big_map.decode;
            case CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq.sapling_state: return CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__sapling_state.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq => Object.values(CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq).includes(tagval);
    return f;
}
export class CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq extends Box<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq>, Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq {
        return new this(variant_decoder(width.Uint8)(proto015ptlimaptlazystoragediffdenestdyndenestseq_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression generated for MichelineProto015PtLimaPtMichelsonV1Expression
export function michelineproto015ptlimaptmichelsonv1expression_mkDecoder(): VariantDecoder<CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression,MichelineProto015PtLimaPtMichelsonV1Expression> {
    function f(disc: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression) {
        switch (disc) {
            case CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Int: return CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Int.decode;
            case CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.String: return CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__String.decode;
            case CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Sequence: return CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Sequence.decode;
            case CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__no_args__no_annots: return CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__no_annots.decode;
            case CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__no_args__some_annots: return CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__some_annots.decode;
            case CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__1_arg__no_annots: return CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__no_annots.decode;
            case CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__1_arg__some_annots: return CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__some_annots.decode;
            case CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__2_args__no_annots: return CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__no_annots.decode;
            case CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__2_args__some_annots: return CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__some_annots.decode;
            case CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__generic: return CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__generic.decode;
            case CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Bytes: return CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Bytes.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression => Object.values(CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression).includes(tagval);
    return f;
}
export class CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression extends Box<MichelineProto015PtLimaPtMichelsonV1Expression> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<MichelineProto015PtLimaPtMichelsonV1Expression>, MichelineProto015PtLimaPtMichelsonV1Expression>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression {
        return new this(variant_decoder(width.Uint8)(michelineproto015ptlimaptmichelsonv1expression_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export type SaplingTransactionCiphertext = { cv: FixedBytes<32>, epk: FixedBytes<32>, payload_enc: Dynamic<Bytes,width.Uint30>, nonce_enc: FixedBytes<24>, payload_out: FixedBytes<80>, nonce_out: FixedBytes<24> };
export enum CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression{
    Int = 0,
    String = 1,
    Sequence = 2,
    Prim__no_args__no_annots = 3,
    Prim__no_args__some_annots = 4,
    Prim__1_arg__no_annots = 5,
    Prim__1_arg__some_annots = 6,
    Prim__2_args__no_annots = 7,
    Prim__2_args__some_annots = 8,
    Prim__generic = 9,
    Bytes = 10
}
export interface CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression {
    Int: CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Int,
    String: CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__String,
    Sequence: CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Sequence,
    Prim__no_args__no_annots: CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__no_annots,
    Prim__no_args__some_annots: CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__no_args__some_annots,
    Prim__1_arg__no_annots: CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__no_annots,
    Prim__1_arg__some_annots: CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__1_arg__some_annots,
    Prim__2_args__no_annots: CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__no_annots,
    Prim__2_args__some_annots: CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__2_args__some_annots,
    Prim__generic: CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Prim__generic,
    Bytes: CGRIDClass__MichelineProto015PtLimaPtMichelsonV1Expression__Bytes
}
export type MichelineProto015PtLimaPtMichelsonV1Expression = { kind: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Int, value: CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression['Int'] } | { kind: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.String, value: CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression['String'] } | { kind: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Sequence, value: CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression['Sequence'] } | { kind: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__no_args__no_annots, value: CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression['Prim__no_args__no_annots'] } | { kind: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__no_args__some_annots, value: CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression['Prim__no_args__some_annots'] } | { kind: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__1_arg__no_annots, value: CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression['Prim__1_arg__no_annots'] } | { kind: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__1_arg__some_annots, value: CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression['Prim__1_arg__some_annots'] } | { kind: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__2_args__no_annots, value: CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression['Prim__2_args__no_annots'] } | { kind: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__2_args__some_annots, value: CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression['Prim__2_args__some_annots'] } | { kind: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Prim__generic, value: CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression['Prim__generic'] } | { kind: CGRIDTag__MichelineProto015PtLimaPtMichelsonV1Expression.Bytes, value: CGRIDMap__MichelineProto015PtLimaPtMichelsonV1Expression['Bytes'] };
export enum Proto015PtLimaPtMichelsonV1Primitives{
    parameter = 0,
    storage = 1,
    code = 2,
    False = 3,
    Elt = 4,
    Left = 5,
    None = 6,
    Pair = 7,
    Right = 8,
    Some = 9,
    True = 10,
    Unit = 11,
    PACK = 12,
    UNPACK = 13,
    BLAKE2B = 14,
    SHA256 = 15,
    SHA512 = 16,
    ABS = 17,
    ADD = 18,
    AMOUNT = 19,
    AND = 20,
    BALANCE = 21,
    CAR = 22,
    CDR = 23,
    CHECK_SIGNATURE = 24,
    COMPARE = 25,
    CONCAT = 26,
    CONS = 27,
    CREATE_ACCOUNT = 28,
    CREATE_CONTRACT = 29,
    IMPLICIT_ACCOUNT = 30,
    DIP = 31,
    DROP = 32,
    DUP = 33,
    EDIV = 34,
    EMPTY_MAP = 35,
    EMPTY_SET = 36,
    EQ = 37,
    EXEC = 38,
    FAILWITH = 39,
    GE = 40,
    GET = 41,
    GT = 42,
    HASH_KEY = 43,
    IF = 44,
    IF_CONS = 45,
    IF_LEFT = 46,
    IF_NONE = 47,
    INT = 48,
    LAMBDA = 49,
    LE = 50,
    LEFT = 51,
    LOOP = 52,
    LSL = 53,
    LSR = 54,
    LT = 55,
    MAP = 56,
    MEM = 57,
    MUL = 58,
    NEG = 59,
    NEQ = 60,
    NIL = 61,
    NONE = 62,
    NOT = 63,
    NOW = 64,
    OR = 65,
    PAIR = 66,
    PUSH = 67,
    RIGHT = 68,
    SIZE = 69,
    SOME = 70,
    SOURCE = 71,
    SENDER = 72,
    SELF = 73,
    STEPS_TO_QUOTA = 74,
    SUB = 75,
    SWAP = 76,
    TRANSFER_TOKENS = 77,
    SET_DELEGATE = 78,
    UNIT = 79,
    UPDATE = 80,
    XOR = 81,
    ITER = 82,
    LOOP_LEFT = 83,
    ADDRESS = 84,
    CONTRACT = 85,
    ISNAT = 86,
    CAST = 87,
    RENAME = 88,
    bool = 89,
    contract = 90,
    int = 91,
    key = 92,
    key_hash = 93,
    lambda = 94,
    list = 95,
    map = 96,
    big_map = 97,
    nat = 98,
    option = 99,
    or = 100,
    pair = 101,
    _set = 102,
    signature = 103,
    _string = 104,
    bytes = 105,
    mutez = 106,
    timestamp = 107,
    unit = 108,
    operation = 109,
    address = 110,
    SLICE = 111,
    DIG = 112,
    DUG = 113,
    EMPTY_BIG_MAP = 114,
    APPLY = 115,
    chain_id = 116,
    CHAIN_ID = 117,
    LEVEL = 118,
    SELF_ADDRESS = 119,
    never = 120,
    NEVER = 121,
    UNPAIR = 122,
    VOTING_POWER = 123,
    TOTAL_VOTING_POWER = 124,
    KECCAK = 125,
    SHA3 = 126,
    PAIRING_CHECK = 127,
    bls12_381_g1 = 128,
    bls12_381_g2 = 129,
    bls12_381_fr = 130,
    sapling_state = 131,
    sapling_transaction_deprecated = 132,
    SAPLING_EMPTY_STATE = 133,
    SAPLING_VERIFY_UPDATE = 134,
    ticket = 135,
    TICKET_DEPRECATED = 136,
    READ_TICKET = 137,
    SPLIT_TICKET = 138,
    JOIN_TICKETS = 139,
    GET_AND_UPDATE = 140,
    chest = 141,
    chest_key = 142,
    OPEN_CHEST = 143,
    VIEW = 144,
    view = 145,
    constant = 146,
    SUB_MUTEZ = 147,
    tx_rollup_l2_address = 148,
    MIN_BLOCK_TIME = 149,
    sapling_transaction = 150,
    EMIT = 151,
    Lambda_rec = 152,
    LAMBDA_REC = 153,
    TICKET = 154
}
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffUpdateUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_update_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffCopyUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_copy_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdatesCommitmentsAndCiphertextsDenestDynDenestSeq = [FixedBytes<32>, CGRIDClass__SaplingTransactionCiphertext];
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiffAllocUpdates = { commitments_and_ciphertexts: Dynamic<Sequence<CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_sapling_state_diff_alloc_updates_commitments_and_ciphertexts_denest_dyn_denest_seq>,width.Uint30>, nullifiers: Dynamic<Sequence<FixedBytes<32>>,width.Uint30> };
export enum CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff{
    update = 0,
    remove = 1,
    copy = 2,
    alloc = 3
}
export interface CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff {
    update: CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__update,
    remove: CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__remove,
    copy: CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__copy,
    alloc: CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff__alloc
}
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff = { kind: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.update, value: CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['update'] } | { kind: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.remove, value: CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['remove'] } | { kind: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.copy, value: CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['copy'] } | { kind: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff.alloc, value: CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqSaplingStateDiff['alloc'] };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffUpdateUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_update_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression> };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffCopyUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_copy_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression> };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeqKeyHash = { script_expr: FixedBytes<32> };
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiffAllocUpdatesDenestDynDenestSeq = { key_hash: CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq_big_map_diff_alloc_updates_denest_dyn_denest_seq_key_hash, key: CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression, value: Option<CGRIDClass__MichelineProto015_PtLimaPtMichelson_v1Expression> };
export enum CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff{
    update = 0,
    remove = 1,
    copy = 2,
    alloc = 3
}
export interface CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff {
    update: CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__update,
    remove: CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__remove,
    copy: CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__copy,
    alloc: CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff__alloc
}
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff = { kind: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff.update, value: CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff['update'] } | { kind: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff.remove, value: CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff['remove'] } | { kind: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff.copy, value: CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff['copy'] } | { kind: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff.alloc, value: CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeqBigMapDiff['alloc'] };
export enum CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq{
    big_map = 0,
    sapling_state = 1
}
export interface CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq {
    big_map: CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__big_map,
    sapling_state: CGRIDClass__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq__sapling_state
}
export type Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq = { kind: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq.big_map, value: CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq['big_map'] } | { kind: CGRIDTag__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq.sapling_state, value: CGRIDMap__Proto015PtLimaPtLazyStorageDiffDenestDynDenestSeq['sapling_state'] };
export type Proto015PtLimaPtLazyStorageDiff = Dynamic<Sequence<CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq>,width.Uint30>;
export class CGRIDClass__Proto015PtLimaPtLazyStorageDiff extends Box<Proto015PtLimaPtLazyStorageDiff> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtLazyStorageDiff {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto015_ptlimapt_lazy_storage_diff_encoder = (value: Proto015PtLimaPtLazyStorageDiff): OutputBytes => {
    return value.encode();
}
export const proto015_ptlimapt_lazy_storage_diff_decoder = (p: Parser): Proto015PtLimaPtLazyStorageDiff => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto015_PtLimaPtLazy_storage_diff_denest_dyn_denest_seq.decode), width.Uint30)(p);
}
