import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
export type Proto015PtLimaPtErrors = Dynamic<U8String,width.Uint30>;
export class CGRIDClass__Proto015PtLimaPtErrors extends Box<Proto015PtLimaPtErrors> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtErrors {
        return new this(Dynamic.decode(U8String.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto015_ptlimapt_errors_encoder = (value: Proto015PtLimaPtErrors): OutputBytes => {
    return value.encode();
}
export const proto015_ptlimapt_errors_decoder = (p: Parser): Proto015PtLimaPtErrors => {
    return Dynamic.decode(U8String.decode, width.Uint30)(p);
}
