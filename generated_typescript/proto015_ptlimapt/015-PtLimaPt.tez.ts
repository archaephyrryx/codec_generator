import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto015PtLimaPtTez = N;
export class CGRIDClass__Proto015PtLimaPtTez extends Box<Proto015PtLimaPtTez> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtTez {
        return new this(N.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto015_ptlimapt_tez_encoder = (value: Proto015PtLimaPtTez): OutputBytes => {
    return value.encode();
}
export const proto015_ptlimapt_tez_decoder = (p: Parser): Proto015PtLimaPtTez => {
    return N.decode(p);
}
