import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int64, Int8, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash generated for Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash extends Box<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature generated for Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature extends Box<Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_predecessor generated for BlockHeaderShellPredecessor
export class CGRIDClass__Block_headerShell_predecessor extends Box<BlockHeaderShellPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_predecessor {
        return new this(record_decoder<BlockHeaderShellPredecessor>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_operations_hash generated for BlockHeaderShellOperationsHash
export class CGRIDClass__Block_headerShell_operations_hash extends Box<BlockHeaderShellOperationsHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_list_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_operations_hash {
        return new this(record_decoder<BlockHeaderShellOperationsHash>({operation_list_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_list_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_list_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_list_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_context generated for BlockHeaderShellContext
export class CGRIDClass__Block_headerShell_context extends Box<BlockHeaderShellContext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_context {
        return new this(record_decoder<BlockHeaderShellContext>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
export type BlockHeaderShellPredecessor = { block_hash: FixedBytes<32> };
export type BlockHeaderShellOperationsHash = { operation_list_list_hash: FixedBytes<32> };
export type BlockHeaderShellContext = { context_hash: FixedBytes<32> };
export type Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto015PtLimaPtBlockHeader = { level: Int32, proto: Uint8, predecessor: CGRIDClass__Block_headerShell_predecessor, timestamp: Int64, validation_pass: Uint8, operations_hash: CGRIDClass__Block_headerShell_operations_hash, fitness: Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>, context: CGRIDClass__Block_headerShell_context, payload_hash: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_toggle_vote: Int8, signature: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature };
export class CGRIDClass__Proto015PtLimaPtBlockHeader extends Box<Proto015PtLimaPtBlockHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtBlockHeader {
        return new this(record_decoder<Proto015PtLimaPtBlockHeader>({level: Int32.decode, proto: Uint8.decode, predecessor: CGRIDClass__Block_headerShell_predecessor.decode, timestamp: Int64.decode, validation_pass: Uint8.decode, operations_hash: CGRIDClass__Block_headerShell_operations_hash.decode, fitness: Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30), context: CGRIDClass__Block_headerShell_context.decode, payload_hash: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature.decode}, {order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.proto.encodeLength +  this.value.predecessor.encodeLength +  this.value.timestamp.encodeLength +  this.value.validation_pass.encodeLength +  this.value.operations_hash.encodeLength +  this.value.fitness.encodeLength +  this.value.context.encodeLength +  this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_toggle_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.proto.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.timestamp.writeTarget(tgt) +  this.value.validation_pass.writeTarget(tgt) +  this.value.operations_hash.writeTarget(tgt) +  this.value.fitness.writeTarget(tgt) +  this.value.context.writeTarget(tgt) +  this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_toggle_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export const proto015_ptlimapt_block_header_encoder = (value: Proto015PtLimaPtBlockHeader): OutputBytes => {
    return record_encoder({order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(value);
}
export const proto015_ptlimapt_block_header_decoder = (p: Parser): Proto015PtLimaPtBlockHeader => {
    return record_decoder<Proto015PtLimaPtBlockHeader>({level: Int32.decode, proto: Uint8.decode, predecessor: CGRIDClass__Block_headerShell_predecessor.decode, timestamp: Int64.decode, validation_pass: Uint8.decode, operations_hash: CGRIDClass__Block_headerShell_operations_hash.decode, fitness: Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30), context: CGRIDClass__Block_headerShell_context.decode, payload_hash: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature.decode}, {order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p);
}
