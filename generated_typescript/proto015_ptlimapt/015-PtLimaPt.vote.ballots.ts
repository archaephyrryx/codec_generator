import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto015PtLimaPtVoteBallots = { yay: Int64, nay: Int64, pass: Int64 };
export class CGRIDClass__Proto015PtLimaPtVoteBallots extends Box<Proto015PtLimaPtVoteBallots> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['yay', 'nay', 'pass']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtVoteBallots {
        return new this(record_decoder<Proto015PtLimaPtVoteBallots>({yay: Int64.decode, nay: Int64.decode, pass: Int64.decode}, {order: ['yay', 'nay', 'pass']})(p));
    };
    get encodeLength(): number {
        return (this.value.yay.encodeLength +  this.value.nay.encodeLength +  this.value.pass.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.yay.writeTarget(tgt) +  this.value.nay.writeTarget(tgt) +  this.value.pass.writeTarget(tgt));
    }
}
export const proto015_ptlimapt_vote_ballots_encoder = (value: Proto015PtLimaPtVoteBallots): OutputBytes => {
    return record_encoder({order: ['yay', 'nay', 'pass']})(value);
}
export const proto015_ptlimapt_vote_ballots_decoder = (p: Parser): Proto015PtLimaPtVoteBallots => {
    return record_decoder<Proto015PtLimaPtVoteBallots>({yay: Int64.decode, nay: Int64.decode, pass: Int64.decode}, {order: ['yay', 'nay', 'pass']})(p);
}
