import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto015PtLimaPtVotingPeriodKind__exploration generated for Proto015PtLimaPtVotingPeriodKind__exploration
export class CGRIDClass__Proto015PtLimaPtVotingPeriodKind__exploration extends Box<Proto015PtLimaPtVotingPeriodKind__exploration> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtVotingPeriodKind__exploration {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Proposal generated for Proto015PtLimaPtVotingPeriodKind__Proposal
export class CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Proposal extends Box<Proto015PtLimaPtVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Promotion generated for Proto015PtLimaPtVotingPeriodKind__Promotion
export class CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Promotion extends Box<Proto015PtLimaPtVotingPeriodKind__Promotion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Promotion {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Cooldown generated for Proto015PtLimaPtVotingPeriodKind__Cooldown
export class CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Cooldown extends Box<Proto015PtLimaPtVotingPeriodKind__Cooldown> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Cooldown {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Adoption generated for Proto015PtLimaPtVotingPeriodKind__Adoption
export class CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Adoption extends Box<Proto015PtLimaPtVotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto015PtLimaPtVotingPeriodKind__exploration = Unit;
export type Proto015PtLimaPtVotingPeriodKind__Proposal = Unit;
export type Proto015PtLimaPtVotingPeriodKind__Promotion = Unit;
export type Proto015PtLimaPtVotingPeriodKind__Cooldown = Unit;
export type Proto015PtLimaPtVotingPeriodKind__Adoption = Unit;
// Class CGRIDClass__Proto015_PtLimaPtVoting_period_kind generated for Proto015PtLimaPtVotingPeriodKind
export function proto015ptlimaptvotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto015PtLimaPtVotingPeriodKind,Proto015PtLimaPtVotingPeriodKind> {
    function f(disc: CGRIDTag__Proto015PtLimaPtVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto015PtLimaPtVotingPeriodKind.Proposal: return CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto015PtLimaPtVotingPeriodKind.exploration: return CGRIDClass__Proto015PtLimaPtVotingPeriodKind__exploration.decode;
            case CGRIDTag__Proto015PtLimaPtVotingPeriodKind.Cooldown: return CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Cooldown.decode;
            case CGRIDTag__Proto015PtLimaPtVotingPeriodKind.Promotion: return CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Promotion.decode;
            case CGRIDTag__Proto015PtLimaPtVotingPeriodKind.Adoption: return CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto015PtLimaPtVotingPeriodKind => Object.values(CGRIDTag__Proto015PtLimaPtVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto015_PtLimaPtVoting_period_kind extends Box<Proto015PtLimaPtVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto015PtLimaPtVotingPeriodKind>, Proto015PtLimaPtVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtVoting_period_kind {
        return new this(variant_decoder(width.Uint8)(proto015ptlimaptvotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__Proto015PtLimaPtVotingPeriodKind{
    Proposal = 0,
    exploration = 1,
    Cooldown = 2,
    Promotion = 3,
    Adoption = 4
}
export interface CGRIDMap__Proto015PtLimaPtVotingPeriodKind {
    Proposal: CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Proposal,
    exploration: CGRIDClass__Proto015PtLimaPtVotingPeriodKind__exploration,
    Cooldown: CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Cooldown,
    Promotion: CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Promotion,
    Adoption: CGRIDClass__Proto015PtLimaPtVotingPeriodKind__Adoption
}
export type Proto015PtLimaPtVotingPeriodKind = { kind: CGRIDTag__Proto015PtLimaPtVotingPeriodKind.Proposal, value: CGRIDMap__Proto015PtLimaPtVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto015PtLimaPtVotingPeriodKind.exploration, value: CGRIDMap__Proto015PtLimaPtVotingPeriodKind['exploration'] } | { kind: CGRIDTag__Proto015PtLimaPtVotingPeriodKind.Cooldown, value: CGRIDMap__Proto015PtLimaPtVotingPeriodKind['Cooldown'] } | { kind: CGRIDTag__Proto015PtLimaPtVotingPeriodKind.Promotion, value: CGRIDMap__Proto015PtLimaPtVotingPeriodKind['Promotion'] } | { kind: CGRIDTag__Proto015PtLimaPtVotingPeriodKind.Adoption, value: CGRIDMap__Proto015PtLimaPtVotingPeriodKind['Adoption'] };
export type Proto015PtLimaPtVotingPeriod = { index: Int32, kind: CGRIDClass__Proto015_PtLimaPtVoting_period_kind, start_position: Int32 };
export class CGRIDClass__Proto015PtLimaPtVotingPeriod extends Box<Proto015PtLimaPtVotingPeriod> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['index', 'kind', 'start_position']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtVotingPeriod {
        return new this(record_decoder<Proto015PtLimaPtVotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto015_PtLimaPtVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p));
    };
    get encodeLength(): number {
        return (this.value.index.encodeLength +  this.value.kind.encodeLength +  this.value.start_position.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.index.writeTarget(tgt) +  this.value.kind.writeTarget(tgt) +  this.value.start_position.writeTarget(tgt));
    }
}
export const proto015_ptlimapt_voting_period_encoder = (value: Proto015PtLimaPtVotingPeriod): OutputBytes => {
    return record_encoder({order: ['index', 'kind', 'start_position']})(value);
}
export const proto015_ptlimapt_voting_period_decoder = (p: Parser): Proto015PtLimaPtVotingPeriod => {
    return record_decoder<Proto015PtLimaPtVotingPeriod>({index: Int32.decode, kind: CGRIDClass__Proto015_PtLimaPtVoting_period_kind.decode, start_position: Int32.decode}, {order: ['index', 'kind', 'start_position']})(p);
}
