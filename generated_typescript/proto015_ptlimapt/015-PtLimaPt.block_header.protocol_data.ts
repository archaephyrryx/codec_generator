import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash generated for Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash
export class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash extends Box<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['value_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash {
        return new this(record_decoder<Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash>({value_hash: FixedBytes.decode<32>({len: 32})}, {order: ['value_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.value_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.value_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature generated for Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature extends Box<Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
export type Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto015PtLimaPtBlockHeaderAlphaUnsignedContentsPayloadHash = { value_hash: FixedBytes<32> };
export type Proto015PtLimaPtBlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto015PtLimaPtBlockHeaderProtocolData = { payload_hash: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash, payload_round: Int32, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, liquidity_baking_toggle_vote: Int8, signature: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature };
export class CGRIDClass__Proto015PtLimaPtBlockHeaderProtocolData extends Box<Proto015PtLimaPtBlockHeaderProtocolData> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtBlockHeaderProtocolData {
        return new this(record_decoder<Proto015PtLimaPtBlockHeaderProtocolData>({payload_hash: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.payload_hash.encodeLength +  this.value.payload_round.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.liquidity_baking_toggle_vote.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.payload_hash.writeTarget(tgt) +  this.value.payload_round.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.liquidity_baking_toggle_vote.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export const proto015_ptlimapt_block_header_protocol_data_encoder = (value: Proto015PtLimaPtBlockHeaderProtocolData): OutputBytes => {
    return record_encoder({order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(value);
}
export const proto015_ptlimapt_block_header_protocol_data_decoder = (p: Parser): Proto015PtLimaPtBlockHeaderProtocolData => {
    return record_decoder<Proto015PtLimaPtBlockHeaderProtocolData>({payload_hash: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_payload_hash.decode, payload_round: Int32.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), liquidity_baking_toggle_vote: Int8.decode, signature: CGRIDClass__Proto015_PtLimaPtBlock_headerAlphaSigned_contents_signature.decode}, {order: ['payload_hash', 'payload_round', 'proof_of_work_nonce', 'seed_nonce_hash', 'liquidity_baking_toggle_vote', 'signature']})(p);
}
