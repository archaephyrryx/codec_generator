import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto015PtLimaPtFitnessLockedRound__Some generated for Proto015PtLimaPtFitnessLockedRound__Some
export class CGRIDClass__Proto015PtLimaPtFitnessLockedRound__Some extends Box<Proto015PtLimaPtFitnessLockedRound__Some> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtFitnessLockedRound__Some {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto015PtLimaPtFitnessLockedRound__None generated for Proto015PtLimaPtFitnessLockedRound__None
export class CGRIDClass__Proto015PtLimaPtFitnessLockedRound__None extends Box<Proto015PtLimaPtFitnessLockedRound__None> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtFitnessLockedRound__None {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto015PtLimaPtFitnessLockedRound__Some = Int32;
export type Proto015PtLimaPtFitnessLockedRound__None = Unit;
// Class CGRIDClass__Proto015_PtLimaPtFitness_locked_round generated for Proto015PtLimaPtFitnessLockedRound
export function proto015ptlimaptfitnesslockedround_mkDecoder(): VariantDecoder<CGRIDTag__Proto015PtLimaPtFitnessLockedRound,Proto015PtLimaPtFitnessLockedRound> {
    function f(disc: CGRIDTag__Proto015PtLimaPtFitnessLockedRound) {
        switch (disc) {
            case CGRIDTag__Proto015PtLimaPtFitnessLockedRound.None: return CGRIDClass__Proto015PtLimaPtFitnessLockedRound__None.decode;
            case CGRIDTag__Proto015PtLimaPtFitnessLockedRound.Some: return CGRIDClass__Proto015PtLimaPtFitnessLockedRound__Some.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto015PtLimaPtFitnessLockedRound => Object.values(CGRIDTag__Proto015PtLimaPtFitnessLockedRound).includes(tagval);
    return f;
}
export class CGRIDClass__Proto015_PtLimaPtFitness_locked_round extends Box<Proto015PtLimaPtFitnessLockedRound> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto015PtLimaPtFitnessLockedRound>, Proto015PtLimaPtFitnessLockedRound>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015_PtLimaPtFitness_locked_round {
        return new this(variant_decoder(width.Uint8)(proto015ptlimaptfitnesslockedround_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__Proto015PtLimaPtFitnessLockedRound{
    None = 0,
    Some = 1
}
export interface CGRIDMap__Proto015PtLimaPtFitnessLockedRound {
    None: CGRIDClass__Proto015PtLimaPtFitnessLockedRound__None,
    Some: CGRIDClass__Proto015PtLimaPtFitnessLockedRound__Some
}
export type Proto015PtLimaPtFitnessLockedRound = { kind: CGRIDTag__Proto015PtLimaPtFitnessLockedRound.None, value: CGRIDMap__Proto015PtLimaPtFitnessLockedRound['None'] } | { kind: CGRIDTag__Proto015PtLimaPtFitnessLockedRound.Some, value: CGRIDMap__Proto015PtLimaPtFitnessLockedRound['Some'] };
export type Proto015PtLimaPtFitness = { level: Int32, locked_round: CGRIDClass__Proto015_PtLimaPtFitness_locked_round, predecessor_round: Int32, round: Int32 };
export class CGRIDClass__Proto015PtLimaPtFitness extends Box<Proto015PtLimaPtFitness> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtFitness {
        return new this(record_decoder<Proto015PtLimaPtFitness>({level: Int32.decode, locked_round: CGRIDClass__Proto015_PtLimaPtFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.locked_round.encodeLength +  this.value.predecessor_round.encodeLength +  this.value.round.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.locked_round.writeTarget(tgt) +  this.value.predecessor_round.writeTarget(tgt) +  this.value.round.writeTarget(tgt));
    }
}
export const proto015_ptlimapt_fitness_encoder = (value: Proto015PtLimaPtFitness): OutputBytes => {
    return record_encoder({order: ['level', 'locked_round', 'predecessor_round', 'round']})(value);
}
export const proto015_ptlimapt_fitness_decoder = (p: Parser): Proto015PtLimaPtFitness => {
    return record_decoder<Proto015PtLimaPtFitness>({level: Int32.decode, locked_round: CGRIDClass__Proto015_PtLimaPtFitness_locked_round.decode, predecessor_round: Int32.decode, round: Int32.decode}, {order: ['level', 'locked_round', 'predecessor_round', 'round']})(p);
}
