import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { Target } from '../../ts_runtime/target';
export type Proto015PtLimaPtScript = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export class CGRIDClass__Proto015PtLimaPtScript extends Box<Proto015PtLimaPtScript> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtScript {
        return new this(record_decoder<Proto015PtLimaPtScript>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
export const proto015_ptlimapt_script_encoder = (value: Proto015PtLimaPtScript): OutputBytes => {
    return record_encoder({order: ['code', 'storage']})(value);
}
export const proto015_ptlimapt_script_decoder = (p: Parser): Proto015PtLimaPtScript => {
    return record_decoder<Proto015PtLimaPtScript>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p);
}
