import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto015PtLimaPtGas__Unaccounted generated for Proto015PtLimaPtGas__Unaccounted
export class CGRIDClass__Proto015PtLimaPtGas__Unaccounted extends Box<Proto015PtLimaPtGas__Unaccounted> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtGas__Unaccounted {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto015PtLimaPtGas__Limited generated for Proto015PtLimaPtGas__Limited
export class CGRIDClass__Proto015PtLimaPtGas__Limited extends Box<Proto015PtLimaPtGas__Limited> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtGas__Limited {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto015PtLimaPtGas__Unaccounted = Unit;
export type Proto015PtLimaPtGas__Limited = Z;
export enum CGRIDTag__Proto015PtLimaPtGas{
    Limited = 0,
    Unaccounted = 1
}
export interface CGRIDMap__Proto015PtLimaPtGas {
    Limited: CGRIDClass__Proto015PtLimaPtGas__Limited,
    Unaccounted: CGRIDClass__Proto015PtLimaPtGas__Unaccounted
}
export type Proto015PtLimaPtGas = { kind: CGRIDTag__Proto015PtLimaPtGas.Limited, value: CGRIDMap__Proto015PtLimaPtGas['Limited'] } | { kind: CGRIDTag__Proto015PtLimaPtGas.Unaccounted, value: CGRIDMap__Proto015PtLimaPtGas['Unaccounted'] };
export function proto015ptlimaptgas_mkDecoder(): VariantDecoder<CGRIDTag__Proto015PtLimaPtGas,Proto015PtLimaPtGas> {
    function f(disc: CGRIDTag__Proto015PtLimaPtGas) {
        switch (disc) {
            case CGRIDTag__Proto015PtLimaPtGas.Limited: return CGRIDClass__Proto015PtLimaPtGas__Limited.decode;
            case CGRIDTag__Proto015PtLimaPtGas.Unaccounted: return CGRIDClass__Proto015PtLimaPtGas__Unaccounted.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto015PtLimaPtGas => Object.values(CGRIDTag__Proto015PtLimaPtGas).includes(tagval);
    return f;
}
export class CGRIDClass__Proto015PtLimaPtGas extends Box<Proto015PtLimaPtGas> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto015PtLimaPtGas>, Proto015PtLimaPtGas>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto015PtLimaPtGas {
        return new this(variant_decoder(width.Uint8)(proto015ptlimaptgas_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto015_ptlimapt_gas_encoder = (value: Proto015PtLimaPtGas): OutputBytes => {
    return variant_encoder<KindOf<Proto015PtLimaPtGas>, Proto015PtLimaPtGas>(width.Uint8)(value);
}
export const proto015_ptlimapt_gas_decoder = (p: Parser): Proto015PtLimaPtGas => {
    return variant_decoder(width.Uint8)(proto015ptlimaptgas_mkDecoder())(p);
}
