import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
export type Proto008PtEdo2ZkGasCost = Z;
export class CGRIDClass__Proto008PtEdo2ZkGasCost extends Box<Proto008PtEdo2ZkGasCost> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkGasCost {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto008_ptedo2zk_gas_cost_encoder = (value: Proto008PtEdo2ZkGasCost): OutputBytes => {
    return value.encode();
}
export const proto008_ptedo2zk_gas_cost_decoder = (p: Parser): Proto008PtEdo2ZkGasCost => {
    return Z.decode(p);
}
