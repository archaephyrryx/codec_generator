import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto008PtEdo2ZkDelegateFrozenBalance = { deposit: N, fees: N, rewards: N };
export class CGRIDClass__Proto008PtEdo2ZkDelegateFrozenBalance extends Box<Proto008PtEdo2ZkDelegateFrozenBalance> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['deposit', 'fees', 'rewards']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkDelegateFrozenBalance {
        return new this(record_decoder<Proto008PtEdo2ZkDelegateFrozenBalance>({deposit: N.decode, fees: N.decode, rewards: N.decode}, {order: ['deposit', 'fees', 'rewards']})(p));
    };
    get encodeLength(): number {
        return (this.value.deposit.encodeLength +  this.value.fees.encodeLength +  this.value.rewards.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.deposit.writeTarget(tgt) +  this.value.fees.writeTarget(tgt) +  this.value.rewards.writeTarget(tgt));
    }
}
export const proto008_ptedo2zk_delegate_frozen_balance_encoder = (value: Proto008PtEdo2ZkDelegateFrozenBalance): OutputBytes => {
    return record_encoder({order: ['deposit', 'fees', 'rewards']})(value);
}
export const proto008_ptedo2zk_delegate_frozen_balance_decoder = (p: Parser): Proto008PtEdo2ZkDelegateFrozenBalance => {
    return record_decoder<Proto008PtEdo2ZkDelegateFrozenBalance>({deposit: N.decode, fees: N.decode, rewards: N.decode}, {order: ['deposit', 'fees', 'rewards']})(p);
}
