import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Rewards generated for Proto008PtEdo2ZkOperationMetadataAlphaBalance__Rewards
export class CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Rewards extends Box<Proto008PtEdo2ZkOperationMetadataAlphaBalance__Rewards> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Rewards {
        return new this(record_decoder<Proto008PtEdo2ZkOperationMetadataAlphaBalance__Rewards>({category: Unit.decode, delegate: CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Rewards_delegate.decode, cycle: Int32.decode, change: Int64.decode}, {order: ['category', 'delegate', 'cycle', 'change']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Fees generated for Proto008PtEdo2ZkOperationMetadataAlphaBalance__Fees
export class CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Fees extends Box<Proto008PtEdo2ZkOperationMetadataAlphaBalance__Fees> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Fees {
        return new this(record_decoder<Proto008PtEdo2ZkOperationMetadataAlphaBalance__Fees>({category: Unit.decode, delegate: CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Fees_delegate.decode, cycle: Int32.decode, change: Int64.decode}, {order: ['category', 'delegate', 'cycle', 'change']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Deposits generated for Proto008PtEdo2ZkOperationMetadataAlphaBalance__Deposits
export class CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Deposits extends Box<Proto008PtEdo2ZkOperationMetadataAlphaBalance__Deposits> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['category', 'delegate', 'cycle', 'change']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Deposits {
        return new this(record_decoder<Proto008PtEdo2ZkOperationMetadataAlphaBalance__Deposits>({category: Unit.decode, delegate: CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Deposits_delegate.decode, cycle: Int32.decode, change: Int64.decode}, {order: ['category', 'delegate', 'cycle', 'change']})(p));
    };
    get encodeLength(): number {
        return (this.value.category.encodeLength +  this.value.delegate.encodeLength +  this.value.cycle.encodeLength +  this.value.change.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.category.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.cycle.writeTarget(tgt) +  this.value.change.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Contract generated for Proto008PtEdo2ZkOperationMetadataAlphaBalance__Contract
export class CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Contract extends Box<Proto008PtEdo2ZkOperationMetadataAlphaBalance__Contract> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract', 'change']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Contract {
        return new this(record_decoder<Proto008PtEdo2ZkOperationMetadataAlphaBalance__Contract>({contract: CGRIDClass__Proto008_PtEdo2ZkContract_id.decode, change: Int64.decode}, {order: ['contract', 'change']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract.encodeLength +  this.value.change.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract.writeTarget(tgt) +  this.value.change.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkContractId__Originated generated for Proto008PtEdo2ZkContractId__Originated
export class CGRIDClass__Proto008PtEdo2ZkContractId__Originated extends Box<Proto008PtEdo2ZkContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkContractId__Implicit generated for Proto008PtEdo2ZkContractId__Implicit
export class CGRIDClass__Proto008PtEdo2ZkContractId__Implicit extends Box<Proto008PtEdo2ZkContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkContractId__Implicit {
        return new this(record_decoder<Proto008PtEdo2ZkContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto008PtEdo2ZkOperationMetadataAlphaBalance__Rewards = { category: Unit, delegate: CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Rewards_delegate, cycle: Int32, change: Int64 };
export type Proto008PtEdo2ZkOperationMetadataAlphaBalance__Fees = { category: Unit, delegate: CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Fees_delegate, cycle: Int32, change: Int64 };
export type Proto008PtEdo2ZkOperationMetadataAlphaBalance__Deposits = { category: Unit, delegate: CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Deposits_delegate, cycle: Int32, change: Int64 };
export type Proto008PtEdo2ZkOperationMetadataAlphaBalance__Contract = { contract: CGRIDClass__Proto008_PtEdo2ZkContract_id, change: Int64 };
export type Proto008PtEdo2ZkContractId__Originated = Padded<CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad,1>;
export type Proto008PtEdo2ZkContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Rewards_delegate generated for Proto008PtEdo2ZkOperationMetadataAlphaBalanceRewardsDelegate
export class CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Rewards_delegate extends Box<Proto008PtEdo2ZkOperationMetadataAlphaBalanceRewardsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Rewards_delegate {
        return new this(record_decoder<Proto008PtEdo2ZkOperationMetadataAlphaBalanceRewardsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Fees_delegate generated for Proto008PtEdo2ZkOperationMetadataAlphaBalanceFeesDelegate
export class CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Fees_delegate extends Box<Proto008PtEdo2ZkOperationMetadataAlphaBalanceFeesDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Fees_delegate {
        return new this(record_decoder<Proto008PtEdo2ZkOperationMetadataAlphaBalanceFeesDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Deposits_delegate generated for Proto008PtEdo2ZkOperationMetadataAlphaBalanceDepositsDelegate
export class CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Deposits_delegate extends Box<Proto008PtEdo2ZkOperationMetadataAlphaBalanceDepositsDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance_Deposits_delegate {
        return new this(record_decoder<Proto008PtEdo2ZkOperationMetadataAlphaBalanceDepositsDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance generated for Proto008PtEdo2ZkOperationMetadataAlphaBalance
export function proto008ptedo2zkoperationmetadataalphabalance_mkDecoder(): VariantDecoder<CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance,Proto008PtEdo2ZkOperationMetadataAlphaBalance> {
    function f(disc: CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance) {
        switch (disc) {
            case CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance.Contract: return CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Contract.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance.Rewards: return CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Rewards.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance.Fees: return CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Fees.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance.Deposits: return CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Deposits.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance => Object.values(CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance).includes(tagval);
    return f;
}
export class CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance extends Box<Proto008PtEdo2ZkOperationMetadataAlphaBalance> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto008PtEdo2ZkOperationMetadataAlphaBalance>, Proto008PtEdo2ZkOperationMetadataAlphaBalance>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance {
        return new this(variant_decoder(width.Uint8)(proto008ptedo2zkoperationmetadataalphabalance_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad generated for Proto008PtEdo2ZkContractIdOriginatedDenestPad
export class CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad extends Box<Proto008PtEdo2ZkContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto008PtEdo2ZkContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkContract_id generated for Proto008PtEdo2ZkContractId
export function proto008ptedo2zkcontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto008PtEdo2ZkContractId,Proto008PtEdo2ZkContractId> {
    function f(disc: CGRIDTag__Proto008PtEdo2ZkContractId) {
        switch (disc) {
            case CGRIDTag__Proto008PtEdo2ZkContractId.Implicit: return CGRIDClass__Proto008PtEdo2ZkContractId__Implicit.decode;
            case CGRIDTag__Proto008PtEdo2ZkContractId.Originated: return CGRIDClass__Proto008PtEdo2ZkContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto008PtEdo2ZkContractId => Object.values(CGRIDTag__Proto008PtEdo2ZkContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto008_PtEdo2ZkContract_id extends Box<Proto008PtEdo2ZkContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto008PtEdo2ZkContractId>, Proto008PtEdo2ZkContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkContract_id {
        return new this(variant_decoder(width.Uint8)(proto008ptedo2zkcontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export type Proto008PtEdo2ZkOperationMetadataAlphaBalanceRewardsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto008PtEdo2ZkOperationMetadataAlphaBalanceFeesDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto008PtEdo2ZkOperationMetadataAlphaBalanceDepositsDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance{
    Contract = 0,
    Rewards = 1,
    Fees = 2,
    Deposits = 3
}
export interface CGRIDMap__Proto008PtEdo2ZkOperationMetadataAlphaBalance {
    Contract: CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Contract,
    Rewards: CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Rewards,
    Fees: CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Fees,
    Deposits: CGRIDClass__Proto008PtEdo2ZkOperationMetadataAlphaBalance__Deposits
}
export type Proto008PtEdo2ZkOperationMetadataAlphaBalance = { kind: CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance.Contract, value: CGRIDMap__Proto008PtEdo2ZkOperationMetadataAlphaBalance['Contract'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance.Rewards, value: CGRIDMap__Proto008PtEdo2ZkOperationMetadataAlphaBalance['Rewards'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance.Fees, value: CGRIDMap__Proto008PtEdo2ZkOperationMetadataAlphaBalance['Fees'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationMetadataAlphaBalance.Deposits, value: CGRIDMap__Proto008PtEdo2ZkOperationMetadataAlphaBalance['Deposits'] };
export type Proto008PtEdo2ZkContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto008PtEdo2ZkContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto008PtEdo2ZkContractId {
    Implicit: CGRIDClass__Proto008PtEdo2ZkContractId__Implicit,
    Originated: CGRIDClass__Proto008PtEdo2ZkContractId__Originated
}
export type Proto008PtEdo2ZkContractId = { kind: CGRIDTag__Proto008PtEdo2ZkContractId.Implicit, value: CGRIDMap__Proto008PtEdo2ZkContractId['Implicit'] } | { kind: CGRIDTag__Proto008PtEdo2ZkContractId.Originated, value: CGRIDMap__Proto008PtEdo2ZkContractId['Originated'] };
export type Proto008PtEdo2ZkDelegateBalanceUpdates = Dynamic<Sequence<CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance>,width.Uint30>;
export class CGRIDClass__Proto008PtEdo2ZkDelegateBalanceUpdates extends Box<Proto008PtEdo2ZkDelegateBalanceUpdates> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkDelegateBalanceUpdates {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto008_ptedo2zk_delegate_balance_updates_encoder = (value: Proto008PtEdo2ZkDelegateBalanceUpdates): OutputBytes => {
    return value.encode();
}
export const proto008_ptedo2zk_delegate_balance_updates_decoder = (p: Parser): Proto008PtEdo2ZkDelegateBalanceUpdates => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__Proto008_PtEdo2ZkOperation_metadataAlphaBalance.decode), width.Uint30)(p);
}
