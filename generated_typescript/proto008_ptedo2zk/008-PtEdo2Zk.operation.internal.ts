import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Transaction generated for Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Transaction
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Transaction extends Box<Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Transaction {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Transaction>({amount: N.decode, destination: CGRIDClass__Proto008_PtEdo2ZkContract_id.decode, parameters: Option.decode(CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Transaction_parameters.decode)}, {order: ['amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Reveal generated for Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Reveal
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Reveal extends Box<Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Reveal {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Reveal>({public_key: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Reveal_public_key.decode}, {order: ['public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Origination generated for Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Origination
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Origination extends Box<Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Origination {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Origination>({balance: N.decode, delegate: Option.decode(CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Origination_delegate.decode), script: CGRIDClass__Proto008_PtEdo2ZkScriptedContracts.decode}, {order: ['balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Delegation generated for Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Delegation
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Delegation extends Box<Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Delegation {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Delegation>({delegate: Option.decode(CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Delegation_delegate.decode)}, {order: ['delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint__set_delegate generated for Proto008PtEdo2ZkEntrypoint__set_delegate
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint__set_delegate extends Box<Proto008PtEdo2ZkEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint__root generated for Proto008PtEdo2ZkEntrypoint__root
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint__root extends Box<Proto008PtEdo2ZkEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint__remove_delegate generated for Proto008PtEdo2ZkEntrypoint__remove_delegate
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint__remove_delegate extends Box<Proto008PtEdo2ZkEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint__named generated for Proto008PtEdo2ZkEntrypoint__named
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint__named extends Box<Proto008PtEdo2ZkEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint___do generated for Proto008PtEdo2ZkEntrypoint___do
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint___do extends Box<Proto008PtEdo2ZkEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint___default generated for Proto008PtEdo2ZkEntrypoint___default
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint___default extends Box<Proto008PtEdo2ZkEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkContractId__Originated generated for Proto008PtEdo2ZkContractId__Originated
export class CGRIDClass__Proto008PtEdo2ZkContractId__Originated extends Box<Proto008PtEdo2ZkContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkContractId__Implicit generated for Proto008PtEdo2ZkContractId__Implicit
export class CGRIDClass__Proto008PtEdo2ZkContractId__Implicit extends Box<Proto008PtEdo2ZkContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkContractId__Implicit {
        return new this(record_decoder<Proto008PtEdo2ZkContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Transaction = { amount: N, destination: CGRIDClass__Proto008_PtEdo2ZkContract_id, parameters: Option<CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Transaction_parameters> };
export type Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Reveal = { public_key: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Reveal_public_key };
export type Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Origination = { balance: N, delegate: Option<CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Origination_delegate>, script: CGRIDClass__Proto008_PtEdo2ZkScriptedContracts };
export type Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Delegation = { delegate: Option<CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Delegation_delegate> };
export type Proto008PtEdo2ZkEntrypoint__set_delegate = Unit;
export type Proto008PtEdo2ZkEntrypoint__root = Unit;
export type Proto008PtEdo2ZkEntrypoint__remove_delegate = Unit;
export type Proto008PtEdo2ZkEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto008PtEdo2ZkEntrypoint___do = Unit;
export type Proto008PtEdo2ZkEntrypoint___default = Unit;
export type Proto008PtEdo2ZkContractId__Originated = Padded<CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad,1>;
export type Proto008PtEdo2ZkContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkScriptedContracts generated for Proto008PtEdo2ZkScriptedContracts
export class CGRIDClass__Proto008_PtEdo2ZkScriptedContracts extends Box<Proto008PtEdo2ZkScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkScriptedContracts {
        return new this(record_decoder<Proto008PtEdo2ZkScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_rhs generated for Proto008PtEdo2ZkOperationAlphaInternalOperationRhs
export function proto008ptedo2zkoperationalphainternaloperationrhs_mkDecoder(): VariantDecoder<CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs,Proto008PtEdo2ZkOperationAlphaInternalOperationRhs> {
    function f(disc: CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs) {
        switch (disc) {
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs.Reveal: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Reveal.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs.Transaction: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Transaction.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs.Origination: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Origination.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs.Delegation: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Delegation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs => Object.values(CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs).includes(tagval);
    return f;
}
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_rhs extends Box<Proto008PtEdo2ZkOperationAlphaInternalOperationRhs> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto008PtEdo2ZkOperationAlphaInternalOperationRhs>, Proto008PtEdo2ZkOperationAlphaInternalOperationRhs>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_rhs {
        return new this(variant_decoder(width.Uint8)(proto008ptedo2zkoperationalphainternaloperationrhs_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Transaction_parameters generated for Proto008PtEdo2ZkOperationAlphaInternalOperationTransactionParameters
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Transaction_parameters extends Box<Proto008PtEdo2ZkOperationAlphaInternalOperationTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Transaction_parameters {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaInternalOperationTransactionParameters>({entrypoint: CGRIDClass__Proto008_PtEdo2ZkEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Reveal_public_key generated for Proto008PtEdo2ZkOperationAlphaInternalOperationRevealPublicKey
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Reveal_public_key extends Box<Proto008PtEdo2ZkOperationAlphaInternalOperationRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Reveal_public_key {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaInternalOperationRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Origination_delegate generated for Proto008PtEdo2ZkOperationAlphaInternalOperationOriginationDelegate
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Origination_delegate extends Box<Proto008PtEdo2ZkOperationAlphaInternalOperationOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Origination_delegate {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaInternalOperationOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Delegation_delegate generated for Proto008PtEdo2ZkOperationAlphaInternalOperationDelegationDelegate
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Delegation_delegate extends Box<Proto008PtEdo2ZkOperationAlphaInternalOperationDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_Delegation_delegate {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaInternalOperationDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkEntrypoint generated for Proto008PtEdo2ZkEntrypoint
export function proto008ptedo2zkentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto008PtEdo2ZkEntrypoint,Proto008PtEdo2ZkEntrypoint> {
    function f(disc: CGRIDTag__Proto008PtEdo2ZkEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint._default: return CGRIDClass__Proto008PtEdo2ZkEntrypoint___default.decode;
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint.root: return CGRIDClass__Proto008PtEdo2ZkEntrypoint__root.decode;
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint._do: return CGRIDClass__Proto008PtEdo2ZkEntrypoint___do.decode;
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint.set_delegate: return CGRIDClass__Proto008PtEdo2ZkEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint.remove_delegate: return CGRIDClass__Proto008PtEdo2ZkEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint.named: return CGRIDClass__Proto008PtEdo2ZkEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto008PtEdo2ZkEntrypoint => Object.values(CGRIDTag__Proto008PtEdo2ZkEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto008_PtEdo2ZkEntrypoint extends Box<Proto008PtEdo2ZkEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto008PtEdo2ZkEntrypoint>, Proto008PtEdo2ZkEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto008ptedo2zkentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad generated for Proto008PtEdo2ZkContractIdOriginatedDenestPad
export class CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad extends Box<Proto008PtEdo2ZkContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto008PtEdo2ZkContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkContract_id generated for Proto008PtEdo2ZkContractId
export function proto008ptedo2zkcontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto008PtEdo2ZkContractId,Proto008PtEdo2ZkContractId> {
    function f(disc: CGRIDTag__Proto008PtEdo2ZkContractId) {
        switch (disc) {
            case CGRIDTag__Proto008PtEdo2ZkContractId.Implicit: return CGRIDClass__Proto008PtEdo2ZkContractId__Implicit.decode;
            case CGRIDTag__Proto008PtEdo2ZkContractId.Originated: return CGRIDClass__Proto008PtEdo2ZkContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto008PtEdo2ZkContractId => Object.values(CGRIDTag__Proto008PtEdo2ZkContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto008_PtEdo2ZkContract_id extends Box<Proto008PtEdo2ZkContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto008PtEdo2ZkContractId>, Proto008PtEdo2ZkContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkContract_id {
        return new this(variant_decoder(width.Uint8)(proto008ptedo2zkcontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type Proto008PtEdo2ZkScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export enum CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs{
    Reveal = 0,
    Transaction = 1,
    Origination = 2,
    Delegation = 3
}
export interface CGRIDMap__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs {
    Reveal: CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Reveal,
    Transaction: CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Transaction,
    Origination: CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Origination,
    Delegation: CGRIDClass__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs__Delegation
}
export type Proto008PtEdo2ZkOperationAlphaInternalOperationRhs = { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs.Reveal, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs['Reveal'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs.Transaction, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs['Transaction'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs.Origination, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs['Origination'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs.Delegation, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaInternalOperationRhs['Delegation'] };
export type Proto008PtEdo2ZkOperationAlphaInternalOperationTransactionParameters = { entrypoint: CGRIDClass__Proto008_PtEdo2ZkEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto008PtEdo2ZkOperationAlphaInternalOperationRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto008PtEdo2ZkOperationAlphaInternalOperationOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto008PtEdo2ZkOperationAlphaInternalOperationDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__Proto008PtEdo2ZkEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto008PtEdo2ZkEntrypoint {
    _default: CGRIDClass__Proto008PtEdo2ZkEntrypoint___default,
    root: CGRIDClass__Proto008PtEdo2ZkEntrypoint__root,
    _do: CGRIDClass__Proto008PtEdo2ZkEntrypoint___do,
    set_delegate: CGRIDClass__Proto008PtEdo2ZkEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto008PtEdo2ZkEntrypoint__remove_delegate,
    named: CGRIDClass__Proto008PtEdo2ZkEntrypoint__named
}
export type Proto008PtEdo2ZkEntrypoint = { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint._default, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['_default'] } | { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint.root, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['root'] } | { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint._do, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['_do'] } | { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint.set_delegate, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint.remove_delegate, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint.named, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['named'] };
export type Proto008PtEdo2ZkContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto008PtEdo2ZkContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto008PtEdo2ZkContractId {
    Implicit: CGRIDClass__Proto008PtEdo2ZkContractId__Implicit,
    Originated: CGRIDClass__Proto008PtEdo2ZkContractId__Originated
}
export type Proto008PtEdo2ZkContractId = { kind: CGRIDTag__Proto008PtEdo2ZkContractId.Implicit, value: CGRIDMap__Proto008PtEdo2ZkContractId['Implicit'] } | { kind: CGRIDTag__Proto008PtEdo2ZkContractId.Originated, value: CGRIDMap__Proto008PtEdo2ZkContractId['Originated'] };
export type Proto008PtEdo2ZkOperationInternal = { source: CGRIDClass__Proto008_PtEdo2ZkContract_id, nonce: Uint16, proto008_ptedo2zk_operation_alpha_internal_operation_rhs: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_rhs };
export class CGRIDClass__Proto008PtEdo2ZkOperationInternal extends Box<Proto008PtEdo2ZkOperationInternal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'nonce', 'proto008_ptedo2zk_operation_alpha_internal_operation_rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationInternal {
        return new this(record_decoder<Proto008PtEdo2ZkOperationInternal>({source: CGRIDClass__Proto008_PtEdo2ZkContract_id.decode, nonce: Uint16.decode, proto008_ptedo2zk_operation_alpha_internal_operation_rhs: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto008_ptedo2zk_operation_alpha_internal_operation_rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.nonce.encodeLength +  this.value.proto008_ptedo2zk_operation_alpha_internal_operation_rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt) +  this.value.proto008_ptedo2zk_operation_alpha_internal_operation_rhs.writeTarget(tgt));
    }
}
export const proto008_ptedo2zk_operation_internal_encoder = (value: Proto008PtEdo2ZkOperationInternal): OutputBytes => {
    return record_encoder({order: ['source', 'nonce', 'proto008_ptedo2zk_operation_alpha_internal_operation_rhs']})(value);
}
export const proto008_ptedo2zk_operation_internal_decoder = (p: Parser): Proto008PtEdo2ZkOperationInternal => {
    return record_decoder<Proto008PtEdo2ZkOperationInternal>({source: CGRIDClass__Proto008_PtEdo2ZkContract_id.decode, nonce: Uint16.decode, proto008_ptedo2zk_operation_alpha_internal_operation_rhs: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaInternal_operation_rhs.decode}, {order: ['source', 'nonce', 'proto008_ptedo2zk_operation_alpha_internal_operation_rhs']})(p);
}
