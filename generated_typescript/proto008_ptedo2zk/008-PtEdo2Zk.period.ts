import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto008PtEdo2ZkPeriod = Int64;
export class CGRIDClass__Proto008PtEdo2ZkPeriod extends Box<Proto008PtEdo2ZkPeriod> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkPeriod {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto008_ptedo2zk_period_encoder = (value: Proto008PtEdo2ZkPeriod): OutputBytes => {
    return value.encode();
}
export const proto008_ptedo2zk_period_decoder = (p: Parser): Proto008PtEdo2ZkPeriod => {
    return Int64.decode(p);
}
