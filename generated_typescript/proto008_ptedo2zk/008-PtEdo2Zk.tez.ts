import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
export type Proto008PtEdo2ZkTez = N;
export class CGRIDClass__Proto008PtEdo2ZkTez extends Box<Proto008PtEdo2ZkTez> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkTez {
        return new this(N.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto008_ptedo2zk_tez_encoder = (value: Proto008PtEdo2ZkTez): OutputBytes => {
    return value.encode();
}
export const proto008_ptedo2zk_tez_decoder = (p: Parser): Proto008PtEdo2ZkTez => {
    return N.decode(p);
}
