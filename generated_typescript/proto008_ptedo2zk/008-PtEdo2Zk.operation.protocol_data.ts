import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Nullable } from '../../ts_runtime/composite/opt/nullable';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Padded } from '../../ts_runtime/composite/padded';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { VPadded } from '../../ts_runtime/composite/vpadded';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32, Int64, Int8, Uint16, Uint8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { N } from '../../ts_runtime/zarith/natural';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Transaction generated for Proto008PtEdo2ZkOperationAlphaContents__Transaction
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Transaction extends Box<Proto008PtEdo2ZkOperationAlphaContents__Transaction> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Transaction {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContents__Transaction>({source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Transaction_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, amount: N.decode, destination: CGRIDClass__Proto008_PtEdo2ZkContract_id.decode, parameters: Option.decode(CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Transaction_parameters.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'amount', 'destination', 'parameters']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.amount.encodeLength +  this.value.destination.encodeLength +  this.value.parameters.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.amount.writeTarget(tgt) +  this.value.destination.writeTarget(tgt) +  this.value.parameters.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Seed_nonce_revelation generated for Proto008PtEdo2ZkOperationAlphaContents__Seed_nonce_revelation
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Seed_nonce_revelation extends Box<Proto008PtEdo2ZkOperationAlphaContents__Seed_nonce_revelation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Seed_nonce_revelation {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContents__Seed_nonce_revelation>({level: Int32.decode, nonce: FixedBytes.decode<32>({len: 32})}, {order: ['level', 'nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Reveal generated for Proto008PtEdo2ZkOperationAlphaContents__Reveal
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Reveal extends Box<Proto008PtEdo2ZkOperationAlphaContents__Reveal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Reveal {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContents__Reveal>({source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Reveal_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, public_key: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Reveal_public_key.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Proposals generated for Proto008PtEdo2ZkOperationAlphaContents__Proposals
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Proposals extends Box<Proto008PtEdo2ZkOperationAlphaContents__Proposals> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposals']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Proposals {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContents__Proposals>({source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Proposals_source.decode, period: Int32.decode, proposals: Dynamic.decode(Sequence.decode(CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['source', 'period', 'proposals']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposals.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposals.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Origination generated for Proto008PtEdo2ZkOperationAlphaContents__Origination
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Origination extends Box<Proto008PtEdo2ZkOperationAlphaContents__Origination> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Origination {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContents__Origination>({source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Origination_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, balance: N.decode, delegate: Option.decode(CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Origination_delegate.decode), script: CGRIDClass__Proto008_PtEdo2ZkScriptedContracts.decode}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'balance', 'delegate', 'script']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.balance.encodeLength +  this.value.delegate.encodeLength +  this.value.script.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.balance.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt) +  this.value.script.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Endorsement generated for Proto008PtEdo2ZkOperationAlphaContents__Endorsement
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Endorsement extends Box<Proto008PtEdo2ZkOperationAlphaContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Endorsement {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContents__Endorsement>({level: Int32.decode}, {order: ['level']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Double_endorsement_evidence generated for Proto008PtEdo2ZkOperationAlphaContents__Double_endorsement_evidence
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Double_endorsement_evidence extends Box<Proto008PtEdo2ZkOperationAlphaContents__Double_endorsement_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['op1', 'op2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Double_endorsement_evidence {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContents__Double_endorsement_evidence>({op1: Dynamic.decode(CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement.decode, width.Uint30), op2: Dynamic.decode(CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement.decode, width.Uint30)}, {order: ['op1', 'op2']})(p));
    };
    get encodeLength(): number {
        return (this.value.op1.encodeLength +  this.value.op2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.op1.writeTarget(tgt) +  this.value.op2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Double_baking_evidence generated for Proto008PtEdo2ZkOperationAlphaContents__Double_baking_evidence
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Double_baking_evidence extends Box<Proto008PtEdo2ZkOperationAlphaContents__Double_baking_evidence> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bh1', 'bh2']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Double_baking_evidence {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContents__Double_baking_evidence>({bh1: Dynamic.decode(CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaFull_header.decode, width.Uint30), bh2: Dynamic.decode(CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaFull_header.decode, width.Uint30)}, {order: ['bh1', 'bh2']})(p));
    };
    get encodeLength(): number {
        return (this.value.bh1.encodeLength +  this.value.bh2.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bh1.writeTarget(tgt) +  this.value.bh2.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Delegation generated for Proto008PtEdo2ZkOperationAlphaContents__Delegation
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Delegation extends Box<Proto008PtEdo2ZkOperationAlphaContents__Delegation> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Delegation {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContents__Delegation>({source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Delegation_source.decode, fee: N.decode, counter: N.decode, gas_limit: N.decode, storage_limit: N.decode, delegate: Option.decode(CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Delegation_delegate.decode)}, {order: ['source', 'fee', 'counter', 'gas_limit', 'storage_limit', 'delegate']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.fee.encodeLength +  this.value.counter.encodeLength +  this.value.gas_limit.encodeLength +  this.value.storage_limit.encodeLength +  this.value.delegate.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.fee.writeTarget(tgt) +  this.value.counter.writeTarget(tgt) +  this.value.gas_limit.writeTarget(tgt) +  this.value.storage_limit.writeTarget(tgt) +  this.value.delegate.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Ballot generated for Proto008PtEdo2ZkOperationAlphaContents__Ballot
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Ballot extends Box<Proto008PtEdo2ZkOperationAlphaContents__Ballot> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['source', 'period', 'proposal', 'ballot']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Ballot {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContents__Ballot>({source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Ballot_source.decode, period: Int32.decode, proposal: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Ballot_proposal.decode, ballot: Int8.decode}, {order: ['source', 'period', 'proposal', 'ballot']})(p));
    };
    get encodeLength(): number {
        return (this.value.source.encodeLength +  this.value.period.encodeLength +  this.value.proposal.encodeLength +  this.value.ballot.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.source.writeTarget(tgt) +  this.value.period.writeTarget(tgt) +  this.value.proposal.writeTarget(tgt) +  this.value.ballot.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Activate_account generated for Proto008PtEdo2ZkOperationAlphaContents__Activate_account
export class CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Activate_account extends Box<Proto008PtEdo2ZkOperationAlphaContents__Activate_account> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'secret']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Activate_account {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContents__Activate_account>({pkh: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Activate_account_pkh.decode, secret: FixedBytes.decode<20>({len: 20})}, {order: ['pkh', 'secret']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.secret.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.secret.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkInlinedEndorsementContents__Endorsement generated for Proto008PtEdo2ZkInlinedEndorsementContents__Endorsement
export class CGRIDClass__Proto008PtEdo2ZkInlinedEndorsementContents__Endorsement extends Box<Proto008PtEdo2ZkInlinedEndorsementContents__Endorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkInlinedEndorsementContents__Endorsement {
        return new this(record_decoder<Proto008PtEdo2ZkInlinedEndorsementContents__Endorsement>({level: Int32.decode}, {order: ['level']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint__set_delegate generated for Proto008PtEdo2ZkEntrypoint__set_delegate
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint__set_delegate extends Box<Proto008PtEdo2ZkEntrypoint__set_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint__set_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint__root generated for Proto008PtEdo2ZkEntrypoint__root
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint__root extends Box<Proto008PtEdo2ZkEntrypoint__root> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint__root {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint__remove_delegate generated for Proto008PtEdo2ZkEntrypoint__remove_delegate
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint__remove_delegate extends Box<Proto008PtEdo2ZkEntrypoint__remove_delegate> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint__remove_delegate {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint__named generated for Proto008PtEdo2ZkEntrypoint__named
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint__named extends Box<Proto008PtEdo2ZkEntrypoint__named> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint__named {
        return new this(Dynamic.decode(U8String.decode, width.Uint8)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint___do generated for Proto008PtEdo2ZkEntrypoint___do
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint___do extends Box<Proto008PtEdo2ZkEntrypoint___do> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint___do {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkEntrypoint___default generated for Proto008PtEdo2ZkEntrypoint___default
export class CGRIDClass__Proto008PtEdo2ZkEntrypoint___default extends Box<Proto008PtEdo2ZkEntrypoint___default> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkEntrypoint___default {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkContractId__Originated generated for Proto008PtEdo2ZkContractId__Originated
export class CGRIDClass__Proto008PtEdo2ZkContractId__Originated extends Box<Proto008PtEdo2ZkContractId__Originated> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkContractId__Originated {
        return new this(Padded.decode(CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad.decode, 1)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkContractId__Implicit generated for Proto008PtEdo2ZkContractId__Implicit
export class CGRIDClass__Proto008PtEdo2ZkContractId__Implicit extends Box<Proto008PtEdo2ZkContractId__Implicit> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkContractId__Implicit {
        return new this(record_decoder<Proto008PtEdo2ZkContractId__Implicit>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type Proto008PtEdo2ZkOperationAlphaContents__Transaction = { source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Transaction_source, fee: N, counter: N, gas_limit: N, storage_limit: N, amount: N, destination: CGRIDClass__Proto008_PtEdo2ZkContract_id, parameters: Option<CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Transaction_parameters> };
export type Proto008PtEdo2ZkOperationAlphaContents__Seed_nonce_revelation = { level: Int32, nonce: FixedBytes<32> };
export type Proto008PtEdo2ZkOperationAlphaContents__Reveal = { source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Reveal_source, fee: N, counter: N, gas_limit: N, storage_limit: N, public_key: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Reveal_public_key };
export type Proto008PtEdo2ZkOperationAlphaContents__Proposals = { source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Proposals_source, period: Int32, proposals: Dynamic<Sequence<CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq>,width.Uint30> };
export type Proto008PtEdo2ZkOperationAlphaContents__Origination = { source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Origination_source, fee: N, counter: N, gas_limit: N, storage_limit: N, balance: N, delegate: Option<CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Origination_delegate>, script: CGRIDClass__Proto008_PtEdo2ZkScriptedContracts };
export type Proto008PtEdo2ZkOperationAlphaContents__Endorsement = { level: Int32 };
export type Proto008PtEdo2ZkOperationAlphaContents__Double_endorsement_evidence = { op1: Dynamic<CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement,width.Uint30>, op2: Dynamic<CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement,width.Uint30> };
export type Proto008PtEdo2ZkOperationAlphaContents__Double_baking_evidence = { bh1: Dynamic<CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaFull_header,width.Uint30>, bh2: Dynamic<CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaFull_header,width.Uint30> };
export type Proto008PtEdo2ZkOperationAlphaContents__Delegation = { source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Delegation_source, fee: N, counter: N, gas_limit: N, storage_limit: N, delegate: Option<CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Delegation_delegate> };
export type Proto008PtEdo2ZkOperationAlphaContents__Ballot = { source: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Ballot_source, period: Int32, proposal: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Ballot_proposal, ballot: Int8 };
export type Proto008PtEdo2ZkOperationAlphaContents__Activate_account = { pkh: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Activate_account_pkh, secret: FixedBytes<20> };
export type Proto008PtEdo2ZkInlinedEndorsementContents__Endorsement = { level: Int32 };
export type Proto008PtEdo2ZkEntrypoint__set_delegate = Unit;
export type Proto008PtEdo2ZkEntrypoint__root = Unit;
export type Proto008PtEdo2ZkEntrypoint__remove_delegate = Unit;
export type Proto008PtEdo2ZkEntrypoint__named = Dynamic<U8String,width.Uint8>;
export type Proto008PtEdo2ZkEntrypoint___do = Unit;
export type Proto008PtEdo2ZkEntrypoint___default = Unit;
export type Proto008PtEdo2ZkContractId__Originated = Padded<CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad,1>;
export type Proto008PtEdo2ZkContractId__Implicit = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkScriptedContracts generated for Proto008PtEdo2ZkScriptedContracts
export class CGRIDClass__Proto008_PtEdo2ZkScriptedContracts extends Box<Proto008PtEdo2ZkScriptedContracts> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['code', 'storage']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkScriptedContracts {
        return new this(record_decoder<Proto008PtEdo2ZkScriptedContracts>({code: Dynamic.decode(Bytes.decode, width.Uint30), storage: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['code', 'storage']})(p));
    };
    get encodeLength(): number {
        return (this.value.code.encodeLength +  this.value.storage.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.code.writeTarget(tgt) +  this.value.storage.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_and_signature_signature generated for Proto008PtEdo2ZkOperationAlphaContentsAndSignatureSignature
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_and_signature_signature extends Box<Proto008PtEdo2ZkOperationAlphaContentsAndSignatureSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_and_signature_signature {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsAndSignatureSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Transaction_source generated for Proto008PtEdo2ZkOperationAlphaContentsTransactionSource
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Transaction_source extends Box<Proto008PtEdo2ZkOperationAlphaContentsTransactionSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Transaction_source {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsTransactionSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Transaction_parameters generated for Proto008PtEdo2ZkOperationAlphaContentsTransactionParameters
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Transaction_parameters extends Box<Proto008PtEdo2ZkOperationAlphaContentsTransactionParameters> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['entrypoint', 'value']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Transaction_parameters {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsTransactionParameters>({entrypoint: CGRIDClass__Proto008_PtEdo2ZkEntrypoint.decode, value: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['entrypoint', 'value']})(p));
    };
    get encodeLength(): number {
        return (this.value.entrypoint.encodeLength +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.entrypoint.writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Reveal_source generated for Proto008PtEdo2ZkOperationAlphaContentsRevealSource
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Reveal_source extends Box<Proto008PtEdo2ZkOperationAlphaContentsRevealSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Reveal_source {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsRevealSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Reveal_public_key generated for Proto008PtEdo2ZkOperationAlphaContentsRevealPublicKey
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Reveal_public_key extends Box<Proto008PtEdo2ZkOperationAlphaContentsRevealPublicKey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Reveal_public_key {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsRevealPublicKey>({signature_v0_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_v0_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Proposals_source generated for Proto008PtEdo2ZkOperationAlphaContentsProposalsSource
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Proposals_source extends Box<Proto008PtEdo2ZkOperationAlphaContentsProposalsSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Proposals_source {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsProposalsSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq generated for Proto008PtEdo2ZkOperationAlphaContentsProposalsProposalsDenestDynDenestSeq
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq extends Box<Proto008PtEdo2ZkOperationAlphaContentsProposalsProposalsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Proposals_proposals_denest_dyn_denest_seq {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsProposalsProposalsDenestDynDenestSeq>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Origination_source generated for Proto008PtEdo2ZkOperationAlphaContentsOriginationSource
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Origination_source extends Box<Proto008PtEdo2ZkOperationAlphaContentsOriginationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Origination_source {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsOriginationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Origination_delegate generated for Proto008PtEdo2ZkOperationAlphaContentsOriginationDelegate
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Origination_delegate extends Box<Proto008PtEdo2ZkOperationAlphaContentsOriginationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Origination_delegate {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsOriginationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Delegation_source generated for Proto008PtEdo2ZkOperationAlphaContentsDelegationSource
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Delegation_source extends Box<Proto008PtEdo2ZkOperationAlphaContentsDelegationSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Delegation_source {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsDelegationSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Delegation_delegate generated for Proto008PtEdo2ZkOperationAlphaContentsDelegationDelegate
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Delegation_delegate extends Box<Proto008PtEdo2ZkOperationAlphaContentsDelegationDelegate> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Delegation_delegate {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsDelegationDelegate>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Ballot_source generated for Proto008PtEdo2ZkOperationAlphaContentsBallotSource
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Ballot_source extends Box<Proto008PtEdo2ZkOperationAlphaContentsBallotSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Ballot_source {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsBallotSource>({signature_v0_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_v0_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Ballot_proposal generated for Proto008PtEdo2ZkOperationAlphaContentsBallotProposal
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Ballot_proposal extends Box<Proto008PtEdo2ZkOperationAlphaContentsBallotProposal> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Ballot_proposal {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsBallotProposal>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Activate_account_pkh generated for Proto008PtEdo2ZkOperationAlphaContentsActivateAccountPkh
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Activate_account_pkh extends Box<Proto008PtEdo2ZkOperationAlphaContentsActivateAccountPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_Activate_account_pkh {
        return new this(record_decoder<Proto008PtEdo2ZkOperationAlphaContentsActivateAccountPkh>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents generated for Proto008PtEdo2ZkOperationAlphaContents
export function proto008ptedo2zkoperationalphacontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents,Proto008PtEdo2ZkOperationAlphaContents> {
    function f(disc: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents) {
        switch (disc) {
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Endorsement: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Endorsement.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Seed_nonce_revelation: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Seed_nonce_revelation.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Double_endorsement_evidence: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Double_endorsement_evidence.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Double_baking_evidence: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Double_baking_evidence.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Activate_account: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Activate_account.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Proposals: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Proposals.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Ballot: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Ballot.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Reveal: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Reveal.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Transaction: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Transaction.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Origination: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Origination.decode;
            case CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Delegation: return CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Delegation.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents => Object.values(CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents extends Box<Proto008PtEdo2ZkOperationAlphaContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto008PtEdo2ZkOperationAlphaContents>, Proto008PtEdo2ZkOperationAlphaContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents {
        return new this(variant_decoder(width.Uint8)(proto008ptedo2zkoperationalphacontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement_signature generated for Proto008PtEdo2ZkInlinedEndorsementSignature
export class CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement_signature extends Box<Proto008PtEdo2ZkInlinedEndorsementSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement_signature {
        return new this(record_decoder<Proto008PtEdo2ZkInlinedEndorsementSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsementContents generated for Proto008PtEdo2ZkInlinedEndorsementContents
export function proto008ptedo2zkinlinedendorsementcontents_mkDecoder(): VariantDecoder<CGRIDTag__Proto008PtEdo2ZkInlinedEndorsementContents,Proto008PtEdo2ZkInlinedEndorsementContents> {
    function f(disc: CGRIDTag__Proto008PtEdo2ZkInlinedEndorsementContents) {
        switch (disc) {
            case CGRIDTag__Proto008PtEdo2ZkInlinedEndorsementContents.Endorsement: return CGRIDClass__Proto008PtEdo2ZkInlinedEndorsementContents__Endorsement.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto008PtEdo2ZkInlinedEndorsementContents => Object.values(CGRIDTag__Proto008PtEdo2ZkInlinedEndorsementContents).includes(tagval);
    return f;
}
export class CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsementContents extends Box<Proto008PtEdo2ZkInlinedEndorsementContents> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto008PtEdo2ZkInlinedEndorsementContents>, Proto008PtEdo2ZkInlinedEndorsementContents>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsementContents {
        return new this(variant_decoder(width.Uint8)(proto008ptedo2zkinlinedendorsementcontents_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement generated for Proto008PtEdo2ZkInlinedEndorsement
export class CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement extends Box<Proto008PtEdo2ZkInlinedEndorsement> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch', 'operations', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement {
        return new this(record_decoder<Proto008PtEdo2ZkInlinedEndorsement>({branch: CGRIDClass__OperationShell_header_branch.decode, operations: CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsementContents.decode, signature: Nullable.decode(CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement_signature.decode)}, {order: ['branch', 'operations', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength +  this.value.operations.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt) +  this.value.operations.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkEntrypoint generated for Proto008PtEdo2ZkEntrypoint
export function proto008ptedo2zkentrypoint_mkDecoder(): VariantDecoder<CGRIDTag__Proto008PtEdo2ZkEntrypoint,Proto008PtEdo2ZkEntrypoint> {
    function f(disc: CGRIDTag__Proto008PtEdo2ZkEntrypoint) {
        switch (disc) {
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint._default: return CGRIDClass__Proto008PtEdo2ZkEntrypoint___default.decode;
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint.root: return CGRIDClass__Proto008PtEdo2ZkEntrypoint__root.decode;
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint._do: return CGRIDClass__Proto008PtEdo2ZkEntrypoint___do.decode;
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint.set_delegate: return CGRIDClass__Proto008PtEdo2ZkEntrypoint__set_delegate.decode;
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint.remove_delegate: return CGRIDClass__Proto008PtEdo2ZkEntrypoint__remove_delegate.decode;
            case CGRIDTag__Proto008PtEdo2ZkEntrypoint.named: return CGRIDClass__Proto008PtEdo2ZkEntrypoint__named.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto008PtEdo2ZkEntrypoint => Object.values(CGRIDTag__Proto008PtEdo2ZkEntrypoint).includes(tagval);
    return f;
}
export class CGRIDClass__Proto008_PtEdo2ZkEntrypoint extends Box<Proto008PtEdo2ZkEntrypoint> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto008PtEdo2ZkEntrypoint>, Proto008PtEdo2ZkEntrypoint>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkEntrypoint {
        return new this(variant_decoder(width.Uint8)(proto008ptedo2zkentrypoint_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad generated for Proto008PtEdo2ZkContractIdOriginatedDenestPad
export class CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad extends Box<Proto008PtEdo2ZkContractIdOriginatedDenestPad> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contract_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkContract_id_Originated_denest_pad {
        return new this(record_decoder<Proto008PtEdo2ZkContractIdOriginatedDenestPad>({contract_hash: FixedBytes.decode<20>({len: 20})}, {order: ['contract_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.contract_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contract_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkContract_id generated for Proto008PtEdo2ZkContractId
export function proto008ptedo2zkcontractid_mkDecoder(): VariantDecoder<CGRIDTag__Proto008PtEdo2ZkContractId,Proto008PtEdo2ZkContractId> {
    function f(disc: CGRIDTag__Proto008PtEdo2ZkContractId) {
        switch (disc) {
            case CGRIDTag__Proto008PtEdo2ZkContractId.Implicit: return CGRIDClass__Proto008PtEdo2ZkContractId__Implicit.decode;
            case CGRIDTag__Proto008PtEdo2ZkContractId.Originated: return CGRIDClass__Proto008PtEdo2ZkContractId__Originated.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto008PtEdo2ZkContractId => Object.values(CGRIDTag__Proto008PtEdo2ZkContractId).includes(tagval);
    return f;
}
export class CGRIDClass__Proto008_PtEdo2ZkContract_id extends Box<Proto008PtEdo2ZkContractId> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto008PtEdo2ZkContractId>, Proto008PtEdo2ZkContractId>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkContract_id {
        return new this(variant_decoder(width.Uint8)(proto008ptedo2zkcontractid_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaUnsigned_contents_seed_nonce_hash generated for Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash
export class CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaUnsigned_contents_seed_nonce_hash extends Box<Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['cycle_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaUnsigned_contents_seed_nonce_hash {
        return new this(record_decoder<Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash>({cycle_nonce: FixedBytes.decode<32>({len: 32})}, {order: ['cycle_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.cycle_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.cycle_nonce.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaSigned_contents_signature generated for Proto008PtEdo2ZkBlockHeaderAlphaSignedContentsSignature
export class CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaSigned_contents_signature extends Box<Proto008PtEdo2ZkBlockHeaderAlphaSignedContentsSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v0']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaSigned_contents_signature {
        return new this(record_decoder<Proto008PtEdo2ZkBlockHeaderAlphaSignedContentsSignature>({signature_v0: FixedBytes.decode<64>({len: 64})}, {order: ['signature_v0']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v0.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v0.writeTarget(tgt));
    }
}
// Class CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaFull_header generated for Proto008PtEdo2ZkBlockHeaderAlphaFullHeader
export class CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaFull_header extends Box<Proto008PtEdo2ZkBlockHeaderAlphaFullHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaFull_header {
        return new this(record_decoder<Proto008PtEdo2ZkBlockHeaderAlphaFullHeader>({level: Int32.decode, proto: Uint8.decode, predecessor: CGRIDClass__Block_headerShell_predecessor.decode, timestamp: Int64.decode, validation_pass: Uint8.decode, operations_hash: CGRIDClass__Block_headerShell_operations_hash.decode, fitness: Dynamic.decode(Sequence.decode(Dynamic.decode(Bytes.decode, width.Uint30)), width.Uint30), context: CGRIDClass__Block_headerShell_context.decode, priority: Uint16.decode, proof_of_work_nonce: FixedBytes.decode<8>({len: 8}), seed_nonce_hash: Option.decode(CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaUnsigned_contents_seed_nonce_hash.decode), signature: CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaSigned_contents_signature.decode}, {order: ['level', 'proto', 'predecessor', 'timestamp', 'validation_pass', 'operations_hash', 'fitness', 'context', 'priority', 'proof_of_work_nonce', 'seed_nonce_hash', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.proto.encodeLength +  this.value.predecessor.encodeLength +  this.value.timestamp.encodeLength +  this.value.validation_pass.encodeLength +  this.value.operations_hash.encodeLength +  this.value.fitness.encodeLength +  this.value.context.encodeLength +  this.value.priority.encodeLength +  this.value.proof_of_work_nonce.encodeLength +  this.value.seed_nonce_hash.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.proto.writeTarget(tgt) +  this.value.predecessor.writeTarget(tgt) +  this.value.timestamp.writeTarget(tgt) +  this.value.validation_pass.writeTarget(tgt) +  this.value.operations_hash.writeTarget(tgt) +  this.value.fitness.writeTarget(tgt) +  this.value.context.writeTarget(tgt) +  this.value.priority.writeTarget(tgt) +  this.value.proof_of_work_nonce.writeTarget(tgt) +  this.value.seed_nonce_hash.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_predecessor generated for BlockHeaderShellPredecessor
export class CGRIDClass__Block_headerShell_predecessor extends Box<BlockHeaderShellPredecessor> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_predecessor {
        return new this(record_decoder<BlockHeaderShellPredecessor>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_operations_hash generated for BlockHeaderShellOperationsHash
export class CGRIDClass__Block_headerShell_operations_hash extends Box<BlockHeaderShellOperationsHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_list_list_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_operations_hash {
        return new this(record_decoder<BlockHeaderShellOperationsHash>({operation_list_list_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_list_list_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_list_list_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_list_list_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Block_headerShell_context generated for BlockHeaderShellContext
export class CGRIDClass__Block_headerShell_context extends Box<BlockHeaderShellContext> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['context_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Block_headerShell_context {
        return new this(record_decoder<BlockHeaderShellContext>({context_hash: FixedBytes.decode<32>({len: 32})}, {order: ['context_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.context_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.context_hash.writeTarget(tgt));
    }
}
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] };
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export type BlockHeaderShellPredecessor = { block_hash: FixedBytes<32> };
export type BlockHeaderShellOperationsHash = { operation_list_list_hash: FixedBytes<32> };
export type BlockHeaderShellContext = { context_hash: FixedBytes<32> };
export type Proto008PtEdo2ZkScriptedContracts = { code: Dynamic<Bytes,width.Uint30>, storage: Dynamic<Bytes,width.Uint30> };
export type Proto008PtEdo2ZkOperationAlphaContentsAndSignatureSignature = { signature_v0: FixedBytes<64> };
export type Proto008PtEdo2ZkOperationAlphaContentsTransactionSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto008PtEdo2ZkOperationAlphaContentsTransactionParameters = { entrypoint: CGRIDClass__Proto008_PtEdo2ZkEntrypoint, value: Dynamic<Bytes,width.Uint30> };
export type Proto008PtEdo2ZkOperationAlphaContentsRevealSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto008PtEdo2ZkOperationAlphaContentsRevealPublicKey = { signature_v0_public_key: CGRIDClass__Public_key };
export type Proto008PtEdo2ZkOperationAlphaContentsProposalsSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto008PtEdo2ZkOperationAlphaContentsProposalsProposalsDenestDynDenestSeq = { protocol_hash: FixedBytes<32> };
export type Proto008PtEdo2ZkOperationAlphaContentsOriginationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto008PtEdo2ZkOperationAlphaContentsOriginationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto008PtEdo2ZkOperationAlphaContentsDelegationSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto008PtEdo2ZkOperationAlphaContentsDelegationDelegate = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto008PtEdo2ZkOperationAlphaContentsBallotSource = { signature_v0_public_key_hash: CGRIDClass__Public_key_hash };
export type Proto008PtEdo2ZkOperationAlphaContentsBallotProposal = { protocol_hash: FixedBytes<32> };
export type Proto008PtEdo2ZkOperationAlphaContentsActivateAccountPkh = { ed25519_public_key_hash: FixedBytes<20> };
export enum CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents{
    Endorsement = 0,
    Seed_nonce_revelation = 1,
    Double_endorsement_evidence = 2,
    Double_baking_evidence = 3,
    Activate_account = 4,
    Proposals = 5,
    Ballot = 6,
    Reveal = 107,
    Transaction = 108,
    Origination = 109,
    Delegation = 110
}
export interface CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents {
    Endorsement: CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Endorsement,
    Seed_nonce_revelation: CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Seed_nonce_revelation,
    Double_endorsement_evidence: CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Double_endorsement_evidence,
    Double_baking_evidence: CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Double_baking_evidence,
    Activate_account: CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Activate_account,
    Proposals: CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Proposals,
    Ballot: CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Ballot,
    Reveal: CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Reveal,
    Transaction: CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Transaction,
    Origination: CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Origination,
    Delegation: CGRIDClass__Proto008PtEdo2ZkOperationAlphaContents__Delegation
}
export type Proto008PtEdo2ZkOperationAlphaContents = { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Endorsement, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents['Endorsement'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Seed_nonce_revelation, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents['Seed_nonce_revelation'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Double_endorsement_evidence, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents['Double_endorsement_evidence'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Double_baking_evidence, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents['Double_baking_evidence'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Activate_account, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents['Activate_account'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Proposals, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents['Proposals'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Ballot, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents['Ballot'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Reveal, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents['Reveal'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Transaction, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents['Transaction'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Origination, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents['Origination'] } | { kind: CGRIDTag__Proto008PtEdo2ZkOperationAlphaContents.Delegation, value: CGRIDMap__Proto008PtEdo2ZkOperationAlphaContents['Delegation'] };
export type Proto008PtEdo2ZkInlinedEndorsementSignature = { signature_v0: FixedBytes<64> };
export enum CGRIDTag__Proto008PtEdo2ZkInlinedEndorsementContents{
    Endorsement = 0
}
export interface CGRIDMap__Proto008PtEdo2ZkInlinedEndorsementContents {
    Endorsement: CGRIDClass__Proto008PtEdo2ZkInlinedEndorsementContents__Endorsement
}
export type Proto008PtEdo2ZkInlinedEndorsementContents = { kind: CGRIDTag__Proto008PtEdo2ZkInlinedEndorsementContents.Endorsement, value: CGRIDMap__Proto008PtEdo2ZkInlinedEndorsementContents['Endorsement'] };
export type Proto008PtEdo2ZkInlinedEndorsement = { branch: CGRIDClass__OperationShell_header_branch, operations: CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsementContents, signature: Nullable<CGRIDClass__Proto008_PtEdo2ZkInlinedEndorsement_signature> };
export enum CGRIDTag__Proto008PtEdo2ZkEntrypoint{
    _default = 0,
    root = 1,
    _do = 2,
    set_delegate = 3,
    remove_delegate = 4,
    named = 255
}
export interface CGRIDMap__Proto008PtEdo2ZkEntrypoint {
    _default: CGRIDClass__Proto008PtEdo2ZkEntrypoint___default,
    root: CGRIDClass__Proto008PtEdo2ZkEntrypoint__root,
    _do: CGRIDClass__Proto008PtEdo2ZkEntrypoint___do,
    set_delegate: CGRIDClass__Proto008PtEdo2ZkEntrypoint__set_delegate,
    remove_delegate: CGRIDClass__Proto008PtEdo2ZkEntrypoint__remove_delegate,
    named: CGRIDClass__Proto008PtEdo2ZkEntrypoint__named
}
export type Proto008PtEdo2ZkEntrypoint = { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint._default, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['_default'] } | { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint.root, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['root'] } | { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint._do, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['_do'] } | { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint.set_delegate, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['set_delegate'] } | { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint.remove_delegate, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['remove_delegate'] } | { kind: CGRIDTag__Proto008PtEdo2ZkEntrypoint.named, value: CGRIDMap__Proto008PtEdo2ZkEntrypoint['named'] };
export type Proto008PtEdo2ZkContractIdOriginatedDenestPad = { contract_hash: FixedBytes<20> };
export enum CGRIDTag__Proto008PtEdo2ZkContractId{
    Implicit = 0,
    Originated = 1
}
export interface CGRIDMap__Proto008PtEdo2ZkContractId {
    Implicit: CGRIDClass__Proto008PtEdo2ZkContractId__Implicit,
    Originated: CGRIDClass__Proto008PtEdo2ZkContractId__Originated
}
export type Proto008PtEdo2ZkContractId = { kind: CGRIDTag__Proto008PtEdo2ZkContractId.Implicit, value: CGRIDMap__Proto008PtEdo2ZkContractId['Implicit'] } | { kind: CGRIDTag__Proto008PtEdo2ZkContractId.Originated, value: CGRIDMap__Proto008PtEdo2ZkContractId['Originated'] };
export type Proto008PtEdo2ZkBlockHeaderAlphaUnsignedContentsSeedNonceHash = { cycle_nonce: FixedBytes<32> };
export type Proto008PtEdo2ZkBlockHeaderAlphaSignedContentsSignature = { signature_v0: FixedBytes<64> };
export type Proto008PtEdo2ZkBlockHeaderAlphaFullHeader = { level: Int32, proto: Uint8, predecessor: CGRIDClass__Block_headerShell_predecessor, timestamp: Int64, validation_pass: Uint8, operations_hash: CGRIDClass__Block_headerShell_operations_hash, fitness: Dynamic<Sequence<Dynamic<Bytes,width.Uint30>>,width.Uint30>, context: CGRIDClass__Block_headerShell_context, priority: Uint16, proof_of_work_nonce: FixedBytes<8>, seed_nonce_hash: Option<CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaUnsigned_contents_seed_nonce_hash>, signature: CGRIDClass__Proto008_PtEdo2ZkBlock_headerAlphaSigned_contents_signature };
export type Proto008PtEdo2ZkOperationProtocolData = { contents: VPadded<Sequence<CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents>,64>, signature: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_and_signature_signature };
export class CGRIDClass__Proto008PtEdo2ZkOperationProtocolData extends Box<Proto008PtEdo2ZkOperationProtocolData> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['contents', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkOperationProtocolData {
        return new this(record_decoder<Proto008PtEdo2ZkOperationProtocolData>({contents: VPadded.decode(Sequence.decode(CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents.decode), 64), signature: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_and_signature_signature.decode}, {order: ['contents', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.contents.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.contents.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
export const proto008_ptedo2zk_operation_protocol_data_encoder = (value: Proto008PtEdo2ZkOperationProtocolData): OutputBytes => {
    return record_encoder({order: ['contents', 'signature']})(value);
}
export const proto008_ptedo2zk_operation_protocol_data_decoder = (p: Parser): Proto008PtEdo2ZkOperationProtocolData => {
    return record_decoder<Proto008PtEdo2ZkOperationProtocolData>({contents: VPadded.decode(Sequence.decode(CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents.decode), 64), signature: CGRIDClass__Proto008_PtEdo2ZkOperationAlphaContents_and_signature_signature.decode}, {order: ['contents', 'signature']})(p);
}
