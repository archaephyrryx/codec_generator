import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto008PtEdo2ZkRoll = Int32;
export class CGRIDClass__Proto008PtEdo2ZkRoll extends Box<Proto008PtEdo2ZkRoll> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkRoll {
        return new this(Int32.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto008_ptedo2zk_roll_encoder = (value: Proto008PtEdo2ZkRoll): OutputBytes => {
    return value.encode();
}
export const proto008_ptedo2zk_roll_decoder = (p: Parser): Proto008PtEdo2ZkRoll => {
    return Int32.decode(p);
}
