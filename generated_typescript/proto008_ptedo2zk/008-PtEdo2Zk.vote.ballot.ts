import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int8 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type Proto008PtEdo2ZkVoteBallot = Int8;
export class CGRIDClass__Proto008PtEdo2ZkVoteBallot extends Box<Proto008PtEdo2ZkVoteBallot> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkVoteBallot {
        return new this(Int8.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const proto008_ptedo2zk_vote_ballot_encoder = (value: Proto008PtEdo2ZkVoteBallot): OutputBytes => {
    return value.encode();
}
export const proto008_ptedo2zk_vote_ballot_decoder = (p: Parser): Proto008PtEdo2ZkVoteBallot => {
    return Int8.decode(p);
}
