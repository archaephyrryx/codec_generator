import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
import { Z } from '../../ts_runtime/zarith/integer';
// Class CGRIDClass__Proto008PtEdo2ZkGas__Unaccounted generated for Proto008PtEdo2ZkGas__Unaccounted
export class CGRIDClass__Proto008PtEdo2ZkGas__Unaccounted extends Box<Proto008PtEdo2ZkGas__Unaccounted> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkGas__Unaccounted {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkGas__Limited generated for Proto008PtEdo2ZkGas__Limited
export class CGRIDClass__Proto008PtEdo2ZkGas__Limited extends Box<Proto008PtEdo2ZkGas__Limited> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkGas__Limited {
        return new this(Z.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto008PtEdo2ZkGas__Unaccounted = Unit;
export type Proto008PtEdo2ZkGas__Limited = Z;
export enum CGRIDTag__Proto008PtEdo2ZkGas{
    Limited = 0,
    Unaccounted = 1
}
export interface CGRIDMap__Proto008PtEdo2ZkGas {
    Limited: CGRIDClass__Proto008PtEdo2ZkGas__Limited,
    Unaccounted: CGRIDClass__Proto008PtEdo2ZkGas__Unaccounted
}
export type Proto008PtEdo2ZkGas = { kind: CGRIDTag__Proto008PtEdo2ZkGas.Limited, value: CGRIDMap__Proto008PtEdo2ZkGas['Limited'] } | { kind: CGRIDTag__Proto008PtEdo2ZkGas.Unaccounted, value: CGRIDMap__Proto008PtEdo2ZkGas['Unaccounted'] };
export function proto008ptedo2zkgas_mkDecoder(): VariantDecoder<CGRIDTag__Proto008PtEdo2ZkGas,Proto008PtEdo2ZkGas> {
    function f(disc: CGRIDTag__Proto008PtEdo2ZkGas) {
        switch (disc) {
            case CGRIDTag__Proto008PtEdo2ZkGas.Limited: return CGRIDClass__Proto008PtEdo2ZkGas__Limited.decode;
            case CGRIDTag__Proto008PtEdo2ZkGas.Unaccounted: return CGRIDClass__Proto008PtEdo2ZkGas__Unaccounted.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto008PtEdo2ZkGas => Object.values(CGRIDTag__Proto008PtEdo2ZkGas).includes(tagval);
    return f;
}
export class CGRIDClass__Proto008PtEdo2ZkGas extends Box<Proto008PtEdo2ZkGas> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto008PtEdo2ZkGas>, Proto008PtEdo2ZkGas>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkGas {
        return new this(variant_decoder(width.Uint8)(proto008ptedo2zkgas_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto008_ptedo2zk_gas_encoder = (value: Proto008PtEdo2ZkGas): OutputBytes => {
    return variant_encoder<KindOf<Proto008PtEdo2ZkGas>, Proto008PtEdo2ZkGas>(width.Uint8)(value);
}
export const proto008_ptedo2zk_gas_decoder = (p: Parser): Proto008PtEdo2ZkGas => {
    return variant_decoder(width.Uint8)(proto008ptedo2zkgas_mkDecoder())(p);
}
