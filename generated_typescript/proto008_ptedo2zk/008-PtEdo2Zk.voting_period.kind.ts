import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Testing_vote generated for Proto008PtEdo2ZkVotingPeriodKind__Testing_vote
export class CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Testing_vote extends Box<Proto008PtEdo2ZkVotingPeriodKind__Testing_vote> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Testing_vote {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Testing generated for Proto008PtEdo2ZkVotingPeriodKind__Testing
export class CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Testing extends Box<Proto008PtEdo2ZkVotingPeriodKind__Testing> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Testing {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Proposal generated for Proto008PtEdo2ZkVotingPeriodKind__Proposal
export class CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Proposal extends Box<Proto008PtEdo2ZkVotingPeriodKind__Proposal> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Proposal {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Promotion_vote generated for Proto008PtEdo2ZkVotingPeriodKind__Promotion_vote
export class CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Promotion_vote extends Box<Proto008PtEdo2ZkVotingPeriodKind__Promotion_vote> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Promotion_vote {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
// Class CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Adoption generated for Proto008PtEdo2ZkVotingPeriodKind__Adoption
export class CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Adoption extends Box<Proto008PtEdo2ZkVotingPeriodKind__Adoption> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Adoption {
        return new this(Unit.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export type Proto008PtEdo2ZkVotingPeriodKind__Testing_vote = Unit;
export type Proto008PtEdo2ZkVotingPeriodKind__Testing = Unit;
export type Proto008PtEdo2ZkVotingPeriodKind__Proposal = Unit;
export type Proto008PtEdo2ZkVotingPeriodKind__Promotion_vote = Unit;
export type Proto008PtEdo2ZkVotingPeriodKind__Adoption = Unit;
export enum CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind{
    Proposal = 0,
    Testing_vote = 1,
    Testing = 2,
    Promotion_vote = 3,
    Adoption = 4
}
export interface CGRIDMap__Proto008PtEdo2ZkVotingPeriodKind {
    Proposal: CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Proposal,
    Testing_vote: CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Testing_vote,
    Testing: CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Testing,
    Promotion_vote: CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Promotion_vote,
    Adoption: CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Adoption
}
export type Proto008PtEdo2ZkVotingPeriodKind = { kind: CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind.Proposal, value: CGRIDMap__Proto008PtEdo2ZkVotingPeriodKind['Proposal'] } | { kind: CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind.Testing_vote, value: CGRIDMap__Proto008PtEdo2ZkVotingPeriodKind['Testing_vote'] } | { kind: CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind.Testing, value: CGRIDMap__Proto008PtEdo2ZkVotingPeriodKind['Testing'] } | { kind: CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind.Promotion_vote, value: CGRIDMap__Proto008PtEdo2ZkVotingPeriodKind['Promotion_vote'] } | { kind: CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind.Adoption, value: CGRIDMap__Proto008PtEdo2ZkVotingPeriodKind['Adoption'] };
export function proto008ptedo2zkvotingperiodkind_mkDecoder(): VariantDecoder<CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind,Proto008PtEdo2ZkVotingPeriodKind> {
    function f(disc: CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind) {
        switch (disc) {
            case CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind.Proposal: return CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Proposal.decode;
            case CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind.Testing_vote: return CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Testing_vote.decode;
            case CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind.Testing: return CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Testing.decode;
            case CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind.Promotion_vote: return CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Promotion_vote.decode;
            case CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind.Adoption: return CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind__Adoption.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind => Object.values(CGRIDTag__Proto008PtEdo2ZkVotingPeriodKind).includes(tagval);
    return f;
}
export class CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind extends Box<Proto008PtEdo2ZkVotingPeriodKind> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<Proto008PtEdo2ZkVotingPeriodKind>, Proto008PtEdo2ZkVotingPeriodKind>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Proto008PtEdo2ZkVotingPeriodKind {
        return new this(variant_decoder(width.Uint8)(proto008ptedo2zkvotingperiodkind_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const proto008_ptedo2zk_voting_period_kind_encoder = (value: Proto008PtEdo2ZkVotingPeriodKind): OutputBytes => {
    return variant_encoder<KindOf<Proto008PtEdo2ZkVotingPeriodKind>, Proto008PtEdo2ZkVotingPeriodKind>(width.Uint8)(value);
}
export const proto008_ptedo2zk_voting_period_kind_decoder = (p: Parser): Proto008PtEdo2ZkVotingPeriodKind => {
    return variant_decoder(width.Uint8)(proto008ptedo2zkvotingperiodkind_mkDecoder())(p);
}
