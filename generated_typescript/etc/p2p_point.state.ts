import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__P2p_pointState_Running_p2p_peer_id generated for P2pPointStateRunningP2pPeerId
export class CGRIDClass__P2p_pointState_Running_p2p_peer_id extends Box<P2pPointStateRunningP2pPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointState_Running_p2p_peer_id {
        return new this(record_decoder<P2pPointStateRunningP2pPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointState_Accepted_p2p_peer_id generated for P2pPointStateAcceptedP2pPeerId
export class CGRIDClass__P2p_pointState_Accepted_p2p_peer_id extends Box<P2pPointStateAcceptedP2pPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointState_Accepted_p2p_peer_id {
        return new this(record_decoder<P2pPointStateAcceptedP2pPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointState__Running generated for P2pPointState__Running
export class CGRIDClass__P2pPointState__Running extends Box<P2pPointState__Running> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'p2p_peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointState__Running {
        return new this(record_decoder<P2pPointState__Running>({event_kind: Unit.decode, p2p_peer_id: CGRIDClass__P2p_pointState_Running_p2p_peer_id.decode}, {order: ['event_kind', 'p2p_peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.p2p_peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.p2p_peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointState__Requested generated for P2pPointState__Requested
export class CGRIDClass__P2pPointState__Requested extends Box<P2pPointState__Requested> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointState__Requested {
        return new this(record_decoder<P2pPointState__Requested>({event_kind: Unit.decode, rhs: Unit.decode}, {order: ['event_kind', 'rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.rhs.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointState__Disconnected generated for P2pPointState__Disconnected
export class CGRIDClass__P2pPointState__Disconnected extends Box<P2pPointState__Disconnected> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointState__Disconnected {
        return new this(record_decoder<P2pPointState__Disconnected>({event_kind: Unit.decode, rhs: Unit.decode}, {order: ['event_kind', 'rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.rhs.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointState__Accepted generated for P2pPointState__Accepted
export class CGRIDClass__P2pPointState__Accepted extends Box<P2pPointState__Accepted> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'p2p_peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointState__Accepted {
        return new this(record_decoder<P2pPointState__Accepted>({event_kind: Unit.decode, p2p_peer_id: CGRIDClass__P2p_pointState_Accepted_p2p_peer_id.decode}, {order: ['event_kind', 'p2p_peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.p2p_peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.p2p_peer_id.writeTarget(tgt));
    }
}
export type P2pPointStateRunningP2pPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointStateAcceptedP2pPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointState__Running = { event_kind: Unit, p2p_peer_id: CGRIDClass__P2p_pointState_Running_p2p_peer_id };
export type P2pPointState__Requested = { event_kind: Unit, rhs: Unit };
export type P2pPointState__Disconnected = { event_kind: Unit, rhs: Unit };
export type P2pPointState__Accepted = { event_kind: Unit, p2p_peer_id: CGRIDClass__P2p_pointState_Accepted_p2p_peer_id };
export enum CGRIDTag__P2pPointState{
    Requested = 0,
    Accepted = 1,
    Running = 2,
    Disconnected = 3
}
export interface CGRIDMap__P2pPointState {
    Requested: CGRIDClass__P2pPointState__Requested,
    Accepted: CGRIDClass__P2pPointState__Accepted,
    Running: CGRIDClass__P2pPointState__Running,
    Disconnected: CGRIDClass__P2pPointState__Disconnected
}
export type P2pPointState = { kind: CGRIDTag__P2pPointState.Requested, value: CGRIDMap__P2pPointState['Requested'] } | { kind: CGRIDTag__P2pPointState.Accepted, value: CGRIDMap__P2pPointState['Accepted'] } | { kind: CGRIDTag__P2pPointState.Running, value: CGRIDMap__P2pPointState['Running'] } | { kind: CGRIDTag__P2pPointState.Disconnected, value: CGRIDMap__P2pPointState['Disconnected'] };
export function p2ppointstate_mkDecoder(): VariantDecoder<CGRIDTag__P2pPointState,P2pPointState> {
    function f(disc: CGRIDTag__P2pPointState) {
        switch (disc) {
            case CGRIDTag__P2pPointState.Requested: return CGRIDClass__P2pPointState__Requested.decode;
            case CGRIDTag__P2pPointState.Accepted: return CGRIDClass__P2pPointState__Accepted.decode;
            case CGRIDTag__P2pPointState.Running: return CGRIDClass__P2pPointState__Running.decode;
            case CGRIDTag__P2pPointState.Disconnected: return CGRIDClass__P2pPointState__Disconnected.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__P2pPointState => Object.values(CGRIDTag__P2pPointState).includes(tagval);
    return f;
}
export class CGRIDClass__P2pPointState extends Box<P2pPointState> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<P2pPointState>, P2pPointState>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointState {
        return new this(variant_decoder(width.Uint8)(p2ppointstate_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const p2p_point_state_encoder = (value: P2pPointState): OutputBytes => {
    return variant_encoder<KindOf<P2pPointState>, P2pPointState>(width.Uint8)(value);
}
export const p2p_point_state_decoder = (p: Parser): P2pPointState => {
    return variant_decoder(width.Uint8)(p2ppointstate_mkDecoder())(p);
}
