import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
export type NetworkVersion = { chain_name: Dynamic<U8String,width.Uint30>, distributed_db_version: Uint16, p2p_version: Uint16 };
export class CGRIDClass__NetworkVersion extends Box<NetworkVersion> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['chain_name', 'distributed_db_version', 'p2p_version']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__NetworkVersion {
        return new this(record_decoder<NetworkVersion>({chain_name: Dynamic.decode(U8String.decode, width.Uint30), distributed_db_version: Uint16.decode, p2p_version: Uint16.decode}, {order: ['chain_name', 'distributed_db_version', 'p2p_version']})(p));
    };
    get encodeLength(): number {
        return (this.value.chain_name.encodeLength +  this.value.distributed_db_version.encodeLength +  this.value.p2p_version.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.chain_name.writeTarget(tgt) +  this.value.distributed_db_version.writeTarget(tgt) +  this.value.p2p_version.writeTarget(tgt));
    }
}
export const network_version_encoder = (value: NetworkVersion): OutputBytes => {
    return record_encoder({order: ['chain_name', 'distributed_db_version', 'p2p_version']})(value);
}
export const network_version_decoder = (p: Parser): NetworkVersion => {
    return record_decoder<NetworkVersion>({chain_name: Dynamic.decode(U8String.decode, width.Uint30), distributed_db_version: Uint16.decode, p2p_version: Uint16.decode}, {order: ['chain_name', 'distributed_db_version', 'p2p_version']})(p);
}
