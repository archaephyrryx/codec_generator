import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Mempool_pending_denest_dyn_denest_dyn_denest_seq generated for MempoolPendingDenestDynDenestDynDenestSeq
export class CGRIDClass__Mempool_pending_denest_dyn_denest_dyn_denest_seq extends Box<MempoolPendingDenestDynDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Mempool_pending_denest_dyn_denest_dyn_denest_seq {
        return new this(record_decoder<MempoolPendingDenestDynDenestDynDenestSeq>({operation_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Mempool_known_valid_denest_dyn_denest_seq generated for MempoolKnownValidDenestDynDenestSeq
export class CGRIDClass__Mempool_known_valid_denest_dyn_denest_seq extends Box<MempoolKnownValidDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['operation_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Mempool_known_valid_denest_dyn_denest_seq {
        return new this(record_decoder<MempoolKnownValidDenestDynDenestSeq>({operation_hash: FixedBytes.decode<32>({len: 32})}, {order: ['operation_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.operation_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.operation_hash.writeTarget(tgt));
    }
}
export type MempoolPendingDenestDynDenestDynDenestSeq = { operation_hash: FixedBytes<32> };
export type MempoolKnownValidDenestDynDenestSeq = { operation_hash: FixedBytes<32> };
export type Mempool = { known_valid: Dynamic<Sequence<CGRIDClass__Mempool_known_valid_denest_dyn_denest_seq>,width.Uint30>, pending: Dynamic<Dynamic<Sequence<CGRIDClass__Mempool_pending_denest_dyn_denest_dyn_denest_seq>,width.Uint30>,width.Uint30> };
export class CGRIDClass__Mempool extends Box<Mempool> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['known_valid', 'pending']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Mempool {
        return new this(record_decoder<Mempool>({known_valid: Dynamic.decode(Sequence.decode(CGRIDClass__Mempool_known_valid_denest_dyn_denest_seq.decode), width.Uint30), pending: Dynamic.decode(Dynamic.decode(Sequence.decode(CGRIDClass__Mempool_pending_denest_dyn_denest_dyn_denest_seq.decode), width.Uint30), width.Uint30)}, {order: ['known_valid', 'pending']})(p));
    };
    get encodeLength(): number {
        return (this.value.known_valid.encodeLength +  this.value.pending.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.known_valid.writeTarget(tgt) +  this.value.pending.writeTarget(tgt));
    }
}
export const mempool_encoder = (value: Mempool): OutputBytes => {
    return record_encoder({order: ['known_valid', 'pending']})(value);
}
export const mempool_decoder = (p: Parser): Mempool => {
    return record_decoder<Mempool>({known_valid: Dynamic.decode(Sequence.decode(CGRIDClass__Mempool_known_valid_denest_dyn_denest_seq.decode), width.Uint30), pending: Dynamic.decode(Dynamic.decode(Sequence.decode(CGRIDClass__Mempool_pending_denest_dyn_denest_dyn_denest_seq.decode), width.Uint30), width.Uint30)}, {order: ['known_valid', 'pending']})(p);
}
