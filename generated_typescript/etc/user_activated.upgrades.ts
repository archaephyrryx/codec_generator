import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int32 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__User_activatedUpgrades_denest_dyn_denest_seq_replacement_protocol generated for UserActivatedUpgradesDenestDynDenestSeqReplacementProtocol
export class CGRIDClass__User_activatedUpgrades_denest_dyn_denest_seq_replacement_protocol extends Box<UserActivatedUpgradesDenestDynDenestSeqReplacementProtocol> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__User_activatedUpgrades_denest_dyn_denest_seq_replacement_protocol {
        return new this(record_decoder<UserActivatedUpgradesDenestDynDenestSeqReplacementProtocol>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__User_activatedUpgrades_denest_dyn_denest_seq generated for UserActivatedUpgradesDenestDynDenestSeq
export class CGRIDClass__User_activatedUpgrades_denest_dyn_denest_seq extends Box<UserActivatedUpgradesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['level', 'replacement_protocol']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__User_activatedUpgrades_denest_dyn_denest_seq {
        return new this(record_decoder<UserActivatedUpgradesDenestDynDenestSeq>({level: Int32.decode, replacement_protocol: CGRIDClass__User_activatedUpgrades_denest_dyn_denest_seq_replacement_protocol.decode}, {order: ['level', 'replacement_protocol']})(p));
    };
    get encodeLength(): number {
        return (this.value.level.encodeLength +  this.value.replacement_protocol.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.level.writeTarget(tgt) +  this.value.replacement_protocol.writeTarget(tgt));
    }
}
export type UserActivatedUpgradesDenestDynDenestSeqReplacementProtocol = { protocol_hash: FixedBytes<32> };
export type UserActivatedUpgradesDenestDynDenestSeq = { level: Int32, replacement_protocol: CGRIDClass__User_activatedUpgrades_denest_dyn_denest_seq_replacement_protocol };
export type UserActivatedUpgrades = Dynamic<Sequence<CGRIDClass__User_activatedUpgrades_denest_dyn_denest_seq>,width.Uint30>;
export class CGRIDClass__UserActivatedUpgrades extends Box<UserActivatedUpgrades> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__UserActivatedUpgrades {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__User_activatedUpgrades_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const user_activated_upgrades_encoder = (value: UserActivatedUpgrades): OutputBytes => {
    return value.encode();
}
export const user_activated_upgrades_decoder = (p: Parser): UserActivatedUpgrades => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__User_activatedUpgrades_denest_dyn_denest_seq.decode), width.Uint30)(p);
}
