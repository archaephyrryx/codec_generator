import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type TimestampSystem = Int64;
export class CGRIDClass__TimestampSystem extends Box<TimestampSystem> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__TimestampSystem {
        return new this(Int64.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const timestamp_system_encoder = (value: TimestampSystem): OutputBytes => {
    return value.encode();
}
export const timestamp_system_decoder = (p: Parser): TimestampSystem => {
    return Int64.decode(p);
}
