import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Protocol_components_denest_dyn_denest_seq generated for ProtocolComponentsDenestDynDenestSeq
export class CGRIDClass__Protocol_components_denest_dyn_denest_seq extends Box<ProtocolComponentsDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['name', '_interface', 'implementation']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Protocol_components_denest_dyn_denest_seq {
        return new this(record_decoder<ProtocolComponentsDenestDynDenestSeq>({name: Dynamic.decode(U8String.decode, width.Uint30), _interface: Option.decode(Dynamic.decode(U8String.decode, width.Uint30)), implementation: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['name', '_interface', 'implementation']})(p));
    };
    get encodeLength(): number {
        return (this.value.name.encodeLength +  this.value._interface.encodeLength +  this.value.implementation.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.name.writeTarget(tgt) +  this.value._interface.writeTarget(tgt) +  this.value.implementation.writeTarget(tgt));
    }
}
export type ProtocolComponentsDenestDynDenestSeq = { name: Dynamic<U8String,width.Uint30>, _interface: Option<Dynamic<U8String,width.Uint30>>, implementation: Dynamic<U8String,width.Uint30> };
export type Protocol = { expected_env_version: Uint16, components: Dynamic<Sequence<CGRIDClass__Protocol_components_denest_dyn_denest_seq>,width.Uint30> };
export class CGRIDClass__Protocol extends Box<Protocol> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['expected_env_version', 'components']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Protocol {
        return new this(record_decoder<Protocol>({expected_env_version: Uint16.decode, components: Dynamic.decode(Sequence.decode(CGRIDClass__Protocol_components_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['expected_env_version', 'components']})(p));
    };
    get encodeLength(): number {
        return (this.value.expected_env_version.encodeLength +  this.value.components.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.expected_env_version.writeTarget(tgt) +  this.value.components.writeTarget(tgt));
    }
}
export const protocol_encoder = (value: Protocol): OutputBytes => {
    return record_encoder({order: ['expected_env_version', 'components']})(value);
}
export const protocol_decoder = (p: Parser): Protocol => {
    return record_decoder<Protocol>({expected_env_version: Uint16.decode, components: Dynamic.decode(Sequence.decode(CGRIDClass__Protocol_components_denest_dyn_denest_seq.decode), width.Uint30)}, {order: ['expected_env_version', 'components']})(p);
}
