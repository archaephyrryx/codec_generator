import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { Target } from '../../ts_runtime/target';
export type SignerMessagesSupportsDeterministicNoncesResponse = { bool: Bool };
export class CGRIDClass__SignerMessagesSupportsDeterministicNoncesResponse extends Box<SignerMessagesSupportsDeterministicNoncesResponse> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bool']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesSupportsDeterministicNoncesResponse {
        return new this(record_decoder<SignerMessagesSupportsDeterministicNoncesResponse>({bool: Bool.decode}, {order: ['bool']})(p));
    };
    get encodeLength(): number {
        return (this.value.bool.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bool.writeTarget(tgt));
    }
}
export const signer_messages_supports_deterministic_nonces_response_encoder = (value: SignerMessagesSupportsDeterministicNoncesResponse): OutputBytes => {
    return record_encoder({order: ['bool']})(value);
}
export const signer_messages_supports_deterministic_nonces_response_decoder = (p: Parser): SignerMessagesSupportsDeterministicNoncesResponse => {
    return record_decoder<SignerMessagesSupportsDeterministicNoncesResponse>({bool: Bool.decode}, {order: ['bool']})(p);
}
