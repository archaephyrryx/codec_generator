import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__P2pPointPoolEventIndex1__Rejecting_request generated for P2pPointPoolEventIndex1__Rejecting_request
export class CGRIDClass__P2pPointPoolEventIndex1__Rejecting_request extends Box<P2pPointPoolEventIndex1__Rejecting_request> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'p2p_peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointPoolEventIndex1__Rejecting_request {
        return new this(record_decoder<P2pPointPoolEventIndex1__Rejecting_request>({event_kind: Unit.decode, p2p_peer_id: CGRIDClass__P2p_pointPool_event_index1_index0_Rejecting_request_p2p_peer_id.decode}, {order: ['event_kind', 'p2p_peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.p2p_peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.p2p_peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointPoolEventIndex1__Rejecting_rejected generated for P2pPointPoolEventIndex1__Rejecting_rejected
export class CGRIDClass__P2pPointPoolEventIndex1__Rejecting_rejected extends Box<P2pPointPoolEventIndex1__Rejecting_rejected> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'p2p_peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointPoolEventIndex1__Rejecting_rejected {
        return new this(record_decoder<P2pPointPoolEventIndex1__Rejecting_rejected>({event_kind: Unit.decode, p2p_peer_id: Option.decode(CGRIDClass__P2p_pointPool_event_index1_index0_Rejecting_rejected_p2p_peer_id.decode)}, {order: ['event_kind', 'p2p_peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.p2p_peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.p2p_peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointPoolEventIndex1__Outgoing_request generated for P2pPointPoolEventIndex1__Outgoing_request
export class CGRIDClass__P2pPointPoolEventIndex1__Outgoing_request extends Box<P2pPointPoolEventIndex1__Outgoing_request> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointPoolEventIndex1__Outgoing_request {
        return new this(record_decoder<P2pPointPoolEventIndex1__Outgoing_request>({event_kind: Unit.decode, rhs: Unit.decode}, {order: ['event_kind', 'rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.rhs.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointPoolEventIndex1__External_disconnection generated for P2pPointPoolEventIndex1__External_disconnection
export class CGRIDClass__P2pPointPoolEventIndex1__External_disconnection extends Box<P2pPointPoolEventIndex1__External_disconnection> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'p2p_peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointPoolEventIndex1__External_disconnection {
        return new this(record_decoder<P2pPointPoolEventIndex1__External_disconnection>({event_kind: Unit.decode, p2p_peer_id: CGRIDClass__P2p_pointPool_event_index1_index0_External_disconnection_p2p_peer_id.decode}, {order: ['event_kind', 'p2p_peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.p2p_peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.p2p_peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointPoolEventIndex1__Disconnection generated for P2pPointPoolEventIndex1__Disconnection
export class CGRIDClass__P2pPointPoolEventIndex1__Disconnection extends Box<P2pPointPoolEventIndex1__Disconnection> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'p2p_peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointPoolEventIndex1__Disconnection {
        return new this(record_decoder<P2pPointPoolEventIndex1__Disconnection>({event_kind: Unit.decode, p2p_peer_id: CGRIDClass__P2p_pointPool_event_index1_index0_Disconnection_p2p_peer_id.decode}, {order: ['event_kind', 'p2p_peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.p2p_peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.p2p_peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointPoolEventIndex1__Connection_established generated for P2pPointPoolEventIndex1__Connection_established
export class CGRIDClass__P2pPointPoolEventIndex1__Connection_established extends Box<P2pPointPoolEventIndex1__Connection_established> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'p2p_peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointPoolEventIndex1__Connection_established {
        return new this(record_decoder<P2pPointPoolEventIndex1__Connection_established>({event_kind: Unit.decode, p2p_peer_id: CGRIDClass__P2p_pointPool_event_index1_index0_Connection_established_p2p_peer_id.decode}, {order: ['event_kind', 'p2p_peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.p2p_peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.p2p_peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointPoolEventIndex1__Accepting_request generated for P2pPointPoolEventIndex1__Accepting_request
export class CGRIDClass__P2pPointPoolEventIndex1__Accepting_request extends Box<P2pPointPoolEventIndex1__Accepting_request> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'p2p_peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointPoolEventIndex1__Accepting_request {
        return new this(record_decoder<P2pPointPoolEventIndex1__Accepting_request>({event_kind: Unit.decode, p2p_peer_id: CGRIDClass__P2p_pointPool_event_index1_index0_Accepting_request_p2p_peer_id.decode}, {order: ['event_kind', 'p2p_peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.p2p_peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.p2p_peer_id.writeTarget(tgt));
    }
}
export type P2pPointPoolEventIndex1__Rejecting_request = { event_kind: Unit, p2p_peer_id: CGRIDClass__P2p_pointPool_event_index1_index0_Rejecting_request_p2p_peer_id };
export type P2pPointPoolEventIndex1__Rejecting_rejected = { event_kind: Unit, p2p_peer_id: Option<CGRIDClass__P2p_pointPool_event_index1_index0_Rejecting_rejected_p2p_peer_id> };
export type P2pPointPoolEventIndex1__Outgoing_request = { event_kind: Unit, rhs: Unit };
export type P2pPointPoolEventIndex1__External_disconnection = { event_kind: Unit, p2p_peer_id: CGRIDClass__P2p_pointPool_event_index1_index0_External_disconnection_p2p_peer_id };
export type P2pPointPoolEventIndex1__Disconnection = { event_kind: Unit, p2p_peer_id: CGRIDClass__P2p_pointPool_event_index1_index0_Disconnection_p2p_peer_id };
export type P2pPointPoolEventIndex1__Connection_established = { event_kind: Unit, p2p_peer_id: CGRIDClass__P2p_pointPool_event_index1_index0_Connection_established_p2p_peer_id };
export type P2pPointPoolEventIndex1__Accepting_request = { event_kind: Unit, p2p_peer_id: CGRIDClass__P2p_pointPool_event_index1_index0_Accepting_request_p2p_peer_id };
// Class CGRIDClass__P2p_pointPool_event_index1_index0_Rejecting_request_p2p_peer_id generated for P2pPointPoolEventIndex1Index0RejectingRequestP2pPeerId
export class CGRIDClass__P2p_pointPool_event_index1_index0_Rejecting_request_p2p_peer_id extends Box<P2pPointPoolEventIndex1Index0RejectingRequestP2pPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointPool_event_index1_index0_Rejecting_request_p2p_peer_id {
        return new this(record_decoder<P2pPointPoolEventIndex1Index0RejectingRequestP2pPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointPool_event_index1_index0_Rejecting_rejected_p2p_peer_id generated for P2pPointPoolEventIndex1Index0RejectingRejectedP2pPeerId
export class CGRIDClass__P2p_pointPool_event_index1_index0_Rejecting_rejected_p2p_peer_id extends Box<P2pPointPoolEventIndex1Index0RejectingRejectedP2pPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointPool_event_index1_index0_Rejecting_rejected_p2p_peer_id {
        return new this(record_decoder<P2pPointPoolEventIndex1Index0RejectingRejectedP2pPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointPool_event_index1_index0_External_disconnection_p2p_peer_id generated for P2pPointPoolEventIndex1Index0ExternalDisconnectionP2pPeerId
export class CGRIDClass__P2p_pointPool_event_index1_index0_External_disconnection_p2p_peer_id extends Box<P2pPointPoolEventIndex1Index0ExternalDisconnectionP2pPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointPool_event_index1_index0_External_disconnection_p2p_peer_id {
        return new this(record_decoder<P2pPointPoolEventIndex1Index0ExternalDisconnectionP2pPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointPool_event_index1_index0_Disconnection_p2p_peer_id generated for P2pPointPoolEventIndex1Index0DisconnectionP2pPeerId
export class CGRIDClass__P2p_pointPool_event_index1_index0_Disconnection_p2p_peer_id extends Box<P2pPointPoolEventIndex1Index0DisconnectionP2pPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointPool_event_index1_index0_Disconnection_p2p_peer_id {
        return new this(record_decoder<P2pPointPoolEventIndex1Index0DisconnectionP2pPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointPool_event_index1_index0_Connection_established_p2p_peer_id generated for P2pPointPoolEventIndex1Index0ConnectionEstablishedP2pPeerId
export class CGRIDClass__P2p_pointPool_event_index1_index0_Connection_established_p2p_peer_id extends Box<P2pPointPoolEventIndex1Index0ConnectionEstablishedP2pPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointPool_event_index1_index0_Connection_established_p2p_peer_id {
        return new this(record_decoder<P2pPointPoolEventIndex1Index0ConnectionEstablishedP2pPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointPool_event_index1_index0_Accepting_request_p2p_peer_id generated for P2pPointPoolEventIndex1Index0AcceptingRequestP2pPeerId
export class CGRIDClass__P2p_pointPool_event_index1_index0_Accepting_request_p2p_peer_id extends Box<P2pPointPoolEventIndex1Index0AcceptingRequestP2pPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointPool_event_index1_index0_Accepting_request_p2p_peer_id {
        return new this(record_decoder<P2pPointPoolEventIndex1Index0AcceptingRequestP2pPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointPool_event_index1 generated for P2pPointPoolEventIndex1
export function p2ppointpooleventindex1_mkDecoder(): VariantDecoder<CGRIDTag__P2pPointPoolEventIndex1,P2pPointPoolEventIndex1> {
    function f(disc: CGRIDTag__P2pPointPoolEventIndex1) {
        switch (disc) {
            case CGRIDTag__P2pPointPoolEventIndex1.Outgoing_request: return CGRIDClass__P2pPointPoolEventIndex1__Outgoing_request.decode;
            case CGRIDTag__P2pPointPoolEventIndex1.Accepting_request: return CGRIDClass__P2pPointPoolEventIndex1__Accepting_request.decode;
            case CGRIDTag__P2pPointPoolEventIndex1.Rejecting_request: return CGRIDClass__P2pPointPoolEventIndex1__Rejecting_request.decode;
            case CGRIDTag__P2pPointPoolEventIndex1.Rejecting_rejected: return CGRIDClass__P2pPointPoolEventIndex1__Rejecting_rejected.decode;
            case CGRIDTag__P2pPointPoolEventIndex1.Connection_established: return CGRIDClass__P2pPointPoolEventIndex1__Connection_established.decode;
            case CGRIDTag__P2pPointPoolEventIndex1.Disconnection: return CGRIDClass__P2pPointPoolEventIndex1__Disconnection.decode;
            case CGRIDTag__P2pPointPoolEventIndex1.External_disconnection: return CGRIDClass__P2pPointPoolEventIndex1__External_disconnection.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__P2pPointPoolEventIndex1 => Object.values(CGRIDTag__P2pPointPoolEventIndex1).includes(tagval);
    return f;
}
export class CGRIDClass__P2p_pointPool_event_index1 extends Box<P2pPointPoolEventIndex1> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<P2pPointPoolEventIndex1>, P2pPointPoolEventIndex1>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointPool_event_index1 {
        return new this(variant_decoder(width.Uint8)(p2ppointpooleventindex1_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export type P2pPointPoolEventIndex1Index0RejectingRequestP2pPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointPoolEventIndex1Index0RejectingRejectedP2pPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointPoolEventIndex1Index0ExternalDisconnectionP2pPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointPoolEventIndex1Index0DisconnectionP2pPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointPoolEventIndex1Index0ConnectionEstablishedP2pPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointPoolEventIndex1Index0AcceptingRequestP2pPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export enum CGRIDTag__P2pPointPoolEventIndex1{
    Outgoing_request = 0,
    Accepting_request = 1,
    Rejecting_request = 2,
    Rejecting_rejected = 3,
    Connection_established = 4,
    Disconnection = 5,
    External_disconnection = 6
}
export interface CGRIDMap__P2pPointPoolEventIndex1 {
    Outgoing_request: CGRIDClass__P2pPointPoolEventIndex1__Outgoing_request,
    Accepting_request: CGRIDClass__P2pPointPoolEventIndex1__Accepting_request,
    Rejecting_request: CGRIDClass__P2pPointPoolEventIndex1__Rejecting_request,
    Rejecting_rejected: CGRIDClass__P2pPointPoolEventIndex1__Rejecting_rejected,
    Connection_established: CGRIDClass__P2pPointPoolEventIndex1__Connection_established,
    Disconnection: CGRIDClass__P2pPointPoolEventIndex1__Disconnection,
    External_disconnection: CGRIDClass__P2pPointPoolEventIndex1__External_disconnection
}
export type P2pPointPoolEventIndex1 = { kind: CGRIDTag__P2pPointPoolEventIndex1.Outgoing_request, value: CGRIDMap__P2pPointPoolEventIndex1['Outgoing_request'] } | { kind: CGRIDTag__P2pPointPoolEventIndex1.Accepting_request, value: CGRIDMap__P2pPointPoolEventIndex1['Accepting_request'] } | { kind: CGRIDTag__P2pPointPoolEventIndex1.Rejecting_request, value: CGRIDMap__P2pPointPoolEventIndex1['Rejecting_request'] } | { kind: CGRIDTag__P2pPointPoolEventIndex1.Rejecting_rejected, value: CGRIDMap__P2pPointPoolEventIndex1['Rejecting_rejected'] } | { kind: CGRIDTag__P2pPointPoolEventIndex1.Connection_established, value: CGRIDMap__P2pPointPoolEventIndex1['Connection_established'] } | { kind: CGRIDTag__P2pPointPoolEventIndex1.Disconnection, value: CGRIDMap__P2pPointPoolEventIndex1['Disconnection'] } | { kind: CGRIDTag__P2pPointPoolEventIndex1.External_disconnection, value: CGRIDMap__P2pPointPoolEventIndex1['External_disconnection'] };
export type P2pPointPoolEvent = [Int64, CGRIDClass__P2p_pointPool_event_index1];
export class CGRIDClass__P2pPointPoolEvent extends Box<P2pPointPoolEvent> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointPoolEvent {
        return new this(tuple_decoder<P2pPointPoolEvent>(Int64.decode, CGRIDClass__P2p_pointPool_event_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
export const p2p_point_pool_event_encoder = (value: P2pPointPoolEvent): OutputBytes => {
    return tuple_encoder(value);
}
export const p2p_point_pool_event_decoder = (p: Parser): P2pPointPoolEvent => {
    return tuple_decoder<P2pPointPoolEvent>(Int64.decode, CGRIDClass__P2p_pointPool_event_index1.decode)(p);
}
