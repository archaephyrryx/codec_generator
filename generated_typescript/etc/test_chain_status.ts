import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Test_chain_status_Running_protocol generated for TestChainStatusRunningProtocol
export class CGRIDClass__Test_chain_status_Running_protocol extends Box<TestChainStatusRunningProtocol> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Test_chain_status_Running_protocol {
        return new this(record_decoder<TestChainStatusRunningProtocol>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Test_chain_status_Running_genesis generated for TestChainStatusRunningGenesis
export class CGRIDClass__Test_chain_status_Running_genesis extends Box<TestChainStatusRunningGenesis> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Test_chain_status_Running_genesis {
        return new this(record_decoder<TestChainStatusRunningGenesis>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Test_chain_status_Running_chain_id generated for TestChainStatusRunningChainId
export class CGRIDClass__Test_chain_status_Running_chain_id extends Box<TestChainStatusRunningChainId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['chain_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Test_chain_status_Running_chain_id {
        return new this(record_decoder<TestChainStatusRunningChainId>({chain_id: FixedBytes.decode<4>({len: 4})}, {order: ['chain_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.chain_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.chain_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__Test_chain_status_Forking_protocol generated for TestChainStatusForkingProtocol
export class CGRIDClass__Test_chain_status_Forking_protocol extends Box<TestChainStatusForkingProtocol> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Test_chain_status_Forking_protocol {
        return new this(record_decoder<TestChainStatusForkingProtocol>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__TestChainStatus__Running generated for TestChainStatus__Running
export class CGRIDClass__TestChainStatus__Running extends Box<TestChainStatus__Running> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['status', 'chain_id', 'genesis', 'protocol', 'expiration']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__TestChainStatus__Running {
        return new this(record_decoder<TestChainStatus__Running>({status: Unit.decode, chain_id: CGRIDClass__Test_chain_status_Running_chain_id.decode, genesis: CGRIDClass__Test_chain_status_Running_genesis.decode, protocol: CGRIDClass__Test_chain_status_Running_protocol.decode, expiration: Int64.decode}, {order: ['status', 'chain_id', 'genesis', 'protocol', 'expiration']})(p));
    };
    get encodeLength(): number {
        return (this.value.status.encodeLength +  this.value.chain_id.encodeLength +  this.value.genesis.encodeLength +  this.value.protocol.encodeLength +  this.value.expiration.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.status.writeTarget(tgt) +  this.value.chain_id.writeTarget(tgt) +  this.value.genesis.writeTarget(tgt) +  this.value.protocol.writeTarget(tgt) +  this.value.expiration.writeTarget(tgt));
    }
}
// Class CGRIDClass__TestChainStatus__Not_running generated for TestChainStatus__Not_running
export class CGRIDClass__TestChainStatus__Not_running extends Box<TestChainStatus__Not_running> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['status']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__TestChainStatus__Not_running {
        return new this(record_decoder<TestChainStatus__Not_running>({status: Unit.decode}, {order: ['status']})(p));
    };
    get encodeLength(): number {
        return (this.value.status.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.status.writeTarget(tgt));
    }
}
// Class CGRIDClass__TestChainStatus__Forking generated for TestChainStatus__Forking
export class CGRIDClass__TestChainStatus__Forking extends Box<TestChainStatus__Forking> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['status', 'protocol', 'expiration']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__TestChainStatus__Forking {
        return new this(record_decoder<TestChainStatus__Forking>({status: Unit.decode, protocol: CGRIDClass__Test_chain_status_Forking_protocol.decode, expiration: Int64.decode}, {order: ['status', 'protocol', 'expiration']})(p));
    };
    get encodeLength(): number {
        return (this.value.status.encodeLength +  this.value.protocol.encodeLength +  this.value.expiration.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.status.writeTarget(tgt) +  this.value.protocol.writeTarget(tgt) +  this.value.expiration.writeTarget(tgt));
    }
}
export type TestChainStatusRunningProtocol = { protocol_hash: FixedBytes<32> };
export type TestChainStatusRunningGenesis = { block_hash: FixedBytes<32> };
export type TestChainStatusRunningChainId = { chain_id: FixedBytes<4> };
export type TestChainStatusForkingProtocol = { protocol_hash: FixedBytes<32> };
export type TestChainStatus__Running = { status: Unit, chain_id: CGRIDClass__Test_chain_status_Running_chain_id, genesis: CGRIDClass__Test_chain_status_Running_genesis, protocol: CGRIDClass__Test_chain_status_Running_protocol, expiration: Int64 };
export type TestChainStatus__Not_running = { status: Unit };
export type TestChainStatus__Forking = { status: Unit, protocol: CGRIDClass__Test_chain_status_Forking_protocol, expiration: Int64 };
export enum CGRIDTag__TestChainStatus{
    Not_running = 0,
    Forking = 1,
    Running = 2
}
export interface CGRIDMap__TestChainStatus {
    Not_running: CGRIDClass__TestChainStatus__Not_running,
    Forking: CGRIDClass__TestChainStatus__Forking,
    Running: CGRIDClass__TestChainStatus__Running
}
export type TestChainStatus = { kind: CGRIDTag__TestChainStatus.Not_running, value: CGRIDMap__TestChainStatus['Not_running'] } | { kind: CGRIDTag__TestChainStatus.Forking, value: CGRIDMap__TestChainStatus['Forking'] } | { kind: CGRIDTag__TestChainStatus.Running, value: CGRIDMap__TestChainStatus['Running'] };
export function testchainstatus_mkDecoder(): VariantDecoder<CGRIDTag__TestChainStatus,TestChainStatus> {
    function f(disc: CGRIDTag__TestChainStatus) {
        switch (disc) {
            case CGRIDTag__TestChainStatus.Not_running: return CGRIDClass__TestChainStatus__Not_running.decode;
            case CGRIDTag__TestChainStatus.Forking: return CGRIDClass__TestChainStatus__Forking.decode;
            case CGRIDTag__TestChainStatus.Running: return CGRIDClass__TestChainStatus__Running.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__TestChainStatus => Object.values(CGRIDTag__TestChainStatus).includes(tagval);
    return f;
}
export class CGRIDClass__TestChainStatus extends Box<TestChainStatus> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<TestChainStatus>, TestChainStatus>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__TestChainStatus {
        return new this(variant_decoder(width.Uint8)(testchainstatus_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const test_chain_status_encoder = (value: TestChainStatus): OutputBytes => {
    return variant_encoder<KindOf<TestChainStatus>, TestChainStatus>(width.Uint8)(value);
}
export const test_chain_status_decoder = (p: Parser): TestChainStatus => {
    return variant_decoder(width.Uint8)(testchainstatus_mkDecoder())(p);
}
