import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
export type P2pPointId = Dynamic<U8String,width.Uint30>;
export class CGRIDClass__P2pPointId extends Box<P2pPointId> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__P2pPointId {
        return new this(Dynamic.decode(U8String.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const p2p_point_id_encoder = (value: P2pPointId): OutputBytes => {
    return value.encode();
}
export const p2p_point_id_decoder = (p: Parser): P2pPointId => {
    return Dynamic.decode(U8String.decode, width.Uint30)(p);
}
