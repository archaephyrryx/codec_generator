import { Codec } from '../../ts_runtime/codec';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export enum P2pPeerState{
    accepted = 0,
    running = 1,
    disconnected = 2
}
export class CGRIDClass__P2pPeerState extends Box<P2pPeerState> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<P2pPeerState>(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPeerState {
        return new this(enum_decoder(width.Uint8)((x): x is P2pPeerState => (Object.values(P2pPeerState).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
export const p2p_peer_state_encoder = (value: P2pPeerState): OutputBytes => {
    return enum_encoder(width.Uint8)<P2pPeerState>(value);
}
export const p2p_peer_state_decoder = (p: Parser): P2pPeerState => {
    return enum_decoder(width.Uint8)((x): x is P2pPeerState => (Object.values(P2pPeerState).includes(x)))(p);
}
