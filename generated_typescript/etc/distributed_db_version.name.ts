import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
export type DistributedDbVersionName = Dynamic<U8String,width.Uint30>;
export class CGRIDClass__DistributedDbVersionName extends Box<DistributedDbVersionName> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__DistributedDbVersionName {
        return new this(Dynamic.decode(U8String.decode, width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const distributed_db_version_name_encoder = (value: DistributedDbVersionName): OutputBytes => {
    return value.encode();
}
export const distributed_db_version_name_decoder = (p: Parser): DistributedDbVersionName => {
    return Dynamic.decode(U8String.decode, width.Uint30)(p);
}
