import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Double } from '../../ts_runtime/float/double';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type TimespanSystem = Double;
export class CGRIDClass__TimespanSystem extends Box<TimespanSystem> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__TimespanSystem {
        return new this(Double.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const timespan_system_encoder = (value: TimespanSystem): OutputBytes => {
    return value.encode();
}
export const timespan_system_decoder = (p: Parser): TimespanSystem => {
    return Double.decode(p);
}
