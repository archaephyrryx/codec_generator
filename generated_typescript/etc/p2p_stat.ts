import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int31, Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type P2pStat = { total_sent: Int64, total_recv: Int64, current_inflow: Int31, current_outflow: Int31 };
export class CGRIDClass__P2pStat extends Box<P2pStat> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['total_sent', 'total_recv', 'current_inflow', 'current_outflow']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pStat {
        return new this(record_decoder<P2pStat>({total_sent: Int64.decode, total_recv: Int64.decode, current_inflow: Int31.decode, current_outflow: Int31.decode}, {order: ['total_sent', 'total_recv', 'current_inflow', 'current_outflow']})(p));
    };
    get encodeLength(): number {
        return (this.value.total_sent.encodeLength +  this.value.total_recv.encodeLength +  this.value.current_inflow.encodeLength +  this.value.current_outflow.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.total_sent.writeTarget(tgt) +  this.value.total_recv.writeTarget(tgt) +  this.value.current_inflow.writeTarget(tgt) +  this.value.current_outflow.writeTarget(tgt));
    }
}
export const p2p_stat_encoder = (value: P2pStat): OutputBytes => {
    return record_encoder({order: ['total_sent', 'total_recv', 'current_inflow', 'current_outflow']})(value);
}
export const p2p_stat_decoder = (p: Parser): P2pStat => {
    return record_decoder<P2pStat>({total_sent: Int64.decode, total_recv: Int64.decode, current_inflow: Int31.decode, current_outflow: Int31.decode}, {order: ['total_sent', 'total_recv', 'current_inflow', 'current_outflow']})(p);
}
