import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__P2p_connectionPool_event_Swap_success_source generated for P2pConnectionPoolEventSwapSuccessSource
export class CGRIDClass__P2p_connectionPool_event_Swap_success_source extends Box<P2pConnectionPoolEventSwapSuccessSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Swap_success_source {
        return new this(record_decoder<P2pConnectionPoolEventSwapSuccessSource>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Swap_request_sent_source generated for P2pConnectionPoolEventSwapRequestSentSource
export class CGRIDClass__P2p_connectionPool_event_Swap_request_sent_source extends Box<P2pConnectionPoolEventSwapRequestSentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Swap_request_sent_source {
        return new this(record_decoder<P2pConnectionPoolEventSwapRequestSentSource>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Swap_request_received_source generated for P2pConnectionPoolEventSwapRequestReceivedSource
export class CGRIDClass__P2p_connectionPool_event_Swap_request_received_source extends Box<P2pConnectionPoolEventSwapRequestReceivedSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Swap_request_received_source {
        return new this(record_decoder<P2pConnectionPoolEventSwapRequestReceivedSource>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Swap_request_ignored_source generated for P2pConnectionPoolEventSwapRequestIgnoredSource
export class CGRIDClass__P2p_connectionPool_event_Swap_request_ignored_source extends Box<P2pConnectionPoolEventSwapRequestIgnoredSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Swap_request_ignored_source {
        return new this(record_decoder<P2pConnectionPoolEventSwapRequestIgnoredSource>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Swap_failure_source generated for P2pConnectionPoolEventSwapFailureSource
export class CGRIDClass__P2p_connectionPool_event_Swap_failure_source extends Box<P2pConnectionPoolEventSwapFailureSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Swap_failure_source {
        return new this(record_decoder<P2pConnectionPoolEventSwapFailureSource>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Swap_ack_sent_source generated for P2pConnectionPoolEventSwapAckSentSource
export class CGRIDClass__P2p_connectionPool_event_Swap_ack_sent_source extends Box<P2pConnectionPoolEventSwapAckSentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Swap_ack_sent_source {
        return new this(record_decoder<P2pConnectionPoolEventSwapAckSentSource>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Swap_ack_received_source generated for P2pConnectionPoolEventSwapAckReceivedSource
export class CGRIDClass__P2p_connectionPool_event_Swap_ack_received_source extends Box<P2pConnectionPoolEventSwapAckReceivedSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Swap_ack_received_source {
        return new this(record_decoder<P2pConnectionPoolEventSwapAckReceivedSource>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Request_rejected_identity_index1 generated for P2pConnectionPoolEventRequestRejectedIdentityIndex1
export class CGRIDClass__P2p_connectionPool_event_Request_rejected_identity_index1 extends Box<P2pConnectionPoolEventRequestRejectedIdentityIndex1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Request_rejected_identity_index1 {
        return new this(record_decoder<P2pConnectionPoolEventRequestRejectedIdentityIndex1>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Request_rejected_identity generated for P2pConnectionPoolEventRequestRejectedIdentity
export class CGRIDClass__P2p_connectionPool_event_Request_rejected_identity extends Box<P2pConnectionPoolEventRequestRejectedIdentity> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Request_rejected_identity {
        return new this(tuple_decoder<P2pConnectionPoolEventRequestRejectedIdentity>(CGRIDClass__P2p_connectionId.decode, CGRIDClass__P2p_connectionPool_event_Request_rejected_identity_index1.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Rejecting_request_peer_id generated for P2pConnectionPoolEventRejectingRequestPeerId
export class CGRIDClass__P2p_connectionPool_event_Rejecting_request_peer_id extends Box<P2pConnectionPoolEventRejectingRequestPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Rejecting_request_peer_id {
        return new this(record_decoder<P2pConnectionPoolEventRejectingRequestPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_New_peer_peer_id generated for P2pConnectionPoolEventNewPeerPeerId
export class CGRIDClass__P2p_connectionPool_event_New_peer_peer_id extends Box<P2pConnectionPoolEventNewPeerPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_New_peer_peer_id {
        return new this(record_decoder<P2pConnectionPoolEventNewPeerPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_External_disconnection_peer_id generated for P2pConnectionPoolEventExternalDisconnectionPeerId
export class CGRIDClass__P2p_connectionPool_event_External_disconnection_peer_id extends Box<P2pConnectionPoolEventExternalDisconnectionPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_External_disconnection_peer_id {
        return new this(record_decoder<P2pConnectionPoolEventExternalDisconnectionPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Disconnection_peer_id generated for P2pConnectionPoolEventDisconnectionPeerId
export class CGRIDClass__P2p_connectionPool_event_Disconnection_peer_id extends Box<P2pConnectionPoolEventDisconnectionPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Disconnection_peer_id {
        return new this(record_decoder<P2pConnectionPoolEventDisconnectionPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Connection_established_peer_id generated for P2pConnectionPoolEventConnectionEstablishedPeerId
export class CGRIDClass__P2p_connectionPool_event_Connection_established_peer_id extends Box<P2pConnectionPoolEventConnectionEstablishedPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Connection_established_peer_id {
        return new this(record_decoder<P2pConnectionPoolEventConnectionEstablishedPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Bootstrap_sent_source generated for P2pConnectionPoolEventBootstrapSentSource
export class CGRIDClass__P2p_connectionPool_event_Bootstrap_sent_source extends Box<P2pConnectionPoolEventBootstrapSentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Bootstrap_sent_source {
        return new this(record_decoder<P2pConnectionPoolEventBootstrapSentSource>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Bootstrap_received_source generated for P2pConnectionPoolEventBootstrapReceivedSource
export class CGRIDClass__P2p_connectionPool_event_Bootstrap_received_source extends Box<P2pConnectionPoolEventBootstrapReceivedSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Bootstrap_received_source {
        return new this(record_decoder<P2pConnectionPoolEventBootstrapReceivedSource>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Advertise_sent_source generated for P2pConnectionPoolEventAdvertiseSentSource
export class CGRIDClass__P2p_connectionPool_event_Advertise_sent_source extends Box<P2pConnectionPoolEventAdvertiseSentSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Advertise_sent_source {
        return new this(record_decoder<P2pConnectionPoolEventAdvertiseSentSource>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Advertise_received_source generated for P2pConnectionPoolEventAdvertiseReceivedSource
export class CGRIDClass__P2p_connectionPool_event_Advertise_received_source extends Box<P2pConnectionPoolEventAdvertiseReceivedSource> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Advertise_received_source {
        return new this(record_decoder<P2pConnectionPoolEventAdvertiseReceivedSource>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionPool_event_Accepting_request_peer_id generated for P2pConnectionPoolEventAcceptingRequestPeerId
export class CGRIDClass__P2p_connectionPool_event_Accepting_request_peer_id extends Box<P2pConnectionPoolEventAcceptingRequestPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionPool_event_Accepting_request_peer_id {
        return new this(record_decoder<P2pConnectionPoolEventAcceptingRequestPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_connectionId generated for P2pConnectionId
export class CGRIDClass__P2p_connectionId extends Box<P2pConnectionId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['addr', 'port']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_connectionId {
        return new this(record_decoder<P2pConnectionId>({addr: Dynamic.decode(U8String.decode, width.Uint30), port: Option.decode(Uint16.decode)}, {order: ['addr', 'port']})(p));
    };
    get encodeLength(): number {
        return (this.value.addr.encodeLength +  this.value.port.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.addr.writeTarget(tgt) +  this.value.port.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Too_many_connections generated for P2pConnectionPoolEvent__Too_many_connections
export class CGRIDClass__P2pConnectionPoolEvent__Too_many_connections extends Box<P2pConnectionPoolEvent__Too_many_connections> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Too_many_connections {
        return new this(record_decoder<P2pConnectionPoolEvent__Too_many_connections>({event: Unit.decode, rhs: Unit.decode}, {order: ['event', 'rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.rhs.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Too_few_connections generated for P2pConnectionPoolEvent__Too_few_connections
export class CGRIDClass__P2pConnectionPoolEvent__Too_few_connections extends Box<P2pConnectionPoolEvent__Too_few_connections> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Too_few_connections {
        return new this(record_decoder<P2pConnectionPoolEvent__Too_few_connections>({event: Unit.decode, rhs: Unit.decode}, {order: ['event', 'rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.rhs.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Swap_success generated for P2pConnectionPoolEvent__Swap_success
export class CGRIDClass__P2pConnectionPoolEvent__Swap_success extends Box<P2pConnectionPoolEvent__Swap_success> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'source']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Swap_success {
        return new this(record_decoder<P2pConnectionPoolEvent__Swap_success>({event: Unit.decode, source: CGRIDClass__P2p_connectionPool_event_Swap_success_source.decode}, {order: ['event', 'source']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.source.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.source.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Swap_request_sent generated for P2pConnectionPoolEvent__Swap_request_sent
export class CGRIDClass__P2pConnectionPoolEvent__Swap_request_sent extends Box<P2pConnectionPoolEvent__Swap_request_sent> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'source']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Swap_request_sent {
        return new this(record_decoder<P2pConnectionPoolEvent__Swap_request_sent>({event: Unit.decode, source: CGRIDClass__P2p_connectionPool_event_Swap_request_sent_source.decode}, {order: ['event', 'source']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.source.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.source.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Swap_request_received generated for P2pConnectionPoolEvent__Swap_request_received
export class CGRIDClass__P2pConnectionPoolEvent__Swap_request_received extends Box<P2pConnectionPoolEvent__Swap_request_received> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'source']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Swap_request_received {
        return new this(record_decoder<P2pConnectionPoolEvent__Swap_request_received>({event: Unit.decode, source: CGRIDClass__P2p_connectionPool_event_Swap_request_received_source.decode}, {order: ['event', 'source']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.source.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.source.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Swap_request_ignored generated for P2pConnectionPoolEvent__Swap_request_ignored
export class CGRIDClass__P2pConnectionPoolEvent__Swap_request_ignored extends Box<P2pConnectionPoolEvent__Swap_request_ignored> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'source']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Swap_request_ignored {
        return new this(record_decoder<P2pConnectionPoolEvent__Swap_request_ignored>({event: Unit.decode, source: CGRIDClass__P2p_connectionPool_event_Swap_request_ignored_source.decode}, {order: ['event', 'source']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.source.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.source.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Swap_failure generated for P2pConnectionPoolEvent__Swap_failure
export class CGRIDClass__P2pConnectionPoolEvent__Swap_failure extends Box<P2pConnectionPoolEvent__Swap_failure> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'source']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Swap_failure {
        return new this(record_decoder<P2pConnectionPoolEvent__Swap_failure>({event: Unit.decode, source: CGRIDClass__P2p_connectionPool_event_Swap_failure_source.decode}, {order: ['event', 'source']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.source.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.source.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Swap_ack_sent generated for P2pConnectionPoolEvent__Swap_ack_sent
export class CGRIDClass__P2pConnectionPoolEvent__Swap_ack_sent extends Box<P2pConnectionPoolEvent__Swap_ack_sent> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'source']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Swap_ack_sent {
        return new this(record_decoder<P2pConnectionPoolEvent__Swap_ack_sent>({event: Unit.decode, source: CGRIDClass__P2p_connectionPool_event_Swap_ack_sent_source.decode}, {order: ['event', 'source']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.source.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.source.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Swap_ack_received generated for P2pConnectionPoolEvent__Swap_ack_received
export class CGRIDClass__P2pConnectionPoolEvent__Swap_ack_received extends Box<P2pConnectionPoolEvent__Swap_ack_received> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'source']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Swap_ack_received {
        return new this(record_decoder<P2pConnectionPoolEvent__Swap_ack_received>({event: Unit.decode, source: CGRIDClass__P2p_connectionPool_event_Swap_ack_received_source.decode}, {order: ['event', 'source']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.source.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.source.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Request_rejected generated for P2pConnectionPoolEvent__Request_rejected
export class CGRIDClass__P2pConnectionPoolEvent__Request_rejected extends Box<P2pConnectionPoolEvent__Request_rejected> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'point', 'identity']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Request_rejected {
        return new this(record_decoder<P2pConnectionPoolEvent__Request_rejected>({event: Unit.decode, point: Dynamic.decode(U8String.decode, width.Uint30), identity: Option.decode(CGRIDClass__P2p_connectionPool_event_Request_rejected_identity.decode)}, {order: ['event', 'point', 'identity']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.point.encodeLength +  this.value.identity.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.point.writeTarget(tgt) +  this.value.identity.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Rejecting_request generated for P2pConnectionPoolEvent__Rejecting_request
export class CGRIDClass__P2pConnectionPoolEvent__Rejecting_request extends Box<P2pConnectionPoolEvent__Rejecting_request> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'point', 'id_point', 'peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Rejecting_request {
        return new this(record_decoder<P2pConnectionPoolEvent__Rejecting_request>({event: Unit.decode, point: Dynamic.decode(U8String.decode, width.Uint30), id_point: CGRIDClass__P2p_connectionId.decode, peer_id: CGRIDClass__P2p_connectionPool_event_Rejecting_request_peer_id.decode}, {order: ['event', 'point', 'id_point', 'peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.point.encodeLength +  this.value.id_point.encodeLength +  this.value.peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.point.writeTarget(tgt) +  this.value.id_point.writeTarget(tgt) +  this.value.peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Outgoing_connection generated for P2pConnectionPoolEvent__Outgoing_connection
export class CGRIDClass__P2pConnectionPoolEvent__Outgoing_connection extends Box<P2pConnectionPoolEvent__Outgoing_connection> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'point']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Outgoing_connection {
        return new this(record_decoder<P2pConnectionPoolEvent__Outgoing_connection>({event: Unit.decode, point: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['event', 'point']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.point.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.point.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__New_point generated for P2pConnectionPoolEvent__New_point
export class CGRIDClass__P2pConnectionPoolEvent__New_point extends Box<P2pConnectionPoolEvent__New_point> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'point']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__New_point {
        return new this(record_decoder<P2pConnectionPoolEvent__New_point>({event: Unit.decode, point: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['event', 'point']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.point.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.point.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__New_peer generated for P2pConnectionPoolEvent__New_peer
export class CGRIDClass__P2pConnectionPoolEvent__New_peer extends Box<P2pConnectionPoolEvent__New_peer> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__New_peer {
        return new this(record_decoder<P2pConnectionPoolEvent__New_peer>({event: Unit.decode, peer_id: CGRIDClass__P2p_connectionPool_event_New_peer_peer_id.decode}, {order: ['event', 'peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Incoming_connection generated for P2pConnectionPoolEvent__Incoming_connection
export class CGRIDClass__P2pConnectionPoolEvent__Incoming_connection extends Box<P2pConnectionPoolEvent__Incoming_connection> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'point']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Incoming_connection {
        return new this(record_decoder<P2pConnectionPoolEvent__Incoming_connection>({event: Unit.decode, point: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['event', 'point']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.point.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.point.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Gc_points generated for P2pConnectionPoolEvent__Gc_points
export class CGRIDClass__P2pConnectionPoolEvent__Gc_points extends Box<P2pConnectionPoolEvent__Gc_points> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Gc_points {
        return new this(record_decoder<P2pConnectionPoolEvent__Gc_points>({event: Unit.decode, rhs: Unit.decode}, {order: ['event', 'rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.rhs.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Gc_peer_ids generated for P2pConnectionPoolEvent__Gc_peer_ids
export class CGRIDClass__P2pConnectionPoolEvent__Gc_peer_ids extends Box<P2pConnectionPoolEvent__Gc_peer_ids> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Gc_peer_ids {
        return new this(record_decoder<P2pConnectionPoolEvent__Gc_peer_ids>({event: Unit.decode, rhs: Unit.decode}, {order: ['event', 'rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.rhs.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__External_disconnection generated for P2pConnectionPoolEvent__External_disconnection
export class CGRIDClass__P2pConnectionPoolEvent__External_disconnection extends Box<P2pConnectionPoolEvent__External_disconnection> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__External_disconnection {
        return new this(record_decoder<P2pConnectionPoolEvent__External_disconnection>({event: Unit.decode, peer_id: CGRIDClass__P2p_connectionPool_event_External_disconnection_peer_id.decode}, {order: ['event', 'peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Disconnection generated for P2pConnectionPoolEvent__Disconnection
export class CGRIDClass__P2pConnectionPoolEvent__Disconnection extends Box<P2pConnectionPoolEvent__Disconnection> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Disconnection {
        return new this(record_decoder<P2pConnectionPoolEvent__Disconnection>({event: Unit.decode, peer_id: CGRIDClass__P2p_connectionPool_event_Disconnection_peer_id.decode}, {order: ['event', 'peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Connection_established generated for P2pConnectionPoolEvent__Connection_established
export class CGRIDClass__P2pConnectionPoolEvent__Connection_established extends Box<P2pConnectionPoolEvent__Connection_established> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'id_point', 'peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Connection_established {
        return new this(record_decoder<P2pConnectionPoolEvent__Connection_established>({event: Unit.decode, id_point: CGRIDClass__P2p_connectionId.decode, peer_id: CGRIDClass__P2p_connectionPool_event_Connection_established_peer_id.decode}, {order: ['event', 'id_point', 'peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.id_point.encodeLength +  this.value.peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.id_point.writeTarget(tgt) +  this.value.peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Bootstrap_sent generated for P2pConnectionPoolEvent__Bootstrap_sent
export class CGRIDClass__P2pConnectionPoolEvent__Bootstrap_sent extends Box<P2pConnectionPoolEvent__Bootstrap_sent> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'source']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Bootstrap_sent {
        return new this(record_decoder<P2pConnectionPoolEvent__Bootstrap_sent>({event: Unit.decode, source: CGRIDClass__P2p_connectionPool_event_Bootstrap_sent_source.decode}, {order: ['event', 'source']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.source.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.source.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Bootstrap_received generated for P2pConnectionPoolEvent__Bootstrap_received
export class CGRIDClass__P2pConnectionPoolEvent__Bootstrap_received extends Box<P2pConnectionPoolEvent__Bootstrap_received> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'source']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Bootstrap_received {
        return new this(record_decoder<P2pConnectionPoolEvent__Bootstrap_received>({event: Unit.decode, source: CGRIDClass__P2p_connectionPool_event_Bootstrap_received_source.decode}, {order: ['event', 'source']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.source.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.source.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Authentication_failed generated for P2pConnectionPoolEvent__Authentication_failed
export class CGRIDClass__P2pConnectionPoolEvent__Authentication_failed extends Box<P2pConnectionPoolEvent__Authentication_failed> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'point']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Authentication_failed {
        return new this(record_decoder<P2pConnectionPoolEvent__Authentication_failed>({event: Unit.decode, point: Dynamic.decode(U8String.decode, width.Uint30)}, {order: ['event', 'point']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.point.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.point.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Advertise_sent generated for P2pConnectionPoolEvent__Advertise_sent
export class CGRIDClass__P2pConnectionPoolEvent__Advertise_sent extends Box<P2pConnectionPoolEvent__Advertise_sent> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'source']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Advertise_sent {
        return new this(record_decoder<P2pConnectionPoolEvent__Advertise_sent>({event: Unit.decode, source: CGRIDClass__P2p_connectionPool_event_Advertise_sent_source.decode}, {order: ['event', 'source']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.source.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.source.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Advertise_received generated for P2pConnectionPoolEvent__Advertise_received
export class CGRIDClass__P2pConnectionPoolEvent__Advertise_received extends Box<P2pConnectionPoolEvent__Advertise_received> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'source']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Advertise_received {
        return new this(record_decoder<P2pConnectionPoolEvent__Advertise_received>({event: Unit.decode, source: CGRIDClass__P2p_connectionPool_event_Advertise_received_source.decode}, {order: ['event', 'source']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.source.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.source.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pConnectionPoolEvent__Accepting_request generated for P2pConnectionPoolEvent__Accepting_request
export class CGRIDClass__P2pConnectionPoolEvent__Accepting_request extends Box<P2pConnectionPoolEvent__Accepting_request> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event', 'point', 'id_point', 'peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent__Accepting_request {
        return new this(record_decoder<P2pConnectionPoolEvent__Accepting_request>({event: Unit.decode, point: Dynamic.decode(U8String.decode, width.Uint30), id_point: CGRIDClass__P2p_connectionId.decode, peer_id: CGRIDClass__P2p_connectionPool_event_Accepting_request_peer_id.decode}, {order: ['event', 'point', 'id_point', 'peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event.encodeLength +  this.value.point.encodeLength +  this.value.id_point.encodeLength +  this.value.peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event.writeTarget(tgt) +  this.value.point.writeTarget(tgt) +  this.value.id_point.writeTarget(tgt) +  this.value.peer_id.writeTarget(tgt));
    }
}
export type P2pConnectionPoolEventSwapSuccessSource = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventSwapRequestSentSource = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventSwapRequestReceivedSource = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventSwapRequestIgnoredSource = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventSwapFailureSource = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventSwapAckSentSource = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventSwapAckReceivedSource = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventRequestRejectedIdentityIndex1 = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventRequestRejectedIdentity = [CGRIDClass__P2p_connectionId, CGRIDClass__P2p_connectionPool_event_Request_rejected_identity_index1];
export type P2pConnectionPoolEventRejectingRequestPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventNewPeerPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventExternalDisconnectionPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventDisconnectionPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventConnectionEstablishedPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventBootstrapSentSource = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventBootstrapReceivedSource = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventAdvertiseSentSource = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventAdvertiseReceivedSource = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionPoolEventAcceptingRequestPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pConnectionId = { addr: Dynamic<U8String,width.Uint30>, port: Option<Uint16> };
export type P2pConnectionPoolEvent__Too_many_connections = { event: Unit, rhs: Unit };
export type P2pConnectionPoolEvent__Too_few_connections = { event: Unit, rhs: Unit };
export type P2pConnectionPoolEvent__Swap_success = { event: Unit, source: CGRIDClass__P2p_connectionPool_event_Swap_success_source };
export type P2pConnectionPoolEvent__Swap_request_sent = { event: Unit, source: CGRIDClass__P2p_connectionPool_event_Swap_request_sent_source };
export type P2pConnectionPoolEvent__Swap_request_received = { event: Unit, source: CGRIDClass__P2p_connectionPool_event_Swap_request_received_source };
export type P2pConnectionPoolEvent__Swap_request_ignored = { event: Unit, source: CGRIDClass__P2p_connectionPool_event_Swap_request_ignored_source };
export type P2pConnectionPoolEvent__Swap_failure = { event: Unit, source: CGRIDClass__P2p_connectionPool_event_Swap_failure_source };
export type P2pConnectionPoolEvent__Swap_ack_sent = { event: Unit, source: CGRIDClass__P2p_connectionPool_event_Swap_ack_sent_source };
export type P2pConnectionPoolEvent__Swap_ack_received = { event: Unit, source: CGRIDClass__P2p_connectionPool_event_Swap_ack_received_source };
export type P2pConnectionPoolEvent__Request_rejected = { event: Unit, point: Dynamic<U8String,width.Uint30>, identity: Option<CGRIDClass__P2p_connectionPool_event_Request_rejected_identity> };
export type P2pConnectionPoolEvent__Rejecting_request = { event: Unit, point: Dynamic<U8String,width.Uint30>, id_point: CGRIDClass__P2p_connectionId, peer_id: CGRIDClass__P2p_connectionPool_event_Rejecting_request_peer_id };
export type P2pConnectionPoolEvent__Outgoing_connection = { event: Unit, point: Dynamic<U8String,width.Uint30> };
export type P2pConnectionPoolEvent__New_point = { event: Unit, point: Dynamic<U8String,width.Uint30> };
export type P2pConnectionPoolEvent__New_peer = { event: Unit, peer_id: CGRIDClass__P2p_connectionPool_event_New_peer_peer_id };
export type P2pConnectionPoolEvent__Incoming_connection = { event: Unit, point: Dynamic<U8String,width.Uint30> };
export type P2pConnectionPoolEvent__Gc_points = { event: Unit, rhs: Unit };
export type P2pConnectionPoolEvent__Gc_peer_ids = { event: Unit, rhs: Unit };
export type P2pConnectionPoolEvent__External_disconnection = { event: Unit, peer_id: CGRIDClass__P2p_connectionPool_event_External_disconnection_peer_id };
export type P2pConnectionPoolEvent__Disconnection = { event: Unit, peer_id: CGRIDClass__P2p_connectionPool_event_Disconnection_peer_id };
export type P2pConnectionPoolEvent__Connection_established = { event: Unit, id_point: CGRIDClass__P2p_connectionId, peer_id: CGRIDClass__P2p_connectionPool_event_Connection_established_peer_id };
export type P2pConnectionPoolEvent__Bootstrap_sent = { event: Unit, source: CGRIDClass__P2p_connectionPool_event_Bootstrap_sent_source };
export type P2pConnectionPoolEvent__Bootstrap_received = { event: Unit, source: CGRIDClass__P2p_connectionPool_event_Bootstrap_received_source };
export type P2pConnectionPoolEvent__Authentication_failed = { event: Unit, point: Dynamic<U8String,width.Uint30> };
export type P2pConnectionPoolEvent__Advertise_sent = { event: Unit, source: CGRIDClass__P2p_connectionPool_event_Advertise_sent_source };
export type P2pConnectionPoolEvent__Advertise_received = { event: Unit, source: CGRIDClass__P2p_connectionPool_event_Advertise_received_source };
export type P2pConnectionPoolEvent__Accepting_request = { event: Unit, point: Dynamic<U8String,width.Uint30>, id_point: CGRIDClass__P2p_connectionId, peer_id: CGRIDClass__P2p_connectionPool_event_Accepting_request_peer_id };
export enum CGRIDTag__P2pConnectionPoolEvent{
    Too_few_connections = 0,
    Too_many_connections = 1,
    New_point = 2,
    New_peer = 3,
    Incoming_connection = 4,
    Outgoing_connection = 5,
    Authentication_failed = 6,
    Accepting_request = 7,
    Rejecting_request = 8,
    Request_rejected = 9,
    Connection_established = 10,
    Disconnection = 11,
    External_disconnection = 12,
    Gc_points = 13,
    Gc_peer_ids = 14,
    Swap_request_received = 15,
    Swap_ack_received = 16,
    Swap_request_sent = 17,
    Swap_ack_sent = 18,
    Swap_request_ignored = 19,
    Swap_success = 20,
    Swap_failure = 21,
    Bootstrap_sent = 22,
    Bootstrap_received = 23,
    Advertise_sent = 24,
    Advertise_received = 25
}
export interface CGRIDMap__P2pConnectionPoolEvent {
    Too_few_connections: CGRIDClass__P2pConnectionPoolEvent__Too_few_connections,
    Too_many_connections: CGRIDClass__P2pConnectionPoolEvent__Too_many_connections,
    New_point: CGRIDClass__P2pConnectionPoolEvent__New_point,
    New_peer: CGRIDClass__P2pConnectionPoolEvent__New_peer,
    Incoming_connection: CGRIDClass__P2pConnectionPoolEvent__Incoming_connection,
    Outgoing_connection: CGRIDClass__P2pConnectionPoolEvent__Outgoing_connection,
    Authentication_failed: CGRIDClass__P2pConnectionPoolEvent__Authentication_failed,
    Accepting_request: CGRIDClass__P2pConnectionPoolEvent__Accepting_request,
    Rejecting_request: CGRIDClass__P2pConnectionPoolEvent__Rejecting_request,
    Request_rejected: CGRIDClass__P2pConnectionPoolEvent__Request_rejected,
    Connection_established: CGRIDClass__P2pConnectionPoolEvent__Connection_established,
    Disconnection: CGRIDClass__P2pConnectionPoolEvent__Disconnection,
    External_disconnection: CGRIDClass__P2pConnectionPoolEvent__External_disconnection,
    Gc_points: CGRIDClass__P2pConnectionPoolEvent__Gc_points,
    Gc_peer_ids: CGRIDClass__P2pConnectionPoolEvent__Gc_peer_ids,
    Swap_request_received: CGRIDClass__P2pConnectionPoolEvent__Swap_request_received,
    Swap_ack_received: CGRIDClass__P2pConnectionPoolEvent__Swap_ack_received,
    Swap_request_sent: CGRIDClass__P2pConnectionPoolEvent__Swap_request_sent,
    Swap_ack_sent: CGRIDClass__P2pConnectionPoolEvent__Swap_ack_sent,
    Swap_request_ignored: CGRIDClass__P2pConnectionPoolEvent__Swap_request_ignored,
    Swap_success: CGRIDClass__P2pConnectionPoolEvent__Swap_success,
    Swap_failure: CGRIDClass__P2pConnectionPoolEvent__Swap_failure,
    Bootstrap_sent: CGRIDClass__P2pConnectionPoolEvent__Bootstrap_sent,
    Bootstrap_received: CGRIDClass__P2pConnectionPoolEvent__Bootstrap_received,
    Advertise_sent: CGRIDClass__P2pConnectionPoolEvent__Advertise_sent,
    Advertise_received: CGRIDClass__P2pConnectionPoolEvent__Advertise_received
}
export type P2pConnectionPoolEvent = { kind: CGRIDTag__P2pConnectionPoolEvent.Too_few_connections, value: CGRIDMap__P2pConnectionPoolEvent['Too_few_connections'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Too_many_connections, value: CGRIDMap__P2pConnectionPoolEvent['Too_many_connections'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.New_point, value: CGRIDMap__P2pConnectionPoolEvent['New_point'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.New_peer, value: CGRIDMap__P2pConnectionPoolEvent['New_peer'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Incoming_connection, value: CGRIDMap__P2pConnectionPoolEvent['Incoming_connection'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Outgoing_connection, value: CGRIDMap__P2pConnectionPoolEvent['Outgoing_connection'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Authentication_failed, value: CGRIDMap__P2pConnectionPoolEvent['Authentication_failed'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Accepting_request, value: CGRIDMap__P2pConnectionPoolEvent['Accepting_request'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Rejecting_request, value: CGRIDMap__P2pConnectionPoolEvent['Rejecting_request'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Request_rejected, value: CGRIDMap__P2pConnectionPoolEvent['Request_rejected'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Connection_established, value: CGRIDMap__P2pConnectionPoolEvent['Connection_established'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Disconnection, value: CGRIDMap__P2pConnectionPoolEvent['Disconnection'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.External_disconnection, value: CGRIDMap__P2pConnectionPoolEvent['External_disconnection'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Gc_points, value: CGRIDMap__P2pConnectionPoolEvent['Gc_points'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Gc_peer_ids, value: CGRIDMap__P2pConnectionPoolEvent['Gc_peer_ids'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Swap_request_received, value: CGRIDMap__P2pConnectionPoolEvent['Swap_request_received'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Swap_ack_received, value: CGRIDMap__P2pConnectionPoolEvent['Swap_ack_received'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Swap_request_sent, value: CGRIDMap__P2pConnectionPoolEvent['Swap_request_sent'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Swap_ack_sent, value: CGRIDMap__P2pConnectionPoolEvent['Swap_ack_sent'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Swap_request_ignored, value: CGRIDMap__P2pConnectionPoolEvent['Swap_request_ignored'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Swap_success, value: CGRIDMap__P2pConnectionPoolEvent['Swap_success'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Swap_failure, value: CGRIDMap__P2pConnectionPoolEvent['Swap_failure'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Bootstrap_sent, value: CGRIDMap__P2pConnectionPoolEvent['Bootstrap_sent'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Bootstrap_received, value: CGRIDMap__P2pConnectionPoolEvent['Bootstrap_received'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Advertise_sent, value: CGRIDMap__P2pConnectionPoolEvent['Advertise_sent'] } | { kind: CGRIDTag__P2pConnectionPoolEvent.Advertise_received, value: CGRIDMap__P2pConnectionPoolEvent['Advertise_received'] };
export function p2pconnectionpoolevent_mkDecoder(): VariantDecoder<CGRIDTag__P2pConnectionPoolEvent,P2pConnectionPoolEvent> {
    function f(disc: CGRIDTag__P2pConnectionPoolEvent) {
        switch (disc) {
            case CGRIDTag__P2pConnectionPoolEvent.Too_few_connections: return CGRIDClass__P2pConnectionPoolEvent__Too_few_connections.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Too_many_connections: return CGRIDClass__P2pConnectionPoolEvent__Too_many_connections.decode;
            case CGRIDTag__P2pConnectionPoolEvent.New_point: return CGRIDClass__P2pConnectionPoolEvent__New_point.decode;
            case CGRIDTag__P2pConnectionPoolEvent.New_peer: return CGRIDClass__P2pConnectionPoolEvent__New_peer.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Incoming_connection: return CGRIDClass__P2pConnectionPoolEvent__Incoming_connection.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Outgoing_connection: return CGRIDClass__P2pConnectionPoolEvent__Outgoing_connection.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Authentication_failed: return CGRIDClass__P2pConnectionPoolEvent__Authentication_failed.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Accepting_request: return CGRIDClass__P2pConnectionPoolEvent__Accepting_request.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Rejecting_request: return CGRIDClass__P2pConnectionPoolEvent__Rejecting_request.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Request_rejected: return CGRIDClass__P2pConnectionPoolEvent__Request_rejected.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Connection_established: return CGRIDClass__P2pConnectionPoolEvent__Connection_established.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Disconnection: return CGRIDClass__P2pConnectionPoolEvent__Disconnection.decode;
            case CGRIDTag__P2pConnectionPoolEvent.External_disconnection: return CGRIDClass__P2pConnectionPoolEvent__External_disconnection.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Gc_points: return CGRIDClass__P2pConnectionPoolEvent__Gc_points.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Gc_peer_ids: return CGRIDClass__P2pConnectionPoolEvent__Gc_peer_ids.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Swap_request_received: return CGRIDClass__P2pConnectionPoolEvent__Swap_request_received.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Swap_ack_received: return CGRIDClass__P2pConnectionPoolEvent__Swap_ack_received.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Swap_request_sent: return CGRIDClass__P2pConnectionPoolEvent__Swap_request_sent.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Swap_ack_sent: return CGRIDClass__P2pConnectionPoolEvent__Swap_ack_sent.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Swap_request_ignored: return CGRIDClass__P2pConnectionPoolEvent__Swap_request_ignored.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Swap_success: return CGRIDClass__P2pConnectionPoolEvent__Swap_success.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Swap_failure: return CGRIDClass__P2pConnectionPoolEvent__Swap_failure.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Bootstrap_sent: return CGRIDClass__P2pConnectionPoolEvent__Bootstrap_sent.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Bootstrap_received: return CGRIDClass__P2pConnectionPoolEvent__Bootstrap_received.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Advertise_sent: return CGRIDClass__P2pConnectionPoolEvent__Advertise_sent.decode;
            case CGRIDTag__P2pConnectionPoolEvent.Advertise_received: return CGRIDClass__P2pConnectionPoolEvent__Advertise_received.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__P2pConnectionPoolEvent => Object.values(CGRIDTag__P2pConnectionPoolEvent).includes(tagval);
    return f;
}
export class CGRIDClass__P2pConnectionPoolEvent extends Box<P2pConnectionPoolEvent> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<P2pConnectionPoolEvent>, P2pConnectionPoolEvent>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionPoolEvent {
        return new this(variant_decoder(width.Uint8)(p2pconnectionpoolevent_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const p2p_connection_pool_event_encoder = (value: P2pConnectionPoolEvent): OutputBytes => {
    return variant_encoder<KindOf<P2pConnectionPoolEvent>, P2pConnectionPoolEvent>(width.Uint8)(value);
}
export const p2p_connection_pool_event_decoder = (p: Parser): P2pConnectionPoolEvent => {
    return variant_decoder(width.Uint8)(p2pconnectionpoolevent_mkDecoder())(p);
}
