import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { tuple_decoder, tuple_encoder } from '../../ts_runtime/constructed/tuple';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Bool } from '../../ts_runtime/primitive/bool';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Unit } from '../../ts_runtime/primitive/unit';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__P2pPointState__Running generated for P2pPointState__Running
export class CGRIDClass__P2pPointState__Running extends Box<P2pPointState__Running> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'p2p_peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointState__Running {
        return new this(record_decoder<P2pPointState__Running>({event_kind: Unit.decode, p2p_peer_id: CGRIDClass__P2p_pointState_Running_p2p_peer_id.decode}, {order: ['event_kind', 'p2p_peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.p2p_peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.p2p_peer_id.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointState__Requested generated for P2pPointState__Requested
export class CGRIDClass__P2pPointState__Requested extends Box<P2pPointState__Requested> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointState__Requested {
        return new this(record_decoder<P2pPointState__Requested>({event_kind: Unit.decode, rhs: Unit.decode}, {order: ['event_kind', 'rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.rhs.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointState__Disconnected generated for P2pPointState__Disconnected
export class CGRIDClass__P2pPointState__Disconnected extends Box<P2pPointState__Disconnected> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'rhs']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointState__Disconnected {
        return new this(record_decoder<P2pPointState__Disconnected>({event_kind: Unit.decode, rhs: Unit.decode}, {order: ['event_kind', 'rhs']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.rhs.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.rhs.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2pPointState__Accepted generated for P2pPointState__Accepted
export class CGRIDClass__P2pPointState__Accepted extends Box<P2pPointState__Accepted> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['event_kind', 'p2p_peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointState__Accepted {
        return new this(record_decoder<P2pPointState__Accepted>({event_kind: Unit.decode, p2p_peer_id: CGRIDClass__P2p_pointState_Accepted_p2p_peer_id.decode}, {order: ['event_kind', 'p2p_peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.event_kind.encodeLength +  this.value.p2p_peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.event_kind.writeTarget(tgt) +  this.value.p2p_peer_id.writeTarget(tgt));
    }
}
export type P2pPointState__Running = { event_kind: Unit, p2p_peer_id: CGRIDClass__P2p_pointState_Running_p2p_peer_id };
export type P2pPointState__Requested = { event_kind: Unit, rhs: Unit };
export type P2pPointState__Disconnected = { event_kind: Unit, rhs: Unit };
export type P2pPointState__Accepted = { event_kind: Unit, p2p_peer_id: CGRIDClass__P2p_pointState_Accepted_p2p_peer_id };
// Class CGRIDClass__P2p_pointState_Running_p2p_peer_id generated for P2pPointStateRunningP2pPeerId
export class CGRIDClass__P2p_pointState_Running_p2p_peer_id extends Box<P2pPointStateRunningP2pPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointState_Running_p2p_peer_id {
        return new this(record_decoder<P2pPointStateRunningP2pPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointState_Accepted_p2p_peer_id generated for P2pPointStateAcceptedP2pPeerId
export class CGRIDClass__P2p_pointState_Accepted_p2p_peer_id extends Box<P2pPointStateAcceptedP2pPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointState_Accepted_p2p_peer_id {
        return new this(record_decoder<P2pPointStateAcceptedP2pPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointState generated for P2pPointState
export function p2ppointstate_mkDecoder(): VariantDecoder<CGRIDTag__P2pPointState,P2pPointState> {
    function f(disc: CGRIDTag__P2pPointState) {
        switch (disc) {
            case CGRIDTag__P2pPointState.Requested: return CGRIDClass__P2pPointState__Requested.decode;
            case CGRIDTag__P2pPointState.Accepted: return CGRIDClass__P2pPointState__Accepted.decode;
            case CGRIDTag__P2pPointState.Running: return CGRIDClass__P2pPointState__Running.decode;
            case CGRIDTag__P2pPointState.Disconnected: return CGRIDClass__P2pPointState__Disconnected.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__P2pPointState => Object.values(CGRIDTag__P2pPointState).includes(tagval);
    return f;
}
export class CGRIDClass__P2p_pointState extends Box<P2pPointState> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<P2pPointState>, P2pPointState>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointState {
        return new this(variant_decoder(width.Uint8)(p2ppointstate_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointInfo_p2p_peer_id generated for P2pPointInfoP2pPeerId
export class CGRIDClass__P2p_pointInfo_p2p_peer_id extends Box<P2pPointInfoP2pPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointInfo_p2p_peer_id {
        return new this(record_decoder<P2pPointInfoP2pPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointInfo_last_seen_index0 generated for P2pPointInfoLastSeenIndex0
export class CGRIDClass__P2p_pointInfo_last_seen_index0 extends Box<P2pPointInfoLastSeenIndex0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointInfo_last_seen_index0 {
        return new this(record_decoder<P2pPointInfoLastSeenIndex0>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointInfo_last_seen generated for P2pPointInfoLastSeen
export class CGRIDClass__P2p_pointInfo_last_seen extends Box<P2pPointInfoLastSeen> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointInfo_last_seen {
        return new this(tuple_decoder<P2pPointInfoLastSeen>(CGRIDClass__P2p_pointInfo_last_seen_index0.decode, Int64.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointInfo_last_rejected_connection_index0 generated for P2pPointInfoLastRejectedConnectionIndex0
export class CGRIDClass__P2p_pointInfo_last_rejected_connection_index0 extends Box<P2pPointInfoLastRejectedConnectionIndex0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointInfo_last_rejected_connection_index0 {
        return new this(record_decoder<P2pPointInfoLastRejectedConnectionIndex0>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointInfo_last_rejected_connection generated for P2pPointInfoLastRejectedConnection
export class CGRIDClass__P2p_pointInfo_last_rejected_connection extends Box<P2pPointInfoLastRejectedConnection> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointInfo_last_rejected_connection {
        return new this(tuple_decoder<P2pPointInfoLastRejectedConnection>(CGRIDClass__P2p_pointInfo_last_rejected_connection_index0.decode, Int64.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointInfo_last_established_connection_index0 generated for P2pPointInfoLastEstablishedConnectionIndex0
export class CGRIDClass__P2p_pointInfo_last_established_connection_index0 extends Box<P2pPointInfoLastEstablishedConnectionIndex0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointInfo_last_established_connection_index0 {
        return new this(record_decoder<P2pPointInfoLastEstablishedConnectionIndex0>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointInfo_last_established_connection generated for P2pPointInfoLastEstablishedConnection
export class CGRIDClass__P2p_pointInfo_last_established_connection extends Box<P2pPointInfoLastEstablishedConnection> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointInfo_last_established_connection {
        return new this(tuple_decoder<P2pPointInfoLastEstablishedConnection>(CGRIDClass__P2p_pointInfo_last_established_connection_index0.decode, Int64.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointInfo_last_disconnection_index0 generated for P2pPointInfoLastDisconnectionIndex0
export class CGRIDClass__P2p_pointInfo_last_disconnection_index0 extends Box<P2pPointInfoLastDisconnectionIndex0> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointInfo_last_disconnection_index0 {
        return new this(record_decoder<P2pPointInfoLastDisconnectionIndex0>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointInfo_last_disconnection generated for P2pPointInfoLastDisconnection
export class CGRIDClass__P2p_pointInfo_last_disconnection extends Box<P2pPointInfoLastDisconnection> implements Codec {
    encode(): OutputBytes {
        return tuple_encoder(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointInfo_last_disconnection {
        return new this(tuple_decoder<P2pPointInfoLastDisconnection>(CGRIDClass__P2p_pointInfo_last_disconnection_index0.decode, Int64.decode)(p));
    };
    get encodeLength(): number {
        return (this.value[0].encodeLength +  this.value[1].encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value[0].writeTarget(tgt) +  this.value[1].writeTarget(tgt));
    }
}
// Class CGRIDClass__P2p_pointInfo_expected_peer_id generated for P2pPointInfoExpectedPeerId
export class CGRIDClass__P2p_pointInfo_expected_peer_id extends Box<P2pPointInfoExpectedPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_pointInfo_expected_peer_id {
        return new this(record_decoder<P2pPointInfoExpectedPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
export type P2pPointStateRunningP2pPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointStateAcceptedP2pPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export enum CGRIDTag__P2pPointState{
    Requested = 0,
    Accepted = 1,
    Running = 2,
    Disconnected = 3
}
export interface CGRIDMap__P2pPointState {
    Requested: CGRIDClass__P2pPointState__Requested,
    Accepted: CGRIDClass__P2pPointState__Accepted,
    Running: CGRIDClass__P2pPointState__Running,
    Disconnected: CGRIDClass__P2pPointState__Disconnected
}
export type P2pPointState = { kind: CGRIDTag__P2pPointState.Requested, value: CGRIDMap__P2pPointState['Requested'] } | { kind: CGRIDTag__P2pPointState.Accepted, value: CGRIDMap__P2pPointState['Accepted'] } | { kind: CGRIDTag__P2pPointState.Running, value: CGRIDMap__P2pPointState['Running'] } | { kind: CGRIDTag__P2pPointState.Disconnected, value: CGRIDMap__P2pPointState['Disconnected'] };
export type P2pPointInfoP2pPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointInfoLastSeenIndex0 = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointInfoLastSeen = [CGRIDClass__P2p_pointInfo_last_seen_index0, Int64];
export type P2pPointInfoLastRejectedConnectionIndex0 = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointInfoLastRejectedConnection = [CGRIDClass__P2p_pointInfo_last_rejected_connection_index0, Int64];
export type P2pPointInfoLastEstablishedConnectionIndex0 = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointInfoLastEstablishedConnection = [CGRIDClass__P2p_pointInfo_last_established_connection_index0, Int64];
export type P2pPointInfoLastDisconnectionIndex0 = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointInfoLastDisconnection = [CGRIDClass__P2p_pointInfo_last_disconnection_index0, Int64];
export type P2pPointInfoExpectedPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pPointInfo = { trusted: Bool, greylisted_until: Option<Int64>, state: CGRIDClass__P2p_pointState, p2p_peer_id: Option<CGRIDClass__P2p_pointInfo_p2p_peer_id>, last_failed_connection: Option<Int64>, last_rejected_connection: Option<CGRIDClass__P2p_pointInfo_last_rejected_connection>, last_established_connection: Option<CGRIDClass__P2p_pointInfo_last_established_connection>, last_disconnection: Option<CGRIDClass__P2p_pointInfo_last_disconnection>, last_seen: Option<CGRIDClass__P2p_pointInfo_last_seen>, last_miss: Option<Int64>, expected_peer_id: Option<CGRIDClass__P2p_pointInfo_expected_peer_id> };
export class CGRIDClass__P2pPointInfo extends Box<P2pPointInfo> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['trusted', 'greylisted_until', 'state', 'p2p_peer_id', 'last_failed_connection', 'last_rejected_connection', 'last_established_connection', 'last_disconnection', 'last_seen', 'last_miss', 'expected_peer_id']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPointInfo {
        return new this(record_decoder<P2pPointInfo>({trusted: Bool.decode, greylisted_until: Option.decode(Int64.decode), state: CGRIDClass__P2p_pointState.decode, p2p_peer_id: Option.decode(CGRIDClass__P2p_pointInfo_p2p_peer_id.decode), last_failed_connection: Option.decode(Int64.decode), last_rejected_connection: Option.decode(CGRIDClass__P2p_pointInfo_last_rejected_connection.decode), last_established_connection: Option.decode(CGRIDClass__P2p_pointInfo_last_established_connection.decode), last_disconnection: Option.decode(CGRIDClass__P2p_pointInfo_last_disconnection.decode), last_seen: Option.decode(CGRIDClass__P2p_pointInfo_last_seen.decode), last_miss: Option.decode(Int64.decode), expected_peer_id: Option.decode(CGRIDClass__P2p_pointInfo_expected_peer_id.decode)}, {order: ['trusted', 'greylisted_until', 'state', 'p2p_peer_id', 'last_failed_connection', 'last_rejected_connection', 'last_established_connection', 'last_disconnection', 'last_seen', 'last_miss', 'expected_peer_id']})(p));
    };
    get encodeLength(): number {
        return (this.value.trusted.encodeLength +  this.value.greylisted_until.encodeLength +  this.value.state.encodeLength +  this.value.p2p_peer_id.encodeLength +  this.value.last_failed_connection.encodeLength +  this.value.last_rejected_connection.encodeLength +  this.value.last_established_connection.encodeLength +  this.value.last_disconnection.encodeLength +  this.value.last_seen.encodeLength +  this.value.last_miss.encodeLength +  this.value.expected_peer_id.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.trusted.writeTarget(tgt) +  this.value.greylisted_until.writeTarget(tgt) +  this.value.state.writeTarget(tgt) +  this.value.p2p_peer_id.writeTarget(tgt) +  this.value.last_failed_connection.writeTarget(tgt) +  this.value.last_rejected_connection.writeTarget(tgt) +  this.value.last_established_connection.writeTarget(tgt) +  this.value.last_disconnection.writeTarget(tgt) +  this.value.last_seen.writeTarget(tgt) +  this.value.last_miss.writeTarget(tgt) +  this.value.expected_peer_id.writeTarget(tgt));
    }
}
export const p2p_point_info_encoder = (value: P2pPointInfo): OutputBytes => {
    return record_encoder({order: ['trusted', 'greylisted_until', 'state', 'p2p_peer_id', 'last_failed_connection', 'last_rejected_connection', 'last_established_connection', 'last_disconnection', 'last_seen', 'last_miss', 'expected_peer_id']})(value);
}
export const p2p_point_info_decoder = (p: Parser): P2pPointInfo => {
    return record_decoder<P2pPointInfo>({trusted: Bool.decode, greylisted_until: Option.decode(Int64.decode), state: CGRIDClass__P2p_pointState.decode, p2p_peer_id: Option.decode(CGRIDClass__P2p_pointInfo_p2p_peer_id.decode), last_failed_connection: Option.decode(Int64.decode), last_rejected_connection: Option.decode(CGRIDClass__P2p_pointInfo_last_rejected_connection.decode), last_established_connection: Option.decode(CGRIDClass__P2p_pointInfo_last_established_connection.decode), last_disconnection: Option.decode(CGRIDClass__P2p_pointInfo_last_disconnection.decode), last_seen: Option.decode(CGRIDClass__P2p_pointInfo_last_seen.decode), last_miss: Option.decode(Int64.decode), expected_peer_id: Option.decode(CGRIDClass__P2p_pointInfo_expected_peer_id.decode)}, {order: ['trusted', 'greylisted_until', 'state', 'p2p_peer_id', 'last_failed_connection', 'last_rejected_connection', 'last_established_connection', 'last_disconnection', 'last_seen', 'last_miss', 'expected_peer_id']})(p);
}
