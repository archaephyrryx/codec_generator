import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__OperationShell_header_branch generated for OperationShellHeaderBranch
export class CGRIDClass__OperationShell_header_branch extends Box<OperationShellHeaderBranch> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['block_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShell_header_branch {
        return new this(record_decoder<OperationShellHeaderBranch>({block_hash: FixedBytes.decode<32>({len: 32})}, {order: ['block_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.block_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.block_hash.writeTarget(tgt));
    }
}
export type OperationShellHeaderBranch = { block_hash: FixedBytes<32> };
export type OperationShellHeader = { branch: CGRIDClass__OperationShell_header_branch };
export class CGRIDClass__OperationShellHeader extends Box<OperationShellHeader> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['branch']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__OperationShellHeader {
        return new this(record_decoder<OperationShellHeader>({branch: CGRIDClass__OperationShell_header_branch.decode}, {order: ['branch']})(p));
    };
    get encodeLength(): number {
        return (this.value.branch.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.branch.writeTarget(tgt));
    }
}
export const operation_shell_header_encoder = (value: OperationShellHeader): OutputBytes => {
    return record_encoder({order: ['branch']})(value);
}
export const operation_shell_header_decoder = (p: Parser): OperationShellHeader => {
    return record_decoder<OperationShellHeader>({branch: CGRIDClass__OperationShell_header_branch.decode}, {order: ['branch']})(p);
}
