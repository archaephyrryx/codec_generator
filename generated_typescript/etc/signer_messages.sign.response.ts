import { Codec } from '../../ts_runtime/codec';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__Signer_messagesSignResponse_signature generated for SignerMessagesSignResponseSignature
export class CGRIDClass__Signer_messagesSignResponse_signature extends Box<SignerMessagesSignResponseSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Signer_messagesSignResponse_signature {
        return new this(record_decoder<SignerMessagesSignResponseSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
export type SignerMessagesSignResponseSignature = { signature_v1: Bytes };
export type SignerMessagesSignResponse = { signature: CGRIDClass__Signer_messagesSignResponse_signature };
export class CGRIDClass__SignerMessagesSignResponse extends Box<SignerMessagesSignResponse> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesSignResponse {
        return new this(record_decoder<SignerMessagesSignResponse>({signature: CGRIDClass__Signer_messagesSignResponse_signature.decode}, {order: ['signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature.writeTarget(tgt));
    }
}
export const signer_messages_sign_response_encoder = (value: SignerMessagesSignResponse): OutputBytes => {
    return record_encoder({order: ['signature']})(value);
}
export const signer_messages_sign_response_decoder = (p: Parser): SignerMessagesSignResponse => {
    return record_decoder<SignerMessagesSignResponse>({signature: CGRIDClass__Signer_messagesSignResponse_signature.decode}, {order: ['signature']})(p);
}
