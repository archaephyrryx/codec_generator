import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type ContextHashVersion = Uint16;
export class CGRIDClass__ContextHashVersion extends Box<ContextHashVersion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__ContextHashVersion {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const context_hash_version_encoder = (value: ContextHashVersion): OutputBytes => {
    return value.encode();
}
export const context_hash_version_decoder = (p: Parser): ContextHashVersion => {
    return Uint16.decode(p);
}
