import { Codec } from '../../ts_runtime/codec';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__P2p_identity_peer_id generated for P2pIdentityPeerId
export class CGRIDClass__P2p_identity_peer_id extends Box<P2pIdentityPeerId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['crypto_box_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_identity_peer_id {
        return new this(record_decoder<P2pIdentityPeerId>({crypto_box_public_key_hash: FixedBytes.decode<16>({len: 16})}, {order: ['crypto_box_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.crypto_box_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.crypto_box_public_key_hash.writeTarget(tgt));
    }
}
export type P2pIdentityPeerId = { crypto_box_public_key_hash: FixedBytes<16> };
export type P2pIdentity = { peer_id: Option<CGRIDClass__P2p_identity_peer_id>, public_key: FixedBytes<32>, secret_key: FixedBytes<32>, proof_of_work_stamp: FixedBytes<24> };
export class CGRIDClass__P2pIdentity extends Box<P2pIdentity> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['peer_id', 'public_key', 'secret_key', 'proof_of_work_stamp']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pIdentity {
        return new this(record_decoder<P2pIdentity>({peer_id: Option.decode(CGRIDClass__P2p_identity_peer_id.decode), public_key: FixedBytes.decode<32>({len: 32}), secret_key: FixedBytes.decode<32>({len: 32}), proof_of_work_stamp: FixedBytes.decode<24>({len: 24})}, {order: ['peer_id', 'public_key', 'secret_key', 'proof_of_work_stamp']})(p));
    };
    get encodeLength(): number {
        return (this.value.peer_id.encodeLength +  this.value.public_key.encodeLength +  this.value.secret_key.encodeLength +  this.value.proof_of_work_stamp.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.peer_id.writeTarget(tgt) +  this.value.public_key.writeTarget(tgt) +  this.value.secret_key.writeTarget(tgt) +  this.value.proof_of_work_stamp.writeTarget(tgt));
    }
}
export const p2p_identity_encoder = (value: P2pIdentity): OutputBytes => {
    return record_encoder({order: ['peer_id', 'public_key', 'secret_key', 'proof_of_work_stamp']})(value);
}
export const p2p_identity_decoder = (p: Parser): P2pIdentity => {
    return record_decoder<P2pIdentity>({peer_id: Option.decode(CGRIDClass__P2p_identity_peer_id.decode), public_key: FixedBytes.decode<32>({len: 32}), secret_key: FixedBytes.decode<32>({len: 32}), proof_of_work_stamp: FixedBytes.decode<24>({len: 24})}, {order: ['peer_id', 'public_key', 'secret_key', 'proof_of_work_stamp']})(p);
}
