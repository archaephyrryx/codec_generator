import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Nullable } from '../../ts_runtime/composite/opt/nullable';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKeyHash__Secp256k1 generated for PublicKeyHash__Secp256k1
export class CGRIDClass__PublicKeyHash__Secp256k1 extends Box<PublicKeyHash__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Secp256k1 {
        return new this(record_decoder<PublicKeyHash__Secp256k1>({secp256k1_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['secp256k1_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__P256 generated for PublicKeyHash__P256
export class CGRIDClass__PublicKeyHash__P256 extends Box<PublicKeyHash__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__P256 {
        return new this(record_decoder<PublicKeyHash__P256>({p256_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['p256_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Ed25519 generated for PublicKeyHash__Ed25519
export class CGRIDClass__PublicKeyHash__Ed25519 extends Box<PublicKeyHash__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Ed25519 {
        return new this(record_decoder<PublicKeyHash__Ed25519>({ed25519_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['ed25519_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKeyHash__Bls generated for PublicKeyHash__Bls
export class CGRIDClass__PublicKeyHash__Bls extends Box<PublicKeyHash__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKeyHash__Bls {
        return new this(record_decoder<PublicKeyHash__Bls>({bls12_381_public_key_hash: FixedBytes.decode<20>({len: 20})}, {order: ['bls12_381_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key_hash.writeTarget(tgt));
    }
}
export type PublicKeyHash__Secp256k1 = { secp256k1_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__P256 = { p256_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Ed25519 = { ed25519_public_key_hash: FixedBytes<20> };
export type PublicKeyHash__Bls = { bls12_381_public_key_hash: FixedBytes<20> };
// Class CGRIDClass__Signer_messagesSupports_deterministic_noncesRequest_pkh generated for SignerMessagesSupportsDeterministicNoncesRequestPkh
export class CGRIDClass__Signer_messagesSupports_deterministic_noncesRequest_pkh extends Box<SignerMessagesSupportsDeterministicNoncesRequestPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Signer_messagesSupports_deterministic_noncesRequest_pkh {
        return new this(record_decoder<SignerMessagesSupportsDeterministicNoncesRequestPkh>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Signer_messagesRequest_Sign_signature generated for SignerMessagesRequestSignSignature
export class CGRIDClass__Signer_messagesRequest_Sign_signature extends Box<SignerMessagesRequestSignSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Signer_messagesRequest_Sign_signature {
        return new this(record_decoder<SignerMessagesRequestSignSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__Signer_messagesRequest_Sign_pkh generated for SignerMessagesRequestSignPkh
export class CGRIDClass__Signer_messagesRequest_Sign_pkh extends Box<SignerMessagesRequestSignPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Signer_messagesRequest_Sign_pkh {
        return new this(record_decoder<SignerMessagesRequestSignPkh>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Signer_messagesRequest_Deterministic_nonce_signature generated for SignerMessagesRequestDeterministicNonceSignature
export class CGRIDClass__Signer_messagesRequest_Deterministic_nonce_signature extends Box<SignerMessagesRequestDeterministicNonceSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Signer_messagesRequest_Deterministic_nonce_signature {
        return new this(record_decoder<SignerMessagesRequestDeterministicNonceSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__Signer_messagesRequest_Deterministic_nonce_pkh generated for SignerMessagesRequestDeterministicNoncePkh
export class CGRIDClass__Signer_messagesRequest_Deterministic_nonce_pkh extends Box<SignerMessagesRequestDeterministicNoncePkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Signer_messagesRequest_Deterministic_nonce_pkh {
        return new this(record_decoder<SignerMessagesRequestDeterministicNoncePkh>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Signer_messagesRequest_Deterministic_nonce_hash_signature generated for SignerMessagesRequestDeterministicNonceHashSignature
export class CGRIDClass__Signer_messagesRequest_Deterministic_nonce_hash_signature extends Box<SignerMessagesRequestDeterministicNonceHashSignature> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_v1']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Signer_messagesRequest_Deterministic_nonce_hash_signature {
        return new this(record_decoder<SignerMessagesRequestDeterministicNonceHashSignature>({signature_v1: Bytes.decode}, {order: ['signature_v1']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_v1.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_v1.writeTarget(tgt));
    }
}
// Class CGRIDClass__Signer_messagesRequest_Deterministic_nonce_hash_pkh generated for SignerMessagesRequestDeterministicNonceHashPkh
export class CGRIDClass__Signer_messagesRequest_Deterministic_nonce_hash_pkh extends Box<SignerMessagesRequestDeterministicNonceHashPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Signer_messagesRequest_Deterministic_nonce_hash_pkh {
        return new this(record_decoder<SignerMessagesRequestDeterministicNonceHashPkh>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__Signer_messagesPublic_keyRequest_pkh generated for SignerMessagesPublicKeyRequestPkh
export class CGRIDClass__Signer_messagesPublic_keyRequest_pkh extends Box<SignerMessagesPublicKeyRequestPkh> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Signer_messagesPublic_keyRequest_pkh {
        return new this(record_decoder<SignerMessagesPublicKeyRequestPkh>({signature_public_key_hash: CGRIDClass__Public_key_hash.decode}, {order: ['signature_public_key_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__SignerMessagesRequest__Supports_deterministic_nonces generated for SignerMessagesRequest__Supports_deterministic_nonces
export class CGRIDClass__SignerMessagesRequest__Supports_deterministic_nonces extends Box<SignerMessagesRequest__Supports_deterministic_nonces> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesRequest__Supports_deterministic_nonces {
        return new this(record_decoder<SignerMessagesRequest__Supports_deterministic_nonces>({pkh: CGRIDClass__Signer_messagesSupports_deterministic_noncesRequest_pkh.decode}, {order: ['pkh']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt));
    }
}
// Class CGRIDClass__SignerMessagesRequest__Sign generated for SignerMessagesRequest__Sign
export class CGRIDClass__SignerMessagesRequest__Sign extends Box<SignerMessagesRequest__Sign> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'data', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesRequest__Sign {
        return new this(record_decoder<SignerMessagesRequest__Sign>({pkh: CGRIDClass__Signer_messagesRequest_Sign_pkh.decode, data: Dynamic.decode(Bytes.decode, width.Uint30), signature: Nullable.decode(CGRIDClass__Signer_messagesRequest_Sign_signature.decode)}, {order: ['pkh', 'data', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.data.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.data.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__SignerMessagesRequest__Public_key generated for SignerMessagesRequest__Public_key
export class CGRIDClass__SignerMessagesRequest__Public_key extends Box<SignerMessagesRequest__Public_key> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesRequest__Public_key {
        return new this(record_decoder<SignerMessagesRequest__Public_key>({pkh: CGRIDClass__Signer_messagesPublic_keyRequest_pkh.decode}, {order: ['pkh']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt));
    }
}
// Class CGRIDClass__SignerMessagesRequest__Deterministic_nonce_hash generated for SignerMessagesRequest__Deterministic_nonce_hash
export class CGRIDClass__SignerMessagesRequest__Deterministic_nonce_hash extends Box<SignerMessagesRequest__Deterministic_nonce_hash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'data', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesRequest__Deterministic_nonce_hash {
        return new this(record_decoder<SignerMessagesRequest__Deterministic_nonce_hash>({pkh: CGRIDClass__Signer_messagesRequest_Deterministic_nonce_hash_pkh.decode, data: Dynamic.decode(Bytes.decode, width.Uint30), signature: Nullable.decode(CGRIDClass__Signer_messagesRequest_Deterministic_nonce_hash_signature.decode)}, {order: ['pkh', 'data', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.data.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.data.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__SignerMessagesRequest__Deterministic_nonce generated for SignerMessagesRequest__Deterministic_nonce
export class CGRIDClass__SignerMessagesRequest__Deterministic_nonce extends Box<SignerMessagesRequest__Deterministic_nonce> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pkh', 'data', 'signature']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesRequest__Deterministic_nonce {
        return new this(record_decoder<SignerMessagesRequest__Deterministic_nonce>({pkh: CGRIDClass__Signer_messagesRequest_Deterministic_nonce_pkh.decode, data: Dynamic.decode(Bytes.decode, width.Uint30), signature: Nullable.decode(CGRIDClass__Signer_messagesRequest_Deterministic_nonce_signature.decode)}, {order: ['pkh', 'data', 'signature']})(p));
    };
    get encodeLength(): number {
        return (this.value.pkh.encodeLength +  this.value.data.encodeLength +  this.value.signature.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pkh.writeTarget(tgt) +  this.value.data.writeTarget(tgt) +  this.value.signature.writeTarget(tgt));
    }
}
// Class CGRIDClass__SignerMessagesRequest__Authorized_keys generated for SignerMessagesRequest__Authorized_keys
export class CGRIDClass__SignerMessagesRequest__Authorized_keys extends Box<SignerMessagesRequest__Authorized_keys> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: []})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesRequest__Authorized_keys {
        return new this(record_decoder<SignerMessagesRequest__Authorized_keys>({}, {order: []})(p));
    };
    get encodeLength(): number {
        return 0;
    };
    writeTarget(tgt: Target): number {
        return 0;
    }
}
// Class CGRIDClass__Public_key_hash generated for PublicKeyHash
export function publickeyhash_mkDecoder(): VariantDecoder<CGRIDTag__PublicKeyHash,PublicKeyHash> {
    function f(disc: CGRIDTag__PublicKeyHash) {
        switch (disc) {
            case CGRIDTag__PublicKeyHash.Ed25519: return CGRIDClass__PublicKeyHash__Ed25519.decode;
            case CGRIDTag__PublicKeyHash.Secp256k1: return CGRIDClass__PublicKeyHash__Secp256k1.decode;
            case CGRIDTag__PublicKeyHash.P256: return CGRIDClass__PublicKeyHash__P256.decode;
            case CGRIDTag__PublicKeyHash.Bls: return CGRIDClass__PublicKeyHash__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKeyHash => Object.values(CGRIDTag__PublicKeyHash).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key_hash extends Box<PublicKeyHash> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKeyHash>, PublicKeyHash>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key_hash {
        return new this(variant_decoder(width.Uint8)(publickeyhash_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export type SignerMessagesSupportsDeterministicNoncesRequestPkh = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type SignerMessagesRequestSignSignature = { signature_v1: Bytes };
export type SignerMessagesRequestSignPkh = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type SignerMessagesRequestDeterministicNonceSignature = { signature_v1: Bytes };
export type SignerMessagesRequestDeterministicNoncePkh = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type SignerMessagesRequestDeterministicNonceHashSignature = { signature_v1: Bytes };
export type SignerMessagesRequestDeterministicNonceHashPkh = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export type SignerMessagesPublicKeyRequestPkh = { signature_public_key_hash: CGRIDClass__Public_key_hash };
export enum CGRIDTag__PublicKeyHash{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKeyHash {
    Ed25519: CGRIDClass__PublicKeyHash__Ed25519,
    Secp256k1: CGRIDClass__PublicKeyHash__Secp256k1,
    P256: CGRIDClass__PublicKeyHash__P256,
    Bls: CGRIDClass__PublicKeyHash__Bls
}
export type PublicKeyHash = { kind: CGRIDTag__PublicKeyHash.Ed25519, value: CGRIDMap__PublicKeyHash['Ed25519'] } | { kind: CGRIDTag__PublicKeyHash.Secp256k1, value: CGRIDMap__PublicKeyHash['Secp256k1'] } | { kind: CGRIDTag__PublicKeyHash.P256, value: CGRIDMap__PublicKeyHash['P256'] } | { kind: CGRIDTag__PublicKeyHash.Bls, value: CGRIDMap__PublicKeyHash['Bls'] };
export type SignerMessagesRequest__Supports_deterministic_nonces = { pkh: CGRIDClass__Signer_messagesSupports_deterministic_noncesRequest_pkh };
export type SignerMessagesRequest__Sign = { pkh: CGRIDClass__Signer_messagesRequest_Sign_pkh, data: Dynamic<Bytes,width.Uint30>, signature: Nullable<CGRIDClass__Signer_messagesRequest_Sign_signature> };
export type SignerMessagesRequest__Public_key = { pkh: CGRIDClass__Signer_messagesPublic_keyRequest_pkh };
export type SignerMessagesRequest__Deterministic_nonce_hash = { pkh: CGRIDClass__Signer_messagesRequest_Deterministic_nonce_hash_pkh, data: Dynamic<Bytes,width.Uint30>, signature: Nullable<CGRIDClass__Signer_messagesRequest_Deterministic_nonce_hash_signature> };
export type SignerMessagesRequest__Deterministic_nonce = { pkh: CGRIDClass__Signer_messagesRequest_Deterministic_nonce_pkh, data: Dynamic<Bytes,width.Uint30>, signature: Nullable<CGRIDClass__Signer_messagesRequest_Deterministic_nonce_signature> };
export type SignerMessagesRequest__Authorized_keys = {  };
export enum CGRIDTag__SignerMessagesRequest{
    Sign = 0,
    Public_key = 1,
    Authorized_keys = 2,
    Deterministic_nonce = 3,
    Deterministic_nonce_hash = 4,
    Supports_deterministic_nonces = 5
}
export interface CGRIDMap__SignerMessagesRequest {
    Sign: CGRIDClass__SignerMessagesRequest__Sign,
    Public_key: CGRIDClass__SignerMessagesRequest__Public_key,
    Authorized_keys: CGRIDClass__SignerMessagesRequest__Authorized_keys,
    Deterministic_nonce: CGRIDClass__SignerMessagesRequest__Deterministic_nonce,
    Deterministic_nonce_hash: CGRIDClass__SignerMessagesRequest__Deterministic_nonce_hash,
    Supports_deterministic_nonces: CGRIDClass__SignerMessagesRequest__Supports_deterministic_nonces
}
export type SignerMessagesRequest = { kind: CGRIDTag__SignerMessagesRequest.Sign, value: CGRIDMap__SignerMessagesRequest['Sign'] } | { kind: CGRIDTag__SignerMessagesRequest.Public_key, value: CGRIDMap__SignerMessagesRequest['Public_key'] } | { kind: CGRIDTag__SignerMessagesRequest.Authorized_keys, value: CGRIDMap__SignerMessagesRequest['Authorized_keys'] } | { kind: CGRIDTag__SignerMessagesRequest.Deterministic_nonce, value: CGRIDMap__SignerMessagesRequest['Deterministic_nonce'] } | { kind: CGRIDTag__SignerMessagesRequest.Deterministic_nonce_hash, value: CGRIDMap__SignerMessagesRequest['Deterministic_nonce_hash'] } | { kind: CGRIDTag__SignerMessagesRequest.Supports_deterministic_nonces, value: CGRIDMap__SignerMessagesRequest['Supports_deterministic_nonces'] };
export function signermessagesrequest_mkDecoder(): VariantDecoder<CGRIDTag__SignerMessagesRequest,SignerMessagesRequest> {
    function f(disc: CGRIDTag__SignerMessagesRequest) {
        switch (disc) {
            case CGRIDTag__SignerMessagesRequest.Sign: return CGRIDClass__SignerMessagesRequest__Sign.decode;
            case CGRIDTag__SignerMessagesRequest.Public_key: return CGRIDClass__SignerMessagesRequest__Public_key.decode;
            case CGRIDTag__SignerMessagesRequest.Authorized_keys: return CGRIDClass__SignerMessagesRequest__Authorized_keys.decode;
            case CGRIDTag__SignerMessagesRequest.Deterministic_nonce: return CGRIDClass__SignerMessagesRequest__Deterministic_nonce.decode;
            case CGRIDTag__SignerMessagesRequest.Deterministic_nonce_hash: return CGRIDClass__SignerMessagesRequest__Deterministic_nonce_hash.decode;
            case CGRIDTag__SignerMessagesRequest.Supports_deterministic_nonces: return CGRIDClass__SignerMessagesRequest__Supports_deterministic_nonces.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__SignerMessagesRequest => Object.values(CGRIDTag__SignerMessagesRequest).includes(tagval);
    return f;
}
export class CGRIDClass__SignerMessagesRequest extends Box<SignerMessagesRequest> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<SignerMessagesRequest>, SignerMessagesRequest>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesRequest {
        return new this(variant_decoder(width.Uint8)(signermessagesrequest_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export const signer_messages_request_encoder = (value: SignerMessagesRequest): OutputBytes => {
    return variant_encoder<KindOf<SignerMessagesRequest>, SignerMessagesRequest>(width.Uint8)(value);
}
export const signer_messages_request_decoder = (p: Parser): SignerMessagesRequest => {
    return variant_decoder(width.Uint8)(signermessagesrequest_mkDecoder())(p);
}
