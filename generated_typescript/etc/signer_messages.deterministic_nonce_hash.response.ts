import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { Target } from '../../ts_runtime/target';
export type SignerMessagesDeterministicNonceHashResponse = { deterministic_nonce_hash: Dynamic<Bytes,width.Uint30> };
export class CGRIDClass__SignerMessagesDeterministicNonceHashResponse extends Box<SignerMessagesDeterministicNonceHashResponse> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['deterministic_nonce_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesDeterministicNonceHashResponse {
        return new this(record_decoder<SignerMessagesDeterministicNonceHashResponse>({deterministic_nonce_hash: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['deterministic_nonce_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.deterministic_nonce_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.deterministic_nonce_hash.writeTarget(tgt));
    }
}
export const signer_messages_deterministic_nonce_hash_response_encoder = (value: SignerMessagesDeterministicNonceHashResponse): OutputBytes => {
    return record_encoder({order: ['deterministic_nonce_hash']})(value);
}
export const signer_messages_deterministic_nonce_hash_response_decoder = (p: Parser): SignerMessagesDeterministicNonceHashResponse => {
    return record_decoder<SignerMessagesDeterministicNonceHashResponse>({deterministic_nonce_hash: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['deterministic_nonce_hash']})(p);
}
