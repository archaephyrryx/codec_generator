import { Codec } from '../../ts_runtime/codec';
import { KindOf, VariantDecoder, variant_decoder, variant_encoder } from '../../ts_runtime/constructed/adt';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__PublicKey__Secp256k1 generated for PublicKey__Secp256k1
export class CGRIDClass__PublicKey__Secp256k1 extends Box<PublicKey__Secp256k1> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['secp256k1_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Secp256k1 {
        return new this(record_decoder<PublicKey__Secp256k1>({secp256k1_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['secp256k1_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.secp256k1_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.secp256k1_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__P256 generated for PublicKey__P256
export class CGRIDClass__PublicKey__P256 extends Box<PublicKey__P256> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['p256_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__P256 {
        return new this(record_decoder<PublicKey__P256>({p256_public_key: FixedBytes.decode<33>({len: 33})}, {order: ['p256_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.p256_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.p256_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Ed25519 generated for PublicKey__Ed25519
export class CGRIDClass__PublicKey__Ed25519 extends Box<PublicKey__Ed25519> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['ed25519_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Ed25519 {
        return new this(record_decoder<PublicKey__Ed25519>({ed25519_public_key: FixedBytes.decode<32>({len: 32})}, {order: ['ed25519_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.ed25519_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.ed25519_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__PublicKey__Bls generated for PublicKey__Bls
export class CGRIDClass__PublicKey__Bls extends Box<PublicKey__Bls> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['bls12_381_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__PublicKey__Bls {
        return new this(record_decoder<PublicKey__Bls>({bls12_381_public_key: FixedBytes.decode<48>({len: 48})}, {order: ['bls12_381_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.bls12_381_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.bls12_381_public_key.writeTarget(tgt));
    }
}
export type PublicKey__Secp256k1 = { secp256k1_public_key: FixedBytes<33> };
export type PublicKey__P256 = { p256_public_key: FixedBytes<33> };
export type PublicKey__Ed25519 = { ed25519_public_key: FixedBytes<32> };
export type PublicKey__Bls = { bls12_381_public_key: FixedBytes<48> };
// Class CGRIDClass__Signer_messagesPublic_keyResponse_pubkey generated for SignerMessagesPublicKeyResponsePubkey
export class CGRIDClass__Signer_messagesPublic_keyResponse_pubkey extends Box<SignerMessagesPublicKeyResponsePubkey> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['signature_public_key']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__Signer_messagesPublic_keyResponse_pubkey {
        return new this(record_decoder<SignerMessagesPublicKeyResponsePubkey>({signature_public_key: CGRIDClass__Public_key.decode}, {order: ['signature_public_key']})(p));
    };
    get encodeLength(): number {
        return (this.value.signature_public_key.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.signature_public_key.writeTarget(tgt));
    }
}
// Class CGRIDClass__Public_key generated for PublicKey
export function publickey_mkDecoder(): VariantDecoder<CGRIDTag__PublicKey,PublicKey> {
    function f(disc: CGRIDTag__PublicKey) {
        switch (disc) {
            case CGRIDTag__PublicKey.Ed25519: return CGRIDClass__PublicKey__Ed25519.decode;
            case CGRIDTag__PublicKey.Secp256k1: return CGRIDClass__PublicKey__Secp256k1.decode;
            case CGRIDTag__PublicKey.P256: return CGRIDClass__PublicKey__P256.decode;
            case CGRIDTag__PublicKey.Bls: return CGRIDClass__PublicKey__Bls.decode;
        }
    }
    f.isValid = (tagval: number): tagval is CGRIDTag__PublicKey => Object.values(CGRIDTag__PublicKey).includes(tagval);
    return f;
}
export class CGRIDClass__Public_key extends Box<PublicKey> implements Codec {
    encode(): OutputBytes {
        return variant_encoder<KindOf<PublicKey>, PublicKey>(width.Uint8)(this.value);
    };
    static decode(p: Parser): CGRIDClass__Public_key {
        return new this(variant_decoder(width.Uint8)(publickey_mkDecoder())(p));
    };
    get encodeLength(): number {
        return (1 +  this.value.value.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (Width.from(width.Uint8, this.value.kind).writeTarget(tgt) +  this.value.value.writeTarget(tgt));
    }
}
export type SignerMessagesPublicKeyResponsePubkey = { signature_public_key: CGRIDClass__Public_key };
export enum CGRIDTag__PublicKey{
    Ed25519 = 0,
    Secp256k1 = 1,
    P256 = 2,
    Bls = 3
}
export interface CGRIDMap__PublicKey {
    Ed25519: CGRIDClass__PublicKey__Ed25519,
    Secp256k1: CGRIDClass__PublicKey__Secp256k1,
    P256: CGRIDClass__PublicKey__P256,
    Bls: CGRIDClass__PublicKey__Bls
}
export type PublicKey = { kind: CGRIDTag__PublicKey.Ed25519, value: CGRIDMap__PublicKey['Ed25519'] } | { kind: CGRIDTag__PublicKey.Secp256k1, value: CGRIDMap__PublicKey['Secp256k1'] } | { kind: CGRIDTag__PublicKey.P256, value: CGRIDMap__PublicKey['P256'] } | { kind: CGRIDTag__PublicKey.Bls, value: CGRIDMap__PublicKey['Bls'] };
export type SignerMessagesPublicKeyResponse = { pubkey: CGRIDClass__Signer_messagesPublic_keyResponse_pubkey };
export class CGRIDClass__SignerMessagesPublicKeyResponse extends Box<SignerMessagesPublicKeyResponse> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['pubkey']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesPublicKeyResponse {
        return new this(record_decoder<SignerMessagesPublicKeyResponse>({pubkey: CGRIDClass__Signer_messagesPublic_keyResponse_pubkey.decode}, {order: ['pubkey']})(p));
    };
    get encodeLength(): number {
        return (this.value.pubkey.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.pubkey.writeTarget(tgt));
    }
}
export const signer_messages_public_key_response_encoder = (value: SignerMessagesPublicKeyResponse): OutputBytes => {
    return record_encoder({order: ['pubkey']})(value);
}
export const signer_messages_public_key_response_decoder = (p: Parser): SignerMessagesPublicKeyResponse => {
    return record_decoder<SignerMessagesPublicKeyResponse>({pubkey: CGRIDClass__Signer_messagesPublic_keyResponse_pubkey.decode}, {order: ['pubkey']})(p);
}
