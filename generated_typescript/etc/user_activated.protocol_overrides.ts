import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq_replacement_protocol generated for UserActivatedProtocolOverridesDenestDynDenestSeqReplacementProtocol
export class CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq_replacement_protocol extends Box<UserActivatedProtocolOverridesDenestDynDenestSeqReplacementProtocol> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq_replacement_protocol {
        return new this(record_decoder<UserActivatedProtocolOverridesDenestDynDenestSeqReplacementProtocol>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq_replaced_protocol generated for UserActivatedProtocolOverridesDenestDynDenestSeqReplacedProtocol
export class CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq_replaced_protocol extends Box<UserActivatedProtocolOverridesDenestDynDenestSeqReplacedProtocol> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq_replaced_protocol {
        return new this(record_decoder<UserActivatedProtocolOverridesDenestDynDenestSeqReplacedProtocol>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
// Class CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq generated for UserActivatedProtocolOverridesDenestDynDenestSeq
export class CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq extends Box<UserActivatedProtocolOverridesDenestDynDenestSeq> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['replaced_protocol', 'replacement_protocol']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq {
        return new this(record_decoder<UserActivatedProtocolOverridesDenestDynDenestSeq>({replaced_protocol: CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq_replaced_protocol.decode, replacement_protocol: CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq_replacement_protocol.decode}, {order: ['replaced_protocol', 'replacement_protocol']})(p));
    };
    get encodeLength(): number {
        return (this.value.replaced_protocol.encodeLength +  this.value.replacement_protocol.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.replaced_protocol.writeTarget(tgt) +  this.value.replacement_protocol.writeTarget(tgt));
    }
}
export type UserActivatedProtocolOverridesDenestDynDenestSeqReplacementProtocol = { protocol_hash: FixedBytes<32> };
export type UserActivatedProtocolOverridesDenestDynDenestSeqReplacedProtocol = { protocol_hash: FixedBytes<32> };
export type UserActivatedProtocolOverridesDenestDynDenestSeq = { replaced_protocol: CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq_replaced_protocol, replacement_protocol: CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq_replacement_protocol };
export type UserActivatedProtocolOverrides = Dynamic<Sequence<CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq>,width.Uint30>;
export class CGRIDClass__UserActivatedProtocolOverrides extends Box<UserActivatedProtocolOverrides> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__UserActivatedProtocolOverrides {
        return new this(Dynamic.decode(Sequence.decode(CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq.decode), width.Uint30)(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const user_activated_protocol_overrides_encoder = (value: UserActivatedProtocolOverrides): OutputBytes => {
    return value.encode();
}
export const user_activated_protocol_overrides_decoder = (p: Parser): UserActivatedProtocolOverrides => {
    return Dynamic.decode(Sequence.decode(CGRIDClass__User_activatedProtocol_overrides_denest_dyn_denest_seq.decode), width.Uint30)(p);
}
