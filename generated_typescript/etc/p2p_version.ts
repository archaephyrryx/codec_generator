import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type P2pVersion = Uint16;
export class CGRIDClass__P2pVersion extends Box<P2pVersion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__P2pVersion {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const p2p_version_encoder = (value: P2pVersion): OutputBytes => {
    return value.encode();
}
export const p2p_version_decoder = (p: Parser): P2pVersion => {
    return Uint16.decode(p);
}
