import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Parser } from '../../ts_runtime/parse';
import { Bytes } from '../../ts_runtime/primitive/bytes';
import { Target } from '../../ts_runtime/target';
export type SignerMessagesDeterministicNonceResponse = { deterministic_nonce: Dynamic<Bytes,width.Uint30> };
export class CGRIDClass__SignerMessagesDeterministicNonceResponse extends Box<SignerMessagesDeterministicNonceResponse> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['deterministic_nonce']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__SignerMessagesDeterministicNonceResponse {
        return new this(record_decoder<SignerMessagesDeterministicNonceResponse>({deterministic_nonce: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['deterministic_nonce']})(p));
    };
    get encodeLength(): number {
        return (this.value.deterministic_nonce.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.deterministic_nonce.writeTarget(tgt));
    }
}
export const signer_messages_deterministic_nonce_response_encoder = (value: SignerMessagesDeterministicNonceResponse): OutputBytes => {
    return record_encoder({order: ['deterministic_nonce']})(value);
}
export const signer_messages_deterministic_nonce_response_decoder = (p: Parser): SignerMessagesDeterministicNonceResponse => {
    return record_decoder<SignerMessagesDeterministicNonceResponse>({deterministic_nonce: Dynamic.decode(Bytes.decode, width.Uint30)}, {order: ['deterministic_nonce']})(p);
}
