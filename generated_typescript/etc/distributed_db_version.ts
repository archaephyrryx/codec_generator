import { Codec } from '../../ts_runtime/codec';
import { Box } from '../../ts_runtime/core/box';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { Target } from '../../ts_runtime/target';
export type DistributedDbVersion = Uint16;
export class CGRIDClass__DistributedDbVersion extends Box<DistributedDbVersion> implements Codec {
    encode(): OutputBytes {
        return this.value.encode();
    };
    static decode(p: Parser): CGRIDClass__DistributedDbVersion {
        return new this(Uint16.decode(p));
    };
    get encodeLength(): number {
        return this.value.encodeLength;
    };
    writeTarget(tgt: Target): number {
        return this.value.writeTarget(tgt);
    }
}
export const distributed_db_version_encoder = (value: DistributedDbVersion): OutputBytes => {
    return value.encode();
}
export const distributed_db_version_decoder = (p: Parser): DistributedDbVersion => {
    return Uint16.decode(p);
}
