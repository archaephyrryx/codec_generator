import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { enum_decoder, enum_encoder } from '../../ts_runtime/constructed/enum';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import { Width } from '../../ts_runtime/core/width';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Int64, Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__P2p_peerPool_event_kind generated for P2pPeerPoolEventKind
export class CGRIDClass__P2p_peerPool_event_kind extends Box<P2pPeerPoolEventKind> implements Codec {
    encode(): OutputBytes {
        return enum_encoder(width.Uint8)<P2pPeerPoolEventKind>(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2p_peerPool_event_kind {
        return new this(enum_decoder(width.Uint8)((x): x is P2pPeerPoolEventKind => (Object.values(P2pPeerPoolEventKind).includes(x)))(p));
    };
    get encodeLength(): number {
        return 1;
    };
    writeTarget(tgt: Target): number {
        return Width.from(width.Uint8, this.value).writeTarget(tgt);
    }
}
export enum P2pPeerPoolEventKind{
    incoming_request = 0,
    rejecting_request = 1,
    request_rejected = 2,
    connection_established = 3,
    disconnection = 4,
    external_disconnection = 5
}
export type P2pPeerPoolEvent = { kind: CGRIDClass__P2p_peerPool_event_kind, timestamp: Int64, addr: Dynamic<U8String,width.Uint30>, port: Option<Uint16> };
export class CGRIDClass__P2pPeerPoolEvent extends Box<P2pPeerPoolEvent> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['kind', 'timestamp', 'addr', 'port']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pPeerPoolEvent {
        return new this(record_decoder<P2pPeerPoolEvent>({kind: CGRIDClass__P2p_peerPool_event_kind.decode, timestamp: Int64.decode, addr: Dynamic.decode(U8String.decode, width.Uint30), port: Option.decode(Uint16.decode)}, {order: ['kind', 'timestamp', 'addr', 'port']})(p));
    };
    get encodeLength(): number {
        return (this.value.kind.encodeLength +  this.value.timestamp.encodeLength +  this.value.addr.encodeLength +  this.value.port.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.kind.writeTarget(tgt) +  this.value.timestamp.writeTarget(tgt) +  this.value.addr.writeTarget(tgt) +  this.value.port.writeTarget(tgt));
    }
}
export const p2p_peer_pool_event_encoder = (value: P2pPeerPoolEvent): OutputBytes => {
    return record_encoder({order: ['kind', 'timestamp', 'addr', 'port']})(value);
}
export const p2p_peer_pool_event_decoder = (p: Parser): P2pPeerPoolEvent => {
    return record_decoder<P2pPeerPoolEvent>({kind: CGRIDClass__P2p_peerPool_event_kind.decode, timestamp: Int64.decode, addr: Dynamic.decode(U8String.decode, width.Uint30), port: Option.decode(Uint16.decode)}, {order: ['kind', 'timestamp', 'addr', 'port']})(p);
}
