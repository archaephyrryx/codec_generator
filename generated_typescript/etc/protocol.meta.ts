import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { Sequence } from '../../ts_runtime/composite/seq/sequence';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { FixedBytes } from '../../ts_runtime/primitive/bytes.fixed';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
// Class CGRIDClass__ProtocolMeta_hash generated for ProtocolMetaHash
export class CGRIDClass__ProtocolMeta_hash extends Box<ProtocolMetaHash> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['protocol_hash']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__ProtocolMeta_hash {
        return new this(record_decoder<ProtocolMetaHash>({protocol_hash: FixedBytes.decode<32>({len: 32})}, {order: ['protocol_hash']})(p));
    };
    get encodeLength(): number {
        return (this.value.protocol_hash.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.protocol_hash.writeTarget(tgt));
    }
}
export type ProtocolMetaHash = { protocol_hash: FixedBytes<32> };
export type ProtocolMeta = { hash: Option<CGRIDClass__ProtocolMeta_hash>, expected_env_version: Option<Uint16>, modules: Dynamic<Sequence<Dynamic<U8String,width.Uint30>>,width.Uint30> };
export class CGRIDClass__ProtocolMeta extends Box<ProtocolMeta> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['hash', 'expected_env_version', 'modules']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__ProtocolMeta {
        return new this(record_decoder<ProtocolMeta>({hash: Option.decode(CGRIDClass__ProtocolMeta_hash.decode), expected_env_version: Option.decode(Uint16.decode), modules: Dynamic.decode(Sequence.decode(Dynamic.decode(U8String.decode, width.Uint30)), width.Uint30)}, {order: ['hash', 'expected_env_version', 'modules']})(p));
    };
    get encodeLength(): number {
        return (this.value.hash.encodeLength +  this.value.expected_env_version.encodeLength +  this.value.modules.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.hash.writeTarget(tgt) +  this.value.expected_env_version.writeTarget(tgt) +  this.value.modules.writeTarget(tgt));
    }
}
export const protocol_meta_encoder = (value: ProtocolMeta): OutputBytes => {
    return record_encoder({order: ['hash', 'expected_env_version', 'modules']})(value);
}
export const protocol_meta_decoder = (p: Parser): ProtocolMeta => {
    return record_decoder<ProtocolMeta>({hash: Option.decode(CGRIDClass__ProtocolMeta_hash.decode), expected_env_version: Option.decode(Uint16.decode), modules: Dynamic.decode(Sequence.decode(Dynamic.decode(U8String.decode, width.Uint30)), width.Uint30)}, {order: ['hash', 'expected_env_version', 'modules']})(p);
}
