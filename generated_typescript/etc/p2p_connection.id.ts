import { Codec } from '../../ts_runtime/codec';
import { Dynamic } from '../../ts_runtime/composite/dynamic';
import { Option } from '../../ts_runtime/composite/opt/option';
import { record_decoder, record_encoder } from '../../ts_runtime/constructed/record';
import { Box } from '../../ts_runtime/core/box';
import width from '../../ts_runtime/core/width.type';
import { OutputBytes } from '../../ts_runtime/encode';
import { Uint16 } from '../../ts_runtime/integer/integer';
import { Parser } from '../../ts_runtime/parse';
import { U8String } from '../../ts_runtime/primitive/string';
import { Target } from '../../ts_runtime/target';
export type P2pConnectionId = { addr: Dynamic<U8String,width.Uint30>, port: Option<Uint16> };
export class CGRIDClass__P2pConnectionId extends Box<P2pConnectionId> implements Codec {
    encode(): OutputBytes {
        return record_encoder({order: ['addr', 'port']})(this.value);
    };
    static decode(p: Parser): CGRIDClass__P2pConnectionId {
        return new this(record_decoder<P2pConnectionId>({addr: Dynamic.decode(U8String.decode, width.Uint30), port: Option.decode(Uint16.decode)}, {order: ['addr', 'port']})(p));
    };
    get encodeLength(): number {
        return (this.value.addr.encodeLength +  this.value.port.encodeLength);
    };
    writeTarget(tgt: Target): number {
        return (this.value.addr.writeTarget(tgt) +  this.value.port.writeTarget(tgt));
    }
}
export const p2p_connection_id_encoder = (value: P2pConnectionId): OutputBytes => {
    return record_encoder({order: ['addr', 'port']})(value);
}
export const p2p_connection_id_decoder = (p: Parser): P2pConnectionId => {
    return record_decoder<P2pConnectionId>({addr: Dynamic.decode(U8String.decode, width.Uint30), port: Option.decode(Uint16.decode)}, {order: ['addr', 'port']})(p);
}
