# Codec Generator

Multi-language compiler framework for machine-generation of `data-encoding`-compliant codecs.

## Introduction

This repository hosts the experimental project `codec_generator`, an OCaml-based compiler
that outputs multi-language 'codecs' from a set of input 'schemas' defined using
`data-encoding` combinators. While part of the Tezos ecosystem, this is intended as a
standalone companion tool for developers of client libraries related to the Tezos protocol.

This project is an offshoot of the `tezos-codec-compiler` project ([repo](https://gitlab.com/tezos/tezos-codec-compiler)), and both are designed to serve a similar purpose, but with different approaches
and implementation specifics.

## Purpose

The focus of this project is the machine-generation of interoperable *codecs*
for arbitrary source-types, based solely on a value of type `'a Data_encoding.t`,
where `'a` is the OCaml type whose encoding-informed definition we are attempting
to replicate.

In particular, the focus of this project is not merely reverse-engineering an
equivalent-up-to-isomorphism typedef in OCaml, but rather to produce source-code
in arbitrary target programming languages, which are intended to yield
as-close-as-possible behavior to the source OCaml transcoding functions, modulo
language-specific limitations or requirements, as well as the inherent limitations
of certain `data-encoding` combinators or usage patterns to convey high-level
properties of a type, which would otherwise result in a more detailed implementation
if they could be statically introspected.

### Target Languages

The languages whose codec-generation are planned are as follows:

* `rustlang` : experimental but full-feature support, neither stagnant nor actively developed
* `typescript` : prototypical support, under active development
* `prose` : custom format native to this project, emitting descriptive text rather than code

In addition, it is worth mentioning that the original project this sub-project
is based on, `tezos-codec-compiler`, contains a somewhat-usable implementation
of a TypeScript codec-generator, and a heavily scaffolded but mostly-abandoned
prototype of a Golang codec-generator.

## Organization

The root directory of this project is divided into several sub-directories as follows:

* Documentation:
  * [`experience_src`](./experience_src): Implementation guide for future development
* Target-Language Runtime Libraries
  * [`rust_runtime`](./rust_runtime): Rust-specific library code
  * [`ts_runtime`](./ts_runtime): Typescript-specific library code
* Codec target directories:
  * `generated_rust`: target directory for Rust codec generation
  * `generated_prose`: target directory for Prose codec generation
  * `generated_typescript`: target directory for Prose codec generation
* `src`:
  * `rust`: Rust-specific codec-generation code
  * `typescript`: TypeScript-specific codec-generation code
  * `common`: Language-agnostic compilation layer and utility code
  * `virtual`: Non-code targets (most notably, [prose](#prose))
* `test`: General tests and hard-coded codec-generation modules
* `support`: Collection of watermarks of per-language support

## Prose

Prose is a custom format native to this project, which is used for human-readable descriptions of the
structural definition of an OCaml type's binary schema.

It is the primary reference language used by the utility script `whatchanged.sh` to compare cross-version
snapshots of schemas/codecs with the same identifier.

As the project evolves, a guide to Prose and how it is to be used and interpreted may be made available.

## Licensing

As the code in this directory is highly volatile and entire modules may be split or merged, or pruned entirely, it has not been
a priority to include the proper attribution and licensing disclaimers in the header of source-files in this sub-directory. Even if
not stated explicitly, and where not specified otherwise, code and other proprietary documents included in this repository are licensed under
an Open Source License (MIT) to ECAD Labs, Inc. (2022)
(previously to Nomadic Labs, Inc. (2021-2022) according to its former inclusion within the data-encoding repository).
