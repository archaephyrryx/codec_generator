# Target Implementation Guide

This document is a guide for extending the `codec_generator` project to support
new target languages. This guide is based primarily on the insights gained from
implementing the original target language, Rust. Furthermore, it is tied specifically
to the current implementation of the OCaml-based `codec_generator` project,
which may be updated in response to upstream changes in `data-encoding`.

## Terminology

This guide adopts the terminology used elsewhere in the `codec_generator` project, listed below:

- *Schema*: a description of the serial format of a particular type
  - Abstractly: model of the binary encoding used for a type
  - Concretely: value of type `'a Data_encoding.Encoding.t` for some OCaml
  type `'a`
- *Intermediate representation*: a value of type `Common.Simplified.t`[^1]
- *Codec*: a definition of a type and its (de)serialization to/from an encoding
format (binary)
- *Target* [*language*]: a programming language for which codecs can be produced
- *Transcoding*: either or both of serialization and deserialization

[^1]: As defined in [`common/simplified.mli`](src/common/simplified.mli).

While the this document will tend to adopt the terms 'serialization' and
'deserialization' over 'encoding' and 'decoding', note that the latter terms may
be used without distinction from the former.

### `data-encoding` Terminology

This document will also make direct use of
certain terms from the language of the `data-encoding`
library itself.

We refer to the following classifications of schema element:

- *Fixed*: serialized form occupies a constant number of bytes
- *Dynamic*: while the number of serialized bytes is not a known constant, the
final byte of the serialized element can be determined at runtime regardless of the context
in which the element appears
- *Variable*: the number of serialized bytes is not a known constant, and the
final byte of the serialized element can only be detected in the context of a
dynamic wrapper, or otherwise as the last byte of the binary sequence being
parsed

## Compiler Design

The `codec_generator` tool consists primarily of a compiler that executes two phases,
**simplification** and **code generation**.

In the first phase, schemas are converted into structurally analogous
intermediate representations.  This process is designed to be target-agnostic,
and should only ever need to be run once for a given schema, regardless of how
many, and which specific targets will be generated from its output.

Following this, **code generation** produces a *codec*, which may be an
in-memory string or an I/O directive.  The textual contents of this codec will
be source-code in the chosen target language. For all targets that are
programming languages, this will include at the bare minimum: a definition of
the root type of the originating schema; all logic necessary to serialize values
of that type; and all logic necessary to deserialize values of that type.

### Runtime Library

The Rust target implementation includes a library containing a large amount of
common utility code for concisely defining codecs. This approach was chosen both
to reduce the amount of boilerplate logic that would otherwise have to be
included in every codec module; as well as to provide a transparent and
externally visible codebase, which may be updated and reviewed even before any codec modules have been produced.
This library, tentatively called `rust_runtime`, is described in further detail in its local [README file](rust_runtime/README.md).

The role of this library is the following: to define sufficient logic for
serializing and deserializing the core primitive types used to model simple
schema elements; and to otherwise provide the necessary building-blocks to
extend this logic to user-defined (in this case, machine-generated) types.

### Target Language AST

As part of the target code itself, an AST module is recommended, as in the case of the
rust target, which defines [`ast.ml`](src/rust/ast.ml).

This module, written in OCaml, is intended to simultaneously model the language-specific
syntax of the target itself, as well as encapsulate some aspects of the extrinsic model
used for codecs, based on the specifics of the runtime library mentioned in the previous
sub-section.

It may not necessarily be obvious, upon cursory inspection, what parts of such a module
are inherent to the language, and which are engineered by the developer of the target runtime
in accordance with their own implementation details.

Beyond this, the AST module for Rust also contains its own pretty-printers, styled using
the [`Printer`](src/common/printer.mli) module included in this project. Such functions
take various elements of the AST and produce compound directives that can be used to
output the equivalent source-code snippets to either an OCaml string or an output channel,
which may be a standard output/error handle or a file handle.

### Generator Logic

The final piece of target support, once a runtime library and corresponding AST module have
been written, is the generator logic, which is located in [`generator.ml`](src/rust/generator.ml)
for Rust.

The key role of this module is to define the mapping from `Common.Simplified.t` to the highest
level AST node (`rust_module` in the case of Rust). This process will inevitably require the
definition of a broad range of subordinate modules and functions, which may be modeled after
the Rust target to the extent that the ASTs are comparable enough not to require comprehensive
redesign of the generation approach.

## Intermediate Representation

In order to describe the steps necessary for writing a novel language
target for `codec_generator`, as well as provide useful recommendations
and cautions as appropriate, it is first necessary to give an overview
of the intermediate representations that such a target must process.

This model is defined by [`simplified.ml`](../src/common/simplified.ml)
and described by the corresponding [`.mli` file](../src/common/simplified.mli).

### `Simplified.t`

In the simplification phase of codec compilation, `data-encoding` schemas
are converted into 'simplified' intermediate representation values, of type `Simplified.t`.

Beyond the immediate aspect of erasing the type-parameter of the original
GADT, this process also unifies several cases that are properly distinguished
in the source type, as well as unwrapping constructors that do not
affect how the enclosed schema element is to be transcoded.

It is worth mentioning that, as the simplification model evolved in tandem
with the Rust target implementation, there may be some aspects of the design
that are subtly conditioned for Rust-based codecs. This may take the form
of metadata that, while helpful or perhaps even essential to the Rust target
implementation, may be extraneous in the context of other targets; alternatively,
there may be certain details that would be relevant for non-Rust languages,
that are not included in the current design. In either case, however,
the impact of this co-evolution is expected to be minor, and as long as
the pre-existing target implementations are updated as appropriate, new
metadata may be added to the `Simplified.t` model in order
to support new targets that require them.

`Simplified.t` is currently defined as a four-variant ADT, encapsulating
sub-types of the following classes: base, composite, product, and sum.

#### `Base`/`base_t`

'Base' types are collectively those schema elements which are effectively
atomic, and cannot be reduced any further.

With the exception of the [`NamedRef`](#namedref) variant, all of these should directly
correspond to an existing type, with some variants possibly mapping to
the same underlying type, whether directly or through a type alias defined
in the per-target runtime library.

#### `Comp`/`comp_t`

'Composite' types represent schemas that are directly parametric over a single primary "element" or "payload" type, whose serialization and
deserialization are comprised of zero or more payload-invariant elements
and some number (possibly zero) elements of the payload type, in some order.

The only true exception to this rule, by a matter of technicality, is the
referentially transparent `comp_t` variant, `VPadded`, which does not signify
a schema-level wrapper, but rather an annotation within the compiler model
itself, which otherwise preserves the serialization and deserialization
of the type it is parametric over.

#### `Prod`/`prod_t`

'Product' types consist of records and tuples, which differ only in how
their constituent element values are organized: in tuples, argument types
are positional and anonymous, while records consist of named fields that
may be presented out-of-order, but are required to encode and decode in
the exact order in which they appear in the original schema definition.

A note worth mentioning is that, unlike base and composite types,
records and tuples are modeled in `simplified.ml` with an optional
field `id`, which holds a string to be used when a type-name has
to be used in place of an inline type-expression. Languages that
have native support for tuples may variously permit or disallow
inline tuple syntax in positions where type-expressions are expected,
and similarly for records/structs/objects. In the case of Rust,
records can only appear in the context of the definition of structs,
and therefore the substitution of a unique identifier is necessary
to extend support to cases in which records are elements in a larger
schema definition.

If a target language implementation is to make use of the `id` field,
it is important that any name-sanitization required happen both at
the original definition of the de-nested product type, and at all
sites where it is to be invoked, in a deterministic and fully
consistent fashion. This may include the replacement of characters
or sequences thereof that are either illegal or discouraged,
handling names that would conflict with language specific keywords
in the context where they are to appear, and concerns such as
capitalization and casing conventions (whether to use `snake_case`, `camelCase`, or some other conventional style). Furthermore,
if the name must appear in different styles, such as when
used directly compared to when it is used as a sub-string of
a larger identifier, the choice of how to reformat the
standard name must also be consistent between definitions and call-sites.

In the current implementation of `simplified.ml`, all identifiers are
produced based on the hierarchical context in which the element in
question is arrived at from the root type, or otherwise the closest
ancestor given an explicit name within the original schema definition.
These names may frequently be fairly long in the case of actual Octez
schemas, which often have multiple layers of nesting without any
named ancestors other than the root type. In such cases, the priority
is for both specificity and predictability, over brevity, given that
such names must be deterministic in order to be stable across compiler
runs, and should ideally correspond as closely as possible to the
known names defined within the hierarchy of schema nodes.

#### `Sum`/`sum_t`

'Sum' types encapsulate C-style `enum` definitions and discriminated
unions over variants with possible payload values.

As with `prod_t`, each of these cases is represented with an optional
`id` field for use when inlining of an anonymous type-union is not
possible, or is otherwise undesirable. As in the previous case,
these names are generated hierarchically based on the context
in which such elements appear, and must be converted into
inline identifiers for the target language in a consistent
and predictable fashion.

C-Style sum-types are annotated with the binary serialization width, as one of
`` `Uint8``, `` `Uint16``, or `` `Uint30``, which represents the numeric schema
type that values of the enumerated type will be regarded as for purposes of
transcoding. For sum-types with payload data (Data-enums), a similar such value is used for the tag-word, which can only be `` `Uint8 `` or `` `Uint16 `` (and not `` `Uint30 ``).

In both cases, the individual variants are provided as a list of an appropriate
type: `string_enum = string * int` for C-Style enumerated types, and `variant = int *
(string * Simplified.t)` for discriminated unions.
Note that in this design approach, there is an asymmetry in terms of what positional argument the numeric case-tag occupies (second for C-style, first for data).
Aside from historical reasons, this choice is justified by the common syntax for C-style enums, wherein the identifier for a case precedes its value; whereas for data-enums, it is natural to think of the discriminant as a prefix to the variant payload, and so it naturally comes first.

#### Simplification Metadata

In the process of simplification from a source schema to an intermediate representation, some information will inevitably be lost, as it either is unnecessary or impossible to preserve.

A key example of the former (extraneous metadata) is the distinction made between `Array` and `List` schemas, with
are unified into the composite-type variant `Seq`.

A key example of the latter (non-preservable metadata) is
any functional mapping over the parametric type of a schema,
primarily relating to the `conv` combinator (which deserves its own section).

As necessary, certain 'discoverables' (i.e. any information about a schema that
can only be computed on demand, and is not otherwise stored within its value)
are reintroduced to the raw output intermediate-representation by an
after-the-fact *annotation* sub-phase, which is not considered an independent
step or even described in most other documentation.

Currently, *annotation* performs a single task: detecting and appropriately
re-packaging certain schema elements with 'virtual padding' as described in
[that section](#vpadded).

Beyond this, the contextually-deterministic generated names used for the `id` field of product- and sum-types, as described in the appropriate preceding sections, also occurs during simplification; unlike 'virtual padding', however, this is done as part of the initial step of conversion, rather than a second pass.

#### Additional Remarks

In later sections, the individual cases encompassed by the above categories will
be described in greater detail. Note that it is possible and to be expected that
the design of the intermediate representation type will change.  In light of
this, it is recommended to consult the current implementation to verify any
possibly-obsolete remarks about certain aspects of the model. As existing target
implementations will naturally be updated to support a revised intermediate
representation model, consulting the code of existing generator/AST modules may
also prove useful.

## Basic Requirements

Though the exact nature of what steps must be carried out may vary drastically based on
the features of the language for which support is being added, the following list of steps
is more or less universal. The details of each step will be provided in following sections,
as well as an explanation of the schemas they encode, and any caveats based on their implementation
in Rust.

Note also that the order in which these items are provided may not directly correspond to the
natural order they should be approached in, and certain cases may require postponement to
have developed the tools necessary for supporting them as fluently as possible.

- [ ] Add support for Base schema types (as defined in `Common.Simplified`):
  - [ ] `Unit`
  - [ ] `Bool`
  - [ ] `Num`
    - [ ] `Int`
      - [ ] `NativeInt`
        - [ ] `Uint8`, `Int8`
        - [ ] `Uint16`, `Int16`
      - [ ] `Uint30`,`Int31`
        - [ ] `Int32`
        - [ ] `Int64`
      - [ ] `RangedInt`
    - [ ] `Float`
      - [ ] `Double`
      - [ ] `RangedDouble`
  - [ ] `Fixed`
    - [ ] `String`
    - [ ] `Bytes`
  - [ ] `Var`
    - [ ] `VString`
    - [ ] `VBytes`
  - [ ] `Zarith` types
    - [ ] `ZarithZ`
    - [ ] `ZarithN`
  - [ ] `NamedRef`
    - [ ] `Abstract`
    - [ ] `Concrete`
- [ ] Add support for Composite schema types
  - [ ] `Opt`
    - [ ] With one-byte tag
    - [ ] Without one-byte tag
  - [ ] `Seq`
    - [ ] `No_limit`
    - [ ] `At_most`
    - [ ] `Exactly`
  - [ ] `Padded`
  - [ ] Dynamic types
    - [ ] `Dyn`
    - [ ] `VPadded`
  - [ ] `Dft`
- [ ] Add support for Product schema types
  - [ ] Tuples
    - [ ] 2-tuple, 3-tuple, etc.
    - [ ] N-tuple
  - [ ] Records
    - [ ] 1-field, 2-field, etc.
    - [ ] N-field
- [ ] Add support for Sum schema types
  - [ ] `CStyle`
  - [ ] `Data`

### "Support"

The notion of 'adding support for X' is intentionally left vague in the previous
section, precisely because it varies on a case-to-case basis, as well as in terms
of what cases have already been 'supported' by the time a particular one is being
handled. At times, adding 'support' may be trivial, while in other cases, it may
require refactoring major runtime logic, re-engineering the AST, or otherwise
bridging large gaps between simpler and much more complex cases to handle.

It is also, as has been previously expressed, largely dependent on what features
the language in question offers. For example, composite types are far easier to
support in languages with native generics, while Sum-types are far easier to support
in languages with native (discriminated) type-unions or algebraic data types.

Furthermore, the presence or absence of class systems, inheritance,
calling conventions, type-associated methods, and other variable aspects
will determine what short-cuts and workarounds are respectively afforded
and necessitated by a target language, compared to the Rust implementation.

### Schema Types: Transient vs Static

In the implementation of the `Rust` target, virtually all types, whether
inherent to the runtime or defined in terms of the runtime API as part of
generated codecs, are sufficiently specific to indicate directly how they are to
be encoded and decoded.  This is on account of the flexibility of `Rust` in
terms of defining newtype patterns, and in general through the use of macros to
fill in the gaps in the language itself as far as encoding and decoding
default-valued fields and custom-tagged ADTs.  This also leverages the trait
system, type-associated methods, and polymorphism through generics, that are
part of the Rust language.

In target languages for which some of these features are not available, or
otherwise far less ergonomic to use as widely as in the Rust runtime, it may be
more sensible to forgo such transparent static types, and instead implicitly
handle the translation between the opaque in-memory type and the intended schema
model within the serialization/deserialization logic.

## Serialization/Deserialization Approach

This section will consist of an overview of the current model chosen for
serialization and deserialization in the Rust target runtime, which may
not be directly translatable into a different target language. Instead,
this will serve as a record of the evolution of the model for parsing
and writing values of progressively more complex types.

### Fixed-Length

During the initial stages of implementing the target runtime, only
fixed-length schemas will yet be supported, and no notion of sequential
values (such as linear collection types, or product types) will be
introduced.

During this phase, the requisite logic for parsing will largely
consist of:

- A type, either custom or language-native, holding the bytes to be parsed
  - Examples:
    - Rust: `Vec<u8>`
    - C: `((const char) **)`
    - Javascript, Typescript: `Uint8Array`
    - Python: `bytearray`
- A set of methods or functions for consuming `N` bytes from said type
  - Examples:
    - No-op:
      - `Unit`
    - Consume one byte
      - `Bool`, `Uint8`, `Int8`
    - Consume two bytes
      - `Uint16`, `Int16`
    - Consume four bytes
      - `Uint30`, `Int31`, `Int32`
    - `N` bytes
      - `String`, `Bytes`

In these cases, persistent state via mutability or multiple return including a
partially consumed parser, is not yet necessary.

It should be noted that if the byte-array being parsed contains fewer bytes
than are requested to be consumed, an appropriate error case or exception
should be returned or raised, ideally with some record of the circumstances
of the failure in question (such as the actual and expected lengths, as well
as possibly the contents of the insufficiently long byte-array).

## Base Types

The intermediate representation of `Common.Simplified.t` is currently split into
four variants, the simplest of which is `Base`. This includes the majority of types
one either expects to be native primitives of the language in question, or otherwise
require no direct reference to any other schema-type to generate code for.

We begin with the simplest-possible such type, `Unit`, which is encoded and decoded
as a zero-width element.

### `Unit`

Narrowing the scope of support initially to only include `Unit` schemas, the task
is very simple.

One need only define, or make use of a language-native, unit-type (namely, a type
which requires minimal, possibly zero bytes, to store in-memory, and has precisely
one possible value).

However one intends to define the logic of serialization and deserialization, whether
through OOP features such as classes/interfaces, as type-inherent methods, or as
standalone functions named or scoped appropriately to be associated with the type
definition they accompany, the implementation of the body of this logic is to do nothing:
there is not yet any need to define any reader or writer objects that one can consume
bytes from or append bytes to during deserialization/serialization, as reading and writing
`Unit` schemas is inherently a no-op.

The model devised for this case will inevitably evolve as various features are
required based on subsequently supported cases, but the inherent nature of the
task of serializing and deserializing unit will typically afford optimizations
compared to the boilerplate logic of later cases.

It may even be the case that implementing support for `Unit` is so trivial it
might make sense to defer it until a handful of slightly more interesting,
but not significantly more complex cases, have been handled, to avoid
the need to rewrite an otherwise trivial implementation. The only reason
to avoid delaying this is in languages that do not have a native equivalent
of `Unit`.

In point of fact, because `Rust` has a unit type `()`, support for `()` as
a codec type was not the first to be added ([`u8`](#uint8) was the first type for which
codec support was implemented).

### `Bool`

As it is very difficult to find a modern production language without a boolean
type, it is the expectation that adding support for boolean schemas will consist
merely of the serialization/deserialization logic for boolean values.

In `data-encoding`, the only legal byte-values for representing booleans
are `0x00` and `0xff`. Finding any other value in the position in which
a boolean is expected should be handled as appropriate on a per-language
basis, whether this is a return value indicating failure (like `Result` in Rust,
or `result` in OCaml) or a runtime exception.

In order to actually serialize and deserialize boolean values, it is actually
natural to begin by parsing `Uint8` and mapping the value, as appropriate,
into the boolean type, as in the pseudo-code below:

```pseudo
parse_bool(buffer) {
    let x = parse_uint8(buffer);
    if (x == 0xff) {
        return true;
    } else if (x == 0x00) {
        return false;
    } else {
        fail_invalid_bool(x);
    }
}
```

### `Uint8`

`Uint8` is the schema type of unsigned integers in the range `0` to `255`.
In C this would be `unsigned char`.

In adding support for handling `Uint8` schemas, the beginnings of a parser-like
model emerge. If this is the first type for which support is added, which is a
fairly reasonable design decision, then only one-byte parse-buffers will be necessary.
However, as it will inevitably be necessary to extend the length of such buffers
for `Uint16` in the immediate future, it is appropriate to leave the length of the
buffer abstract rather than eagerly cementing it to be one-byte.

At this stage, the decision of whether parsing returns the remaining bytes after
the consume operation, or mutates it (if such a choice is even sensible in the
context of the language, which may not support multiple-return, or alternately,
discourage mutability) can be deferred, as the expectation is that there is no
value, meaningful or otherwise, left over in the buffer after the single-byte
schema is processed.

Due to the byte-oriented design of the parser model, extracting the first byte
of a non-empty buffer and discarding the remainder is sufficient, at least at
this stage.

For serialization, merely appending the `Uint8` value as a raw byte to the
serialization target is sufficient as well. In practice, since this case is
likely to be handled well before `Seq`, `Tup`, or any multi-element cases, the
serialization target will almost certainly be empty when the `Uint8` value is to
be written to it, so allocating a fresh buffer containing just the `Uint8` value
is also a valid decision at this phase.

### `Int8`

`Int8` is the signed equivalent of `Uint8`, ranging from `-128` to `127`, equivalent
to `signed char` in C.

Parsing a value of type `Int8`, in most languages, will be as simple as reinterpreting
the value returned by parsing an `Int8` value, either through implicit type-coercion,
typecasting syntax, or through type-conversion functions or methods.

The same logic applies in reverse for serialization.

### `Uint16`/`Int16`

`Uint16` and `Int16` are 2-byte (16-bit) integral schema types whose values
range either from `0` to `65535` (for `Uint16`) or `-32,768` to `32,767`
(for `Int16`). In C these would be `unsigned short` and `short`.

Depending on the language itself, and the prototype model for the parse-buffer,
and also dependent on the endianness of the target architecture in rare cases,
parsing a `Uint16` or `Int16` value may be as simple as a pointer cast, or
require parsing either two `Uint8` values in succession or as an array,
and performing a big-endian conversion to the appropriately-signed 16-bit
value.

In turn, serialization would require a similar process, of either writing
to a pointer-type-cast target, or decomposing into individual `Uint8` values
and writing in succession.

### `Uint30`/`Int31`

In a slight departure from the expected succession of doubling bit-widths,
`Uint30` and `Int31` are respectively based on the memory representation
used for `int` on 32-bit OCaml builds, which reserves the highest bit to
indicate whether the remaining 31 bytes are boxed pointer or an unboxed
value. In this way, the sign bit may also be discarded to leave a 30-bit
unsigned integer, which is guaranteed to fit in the specified range due
to the two's-complement model for the implicitly signed OCaml `int` type.

Unlike the shorter integral types, `Int31` and `Uint30` will rarely be
native types in the target language, and must be implemented as part of
the runtime. Despite holding only 30 or 31 significant bits, they are
parsed as 4 full bytes, which may be invalid, unlike the other integral types.
In particular, if the highest bit of the most significant (first) byte
is set, or if either the highest or second-highest bit is set in the case
of `Uint30`, parsing should fail with an appropriate indication of the
reason for failure.

Otherwise, parsing an `Int31` or `Uint30` may be handled as a two-step (or
three-step) process: parsing an [`Int32`](#int32) value, and coercing it to the
appropriate range. As with [`bool`](#bool), an error value or exception should
be returned or raised if an invalid value is encountered, ideally indicating
what the value in question would have been, to facilitate debugging.

In the case of the Rust target implementation, the current approach taken for
`Uint30` and `Int31` is the definition of two type aliases, `u30` and `i31`
(after the standard notation for integer primitive types in Rust), which are
defined in terms of the model of [`RangedInt`](#rangedint) schemas.  This may
not be possible in other languages, especially those without generics (or, more
specifically, const generics), but those with both generics and type-level
literal integers (such as TypeScript) may be able to use a similar approach.

Otherwise, it ought to be possible to individually define types that represent
`Uint30` and `Int31` without generics, as these cases are far more commonly
encountered than any arbitrary partial-range integral types.

Care should be taken to ensure that the appropriate range-checking occurs
during at least deserialization, when potentially invalid values cannot be
precluded statically, and possibly during serialization as well, especially
in languages that allow for unchecked type-casting that, while potentially
more efficient, cannot preclude the possibility of invalid instances of
schema types that may require certain kinds of validation.

### `Int32`

While the primitive Ocaml `int` is defined as 31 bits for portability, on both
32-bit and 64-bit platforms, an `Int32` module is defined as well, which covers
the full range from $-2^{31}$ to $2^{31}-1$. This type is more likely to be
native to the target language than `Int31`, and given that all possible 4-byte
binary sequences are legal values of `Int32`, there is no need for the kind of
validation that is required for `Int31` and `Uint30`.

As with `Int16`, either pointer type-casting or big-endian conversion of four
individual bytes are suggested approaches for parsing `Int32`, with the reverse
process for serialization.

In implementations that handle `Int31` and `Uint30` by first running the parser
for `Int32`, it may be possible to short-circuit the conversion by checking the
highest bit(s) of the first byte as appropriate, to detect invalid binary
sequences early. This approach is not taken in the Rust implementation, due to
the fact that `u30` and `i31` are aliases for a generic type that cannot yet be
specialized for this particular validation check.

### `Int64`

As with `Int32`, `Int64` (64-bit integer schema type) is a fairly standard type
in general applications, and is therefore likely, if not already included as a
primitive type, to be part of a fairly ubiquitous standard or open-source
library.

As the only application in which 8-byte integral types occur is directly as
`Int64` (`RangedInt` is at most a 4-byte schema), it is not anticipated that
anything beyond the basic serialization/deserialization logic for 64-bit
integers need be defined as part of the runtime.

### `RangedInt`

The `RangedInt` schema type is very likely to be novel to most languages,
with varying degrees of ease-of-definition depending on language features.

It represents an integer value $x$ constrained to a particular range
$x \in [\mathtt{MIN},\mathtt{MAX}]$, for the schema-level bounds
$-2^{30} \le \mathtt{MIN} \le \mathtt{MAX} \le 2^{30} - 1$[^3]

[^3]: In practice, even in the rare cases in which arbitrary `RangedInt`
schemata are encountered, it is very rare that `MIN == MAX` will ever be the
case, as this type would effectively be a constant-valued singleton.

The integer type used to model a RangedInt schema is decided
during the simplification phase, and should always be the same choice as
`data-encoding` itself makes. This decision is not binding at the runtime representation level for the target language, as not all languages distinguish between the variously-sized integer types. However,
during
serialization/deserialization, this choice must be
respected.

The implementor of a target language may assume that
the chosen representative integer width/signedness is
sufficient to express the full range from `MIN` to `MAX` (in terms of cardinality, rather than transparent equivalence)
with the following additional properties:

- If `MIN < 0`, the chosen representative will be signed (`Int8`, `Int16`, or `Int31`)
- If `MIN >= 0`, the chosen representative will be unsigned (`Uint8`, `Uint16`, `Uint30`)

#### **Serialization/Deserialization**

A `RangedInt` with a value of $x$ for $\mathtt{MIN} \le x \le \mathtt{MAX}$ is encoded
transparently as its representative type,
provided that $\mathtt{MIN} \le 0$.
That is to
say, aside from questions of range-validation, a `Uint8`-backed
`RangedInt` schema is encoded, and decoded, identically to a `Uint8`
value for non-positive minimum bounds.

When $\mathtt{MIN} > 0$, a different approach is taken
entirely. During deserialization, a sequence of bytes $[b_0b_1\ldots\ b_{N-1}]$ in a position reserved for a
`RangedInt` value with a backing integer type `U` with a byte-width of `N`,
represents a numeric value equal to $\mathtt{MIN} + \mathrm{decode}_\mathtt{U}([b_0\ldots\ b_{n-1}])$. Similarly, during serialization, the value written
for $x$ of schema type $\mathtt{RangedInt}_{[\mathtt{MIN}, \mathtt{MAX}]}$ for $\mathtt{MIN} > 0$ will be
$\mathrm{encode}_\mathtt{U}(x - \mathtt{MIN})$.

Care must be taken to distinguish, both in documentation and in actual
implementation, between the 'raw' value (in-memory representation) of a
`RangedInt` type with `MIN > 0`, and the numeric value it represents. This
distinction is especially important when the range `[MIN, MAX]` falls either
partially or entirely outside of the range of values representable by the
backing integer type, such as (in the Rust runtime syntax) `RangedInt<u8, 1024,
1025>`, which is represented in memory as $\{ 0\mathtt{u8}, 1\mathtt{u8} \}$,
but which signifies the set of values $\{ 1024, 1025 \} \nsubseteq \mathtt{u8}$. In the case of Rust, this mandates a completely different type signature for the numeric value and the raw value accessors of the `RangedInt` type.

As mentioned in the sections on [`Uint30` and `Int31`](#uint30int31),
it is similarly expected that an error is returned, or an exception raised, when
an illegal value of a `RangedInt` type would be instantiated, either in
deserialization from a binary sequence that holds an out-of-range value,
or as part of the runtime API that allows for the construction, coercion,
or conversion from other types to `RangedInt`.

#### Language-Dependent Strategies

The implementation of `RangedInt` in the Rust target
runtime relies on both generic types and const generics,
specifying both the representative type that informs
serialization/deserialization, and the two constants `MIN`
and `MAX` used for the bounds.

In other languages, it may be necessary to use template code
such as interfaces, abstract classes, pre-processor macros,
or language-native template syntax, to reduce
the overhead of defining novel types for each
`RangedInt` instance in a type, aside from `Uint30` and `Int31` if they are modeled in this fashion.

Otherwise, it may be necessary to generate boilerplate code for each unique
`RangedInt` type appearing in a codec module, which should not impose too
considerable an overhead due to the relative rarity of `RangedInt` in encodings
within the Octez ecosystem, at least as of the time this guide was written.

#### Bounds Sanity

Beyond the basic validation necessary to ensure that a candidate value
of a `RangedInt` codec is within the required range, it may, depending
on language facilities and the specifics of the implementation, be
further necessary to verify the inequality $-2^{30} \le \mathtt{MIN} \le \mathtt{MAX} \le 2^{30} - 1$. If this is not met, the type in question
is fundamentally invalid, and it is up to the runtime to ensure that
such cases are properly reported to the code responsible for requesting
such an incoherent type instance.

### `Double`

In `data-encoding`, all floating-point numbers are IEEE-754 double-precision (`binary64`/`FP64`).

As with `Int64`, this type is expected to be readily available in almost all modern production languages, either natively or through standard libraries.

The encoding and decoding of such values is directly adherent to the binary format of the standard in question.

### `RangedDouble`

`RangedDouble` is an analogue to [`RangedInt`](#rangedint) over double-precision floating point numbers; namely, such a schema
is parametrized over two constant values `MIN` and `MAX`, and
represents all `FP64` values $x$ where $\mathtt{MIN} \le x \le \mathtt{MAX}$.

Unlike `RangedInt`, however, the representative type is
always `FP64`, and there is a transparent equivalence
between the numeric value and the proxy `FP64` value
of $x$ used for serialization/deserialization purposes,
irrespective of the values of `MIN` or `MAX` (provided
that $\mathtt{MIN} \le \mathtt{MAX}$ and neither are any form of `NaN`).

In that respect, serializing and deserializing a `RangedDouble` codec should
consist only of two operations, in the appropriate order: checking that the
value in question is in range (and perhaps also that the range itself is valid
at all); and calling the appropriate serialization/deserialization operation on
the `FP64` value that is intended by the value in question. Any destructing or
constructing based on the language in question and the definition of
`RangedDouble` are considered an implicit third step, when applicable.

As with `RangedInt`, a runtime implementation of support
for `RangedDouble` codecs may be highly dependent on the features
available in the targeted language. In Rust, the use of const generics enables type-level parametric specification of the `MIN` and `MAX` bounds (with a certain amount of indirection due to the present ineligibility of `f64` as a const generic), which allows
for blanket definitions for the methods and traits implemented for all possible instantiations of the type.

In other languages, template syntax, interfaces or abstract classes, or other
boilerplate-reduction measures may be the most viable alternative when such
type-level generics are not available. Failing this, the rarity of
`RangedDouble` as a component of real-world (Octez) schemas means that, if the
only option is to generate the necessary function and method definitions on a
per-instance basis within the codec module itself rather than in the runtime,
this should not impose too much of a burden, provided that the design is tested
thoroughly enough that any potential bugs are detected early, as very few
real-world schemas, if any, will provide live examples suitable for testing
purposes.

### `String`

As currently implemented, the `String` variant defined in `Common.Simplified`
corresponds not to variable-length strings, but to strings of explicit,
constant length. This is signified by its inclusion under the heading of
the ADT `fixed_t`, and the presence of an `int`-typed argument.

All cases listed up until this point have been of known, fixed binary length,
and so it is expected that the framework developed to handle such cases will
by now be sufficient for support of fixed-length strings to be fairly
trivial.

That being said, there are several points worth raising at this stage.

The first point is that, as this schema is modeled after OCaml strings,
which are only advised, but not required, to adhere to Unicode (UTF-8)
conventions. Otherwise, they can hold arbitrary byte sequences, including
'characters' that might be reserved or illegal in the contexts in which
they appear; for example, it is perfectly possible for a null byte to
appear within the body of a `String` codec value, even though this is
not portable with `C`, in which a null byte, by convention, terminates
the payload of a string-like `char *` and would cause issues if embedded
directly without other signals of end-of-string.
However, it is expected that at the very least, the byte-sequence
for a `String` will be valid as a UTF-8 formatted Unicode string,
which may either be assumed or explicitly checked by the implementation.

The second consideration is that, depending on the target language in
question, it may not be directly possible to encode the length of a
string, even as a raw byte-array, at the type-level. If fixed-length
arrays are native to the language, something approximating the definition,
in the Rust target should be opted for, even if generics are not available
and other means of avoiding duplicate boilerplate must be used instead.

In Rust, the definition used for this case is currently modeled either
directly, or analogously, to the following:

```rust
pub struct FixedString<const N: usize> {
    val: [u8; N]
}
```

In this case, the length of the string is preserved at the type level.  Due to
the fully transparent nature of serializing and deserializing what is already a
byte-array at the type level, it may also be possible to inline the definition
as a raw array, though that decision may have unforeseen consequences in terms
of the distinction between `String` and [`Bytes`](#bytes). In the case of the
Rust target language, this distinction was preserved through the use of newtype
patterns, in which separate struct definitions are used to model the distinct
schema types.

### `Bytes`

Though superficially similar to `String`, the fixed-length codec
`Bytes` is specifically intended for uses that is not intended
to be interpreted or viewed as character-oriented data, but
rather preserved as a raw sequence of `N` bytes. Example cases
for this schema include hashes, signatures, public keys,
and other fixed-length cryptographic data. It may also be used
for specific Octez identifiers for branches, protocols, and
so on.

As with `String`, `Bytes` is a fixed-length type, and can
be serialized and deserialized transparently, even more so
than `String`, as there is no form of value restriction
on its contents beyond the exact length requirement.

### `VString`

`VString` is a variable-length analogue for `String`, and the
same caveat about the binary contents apply. Beyond that,
the main point worth mentioning is that, while serialization
is trivial given the runtime-known length of the value being
encoded, deserialization will implicitly consume the entire
buffer. This detail may require minor tweaks to the parsing model
compared to the previous cases, all of which have been fixed-length.

As with `String` and `Bytes`, it is advised that some facility should be
provided for implementing different methods for `VString` and `VBytes`, given
that it is sensible to attempt to convert the former to a character-based
string, while the latter is usually either left raw, or encoded in a custom
format.

It is important to note that, while most often string-based schema elements will
be enclosed in either a `Dyn` or `VPadded` wrapper, `VString` is the inner type
in this equation, and should be handled as such.

### `VBytes`

`VBytes` is to `Bytes` as `VString` is to `String`, and by this point
there should be a robust enough framework to support `VBytes` without
any revisions to the parsing or encoding model.

As with `VString`, deserialization must, at this point, implicitly
consume the entire buffer, while serialization writes as many bytes
as are stored in the value being encoded.

### `ZarithZ`/`ZarithN`

The `ZarithZ` schema type models arbitrary-precision
integers, as defined in the OCaml package `zarith`,
a dependency of `data-encoding`. The `Z` suffix indicates
it is used to model all integers $\mathbb{Z}$, including
negatives.

The schema type `ZarithN` is an analogous type restricted
to only (non-negative) natural numbers, and has a similar transcoding strategy.

#### Preamble: Deserialization

For deserialization, there are two suggested approaches, either of which can be
chosen without much consequence beyond portability and flexibility of the
runtime code.

The first approach is the one used in the Rust target runtime. It uses a two-pass
algorithm, dividing the work between the type-agnostic parser logic and the type-aware
deserialization logic.

In the serialized forms of both `ZarithZ` and `ZarithN` values, every byte but
the final will have the high bit set (`0b1xxxxxxx`) and the final byte will
always be the first whose high bit is unset (`0b0xxxxxxx`). Based on this
observation, the only information that is absolutely necessary within the parser
logic, is the byte-level predicate to terminate upon fulfilling (inclusive of
the byte for which the predicate held). In this way, only one parser method is
required between `ZarithN` and `ZarithZ`, as the predicate they terminate on is
identical, and a raw byte-vector can be yielded early by the parser for later
type-aware post-processing.

The second approach would be to specifically design two parser methods that
yield the model types for `ZarithZ` and `ZarithN`, based on the minor
differences between how the bytes of each schema type are to be interpreted.
This may be more performant by varying margins, and avoids reliance on closures
or functional arguments (which may not be available in certain langauges);
furthermore, it eliminates the generic nature of the modular approach described
above, which may lead to further optimizations within the compiler or
interpreter beyond merely unifying the algorithm of consuming bytes and
calculating the ultimate value from two passes into one.  However, it inevitably
hard-codes the choice of arbitrary-precision integer/natural type into the
parser, which may also determine what operations are available to reconstruct
the value during deserialization; furthermore, it may turn out that the most
efficient means of decoding the numeric value from the byte-sequence, relies on
having all of the bytes already available in memory, in which case there is
little added advantage over the first approach.

#### Deserializing `ZarithZ`

Due to the nature of the encoding scheme adopted for Zarith integers
in `data-encoding`, it is somewhat more natural to describe the
deserialization strategy first.

Within the `data-encoding` library, the following encoding format is
used to represent `ZarithZ`, written as a series of bytes decomposed into bitmasks:

```text
zarith_z_bits := 1Sxxxxxx (1xxxxxxx)* 0xxxxxxx
               | 0Sxxxxxx
```

or as the pattern `LSxxxxxx (Lxxxxxxx)*`.

In this notation, the highest bit of each constituent byte,
`L := 0 | 1`, is a flag used to indicate the final byte of the serialized `ZarithZ` value: so long as the high bit is set, the following byte is a continuation of the serialized `Zarith` integer; and as soon as it is found to be unset, no further bytes remain.

A special bit-flag `S := 0 | 1` is additionally reserved in the first byte only, and indicates whether the original integer was positive or negative. Note that regardless of this value, the payload bits of each byte, whose significance is described below, encode the binary of the **absolute value** of the source integer.

The lower 6 bits of the first byte, and the lower 7 bits of any subsequent bytes in the serialized form, are a little-endian encoding of the binary of the absolute value of the integer. For example, the byte-sequence

```text
0: 10ab cdef
1: 1ghi jklm
2: 0nop qrst
```

would correspond to the binary-notation integer

$+(nopqrst\;ghijklm\;abcdef)_2$.

That is to say, taking the payload bits of each byte
in identical intra-byte bit-order, but reversed byte
order, will reconstruct the binary form of the encoded
number.

In terms of a desrialization algorithm, the following approach is taken in the
Rust runtime, based on the operations available in the `num_bigint` crate used
as the standard supplier of arbitrary-precision integers:

```rust
// This is to be treated as Rust-style pseudocode rather than an actual code snippet
fn z_from_bytes(bytes: impl IntoIterator<Item = u8>) -> Z {
    // Boilerplate code
    let mut bytes = bytes.into_iter();

    // ALGORITHM BEGINS

    // Read the first byte
    let first = bytes.next().unwrap();

    // Record the sign-bit
    let sign : Sign = if (first & 0x40u8) != 0 {
        Sign::Minus
    } else {
        Sign::Plus
    };


    // Preserve the (lower 6) value-bits
    let low6 : u8 = first & 0x3fu8;

    // Stores the remaining bytes, without the high bit, in a vector
    let low7s : Vec<u8> = bytes.map(|b| b & 0x7f).collect();

    // Read low7s as a little-endian integer in base 128 (7 bits)
    match BigUint::from_radix_le(&low7s, 0x80) {
        Some(mut abs) => {
            // shift up and re-include the 6 least significant bits
            abs <<= 6;
            abs |= BigUint::from(bot6);
            // build an integer from the absolute value and sign, and cast it to Z
            Z::from(BigInt::from_biguint(sign, abs))
        }
        None => unreachable!(),
    }
}
```

#### Serializing `ZarithZ`

As described on the section on deserialization, Zarith arbitrary precision
integers are encoded as a series of bytes whose high bit is set (i.e.
`1xxxxxxx`) for all but the final byte written, and whose remaining 7 bits
encode the binary representation of the absolute value of the integer in groups
of 7. As an exception to this, the second highest bit in the first byte is
reserved for the sign of the integer, and is masked as `x1xxxxxx` when negative,
`x0xxxxxx` when non-negative.

These groups of 7 bits (6 for the first) are begin with the lowest 6 bits of the
absolute value, and proceed in order of ascending significance in ascending
significance (little-endian).  Namely, the LSB of the first byte in the
serialized form equal to the LSB of the absolute value of the argument integer,
and the MSB is the highest set bit in the final byte, which will have the high
bit unset.

Care should be taken to ensure that no 'leading zeroes' are appended, namely any
sequence of bytes `( 80 )* 00` (shown in hex). The only exception to this is
when the integer being encoded is $0$, in which case the proper encoding is the
single-byte `0x00`. A curious property of this encoding is that the numeric
value $0$ may be represented either as $+0 \mapsto \mathtt{0x00}$ or
$-0 \mapsto \mathtt{0x40}$, though when serializing a $0$ value, the former is to be
preferred in all cases for reasons of consistency.

A sample implementation in Rust is given below for the serialization logic
described above:

```rust
fn serialize_z(x: Z) -> Vec<u8> {
    // Decompose x into its sign and absolute value
    let (sg: Sign, mut abs: BigUint) = BigInt::into_parts(&x.into());

    // we shift to align the second byte on with the correct 7-bit chunk
    abs <<= 1;

    let mut bytes = abs.to_radix_le(0x80);

    match bytes.first_mut() {
        Some(first) => {
            *first_byte >>= 1u8;
            match sg {
                Sign::Minus => *first_byte |= 0x40u8,
                _ => {}
            };
        }
        _ => unreachable!()
    }

    match bytes.last_mut() {
        Some(last) => *last ^= 0x80,
        None => unreachable!(),
    }

    for byt in bytes.iter_mut() {
        *byt ^= 0x80
    }

    bytes
}
```

#### Deserializing `ZarithN`

Compared to `ZarithZ`, the natural number schema `ZarithN` is simpler to
serialize and deserialize. It adopts the same general strategy, with
the removal of the sign-bit in the first byte, meaning that all bytes
of a `ZarithN` value are equal in terms of how many value-bits they contain.

As before, a sample of Rust code based on the deserialization logic within
the target runtime implementation is given below:

```rust
// This is to be treated as Rust-style pseudocode rather than an actual code snippet
fn n_from_bytes(bytes: impl IntoIterator<Item = u8>) -> N {
    // ALGORITHM BEGINS

    // Stores the sequence of bytes, without the high bit, in a vector
    let low7s : Vec<u8> = bytes.into_iter().map(|b| b & 0x7f).collect();

    // Read low7s as a little-endian integer in base 128 (7 bits)
    match BigUint::from_radix_le(&low7s, 0x80) {
        Some(nat) => {
            N::from(nat)
        }
        None => unreachable!(),
    }
}
```

#### Serializing `ZarithN`

The process of serializing `ZarithN` is identical to that
of `ZarithZ`, but without preprocessing of the low 6 bits
to insert the one-bit sign-flag before the main loop described in the relevant section:

```rust
fn serialize(&self) -> Vec<u8> {
    let mut ret = self.0.to_radix_le(0x80);

    match ret.last_mut() {
        Some(last) => *last ^= 0x80,
        None => unreachable!(),
    }

    for byt in ret.iter_mut() {
        *byt ^= 0x80
    }

    ret
}
```

Alternatively, if there is an available function and method that can produce a
byte-array from $x$ in a base-128 decomposition, this can avoid the need for a
potentially expensive loop; once this array is produced, all that need be done
is to flip the high bit of all elements but the final, which can also be
achieved by masking all of them with `0b10000000` unconditionally, and toggling
the high bit of the final byte independently to restore it to `0`; in this way,
it may be possible to avoid per-iteration finality-testing or other overhead
inherent to conditionals.

### `NamedRef`

In the course of deconstructing a sufficiently complex
schema, it may be necessary to introduce layers of 'de-nesting', or indirection. These are represented
as base-type variants `NamedRef of string * ref_t`, where

```ocaml
type ref_t = Concrete of t
           | Abstract
```

(In this case, `t` refers to the top-level `Common.Simplified.t`)

The difference between the two cases is explained below

#### Concrete References

A 'concrete' `ref_t` is one that includes the original definition for the type signified by the associated name.
These are generally produced in the process of de-nesting tuples, records, and sum-types that appear embedded in a larger
surrounding structure.

Specifically, whenever it may be necessary, in an arbitrary
target language, to refer to a type by name rather than
by its actual structural definition, a `NamedRef` will
typically be used.

For more details on the suggested approach for handling
concrete `NamedRef` values, see the section on [Concrete Reference Resolution](#concrete-reference-resolution).

#### Abstract References

'Abstract' references correspond precisely and exclusively to auto-referential or co-referential types embedded within recursive schemas via the `Mu` variant and corresponding `mu` combinator.

As they refer to the contained self-type of a recursive definition, some layer of language-specific indirection
may be required. Otherwise, due to the unavailability
of a definition to substitute, the (sanitized) name
provided in the original `NamedRef` is to be inserted
opaquely.

##### Self-Referential Types

In Rust, almost all user-defined data types will tend to
implement the auto-trait `Sized`, which requires a fixed-width in-memory representation, and is implicitly mandated on all return types and direct argument types of functions.
In light of this, auto-recursive and mutually recursive
types cannot be defined without indirection via heap-allocated
smart pointers around the self- or co-referential element.
This is typically achieved through the `std::boxed::Box` type
in the Rust standard library; in the case of the runtime library included in this project, the custom definition
`AutoBox` is used instead.

Similar approaches may be required in other languages that do not have implicit
boxing, including many languages in the C family, in which pointers will
typically be used to stand in for a directly embedded auto-recursive or
co-recursive field. Certain functional languages such as OCaml and Haskell, on
the other hand, may allow for recursive types; in the case of OCaml, however,
mutually recursive types must be defined in a single clause, via the syntax

```ocaml
type foo = FooA of bar | FooB

and bar = BarA of foo | BarB
```

It is also possible that for certain languages, even those that allow for
directly self-referential types, certain keywords may be mandatory. The converse
is true in `OCaml`, where only `type nonrec` is required to be explicit, while
potential recursivity in types is the default behavior without the `nonrec`
keyword.

## Composite Types

### `Opt`

The `Opt` composite type corresponds to the specification
of optional fields in the source schema, and encloses
the nominal type the field would take, when provided.
As such, this case will be found exclusively to the context of
records, even though this may not have any direct consequence on how support is to be added to handle it.

One key property of `Opt` is that it is designed only to accept argument types that are not zero-width in binary serialization form. Therefore, it is safe to assume that
values such as `null`, `undefined`, etc. will not appear
as a legal value of the actual element, and can be
unambiguously used within the per-target model for optionality
of types.

#### Tagged vs Untagged

In most cases, the serialization strategy for an OCaml `a option` is the following:

$\mathrm{serialize}_{\mathrm{Opt}(T)}(x) \coloneqq \begin{cases}
\mathtt{0x00} & x \sim \mathtt{None} \\
\mathtt{0xff} \oplus \mathrm{serialize}_{T}(y) & x \sim \mathtt{Some} \; y \\
\end{cases}$

In OCaml (semantic specification, suboptimal performance):

```ocaml
let encode_t : t -> Bytes.t = (* ... *)

let encode_opt_t : t option -> Bytes.t = function
  | Some y -> Bytes.cat (encode_bool true) (encode_t y)
  | None -> encode_bool false
```

For deserialization purposes, it is then only necessary
to read the first (prefix) byte of the composite value
as a raw byte or a boolean,
and determine whether a field-value follows: if the byte is `0x00` (`false`), nothing is left to be done, and any subsequent schema elements can be deserialized immediately; if it is `0xff` (`true`), the field-type value is to
be deserialized as normal, as if the field were non-optional
and the `0xff` byte had been elided, with the possible requirement of wrapping the resulting value in the language-specific equivalent of the `Some` variant constructor.

For example, we might write, in Haskell:

```haskell
import Data.Bifunctor (first)
import Data.ByteString (ByteString, uncons)
import Data.Functor ((<$>))

class Deserialize a where
  parse :: ByteString -> Either String (a, ByteString)

instance (Deserialize a) => Deserialize (Maybe a) where
  parse = \bytes ->
    case uncons bytes of
      Nothing -> Left "Unexpected EOB"
      Just (0x00, rest) -> Right (Nothing, rest)
      Just (0xff, rest) -> first Just <$> parse rest
      Just _ -> Left "Invalid option-flag"
```

However, there also exists a less-common variety of optional fields, which are
only distinguished in how they are serialized and deserialized:

$\mathrm{serialize}_{\mathrm{VarOpt}(T)}(x) \coloneqq \begin{cases}
\epsilon & x \sim \mathtt{None} \\
\mathrm{serialize}_{T}(y) & x \sim \mathtt{Some} \; y \\
\end{cases}$

We use the name `VarOpt` in the previous listing only
as a means of distinguishing the two flavors of
optional-field encoding. In the actual definition
of `Simplified.comp_t`, we have the unified variant
`Opt of opt_t` that is used in both cases with

```ocaml
type opt_t = { has_tag : bool; enc : t }
```

(where again, `t` refers to `Simplified.t`)

When `has_tag` is true, the default encoding
(with the one-byte tag) is mandated; otherwise,
the schema type provisionally named `VarOpt` is
indicated.

In terms of deserialization untagged optionals can
only typically be defined within the full context
of the parser implementation. While other approaches
may be possible, the one suggested below is a
distillation of the approach used in the Rust target
runtime. It omits several details about the Rust
parser that may be necessary to understand the
pseudocode given. However, for the sake of generality,
the exact means by which certain predicates are determined,
will be left to the discretion of the implementer:

```text
parse_untagged_option<T>(buffer):
    determine how many bytes are available to consume
    if 0:
        do nothing to buffer
        return equivalent of None
    else:
        attempt to parse a transparent T value from buffer
        if successful:
            yield result after wrapping in equivalent of Some
        else:
            propagate failure into caller, in appropriate form
```

The exact meaning of 'available to consume' is dependent
on the target implementation, but refers in abstract
to dynamic parse-contexts (cf. [Dyn](#dyn) and [VPadded](#vpadded)).

#### Option-Type Models

Depending on what features a target language has, several approaches are
possible for defining either a polymorphic/generic option-type in the runtime, using an existing option-type,
or simulating option-types with [transient](#schema-types-transient-vs-static) codec typing
and option-aware transcoding functions.

If the language already has native option-types, under
any name, those may be used directly, e.g.:

- OCaml: `'a option`,
- Rust: `Option<T>`
- Haskell: `Maybe a`

If the language does not have such a type already defined, but can allow for a
suitable generic definition, one of the following internal models may be used to
define an effective option-type (depending on what is possible based on
language-specific feature-sets). Note that the examples provided are only
suggestions of how one might go about defining such a model, and do not
constitute a recommendation of one model over any other. In certain case, the
programming language the sample is written in, may be one in which a different
model is strictly better; the languages used are chosen based on familiarity and the language feature or property
that informs the particular choice of model, and not because
it is even necessarily a language for which the example
definition is sensible.

##### Null-Free

In certain languages, null-values, null-pointers, and null-types may either not exist, be unsafe or difficult to use, or their use may be discouraged whenever they are not mandated for a particular use.

Several possible null-free implementations of option-types are given below:

###### Empty/Singleton List

In languages that support list/array types whose length is not determined at the type-level, it may be possible (with care to avoid conflicting with other uses of list as models of schema types) to model option-types as lists whose element is the original value-type of the optional field:

```typescript
// TypeScript
export class Opt<T> {
    public static Some<T>(value: T): Opt<T> {
        return new this([value]);
    }

    public static None<T>(): Opt<T> {
        return new this([]);
    }

    private constructor(private readonly value_store: T[]) { }
}
```

```python
# Python:
class Opt:
    def __init__(self, val = None):
        if val is not None:
            self.store = [val]
        else:
            self.store = []
```

###### Custom ADT

(Both listings given below are in Haskell)

```haskell
-- polymorphic version
data Option a = Some a | None
```

```haskell
-- monomorphic version
data OptionFoo = Some Foo | None
```

##### Union with Null-Type

For a union with a null-type, either a tagged or untagged approach may be alternately preferable. In terms of representing both normal `Opt` and untagged `Opt`,
both models may be simultaneously desirable, letting
the presence or absence of a boolean flag stand in
for the inclusion or omission of a discriminant byte.
Conveniently, the nature of tuple serialization
actually means that using these approaches more or
less transparently achieves the desired transcoding
semantics automatically.

```typescript
export type UntaggedOption<T> = T | null;

export type Option<T> = (bool, UntaggedOption<T>);
```

##### Untyped Null-Value

In certain languages, there may be a conceptual 'value', perhaps a language-level token or builtin definition,
that is allowed to inhabit all types. This might
be called `null`, `None`, or `undefined` variously,
though other names are possible. Alternatively,
this can be achieved in language with native support
for specifying record or class fields as being
optional.

Depending on whether this value can be safely introspected,
type-level unions are not necessary:

```typescript
export class Option<T> {
    constructor(private readonly value?: T) {}

    public is_none(): boolean {
        return (this.value === undefined);
    }

    public is_some(): boolean {
        return !(this.is_none());
    }
}
```

```python
class Opt:
    def __init__(self, val = None)
        self.val = val

    def is_some(self):
        return self.val is not None

    def is_none(self):
        return self.val is None
```

In some languages, it may be unsafe, unsound, or
impossible to determine whether a value is
undefined. In this case, it may be necessary
to include a boolean flag that indicates whether
the stored value is defined:

```haskell
data Opt a = Opt { defined: Bool, value: a }

is_some :: forall a. Opt a -> Bool
is_some (Opt { defined, ~value }) = defined

is_none :: forall a. Opt a -> Bool
is_none = not . is_some
```

```rust
pub struct UnsafeOption<T>(bool, std::mem::MaybeUninit<T>);

impl<T> UnsafeOption<T> {
    pub fn is_some(&self) -> bool {
        self.0
    }

    pub fn is_none(&self) -> bool {
        !self.0
    }
}
```

##### Null Pointer

In certain languages, it may be simplest to represent
option-types using pointers to the original field-type,
using the null-pointer to represent the `None` case.

```C
typedef struct OptFoo { foo_t *val; } optfoo_t;
```

### `Seq`

The `Seq` composite type represents the equivalent of OCaml `list` or `array` as a unified meta-type. Most production
languages are expected to have some sort of list-like type, namely a collection of zero or more elements of a single type, with a notion of index that maintains the original order of the elements in question.

#### Length-Limits

There are three varieties of `Seq` schema, specified at the
value level rather than the type level through the following definition in `simplified.ml`

```ocaml
(* originally defined in Data_encoding.Encoding, but not exported *)
type limit = Data_encoding__Encoding.limit =
    | No_limit
    | At_most of int
    | Exactly of int

(* ... *)

type seq_t = { elt : t; lim : limit }
```

For each case of `limit`, a different set of assumptions
are made, and subsequently enforced, regarding the
**number of elements** (and not the number of bytes)
in the sequence.

##### `No_limit`

The number of elements in the sequence is not restricted at the type level, and can be arbitrarily
long provided that it does not violate any limits
on serialized byte-length (note that such considerations
are either enforced at a different level, or not at all).

Certain target languages may require arrays to have
constant size at the type level; in such cases, a suitable
'vector' type should be used, when available. This may
be obviated in languages where array/list types either
do not require, or do not naturally indicate, their
overall length at the type level.

##### `At_most N`

Although not precluded by typing, the integer argument of `At_most` must be non-negative, and determines the maximum
legal number of elements in the list. This is to be verified
during serialization of potentially oversaturated sequences,
and should either be enforced or assumed during deserialization, depending on how much external knowledge is available in the parsing context.

Any number of elements between $0$ and $N$ (inclusive of both) is allowed for a value of such a sequence element.

In the rare case that `N == 0`, the sequence is implicitly zero-length; it is not expected that any such case will occur in real-world (Octez) schemata, but if it is easier
to allow than to preclude this case, there is no reason
to enforce this expectation.

Depending on target language, it may either be natural
to define this using an N-element constant array
of possibly uninitialized values, that either
explicitly or implicitly records its actual length
to avoid overrun or invalid access (if such a type
is not available through an external or native library,
it may be reasonable to define it). In other cases,
it may be more natural to use type-level arithmetic
or type-level integers to annotate an unbounded
list/vector type with the number of elements
it can maximally contain, and to enforce this
limit at runtime rather than as a static guarantee.

##### `Exactly N`

Similar to the case of `At_most N`, the `N` in question
must be a non-negative integer, and can be generally
assumed to be non-zero as well (i.e. positive).

A collection of this type is only valid if it consists
of exactly `N` elements, with both under- and over-saturated
collections to be handled as illegal cases, both
when serializing and deserializing.

Certain optimizations may occur in schemas that contain fixed-cardinality arrays
of elements with fixed serialization lengths (including all fixed-precision
numerics, `Bool`, `Bytes`, and `String`); namely, because the number of bytes
per element and the number of elements are both constant, the overall length of
the schema element when serialized is also a constant value.  Care should be
taken to note any relevant optimizations that `data-encoding` performs for such
cases, as they may cause different encoding strategies to be chosen for
preceding elements in a containing structure. These optimizations may consist of
the elision of prefixes that would otherwise be mandated due to variable-length
trailing elements.  For more detail on this matter, see the notes on
[`VPadded`](#vpadded) and [`Dyn`](#dyn).

#### Sequence Serialization/Deserialization

All sequence types have the same serialization
strategy, provided the value being serialized
is not considered invalid due to over- or
under-saturation for non-`No_limit` cases.

$\forall \; \mathcal{L} \simeq [x_0,\,x_1,\,\ldots,\,x_n] \; . \; \mathrm{serialize}_{\mathrm{Seq}(T)}(\mathcal{L}) \coloneqq
 \bigoplus\limits_{i = 0}^{n} \mathrm{serialize}_{T}(x_i)
$

Namely, the empty list is encoded as the empty byte-sequence $\varepsilon$, while any other list is serialized as the raw concatenation of the serialized forms of each element, in their original order.

```text
write_seq(seq, buffer)
    if x is oversaturated or undersaturated
        raise exception or return error
    for each element x in seq
        write element x to end of buffer
```

For deserialization, the strategy is slightly different, depending on the particular variety of sequence being decoded.

##### For `No_limit`

```text
deserialize_seq_nolimit<T>(buffer)
    let tmp = new, initially-empty list
    while at least one byte is available to consume
        attempt to deserialize T from buffer
        if successful
            append return value to end of x
        else
            short-circuit and propagate error
    return tmp
```

##### For `At_most N`

```text
deserialize_seq_limit<T, N>(buffer)
    let tmp = new list that can hold up to N elements
    while at least one byte is available to consume
        if tmp has reached N elements already
            short-circuit and yield appropriate error
        attempt to deserialize T from buffer
        if successful
            append return value to end of x
        else
            short-circuit and propagate error
    return tmp
```

##### For `Exactly N`

```text
deserialize_seq_exact<T, N>(buffer)
    let tmp = new list with room for N elements
    while tmp has fewer than N elements
        if no bytes are available to consume
            short-circuit and yield appropriate error
        attempt to deserialize T from buffer
        if successful
            append return value to end of x
        else
            short-circuit and propagate error
    return tmp
```

It is important to note that only in the case of
`Exactly N` sequences, there is an early exit
condition after `N` elements have been consumed.
**Even if it would be possible to consume further
bytes**, no further iterations or recursions are
attempted, and the buffer is left in the immediate
state, with
the saturated `N`-element sequence being returned
normally.

This is in contrast to the loop (or recursion) logic of the other two types of
sequence: for `No_limit` and `At_most N`, the end-condition is running out of
bytes to consume, and not predicated on the number of elements added thusfar.
This may seem slightly counter-intuitive in the case of `At_most N`, where one
might be tempted to assume that after `N` elements are added, it is immaterial
whether any bytes remain to consume. At least in the current implementation of
`data-encoding`, however, this is not the case, and it is a failure of
expectation to find that more bytes can be consumed past the point where any
elements can be added without oversaturating the sequence accumulator.

As a brief note, it is worth mentioning that the intended behavior for
deserializing `Exactly N` schema elements is not currently implemented for the
Rust runtime, which has been noted in the `Known Bugs` section of the
accompanying README file.  The intended behavior should not be difficult to
implement, but it is still necessary to do so.

### Padded

The `Padded` composite type represents a compound schema
element consisting of a payload value, followed by
a constant number of nill padding-bytes.

The number of bytes of padding is a positive integer
specified in the `Padded` variant arguments,
as `Padded (N, payload_encoding)`

It is serialized by writing the bytes of the payload,
and appending `N` zeroed bytes afterwards.

It is deserialized by first parsing the payload type, and
either skipping or consuming and discarding the
appropriate number of padding bytes.

One important property regarding `Padded` schemas is that, as of the current
implementation of `data-encoding`, only payload schemas that are determined to
be of fixed serialization width are considered valid. This fact may eventually
change, but unless otherwise noted, can be assumed.

Another consideration is that it `data-encoding` deserialization logic does not
attempt to verify that the extra 'padding' bytes were all zero-valued, and so
this check is not necessary to perform in target implementations, except for
possible debugging purposes in a pre-stable version.

Similarly, it is not strictly necessary to ensure that the bytes written in the
padding during serialization are all zero-valued; however, it is strongly
recommended that, unless it imposes an excessive overhead on performance, only
zeroed bytes should be used for padding.

### `Dyn`

The `Dyn` composite type attaches a length-prefix to the
payload type it applies to, indicating how many subsequent
bytes belong to the wrapped element.

Its use is primarily to embed normally variable schema elements into a larger context, as `Dyn (lp, x)`
is dynamic and can therefore be parsed in a context-free
manner.

Within the `comp_t` definition, `Dyn` takes two arguments:

```ocaml
type lenpref = [ `Uint30 | `Uint16 | `Uint8 ]

type comp_t = (* ... *)
  | Dyn of lenpref * t
```

(where `t` refers to `Simplified.t`).

The value of type `lenpref` is a polymorphic variant (polyvar), and determines
how many bytes to use for the length-prefix for the serialized payload. The choice of which prefix width
to use is both determined by, and itself determines,
how long a payload value can be in serialized bytes.

$\mathrm{serialize}_{\mathrm{Dyn}_{T}}(\mathrm{Dyn}(U, x))\coloneqq \mathrm{serialize}_{\mathtt{U}}(\mathbf{len}~  \mathtt{X}) \oplus \mathtt{X} \\ \quad \mathrm{where}\ \mathtt{X} \coloneqq \mathrm{serialize}_{T}(x) \land \mathbf{len}~\mathtt{X} \le \max_U \land\ U \in \{ \mathtt{`Uint8}, \mathtt{`Uint16}, \mathtt{`Uint30}\}$

Namely, the serialized form equivalent to a `Dyn`-wrapped value `x` with an implicit prefix-width of `U`, consists
of the 1-, 2- , or 4-byte unsigned integer representation
of the serialized length of `x` (as determined by the length-prefix type-argument), followed by the serialization of `x` itself. In algorithmic terms, one might envision the process as follows:

```text
serialize_dynamic(U, x, buffer)
    let ix = length of buffer
    append size_of(U) bytes to the buffer
    let payload_ix = new length of buffer
    append serialized bytes of x to buffer
    let total_ix = new length of buffer
    let payload_bytes = total_ix - payload_ix
    if payloaad_bytes > maxval(U)
        short-circuit and return error or throw exception
    let lp_bytes = serialize(payload_bytes as U)
    blit lp_bytes into buffer[ix..payload_ix]
```

In the actual Rust runtime implementation, there is a facility that allows for
cheap, accurate calculation of how many bytes an arbitrary codec value will take
up when serialized. Using this it is possible to write the prefix inn-place
without backtracking to update it, as well as to avoid modifying the buffer
before it is known whether the serialized payload would overflow the
length-prefix type, in which case an error is returned early.

Accordingly, the approach taken in Rust may also be used in other languages
that this design can be replicated in. If it is far cheaper
to post-modify the reserved bytes for the prefix, however, that is a viable alternative.

For deserialization, with `LP = [Uint8 | Uint16 | Uint30]`:

```text
deserialize_dyn<LP, T>(buffer)
    let n : LP = deserialize<LP>(buffer)
    if unsuccessful
        propogate this failure to caller
    if fewer than n bytes are available to consume
        return error or raise exception
    temporarily restrict buffer so at most n bytes can be consumed
    attempt to deserialize T value as normal from restricted buffer
    if unsuccessful
        propogate this failure to caller
    else if any bytes can be consumed in restricted buffer
        return error or raise exception
    remove the restriction previously placed on buffer
    wrap deserialized T value as necessary and return it
```

It is important to note that, regardless of how 'restrict buffer so at most n bytes can be consumed' and 'remove the restriction previously placed on buffer' are achieved
in the actual implementation,
it should be done in such a way that any extant restrictions on the buffer that is passed in are both respected, and preserved, by the addition and removal of the temporary
guards around the deserialization of the payload type. This is because it is both possible, and indeed not uncommon, for `Dyn` elements to have a `Dyn`-wrapped ancestor. It is not
known how deep this stack is likely to extend in practice, but optimization around the assumption that such stacks will have relatively small (< 32) maximum occupancy may yield marginal performance improvements.

Note also that `Uint30` is used for the 4-byte prefix case, and not `Uint32` (which is not part of the schema language). This means that even payloads whose lengths fit into 4 unsigned bytes may be invalid, if they exceed the maximum `Uint30` value of $2^{30} - 1 = \mathtt{0x3f\,ff\;ff\,ff}$.

### `VPadded`

The `VPadded` composite type, unlike all others, is a virtual wrapper around a schema element,
used for preserving structurally opaque metadata closer to the leaf-node in the intermediate representation AST where a given element appears.

`VPadded` is inserted as a wrapper during the final *annotation* sub-phase of simplification.

`VPadded (n, enc)` indicates a schema element that is transparently identical to `enc` in all respects, but
includes the annotation `n : int` that signals how
many bytes of trailing content appear in the same
container as the self-element.

Namely, a codec that looks like

```ocaml
tup2 Variable.string bool
```

consists of a Variable-length string followed by a single byte of trailing content. Such a construction is both legal and sound, as the overall length of the tuple-encoding is known, and the relative offset from the final byte that the variable-length string must terminate with, can be inferred.

This encoding is simplified to

```ocaml
Prod (Tuple { id; elems =
  [ Comp (VPadded (1, Base (Var VString)))
  ; Base Bool
  ]
})
```

The purpose of `VPadded` is to record, in place, the information about a variable-length schema element,
that allow it to be treated as dynamic for the purposes
of downstream target-language code. In `data-encoding`
itself, this is not necessary, as the schema in its
original form can be parsed by effectively performing
in-place length-marking within binary reader logic,
as it operates on the unsimplified source schema.

Thus, for serialization purposes, `VPadded` is
completely transparent, and the payload is
encoded directly without alteration, modification,
or validation.

During deserialization, however, the dynamic information
made available through the `VPadded` wrapper should be
used to determine the final byte of the payload element
through arithmetic:

```text
deserialize_vpadded<T, N>(buffer)
    let rem = number of bytes that are available to consume from buffer
    if rem < N
        return error or throw exception
    let wlen = rem - N
    restrict buffer so that at most wlen bytes can be consumed
    attempt to deserialize T value from restricted buffer
    if this operation failed
        return or raise the original error
    else if any bytes can be consumed without violating restriction
        return error or raise exception
    lift the restriction we added before
    return the deserialized value
```

## Product Types

Product types consist of a fixed number and order of possibly-distinct-typed elements, referred to
either by positional index (for tuples), or
by a field-name (for records).

For all intents and purposes, records are encoded
and decoded as if they were tuples with the fields
specified as positional arguments in the same absolute
order. The only complexities to be aware of are the
in-language model to use for records, which may not
always be natively supported; tuples, if not natively
supported, may be encoded as index-keyed records,
dictionaries, structures, or maps, depending on the
language in question.

Tuples and records are both encoded by serializing
each field or positional argument in order of definition,
without any intervening padding, separation, or delimitation
except as specified on a per-element layer. Namely,

```text
serialize_tuple( (x1 : t1, x2 : t2, ..., xn : tn) ) ===
serialize_t1(x1) + serialize_t2(x2) + ... + serialize_tn(xn)
```

as a direct concatenation of byte-sequences.

If necessary, locally unique and predictable identifiers are generated for most product-type simplifications, which are stored as an optional string-field `id` in the variant-associated record for the given product type.

## Sum Types

Sum types are type-level enumerations or disjunctions. They are encoded largely as-normal, but with specific details to be explained in dedicated sections below.

### `CStyle`

The `CStyle` variant of `sum_t` is defined as

```ocaml
type enum_size = [`Uint30 | `Uint16 | `Uint8]

type sum_t = (* ... *)
  | CStyle of { id : string option;
                size: enum_size;
                enums : (string * int) list
              }
```

where `id` is an optional name for the enumerated type itself,
`size` indicates the byte-width of unsigned integer
that is to be used for transcoding enum values,
and `enums` is a list of associations between
identifiers and non-negative integers they
are modeled as (which must all fit within
the type referred to by `size`).

Broadly speaking, C-Style enums are custom types
with one or more possible values, which are all specified
explicitly and given locally unique identifiers, as well
as an integer they either represent, or which they are
to be represented by.

Aside from how to define such codec types in a given
language, which may not support enums, there is very
little of note regarding this case. Aside from ensuring
that a deserialized value is a valid member of the
enum being decoded, it can be handled mostly through
transparent type-casting over the backing unsigned
integer type specified by `size`. As noted
in the section on [`Dyn`](#dyn), it should
also be checked that values above `0x3fff_ffff`
are illegal in both the definition of, and the
deserialization of `` `Uint30 ``-backed C-Style
enums.

### `Data`

`Data` enums are defined similarly to `CStyle` enums:

```ocaml
type tag_size = [`Uint8 | `Uint16]

type sum_t = (* ... *)
  | Data of {
      id : string option;
      size : tag_size;
      vars : (int * (string * t)) list;
    }
```

The field `id` serves an identical purpose to
`CStyle`, while `size` is restricted to only
``[`Uint8 | `Uint16]`` and `vars` specifies
a discriminant, a variant name, and a payload
schema.

The discriminant of a `Data`-enum acts as
a tag for an otherwise undiscriminated
type-union, and is encoded before the
payload value.

Serialization consist of encoding the
associated tag of a given case (in the
width of the `size` field schema),
followed by the unmodified bytes
of the serialized payload value.

Deserialization correspondingly
consists of first reading the
tag as an appropriate-width unsigned
integer, identifying which payload
it corresponds to (if any; otherwise,
the value is illegal and should result in
an error or exception); and then parsing
the payload of the indicated variant
directly and ensuring the resulting
value is packaged correctly.

#### Transient Discriminants

Even in languages that have native
support of enumerated types with
discriminants, in certain cases
it may not be possible to directly
associate a custom discriminant
with a given variant. This is the
case, most notably, for Rust,
which is the motivating reason
for including this section.

If it not possible to control the in-memory discriminant
for variants of an enumerated type, and even as a general
strategy for languages in which enumerated types cannot
be defined in any case, a transient transcoding strategy
can be employed.

Namely, the discriminants can exist only in method and function definitions as a
switch-statement, pattern-match, multi-way-if, or lookup key, which encodes the
custom discriminants in a black-box fashion. Such code may even be generated
using boilerplate-reduction techniques, such as template language, preprocessor
directives, or macros.

For example, if we wished to define (in Rust)

```rust
enum SmallPrimePower {
    Unit = 1,
    Pow2(u8) = 2,
    Pow3(u8) = 3,
    Pow5(u8) = 5,
}
```

this would be invalid syntax, at least in stable Rust `1.16`,
as custom discriminants are only legal for c-style enums.

However, one could eschew type-level discriminants and use transient pattern-matching:

```rust
enum SmallPrimePower {
    Unit,
    Pow2(u8),
    Pow3(u8),
    Pow5(u8),
}

fn encode_spp(x: SmallPrimePower) -> Vec<u8> {
    match x {
        SmallPrimePower::Unit => vec![1],
        SmallPrimePower::Pow2(n) => vec![2, n],
        SmallPrimePower::Pow3(n) => vec![3, n],
        SmallPrimePower::Pow5(n) => vec![5, n],
    }
}

fn decode_spp(x: &[u8]) -> Option<SmallPrimePower> {
    match x[0] {
        1 => Some(SmallPrimePower::Unit),
        2 => Some(SmallPrimePower::Pow2(x[1])),
        3 => Some(SmallPrimePower::Pow3(x[1])),
        5 => Some(SmallPrimePower::Pow5(x[1])),
        _ => None
    }
}
```

(note this is a simplified listing for illustrative purposes and is not intended to model the actual design of the Rust runtime, but rather a naive showcase of the strategy of transient discriminant representation).

## Rust-Target Development History

This section is reserved for an overview of the development history
of support for the Rust target, both in terms of the runtime library
and the compiler design, over the course of its evolution.

Note that this will often be an idealized form of the overall design at each
phase, and the code listings provided will exhibit more idiomatic, performant,
sound, or otherwise mature practices and conventions than in the project's
actual history.  In that way, this section is more of an overview of how one
would write a new target that happened also to be Rust, given the lessons
learned in the process of implementing it from scratch originally.

### Initial Support: Parser model and `u8`

The initial incarnation of the Rust runtime consisted of a handful of
definitions, some of which persist in modified forms in the current version. The
first case for which prototypical support was added was `Uint8`, which was
modelled directly using the Rust primitive type `u8`.

> **Source Code**
>
> Main Definitions:
>
> [`hexstring.rs`](experience_src/rust_samples/v1_uint8/src/hexstring.rs)
> [`parse.rs`](experience_src/rust_samples/v1_uint8/src/parse.rs)
> [`decode.rs`](experience_src/rust_samples/v1_uint8/src/decode.rs)
>
> Packaging:
>
> [`lib.rs`](experience_src/rust_samples/v1_uint8/src/lib.rs)
> [`main.rs`](experience_src/rust_samples/v1_uint8/src/main.rs)

Several decisions at this level may not initially be obvious upon browsing the
linked source files above.  For example, the decision to interpret `&str` as
hex-encoded strings should be highlighted at this point. This decision is made
for reasons of both portability and readability.  Namely, as not every language
considers `0x00 - 0xff` as valid in the context of a one-byte string-value, and
different languages may require different constraints on values that are to be
read as the most natural equivalent to a string-type, hex-strings are far more
cross-language portable as the representation of serialized values.

Secondly, the implementation of `ByteParser` is very basic at this
stage; even though it technically support `consume` calls of any
length, if the only type that is ever decoded is `u8`, there is
no code-path that invokes said method with an argument of anything
but `1`. However, as `consume` is a fundamental operation for
the evolving parser design, it is more flexible than it needs
to be for the present purposes.

Another point worth mentioning is the separation between the method `take_u8`
which is reserved typically for standard types defined as schema-level
primitives, and the standalone function `decode_u8`, which operates on a higher
level (i.e. taking a possibly-invalid yet portable hex-string, rather than an
initialized parser), and expects to fully exhaust the parser it constructs and
then consumes from in the process of decoding a value of the appropriate type.

Note that, in the absence of generic types, this series of definitions
would still be possible, as the generic aliases to specific-error `Result` types can all be inlined; in languages for which the standard
way to handle errors is to raise exceptions, these signatures can be
rewritten altogether. Lastly, no traits have been defined by this
code, and so even in languages without an equivalent notion of type-classes, it is fairly simple to translate this small codebase
into many different target languages with varied feature-sets.

### Trait-Based Approach

The next avenue for development introduces novel traits to allow for common
methods (such as serialization and deserialization) to be associated directly
with even Rust-primitive types (as inherent methods cannot be added outside of
the crate that defines a type), which becomes the model used for the rest of the
development of the runtime, even if the names of the traits, as well as both
their design and internal implementation, all changed a great deal.

> Source Code:
> [`decode.rs`](rust_samples/v2_traits/src/decode.rs)
> [`encode.rs`](rust_samples/v2_traits/src/encode.rs)
> [`parse.rs`](rust_samples/v2_traits/src/parse.rs)
> [`hexstring.rs`](rust_samples/v2_traits/src/hexstring.rs)

For the purposes of handling the deserialization of a single primitive type,
the first approach, in which we defined a standalone function `decode_u8`,
was a reasonable choice.

However, this design does not properly hold up in the larger scale, compared
at least to the approach that was chosen at this point for the Rust runtime
library. Namely, to use Rust's trait system to associate the requisite
methods for serializing and deserializing a type directly to the type,
rather than requiring client code to know the machine-generated
function-names for each type they wish to encode or decode.

This changeset therefore substitutes in trait definitions
with methods for deserialization (`Decode`), along with
support for serialization as well, with the `Encode` trait.[^4]

[^4]: Note that, while this document generally refers to the processes of
converting a value to and from binary as serialization and deserialization, the
trait names `Serialize` and `Deserialize` would overlap with the widely-used
Rust crate `serde`, and therefore `Encode` and `Decode` are chosen for reasons
of disambiguation.

As the trait-based approach is specific to the features available in Rust,
this redesign will not always be possible, or sensible even when possible,
in other target languages. In certain such languages, other features may
allow for slightly more ergonomic designs, such as global dictionaries
that associate the name of a schema with the otherwise inscrutably named
functions that serialize and deserialize values of the corresponding codec type.
This may be especially useful in cases where the language does not distinguish
between various numeric schema types (such as in TypeScript), or in which
there is no way to add further methods to pre-existing data-types.

### AST-Generator Prototype

Having developed a runtime API that is sufficient for `Uint8` schemas,
the next phase is to prototype the code generation logic, including
the AST model for the target language.

A fairly simple way to go about this is to begin by manually writing, the ideal
source-code output that one would want the code generator for the target
language to produce, when given a `Uint8` schema as input. This can then be
broken apart into distinct AST node-types: directives for importing definitions
from the appropriate libraries, packages, or header files; various kinds of
top-level definition; the types that are necessary to write out in the generated
code, both general types used within the logic of serialization/deserialization,
as well as the codec types themselves; and lower level atoms such as
expressions, statements, and values.

Given that the scope of the project will inevitably require the AST model and
generator logic to evolve, it is not yet necessary to ensure that the initial
implementation is future-proof.  In some ways, it might even be better to start
with a good-enough version that demonstrates the minimum capabilities of the
target language implementation. Any changes that will eventually be mandated by
the increasing complexity of additional schema types, need not be front-loaded.

Once this compiler support is added, and produces code in the target language,
it is possible to troubleshoot any issues with either the generator logic, or
the runtime itself, both in terms of syntax errors or other issues that would
prevent successful compilation of the output codec module; and once that work,
if any, is done, to then determine whether the codec module exhibits the
intended behavior. For `Uint8`, this should be fairly trivial to verify, as for
this schema there are no invalid byte-sequences (provided they do contain a
single byte, of course). Furthermore, it is unlikely that the mapping from the
consumed byte into the type chosen to represent `Uint8` will require any special
considerations, even for target languages in which such a conversion is
necessary.

A note to make at this point, having discussed generator logic, is on the
subject of names and identifiers. To the extent that the the compiler should
attempt to preserve, as much as possible, the original identifier associated
with a schema, it will eventually, though not immediately, become necessary to
add logic for both name sanitization, and identifier lookup, once the developer
of the target language no longer has control over what names are chosen for
codec types. For now, it is reasonable enough to chose identifiers for the input
schema that are already well-formatted in the target language, in terms of legal
characters and conventions for capitalization, and casing guidelines
(`snake_case` vs `PascalCase`, and so forth).

A further note is that, due to the fact that there are several smart
constructors in `data-encoding` that are handled transparently by the
simplification phase, `Encoding.t` values other than `uint8` will also be
simplified as `Uint8`.  For example, `delayed`, `def`, `conv`[^5], and so forth,
will all preserve the identity of the schema they are applied to, and do not, as
of yet, require any special consideration.

[^5]: The `conv` constructor in particular poses a major challenge to the scope of this project, and will undoubtedly be described in its own section. For now, the issues related
to its usage will be overlooked as they
are not yet relevant in the preceding context.

### Automated Test-Generation

Though potentially under-used in the Rust runtime as it stands, the next change
worth mentioning is the introduction of a framework, within the generator/AST
modules, to allow for callers to provide a list of test-cases, as hex-strings,
that the generator will translate into unit tests that ensure that the selected
values 'round-trip': namely, that `encode(decode(x)) == x` for all the
provided cases. This might, for certain languages, require the generator to
produce source-code for more than one standalone file, if the testing framework
being used does not work for module-internal unit tests.

In the driver for the compiler run over the selected target, it is then possible
to engineer a system of automatically encoding the desired values from the
source OCaml type, which is a feature already included in the compiler itself,
and has the advantage of being target-agnostic, provided that the inevitable
need for producing source-code snippets for string literals is added by this
point.

For `Uint8`, it is even possible to test the full range of values, though it is
perhaps sufficient to test the minimum and maximum values, as well as an
assortment of intermediate values chosen arbitrarily.

### `bool` Support

Having implemented support for `Uint8`, it is fairly trivial to extend support to `Bool`, which
can be modeled as a `Uint8` value that is then
mapped into the appropriate boolean type.

Note that this conversion is fallible, in that
only `0x00` and `0xff` are allowed as boolean
encodings; all other values should be rejected
either with an error value or an exception.

> Source Code:
> [`decode.rs`](rust_samples/v3_boolean/src/decode.rs)
> [`encode.rs`](rust_samples/v3_boolean/src/encode.rs)
> [`parse.rs`](rust_samples/v3_boolean/src/parse.rs)
> [`hexstring.rs`](rust_samples/v3_boolean/src/hexstring.rs)

Further exploration of the history of the Rust target development process can be done at the reader's discretion
by viewing the project history leading up to the branch of this document's release.
