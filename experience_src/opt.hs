import Data.Bifunctor (first)
import Data.ByteString (ByteString, uncons)
import Data.Functor ((<$>))

class Deserialize a where
  parse :: ByteString -> Either String (a, ByteString)

instance (Deserialize a) => Deserialize (Maybe a) where
  parse = \bytes ->
    case uncons bytes of
      Nothing -> Left "Unexpected EOB"
      Just (0x00, rest) -> Right (Nothing, rest)
      Just (0xff, rest) -> first Just <$> parse rest
      Just _ -> Left "Invalid Option-flag"
