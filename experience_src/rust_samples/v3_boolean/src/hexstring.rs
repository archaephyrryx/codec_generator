#[derive(Clone, Debug, PartialEq, PartialOrd, Eq, Ord)]
pub enum HexConvError {
    OddParity(String),
    NonHex(String),
}

impl std::fmt::Display for HexConvError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::OddParity(_) => {
                write!(f, "hex-conversion failed on odd-length string")
            }
            Self::NonHex(_) => {
                write!(f, "hex-conversion failed on non-hex character")
            }
        }
    }
}

impl std::error::Error for HexConvError {}

pub type ConvResult<T> = std::result::Result<T, HexConvError>;

pub fn bytes_of_hex(s: &str) -> ConvResult<Vec<u8>> {
    let ascii_len = s.len();

    if ascii_len == 0 {
        return Ok(Vec::new());
    } else if ascii_len % 2 != 0 {
        return Err(HexConvError::OddParity(s.to_string()));
    }

    let bytes_len: usize = ascii_len / 2;

    let mut dst: Vec<u8> = Vec::with_capacity(bytes_len);

    for ix in 0..bytes_len {
        match u8::from_str_radix(&s[(ix * 2)..(ix * 2) + 2], 16) {
            Ok(byte) => dst.push(byte),
            Err(_) => return Err(HexConvError::NonHex(s.to_string())),
        }
    }
    Ok(dst)
}
