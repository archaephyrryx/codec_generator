use crate::hexstring::{bytes_of_hex, ConvResult};

#[derive(Clone, Debug, PartialEq)]
pub struct ConsumeOverflowError {
    requested: usize,
    available: usize,
}

impl std::fmt::Display for ConsumeOverflowError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} bytes were requested with only {} available",
            self.requested, self.available
        )
    }
}

impl std::error::Error for ConsumeOverflowError {}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct InvalidBooleanError(u8);

impl std::fmt::Display for InvalidBooleanError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "coercion to boolean failed on invalid byte `{:#02x}`", self.0)
    }
}

impl std::error::Error for InvalidBooleanError {}

#[derive(Debug, Clone, PartialEq)]
#[non_exhaustive]
pub enum ParseError {
    Consume(ConsumeOverflowError),
    Bool(InvalidBooleanError)
}

impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Consume(err) => write!(f, "{}", err),
            Self::Bool(err) => write!(f, "{}", err),
        }
    }
}

impl From<ConsumeOverflowError> for ParseError {
    fn from(err: ConsumeOverflowError) -> Self {
        Self::Consume(err)
    }
}

impl From<InvalidBooleanError> for ParseError {
    fn from(err: InvalidBooleanError) -> Self {
        Self::Bool(err)
    }
}

impl std::error::Error for ParseError {
    fn source(&self) -> Option<&(dyn 'static + std::error::Error)> {
        match self {
            Self::Consume(err) => Some(err),
            Self::Bool(err) => Some(err),
        }
    }
}


pub type ParseResult<T> = Result<T, ParseError>;

#[derive(Clone, Debug, PartialEq, PartialOrd, Eq, Ord)]
#[repr(transparent)]
pub(crate) struct VecBuffer(Vec<u8>);

impl VecBuffer {
    pub(crate) fn try_from_hex(s: &str) -> ConvResult<Self> {
        Ok(Self(bytes_of_hex(s)?))
    }

    pub(crate) fn from_bytes(arr: &[u8]) -> Self {
        Self(arr.to_vec())
    }

    pub(crate) unsafe fn get_slice(&self, ix: usize, len: usize) -> &[u8] {
        &self.0[ix..ix + len]
    }

    pub(crate) unsafe fn get_byte(&self, ix: usize) -> u8 {
        self.0[ix]
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }
}

#[derive(Clone, Debug)]
pub struct ByteParser {
    buffer: VecBuffer,
    offset: usize,
}

impl ByteParser {
    pub fn try_from_hex(s: &str) -> ConvResult<Self> {
        Ok(Self {
            buffer: VecBuffer::try_from_hex(s)?,
            offset: 0usize,
        })
    }

    pub fn from_bytes(arr: &[u8]) -> Self {
        Self {
            buffer: VecBuffer::from_bytes(arr),
            offset: 0usize,
        }
    }

    pub fn remaining(&self) -> usize {
        self.buffer.len() - self.offset
    }

    pub fn consume_byte(&mut self) -> ParseResult<u8> {
        let ix: usize = self.offset;

        debug_assert!(
            self.buffer.len() >= ix,
            "byteparser offset {} exceeds length of buffer {}",
            ix,
            self.buffer.len()
        );
        let available: usize = self.buffer.len() - ix;

        if available == 0 {
            Err(ConsumeOverflowError {
                requested: 1,
                available,
            }
            .into())
        } else {
            let ret = unsafe { self.buffer.get_byte(ix) };
            self.offset += 1;
            Ok(ret)
        }
    }

    pub fn consume(&mut self, nbytes: usize) -> ParseResult<&[u8]> {
        let ix: usize = self.offset;

        debug_assert!(
            self.buffer.len() >= ix,
            "byteparser offset {} exceeds length of buffer {}",
            ix,
            self.buffer.len()
        );
        let available: usize = self.buffer.len() - ix;

        if nbytes > available {
            Err(ConsumeOverflowError {
                requested: nbytes,
                available,
            }
            .into())
        } else {
            let ret = unsafe { self.buffer.get_slice(ix, nbytes) };
            self.offset += nbytes;
            Ok(ret)
        }
    }
}

impl ByteParser {
    pub fn take_u8(&mut self) -> ParseResult<u8> {
        self.consume_byte()
    }

    pub fn take_bool(&mut self) -> ParseResult<bool> {
        match self.consume_byte()? {
            0xff => Ok(true),
            0x00 => Ok(false),
            byte => Err(InvalidBooleanError(byte).into()),
        }
    }
}