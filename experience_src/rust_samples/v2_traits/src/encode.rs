pub trait Encode {
    fn write_to_vec(&self, buf: &mut Vec<u8>);

    fn to_bytes(&self) -> Vec<u8> {
        let mut ret = Vec::new();
        self.write_to_vec(&mut ret);
        ret
    }
}

impl Encode for u8 {
    fn write_to_vec(&self, buf: &mut Vec<u8>) {
        buf.push(*self)
    }

    fn to_bytes(&self) -> Vec<u8> {
        vec![*self]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn encode_expected() {
        assert_eq!(0u8.to_bytes().as_slice(), &[0u8]);
        assert_eq!(255u8.to_bytes().as_slice(), &[255u8]);
    }
}