use crate::hexstring::HexConvError;
use crate::parse::{ByteParser, ParseError, ParseResult};

#[derive(Debug, Clone, PartialEq)]
pub enum DecodeError {
    Hex(HexConvError),
    Parse(ParseError),
}

impl From<HexConvError> for DecodeError {
    fn from(err: HexConvError) -> Self {
        Self::Hex(err)
    }
}

impl std::fmt::Display for DecodeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Hex(err) => write!(f, "{err}"),
            Self::Parse(err) => write!(f, "{err}"),
        }
    }
}

impl From<ParseError> for DecodeError {
    fn from(err: ParseError) -> Self {
        Self::Parse(err)
    }
}

impl std::error::Error for DecodeError {
    fn source(&self) -> Option<&(dyn 'static + std::error::Error)> {
        match self {
            Self::Hex(err) => Some(err),
            Self::Parse(err) => Some(err),
        }
    }
}

pub type DecodeResult<T> = Result<T, DecodeError>;

pub trait Decode {
    fn parse(p: &mut ByteParser) -> ParseResult<Self>
    where
        Self: Sized;

    fn decode<S: AsRef<str>>(s: S) -> DecodeResult<Self>
    where
        Self: Sized,
    {
        let mut p = ByteParser::try_from_hex(s.as_ref())?;
        let ret = Self::parse(&mut p)?;
        debug_assert_eq!(
            p.remaining(),
            0,
            "decode call failed to exhaust contents of parser object"
        );
        Ok(ret)
    }
}

impl Decode for u8 {
    fn parse(p: &mut ByteParser) -> ParseResult<Self> {
        p.take_u8()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_expected() {
        assert_eq!(u8::decode("00"), Ok(0x00));
        assert_eq!(u8::decode("ff"), Ok(0xff));
    }
}
