extern crate v1;

fn main() {
    assert_eq!(v1::decode::decode_u8("00").unwrap(), 0x00u8);
    assert_eq!(v1::decode::decode_u8("ff").unwrap(), 0xffu8);
}