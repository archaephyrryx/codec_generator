use crate::parse::{ByteParser, ParseError};
use crate::hexstring::HexConvError;

#[derive(Debug, Clone, PartialEq)]
pub enum DecodeError {
    Hex(HexConvError),
    Parse(ParseError),
}

impl From<HexConvError> for DecodeError {
    fn from(err: HexConvError) -> Self {
        Self::Hex(err)
    }
}

impl std::fmt::Display for DecodeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Hex(err) => write!(f, "{err}"),
            Self::Parse(err) => write!(f, "{err}"),
        }
    }
}

impl From<ParseError> for DecodeError {
    fn from(err: ParseError) -> Self {
        Self::Parse(err)
    }
}

impl std::error::Error for DecodeError {
    fn source(&self) -> Option<&(dyn 'static + std::error::Error)> {
        match self {
            Self::Hex(err) => Some(err),
            Self::Parse(err) => Some(err),
        }
    }
}

pub type DecodeResult<T> = Result<T, DecodeError>;

pub fn decode_u8<S: AsRef<str>>(src: S) -> DecodeResult<u8> {
    let mut p = ByteParser::try_from_hex(src.as_ref())?;
    let ret = p.take_u8()?;
    debug_assert_eq!(p.remaining(), 0, "leftover bytes after decoding u8");
    Ok(ret)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_expected() {
        assert_eq!(decode_u8("00"), Ok(0x00));
        assert_eq!(decode_u8("ff"), Ok(0xff));
    }
}
