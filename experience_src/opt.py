class Opt:

    def __init__(self, val=None):
        if val is not None:
            self.store = [val]
        else:
            self.store = []

    def some(val):
        return Opt(val)

    def none():
        return Opt()

    def has_value(self):
        return (len(self.store) != 0)

    def is_empty(self):
        return (len(self.store) == 0)

    def get(self, default=None):
        return (self.store + [default])[0]

    def take(self):
        if self.store:
            return self.store.pop()
        else:
            return None

    def replace(self, newval):
        if self.store:
            ret = self.store[0]
            self.store[0] = newval
            return ret
        else:
            self.store = [newval]
            return None
