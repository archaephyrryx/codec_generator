open Data_encoding
open Codec_generator.Rust.Generator
open Codec_generator.Rust.Ast

let to_images enc =
  let to_image i = Binary.to_bytes_exn enc i |> Hex.of_bytes |> Hex.show in
  List.map to_image

let reverse_prune : tgt:string -> string -> string =
  let rec drop_until x ys =
    match ys with
    | [] -> []
    | y :: _ when x = y -> ys
    | _ :: yt -> drop_until x yt
  in
  fun ~tgt path ->
    String.split_on_char '/' path
    |> List.rev |> drop_until tgt |> List.rev |> String.concat "/"

let produce_sourcefile ?preimages ~modulename ~typename enc =
  let images = Option.map (to_images enc) preimages in
  let oput =
    enc |> Simplified.of_encoding |> Gen.generate ?images typename |> pp_module
  in
  (* Ascend the directory tree until we get to the closest ancestor directory
     that is called [codec_generator] in its parent directory. *)
  let path = Sys.getcwd () |> reverse_prune ~tgt:"codec_generator" in
  let filepath =
    Printf.sprintf "%s/generated_rust/src/sample/%s.rs" path modulename
  in
  let oc = open_out filepath in
  Common.Printer.(oput $ Doc oc) ;
  close_out oc

let main_int () =
  let preimages = [0; 1; 2; 3; 255] in
  produce_sourcefile ~preimages ~modulename:"uint8" ~typename:"Uint8" uint8 ;
  let preimages = [0; 1; 2; 3; 0x7f; ~-0x80; ~-1] in
  produce_sourcefile ~preimages ~modulename:"int8" ~typename:"Int8" int8 ;
  let preimages = [1; 2; 3; 0x7fff; 0x8000; 0xffff] in
  produce_sourcefile ~preimages ~modulename:"uint16" ~typename:"Uint16" uint16 ;
  let preimages = [1; 2; 3; 0x7fff; ~-0x8000; ~-1] in
  produce_sourcefile ~preimages ~modulename:"int16" ~typename:"Int16" int16 ;
  let preimages = [1; 2; 3; 0x3fff_ffff] in
  produce_sourcefile
    ~preimages
    ~modulename:"uint30"
    ~typename:"Uint30"
    (ranged_int 0x0 0x3fff_ffff) ;
  let preimages = [1; 2; 3; 0x3fff_ffff; ~-0x4000_0000; ~-0x1] in
  produce_sourcefile
    ~preimages
    ~modulename:"int31"
    ~typename:"Int31"
    (ranged_int ~-0x4000_0000 0x3fff_ffff) ;
  let preimages : Int32.t list = Int32.[one; max_int; min_int; minus_one] in
  produce_sourcefile ~preimages ~modulename:"int32" ~typename:"Int32" int32 ;
  let preimages : Int64.t list = Int64.[one; max_int; min_int; minus_one] in
  produce_sourcefile ~preimages ~modulename:"int64" ~typename:"Int64" int64 ;
  ()

let main_float () =
  let preimages : float list = [1.0; ~-.1.0; Float.pi] in
  produce_sourcefile ~preimages ~modulename:"double" ~typename:"Double" float ;
  let preimages : float list =
    [0.99999; ~-.0.99999; Float.(neg zero); Float.(zero)]
  in
  produce_sourcefile
    ~preimages
    ~modulename:"iota"
    ~typename:"Iota"
    (ranged_float ~-.1. 1.) ;
  ()

let main_zarith () =
  let preimages : Z.t list =
    Z.[of_int 0; of_int 1; of_int 142857; of_int (-32)]
  in
  produce_sourcefile ~preimages ~modulename:"apint" ~typename:"APInt" z ;
  let preimages : Z.t list = Z.[of_int 0; of_int 1; of_int 142857] in
  produce_sourcefile ~preimages ~modulename:"apnat" ~typename:"APNat" n ;
  ()

let main_fixed () =
  let preimages : string list = ["a"; "\n"] in
  produce_sourcefile
    ~preimages
    ~modulename:"monogram"
    ~typename:"Monogram"
    (Fixed.string 1) ;
  let preimages : Bytes.t list =
    Bytes.[of_string "ç"; of_string "42"; of_string "\x00\x00"]
  in
  produce_sourcefile
    ~preimages
    ~modulename:"digraph"
    ~typename:"Digraph"
    (Fixed.bytes 2) ;
  ()

let main_var () =
  let preimages : string list = ["foobar"; "hello world!"] in
  produce_sourcefile
    ~preimages
    ~modulename:"rawstring"
    ~typename:"RawString"
    Variable.string ;
  let preimages : Bytes.t list =
    Bytes.[of_string "foobar"; of_string "hello world!"]
  in
  produce_sourcefile
    ~preimages
    ~modulename:"rawbytes"
    ~typename:"RawBytes"
    Variable.bytes ;
  ()

let main_base () =
  main_int () ;
  main_float () ;
  main_zarith () ;
  main_fixed () ;
  main_var () ;
  produce_sourcefile
    ~preimages:[true; false]
    ~modulename:"boolean"
    ~typename:"Boolean"
    bool ;
  produce_sourcefile ~preimages:[()] ~modulename:"unit" ~typename:"Unit" unit ;
  ()

let main_comp () =
  let preimages : string list = ["foobar"; "hello world!"] in
  produce_sourcefile
    ~preimages
    ~modulename:"mystring"
    ~typename:"MyString"
    string ;
  let preimages : Bytes.t list =
    Bytes.[of_string "foobar"; of_string "hello world!"]
  in
  produce_sourcefile ~preimages ~modulename:"mybytes" ~typename:"MyBytes" bytes ;
  let preimages : bool list list = [[]; [true; false]] in
  produce_sourcefile
    ~preimages
    ~modulename:"boollist"
    ~typename:"BoolList"
    (list bool) ;
  ()

let main_prod_nested ~preimages () =
  let nest x = obj1 (req "contents" x) in
  let nstring = nest string in
  let nnstring = nest nstring in
  let nnnstring = nest nnstring in
  produce_sourcefile
    ~preimages
    ~modulename:"nstring"
    ~typename:"NestedString"
    nstring ;
  produce_sourcefile
    ~preimages
    ~modulename:"nnstring"
    ~typename:"NNestedString"
    nnstring ;
  produce_sourcefile
    ~preimages
    ~modulename:"nnnstring"
    ~typename:"NNNestedString"
    nnnstring ;
  ()

let main_quad_groups ~preimages () =
  produce_sourcefile
    ~preimages
    ~modulename:"quad4"
    ~typename:"Quad4"
    (tup4 int31 int31 int31 int31) ;
  let skew (x, y, z, w) = (x, (y, z, w)) in
  produce_sourcefile
    ~preimages:(List.map skew preimages)
    ~modulename:"quad213"
    ~typename:"Quad213"
    (tup2 (tup1 int31) (tup3 int31 int31 int31)) ;
  let skew (x, y, z, w) = ((x, y), (z, w)) in
  produce_sourcefile
    ~preimages:(List.map skew preimages)
    ~modulename:"quad222"
    ~typename:"Quad222"
    (tup2 (tup2 int31 int31) (tup2 int31 int31)) ;
  let skew (x, y, z, w) = (x, y, (z, w)) in
  produce_sourcefile
    ~preimages:(List.map skew preimages)
    ~modulename:"quad3112"
    ~typename:"Quad3112"
    (tup3 (tup1 int31) (tup1 int31) (tup2 int31 int31)) ;
  let skew (x, y, z, w) = (x, (y, (z, w))) in
  produce_sourcefile
    ~preimages:(List.map skew preimages)
    ~modulename:"quadleft"
    ~typename:"QuadLeft"
    (tup2 (tup1 int31) (tup2 (tup1 int31) (tup2 (tup1 int31) (tup1 int31)))) ;
  ()

let main_prod () =
  let preimages : (int * int) list = [(1, 2); (3, 4)] in
  produce_sourcefile
    ~preimages
    ~modulename:"wordpair"
    ~typename:"WordPair"
    (tup2 int8 int8) ;
  produce_sourcefile
    ~preimages
    ~modulename:"firstlast"
    ~typename:"FirstLast"
    (obj2 (req "first" int8) (req "last" int8)) ;
  let preimages : ((string * string) * bool) list =
    [(("Sherlock", "Holmes"), true); (("Alan", "Turing"), false)]
  in
  produce_sourcefile
    ~preimages
    ~modulename:"person"
    ~typename:"Person"
    (obj2
       (req "name" (obj2 (req "first" string) (req "last" string)))
       (req "fictional" bool)) ;
  let preimages : string list = ["foo"; "bar"] in
  main_prod_nested ~preimages () ;
  let nest x = obj1 (req "contents" x) in
  produce_sourcefile
    ~preimages
    ~modulename:"tetramonic"
    ~typename:"TetraMonic"
    (nest
       (def
          "trimonic"
          (nest (def "dimonic" (nest (def "monic" (nest string))))))) ;
  produce_sourcefile
    ~preimages
    ~modulename:"harmonic"
    ~typename:"Harmonic"
    (nest (nest (def "enharmonic" (nest (nest string))))) ;
  let preimages : (int * int * int * int) list =
    [(1, 2, 3, 4); (1, 3, 5, 7); (2, 3, 5, 7); (1, 2, 3, 5)]
  in
  main_quad_groups ~preimages () ;
  ()

type survey_answer = Answered of bool | NoAnswer

let main_sum () =
  let preimages : survey_answer list =
    [Answered true; Answered false; NoAnswer]
  in
  produce_sourcefile
    ~preimages
    ~modulename:"surveyanswer"
    ~typename:"SurveyAnswer"
    (union
       [
         case
           ~title:"Answered"
           (Tag 0)
           bool
           (function Answered ans -> Some ans | _ -> None)
           (fun ans -> Answered ans);
         case
           ~title:"NoAnswer"
           (Tag 1)
           empty
           (function NoAnswer -> Some () | _ -> None)
           (fun _ -> NoAnswer);
       ]) ;
  ()

module Person = struct
  type t = {first : string; middle : string option; last : string}

  let to_tuple {first; middle; last} = (first, middle, last)

  let of_tuple (first, middle, last) = {first; middle; last}

  let schema_name = "Person"

  let encoding =
    Data_encoding.(
      conv
        to_tuple
        of_tuple
        (obj3 (req "first" string) (opt "middle" string) (req "last" string)))
end

module IPv4 = struct
  type t = int * int * int * int

  let schema_name = "IPv4"

  let encoding = Data_encoding.(tup4 uint8 uint8 uint8 uint8)
end

module FilePath = struct
  type t = Absolute of string list | Relative of string list

  let schema_name = "FilePath"

  let encoding =
    Data_encoding.(
      union
        [
          case
            (Tag 0)
            ~title:"Absolute"
            (list string)
            (function Absolute xs -> Some xs | _ -> None)
            (fun xs -> Absolute xs);
          case
            (Tag 1)
            ~title:"Relative"
            (list string)
            (function Relative xs -> Some xs | _ -> None)
            (fun xs -> Relative xs);
        ])
end

module Nat = struct
  type t = Z | S of t

  let schema_name = "Nat"

  let encoding =
    Data_encoding.(
      mu "Nat" (fun enc ->
          union
            [
              case
                (Tag 0)
                ~title:"Z"
                empty
                (function Z -> Some () | _ -> None)
                (fun _ -> Z);
              case
                (Tag 1)
                ~title:"S"
                enc
                (function S n -> Some n | _ -> None)
                (fun n -> S n);
            ]))
end

module TTOL = struct
  type t = OneLie of bool | TwoTruths of bool * bool

  let schema_name = "TTOL"

  let encoding =
    Data_encoding.(
      union
        [
          case
            (Tag 1)
            ~title:"OneLie"
            (Fixed.add_padding bool 1)
            (function OneLie x -> Some x | _ -> None)
            (fun x -> OneLie x);
          case
            (Tag 2)
            ~title:"TwoTruths"
            (tup2 bool bool)
            (function TwoTruths (x, y) -> Some (x, y) | _ -> None)
            (fun (x, y) -> TwoTruths (x, y));
        ])
end

let main_samples () =
  let preimages : Person.t list =
    List.map
      Person.of_tuple
      [("Haskell", Some "Brooks", "Curry"); ("Alonzo", None, "Church")]
  in
  produce_sourcefile
    ~preimages
    ~modulename:"sample_person"
    ~typename:Person.schema_name
    Person.encoding ;
  let preimages : IPv4.t list =
    [(127, 0, 0, 1); (192, 168, 1, 1); (8, 8, 8, 8)]
  in
  produce_sourcefile
    ~preimages
    ~modulename:"sample_ipv4"
    ~typename:IPv4.schema_name
    IPv4.encoding ;
  let preimages : FilePath.t list =
    FilePath.
      [
        Absolute ["usr"; "bin"; "bash"]; Relative ["src"; "common"; "printer.ml"];
      ]
  in
  produce_sourcefile
    ~preimages
    ~modulename:"sample_filepath"
    ~typename:FilePath.schema_name
    FilePath.encoding ;
  let preimages : Nat.t list = Nat.[Z; S Z; S (S (S (S Z)))] in
  produce_sourcefile
    ~preimages
    ~modulename:"sample_nat"
    ~typename:Nat.schema_name
    Nat.encoding ;
  let preimages : TTOL.t list =
    TTOL.
      [
        TwoTruths (true, true);
        OneLie false;
        OneLie true;
        TwoTruths (true, false);
      ]
  in
  produce_sourcefile
    ~preimages
    ~modulename:"sample_ttol"
    ~typename:TTOL.schema_name
    TTOL.encoding ;
  let preimages : int list list = [[]; [0]; [17; 19]; [1; 1; 2; 3; 5]] in
  produce_sourcefile
    ~preimages
    ~modulename:"sample_shortintseq"
    ~typename:"ShortIntSeq"
    (Variable.list ~max_length:9 int31) ;
  ()

let rust_main () =
  main_base () ;
  main_comp () ;
  main_prod () ;
  main_sum () ;
  main_samples () ;
  ()

let () = rust_main ()
