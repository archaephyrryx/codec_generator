(** In the current implementation of [Data_encoding.Encoding.Fixed.add_padding],
    there is an undocumented invariant that rejects argument encodings whose
    binary length is non-fixed.

    In order to track the evolution of this invariant, and more specifically to
    provide a notice if it is ever relaxed or modified, this test is designed to
    ensure that a variety of fixed-length encodings are all supported, and
    conversely, a sufficient variety of non-fixed-length cases are all rejected. *)
open Data_encoding

let check_padding ?title ?(should_exn = false) enc =
  match Encoding.Fixed.add_padding enc 4 with
  | exception Invalid_argument _ ->
      if should_exn then ()
      else
        failwith
          ("Fixed.add_padding rejected an encoding that was expected to be \
            accepted: "
          ^ Option.value ~default:"?" title)
  | exception _ -> assert false
  | _enc ->
      if not should_exn then ()
      else
        failwith
          ("Fixed.add_padding accepted an encoding that was expected to be \
            rejected: "
          ^ Option.value ~default:"?" title)

(** Cases that are expected to succeed due to satisfying the fixed-length invariant *)
let accepted_cases () =
  check_padding ~title:"unit" Encoding.unit ;
  check_padding ~title:"int8" Encoding.int8 ;
  check_padding ~title:"4bytes" (Encoding.Fixed.bytes 4) ;
  check_padding ~title:"unit4" (Encoding.Fixed.add_padding Encoding.unit 4) ;
  check_padding
    ~title:"4x4bytes"
    (Encoding.Fixed.list 4 (Encoding.Fixed.bytes 4)) ;
  check_padding
    ~title:"foobar"
    Encoding.(
      union
        [
          case
            ~title:"foo"
            (Tag 0)
            (Fixed.add_padding uint8 1)
            (function `Foo x -> Some x | _ -> None)
            (fun x -> `Foo x);
          case
            ~title:"bar"
            (Tag 1)
            uint16
            (function `Bar x -> Some x | _ -> None)
            (fun x -> `Bar x);
        ]) ;
  ()

(** Cases that are expected to fail due to violating the fixed-length invariant *)
let rejected_cases () =
  check_padding ~should_exn:true Encoding.bytes ;
  check_padding ~should_exn:true Encoding.Variable.bytes ;
  check_padding ~should_exn:true Encoding.z ;
  check_padding ~should_exn:true (Encoding.list Encoding.bool) ;
  ()

(** Cases that do not exhibit the expected behavior despite
    not breaking the invariant, due to failure of classification *)
let unexpected_cases () =
  check_padding
    ~title:"nonrec_bool"
    (Encoding.mu "nonrec_bool" (fun _ -> Encoding.bool)) ;
  ()

let () =
  accepted_cases () ;
  rejected_cases () ;
  ()
