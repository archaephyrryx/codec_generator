open Data_encoding
open Codec_generator.Common

let verify t enc = assert (Simplified.(t =~ of_encoding enc)) [@@inline]

let test_simplified_of_encoding () =
  verify Simplified.uint8 uint8 ;
  verify Simplified.uint8 (tup1 uint8) ;
  verify Simplified.uint8 (def "foo" uint8) ;
  verify Simplified.uint8 (ranged_int 0x00 0xff) ;
  verify Simplified.uint8 (splitted ~json:uint16 ~binary:uint8) ;
  verify Simplified.uint8 (conv Fun.id Fun.id uint8) ;
  verify Simplified.int8 int8 ;
  verify Simplified.int8 (tup1 int8) ;
  verify Simplified.int8 (def "foo" int8) ;
  verify Simplified.int8 (ranged_int ~-0x80 0x7f) ;
  verify Simplified.int8 (splitted ~json:uint16 ~binary:int8) ;
  verify Simplified.int8 (conv Fun.id Fun.id int8) ;
  verify Simplified.bool bool ;
  verify Simplified.bool (tup1 bool) ;
  verify Simplified.bool (def "foo" bool) ;
  verify Simplified.bool (splitted ~json:bool ~binary:bool) ;
  verify Simplified.bool (conv Fun.id Fun.id bool) ;
  verify
    Simplified.(Prod (Record {id = None; fields = [("foo", uint8)]}))
    (obj1 (req "foo" uint8)) ;
  verify Simplified.unit unit ;
  verify Simplified.unit empty ;
  verify Simplified.unit null ;
  verify Simplified.unit @@ constant "foo" ;
  ()

let () = test_simplified_of_encoding ()
