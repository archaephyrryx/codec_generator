open Compiler_lib

let () =
  let config =
    Config.
      {
        default_config with
        verbosity = Silent;
        targets =
          default_targets
          |> List.filter (function
                 | ForLang (Typescript _) -> true
                 | _ -> false);
      }
  in
  let env = Pipeline.get_env ~config ()
  and conf = Pipeline.configure ~config () in
  let inputs =
    [
      Pipeline.of_registration
        ( "foo",
          Any
            Data_encoding.(
              Encoding.(
                def "foo"
                @@ obj3
                     (req "x" string)
                     (opt "y" (list bool))
                     (req "z" (tup2 int8 int8)))) );
    ]
  in
  Pipeline.run_pipeline ~env ~conf inputs
